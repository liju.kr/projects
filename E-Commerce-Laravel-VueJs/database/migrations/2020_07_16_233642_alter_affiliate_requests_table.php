<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAffiliateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate_requests', function (Blueprint $table) {
            //
            $table->string('website')->nullable()->after('payout_email');
            $table->text('description')->nullable()->after('website');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_requests', function (Blueprint $table) {
            //
            $table->dropColumn('website');
                 $table->dropColumn('description');
        });
    }
}
