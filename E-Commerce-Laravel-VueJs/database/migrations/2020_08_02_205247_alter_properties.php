<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->text('svg')->nullable()->after('name');
            $table->integer('default_status')->default(0)->after('svg');
        });
        Schema::table('file_types', function (Blueprint $table) {
            $table->text('svg')->nullable()->after('name');
            $table->integer('default_status')->default(0)->after('svg');
        });
        Schema::table('application_supports', function (Blueprint $table) {
            $table->text('svg')->nullable()->after('name');
            $table->integer('default_status')->default(0)->after('svg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('svg');
            $table->dropColumn('default_status');
        });
        Schema::table('file_types', function (Blueprint $table) {
            $table->dropColumn('svg');
            $table->dropColumn('default_status');
        });
        Schema::table('application_supports', function (Blueprint $table) {
            $table->dropColumn('svg');
            $table->dropColumn('default_status');
        });
    }
}
