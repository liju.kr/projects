<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrateTableProductSubCategoryFileTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sub_category_file_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_sub_category_id')->references('id')->on('product_sub_categories')->onDelete('cascade')->nullable();
            $table->integer('file_type_id')->references('id')->on('file_types')->onDelete('cascade')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sub_category_file_types');
    }
}
