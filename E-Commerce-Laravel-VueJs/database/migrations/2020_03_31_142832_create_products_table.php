<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('admin_id')->references('id')->on('users')->nullable();
            $table->integer('author_id')->references('id')->on('users')->nullable();
            $table->integer('product_sub_category_id')->references('id')->on('product_sub_categories')->onDelete('cascade')->nullable();
            $table->string('name')->nullable();
            $table->double('price')->default(0);
            $table->text('tag_line')->nullable();
            $table->text('external_url')->nullable();
            $table->longText('description')->nullable();
            $table->text('product_file')->nullable();
            $table->text('cover_photo')->nullable();
            $table->integer('status')->default(0);
            $table->integer('update_status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
