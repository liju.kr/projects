<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagicPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magic_pins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('secret_pin')->nullable();
            $table->string('assign_to')->nullable();
            $table->integer('user_id')->references('id')->on('users')->onDelete('cascade')->nullable();
            $table->integer('admin_id')->references('id')->on('users')->onDelete('cascade')->nullable();
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magic_pins');
    }
}
