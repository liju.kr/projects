<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterInvoiceTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->integer('download_id')->references('id')->on('downloads')->after('admin_id')->nullable();
            $table->double('author_fee')->after('download_id')->default(0);
            $table->double('affiliate_fee')->after('author_fee')->default(0);
            $table->double('market_fee')->after('affiliate_fee')->default(0);
            $table->string('transaction_id')->after('market_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
            $table->dropColumn('download_id');
            $table->dropColumn('author_fee');
            $table->dropColumn('affiliate_fee');
            $table->dropColumn('market_fee');
            $table->dropColumn('transaction_id');
        });
    }
}
