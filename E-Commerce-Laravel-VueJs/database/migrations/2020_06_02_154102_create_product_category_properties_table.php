<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_category_id')->references('id')->on('product_categories')->onDelete('cascade')->nullable();
            $table->integer('property_id')->references('id')->on('properties')->onDelete('cascade')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category_properties');
    }
}
