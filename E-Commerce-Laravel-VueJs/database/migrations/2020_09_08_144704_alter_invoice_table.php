<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
            $table->string('title')->after('id')->nullable();
            $table->date('bill_date')->after('title')->nullable();
            $table->string('bill_no')->after('bill_date')->nullable();
            $table->string('account_type')->after('bill_no')->nullable();
            $table->double('amount')->after('account_type')->default(0);
            $table->string('type')->after('amount')->nullable();
            $table->string('description')->after('type')->nullable();
            $table->integer('admin_id')->references('id')->on('users')->after('description')->nullable();
            $table->integer('status')->default('1')->after('admin_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
            $table->dropColumn('title');
            $table->dropColumn('bill_date');
            $table->dropColumn('bill_no');
            $table->dropColumn('account_type');
            $table->dropColumn('amount');
            $table->dropColumn('type');
            $table->dropColumn('description');
            $table->dropColumn('admin_id');
            $table->dropColumn('status');
        });
    }
}
