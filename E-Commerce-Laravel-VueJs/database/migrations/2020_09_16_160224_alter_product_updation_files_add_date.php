<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductUpdationFilesAddDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_updation_files', function (Blueprint $table) {
            //
            $table->dateTime('approved_on')->nullable()->after('status');
            $table->integer('admin_id')->references('id')->on('users')->nullable()->after('approved_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_updation_files', function (Blueprint $table) {
            //
            $table->dropColumn('approved_on');
            $table->dropColumn('admin_id');
        });
    }
}
