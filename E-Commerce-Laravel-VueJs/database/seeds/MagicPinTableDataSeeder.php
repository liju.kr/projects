<?php

use Illuminate\Database\Seeder;
use \App\Models\User\MagicPin;

class MagicPinTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for ($i=0; $i<500; $i++)
       {
           MagicPin::create([
               'secret_pin' => mt_rand(10001,99999),
               'status' => 0
           ]);
       }
    }
}
