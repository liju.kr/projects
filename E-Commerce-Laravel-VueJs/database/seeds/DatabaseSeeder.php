<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AutomaticMessagesTableDataSeeder::class);
        $this->call(AutomaticNotificationsTableDataSeeder::class);
        $this->call(PoliciesTableDataSeeder::class);
        $this->call(MagicPinTableDataSeeder::class);
    }
}
