<?php

use Illuminate\Database\Seeder;
use App\Models\Message\AutomaticMessage;
class AutomaticMessagesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AutomaticMessage::create([
            'slug' => "welcome_message",
            'message' => "You're In, We are happy to have you on board.",
            'status' => 1
        ]);

        AutomaticMessage::create([
            'slug' => "product_approved",
            'message' => "Your Product approved.",
            'status' => 1
        ]);

        AutomaticMessage::create([
            'slug' => "product_removed",
            'message' => "Your product has been removed.",
            'status' => 1
        ]);

        AutomaticMessage::create([
            'slug' => "affiliate_approved",
            'message' => "Hurrrray! You are an affiliate partner now.",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "author_approved",
            'message' => "Yeah! You got a Green flag. Set up your shop now.",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "author_removed",
            'message' => "Your shop has been removed.",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "author_suspended",
            'message' => "You’re Out, we are sad to see you go, hope you will be back!",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "payment_success",
            'message' => "“Thank ewe” We got your payment. Your download will begin shortly.",
            'status' => 1
        ]);

        AutomaticMessage::create([
            'slug' => "payment_declined",
            'message' => "Ouch! Transaction declined. Hope you will “retry soon”.",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "payment_un_success",
            'message' => "Ouch! Transaction unsuccessful. Hope you will “retry soon”.",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "payout_processed",
            'message' => "Hurrey! We have processed your happiness, via PayPal!",
            'status' => 1
        ]);
        AutomaticMessage::create([
            'slug' => "payout_rejected",
            'message' => "Oops! Your payout is rejected, contact support.",
            'status' => 1
        ]);

    }
}
