<?php

use Illuminate\Database\Seeder;
use App\Models\Message\AutomaticNotification;


class AutomaticNotificationsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AutomaticNotification::create([
            'slug' => "welcome_toast",
            'message' => "Logged In Successfully",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "product_created",
            'message' => "Product Uploaded, Waiting For Review",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "product_updated",
            'message' => "Product Updated, Waiting For Review",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "request_submitted",
            'message' => "Submit Successfully",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "update_success",
            'message' => "Update Successfully",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "try_again_later",
            'message' => "Please Try Again Later",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "wrong",
            'message' => "Something Went To Wrong",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "error",
            'message' => "Ooops Error",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "success",
            'message' => "Hurray Success",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "payment_success",
            'message' => "“Thank ewe” We got your payment",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "payment_declined",
            'message' => "Ouch! Transaction declined",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "payment_un_success",
            'message' => "Ouch! Transaction unsuccessful",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "download_success",
            'message' => "Ready to Download",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "add_cart",
            'message' => "Added to Cart",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "remove_cart",
            'message' => "Remove From Cart",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "cart_exist",
            'message' => "Already Added",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "follow",
            'message' => "Follow Successfully",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "un_follow",
            'message' => "UnFollow Successfully",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "favorite",
            'message' => "Favorite Successfully",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "un_favorite",
            'message' => "UnFavorite Successfully",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "rate",
            'message' => "Product Rated",
            'status' => 1
        ]);

        AutomaticNotification::create([
            'slug' => "complete_profile",
            'message' => "Complete Your Profile First",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "sent_support_mail_success",
            'message' => "We Will Get Back To You Soon",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "magic_pin_success",
            'message' => "Hoorray Success",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "magic_pin_failed",
            'message' => "Failed, Try again later",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "password_update_success",
            'message' => "Success",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "password_wrong",
            'message' => "Wrong",
            'status' => 1
        ]);
        AutomaticNotification::create([
            'slug' => "news_subscription_success",
            'message' => "Success",
            'status' => 1
        ]);

    }

}
