<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//
Route::get('fileupload','TestController@create');
//Route::post('fileupload','TestController@fileUpload');

//Route::get('/email-shop',function () {
//    $data = [
//        'name' => "Liju",
//        'image' => 'https://www.creativegoods.net/images/approve.png',
//        'subject' => "Your shop has been Approved",
//        'message' => "Your Shop Has been Aprroved Message",
//        'button' => "View Dashboard",
//        'button_url' => "https://www.creativegoods.net/home",
//    ];
//    return view('emails/shop_test', compact('data'));
//});
//
//Route::get('/email-product',function () {
//    $data = [
//        'name' => "Liju",
//        'image' => 'https://creativegoods-production.s3.us-east-2.amazonaws.com/uploads/products/cover_photos/1599649158.jpeg',
//        'subject' => "Your Product has been Approved",
//        'message' => "Your Product Has been Aprroved Message",
//        'button' => "View Product",
//        'button_url' => "https://www.creativegoods.net/home",
//    ];
//    return view('emails/product_test', compact('data'));
//});

//Route::get('/email-verify',function () {
//    return view('emails/verify_test');
//});
//
//Route::get('/password-reset',function () {
//    return view('emails/reset_test');
//});
//
//Route::get('/invoice',function () {
//    return view('invoice');
//});
//
//Route::get('/pdf',function () {
//    return view('pdf');
//});

Route::get('/img/{id}',function () {
    return view('main/img');
});

Route::get('/coming-soon',function () {
    return view('main/coming_soon');
});

Auth::routes(['verify' => true,]);



// login free pages

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');


Route::post('/add_to_cart', 'Main\CartController@addToCart');
Route::post('/remove_cart', 'Main\CartController@removeFromCart');

Route::get('/about-us', 'Main\PrivacyPolicyController@getAboutUsPage');

Route::get('/welcome-shop', 'Main\UserController@welcomeShop');
Route::get('/welcome-affiliate', 'Main\AffiliateController@welcomeAffiliate');


Route::get('/affiliate-privacy-policy', 'Main\PrivacyPolicyController@getPrivacyPolicyPage');
Route::get('/seller-privacy-policy', 'Main\PrivacyPolicyController@getPrivacyPolicyPage');

Route::get('/help', 'Main\HelpController@getHelpPage');
Route::get('/help/search/{category_slug?}', 'Main\HelpController@getHelpSearchPage');
Route::get('/help/{category_slug}', 'Main\HelpController@getHelpCategoryPage');
Route::get('/help/{category_slug}/{detail_slug}', 'Main\HelpController@getHelpDetailPage');


Route::get('/all-items/{category_slug?}/{sub_category_slug?}', 'Main\ProductController@getShopPage');
Route::get('/get_shop_products', 'Main\ProductController@getShopProducts');
Route::get('/get_shop_product_sub_categories', 'Main\ProductController@getShopProductCategories');
Route::get('/get_profile_shop_products', 'Main\ProductController@getProfileShopProducts');
Route::get('/fetch_fonts', 'Main\ProductController@fetchFonts');


Route::get('/{store_slug}/item/{product_slug}', 'Main\ProductController@getProductDetail');
Route::post('/{store_slug}/item/{product_slug}/buy-now', 'Main\ProductController@buyNow');
Route::post('/change-font-preview-text', 'Main\FontPreviewController@changeFontText');

$router = app()->make('router');
$store_slugs  = \App\Models\User\UserProfile::where('store_slug', '!=', NULL)->where('store_slug', '!=', "")->pluck('store_slug');
foreach($store_slugs as $store_slug) $router->get('/'.$store_slug, 'Main\DashboardController@getUserProfile');

Route::post('/news-letter-subscriptions', 'Main\SupportController@subscribeNewsLetter');

Route::get('/product_details_message', 'Main\RedirectController@productDetail');
Route::get('/settings_message', 'Main\RedirectController@settingsMessage');

Route::get('/get_featured', 'HomeController@getFeatured');
Route::get('/get_most_downloaded', 'HomeController@getMostDownloaded');

Route::get('/handpicked',function () {
    return view('main/coming_soon');
});

Route::get('/community',function () {
    return view('main/coming_soon');
});

Route::get('/discussions',function () {
    return view('main/coming_soon');
});

Route::get('/blog',function () {
    return view('main/coming_soon');
});


Route::post('/magic-pin', 'Main\MagicPinController@checkMagicPin');
Route::get('/read/{policy_slug}', 'Main\PrivacyPolicyController@getPolicyPage');
Route::get('/license', 'Main\PrivacyPolicyController@getLicensePage');

Route::get('/support/{category?}', 'Main\SupportController@getSupportPage');
Route::post('/send-support-mail', 'Main\SupportController@sendSupportMail');



// if authenticated
Route::middleware(['auth','verified', 'check_user_status'])->group(function () {


    Route::get('/cart', 'Main\CartController@getCartItems');

    Route::get('/suspended', function () { return view('auth/suspended');});

    Route::get('/ajax_product_categories', 'Main\ProductController@getProductCategories');
    Route::get('/ajax_product_sub_categories', 'Main\ProductController@getProductSubCategories');
    Route::get('/ajax_product_properties', 'Main\ProductController@getProductProperties');

    Route::get('/get_my_items', 'Main\DashboardController@getMyItems');

    Route::get('/get_my_sales_author', 'Main\DashboardController@getMySalesAuthor');

    Route::get('/get_my_downloads', 'Main\DashboardController@getMyDownloads');

    Route::get('/get_my_favorites', 'Main\DashboardController@getMyFavorites');

    Route::post('/affiliate_earnings', 'Main\AffiliateController@affiliateEarnings');

    Route::get('/get_my_affiliate_sales_all', 'Main\AffiliateController@getAffiliateSalesAll');
    Route::get('/get_my_affiliate_sales_most', 'Main\AffiliateController@getAffiliateSalesMost');

    Route::get('/get_my_notifications', 'Main\NotificationController@getMyNotifications');
    Route::get('/get_my_followers', 'Main\NotificationController@getMyFollowers');
    Route::get('/get_my_messages', 'Main\MessageController@getMyMessages');
    Route::get('/get_my_message_users', 'Main\MessageController@getMyMessageUsers');


    Route::get('/{store_slug}/item/{product_slug}/buy-now/pay-with-paypal', 'Main\PaymentController@checkPayment');
    Route::get('/cart/pay-with-paypal', 'Main\PaymentController@checkPayment');
    Route::get('/payment-status', 'Main\PaymentController@getPaymentStatus');
    Route::get('/paypal-payments', 'Main\PaymentController@payWithpaypal');
    //Route::post('/payment/add-funds/paypal', 'Main\PaymentController@payWithpaypal');

    Route::get('/{store_slug}/item/{product_slug}/buy-now/pay-with-stripe', 'Main\StripePaymentController@stripe');
    Route::get('/cart/pay-with-stripe', 'Main\StripePaymentController@stripe');

    Route::post('stripe', 'Main\StripePaymentController@stripePost')->name('stripe.post');

    Route::get('/seller-application', 'Main\UserController@becomeSeller');
    Route::get('/affiliate-application', 'Main\AffiliateController@becomeAffiliate');

    Route::get('/complete-profile', 'Main\RedirectController@settingsComplete');



    //follow
    Route::post('follow', 'Main\UserController@followUserRequest')->name('follow');
    //favorite
    Route::post('favorite', 'Main\UserController@favoriteProduct')->name('favorite');

    Route::put('/buy_now_free_cart', 'Main\PurchaseController@buyNowFreeCart');
    Route::put('/buy_now_free', 'Main\PurchaseController@buyNowFree');
    Route::post('/rate_my_item', 'Main\ReviewController@rateMyItem');

    Route::post('/sent_message', 'Main\MessageController@sentMessage');
    Route::post('/message_subject', 'Main\MessageController@messageSubject');

    Route::post('/font_preview_check', 'Main\ProductController@testPreview');

    Route::get('/{store_slug}/item/{product_slug}/edit-product', 'Main\ProductController@updateForm');
    Route::post('/{store_slug}/item/{product_slug}/edit_product', 'Main\ProductController@updateProduct');

    Route::get('/{store_slug}/item/{product_slug}/favorite', 'Main\ProductController@getProductDetail');

    $router = app()->make('router');
    $store_slugs  = \App\Models\User\UserProfile::where('store_slug', '!=', NULL)->where('store_slug', '!=', "")->pluck('store_slug');
    foreach($store_slugs as $store_slug) $router->get('/'.$store_slug.'/{message_follow?}', 'Main\DashboardController@getUserProfile');

    Route::prefix('/user/{user_name}/{user_id}')->middleware(['user_or_author'])->group(function () {



        Route::get('/affiliate/{tab?}', 'Main\AffiliateController@getAffiliateDashboard');

        Route::get('/dashboard/{tab1?}/{tab2?}', 'Main\DashboardController@getDashboardData');



        Route::get('/messages/', 'Main\MessageController@getMessages');
        Route::get('/notifications/{type}', 'Main\NotificationController@getNotifications');

        Route::get('/create-product', 'Main\ProductController@uploadForm');


        Route::get('/download-my-item', 'Main\PurchaseController@downloadMyItem');
        Route::get('/download-my-own-item', 'Main\PurchaseController@downloadMyOwnItem');



        Route::post('/update_author_profile', 'Main\UserController@updateAuthorProfile');
        Route::post('/update_user_profile', 'Main\UserController@updateUserProfile');
        Route::post('/update_password', 'Main\UserController@updatePassword');

        Route::put('/become_a_seller', 'Main\UserController@updateUserToAuthor');
        Route::put('/become_a_affiliate', 'Main\AffiliateController@updateUserToAffiliate');
        Route::put('/profile_pic', 'Main\UserController@updateProfilePic');

        Route::post('/create_product', 'Main\ProductController@createProduct');




    });

});

//SubAdmins

Route::prefix('cg_admin', 'verified')->middleware(['auth','accounting_admin'])->group(function () {

    Route::get('/dashboard/', 'Admin\AdminDashboardController@getAdminDashboardData');

    Route::resource('invoices', 'Admin\Accounts\InvoiceController');

});

Route::prefix('cg_admin', 'verified')->middleware(['auth','marketing_admin'])->group(function () {

    Route::get('/dashboard/', 'Admin\AdminDashboardController@getAdminDashboardData');

    Route::get('/products/', 'Admin\Product\ProductController@index');

    Route::get('/products/{id}', 'Admin\Product\ProductController@show');
    Route::get('/products/{id}/seo', 'Admin\Product\ProductController@getMetaDetail');

    Route::put('/products/{id}/seo', 'Admin\Product\ProductController@updateProductMetaStatus');

    Route::get('/sales/', 'Admin\Sales\SaleController@index');
});

//Admin
Route::prefix('cg_admin')->middleware(['auth','admin', 'verified'])->group(function () {

    //post put

    Route::get('/site-settings/', 'Admin\SiteSettingsController@getSiteSettingsData');

    //user / author / affiliates

    Route::get('/users/{type}', 'Admin\User\UserController@getUsers');
    Route::get('/users/{type}/{id}/{tab?}', 'Admin\User\UserController@getUserDetail');

    // author requests get
    Route::get('/author_requests/', 'Admin\User\UserController@getAuthorRequests');
    Route::get('/author_requests/{id}/review', 'Admin\User\UserController@getAuthorReviewDetail');
    // author requests post
    Route::put('/author_requests/{id}/review', 'Admin\User\UserController@updateUserAuthorStatus');
    Route::put('/users/{type}/{id}/enable_disable', 'Admin\User\UserController@enableDisable');


    //product get

    Route::get('/products/{id}/review', 'Admin\Product\ProductController@getReviewDetail');
    Route::get('/products/{id}/edit', 'Admin\Product\ProductController@edit');



    Route::get('/product_updates/{id}/review', 'Admin\Product\ProductUpdateController@getReviewDetail');
    Route::get('/product_updates/{product_id?}/{status?}', 'Admin\Product\ProductUpdateController@index');

    //product post put
    Route::put('/products/{id}/review', 'Admin\Product\ProductController@updateProductStatus');
    Route::put('/products/{id}/edit', 'Admin\Product\ProductController@updateProduct');
    Route::put('/products/{id}/enable_disable', 'Admin\Product\ProductController@enableDisable');
    Route::put('/product_category/{id}/edit', 'Admin\Product\ProductController@updateProductCategory');

    Route::post('/product_previews', 'Admin\Product\ProductController@storeProductPreview');
    Route::put('/product_previews/{id}/edit', 'Admin\Product\ProductController@updateProductPreview');
    Route::delete('/product_previews/{id}/delete', 'Admin\Product\ProductController@destroyProductPreview');

    Route::post('/font_previews', 'Admin\Product\ProductController@storeFontPreview');
    Route::put('/font_previews/{id}/edit', 'Admin\Product\ProductController@updateFontPreview');
    Route::delete('/font_previews/{id}/delete', 'Admin\Product\ProductController@destroyFontPreview');

    Route::put('/product_updates/{id}/review', 'Admin\Product\ProductUpdateController@updateProductStatus');
    Route::put('/product_updates/{id}/edit', 'Admin\Product\ProductUpdateController@updateProductUpdate');
    Route::delete('/product_updates/{id}/delete', 'Admin\Product\ProductUpdateController@destroy');


    //product properties get
    Route::get('/product_properties/{property_name}', 'Admin\Product\ProductPropertyController@index');
    Route::get('/product_properties/{property_name}/create', 'Admin\Product\ProductPropertyController@create');
    Route::get('/product_properties/{property_name}/{id}/edit', 'Admin\Product\ProductPropertyController@edit');
   //product properties post put
    Route::post('/product_properties/{property_name}', 'Admin\Product\ProductPropertyController@store');
    Route::put('/product_properties/{property_name}/{id}/update', 'Admin\Product\ProductPropertyController@update');
    Route::delete('/product_properties/{property_name}/{id}/delete', 'Admin\Product\ProductPropertyController@destroy');

    //affiliate_requests get
    Route::get('/affiliate_requests/', 'Admin\User\AffiliateController@index');
    Route::get('/affiliate_requests/{id}', 'Admin\User\AffiliateController@show');
    Route::get('/affiliate_requests/{id}/review', 'Admin\User\AffiliateController@getReviewDetail');
    //affiliate_requests post put
    Route::put('/affiliate_requests/{id}/review', 'Admin\User\AffiliateController@updateAffiliateStatus');
    Route::delete('/affiliate_requests/{id}/delete', 'Admin\User\AffiliateController@destroy');

    Route::get('/download_by_admin', 'Admin\Product\ProductController@downloadByAdmin');
    Route::get('/update_download_by_admin', 'Admin\Product\ProductController@updateDownloadByAdmin');

    //Resources
    Route::resource('product_categories', 'Admin\Product\ProductCategoryController');
    Route::resource('product_sub_categories', 'Admin\Product\ProductSubCategoryController');


    Route::resource('help_categories', 'Admin\Help\HelpCategoryController');
    Route::resource('help_topics', 'Admin\Help\HelpTopicController');
    Route::resource('policies', 'Admin\Help\PolicyController');
    Route::resource('magic_pins', 'Admin\User\MagicPinController');
    Route::resource('automatic_notifications', 'Admin\Help\AutomaticNotificationController');
    Route::resource('automatic_messages', 'Admin\Help\AutomaticMessageController');




    Route::post('/editor/upload', 'Admin\CkeditorController@upload')->name('editor.upload');





Route::get('/support', function () {
    return view('admin/support/list');
});
Route::get('/support/detail', function () {
    return view('admin/support/detail');
});


Route::get('/notifications', function () {
    return view('admin/notifications/notifications');
});

});



