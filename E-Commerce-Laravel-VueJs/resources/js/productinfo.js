import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLazyload from 'vue-lazyload'

Vue.use(VueRouter)

Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: '/images/miniloader.svg',
  attempt: 1
})

import DirectBuy from './views/DirectBuy'

const SimilarProduct = Vue.component('similar-products', require('./components/SimilarProduct.vue').default);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path:  product_url +'/buy_now',
            name: 'buy_now',
            component: DirectBuy,
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});

const similar_products = new Vue({
    el: '#similar-products',
    router,
    //components: { SimilarProduct },
});
