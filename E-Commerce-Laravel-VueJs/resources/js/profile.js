import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import ProfileItems from './views/ProfileItems'

const router = new VueRouter({
	 mode: 'history',
    routes: [{
            path: profile_url,
         	name: 'profile',
            component: ProfileItems
        },
       ]
});



const profile = new Vue({
    el: '#profile',
    // components: { Dashboard },
    router,
    computed:{
        currentRouteName() {
            return this.$route.name;
        }
    },
    data: {
        // declare message with an empty value
        settings: false,
    },
    mounted(){
        if(this.currentRouteName == 'settings'){

            this.settings = true

        }else{
            this.settings = false
        }
    },
    beforeUpdate(){
        if(this.currentRouteName == 'settings'){

            this.settings = true

        }else{
            this.settings = false
        }

    }

});
