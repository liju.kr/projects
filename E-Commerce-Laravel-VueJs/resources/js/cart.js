import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Cart from './views/Cart'
// import DirectBuy from './views/DirectBuy'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/cart',
            name: 'cart',
            component: Cart
        },
        // {
        //     path: '/user/createproduct',
        //     redirect: '/user/dashboard/downloads'
        // },
    ],
    
});

const app = new Vue({
    el: '#cart',
    router,
    computed:{
        // currentPage(){
        //     return this.$route.path;
        // },
    },
    mounted(){
        // console.log(this.$route.path)
        // console.log(unique_id)
        // :class="[currentPage.includes('favorites') ? activeClass :'','active']"
    }
});
