import Vue from 'vue'
import VueRouter from 'vue-router'
import VuePreloaders from 'vue-preloaders'
import VueLazyload from 'vue-lazyload'


Vue.use(VuePreloaders, /*{ options }*/)
Vue.use(VueRouter)

Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: '/images/miniloader.svg',
  attempt: 1
})




const ShopProducts = Vue.component('shop-products', require('./components/ShopProducts.vue').default);




const products = new Vue({
    el: '#shop-products',
	components: { ShopProducts },
});
