import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// import App from './views/App'
import Register from './views/Register'
import Login from './views/Login'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/account/login',
            name: 'login',
            component: Login
        },
        {
            path: '/account/register',
            name: 'register',
            component: Register,
        },
    ],
});

const app = new Vue({
    el: '#app',
    // components: { App },
    router,
});