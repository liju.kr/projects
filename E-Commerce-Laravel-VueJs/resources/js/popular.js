import Vue from 'vue';
import VueRouter from 'vue-router';
import VueLazyload from 'vue-lazyload'
// import VuePreloaders from 'vue-preloaders'

// Vue.use(VuePreloaders, /*{ options }*/)

Vue.use(VueRouter);

Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: '/images/miniloader.svg',
  attempt: 1
})


const Popular = Vue.component('popular', require('./components/Popular.vue').default);


const router = new VueRouter({
  mode: 'history'
})


const popular = new Vue({
    el: '#popular',
    components: { Popular },
});
