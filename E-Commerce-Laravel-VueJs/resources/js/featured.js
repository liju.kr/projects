import Vue from 'vue';
import VueRouter from 'vue-router';
import VueLazyload from 'vue-lazyload'
// import VuePreloaders from 'vue-preloaders'

// Vue.use(VuePreloaders, /*{ options }*/)

Vue.use(VueRouter);

Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: '/images/miniloader.svg',
  attempt: 1
})


const Featured = Vue.component('featured', require('./components/Featured.vue').default);


const router = new VueRouter({
  mode: 'history'
})


const featured = new Vue({
    el: '#featured', 
    components: { Featured },
});