import Vue from 'vue'
import Preview from './components/Preview'

Vue.config.productionTip = false

new Vue({
  render: h => h(Preview)
}).$mount('#app')
