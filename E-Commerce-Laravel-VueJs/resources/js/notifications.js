import Vue from 'vue'
import VueRouter from 'vue-router'
import VuePreloaders from 'vue-preloaders'

Vue.use(VuePreloaders, /*{ options }*/)

Vue.use(VueRouter)


import Notifications from './views/Notifications'
import NotificationRatings from './views/NotificationRatings'
import NotificationSales from './views/NotificationSales'
import NotificationFollows from './views/NotificationFollows'
import NotificationFollowers from './views/NotificationFollowers'
import NotificationFavorites from './views/NotificationFavorites'



const router = new VueRouter({
    mode: 'history',
    routes: [

        {
            path: user_url +'/notifications/all',
            name: 'notifications',
            component: Notifications
        },

        {
            path: user_url +'/notifications/ratings',
            name: 'notification_ratings',
            component: NotificationRatings
        },
        {
            path: user_url +'/notifications/sales',
            name: 'notification_sales',
            component: NotificationSales
        },
        {
            path: user_url +'/notifications/follows',
            name: 'notification_follows',
            component: NotificationFollows
        },
        {
            path: user_url+'/notifications/followers',
            name: 'notification_followers',
            component: NotificationFollowers
        },
        {
            path: user_url +'/notifications/favorites',
            name: 'notification_favorites',
            component: NotificationFavorites
        },
        // {
        //     path: '/user/'+ unique_id +'/dashboard/',
        //     redirect: '/user/'+ unique_id +'/dashboard/downloads'
        // },

    ],
});

const notifications = new Vue({
    el: '#notifications',
    // components: { Dashboard },
    router,
    computed:{
        // currentPage(){
        //     return this.$route.path;
        // },
    },
    mounted(){

        // :class="[currentPage.includes('favorites') ? activeClass :'','active']"
    }

});
