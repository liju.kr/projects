import Vue from 'vue'
import VueRouter from 'vue-router'
import VuePreloaders from 'vue-preloaders'

Vue.use(VuePreloaders, /*{ options }*/)

Vue.use(VueRouter)

// import Dashboard from './views/Dashboard'
import Downloads from './views/Downloads'
import Favourites from './views/Favourites'
import Settings from './views/Settings'
import MyProducts from './views/MyProducts'
import MySales from './views/MySales'
import MyProductsLive from './views/MyProductsLive'
import MyProductsRejected from './views/MyProductsRejected'
import MyProductsPending from './views/MyProductsPending'
import MySalesMonth from './views/MySalesMonth'
import MySalesLastMonth from './views/MySalesLastMonth'
import MySalesLastYear from './views/MySalesLastYear'
import MySalesAllTime from './views/MySalesAllTime'





const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: user_url +'/dashboard/downloads',
            name: 'downloads',
            component: Downloads
        },
        {

            path: user_url +'/dashboard/favourites',
            name: 'favourites',
            component: Favourites,
        },
        {

            path: user_url +'/dashboard/settings',
            name: 'settings',
            component: Settings,
        },
        {

            path: user_url +'/dashboard/my-products',
            name: 'my-products',
            component: MyProducts,
            redirect: user_url +'/dashboard/my-products/live',
            children: [
                    {
                      // UserProfile will be rendered inside User's <router-view>
                      // when /user/:id/profile is matched
                      name: 'live',
                      path: 'live',
                      component: MyProductsLive
                    },
                    {
                      // UserPosts will be rendered inside User's <router-view>
                      // when /user/:id/posts is matched
                      name: 'rejected',
                      path: 'rejected',
                      component: MyProductsRejected
                    },
                    {
                      // UserPosts will be rendered inside User's <router-view>
                      // when /user/:id/posts is matched
                      name: 'pending',
                      path: 'pending',
                      component: MyProductsPending
                    }
                  ]
        },
        {

            path: user_url +'/dashboard/my-sales',
            name: 'my-sales',
            component: MySales,
            redirect: user_url +'/dashboard/my-sales/this-month',
            children: [
                    {
                      // UserProfile will be rendered inside User's <router-view>
                      // when /user/:id/profile is matched
                      name: 'this-month',
                      path: 'this-month',
                      component: MySalesMonth
                    },
                    {
                      // UserPosts will be rendered inside User's <router-view>
                      // when /user/:id/posts is matched
                      name: 'last-month',
                      path: 'last-month',
                      component: MySalesLastMonth
                    },
                    {
                      // UserPosts will be rendered inside User's <router-view>
                      // when /user/:id/posts is matched
                      name: 'last-year',
                      path: 'last-year',
                      component: MySalesLastYear
                    },
                    {
                        // UserPosts will be rendered inside User's <router-view>
                        // when /user/:id/posts is matched
                        name: 'all-time',
                        path: 'all-time',
                        component: MySalesAllTime
                    }
                  ]
        },
        {
            path: user_url +'/dashboard/',
            redirect: user_url +'/dashboard/downloads'
        },
        // {
        //     path: '/user/'+ unique_id +'/my_products/',
        //     redirect: '/user/'+ unique_id +'/my_products/live'
        // },
    ],
});

const dashboard = new Vue({
    el: '#dashboard',
    // components: { Dashboard },
    router,
    computed:{
        // currentPage(){
        //     return this.$route.path;
        // },
        currentRouteName() {
        return this.$route.name;
      }
    },
    data: {
    // declare message with an empty value
     settings: true,
    },
    mounted()
    {
        if(this.currentRouteName == 'settings'){

            this.settings = true

        }else{
            this.settings = false
        }
    },
    created(){
        if(this.currentRouteName == 'settings'){

            this.settings = true

        }else{
            this.settings = false
        }
        // console.log(user_url)
        // :class="[currentPage.includes('favorites') ? activeClass :'','active']"
    },
    beforeUpdate(){
      if(this.currentRouteName == 'settings'){

        this.settings = true

      }else{
        this.settings = false
      }

    }

});



