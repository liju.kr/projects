import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

import CreateProduct from './views/CreateProduct';
import EditProduct from './views/EditProduct';
// import DirectBuy from './views/DirectBuy'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path:  user_url +'/create-product',
            name: 'createproduct',
            component: CreateProduct
        },
        {
            path: product_url +'/edit-product',
            name: 'editproduct',
            component: EditProduct
        },
        // {
        //     path: '/user/createproduct',
        //     redirect: '/user/dashboard/downloads'
        // },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});

const app = new Vue({
    el: '#product',
    router,
    computed:{
        // currentPage(){
        //     return this.$route.path;
        // },
    },
    mounted(){
        // console.log(this.$route.path)
        // console.log(unique_id)
        // :class="[currentPage.includes('favorites') ? activeClass :'','active']"
    }
});
