import Vue from 'vue'
import VueRouter from 'vue-router'
import VuePreloaders from 'vue-preloaders'

Vue.use(VuePreloaders, /*{ options }*/)
Vue.use(VueRouter)

// import App from './views/App'
// import Earning from './views/Earning'
const Earning = Vue.component('earning-tab', require('./views/AffiliateEarning.vue').default);
const Notification = Vue.component('notification-tab', require('./views/AffiliateNotification.vue').default);
const Sales = Vue.component('sales-tab', require('./views/AffiliateSales.vue').default);

// import Sales from './views/AffiliateSales'
import SalesMost from './views/AffiliateSalesMost'
import SalesSource from './views/AffiliateSalesSource'
import SalesAll from './views/AffiliateSalesAll'

// const SalesMost = Vue.component('sales-most', require('./views/AffiliateSalesMost.vue').default);

const router = new VueRouter({
    mode: 'history',
    routes: [
    {
            path: user_url +'/affiliate',
            name: 'sales-all',
            component: SalesAll,
        },
    {
            path: user_url +'/affiliate/sales-most',
            name: 'sales-most',
            component: SalesMost
        },
    {
            path: user_url +'/affiliate/sales-source',
            name: 'sales-source',
            component: SalesSource
        },
    ],
});

const affiliation = new Vue({
    el: '#affiliation',
    // components: { App },
    router,
    mounted(){
        //  console.log(this.$route.path)
        // console.log(user_url)
        // :class="[currentPage.includes('favorites') ? activeClass :'','active']"
    }
});
