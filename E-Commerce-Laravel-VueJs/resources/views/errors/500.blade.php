@extends('errors::minimal')

@section('title', __('Server Error | Creativegoods'))
@section('code', '500')
@section('image', 'images/404.svg')
@section('message', __('Server Error'))
