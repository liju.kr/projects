@extends('errors::minimal')

@section('title', __('Forbidden | Creativegoods'))
@section('code', '403')
@section('image', 'images/404.svg')
@section('message', __($exception->getMessage() ?: 'Forbidden'))
