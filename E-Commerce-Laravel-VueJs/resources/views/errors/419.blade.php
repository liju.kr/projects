@extends('errors::minimal')

@section('title', __('Page Expired | Creativegoods'))
@section('code', '419')
@section('image', 'images/404.svg')
@section('message', __('Page Expired'))
