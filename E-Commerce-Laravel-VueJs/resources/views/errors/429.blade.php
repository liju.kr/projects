@extends('errors::minimal')

@section('title', __('Too Many Requests | Creativegoods'))
@section('code', '429')
@section('image', 'images/404.svg')
@section('message', __('Too Many Requests'))
