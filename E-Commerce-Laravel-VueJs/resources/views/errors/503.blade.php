@extends('errors::minimal')

@section('title', __('Service Unavailable | Creativegoods'))
@section('code', '503')
@section('image', 'images/404.svg')
@section('message', __($exception->getMessage() ?: 'Service Unavailable'))
