@extends('errors::minimal')

@section('title', __('Unauthorized | Creativegoods'))
@section('code', '401')
@section('image', 'images/404.svg')
@section('message', __('Unauthorized'))
