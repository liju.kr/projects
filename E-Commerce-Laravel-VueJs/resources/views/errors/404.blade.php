@extends('errors::minimal')

@section('title', __("404 Page Not Found | Creativegoods"))
@section('code', '404')
@section('image', 'images/404.svg')
@section('message', __("Oops! The link is broken or we can't find a page you are looking for."))
