<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="OXSozJhF1ymNcOTObA9DZs74LfdiM8fjMxtVkdQl">
    <title>Fontfolio - Free Goods</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">

    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">
    <link href="http://127.0.0.1:8000/css/app.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://127.0.0.1:8000/stylesheets/libs/plugins.css">
    <link rel="stylesheet" type="text/css" href="http://127.0.0.1:8000/stylesheets/cg-main.css">
    <link rel="stylesheet" type="text/css" href="http://127.0.0.1:8000/stylesheets/cg-styles.css">
    <link rel="stylesheet" type="text/css" href="http://127.0.0.1:8000/stylesheets/mobile.css">
    <!-- Styles -->
</head>
<body>
<div id="preloader2">
    <div id="status2">&nbsp;</div>
</div>
<div id="master-wrap" class="master-wrap ">
    <div class="guest-header align-items-center justify-content-between d-flex">
      <img src="http://127.0.0.1:8000/images/logo1.png" class="img-fluid" alt="fontfolio">
      <button class="guest-login" type="button" name="button">Sign in</button>
    </div>
    <section class="cg-guest-wrap align-items-center justify-content-center d-flex">
      <div class="cg-guest-block align-items-center justify-content-center d-flex">
        <div class="event-img-box"><img class="img-fluid" src="images/event/guest.svg" alt="{{ env('APP_NAME') }}"></div>
      </div>
      <div class="cg-guest-form-wrap align-items-center justify-content-center d-flex flex-column">
        <div class="cg-guest-form">
          <h2>Hello,</h2>
          <h3>Fontfolio is a newborn marketplace, that aims to upthrust talented designers all around the world and to provide creative assets to help bring ideas to life.</h3>
          <div class="magic-code-box">
            <form action="/action_page.php">
              <label>Enter your Magic pin to open your shop</label>
              <div class="magic-code-items">
                <input placeholder="0" maxlength="1" size="1" id="counter">
                <input placeholder="0" maxlength="1" size="1" id="counter">
                <input placeholder="0" maxlength="1" size="1" id="counter">
                <input placeholder="0" maxlength="1" size="1" id="counter">
                <input placeholder="0" maxlength="1" size="1" id="counter">
              </div>
              <button class="guest-button" type="button" name="button">Join Fontfolio</button>
            </form>
          </div>
        </div>
      </div>
      <div class="guest-footer align-items-center justify-content-between d-flex">
        <p>© 2021 Fontfolio All Rights Reserved.</p>
        <div class="cg-sub-footer-icon-wrap">
          <ul class="cg-sub-footer-icons d-flex">
              <li><a href="https://www.facebook.com/fontfolio.net"><i class="fb-ico fab fa-facebook-f"></i></a></li>
              <li><a href="https://www.facebook.com/fontfolio.net"><i class="insta-ico fab fa-instagram"></i></a></li>
              <li><a href="https://www.facebook.com/fontfolio.net"><i class="twit-ico fab fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/fontfolio.net"><i class="fab fa-pinterest"></i></a></li>
              <li><a href="https://www.facebook.com/fontfolio.net"><i class="fab fa-behance"></i></a></li>
          </ul>
        </div>
      </div>
    </section>

<!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
</div>

<script src="http://127.0.0.1:8000/js/app.js" type="text/javascript"></script>
<script type="text/javascript" src="http://127.0.0.1:8000/javascripts/libs/navigation.js"></script>
<script src="http://127.0.0.1:8000/javascripts/libs/plugins.js"></script>
<script src="http://127.0.0.1:8000/javascripts/custom/focal.js"></script>
    <script src="http://127.0.0.1:8000/js/featured.js"></script>
    <script src="http://127.0.0.1:8000/javascripts/custom/svg-bg.js"></script>

</body>
</html>
