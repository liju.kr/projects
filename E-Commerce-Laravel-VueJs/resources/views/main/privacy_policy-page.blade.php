@extends('layouts.app', ['page_class' => 'master-wrap cg-help-view-page privacy-page seller-terms affiliate-terms first-bg'])
@section('title', $policy->title)
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">

@endsection
@section('content')
    <section class="privacy-page-wrap d-flex justify-content-center align-items-center flex-column">

      <div class="privacy-contents d-flex justify-content-center align-items-center text-center flex-column">
        <h2 class="theme-text">{{ $policy->title }}</h2>
        <h5>Know more about Fontfolio's {{ $policy->title }}</h5>
      </div>
      <div class="privacy-body-block">
      {!! $policy->description !!}
      </div>
      @if($related_policies->count())
      <div class="related-privacy-nav">
        <h3 class="f16 font1 w700 no-margin pad-btm-sml">Related Informations</h3>
        <ul class="d-flex justify-content-start align-items-center flex-wrap">
            @foreach($related_policies as $related_policy)
                <li><a href="{{ url($related_policy->getPolicyURL())}}" class="grey f13 font1">{{ $related_policy->title }}</a></li>
            @endforeach
        </ul>
      </div>
      @endif
    </section>
@endsection
