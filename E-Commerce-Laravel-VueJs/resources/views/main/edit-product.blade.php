@extends('layouts.app', ['page_class' => 'common-popup upload-dashboard font1'])
@section('title', 'Edit Product')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    <div class="lic_pop_modal d-flex justify-content-center align-items-center text-center">
        <div class="licence-pop-details flex-wrap d-flex justify-content-center align-items-center flex-column">
            <a class="preview-delete-button close-lic-modal" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></a>
            <h4>Licence Types</h4>
            <p>The price of the products will change automatically according to the license type.</p>
            <div class="lic-pop-item">
                <h5>Single License</h5>
                <ul>
                    <li>Premium Commercial License</li>
                </ul>
            </div>
            <div class="lic-pop-item">
                <h5>Multiple Licenses</h5>
                <ul>
                    <li>Personal License</li>
                    <li>Commercial License</li>
                    <li>Premium Commercial License</li>
                </ul>
            </div>
            <a class="goto-licence-page" target="_blank" href="{{ url('/license') }}">Know More</a>
        </div>
    </div>
<div class="cg-profile-over-view-wrap"></div>
<div id="product">
     <transition>
            <router-view user_url="{{$product->author->user_url}}" product_url="{{$product->product_url}}" product="{{ $product }}" get_properties="{{ $get_properties }}" get_file_types="{{ $get_file_types }}" get_application_supports="{{ $get_application_supports }}" assigned_properties="{{ $assigned_properties }}" assigned_file_types="{{ $assigned_file_types }}" assigned_application_supports="{{ $assigned_application_supports }}" ></router-view>
    </transition>
</div>
@endsection
@push('footer_scripts')
    <script>
        var user_url = "{{ $product->author->user_url }}";
        var product_url = "{{ $product->product_url }}";
    </script>
    <script src="{{ asset('js/newproduct.js') }}"></script>
    <script src="{{ asset('javascripts/custom/file-upload.js') }}"></script>
@endpush
