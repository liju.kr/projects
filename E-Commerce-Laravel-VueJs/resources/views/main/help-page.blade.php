@extends('layouts.app', ['page_class' => 'master-wrap cg-help-view-page first-bg'])

@section('title', 'Help')

@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
         <section class="cg-landing">
          <div class="cg-landing-overlay d-flex justify-content-center align-items-center text-center">
            <h3 class="first font1 w600 f26 pad-top-sml">Search our Help Center or browse FAQs and <br />popular resources.</h3>
          </div>
          <div class="cg-landing-search d-flex justify-content-center align-items-center">
              <div class="cg-search-bar">
                  <form id="help_search_form" action="{{ url('/help/search') }}" method="get">
                      <input class="font1 cg-home-search-input-section second w600 add-btm-xsml" id="search" name="key_word" placeholder="Search here" data-placeholder="Search">
                      <a type="submit" onclick="document.getElementById('help_search_form').submit();" class="cg-home-search-ico" ><i class="fas fa-search grey"></i></a>
                  </form>
              </div>
          </div>
        </section>
        <section class="container cg-help-content-wrap">
          <div><h3 class="cg-help-content-wrap-head text-center pad-top-mdm f18 font1 w700">Explore by Region</h3></div>
          <div class="cg-hel-content-card-row-1 pad-top-sml d-flex justify-content-center align-items-start text-center">
              @foreach($help_categories as $help_category)
                  @if($help_category->help_topics->count())
            <div class="cg-help-content-card">
              <h3 class="font1 f16 second w600">{{ $help_category->name }}</h3>
              <p class="font1 f14 grey w400"> {{ $help_category->description }}</p>
              <a href="{{ url($help_category->getHelpCategoryURL())}}" class="font1 w700 first f13"><span><i class="fas fa-chevron-right"></i></span></a>
            </div>
                  @endif
           @endforeach
          </div>
           @if($faqs->count())
          <div class="help-center-faq-head"><h3 class="text-center pad-top-mdm f18 font1 w700">Frequently Asked Questions</h3></div>
          <div class="cg-help-faq-list-wrap d-flex justify-content-center align-items-start">
            <div class="cg-help-faq-list-side">
              <ul class="list-group">
                  @php $i=0;  @endphp
                  @foreach($faqs as $faq)
                      @if($i % 2 ==0)
                <li>
                  <div class="d-flex justify-content-between align-items-center">
                    <button type="button" class="second f14 font1" data-toggle="collapse" data-target="#cg-hlp-list-{{ $faq->id }}">{{ $faq->title }}</button>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                  </div>
                  <div id="cg-hlp-list-{{ $faq->id }}" class="collapse">
                    <p class="font1 grey f14 add-top-sml">
                        {{ \Illuminate\Support\Str::limit(strip_tags($faq->description), 230, $end='...') }}
                    </p>
                    <a href="{{ url($faq->getHelpTopicURL())}}" class="theme font1 f12 add-btm-sml">View more</a>
                  </div>
                </li>
                      @endif
                      @php $i=$i+1;  @endphp
                  @endforeach
              </ul>
            </div>
            <div class="cg-help-faq-list-side">
              <ul class="list-group">
                  @php $j=0;  @endphp
                  @foreach($faqs as $faq)
                      @if($j % 2 !=0)
                          <li>
                            <div class="d-flex justify-content-between align-items-center">
                              <button type="button" class="second f14 font1" data-toggle="collapse" data-target="#cg-hlp-list-{{ $faq->id }}">{{ $faq->title }}</button>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                            </div>
                            <div id="cg-hlp-list-{{ $faq->id }}" class="collapse">
                                  <p class="font1 grey f14 add-top-sml">
                                      {{ \Illuminate\Support\Str::limit(strip_tags($faq->description), 230, $end='...') }}
                                  </p>
                                  <a href="{{ url($faq->getHelpTopicURL())}}" class="theme font1 f12 add-btm-sml">View more</a>
                              </div>
                          </li>
                      @endif
                      @php $j=$j+1;  @endphp
                  @endforeach
              </ul>
            </div>
          </div>
      @endif
        </section>
        <div class="cg-help-intermediate-wrap d-flex justify-content-center align-items-center flex-column">
          <div class="cg-help-inter-overlay"></div>
          <h4 class="font1 f18 first w600">Still no luck to find the right one?</h4>
          <p class="text-center light-grey font1 f14 w500">If any of the information we have provided above does not help you, please contact us. We look forward to helping you.</p>
          <a href="{{ url('/support') }}" class="font1 f14 first theme-bg w600">Contact Support</a>
        </div>
@endsection
