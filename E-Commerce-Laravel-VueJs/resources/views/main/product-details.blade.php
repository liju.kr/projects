@extends('layouts.app', ['page_class' => 'basic-product-page-wrap'])
@section('title', $product->name)
@section('meta')
    @php
        $url = URL::current();
        $fake_id = uniqid();
    @endphp
    {{--    <!-- Search Engine -->--}}
    {{--    <meta name="description" content="{{ $product->description }}">--}}
    {{--    <meta name="image" content="{{ $product->cover_photo_path }}">--}}
    {{--    <meta name="author" content="{{ $product->author->user_profile->store_name }}">--}}
    {{--    <meta name="copyright" content="{{ $product->author->user_profile->store_name }}">--}}
    {{--    <meta name="keywords" content="{{ $product->tag_line }}" />--}}
    <!-- Search Engine -->
    <meta property="description" content="{{ $product->getMetaDescription() }}">
    <meta property="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta property="author" content="{{ env('APP_NAME') }}">
    <meta property="copyright" content="{{ env('APP_NAME') }}">
    <meta property="keywords" content="{{ $product->getMetaKeyWords() }}" />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="{{ $product->getMetaDescription() }}">
    <meta content="{{ $product->getMetaImage() }}">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary">
    <meta property="twitter:title" content="{{ $product->getMetaTitle() }}">
    <meta property="twitter:description" content="{{ $product->getMetaDescription() }}">
    <meta property="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta property="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta property="twitter:image:src" content="{{ $product->getMetaImage() }}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta property="og:title" content="{{ $product->getMetaTitle() }}">
    <meta property="og:description" content="{{ $product->getMetaDescription() }}">
    <meta property="og:image" content="{{ $product->getMetaImage() }}">
    <meta property="og:url" content="{{ $url }}">
    <meta property="og:site_name" content="{{ env('APP_NAME') }}">
    <meta property="fb:admins" content="814789562597194">
    <meta property="og:type" content="website">
@endsection
@section('content')
    <section class="cg-space-fixer"></section>
    <section id="cg-product-head-wrap" class="cg-product-page-wrap">
        <div class="container">
            <div class="cg-container">
                <div class="row cg-product-info-tray d-flex justify-content-between align-items-center">

                    <div class="cg-pp-header-contents d-flex align-items-center">
                        <div class="cg-product-root-wrap mr-auto">
                            <h5><a class="grey f14 font1 w700" href="{{ url('/all-items') }}">All item  </a> <span class="theme w700 f10"> / </span> <a class="grey f14 font1 w700" href="{{ url($product->product_sub_category->product_sub_category_url) }}"> {{ $product->product_sub_category->name }}</a></h5>
                            <h2 class="cg-product-name second font1 w700">{{ $product->name }}</h2>
                            <div class="cg-store-in-category-detail-page">
                                <a href="{{ url($product->author->profile_url) }}">
                                    <h6 class="cg-account-name f14 light-grey pad-left-mdm pad-btm-mdm">{{ $product->author->user_profile->store_name }}</h6>
                                </a>
                                <a>
                                    <h6 class="cg-account-name f14 light-grey pad-left-mdm pad-btm-mdm">in </h6>
                                </a>

                                <a href="{{ url($product->product_sub_category->product_sub_category_url) }}">
                                    <h6 class="cg-account-name f14 light-grey pad-left-mdm pad-btm-mdm">{{ $product->product_sub_category->name }}</h6>
                                </a>
                            </div>
                        </div>
                        <div class="cg-extra-icon-block">
                            <ul class="cg-product-extras-wrap d-flex justify-content-center align-items-center">
                                @guest
                                    <li class="cg-product-love-wrap cg-extra-ico d-flex justify-content-center align-items-center">
                                        <a href="{{ url($product->product_url.'/favorite') }}" id="favorite_button" >
                                            <svg class="cg-product-love-ico feather feather-heart" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
                                        </a>
                                    </li>
                                @else
                                    @if(!$product->is_my_product)
                                        <li class="cg-product-love-wrap cg-extra-ico d-flex justify-content-center align-items-center">
                                            <a href="#" id="favorite_button" class="action-favorite" data-product_id="{{ $product->id }}">
                                                <svg id="favorite_icon" class="feather feather-heart cg-product-love-ico @if($product->isFavoritedBy(Auth::user())) favourited @else second @endif " xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
                                            </a>
                                        </li>
                                    @endif
                                @endguest
                                <li class="cg-product-share-wrap cg-extra-ico d-flex justify-content-center align-items-center">
                                    <button type="button" data-toggle="modal" data-target="#cg-shareModal">
                                        <svg class="cg-product-share-ico feather feather-share" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg>
                                    </button>
                                </li>
                                @if($product->is_downloaded)
                                    <li class="cg-product-share-wrap cg-extra-ico d-flex justify-content-center align-items-center">
                                        <a  href="{{ url(Auth::user()->user_url.'/download-my-item?i='.$product->id.'&j='.$fake_id) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="product-redwnload-ico feather feather-download"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg>
                                        </a>
                                    </li>
                                @endif
                                <li>
                                    <div class="cg-price-bubble"><span class="cg-product-price font1 w700 first"> @if($product->price) ${{ $product->price }} @else Free @endif</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="cg-shareModal modal fade" id="cg-shareModal" role="dialog">
                        <div class="modal-dialog d-flex align-items-center justify-content-center">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="modal-body d-flex align-items-center justify-content-center">
                                    <div class="cg-modal-main-content pad-btm-sml text-center">
                                        <h5 class="font1 second f24 w600">Share Now</h5>
                                        <p class="font1 dull-white f14 w500">Share this Fontfolio product: {{ $product->name }}</p>
                                        <div class="cg-share-modal-icons">
                                            <ul class="d-flex justify-content-center align-items-center">
                                                <li class="d-flex justify-content-center align-items-center">
                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $url }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>
                                                    </a>
                                                </li>
                                                <li class="d-flex justify-content-center align-items-center">
                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text={{ $product->getMetaDescription() }}&amp;url={{ $url }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>
                                                    </a>
                                                </li>
                                                <li class="d-flex justify-content-center align-items-center">
                                                    <a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?url={{ $url }}&is_video=false&description{{ $product->getMetaDescription() }}=&media={{ $product->getMetaImage() }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><path class="st0" d="M14.5,0.9c-2.1-1.1-4.4-1.1-6.6-0.5C4,1.4,1.7,5.2,2.5,9c0.3,1.4,0.9,2.6,2.2,3.4c0.5,0.3,0.6,0.3,0.7-0.3
                          		c0.1-0.3,0.2-0.7,0.2-1c0-0.2,0-0.5-0.1-0.6c-1.4-2.2-1-5,0.9-6.8c2.5-2.3,6.9-1.7,8.5,1c1.2,2.1,0.8,5.6-0.9,7.4
                          		c-0.5,0.5-1.1,0.9-1.8,1c-1.8,0.4-2.9-1-2.4-2.5c0.2-0.6,0.4-1.2,0.5-1.8c0.2-0.7,0.4-1.4,0.4-2c0.1-0.9-0.3-1.5-0.9-1.8
                          		c-0.7-0.3-1.5-0.1-2,0.5c-1,1.2-1,2.5-0.6,3.9c0.1,0.3,0.1,0.6,0,0.8C7,11.2,6.8,12.1,6.6,13c-0.5,2.1-1.1,4.2-0.8,6.4
                          		c0.1,0.2,0.3,0.9,1,0.3c1.3-1.8,1.7-3.9,2.2-6c0.7,0.8,1.4,1.2,2.4,1.4c0.9,0.1,1.9,0,2.7-0.4c2.4-1,3.6-3,4.1-5.4
                          		C19.1,5.7,17.6,2.5,14.5,0.9z"/></svg>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Slider Section Starts-->
    <section class="cg-product-slider-wrap">
        <div class="product-image-slider">
            <div class="slide-thumb" ><img class="product-thumb img-fluid" src="{{ $product->cover_photo_original_path }}" alt="{{ $product->name }}"></div>
            @foreach($product->preview_photos as $preview_photo)
                <div class="slide-thumb"><img class="product-thumb img-fluid" src="{{ $preview_photo->preview_photo_path  }}" alt="{{ $product->name }}"></div>
            @endforeach
        </div>
    </section>
    <!-- Product Slider Section Ends-->
    <section id="product-page-details" class="product-page-details container">
        <!-- <div class="container"> -->
        <div class="cg-container">
            <div class="row">
                <div class="col-md-9 cg-product-details-resizer">
                    <div class="cg-product-details-wrap">
                        <div class="pp-alternate-button-wrap">
                            <div class="multi-pourpose-buttons d-flex align-items-center justify-content-start">
                                @if($product->external_url)
                                    <div class="cg-live-preview-button d-flex align-items-center justify-content-between">
                                        <a href="{{ $product->external_url }}" rel="nofollow" target="_blank">Live Preview</a>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="ml-auto feather feather-layout"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="3" y1="9" x2="21" y2="9"></line><line x1="9" y1="21" x2="9" y2="9"></line></svg>
                                    </div>
                                @endif
                                @foreach($product->product_sub_category->admin_properties()->default()->get() as $default_property)
                                    <div class="alt-support-btn d-flex align-items-center justify-content-between">
                                        <a>{{ $default_property->name }}</a>
                                        {!! $default_property->svg !!}
                                    </div>
                                @endforeach
                                @foreach($product->properties as $property)
                                    <div class="alt-support-btn d-flex align-items-center justify-content-between">
                                        <a>{{ $property->name }}</a>
                                        {!! $property->svg !!}
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        @php
                            if($product->product_category->extract_status==1 && $product->font_previews->count())
                            {
                        @endphp
                        <div class="cg-font-preview-input">
                            <input type="text" name="font_text" value="Two driven jocks help fax my big quiz." class="font1 cg-product-font-custom-preview-box second add-btm-xsml">
                            <div class="ff-font-pr-btn-size-wrap">

                                <select id="font_size_select" name="font_size" class="cg-font-preview-size font-preview-size-select">
                                    <option value="42.5%">14px</option>
                                    <option value="62.50%">20px</option>
                                    <option value="125%" selected>40px</option>
                                    <option value="175%">60px</option>
                                    <option value="235%">80px</option>
                                </select>
                                <button onclick="changeFontText()" class="font_preview_btn">Preview</button>
                            </div>
                        </div>
                        @php
                            echo '<div class="font_preview_section" id="font_preview_section">';
                          foreach($product->font_previews as $key=>$font_preview)
                          {
                              echo '<img src="/img/'.$font_preview->id.'" class="font_preview_img">';
                              echo '</br>';
                          }
                            echo '</div>';
                      }
                        @endphp
                        {{--     @if($product->product_category->extract_status==1 && $product->font_previews->count())--}}
                        @if(false)
                        <!-- Font Preview -->
                            <div class="cg-product-font-preview-wrap">
                                <h2 class="font1 f16 black w700 cg-section-sml-heading">Font Preview</h2>
                                <div class="cg-product-font-preview-area">
                                    <div id="app"></div>
                                </div>
                            </div>
                            <!-- Font Preview -->
                        @endif
                        <div class="cg-product-desc-wrap">
                            <h2 class="font1 f16 black w700 cg-section-sml-heading">Description</h2>
                            <p class="font1 grey w300 f14 add-top-sml pad-btm-sml">{!! $product->description !!} </p>
                        </div>
                        @if($product->product_tags)
                            <div class="cg-product-desc-wrap">
                                <h2 class="font1 f16 black w700 cg-section-sml-heading">Product Tags</h2>
                                <div class="cg-product-tag">
                                    <ul class="cg-product-tag-items">
                                        @php $tag_array = explode(",", $product->product_tags) @endphp
                                        @foreach($tag_array as $tag)
                                            <li>{{ $tag }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="cg-product-buy-wrap-resizer col-md-3">
                    <div class="cg-product-buy-wrap d-flex flex-column">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="cg-primary-dp">
                                <img src="{{ $product->author->user_profile->image_path }}" class="img-fluid" alt="{{ $product->author->user_profile->store_name }}">
                            </div>
                            <div class="cg-pp-store-name">
                                <h6 class="cg-account-name font1 w700 f14 second">
                                    <a href="{{ url($product->author->profile_url) }}">
                                        {{ $product->author->user_profile->store_name }}
                                    </a>
                                </h6>
                            </div>
                            <div class="ml-auto">
                                <h3 class="cg-product-price theme font1 w700 f40 text-right"><span>@if($product->price) ${{ $product->price }} @else Free @endif</span></h3>
                            </div>
                        </div>
                        <div>
                            <h4 class="cg-product-name second font1 f18 w700">{{ $product->name }}</h4>
                        </div>
                        <div class="cg-product-star-rating">
                            <svg class="f13 feather feather-star @if($product->total_average_rating >=1) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                            <svg class="f13 feather feather-star @if($product->total_average_rating >=2) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                            <svg class="f13 feather feather-star @if($product->total_average_rating >=3) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                            <svg class="f13 feather feather-star @if($product->total_average_rating >=4) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                            <svg class="f13 feather feather-star @if($product->total_average_rating >=5) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                        </div>
                        <!-- Desktop buttons -->
                        <div class="cg-product-buy-btn-wrap">
                            @if($product->is_my_product)
                                <a href="{{ url($product->product_url.'/edit-product') }}" class="cg-buy-btn"  >Edit</a>
                                <a href="{{ url(Auth::user()->user_url.'/download-my-own-item?i='.$product->id.'&j='.$fake_id) }}" target="_blank" class="cg-cart-btn"  >Download</a>
                            @else
                                @if($product->is_downloaded)
                                    <a href="{{ url(Auth::user()->user_url.'/download-my-item?i='.$product->id.'&j='.$fake_id) }}" class="cg-buy-btn"  >Download</a>
                                @else
                                    @if(Auth::check())
                                        <button onclick="buyNow({{ $product->id }})" class="cg-buy-btn"  >Buy Now</button>
                                    @else
                                        <a href="{{ url($url.'/favorite') }}"  class="cg-buy-btn"  >Buy Now</a>
                                    @endif
                                @endif
                                <button class="cg-cart-btn" id="cart_button_1" onclick="addToCart({{ $product->id }})" type="button" @if($product->is_carted || $product->is_my_product || $product->is_downloaded) disabled @endif  name="cart">@if($product->is_downloaded) Already Purchased @elseif($product->is_carted)Added to cart @else Add to Cart @endif</button>
                            @endif
                        </div>
                        <!-- Desktop buttons -->

                        <!-- Product License Type -->
                        <div class="cg-product-license-wrap">
                            {!! Form::open(['method' => 'POST', 'url' => $product->product_url.'/buy-now','id' => 'buyNowForm']) !!}
                            <div class="d-flex justify-content-between align-items-end cg-product-license-head cg-section-sml-heading">
                                <label for="freebies_status" class="">License Type</label>
                                <a href="{{ url('/license') }}" target="_blank">More info</a>
                            </div>
                            <div class="license-wrap d-flex justify-content-between flex-column">
                                @if($product->product_license)
                                    <div class="cg-license-checkbox d-flex align-items-center">
                                        <li class="d-flex align-items-center"><label for="license_1" class="cg-radio-btn"> {{ env('LICENSE_1') }}
                                                <input type="radio" name="license" id="license_1" value="LICENSE_1"> <span class="checkmark"></span></label>
                                        </li>
                                        <li class="reg_license_price ml-auto"><span>${{ $product->price * env('LICENSE_1_VALUE')}}</span></li>
                                    </div>

                                    <div class="cg-license-checkbox d-flex align-items-center">
                                        <li class="d-flex align-items-center"><label for="license_2" class="cg-radio-btn"> {{ env('LICENSE_2') }}
                                                <input type="radio" name="license" id="license_2" value="LICENSE_2"> <span class="checkmark"></span></label>
                                        </li>
                                        <li class="com_license_price ml-auto"><span>${{ $product->price * env('LICENSE_2_VALUE')}}</span></li>
                                    </div>

                                    <div class="cg-license-checkbox d-flex align-items-center">
                                        <li class="d-flex align-items-center"><label for="license_3" class="cg-radio-btn"> {{ env('LICENSE_3') }}
                                                <input type="radio" name="license" id="license_3" value="LICENSE_3"> <span class="checkmark"></span></label>
                                        </li>
                                        <li class="reg_license_price ml-auto"><span>${{ $product->price * env('LICENSE_3_VALUE')}}</span></li>
                                    </div>
                                @else
                                    <div class="cg-license-checkbox d-flex align-items-center">
                                        <li class="d-flex align-items-center"><label for="license_0" class="cg-radio-btn"> {{ env('LICENSE_0') }}
                                                <input type="radio" name="license" id="license_0" value="LICENSE_0"> <span class="checkmark"></span></label>
                                        </li>
                                        <li class="reg_license_price ml-auto"><span>${{ $product->price * env('LICENSE_0_VALUE')}}</span></li>
                                    </div>
                                @endif
                            </div>
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            {!! Form::close() !!}
                        </div>
                        <!-- Product License Type Ends-->
                        <label for="freebies_status" class="cg-section-sml-heading">Product Details</label>
                        <ul class="no-padding cg-product-spec-lines">

                            @if($product->product_sub_category->name)
                                <div class="cg-spec-line d-flex align-items-center justify-content-between">
                                    <h5 class="w700 font1 grey f13">Category</h5>
                                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->product_sub_category->name }}</span>
                                </div>
                            @endif

                            @if($product->tag_line)
                                <div class="cg-spec-line cg-tagline-opt d-flex align-items-center justify-content-between">
                                    <h5 class="w700 font1 grey f13">Tagline</h5>
                                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->tag_line }}</span>
                                </div>
                            @endif

                            @if($product->file_size)
                                <div class="cg-spec-line d-flex align-items-center justify-content-between">
                                    <h5 class="w700 font1 grey f13">File Size</h5>
                                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->file_size }}</span>
                                </div>
                            @endif

                            @if($product->date_added)
                                <div class="cg-spec-line d-flex align-items-center justify-content-between">
                                    <h5 class="w700 font1 grey f13">Date Added</h5>
                                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->date_added }}</span>
                                </div>
                            @endif

                            @if($product->date_updated)
                                <div class="cg-spec-line d-flex align-items-center justify-content-between">
                                    <h5 class="w700 font1 grey f13">Date Updated</h5>
                                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->date_updated }}</span>
                                </div>
                            @endif
                        </ul>

                        @if($product->file_types()->count() || $product->product_sub_category->admin_file_types()->default()->count())
                            <div class="file-supports cg-spec-line">
                                <h2 class="app-support-heading">File Types</h2>
                                <ul class="cg-product-categories grey">
                                    @foreach($product->product_sub_category->admin_file_types()->default()->get() as $default_file_type)
                                        <li>
                                            {{ $default_file_type->name }}
                                        </li>
                                    @endforeach
                                    @foreach($product->file_types as $file_type)
                                        <li> {{ $file_type->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if($product->application_supports->count() || $product->product_sub_category->admin_application_supports()->default()->count())
                            <div class="app-supports">
                                <h2 class="app-support-heading">Compatiblity</h2>
                                <ul class="d-flex align-items-center justify-content-start flex-wrap">
                                    @foreach($product->product_sub_category->admin_application_supports()->default()->get() as $default_application_support)
                                        <li data-toggle="tooltip" title="{{ $default_application_support->name }}" data-placement="top">
                                            {!! $default_application_support->svg !!}
                                        </li>
                                    @endforeach
                                    @foreach($product->application_supports as $application_support)
                                        <li data-toggle="tooltip" title="{{ $application_support->name }}" data-placement="top">
                                            {!! $application_support->svg !!}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="cg-share-earn">
                            <h2 class="font1 black f16 w700 cg-section-sml-heading"> @if($product->product_affiliate_url) Share & Earn @else Share @endif</h2>
                            <div class="cg-product-link-holder">
                                <div class="opt-spacer"></div>
                                @if($product->product_affiliate_url)
                                    <textarea id="share-textarea" data-toggle="tooltip" title="Click to Copy" data-placement="top" class="font1 grey f14 w300 add-top-xsml" spellcheck="false" onclick="copy()">{{ url($product->product_affiliate_url) }}</textarea>
                                @else
                                    <textarea id="share-textarea" data-toggle="tooltip" title="Click to Copy" data-placement="top" class="font1 grey f14 w300 add-top-xsml" spellcheck="false" onclick="copy()">{{ url($product->product_url) }}</textarea>
                                @endif
                            </div>
                        </div>
                        @if(Auth::check() && !$product->is_my_product)
                            <a data-toggle="collapse" href="#collapseExample"  role="button" aria-expanded="false" aria-controls="collapseExample">
                                <div class="cg-contact-seller d-flex align-items-center justify-content-between">
                                    Contact Author
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-feather"><path d="M20.24 12.24a6 6 0 0 0-8.49-8.49L5 10.5V19h8.5z"></path><line x1="16" y1="8" x2="2" y2="22"></line><line x1="17.5" y1="15" x2="9" y2="15"></line></svg>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- mobile buttons -->
    <div class="cg-product-buy-btn-wrap-mob pad-top-xl pad-btm-xl">
        @if($product->is_my_product)
            <a href="{{ url(Auth::user()->user_url.'/download-my-own-item?i='.$product->id.'&j='.$fake_id) }}" target="_blank" class="cg-cart-btn theme font1 add-btm-sml"  >Download</a>
            <a href="{{ url($product->product_url.'/edit-product') }}" class="cg-buy-btn theme-bg first font1"  >Edit</a>
        @else
            <button class="cg-cart-btn theme font1 add-btm-sml" id="cart_button_2" onclick="addToCart({{ $product->id }})" type="button" @if($product->is_carted || $product->is_downloaded) disabled @endif  name="cart">@if($product->is_downloaded) Already Purchased @elseif($product->is_carted)Added to cart @else Add to Cart @endif </button>
            @if($product->is_downloaded)
                <a href="{{ url(Auth::user()->user_url.'/download-my-item?i='.$product->id.'&j='.$fake_id) }}" target="_blank" class="cg-buy-btn theme-bg first font1"  >Download</a>
            @else
                @if(Auth::check())
                <button  onclick="buyNow({{ $product->id }})" class="cg-buy-btn theme-bg first font1"  >Buy Now</button>
                    @else
                    <a href="{{ url($url.'/favorite') }}"  class="cg-buy-btn theme-bg first font1"  >Buy Now</a>
                @endif
            @endif
        @endif
    </div>
    <!-- Mobile buttons -->
    @if($similar_products->count())
        <div id="similar-products">
            <similar-products similar_products="{{ $similar_products}}" ></similar-products>
        </div>
    @endif
    <div id="toast_wrap" class="cg-toast-one toast_display_none">
        <ul class="d-flex justify-content-between">
            <li class="first"><p id="toast_message"></p></li>
            <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
        </ul>
    </div>

    @if(Auth::check())
        <div class="message-pop-wrap collapse" id="collapseExample">
            <div class="cg-uni-popup text-center">
                <div class="close-popup-wrap d-flex justify-content-end">
                    <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="fas fa-times"></a>
                </div>
                <div class="chat-text-block ">
                    <h3 class="font1 cg-section-sml-heading w700 text-left">Say hello to,<br>{{ $product->author->account_short_user_name }}</h3>
                    <div class="pop-thumb">
                        <img src="{{ $product->author->user_profile->image_path }}" class="img-fluid" alt="{{ $product->author->account_short_user_name }}">
                    </div>
                </div>
                <div class="popup-box-form-wrap">
                    {!! Form::open(array('id'=>'message_form')) !!}
                    <input type="hidden" name="receiver_id" id="receiver_id" value="{{ $product->author->id }}">
                    <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                        <div class="cg-top-input">
                            <label class="message-col" for="subject" >Subject</label>
                            <select name="subject" id="subject" required>
                                <option value="">Select Subject</option>
                                <option value="pre-sale">Pre Sale</option>
                                <option value="support">Support</option>
                                <option value="refund">Refund</option>
                            </select>
                        </div>
                    </div>
                    <div id="dynamic_section">
                    </div>
                    <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                        <div class="cg-top-input">
                            <label class="message-col" for="name">Your Message</label>
                            <textarea type="text" id="name" name="message_text" required placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center">
                        <input type="submit" value="Send Message" name="submit" id="submit" />
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif
@endsection
@push('footer_scripts')
    <script>
        var product_url = "{{ $product->product_url }}";
    </script>
    @if($similar_products->count())
        <script src="{{ asset('js/productinfo.js') }}"></script>
    @endif
    <script src="{{ asset('../javascripts/custom/product-detail.js') }}"></script>


    <script>
        var product_id = "{{ $product->id }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function addToCart(product_id)
        {

            var license = document.querySelector('input[name = "license"]:checked');

            if(license != null){
                $.ajax({
                    type:'POST',
                    url:'/add_to_cart',
                    data:{product_id:product_id,license:license.value},
                    success:function(data){
                        console.log(data.success);
                        if(data.status == 'success')
                        {
                            var span = document.createElement("SPAN");
                            document.getElementById('cart').appendChild(span);
                            document.getElementById("toast_wrap").classList.remove("toast-killed");
                            document.getElementById("toast_wrap").classList.remove("toast_display_none");
                            document.getElementById('toast_message').innerHTML=data.message;
                            document.getElementById('cart_button_1').innerHTML="Added To Cart";
                            document.getElementById('cart_button_2').innerHTML="Added To Cart";
                            document.getElementById('cart_button_1').disabled=true
                            document.getElementById('cart_button_2').disabled=true
                        }
                        else
                        {
                            document.getElementById("toast_wrap").classList.remove("toast-killed");
                            document.getElementById("toast_wrap").classList.remove("toast_display_none");
                            document.getElementById('toast_message').innerHTML=data.message;


                        }
                    }
                });
            } else {
                document.getElementById("toast_wrap").classList.remove("toast-killed");
                document.getElementById("toast_wrap").classList.remove("toast_display_none");
                document.getElementById('toast_message').innerHTML="Select License";
            }

        }

        function buyNow(product_id)
        {

            var license = document.querySelector('input[name = "license"]:checked');

            if(license != null){
                document.getElementById("buyNowForm").submit();
            } else {
                document.getElementById("toast_wrap").classList.remove("toast-killed");
                document.getElementById("toast_wrap").classList.remove("toast_display_none");
                document.getElementById('toast_message').innerHTML="Select License";
            }

        }

        $('.action-favorite').click(function(){
            document.getElementById("toast_wrap").classList.remove("toast-killed");
            var product_id = $(this).data('product_id');
            $.ajax({
                type:'POST',
                url:'/favorite',
                data:{product_id:product_id},
                success:function(data){

                    console.log(data.success);
                    if(!data.success)
                    {
                        //not favorite
                        document.getElementById("favorite_icon").classList.add("second")
                        document.getElementById("favorite_icon").classList.remove("favourited")
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML=data.message;
                    }
                    else
                    {
                        document.getElementById("favorite_icon").classList.add("favourited")
                        document.getElementById("favorite_icon").classList.remove("second")
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML=data.message;

                        // favourites
                    }
                }
            });
        });


        $("form#message_form").submit(function(event) {
            event.preventDefault();
            var subject = $("select[name='subject']").val();
            if(subject == "support"){
                var product = $("select[name='product']").val();
                var reason = "";
            }
            else if(subject == "refund")
            {
                var product = $("select[name='product']").val();
                var reason = $("select[name='reason']").val();
            }
            else
            {
                var product = "";
                var reason = "";
            }
            var receiver_id = $("input[name='receiver_id']").val();
            var message_text = $("textarea[name='message_text']").val();
            if (message_text)
            {

                $.ajax({
                    type: 'POST',
                    url: '/sent_message',
                    data: { receiver_id: receiver_id, message_text:message_text, subject:subject, product:product, reason:reason },
                    success: function (data) {
                        console.log(data.success);
                        if (data.status == 'success') {
                            message_success = true;
                            window.location = data.inbox_url;

                        } else {
                            message_error = true;

                        }
                    }
                });
            }
        });

        $('#subject').on('change',function(e)
        {
            var receiver_id = $("input[name='receiver_id']").val();
            var subject = $("select[name='subject']").val();
            if (subject)
            {
                $.ajax({
                    type: 'POST',
                    url: '/message_subject',
                    data: { receiver_id: receiver_id, subject:subject },
                    success: function (data) {
                        document.getElementById("dynamic_section").innerHTML=data.options;
                    }
                });

            }

        });


        function changeFontText()
        {
            document.getElementById("toast_wrap").classList.remove("toast-killed");
            var product_id = "{{ $product->id }}";
            var font_text = $("input[name='font_text']").val();
            $.ajax({
                type:'POST',
                url:'/change-font-preview-text',
                data:{product_id:product_id, font_text:font_text},
                success:function(data){
                    console.log(data.success);
                    if(!data.success)
                    {
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML="Oops!... Try again later";
                    }
                    else
                    {
                        document.getElementById("font_preview_section").innerHTML=data.options;
                        var font_size = $("select[name='font_size']").val();
                        var images = document.getElementsByClassName('font_preview_img');
                        for(var i = 0; i < images.length; i++) {
                            images[i].style.width = font_size;
                        }


                    }
                }
            });
        }
        $('#font_size_select').on('change',function(e)
        {
            var font_size = $("select[name='font_size']").val();
            var images = document.getElementsByClassName('font_preview_img');
            for(var i = 0; i < images.length; i++) {
                images[i].style.width = font_size;
            }
        });

    </script>
@endpush
