<div class="row d-flex add-btm-xsml d-flex justify-content-center">
<div class="cg-top-input">
    <label class="message-col" for="product">Choose Product</label>
    <select name="product" id="product" required>
        @if(count($downloads))
            <option value="">Select Product</option>
            @foreach($downloads as $download)
                <option value="{{ $download->product->id }}">{{ $download->product->name }}</option>
            @endforeach
        @else
            <option value="">No Products</option>
        @endif
    </select>
</div>
</div>
