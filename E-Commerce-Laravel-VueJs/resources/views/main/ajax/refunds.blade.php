<div class="row d-flex add-btm-xsml d-flex justify-content-center">
<div class="cg-top-input">
    <label class="message-col" for="product">Choose Product</label>
    <select name="product" id="product" required>
        @if(count($refunds))
            <option value="">Select Product</option>
            @foreach($refunds as $refund)
                <option value="{{ $refund->product->id }}">{{ $refund->product->name }}</option>
            @endforeach
        @else
            <option value="">No Products</option>
        @endif
    </select>
</div>
</div>

<div class="row d-flex add-btm-xsml d-flex justify-content-center">
    <div class="cg-top-input">
        <label class="message-col" for="reason">Reason</label>
        <select name="reason" id="reason" required>
                <option value="">Select Reason</option>
               <option value="Compatibility or usability issues">Compatibility or usability issues</option>
              <option value="Damaged or incorrect product files">Damaged or incorrect product files</option>
              <option value="Incorrect product description">Incorrect product description</option>
        </select>
    </div>
</div>
