@extends('layouts.app', ['page_class' => 'user-dash-fav user-dashboard'])
@if(request()->segment(count(request()->segments())) != 'settings') @section('title', 'Welcome to '.env('APP_NAME')) @else @section('title', ' Account Setup') @endif
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
<div class="cg-profile-over-view-wrap"></div>
        <div id="dashboard">

        <div class="container">
            <div class="col-md-12">
                <div class="cg-profile-inner-nav-wrap col-md-12 d-flex justify-content-between align-items-center">
                    <ul class="cg-user-buttons">
                        @guest
                        {{--  guest--}}
                            <li> <a href="{{ url($user->profile_url.'/message') }}" class="cg-user-message theme-bg">Message</a> </li>
                            <li><a href="{{ url($user->profile_url.'/follow') }}">  Follow</a></li>

                        @else
                            {{-- not guest--}}
                            @if(Auth::user()->is_author && $user->is_it_me && !$profile_page)
                               <li class="cg-dash-cp-button">
                                    <a href="{{ url($user->user_url.'/create-product') }}" class="theme-bg">Create a Product</a>
                                </li>

                                    <li class="cg-dash-ep-button">
                                        @if(request()->segment(count(request()->segments())) != 'settings')
                                            <a href="#" v-if="settings" id="set_pass_pop_up">Change Password</a>
                                            <a  v-if="!settings" href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                        @else
                                            <a href="#" v-if="settings" id="set_pass_pop_up">Change Password</a>
                                            <a  v-if="!settings" href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                        @endif
                                    </li>
                                @elseif(Auth::user()->is_author && $user->is_it_me && $profile_page)
                                    <li class="cg-dash-cp-button">
                                        <a href="{{ url($user->user_url.'/create-product') }}" class="theme-bg">Create a Product</a>
                                    </li>
                                    <li class="cg-dash-ep-button">
                                            <a  href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                    </li>
                                @elseif(Auth::user()->is_user && $user->is_it_me && !$profile_page)
                                    <li class="cg-dash-bs-button">
                                        <a href="{{ url('/welcome-shop') }}" class="theme-bg">Become a Seller</a>
                                    </li>
                                    <li class="cg-dash-ep-button">
                                        @if(request()->segment(count(request()->segments())) != 'settings')
                                            <a  v-if="!settings" href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                            <a href="#" v-if="settings" id="set_pass_pop_up">Change Password</a>
                                        @else
                                            <a href="#" v-if="settings" id="set_pass_pop_up">Change Password</a>
                                            <a  v-if="!settings" href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                        @endif
                                    </li>
                            @elseif(Auth::user()->is_user && $user->is_it_me && $profile_page)
                                <li class="cg-dash-bs-button">
                                    <a href="{{ url('/welcome-shop') }}" class="theme-bg">Become a Seller</a>
                                </li>
                                <li class="cg-dash-ep-button">
                                        <a  v-if="!settings" href="{{ url($user->user_url.'/dashboard/settings') }}">Edit Profile</a>
                                </li>
                             @else
                                <li class="cg-dash-ms-button"> <a data-toggle="collapse" href="#collapseExample" class="theme-bg" role="button" aria-expanded="false" aria-controls="collapseExample">Message</a> </li>
                                <li class="cg-dash-fw-button">
                                    <a href="#" id="follow_button" class="action-follow" data-id="{{ $user->id }}">
                                        @if(auth()->user()->isFollowing($user))
                                            UnFollow
                                        @else
                                            Follow
                                        @endif
                                    </a>
                                </li>
                                @endif
                        @endguest
                    </ul>
                    <div class="cg-profile-info">
                        <div class="cg-primary-dp-wrap">
                            <div class="cg-primary-dp">
                                <img class="img-fluid" src="{{ $user->user_profile->image_path }}" alt="{{ $user->name }}" data-no-retina>
                        @if(Auth::check() && Auth::user()->is_it_me && $user->is_it_me && request()->segment(count(request()->segments())) == "settings")
                        <div class="dpupwrap" v-if="settings">
                        {!! Form::open(array('class'=>'dpupform','url' => [$user->user_url.'/profile_pic'],'method'=>'PUT','files' => true)) !!}

                        @if($errors->has('profile_photo'))
                            <span  class="text-danger">{{ $errors->first('profile_photo') }}</span>
                        @endif
                        <div class="dpupload" data-toggle="tooltip" title="Aspect Ratio: 1:1" data-placement="top">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-camera dpup"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                          <input type="file"  accept="image/*" onchange="form.submit()" name="profile_photo">
                        </div>
                        {!! Form::close() !!}
                        </div>
                        @endif
                            </div>
                        </div>
                        <h3 class="font1 cg-account-name w700">@if($user->is_author){{ $user->user_profile->store_name }} @else {{ $user->name }} @endif </h3>
                        <h4 class="font1 cg-account-website">{{ $user->email }}</h4>
                    </div>
                @if($profile_page_status)
                    <div class="cg-profile-inner-nav">
                        <ul class="font1 black cg-profile-inner-menu">
                            <li><a>Products<span>{{ $user->products()->valid()->demo()->enable()->count() }}</span></a></li>
                            <li><a>Following<span>{{ $user->followings()->get()->count() }}</span></a></li>
                            <li><a>Followers<span id="followers_count">{{ $user->followers()->get()->count() }}</span></a></li>
                        </ul>
                    </div>
                            @else
                            <div class="col-md-10 cg-profile-inner-nav">
                     <ul class="font1 black cg-profile-inner-menu">
                                @if($user->is_author)
                                    <li><router-link :to="{ name: 'my-products' }"  active-class = "active w700" >My Items</router-link></li>
                                    <li><router-link :to="{ name: 'my-sales' }" active-class = "active w700">Sales</router-link></li>
                                @endif
                                <li> <router-link :to="{ name: 'downloads' }" active-class = "active w700">Downloads</router-link></li>
                                <li><router-link :to="{ name: 'favourites' }" active-class = "active w700">Favorites</router-link></li>

                            </ul>

                    </div>
                        @endif
                </div>
                @if($profile_page_status)
                            <div class="cg-seller-profile-bio font1">
                                <h3 class="cg-border-top">
                                    {{ $user->user_profile->description }}
                                </h3>
                            </div>
                            @else
                        @endif
            </div>

            @if(!$profile_page_status)
              <transition>
                    <router-view user="{{ $user }}" message="@if(session('message')) {{ session('message') }} @endif" countries="{{ $countries }}"></router-view>
                </transition>
            @endif
        </div>

            @if($profile_page_status && $user->products()->valid()->demo()->count())
                <div id="profile">
                    <transition>
                        <router-view user="{{ $user }}"></router-view>
                    </transition>
                </div>
            @endif

</div>
@if(Auth::check())
<div class="message-pop-wrap collapse" id="collapseExample">
  <div class="cg-uni-popup text-center">
    <div class="close-popup-wrap d-flex justify-content-end">
      <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="fas fa-times"></a>
    </div>
    <div class="chat-text-block ">
      <h3 class="font1 cg-section-sml-heading w700 text-left">Say hello to,<br>{{ $user->name }}</h3>
      <div class="pop-thumb">
        <img src="{{ $user->user_profile->image_path }}" class="img-fluid" alt="{{ $user->name }}">
      </div>
    </div>
    <div class="popup-box-form-wrap">
        {!! Form::open(array('id'=>'message_form')) !!}
        <input type="hidden" name="receiver_id" id="receiver_id" value="{{ $user->id }}">
        <div class="row d-flex add-btm-xsml d-flex justify-content-center">
            <div class="cg-top-input">
                <label class="message-col" for="subject" >Subject</label>
                <select name="subject" id="subject" required>
                    <option value="">Select Subject</option>
                    <option value="pre-sale">Pre Sale</option>
                    <option value="support">Support</option>
                    <option value="refund">Refund</option>
                </select>
            </div>
        </div>
        <div id="dynamic_section">
        </div>
      <div class="row d-flex add-btm-xsml d-flex justify-content-center">
        <div class="cg-top-input">
            <label class="message-col" for="name">Your Message</label>
            <textarea type="text" id="name" name="message_text" required placeholder=""></textarea>
        </div>
      </div>

      <div class="row d-flex justify-content-center">
        <input type="submit" value="Send Message" name="submit" id="submit"  />
      </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>
 @endif

    <div id="toast_wrap" class="cg-toast-one toast_display_none">
        <ul class="d-flex justify-content-between">
            <li class="first"><p id="toast_message"></p></li>
            <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
        </ul>
    </div>

    @if (session('success'))
        <div class="cg-toast-one">
            <ul class="d-flex justify-content-between">
                <li class="first"><p>{{ Session('success') }}</p></li>
                <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
            </ul>
        </div>
    @endif
    @if (session('error'))
        <div class="cg-toast-one">
            <ul class="d-flex justify-content-between">
                <li class="first"><p>{{ Session('error') }}</p></li>
                <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
            </ul>
        </div>
    @endif

@endsection


@push('footer_scripts')
<script>
    var message_success = false;
    var message_error = false
    var user_url = "{{ $user->user_url }}";
    var profile_url = "{{ $user->profile_url }}";
  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.action-follow').click(function(){
            var user_id = $(this).data('id');
            var count = document.getElementById("followers_count").innerHTML;
            document.getElementById("toast_wrap").classList.remove("toast-killed");
             $.ajax({
                type:'POST',
                url:'/follow',
                data:{user_id:user_id},
                success:function(data){

                    console.log(data.success);
                    if(!data.success)
                    {
                        document.getElementById("follow_button").innerHTML = "Follow";
                        document.getElementById("followers_count").innerHTML = parseInt(count)-1;
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML=data.message;
                    }
                    else
                        {
                        document.getElementById("follow_button").innerHTML = "UnFollow";
                        document.getElementById("followers_count").innerHTML = parseInt(count)+1;
                            document.getElementById("toast_wrap").classList.remove("toast_display_none");
                            document.getElementById('toast_message').innerHTML=data.message;
                    }
                }
            });
        });


    $("form#message_form").submit(function(event) {
        event.preventDefault();
        var subject = $("select[name='subject']").val();
        if(subject == "support"){
            var product = $("select[name='product']").val();
            var reason = "";
        }
        else if(subject == "refund")
        {
            var product = $("select[name='product']").val();
            var reason = $("select[name='reason']").val();
        }
        else
        {
            var product = "";
            var reason = "";
        }
        var receiver_id = $("input[name='receiver_id']").val();
        var message_text = $("textarea[name='message_text']").val();
        if (message_text)
        {

            $.ajax({
                type: 'POST',
                url: '/sent_message',
                data: { receiver_id: receiver_id, message_text:message_text, subject:subject, product:product, reason:reason },
                success: function (data) {
                    console.log(data.success);
                    if (data.status == 'success') {
                       message_success = true;
                       window.location = data.inbox_url;

                    } else {
                        message_error = true;

                    }
                }
            });
    }
    });

    $('#subject').on('change',function(e)
    {
        var receiver_id = $("input[name='receiver_id']").val();
        var subject = $("select[name='subject']").val();
        if (subject)
        {
            $.ajax({
                type: 'POST',
                url: '/message_subject',
                data: { receiver_id: receiver_id, subject:subject },
                success: function (data) {
                    document.getElementById("dynamic_section").innerHTML=data.options;
                }
            });

        }

    });
</script>

@if($profile_page_status)
    <script src="{{ asset('js/profile.js') }}"></script>
@else
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endif
@endpush
