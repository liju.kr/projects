@php
// $pre_class = "cg-precode-shop";
if($price_type)
{
    if($price_type =="free")
    {
    $free_class = " cg-free-shop";
    }
    else
    {
    $free_class = "";
    }
}
else
{
$free_class = "";
}
@endphp
@extends('layouts.app', ['page_class' => 'cg-shop-page'.$free_class])
@section('title', 'Store')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
<section class="precoded-welcome d-flex justify-content-center align-items-center text-center">
  <div class="precoded-welcome-block d-flex justify-content-center align-items-start text-left flex-column">
    <h2 class="themed-text">Precoded <br>Blocks</h2>
    <h5>Ready to use Pre-Coded Web elements</h5>
    <p>Beautiful Precoded web components and interactive Elements that you can easily use in any web projects at attractive prices.</p>
  </div>
  <div class="precoded-mega-banner">
    <div class="pc-image-poper">
      <img class="img-fluid" src="{{ asset('images/landing/precoded.png') }}" alt="{{ env('APP_NAME') }}">
    </div>
    <svg version="1.1" id="e2652d7b-d014-44e7-aeea-709bf92e6067"
    	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 853.3 818"
    	 style="enable-background:new 0 0 853.3 818;" xml:space="preserve">
    <path class="st0 precoded-shape-bg" d="M707.5,464.2C599.8,627.2,549.1,818,353.8,818S0,659.6,0,464.2s176.5-271.5,353.8-353.8
    	C929.4-156.6,948.5,99.5,707.5,464.2z"/>
    </svg>
  </div>
</section>
<section class="free-event-welcome d-flex justify-content-center align-items-center text-center flex-column">
  <div class="free-event-welcome-block d-flex justify-content-center align-items-center text-center flex-column">
    <div class="free-brand-banner-wrap">
      <div class="free-brand-banner"></div>
    </div>
    <p>A beautiful collection of free creative assets waiting for you, enjoy your freebies.</p>
    <div class="free-page-ico d-flex justify-content-center align-items-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg></div>
  </div>
</section>
      <div id="shop-products">
        <shop-products category_slug="{{ $category_slug }}" sub_category_slug="{{ $sub_category_slug }}" product_sub_categories="{{ $product_sub_categories }}" sub_category_id ="{{ $sub_category_id }}" product_categories ="{{ $product_categories }}" product_category ="{{ $product_category }}" key_word ="{{ $key_word }}" price_type ="{{ $price_type }}"></shop-products>
      </div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/shop.js') }}"></script>
@endpush
