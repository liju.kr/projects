@extends('layouts.app', ['page_class' => 'seller-application common-popup upload-dashboard font1'])
@section('title', 'Seller Application')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
 <div class="cg-profile-over-view-wrap"></div>
        <section class="cg-pop-panel-wrap">
            <div class="cg-pop-panel-body  d-flex justify-content-center">
              <div class="cg-semi-body-contents">
                <div class="float-panel-part text-center d-flex align-items-center justify-content-center">
                <h3 class="font1 cg-section-sml-heading w700">Create a Store</h3>
                </div>
                  @if($author_request_status == 1)
                      <!-- Approved -->
                      <div class="cg-404-content-wrap waiting-for-result application-approved text-center">
                          <div class="cg-404-img-wrap text-center">
                              <img src="{{asset('images/approve.svg')}}" class="img-fluid" alt="{{ env('APP_NAME') }}-404"/>
                          </div>
                          <h4 class="font1 grey w600">Congratulations, We have successfully approved your application.</h4>
                          <div class="">
                              <a href="{{ url($user->user_url.'/dashboard/my-products') }}" class="first">Go to Dashboard</a>
                          </div>
                      </div>
                  @elseif($author_request_status == 2)
                  <!-- Rejected -->
                <div class="cg-404-content-wrap waiting-for-result rejected-application text-center">
                      <div class="cg-404-img-wrap text-center">
                            <img src="{{asset('images/reject.svg')}}" class="img-fluid" alt="{{ env('APP_NAME') }}-404"/>
                      </div>
                      <h4 class="font1 grey w600">Unfortunately, your application was rejected. Never get tired. <br>Learn more and try to be with us.</h4>
                    <div class="">
                        <a href="{{ url('/') }}" class="first">Back to Home</a>
                    </div>
                  </div>
                  @elseif($author_request_status == 0)
                  <!-- Pending -->
                    <div class="cg-404-content-wrap waiting-for-result text-center">
                          <div class="cg-404-img-wrap text-center">
                                <img src="{{asset('images/wait.svg')}}" class="img-fluid" alt="{{ env('APP_NAME') }}-404"/>
                          </div>
                          <h4 class="font1 grey w600">Application is being reviewed. The review may take up to 48 hours.</h4>
                        <div class="">
                            <a href="{{ url('/') }}" class="first">Back to Home</a>
                        </div>
                      </div>

                  @else
                <div class="product-upload-form-wrap payout-select-form d-flex align-items-center justify-content-center">
                        <div class="col-md-10 cg-nice-top-line">
                                {!! Form::open(array('url' => [$user->user_url.'/become_a_seller'],'method'=>'PUT','files' => true)) !!}
                            <div class="row d-flex add-btm-xsml d-flex align-items-start justify-content-center">
                            <div class="col-md-6 cg-top-input">
                                <label for="store_name">Preferred Store name <span class="mp-must-need">*</span></label>
                                <input type="text" id="store_name" value="{{ old('store_name') }}" name="store_name" @if($errors->has('store_name')) class="cg_error_class" @endif/>
                                @if($errors->has('store_name'))
                                    <span  class="text-danger">{{ $errors->first('store_name') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6 cg-top-input">
                              <label for="website">Existing Portfolio or Website <span class="mp-must-need">*</span></label>
                              <input type="url" id="website" value="{{ old('website') }}" name="website" @if($errors->has('website')) class="cg_error_class" @endif>
                                @if($errors->has('website'))
                                    <span  class="text-danger">{{ $errors->first('website') }}</span>
                                @endif
                            </div>
                            </div>
                            <div class="row d-flex add-btm-xsml d-flex align-items-start justify-content-center">
                                <div class="col-md-12 cg-top-input">
                                    <label for="store_slug">Store Slug <span class="mp-must-need">*</span></label>
                                    <input type="text" id="store_slug" value="{{ old('store_slug') }}" name="store_slug" @if($errors->has('store_slug')) class="cg_error_class" @endif>
                                    @if($errors->has('store_slug'))
                                        <span  class="text-danger">{{ $errors->first('store_slug') }}</span>
                                    @endif
                                </div>
                            </div>
                        <div class="row d-flex add-btm-xsml d-flex align-items-start justify-content-center">
                            <div class="col-md-4 cg-top-input">
                                <label for="contact_email">Primary Contact Email <span class="mp-must-need">*</span></label>
                                <input type="email" id="contact_email" value="{{ $user->email }}" name="contact_email" readonly @if($errors->has('contact_email')) class="cg_error_class" @endif/>
                                @if($errors->has('contact_email'))
                                    <span  class="text-danger">{{ $errors->first('contact_email') }}</span>
                                @endif
                            </div>
                            <div class="col-md-4 cg-top-input d-flex flex-column">
                                <label for="payout_provider">Preferred Payout Provider <span class="mp-must-need">*</span></label>
                                <select name="payout_provider" id="payout_provider" @if($errors->has('payout_provider')) class="cg_error_class" @endif>
                                    <option value="PayPal" @if(old('payout_provider') == "PayPal") selected @endif>PayPal</option>
                                </select>
                                @if($errors->has('payout_provider'))
                                    <span  class="text-danger">{{ $errors->first('payout_provider') }}</span>
                                @endif
                            </div>

                            <div class="col-md-4 cg-top-input">
                                <label for="payout_email">Preferred Payout Email <span class="mp-must-need">*</span></label>
                                <input type="email" id="payout_email" value="{{ old('payout_email') }}" name="payout_email" @if($errors->has('payout_email')) class="cg_error_class" @endif>
                                @if($errors->has('payout_email'))
                                    <span  class="text-danger">{{ $errors->first('payout_email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cg-comment-type-box">
                                <label for="description">Tell us about your skills and experience. <span id="count">(0 Characters)</span> <span class="mp-must-need">*</span></label>
                                <textarea  data-toggle="tooltip" title="Required 250 Characters" data-placement="top" name="description" id="description" placeholder="" @if($errors->has('description')) class="cg_error_class" @endif>{{ old('description') }}</textarea>
                                @if($errors->has('description'))
                                    <span  class="text-danger">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>
                            <div class="cg-category-extra-wrap">
                                <div class="d-flex justify-content-start cg-category-extra">
                                    <div class="cg-category-checkbox d-flex align-items-center">
                                        <label for="privacy_policy" class="cg-shop-filter-check">I have Read <a href="{{ \App\Traits\HelperTrait::policyLink('author-terms') }}">Author Terms</a>
                                            <input type="checkbox" value="1" name="privacy_policy"  id="privacy_policy"/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <div class="d-flex justify-content-center">
                          <input type="submit" value="Create Store" disabled name="submit" id="submit" />
                        </div>
                            {!! Form::close() !!}
                    </div>
                </div>
                  @endif
              </div>
            </div>
        </section>
@endsection
@push('footer_scripts')
    <script>
        $('#privacy_policy').click(function(){
            //If the checkbox is checked.
            if($(this).is(':checked')){
                //Enable the submit button.
                $('#submit').attr("disabled", false);
            } else{
                //If it is not checked, disable the button.
                $('#submit').attr("disabled", true);
            }
        });

        $("#description").keyup(function(){
            document.getElementById('count').innerHTML = "("+this.value.length + " Characters)";
        });
    </script>
@endpush
