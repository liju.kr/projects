@extends('layouts.app', ['page_class' => 'master-wrap cg-help-view-page help-inner-detail-page'])
@section('title', 'Help')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
        <section class="help-main-inner-page-wrap d-flex justify-content-center align-items-center flex-column">
          <div class="privacy-sub-nav help-inner-sub-nav">
            <ul class="d-flex align-items-center">
                <li><a href="{{ url('/help') }}" class="f12 grey">Help Center </a></li>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>
                <li><a href="{{ url($help_category->getHelpCategoryURL()) }}" class="f12 grey"> {{ $help_category->name }}</a></li>
            </ul>
          </div>
          <div class="privacy-body-block">
            <div class="help-inner-main-contents d-flex justify-content-start align-items-start text-center flex-column">
              <h2 class="theme-text">{{ $help_topic->title }}</h2>
            </div>
          {!! $help_topic->description !!}
          </div>
          @if($related_help_topics->count())
          <div class="related-privacy-nav">
            <h3 class="f16 font1 w700 no-margin pad-btm-sml">Related Informations</h3>
            <ul class="d-flex justify-content-start align-items-center flex-wrap">
                @foreach($related_help_topics as $related_help_topic)
                    <li><a href="{{ url($related_help_topic->getHelpTopicURL())}}" class="grey f13 font1">{{ $related_help_topic->title }}</a></li>
                @endforeach
            </ul>
          </div>
          @endif
        </section>
        <div class="cg-help-intermediate-wrap d-flex justify-content-center align-items-center flex-column">
          <div class="cg-help-inter-overlay"></div>
          <h4 class="font1 f18 first w600">Still no luck to find the right one?</h4>
          <p class="text-center light-grey font1 f14 w500">If any of the information we have provided above does not help you, please contact us. We look forward to helping you.</p>
          <a href="{{ url('/support/'.$help_topic->help_category->slug) }}" class="font1 f14 first theme-bg w600">Contact Support</a>
        </div>
@endsection
