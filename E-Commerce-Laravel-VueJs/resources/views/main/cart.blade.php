@extends('layouts.app', ['page_class' => 'master-wrap common-popup cart-items-page font1'])
@section('title', 'Cart')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    @php
        $url = URL::current();
    @endphp
 <div class="cg-profile-over-view-wrap"></div>
        <section class="cg-pop-panel-wrap cart-item-wrap">
            <div class="cg-pop-panel-body  d-flex justify-content-center">
              <div class="cg-semi-body-contents" id="cart">
                <div class="float-panel-part text-center d-flex align-items-center justify-content-center">
                <h3 class="font1 cg-section-sml-heading w700">Cart Items</h3>
                </div>
                  <div class="product-upload-form-wrap">
                      <div class="col-md-12 cart-page-wrap">
                          @php
                              $total_price=0;
                          @endphp
                          @if(isset($session_cart_items) && !empty($session_cart_items))
                          @foreach($session_cart_items as $session_cart_item)
                              @php
                                  $cart_item = \App\Models\Product\Product::where('id', $session_cart_item['product_id'])->first();
                              @endphp
                          <div class="row cg-sale-chart-border">
                              <div class="cg-sale-chart-item col-md-12 d-flex align-items-center justify-content-between">
                                  <div class="cg-product-dp col-md-1"><img class="img-fluid" src="{{$cart_item->cover_photo_path}}" alt="{{$cart_item->name}}" data-no-retina></div>
                                  <div class="cg-product-name col-md-4"><h4>{{ \Illuminate\Support\Str::limit($cart_item->name, 31, $end='...') }}</h4></div>
                                  <div class="cg-product-category col-md-2"><h4>{{$cart_item->product_sub_category->name}}</h4></div>
                                  <div class="cg-product-date col-md-2"><h4 class="text-uppercase">{{ date("j F Y", strtotime($session_cart_item['date_added'])) }}</h4></div>
                                  <div class="cg-product-commission col-md-1"><h4 class="text-uppercase">{{$cart_item->file_type}}</h4></div>
                                  <div class="cg-product-price col-md-1"><h4>@if($cart_item->price) ${{ $cart_item->price * $session_cart_item['license_value'] }} @else Free @endif</h4></div>
                                  <div class="cg-delete-item col-md-1">
                                    <a onclick="removeCart({{ $session_cart_item['product_id'] }})"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a>
                                  </div>
                              </div>
                          </div>
                              @php
                              $total_price = $total_price + $cart_item->price * $session_cart_item['license_value'];
                              @endphp
                              @endforeach

                               @elseif(isset($cart_items) && $cart_items->count())
                                  @php
                                      $total_price=0;
                                  @endphp
                                  @foreach($cart_items as $cart_item)

                                      <div class="row cg-sale-chart-border">
                                          <div class="cg-sale-chart-item col-md-12 d-flex align-items-center justify-content-between">
                                              <div class="cg-product-dp col-md-1"><img class="img-fluid" src="{{$cart_item->product->cover_photo_path}}" alt="{{$cart_item->product->name}}" data-no-retina></div>
                                              <div class="cg-product-name col-md-4"><h4>{{ \Illuminate\Support\Str::limit($cart_item->product->name, 31, $end='...') }}</h4></div>
                                              <div class="cg-product-category col-md-2"><h4>{{$cart_item->product->product_sub_category->name}}</h4></div>
                                              <div class="cg-product-date col-md-2"><h4 class="text-uppercase">{{ $cart_item->cart_added }}</h4></div>
                                              <div class="cg-product-commission col-md-1"><h4 class="text-uppercase">{{$cart_item->product->file_type}}</h4></div>
                                              <div class="cg-product-price col-md-1"><h4>@if($cart_item->product->price) ${{ $cart_item->product->price * $cart_item->license_value }} @else Free @endif</h4></div>
                                              <div class="cg-delete-item col-md-1"><a onclick="removeCart({{ $cart_item->product->id }})"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a></div>
                                          </div>
                                      </div>
                                      @php
                                          $total_price = $total_price + ($cart_item->product->price * $cart_item->license_value);
                                      @endphp
                                  @endforeach

                              @else
                              <div class="cg-empty-wrap">
                                  <div class="cg-empty-content-wrap text-center add-btm-mdm">
                                      <img src="{{ asset('/images/no-items.svg') }}" alt="{{ env('APP_NAME') }}" class="img-ﬂuid width-30">
                                      <h4 class="font1 grey f18 w500 pad-top-mdm pad-btm-xsml">You don't have any cart items yet.</h4>
                                      <a href="{{ url('/all-items') }}" class="ﬁrst theme-bg f14 w600">Browse Products</a>
                                  </div>
                              </div>
                              @endif
                      </div>

                      @if($total_price)
                          <div class="row cg-cart-total d-flex justify-content-end align-items-center add-top-mdm">
                              <h3 class="f18 font1 w700">Order Total</h3>
                              <h2 class="f40 font1 w700 theme add-left-mdm">@if($total_price) ${{ $total_price }} @else 000.00 @endif</h2>
                          </div>
                          <div class="cg-cart-proceed-btn d-flex justify-content-end">
{{--                              <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/payments') }}" >Proceed to Checkout</a>--}}
                              @if(Auth::check() && Auth::user()->user_profile->update_status)
                                  @if(Auth::user()->user_profile->country =="IN")
                                      <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-paypal') }}" >Proceed to Checkout (Paypal)</a>
                                  @else
{{--                                      <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-paypal') }}" >Proceed to Checkout (Paypal)</a>--}}
                                      <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-stripe') }}" >Proceed to Checkout (Stripe)</a>
                                  @endif
                              @else
                                  <div class="cg-user-profile-message">
                                      <span> <a href="{{ url('/complete-profile') }}">Please provide your billing information to checkout</a> </span>
                                  </div>
                              @endif
                          </div>
                      @elseif(!$total_price && ((isset($session_cart_items) && !empty($session_cart_items)) || (isset($cart_items) && $cart_items->count() ) ))
                          <div class="row cg-cart-total d-flex justify-content-end align-items-center add-top-mdm">
                              <h3 class="f18 font1 w700">Order Total</h3>
                              <h2 class="f40 font1 w700 theme add-left-mdm">@if($total_price) ${{ $total_price }} @else 000.00 @endif</h2>
                          </div>
                          <div class="cg-direct-buy-proceed-btn">
                              {!! Form::open(array('url' => ['/buy_now_free_cart'],'method'=>'PUT','files' => true)) !!}
                              <input type="submit" value="Proceed to Checkout" name="submit" id="submit" />
                              {!! Form::close() !!}
                          </div>
                      @endif


                  </div>

              </div>
            </div>
        </section>
 <div id="toast_wrap" class="cg-toast-one toast_display_none">
     <ul class="d-flex justify-content-between">
         <li class="first"><p id="toast_message"></p></li>
         <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
     </ul>
 </div>

@endsection

@push('footer_scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function removeCart(product_id) {
            document.getElementById("toast_wrap").classList.remove("toast-killed");
            $.ajax({
                type:'POST',
                url:'/remove_cart',
                data:{product_id:product_id},
                success:function(data){

                    console.log(data.success);
                    if(data.status == "success")
                    {
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML=data.message;
                        window.location.reload();
                    }
                    else
                    {
                        document.getElementById("toast_wrap").classList.remove("toast_display_none");
                        document.getElementById('toast_message').innerHTML=data.message;
                        window.location.reload();
                    }
                }
            });
        }
    </script>
@endpush
