@extends('layouts.app', ['page_class' => 'common-popup direct-buy font1'])
@section('title', 'Buy Now')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    @php
        $url = URL::current();
    @endphp
    <section class="cg-profile-over-view-wrap single-product-buy-page">
        <div class="cg-direct-buy-wrap d-flex justify-content-between align-items-center flex-column">

          <div class="d-buy-item-preview">
              <img class="img-fluid" src="{{ $product->cover_photo_original_path }}" alt="{{$product->name}}">
              <h2 class="cg-d-buy-cover-price d-flex align-items-center justify-content-center">
                <span>@if($product->price) ${{ $product->price * $license_value }} @else Free @endif</span>
              </h2>
          </div>

          <div class="d-flex justify-content-center align-items-start flex-column cg-d-buy-item-identify">
            <h3 class="font1 cg-d-buy-heading w700">Order Details<span class="d-buy-hori-line"></span></h3>
            <h4 class="cg-d-buy-name">{{ $product->name }}</h4>
            <div class="cg-product-star-rating d-flex justify-content-start">
                <svg class="f13 feather feather-star @if($product->total_average_rating >=1) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                <svg class="f13 feather feather-star @if($product->total_average_rating >=2) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                <svg class="f13 feather feather-star @if($product->total_average_rating >=3) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                <svg class="f13 feather feather-star @if($product->total_average_rating >=4) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                <svg class="f13 feather feather-star @if($product->total_average_rating >=5) cg-rated @else cg-norated @endif" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
            </div>
          </div>

          <div class="cg-d-buy-features">
              <ul>
{{--                  @if($product->product_sub_category->product_category->name)--}}
{{--                  <li class="cg-spec-line d-flex align-items-center justify-content-between">--}}
{{--                    <h5 class="w700 font1 grey f13">Category:</h5>--}}
{{--                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->product_sub_category->product_category->name }}</span>--}}
{{--                  </li>--}}
{{--                  @endif--}}
                  @if($product->product_sub_category->name)
                  <li class="cg-spec-line d-flex align-items-center justify-content-between">
                    <h5 class="w700 font1 grey f13">Category:</h5>
                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->product_sub_category->name }}</span>
                  </li>
                  @endif

                  @if($product->file_size)
                  <li class="cg-spec-line d-flex align-items-center justify-content-between">
                    <h5 class="w700 font1 grey f13">File Size: </h5>
                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->file_size }}</span>
                  </li>
                  @endif
                  @if($product->date_added)
                  <li class="cg-spec-line d-flex align-items-center justify-content-between">
                    <h5 class="w700 font1 grey f13">Created Date: </h5>
                    <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->date_added }}</span>
                  </li>
                 @endif
                 @if($product->date_updated)
                 <li class="cg-spec-line d-flex align-items-center justify-content-between">
                   <h5 class="w700 font1 grey f13">Last Updated: </h5>
                   <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $product->date_updated }}</span>
                 </li>
                @endif
                      @if($product->file_types()->count())
                <li class="cg-spec-line d-flex align-items-center justify-content-between">
                  <h5 class="w700 font1 grey f13">File Types:</h5>
                <span class="add-left-sml cg-product-categories w300 font1 grey f14">
                                @php
                                    $types = "";
                                     foreach($product->product_sub_category->admin_file_types()->default()->get() as $default_file_type)
                                     {
                                      $types = $types.$default_file_type->name.", ";
                                     }
                                    foreach($product->file_types as $file_type)
                                    {
                                    $types = $types.$file_type->name.", ";
                                    }
                                @endphp
                    {{ rtrim($types, ", ") }}
                                    </span>
                </li>
                 @endif
                 <li class="cg-spec-line d-flex align-items-center justify-content-between">
                   <h5 class="w700 font1 grey f13">Licence Type:</h5>
                   <span class="add-left-sml cg-product-categories w300 font1 grey f14">{{ $license_name }}</span>
                 </li>
              </ul>
          </div>

          <div class="cg-single-cart-details">
              <div class="cg-d-buy-total d-flex align-items-center justify-content-between">
                  <h3 class="f18 font1 w700">Order Total </h3>
                  <h2 class="f40 font1 w700 theme add-left-mdm"><span>@if($product->price) ${{ $product->price * $license_value }} @else Free @endif</span></h2>
              </div>
          </div>
            @if($product->price && !$product->is_downloaded)
                <div class="cg-cart-proceed-btn">
{{--                    <form  method="POST" id="payment-form"  action="/payment/add-funds/paypal">--}}
{{--                        {{ csrf_field() }}--}}
{{--                        <input type="hidden" value="{{ $product->id }}" name="product_id">--}}
{{--                        <input type="hidden" value="{{ $license_value }}" name="license_value">--}}
{{--                        <input type="hidden" value="{{ $license_name }}" name="license_name">--}}
{{--                        <input type="hidden" value="{{ $product->price * $license_value }}" name="amount">--}}
{{--                        <button class="cg-direct-buy-proceed-btn">Pay with PayPal</button>--}}
{{--                    </form>--}}
                    @if(Auth::check() && Auth::user()->user_profile->update_status)
                        @if(Auth::user()->user_profile->country =="IN")
                        <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-paypal') }}" >Proceed to Checkout (Paypal)</a>
                        @else
{{--                            <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-paypal') }}" >Proceed to Checkout (Paypal)</a>--}}
                            <a class="cg-direct-buy-proceed-btn" href="{{ url($url.'/pay-with-stripe') }}" >Proceed to Checkout (Stripe)</a>
                        @endif
                    @else
                        <div class="cg-user-profile-message">
                           <span> <a href="{{ url('/complete-profile') }}">Please provide your billing information to checkout</a> </span>
                        </div>
                         @endif
                </div>
                @elseif(!$product->price && !$product->is_downloaded)
                <div class="cg-direct-buy-proceed-btn">
                 {!! Form::open(array('url' => ['/buy_now_free'],'method'=>'PUT','files' => true)) !!}
                  <input type="hidden" value="{{ $product->id }}" name="product_id">
                  <input type="hidden" value="{{ $license_value }}" name="license_value">
                  <input type="hidden" value="{{ $license_name }}" name="license_name">
                  <input type="submit" value="Proceed to Checkout" name="submit" id="submit" />
                 {!! Form::close() !!}
                </div>
                @else
                <div class="cg-direct-buy-proceed-btn">
                    <input type="submit" value="Already Purchased" name="submit" id="submit" />
                </div>
                @endif

          </div>
    </section>
    @if ($message = Session::get('success'))
        <div class="w3-panel w3-green w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
              class="w3-button w3-green w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('success');?>
    @endif
    @if ($message = Session::get('error'))
        <div class="w3-panel w3-red w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
              class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('error');?>
    @endif
@endsection
@push('footer_scripts')
    <script src="{{ asset('javascripts/custom/file-upload.js') }}"></script>
@endpush
