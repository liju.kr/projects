@extends('layouts.app', ['page_class' => 'common-popup cg-main-notification-page font1'])
@section('title', 'Notifications')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">

@endsection
@section('content')
<div class="cg-profile-over-view-wrap"></div>
 <section class="cg-pop-panel-wrap notification-item-wrap">
            <div class="cg-pop-panel-body  d-flex justify-content-center">
              <div class="cg-semi-body-contents" id="notifications">
                <div class="float-panel-part text-center d-flex align-items-center justify-content-center">
                  <h3 class="font1 cg-section-sml-heading w700">Your Notification</h3>
                </div>
                <nav>
                  <div class="nav notification-nav-head-wrap nav-tabs d-flex justify-content-center" id="nav-tab" role="tablist">
                  	<router-link :to="{ name: 'notifications' }" active-class = "active"  class="cg-notification-thumb nav-item nav-link"><h3>All Notifications</h3></router-link>
                  	<router-link :to="{ name: 'notification_ratings' }" active-class = "active" class="cg-notification-thumb nav-item nav-link"><h3>Reviews</h3></router-link>
                  	<router-link :to="{ name: 'notification_sales' }" active-class = "active" class="cg-notification-thumb nav-item nav-link"><h3>Sales</h3></router-link>
                   <router-link :to="{ name: 'notification_follows' }" active-class = "active" class="cg-notification-thumb nav-item nav-link"><h3>Follows</h3></router-link>
                  <router-link :to="{ name: 'notification_favorites' }" active-class = "active" class="cg-notification-thumb nav-item nav-link"><h3>Favorites</h3></router-link>
                  </div>
                </nav>
                <!-- Notification nav for mobiles -->
                  <div class="cg-mobile-notification-nav-wrap">
                      <div class="dropdown">
                          <button class="btn btn-default dropdown-toggle font1" type="button" data-toggle="dropdown" value="Fast">All Notification
                              <span class="caret"><i class="fas fa-chevron-down"></i></span></button>
                          <ul id="notification-Dropdown" class="dropdown-menu" value="Fast">
                              <router-link :to="{ name: 'notifications' }" tag="li" class="font1">All Notifications</router-link>
                              <router-link :to="{ name: 'notification_ratings' }" tag="li"  class="font1">Reviews</router-link>
                              <router-link :to="{ name: 'notification_sales' }" tag="li"  class="font1">Sales</router-link>
                              <router-link :to="{ name: 'notification_follows' }" tag="li"  class="font1">Follows</router-link>
                              <router-link :to="{ name: 'notification_favorites' }" tag="li"  class="font1">Favorites</router-link>
                          </ul>
                      </div>
                  </div>
                <div class="tab-content pad-top-sml cg-notification-tab-content" id="nav-tabContent">
     			<transition>
            		<router-view></router-view>
    			</transition>
               </div>
 			 </div>
            </div>
        </section>
@endsection
@push('footer_scripts')
    <script>
        var user_url = "{{ $user->user_url }}";
    </script>
    <script>
        jQuery(document).ready(function($) {
        $myDropdown = $('#notification-Dropdown');
        $myDropdown.find("li").click(function() {
            $caret = ' <span class="caret"><i class="fas fa-chevron-down"></i></span>';
            $val = $(this).html();
            $(".btn.dropdown-toggle").html($val + $caret)
        });
        });

    </script>
    <script src="{{ asset('js/notifications.js') }}"></script>
@endpush
