@extends('layouts.app', ['page_class' => 'common-popup login-signup-page font1'])
@section('title', 'Login to Fontfolio')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author"  content="{{ env('APP_NAME') }}">
    <meta name="copyright"  content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="fontfolio, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, creativegoods.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta  content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Creativegoods">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@creativegoods">
    <meta name="twitter:creator" content="@creativegoods">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
   <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    <div class="cg-profile-over-view-wrap d-flex justify-content-center align-items-center">
        <div class="login-wrap text-center">
            <form id="form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="login-text-block ">
                    <h3 class="font1 cg-section-sml-heading w700">Login to Fontfolio</h3>
                </div>
                <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                    <div class="cg-login-inputs">
                        <label for="email">Email ID</label>
                        <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                    <div class="cg-login-inputs">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="cg-main-login-buttons d-flex justify-content-center">
                    <input type="submit" value="Login Securely" name="submit" id="submit" />
                </div>
                <div class="row d-ﬂex add-btm-xsml d-ﬂex justify-content-center">
 <span class="login-additional-links" >New to {{ env('APP_NAME') }}? <a href="{{ url('/register') }}">Sign up!</a></span>
        </div>
            </form>
        </div>
    </div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/login.js') }}"></script>
@endpush
