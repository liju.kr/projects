@extends('layouts.app', ['page_class' => 'master-wrap welcome-affiliate-page first-bg'])
@section('title', 'Affiliate Program')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="Fontfolio">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    <section class="event-welcome d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-welcome-block d-flex justify-content-center align-items-center text-center flex-column">
        <h2 class="theme-text">Hello,</h2>
        <h5>Welcome to our Affiliate Program</h5>
        <p>Do you have a social media, blog or Website related to design and creativity? If so, we are happy to give you incredible earnings by sharing some stunning products of Fontfolio.</p>
      </div>
      <div class="event-img-box"><img class="img-fluid" src="{{ asset('images/event/affiliate.svg') }}" alt="{{ env('APP_NAME') }}"></div>
    </section>
    <section class="event-feature-container d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-feature-wrap d-flex justify-content-center align-items-start flex-wrap">
        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
            </div>
          </div>
          <h4>20% Commission</h4>
          <p>Introduce our products on your sites and among your audience. Each purchase from the customers, through your referral ID will gain you upto 20% commission.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
            </div>
          </div>
          <h4>60 Day Cookie</h4>
          <p>Each purchase by the customer in Fontfolio, made within 60 days from once clicking the referral link, may fill one’s pocket.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
            </div>
          </div>
          <h4>Easy Payouts</h4>
          <p>Payouts are in your hands. You are the one who decides when to process your payouts. So just let us know, and we process your payout within no time.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" fill="#fff" stroke="none" viewBox="0 0 24 24" xmlns:v="https://vecta.io/nano"><circle cx="12" cy="12" r="10" fill="none" stroke="#fff" stroke-width="2" stroke-linejoin="round"/><path d="M16 13.1c0 1.6-.3 2.8-1.1 3.6-.7.8-1.7 1.3-3 1.3s-2.3-.4-3-1.3c-.6-.9-.9-2.1-.9-3.6V11c0-1.6.3-2.8 1-3.7.7-.8 1.7-1.3 3-1.3s2.3.4 3 1.3 1 2.1 1 3.7v2.1zm-2.6-2.5c0-.8-.1-1.5-.3-1.9a1.1 1.1 0 0 0-1-.6 1 1 0 0 0-1 .6c-.2.4-.3 1-.3 1.7v3c0 .9.1 1.5.3 1.9a1.1 1.1 0 0 0 1 .6 1 1 0 0 0 1-.6c.2-.4.3-1 .3-1.8v-2.9z"/></svg>
            </div>
          </div>
          <h4>No Setup Fee</h4>
          <p>We only make sure that you are capable of being an affiliate, fee or other charges don't make any sense. Feel free to join Fontfolio community.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share-2"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>
            </div>
          </div>
          <h4>Easy to share</h4>
          <p>You can copy the product link that comes with your referral id on a single click, and easily share it among your websites, blog and social media.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sidebar"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="3" x2="9" y2="21"></line></svg>
            </div>
          </div>
          <h4>Dashboard</h4>
          <p>Affiliates are given access to a wonderful dashboard where it is easy to get details which include sales, earnings and also to manage payouts.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>
            </div>
          </div>
          <h4>Social share</h4>
          <p>Share and promote through your social media accounts. Earn a part time income and support our authors.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mouse-pointer"><path d="M3 3l7.07 16.97 2.51-7.39 7.39-2.51L3 3z"></path><path d="M13 13l6 6"></path></svg>
            </div>
          </div>
          <h4>PPC Support</h4>
          <p>Make use of ppc (pay per click) support for non-branded keywords. We don't have any other restrictions.</p>
        </div>

        <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
          <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
            <div class="event-svg-set">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-headphones"><path d="M3 18v-6a9 9 0 0 1 18 0v6"></path><path d="M21 19a2 2 0 0 1-2 2h-1a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2h3zM3 19a2 2 0 0 0 2 2h1a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H3z"></path></svg>
            </div>
          </div>
          <h4>Best Support</h4>
          <p>Do you have any questions? We are here to help you, don’t hesitate to ask. We assure our best support.</p>
        </div>
      </div>
      <div class="event-img-box-2"><img class="img-fluid" src="{{ asset('images/event/dash.png') }}" alt="{{ env('APP_NAME') }}"></div>
  </section>

    <section class="cg-event-intermediate-wrap d-flex justify-content-center align-items-center text-center flex-column">
      <div class="cg-event-inter-overlay"></div>
      <div class="event-banner-ico d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg>
        </div>
      </div>
      <h4>Interested to become an Affiliate?</h4>
      <p>We are happy that our best products reach out to more people by embracing new affiliate partners. We humbly remind you that applications are accepted based on your ability to promote referral links. Read our affiliate guide<a class="event-read-more" target="_blank" href="{{ url(\App\Traits\HelperTrait::policyLink('affiliate-guide')) }}">here.</a></p>

        @if(Auth::check() && Auth::user()->is_affiliate)
            <a class="join-event" class="font1 f14 first theme-bg w600" href="{{ url(Auth::user()->user_url.'/affiliate') }}"> Affiliate Dashboard</a>
        @else
            <a class="join-event" class="font1 f14 first theme-bg w600" href="{{ url('/affiliate-application') }}">Join as Affiliate</a>
        @endif
    </section>


@endsection
