@extends('layouts.app', ['page_class' => 'master-wrap common-popup cg-main-affiliate-page common-mini-page'])
@section('title', 'Support')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    <section class="mini-page-wrap cg-support-landing-wrap d-flex align-items-center justify-content-center">
        <div class="mini-page d-flex align-items-center justify-content-end flex-column">
            <div class="mp-user-thumb-wrap d-flex align-items-center justify-content-center flex-column">
                <div class="mp-thumb-icon">
                    @if(Auth::check())
                    <img src="{{ Auth::user()->user_profile->image_path }}" class="img-fluid" alt="{{ Auth::user()->name }}">
                        @else
                        <img src="{{ asset('images/mp-avatar.png') }}" class="img-fluid" alt="user-default">
                    @endif
                </div>
                <h2 class="mp-main-heading-text"> @if(Auth::check()) Hey {{ Auth::user()->short_user_name }}, @endif Let's Talk</h2>
            </div>
            {{ Form::open(array('url' => 'send-support-mail','data-parsley-validate','novalidate',
                                   'files'=>true)) }}
            <div class="mp-form-wrap">
                <div class="mp-name-box d-flex flex-column">
                    <label for="name">Name <span class="mp-must-need">*</span></label>
                    <input placeholder="Your Name" type="text" @if(Auth::check()) value="{{ Auth::user()->name }}" @endif id="name" name="name"  class="@error('name') is-invalid @enderror"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                    @enderror
                </div>
                <div class="mp-email-box d-flex flex-column">
                    <label for="email_id">Email <span class="mp-must-need">*</span></label>
                    <input placeholder="example@domain.com" type="email" id="email_id" name="email_id" @if(Auth::check()) value="{{ Auth::user()->email }}" @endif class="@error('email_id') is-invalid @enderror" />
                    @error('email_id')
                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                    @enderror
                </div>
                <div class="mp-support-type-box d-flex flex-column">
                    <label for="topic">Select Topic <span class="mp-must-need">*</span></label>
                    <select name="topic" id="topic" class="@error('topic') is-invalid @enderror">
                        <option value="">Select Topic</option>
                        @foreach($help_categories as $help_category)
                        <option value="{{ $help_category->id }}" @if($category_id && $category_id == $help_category->id) selected @endif >{{ $help_category->name }}</option>
                        @endforeach
                    </select>
                    @error('topic')
                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                    @enderror
                </div>
                <div class="input-group mp-upload-wrap">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="document" id="inputGroupFile01">
                        <label class="custom-file-label" for="inputGroupFile01">Attach file</label>
                        @error('document')
                        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                        @enderror
                    </div>
                </div>
                <div class="mp-text-box d-flex flex-column">
                    <label for="message">Your Message <span class="mp-must-need">*</span></label>
                    <textarea name="message" id="message" class="@error('message') is-invalid @enderror"></textarea>
                    @error('message')
                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                    @enderror
                </div>
            </div>

{{--            <div class="mp-captcha-box d-flex align-items-center justify-content-center flex-column">--}}
{{--                <!-- <label for="name">Security Check</label> -->--}}
{{--                <img src="http://3.21.156.212/images/captcha.png" class="img-fluid" alt="fontfolio">--}}
{{--                <input placeholder="Enter the above text here.." type="text" id="fname" name="fname">--}}
{{--            </div>--}}

            <div class="submit-support d-flex align-items-center justify-content-center">
                <button class="btn d-flex align-items-center justify-content-center" type="submit" id="submitform" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>
                </button>
                {{ Form::close() }}
        </div>
        </div>
    </section>
@endsection
@push('footer_scripts')
@endpush
