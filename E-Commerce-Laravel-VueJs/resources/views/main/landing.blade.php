<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Fontfolio - Welcome to Fontfolio</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity. We will add Best free Creative Goods Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">

    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/libs/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/cg-main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/cg-styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/mobile.css')}}">
</head>
<body>
<div id="preloader2">
    <div id="status2">&nbsp;</div>
</div>
<div id="master-wrap" class="master-wrap ">
    @if(Session('message'))
        <div class="cg-toast-one">
            <ul class="d-flex justify-content-between">
                <li class="first"><p>{{ Session('message') }}</p></li>
                <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
            </ul>
        </div>
@endif
     <div class="guest-header align-items-center justify-content-between d-flex">
      <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="{{ env('APP_NAME') }}">
      <a href="{{ url('/login') }}" class="guest-login" type="button" name="button">Sign in</a>
    </div>
    <section class="cg-guest-wrap align-items-center justify-content-center d-flex">
      <div class="cg-guest-block align-items-center justify-content-center d-flex">
        <div class="event-img-box"><img class="img-fluid" src="{{asset('images/event/guest.svg')}}" alt="{{ env('APP_NAME') }}"></div>
      </div>
      <div class="cg-guest-form-wrap align-items-center justify-content-center d-flex flex-column">
        <div class="cg-guest-form">
          <h2>Hello,</h2>
          <h3>FontFolio is a highly optimized and focused marketplace for high quality digital assets. We aim to present the best designers and curated collection to get our customers upto speed they deserve.</h3>
          <div class="magic-code-box">
              {{ Form::open(array('url' => 'magic-pin','data-parsley-validate','novalidate',
                                      'files'=>true)) }}
              <label>Enter your Magic pin to open your shop</label>
              <div class="magic-code-items">
                <input placeholder="0"   maxlength="1" name="pin_1" size="1" id="counter">
                <input placeholder="0"   maxlength="1" name="pin_2"size="1" id="counter">
                <input placeholder="0"   maxlength="1" name="pin_3" size="1" id="counter">
                <input placeholder="0"   maxlength="1" name="pin_4" size="1" id="counter">
                <input placeholder="0"   maxlength="1" name="pin_5" size="1" id="counter">
              </div>
              <button type="submit" class="guest-button" type="button" name="submit">Join {{ env('APP_NAME') }}</button>
              {{ Form::close() }}
          </div>
        </div>
      </div>
      <div class="guest-footer align-items-center justify-content-between d-flex">
        <p>© {{ date('Y') }} {{ env('APP_NAME') }} | All Rights Reserved.</p>
        <div class="cg-sub-footer-icon-wrap">
          <ul class="cg-sub-footer-icons d-flex">
              <li><a href="https://www.facebook.com/fontfolio.net" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
              <li><a href="https://www.instagram.com/fontfolio_net/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
              <li><a href="https://twitter.com/fontfolio" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
              <li><a href="https://www.pinterest.com/fontfolio/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><path class="st0" d="M14.5,0.9c-2.1-1.1-4.4-1.1-6.6-0.5C4,1.4,1.7,5.2,2.5,9c0.3,1.4,0.9,2.6,2.2,3.4c0.5,0.3,0.6,0.3,0.7-0.3
                          		c0.1-0.3,0.2-0.7,0.2-1c0-0.2,0-0.5-0.1-0.6c-1.4-2.2-1-5,0.9-6.8c2.5-2.3,6.9-1.7,8.5,1c1.2,2.1,0.8,5.6-0.9,7.4
                          		c-0.5,0.5-1.1,0.9-1.8,1c-1.8,0.4-2.9-1-2.4-2.5c0.2-0.6,0.4-1.2,0.5-1.8c0.2-0.7,0.4-1.4,0.4-2c0.1-0.9-0.3-1.5-0.9-1.8
                          		c-0.7-0.3-1.5-0.1-2,0.5c-1,1.2-1,2.5-0.6,3.9c0.1,0.3,0.1,0.6,0,0.8C7,11.2,6.8,12.1,6.6,13c-0.5,2.1-1.1,4.2-0.8,6.4
                          		c0.1,0.2,0.3,0.9,1,0.3c1.3-1.8,1.7-3.9,2.2-6c0.7,0.8,1.4,1.2,2.4,1.4c0.9,0.1,1.9,0,2.7-0.4c2.4-1,3.6-3,4.1-5.4
                          		C19.1,5.7,17.6,2.5,14.5,0.9z"/></svg></a></li>
          </ul>
        </div>
      </div>
    </section>
</div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
@if(env('APP_ENV')=="production")
    <script>
        $(document).bind("contextmenu",function(e) {
            e.preventDefault();
        });
        $(document).keydown(function(e){
            if(e.which === 123){
                return false;
            }
        });
    </script>
        @endif
<script type="text/javascript" src="{{asset('javascripts/libs/navigation.js')}}"></script>
<script src="{{asset('javascripts/libs/plugins.js')}}"></script>
<script src="{{asset('javascripts/custom/focal.js')}}"></script>
</body>
</html>
