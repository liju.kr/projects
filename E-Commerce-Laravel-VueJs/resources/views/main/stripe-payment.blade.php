@extends('layouts.app', ['page_class' => 'cg-shop-page pre-mail-verification cg-payment-page'])
@section('title', 'Payment')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">

@php
    $product = Session::get('product');
    $license_value = Session::get('license_value');
    $license_name = Session::get('license_name');
    $direct_buy_status = Session::get('direct_buy_status');
    $cart_items = Session::get('cart_items');

    if(!$direct_buy_status)
    {
    $total_price = 0;
        foreach ($cart_items as $cart_tiem)
        {
            $total_price = $total_price + ($cart_tiem->product->price * $cart_tiem->license_value);
        }
    }
    else
    {
     $total_price =$product->price * $license_value;
    }
@endphp
@endsection
@section('content')


  <div class="cg-profile-over-view-wrap"></div>
  <section class="cg-pop-panel-wrap">
      <div class="cg-pop-panel-body  d-flex justify-content-center">
        <div class="cg-semi-body-contents">
          <div class="float-panel-part text-center d-flex align-items-center justify-content-center">

          <h3 class="font1 cg-section-sml-heading w700">Payment Details</h3>
        </div>


                <div class="panel panel-default credit-card-box">
                    <div class="panel-body">

                        @if (Session::has('success'))
                            <div class="alert alert-success text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif

                        <form
                            role="form"
                            action="{{ route('stripe.post') }}"
                            method="post"
                            class="require-validation"
                            data-cc-on-file="false"
                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                            id="payment-form">
                            @csrf
                               <div class='form-row row'>
                                <div class='col-md-6 form-group required'>
                                    <label class='control-label'>Name on Card</label> <input
                                        class='form-control' size='4' type='text' >
                                </div>
                                <div class='col-md-6 form-group card required'>
                                    <label class='control-label'>Card Number</label> <input
                                        autocomplete='off' class='form-control card-number' size='20'
                                        type='text'>
                                </div>
                            </div>



                            <div class='form-row row'>
                                <div class='col-xs-12 col-md-4 form-group cvc required'>
                                    <label class='control-label'>CVV</label> <input autocomplete='off'
                                                                                    class='form-control card-cvc'  size='4'
                                                                                    type='text'>
                                </div>
                                <div class='col-xs-12 col-md-4 form-group expiration required'>
                                    <label class='control-label'>Expire Month</label> <input
                                        class='form-control card-expiry-month' placeholder='MM' size='2'
                                        type='text'>
                                </div>
                                <div class='col-xs-12 col-md-4 form-group expiration required'>
                                    <label class='control-label'>Expire Year</label> <input
                                        class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                        type='text'>
                                </div>
                            </div>
                            <div class="product-details-dd">
                              <div class="d-flex justify-content-between align-items-center">
                                <button type="button" class="second f14 font1" data-toggle="collapse" data-target="#product-details-dd">Product Details <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></button>
                              </div>
                              <div id="product-details-dd" class="collapse">
                                <div class="product-details-dd-collapser">
                                    <!-- item1 -->
                                    @if($direct_buy_status)
                                        <h4 class="font1 grey f14 add-top-sml">{{ $product->name }}<span class="payment-page-product-license">({{ $license_name }})</span> - <span class="payout-item-price">${{ $product->price * $license_value }}</span></h4>
                                    @else
                                        @foreach($cart_items as $cart_item)
                                        <h4 class="font1 grey f14 add-top-sml">{{ $cart_item->product->name }}<span class="payment-page-product-license">({{ $cart_item->license_name }})</span> - <span class="payout-item-price">${{ $cart_item->product->price * $cart_item->license_value }}</span></h4>
                                            @endforeach
                                    @endif
                                      </div>
                              </div>
                          </div>

                          <div class="pay-cards-wrap">
                              <img class="img-responsive pull-right" src="{{ asset('images/cards.png') }}">
                          </div>
                          <span class="accepted-cards-text d-flex justify-content-center">(Accepted Cards)</span>

                          <div class="pay-btn-wrap">
                              <button class="pay-btn btn btn-primary btn-lg btn-block" type="submit">Pay Now (${{ $total_price}})</button>
                          </div>

                        </form>
                    </div>
                </div>

          </div>
        </div>
      </section>

  <div id="toast_wrap" class="cg-toast-one toast_display_none">
      <ul class="d-flex justify-content-between">
          <li class="first"><p id="toast_message"></p></li>
          <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
      </ul>
  </div>
@endsection
@push('footer_scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">
        $(function() {

            var $form         = $(".require-validation");

            $('form.require-validation').bind('submit', function(e) {
                var $form         = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs       = $form.find('.required').find(inputSelector),
                    valid         = true;

                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val(),
                    }, stripeResponseHandler);
                }

            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    document.getElementById("toast_wrap").classList.remove("toast-killed");
                    document.getElementById("toast_wrap").classList.remove("toast_display_none");
                    document.getElementById('toast_message').innerHTML=response.error.message;
                } else {
                    /* token contains id, last4, and card type */
                    var token = response['id'];

                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }

        });
    </script>
@endpush
