@extends('layouts.app', ['page_class' => 'master-wrap welcome-shop cg-help-view-page first-bg'])
@section('title', 'welcome')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
<section class="event-welcome d-flex justify-content-center align-items-center text-center flex-column">
  <div class="event-welcome-block d-flex justify-content-center align-items-center text-center flex-column">
    <h2 class="theme-text">Hello,</h2>
    <h5>Happy to see you here, Let's open a shop for you</h5>
    <p>Sharpen your Skills, Craft your Ideas, publish it on Fontfolio. We welcome you to the great community of creators. Inspire and earn through your talents.</p>
  </div>
  <div class="event-img-box"><img class="img-fluid" src="{{ asset('images/event/shop.svg') }}" alt="{{ env('APP_NAME') }}"></div>
</section>
<section class="event-feature-container d-flex justify-content-center align-items-center text-center flex-column">
  <div class="event-feature-wrap d-flex justify-content-center align-items-start flex-wrap">
    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
        </div>
      </div>
      <h4>Upto 70% Commission</h4>
      <p>List your awesome works on Fontfolio. Each sale of your item gives you 70% of the total price. Excite many with your awesome works.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-anchor"><circle cx="12" cy="5" r="3"></circle><line x1="12" y1="22" x2="12" y2="8"></line><path d="M5 12H2a10 10 0 0 0 20 0h-3"></path></svg>
        </div>
      </div>
      <h4>No Exclusivity</h4>
      <p>We always appreciate your effort. So where to sell, depends on you. Anyway, now you're at the right place, feel free to join us.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
        </div>
      </div>
      <h4>Easy Payouts</h4>
      <p>Every month we hand over the amount which you collected through sales, right into your arms. Don’t be nervous, earn peacefully. </p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
        </div>
      </div>
      <h4>No hidden Deductions</h4>
      <p>We are transparent, and won’t deduct any amount from your earnings. You get exactly what you deserve. </p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-award"><circle cx="12" cy="8" r="7"></circle><polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88"></polyline></svg>
        </div>
      </div>
      <h4>Rank Benefits</h4>
      <p>When you leap over each milestone, it awaits you a rank with a number of benefits. So move on to the higher grounds.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sidebar"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="3" x2="9" y2="21"></line></svg>
        </div>
      </div>
      <h4>Dashboard</h4>
      <p>Sellers are given access to a wonderful dashboard where it is easy to handle products and other details.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
        </div>
      </div>
      <h4>Seller Assistance</h4>
      <p>You can assist or provide support to your customers if you wish to do so. And we had made it simple for you to manage.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>
        </div>
      </div>
      <h4>Best Promotions</h4>
      <p>We assure that your efforts will be fruitful. We will make your work available among our audience.</p>
    </div>

    <div class="event-feature-item d-flex justify-content-center align-items-center text-center flex-column">
      <div class="event-ico-box d-flex justify-content-center align-items-center text-center">
        <div class="event-svg-set">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sunrise"><path d="M17 18a5 5 0 0 0-10 0"></path><line x1="12" y1="2" x2="12" y2="9"></line><line x1="4.22" y1="10.22" x2="5.64" y2="11.64"></line><line x1="1" y1="18" x2="3" y2="18"></line><line x1="21" y1="18" x2="23" y2="18"></line><line x1="18.36" y1="11.64" x2="19.78" y2="10.22"></line><line x1="23" y1="22" x2="1" y2="22"></line><polyline points="8 6 12 2 16 6"></polyline></svg>
        </div>
      </div>
      <h4>Affiliate links</h4>
      <p>Our affiliates are busy promoting your items. They will help us in spreading your works all across the world.</p>
    </div>
  </div>
{{--  <div class="event-img-box-2"><img class="img-fluid" src="{{ asset('images/event/dash.png') }}" alt="{{ env('APP_NAME') }}"></div> --}}
</section>

<section class="cg-event-intermediate-wrap d-flex justify-content-center align-items-center text-center flex-column">
  <div class="cg-event-inter-overlay"></div>
  <div class="event-banner-ico d-flex justify-content-center align-items-center text-center">
    <div class="event-svg-set">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#666" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
    </div>
  </div>
  <h4>Interested to become an author?</h4>
  <p>We are happy to make our community more attractive by embracing new authors. We humbly remind you that the acceptance of applications is based on your qualifications and skills. please read our Author Guide<a class="event-read-more" target="_blank" href="{{ url(\App\Traits\HelperTrait::policyLink('author-guide')) }}">here.</a></p>
    @if(Auth::check() && Auth::user()->is_author)
        <a class="join-event" class="font1 f14 first theme-bg w600" href="{{ url(Auth::user()->profile_url) }}"> My Shop</a>
            @else
                <a class="join-event" class="font1 f14 first theme-bg w600" href="{{ url('/seller-application') }}"> Create New Store</a>
                    @endif
</section>
@endsection
