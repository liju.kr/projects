@extends('layouts.app', ['page_class' => 'master-wrap about-page-wrap first-bg'])
@section('title', 'About')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity. We will add Best free Creative Goods Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection

@section('content')
<section class="event-welcome d-flex justify-content-center align-items-center text-center flex-column">
  <div class="event-welcome-block d-flex justify-content-center align-items-center text-center flex-column">
    <div class="brand-banner-wrap">
      <div class="brand-banner"></div>
    </div>
    <h2 class="theme-text hide-about-head">Fontfolio</h2>
    <p>Fontfolio is a highly optimized and focused marketplace for high quality digital assets. We aim to present talented designers from around the world who are handpicked by our expert panel after careful consideration of their works and thereby providing only the best to our customers, always.</p>
  </div>
  <div class="event-img-box"><img class="img-fluid" src="images/event/about.svg" alt="fontfolio"></div>
</section>

<section class="event-description d-flex justify-content-center align-items-center text-center flex-column">
  <div class="contdainer">
    <div class="about-intro text-left">
      <h3>About Fontfolio</h3>
      <p>Fontfolio is a digital marketplace for high quality creative assets, owned and managed by UniqueEnroute Labs. Our team of experts are industry best and have been key drivers in creating trends in various domains. We strive to present only the best of the lot and that is our key focus as well. We aim to support and work together with the industry best designers and developers creating a better world for people in need of high quality digital contents.</p>
    </div>
    <div class="about-offered-product text-left">
      <h3>Types of products available</h3>
      <p>Fontfolio offer a collection of remarkable digital products in Fonts, Graphics, Graphic Templates, Web Templates, Themes, Addons & Plugins. All these products are carefully analysed and handpicked by our expert panel. We are constantly working to increase our categories and product types.</p>
    </div>
    <div class="about-offered-product text-left">
      <h3>Benefits of joining Fontfolio</h3>
      <p>Fontfolio has an easy to use and unique user interface combined with well-organized content libraries. The integrated messaging platform provides quick support and you will never feel lost. The policies and terms of usage is well drafted to make sure that the world is a good place for all of us. We love working at Fontfolio and we know you will too.</p>
    </div>
    </div>
</section>

<section class="cg-event-intermediate-wrap d-flex justify-content-center align-items-center text-center flex-column">
  <div class="cg-event-inter-overlay"></div>
  <div class="event-banner-ico d-flex justify-content-center align-items-center text-center">
    <div class="event-svg-set">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#666" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
    </div>
  </div>
  <h4>Have any questions or feedback?</h4>
  <p>We are always excited to read an email unless it’s a spam. Write to us with your questions, feedback or suggestions.</p>
  <a class="join-event" href="{{ url('/support') }}" class="font1 f14 first theme-bg w600">Say Hello</a>
</section>
@endsection
