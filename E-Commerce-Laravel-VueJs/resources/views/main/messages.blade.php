@extends('layouts.app', ['page_class' => 'master-wrap common-mini-page cg-message-center'])
@section('title', 'Messages')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
<!-- Modal starts here -->
<div class="lic_pop_modal d-flex justify-content-center align-items-center text-center">
  <div class="licence-pop-details flex-wrap d-flex justify-content-center align-items-center flex-column">
    <a class="preview-delete-button close-lic-modal" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></a>
    <h4>Report Message</h4>
    <p>Please specify the reason for reporting this conversation.</p>
    <ul class="d-flex justify-content-center align-items-center cg-msg-rpt-wrap">
      <li class="d-flex align-items-center cnc-margin">
        <label for="report-2" class="cg-radio-btn">Spam<input type="radio" name="product_categories" id="report-2" value="add-ons"> <span class="checkmark"></span></label>
      </li>
      <li class="d-flex align-items-center cnc-margin">
        <label for="report-3" class="cg-radio-btn">Harassment<input type="radio" name="product_categories" id="report-3" value="add-ons"> <span class="checkmark"></span></label>
      </li>
      <li class="d-flex align-items-center cnc-margin">
        <label for="report-4" class="cg-radio-btn">Something Else<input type="radio" name="product_categories" id="report-4" value="add-ons"> <span class="checkmark"></span></label>
      </li>
    </ul>
    <textarea class="lic-pop-txt-area" placeholder="Type Message here.." name="name" rows="8" cols="80"></textarea>
    <a class="goto-licence-page" target="_blank" href="{{ url('/licences') }}">Report</a>
  </div>
</div>
   <section class="mini-page-wrap cg-message-landing-wrap d-flex align-items-start justify-content-center" id="messages">
      <a class="click-to-see-people">< Contacts</a>
        <transition>
            <router-view></router-view>
        </transition>
    </section>
    <section id="new-messege-here"></section>
@endsection
@push('footer_scripts')
    <script>
        var user_url = "{{ $user->user_url }}";
	    </script>
	    <script src="{{ asset('js/messages.js') }}"></script>
    <script>$(".go-to-new").click(function(){$('html, body').animate({scrollTop:$("#new-messege-here").offset().top},1000)});</script>
@endpush
