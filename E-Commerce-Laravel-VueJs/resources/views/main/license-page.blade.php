@extends('layouts.app', ['page_class' => 'license-page-wrap'])
@section('title', "licences")
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
<section class="licence-wrap d-flex justify-content-center align-items-center text-center flex-column">
  <div class="licence-welcome-block d-flex justify-content-center align-items-center text-center flex-column">
    <h2 class="theme-text">License</h2>
    <h5>License types Fontfolio offers.</h5>
    <p>Licenses available from Fontfolio are listed here. From these options, you can choose the appropriate license. Let's support the designer and his efforts by selecting the proper license type according to your purpose.</p>
  </div>
  <div class="licence-items d-flex justify-content-center align-items-start">
    <div class="lic-block">
      <h2>Personal</h2>
      <h4>Unlimited personal use</h4>
      <ul>
        <h3 class="lic-green">You can:</h3>
        <li>use this item for unlimited personal use.</li>
        <li>modify that item to create an end product which can only be used for personal purposes.</li>
        <li>use this product in any personal Websites, Blogs, Social-medias, which existing without any financial gain.</li>
      </ul>
      <ul>
        <h3 class="lic-red">You cannot:</h3>
        <li>use this item in any public platforms. (Apps & Commercial Websites)</li>
        <li>modify and Redistribute (Sale) this item anywhere.</li>
        <li>use this product in any Platforms which using ads to generate income.(Ex: Websites, Apps, Blogs)</li>
      </ul>
    </div>
    <div class="lic-block">
      <h2>Commercial</h2>
      <h4>Limited commercial use</h4>
      <ul>
        <h3 class="lic-green">You can:</h3>
        <li>use this item for limited commercial use.</li>
        <li>use this item or its end product only for a single commercial purpose.</li>
        <li>use this item to create an end product which can distribute to a single client for remuneration.</li>
      </ul>
      <ul>
        <h3 class="lic-red">You cannot:</h3>
        <li>use this item in more than one public platforms. (Apps & Commercial Websites)</li>
        <li>modify and Redistribute (Sale) this item anywhere.</li>
        <li>use this item in more than one public Platforms which using ads to generate income.(Ex: Websites, Apps, Blogs)</li>
      </ul>
    </div>
    <div class="lic-block">
      <h2>Premium Commercial</h2>
      <h4>Unlimited commercial use</h4>
      <ul>
        <h3 class="lic-green">You can:</h3>
        <li>You can modify and use it for any commercial purposes.</li>
        <li>You can modify and redistribute to unlimited clients with sublicensing.</li>
        <li>use this product in any Websites, Blogs, Social-medias and any public platforms.</li>
      </ul>
      <ul>
        <h3 class="lic-red">You cannot:</h3>
        <li>replicate or duplicate the item and sell it on Fontfolio or any other marketplaces.</li>
        <li>use end products to sell on another marketplaces & Ecommerce Platforms.</li>
        <li>claim the ownership of the product or services.</li>
      </ul>
    </div>
  </div>
<a class="direct-to-policy" href="{{ \App\Traits\HelperTrait::policyLink('user-terms') }}">Know More</a>
</section>
@endsection
@push('footer_scripts')
    <script>
    </script>
@endpush
