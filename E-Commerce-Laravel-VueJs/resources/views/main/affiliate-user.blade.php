@extends('layouts.app', ['page_class' => 'common-popup cg-main-affiliate-page font1'])

@section('title', 'Affiliate Dashboard')

@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity. We will add Best free Fontfolio Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <meta name="copyright" content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="{{ env('APP_NAME') }}, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, {{ env('APP_NAME') }}.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Fontfolio for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection

@section('content')
<div class="cg-profile-over-view-wrap"></div>
        <section class="cg-pop-panel-wrap notification-item-wrap" id="affiliation">
            <div class="cg-pop-panel-body  d-flex justify-content-center">
              <div class="cg-semi-body-contents">
                <div class="float-panel-part text-center d-flex align-items-center justify-content-center">
                  <h3 class="font1 cg-section-sml-heading w700">Affiliate Dashboard</h3>
                </div>
                <div class="affiliate-earnings-over-view d-flex align-items-center justify-content-center">
                  <div class="top-aff-tab text-center aff-earned">
                    <h2>${{ $total['total_sold'] }}</h2>
                    <p>Total Sold</p>
                  </div>
                  <div class="top-aff-tab text-center aff-earned">
                    <h2>${{ $total['total_earned'] }}</h2>
                    <p>Total Earned</p>
                  </div>
                  <div class="top-aff-tab text-center aff-paid">
                    <h2>${{ $total['total_paid'] }}</h2>
                    <p>Total Paid</p>
                  </div>
                  <div class="top-aff-tab text-center aff-balance">
                    <h2>${{ $total['total_balance'] }}</h2>
                    <p>Current Balance</p>
                  </div>
                </div>
    <earning-tab earnings="{{ $earnings }}"></earning-tab>
    <notification-tab user="{{ $user }}"></notification-tab>
              <div class="add-top-sml text-center cg-affiliate-link-tray">
                <div class="alert alert-info" role="alert">Affiliate Link : To share any Fontfolio products, add the following code to the end of the URL: <b>?u={{ Auth::user()->affiliate_code }}</b></div>
              </div>
                <!-- Affiliate sale details -->
                <!-- ○══════════════════════════════════════════════════════════════════════════○ -->
<sales-tab ></sales-tab>
                <div class="aff-attention-alert">
                  <div class="alert alert-light" role="alert">To read more about Affiliate Terms, Guide & User Terms click
                    <a href="{{ \App\Traits\HelperTrait::policyLink('affiliate-guide') }}" class="alert-link">here</a>.
                  </div>
                </div>
              </div>

            </div>
        </section>
@endsection
@push('footer_scripts')
    <script>
         var user_url = "{{ $user->user_url }}";
    </script>
    <script src="{{ asset('js/affiliate.js') }}"></script>
@endpush
