@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Invoices</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}<!-- Button trigger modal -->
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAdd">
			New Invoice
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">

        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Bill Date</th>
                    <th>Account Type</th>
                    <th>Amount (USD)</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $key=>$invoice)
                    <tr class="@if($invoice->account_type=="debit") text-danger @elseif($invoice->account_type=="credit") text-success @endif">
                        <td scope="row">{{(($invoices->currentpage()-1)*$invoices->perpage())+$key+1}}</td>
                        <td>{{ $invoice->title }}</td>
                        <td>{{ date('d-m-Y', strtotime($invoice->bill_date)) }}</td>
                        <td><span class="badge @if($invoice->account_type=="debit") badge-danger @elseif($invoice->account_type=="credit") badge-success @endif text-capitalize">{{ $invoice->account_type }}</span></td>
                        <td>${{ $invoice->amount }}</td>
                        <td>
                            <a href="#Modal{{ $invoice->id }}"  type="button" data-toggle="modal" data-target="#ModalEdit{{ $invoice->id }}" class="btn-sm btn-warning">Edit</a>
                        </td>
                    </tr>
                    <div class="modal mod_ck_editor fade" id="ModalEdit{{ $invoice->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $invoice->id }}" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="topicModal">Edit Invoice</h5>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['invoices.destroy', $invoice->id],'id' => 'deleteForm'.$invoice->id, ]) !!}
                                    <input type="hidden" name="page" value="{{ $invoices->currentPage() }}">
                                    {!! Form::close() !!}
                                    @if($invoice->type == "automatic")
                                        <a  class="btn btn-danger btn-sm" onclick="deleteLocked{{$invoice->id}}()" href="">Locked</a>
                                    @else
                                        <a  class="btn btn-danger btn-sm" onclick="deleteConfirm{{$invoice->id}}()" href="">Delete</a>
                                    @endif
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ Form::model($invoice, array('route' => array('invoices.update', $invoice->id),
                           'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $invoice->title }}">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="{{ $invoice->description }}">
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="account_type">
                                            <option value="debit" @if($invoice->account_type == "debit") selected @endif>Debit</option>
                                            <option value="credit" @if($invoice->account_type == "credit") selected @endif>Credit</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount in USD" value="{{ $invoice->amount }}">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="bill_no" name="bill_no" placeholder="Bill Number" value="{{ $invoice->bill_no }}">
                                    </div>

                                    <div class="form-group">
                                        <input type="date" class="form-control" id="bill_date" name="bill_date" placeholder="Amount" value="{{ $invoice->bill_date }}">
                                    </div>

                                    <hr>

                                    @if($invoice->admin_id)
                                        <p>
                                            Last Updated By : <a class="btn btn-light" data-toggle="collapse" href="" role="button" > {{ $invoice->admin->name }} </a>
                                        </p>
                                    @endif
                                    <p>
                                        Type : <span class="badge @if($invoice->type=="manual") badge-primary @else badge-warning @endif text-capitalize">{{ $invoice->type }}</span>
                                    </p>
                                    <p>
                                        Updated On : {{ date('j M, Y h:i A', strtotime($invoice->updated_at)) }}
                                    </p>

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
            </table>
        </div>


	</div>
    <div class="row">  {{ $invoices->links() }}</div>
</div>

<!-- Modal -->
<div class="modal mod_ck_editor fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="ModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="topicModal">New Invoice</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/invoices','data-parsley-validate','novalidate',
                            'files'=>true)) }}

                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                </div>

                <div class="form-group">
                    <select class="form-control" name="account_type">
                        <option value="debit">Debit</option>
                        <option value="credit">Credit</option>
                    </select>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount in USD">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="bill_no" name="bill_no" placeholder="Bill Number">
                </div>

                <div class="form-group">
                    <input type="date" class="form-control" id="bill_date" name="bill_date" placeholder="Amount">
                </div>


					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($invoices as $invoice)
function deleteConfirm{{$invoice->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteForm{{$invoice->id}}").submit();
    }
    else {}
}
        function deleteLocked{{$invoice->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach


    </script>

@endpush
