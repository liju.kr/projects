@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Product Sub Categories</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
          <a type="button" href="{{ route('product_sub_categories.create') }}" class="btn btn-sm btn-info mb-2">Add</a>
            </div>

        </div>
    </div>
    <!-- Portfolio Item Heading -->
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">Sort</th>
                <th scope="col">Name</th>
                <th scope="col">Product Category</th>
                <th scope="col">Status</th>
                <th scope="col">Added On</th>
                <th scope="col">Last Updated By</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product_categories as $key=>$product_category)
                <tr class="bg-info text-center font-weight-bold text-white">
                    <th colspan="8">{{(($product_categories->currentpage()-1)*$product_categories->perpage())+$key+1}} ==> {{ $product_category->name }}</th>
                </tr>
                @php $i=1; @endphp
            @foreach($product_category->product_sub_categories()->order()->get() as $key=>$product_sub_category)
            <tr>
                <td scope="row">{{ $i }}</td>
                <td scope="row">{{ $product_sub_category->sort }}</td>
                <td scope="row"> {{ $product_sub_category->name }}</td>
                <td scope="row"> {{ $product_sub_category->product_category->name }}</td>
                 <td scope="row">
                  @if($product_sub_category->status ==1)
                      <a href="" class="badge-success badge">Active</a>
                      @else
                        <a href="" class="badge-danger badge">In Active</a>
                      @endif
                </td>
                <td scope="row"> {{ date('d-m-y h:i A', strtotime($product_sub_category->created_at)) }}</td>
                <td scope="row">{{ $product_sub_category->admin->name }} <br>{{ date('d-m-y h:i A', strtotime($product_sub_category->updated_at)) }}</td>
                <td scope="row">
                    {!! Form::open(['method' => 'DELETE', 'route' => ['product_sub_categories.destroy', $product_sub_category->id],'id' => 'deleteForm'.$product_sub_category->id, ]) !!}
                     <input type="hidden" name="page" value="{{ $product_categories->currentPage() }}">
                    {!! Form::close() !!}
                    <div class="btn-group" role="group">
                        <a  class="btn btn-warning btn-sm" href="{{ url('/cg_admin/product_sub_categories/'.$product_sub_category->id.'/edit?page='.$product_categories->currentPage()) }}">Edit</a>
                            <a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                More <span class="caret"></span>
                            </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if($product_sub_category->products()->count() )
                                 <a  class="dropdown-item" onclick="deleteLocked{{$product_sub_category->id}}()" href="">Locked</a>
                                 @else
                                 <a  class="dropdown-item" onclick="deleteConfirm{{$product_sub_category->id}}()" href="">Delete</a>
                                 @endif
                       </div>
                </div>

                </td>
            </tr>
            <script>
                @foreach($product_category->product_sub_categories as $product_sub_category)
                function deleteConfirm{{$product_sub_category->id}}() {
                    event.preventDefault();
                    if (confirm('Are you sure you want to delete this ?'))
                    {
                        document.getElementById("deleteForm{{$product_sub_category->id}}").submit();
                    }
                    else {}
                }
                function deleteLocked{{$product_sub_category->id}}() {
                    event.preventDefault();
                    alert('Has many Relations')
                }
                @endforeach
            </script>
            @php $i= $i+1; @endphp
         @endforeach
            @endforeach

            </tbody>
        </table>

        <nav aria-label="Page navigation example">
           {{ $product_categories->links() }}
        </nav>

    </div>

@endsection
