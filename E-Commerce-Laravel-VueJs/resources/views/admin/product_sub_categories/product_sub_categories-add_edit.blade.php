@extends('layouts.admin-app')

@section('content')
    <div class="row">
     <div class="col-md-12">
         <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
             <h1 class="h2">Product Sub Categories</h1>
             <div class="btn-toolbar mb-2 mb-md-0">
                 <div class="btn-group mr-2">
                     <a type="button" href="{{ url('/cg_admin/product_sub_categories?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                 </div>

             </div>
         </div>
          @if(isset($product_sub_category))

             {{ Form::model($product_sub_category, array('route' => array('product_sub_categories.update', $product_sub_category->id),
                      'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

         @else
                {{ Form::open(array('url' => 'cg_admin/product_sub_categories','data-parsley-validate','novalidate',
                         'files'=>true)) }}
            @endif

                <input type="hidden" value="{{ request()->get('page') }}" name="page">
         <div class="form-group">
             <label for="product_category">Product Category</label>
             <select class="form-control" id="product_category" name="product_category">
                 @foreach($product_categories as $product_category)
                 <option value="{{ $product_category->id }}" @if(isset($product_sub_category) && $product_sub_category->product_category_id == $product_category->id) selected @endif>{{ $product_category->name }}</option>
                @endforeach
             </select>
         </div>
            <div class="form-group">
                <label for="name">Name</label>
               <input type="text" id="name" class="form-control" value="@if(isset($product_sub_category)){{ $product_sub_category->name }}@endif" name="name">
            </div>
         <div class="form-group">
             <label for="sort">Sort</label>
             <input type="text" id="sort" class="form-control" value="@if(isset($product_sub_category)){{ $product_sub_category->sort }} @else{{ $sort }} @endif" name="sort">
         </div>
         <div class="form-group">
             <label for="">Properties</label><br>
             @foreach($properties as $property)
                 <input type="checkbox" id="property{{ $property->id  }}" value="{{ $property->id  }}" name="properties[]" @if(isset($product_sub_category)) {{in_array($property->id,$assignedProperties)?'checked':''}} @endif>
                 <label for="property{{ $property->id  }}">{{ $property->name }}</label> &nbsp;&nbsp;
             @endforeach
         </div>
         <div class="form-group">
             <label for="">Application Supports</label><br>
             @foreach($application_supports as $application_support)
                 <input type="checkbox" id="application_support{{ $application_support->id  }}" value="{{ $application_support->id  }}" name="application_supports[]" @if(isset($product_sub_category)) {{in_array($application_support->id,$assignedApplicationSupports)?'checked':''}} @endif>
                 <label for="application_support{{ $application_support->id  }}">{{ $application_support->name }}</label> &nbsp;&nbsp;
             @endforeach
         </div>
         <div class="form-group">
             <label for="">File Types</label><br>
             @foreach($file_types as $file_type)
                 <input type="checkbox" id="file_type{{ $file_type->id  }}" name="file_types[]" value="{{ $file_type->id  }}" @if(isset($product_sub_category)) {{in_array($file_type->id,$assignedFileTypes)?'checked':''}} @endif>
                 <label for="file_type{{ $file_type->id  }}">{{ $file_type->name }}</label> &nbsp;&nbsp;
             @endforeach
         </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="1" @if(isset($product_sub_category) && $product_sub_category->status ==1) selected @endif>Active</option>
                    <option value="0" @if(isset($product_sub_category) && $product_sub_category->status ==0) selected @endif>In Active</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}


        </div>
    </div>

@endsection
