<div class="row">
    <div class="col-md-12 mb-4">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">Total Purchase <span class="badge badge-success badge-pill">{{ $purchases->total() }}</span></li>
        </ul>
    </div>
    <div class="col-md-12">
    <table class="table table-sm table-hover table-striped">
        <tbody>
        @foreach($purchases as $purchase)
        <tr>
            <td>
                <a href="{{ url('/cg_admin/products/'.$purchase->product->id) }}"><h5 class="pt-2 mb-0">{{ $purchase->product->name }}</h5></a>  <small class="float-right">Purchased on {{ $purchase->date_added }}</small>
                <p class="mb-2"><a href="{{ url($purchase->product->author->profile_url) }}" target="_blank" class="badge badge-warning mr-2"> {{ $purchase->product->author->user_profile->store_name }}</a> <span class="badge badge-info">{{ $purchase->product->product_category->name }} >> {{ $purchase->product->product_sub_category->name }}</span> <a href="{{ url($purchase->product->product_url) }}" target="_blank" class="badge badge-dark mr-2"> View In Store</a></p>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div class="col-md-12 mb-4">
        {{ $purchases->links() }}
    </div>
</div>
