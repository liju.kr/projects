<div class="row">
    <div class="col-md-12">
        <table class="table table-sm table-hover table-striped">

            <tbody>
            <tr>
                <td width="30%">Profile Image</td>
                <td width="70%"><img src="{{ $user->user_profile->image_path }}" width="100" height="100" class="img-thumbnail"></td>
            </tr>
            <tr>
                <td>User ID</td>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <td>User Name</td>
                <td>{{ $user->user_name }}</td>
            </tr>
            <tr>
                <td>First Name</td>
                <td>{{ $user->name }}</td>
            </tr>
            @if($user->user_profile->last_name)
            <tr>
                <td>Last Name</td>
                <td>{{ $user->user_profile->last_name }}</td>
            </tr>
            @endif
                <tr>
                    <td>Email Address</td>
                    <td>{{ $user->email }}</td>
                </tr>
            <tr>
            <tr>
                <td>Status</td>
                <td>         {{ Form::model($user, array('url' => '/cg_admin/users/'.$type.'/'.$user->id.'/enable_disable',
                          'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                    <input type="hidden" name="status" value="{{ $user->status }}">
                    @if($user->status ==1)
                        <button type="submit"  class="badge-success badge">Active</button>
                    @else
                        <button type="submit" class="badge-danger badge">InActive</button>
                    @endif
                    {!! Form::close() !!}</td>
            </tr>
                <td>Role</td>
                <td class="text-capitalize">
                        @if($user->is_affiliate) <span class="text-capitalize badge-primary badge">Affiliate {{ $user->role }}</span> @else <span class="text-capitalize badge-warning badge">{{ $user->role }}</span> @endif
                   </td>
            </tr>
            @if($user->is_affiliate)
            <tr>
                <td>Affiliate Code</td>
                <td>{{ $user->affiliate_code }}</td>
            </tr>
            @endif
            @if($user->user_profile->payout_email)
                <tr>
                    <td>Store Name</td>
                    <td><a href="{{ url($user->profile_url) }}" target="_blank">{{ $user->user_profile->store_name }}</a></td>
                </tr>
            @endif
            @if($user->user_profile->payout_email)
                <tr>
                    <td>Payout Email</td>
                    <td>{{ $user->user_profile->payout_email }}</td>
                </tr>
            @endif
            @if($user->user_profile->affiliate_payout_email)
                <tr>
                    <td>Affiliate Payout Email</td>
                    <td>{{ $user->user_profile->affiliate_payout_email }}</td>
                </tr>
            @endif
            @if($user->user_profile->contact_email)
                <tr>
                    <td>Contact Email</td>
                    <td>{{ $user->user_profile->contact_email }}</td>
                </tr>
            @endif
            @if($user->user_profile->city)
                <tr>
                    <td>City</td>
                    <td>{{ $user->user_profile->city }}</td>
                </tr>
            @endif
            @if($user->user_profile->state)
                <tr>
                    <td>State</td>
                    <td>{{ $user->user_profile->state }}</td>
                </tr>
            @endif
            @if($user->user_profile->country)
                <tr>
                    <td>Country</td>
                    <td>{{ $user->user_profile->country }}</td>
                </tr>
            @endif
            @if($user->user_profile->website_link)
                <tr>
                    <td>Website</td>
                    <td>{{ $user->user_profile->website_link }}</td>
                </tr>
            @endif
            @if($user->user_profile->facebook)
                <tr>
                    <td>Facebook</td>
                    <td>{{ $user->user_profile->facebook }}</td>
                </tr>
            @endif
            @if($user->user_profile->instagram)
                <tr>
                    <td>Instagram</td>
                    <td>{{ $user->user_profile->instagram }}</td>
                </tr>
            @endif
            @if($user->user_profile->twitter)
                <tr>
                    <td>Twitter</td>
                    <td>{{ $user->user_profile->twitter }}</td>
                </tr>
            @endif
            @if($user->user_profile->youtube)
                <tr>
                    <td>Youtube</td>
                    <td>{{ $user->user_profile->youtube }}</td>
                </tr>
            @endif
            @if($user->user_profile->whatsapp)
                <tr>
                    <td>Whatsapp</td>
                    <td>{{ $user->user_profile->whatsapp }}</td>
                </tr>
            @endif
            @if($user->user_profile->pinterest)
                <tr>
                    <td>Pinterest</td>
                    <td>{{ $user->user_profile->pinterest }}</td>
                </tr>
            @endif
            @if($user->user_profile->description)
                <tr>
                    <td >Description</td>
                    <td>{{ $user->user_profile->description }}</td>
                </tr>
            @endif
            @if($user->author_request && $user->author_request->created_at)
                <tr>
                    <td >Author Request Submit On</td>
                    <td >{{ date('d-m-Y, h:i A', strtotime($user->author_request->created_at)) }}</td>
                </tr>
            @endif
            @if($user->author_request && $user->author_request->author_update_on)
                <tr>
                    <td>Author Request Approved On</td>
                    <td>{{ date('d-m-Y, h:i A', strtotime($user->author_request->author_update_on)) }}</td>
                </tr>
            @endif
            @if($user->author_request)
            <tr>
                <td>Author Request Status</td>
                <td> @if($user->author_request->status==1)<span class="badge-success badge">Approved</span>@elseif($user->author_request->status==0)<span class="badge-danger badge">Pending</span>@elseif($user->author_request->status==2)<span class="badge-danger badge">Rejected</span>@endif</td>
            </tr>
            @endif
            @if($user->author_request && $user->author_request->message)
                <tr>
                    <td>Author Request Message By Admin </td>
                    <td>{{ $user->author_request->message }}</td>
                </tr>
            @endif
            @if($user->author_request && $user->author_request->admin_id)
                <tr>
                    <td>Author Request Last Reviewed By</td>
                    <td>{{ $user->author_request->admin->name }} || {{ $user->author_request->admin->role }} || On {{ date('d-m-Y, h:i A', strtotime($user->author_request->updated_on)) }}</td>
                </tr>
            @endif
            <tr>
                <td>Registered On</td>
                <td>{{ date('d-m-Y, h:i A', strtotime($user->created_at)) }}</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>
