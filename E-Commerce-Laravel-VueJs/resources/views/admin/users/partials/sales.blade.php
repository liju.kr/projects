<div class="row">
    <div class="col-md-12 mb-4">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">Total Sales <span class="badge badge-success badge-pill">{{ $sales->total() }}</span></li>
        </ul>
    </div>
    <div class="col-md-12">
    <table class="table table-sm table-hover table-striped">
        <tbody>
        @foreach($sales as $sale)
        <tr>
            <td>
                <a href="{{ url('/cg_admin/products/'.$sale->product->id) }}"><h5 class="pt-2 mb-0">{{ $sale->product->name }}</h5></a>  <small class="float-right">Sale on {{ $sale->date_added }}</small>
                <p class="mb-2"><a href="{{ url('/cg_admin/users/'.$sale->purchase_user->role.'/'.$sale->purchase_user->id) }}" class="badge badge-warning mr-2"> {{ $sale->purchase_user->name }}</a> <span class="badge badge-info">{{ $sale->product->product_category->name }} >> {{ $sale->product->product_sub_category->name }}</span> <a href="{{ url($sale->product->product_url) }}" target="_blank" class="badge badge-dark mr-2"> View In Store</a></p>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div class="col-md-12 mb-4">
        {{ $sales->links() }}
    </div>
</div>
