 <div class="row">
        <div class="col-md-12">

            <div class="card bg-light mb-4">
                <div class="card-header">
                    Statistics</div>
                <div class="card-body">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-3">

                            <h5 class="card-title">$000</h5>
                            <p class="text-muted"><small>Total Sales</small></p>
                        </div>
                        <div class="col-md-3">

                            <h5 class="card-title">$000</h5>
                            <p class="text-muted"><small>Spend</small></p>
                        </div>
                        <div class="col-md-3">

                            <h5 class="card-title">$000</h5>
                            <p class="text-muted"><small>Earned</small></p>
                        </div>
                        <div class="col-md-3">

                            <h5 class="card-title">$000</h5>
                            <p class="text-muted"><small>Commission</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            @if($user->is_author)
                <span class="badge badge-info"> {{ $active_products_count }} Live Products</span>
                <span class="badge badge-danger">{{ $sales_count }} Sales</span>
            @endif
                <span class="badge badge-success">{{ $purchase_count }} Purchases</span>
            @if($user->is_affiliate)
                <span class="badge badge-warning">{{ $user->affiliate_users()->count() }} Affiliate Users</span>
            @endif
        </div>
        <div class="col-md-12">
            <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Recent Activities</h5>
            <table class="table table-sm table-hover table-striped">
                <tbody>
                @foreach($notifications as $notification)
                <tr>
                    <td>
                        <strong> <a href="{{ url('/cg_admin/users/'.$notification->user->role.'/'.$notification->user->id) }}" target="_blank"> {{ $notification->user->name }} </a> </strong>{{ $notification->message }} <strong> @if($notification->type !='follow')<a href="{{ url('/cg_admin/products/'.$notification->product->id) }}" target="_blan">  {{ $notification->product->name }} </a> @endif  @if($notification->type =='follow')<a href=""> Profile </a> @endif</strong>. <br>
                        {{ $notification->date_added }} ( {{ date('d-m-Y, h:i A', strtotime($notification->created_at)) }} )
                    </td>
                </tr>
               @endforeach
                </tbody>
            </table>
        </div>
     <div class="col-md-12 mb-4">
         {{ $notifications->links() }}
     </div>
    </div>

