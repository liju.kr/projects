<div class="row">
    <div class="col-md-12 mb-4">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">Live Products <span class="badge badge-success badge-pill">{{ $active_products->total() }}</span></li>
        </ul>
    </div>
    @foreach($active_products as $active_product)
        <div class="col-sm-4 mb-1">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ url('/cg_admin/products/'.$active_product->id) }}">{{ $active_product->name }}</a></h5>
                    <p class="text-muted"><small>{{ $active_product->product_category->name }} >> {{ $active_product->product_sub_category->name }}</small></p>
                    <p class="card-text"><span class="badge badge-warning mr-1">Sales {{ $active_product->downloads_count }}</span>
                        <span class="badge badge-primary mr-1">Price ${{ $active_product->downloads()->sum('price') }}</span>
                        <span class="badge badge-info mr-1">Total Sales Amount ${{ $active_product->downloads()->sum('price') }}</span> <br>
                        <a class="badge badge-dark mr-1" href="{{ url($active_product->product_url) }}" target="_blank">View In Store</a>
                    </p>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-md-12 mb-4">
        {{ $active_products->links() }}
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">Pending Products <span class="badge badge-warning badge-pill">{{ $pending_products->total() }}</span></li>
        </ul>
    </div>
    @foreach($pending_products as $pending_product)
        <div class="col-sm-4 mb-1">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="{{ url('/cg_admin/products/'.$pending_product->id) }}">{{ $pending_product->name }}</a><br>
                    </h5>
                    <p class="text-muted"><small>{{ $pending_product->product_category->name }} >> {{ $pending_product->product_sub_category->name }}</small></p>
                    <p class="card-text">
                        <span class="badge badge-primary mr-1">Price ${{ $pending_product->downloads()->sum('price') }}</span> <a class="badge badge-dark mr-1" href="{{ url($pending_product->product_url) }}" target="_blank">View In Store</a>
                       </p>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-md-12 mb-4">
        {{ $pending_products->links() }}
    </div>
</div>


<div class="row">
    <div class="col-md-12 mb-4">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">Rejected Products <span class="badge badge-danger badge-pill">{{ $rejected_products->total() }}</span></li>
        </ul>
    </div>
    @foreach($rejected_products as $rejected_product)
        <div class="col-sm-4 mb-1">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ url('/cg_admin/products/'.$rejected_product->id) }}">{{ $rejected_product->name }}</a></h5>
                    <p class="text-muted"><small>{{ $rejected_product->product_category->name }} >> {{ $rejected_product->product_sub_category->name }}</small></p>
                    <p class="card-text">
                        <span class="badge badge-primary mr-1">Price ${{ $rejected_product->downloads()->sum('price') }}</span> <a class="badge badge-dark mr-1" href="{{ url($rejected_product->product_url) }}" target="_blank">View In Store</a>
                    </p>

                </div>
            </div>
        </div>
    @endforeach
    <div class="col-md-12 mb-4">
        {{ $rejected_products->links() }}
    </div>
</div>

