@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">{{ $page_title }}</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
			<form action="" method="get" class="form-inline">
              <div class="form-group mx-sm-3 mb-2">
                  <select class="form-control" name="key" required>
                      <option @if($key && $key == 'name') selected @endif value="name">Name</option>
                      @if($type != 'user')
                      <option @if($key && $key == 'store_name') selected @endif value="store_name">Store Name</option>
                          @endif
                  </select>
              </div>
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" name="key_word" @if($key_word) value="{{ $key_word }}" @endif placeholder="Key Word" required>
                </div>
            <button type="submit" class="btn btn-primary mb-2">Search</button>
        </form>

		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">
		<div class="col-md-12">

			<div class="table-responsive">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>E-Mail</th>
							<th>Account Created On</th>
                            <th>Role</th>
							<th>Status</th>
							<th><p class="text-right m-0 pr-4">Action</p></th>
						</tr>
					</thead>
					<tbody>
                    @foreach($users as $key1=>$user)
						<tr>
                            <td scope="row">{{(($users->currentpage()-1)*$users->perpage())+$key1+1}}</td>
							<td><p>{{ $user->name }} @if($user->is_author) <br> <a href="{{ url($user->profile_url) }}">{{ $user->user_profile->store_name }}</a> @endif</p></td>
							<td><p>{{ $user->email }}</p></td>
							<td><p> {{ date('j M, Y', strtotime($user->created_at)) }}</p></td>
							<td>@if($type !='affiliate')
                                    @if($user->is_affiliate) <span class="text-capitalize badge-primary badge">Affiliate {{ $user->role }}</span> @else <span class="text-capitalize badge-warning badge">{{ $user->role }}</span> @endif
                                    @else
                                    @if($user->is_user) <span class="badge-primary badge">{{ $user->role }}</span> @elseif($user->is_author) <span class="text-capitalize badge-warning badge">{{ $user->role }}</span> @endif
                                @endif
                            <td>
                                {{ Form::model($user, array('url' => '/cg_admin/users/'.$type.'/'.$user->id.'/enable_disable',
                          'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                                <input type="hidden" name="page" value="{{ $users->currentPage() }}">
                                <input type="hidden" name="status" value="{{ $user->status }}">
                                @if($user->status ==1)
                                    <button type="submit"  class="badge-success badge">Active</button>
                                @else
                                    <button type="submit" class="badge-danger badge">InActive</button>
                                @endif
                                {!! Form::close() !!}
                            </td>

							<td>
								<div class="btn-group float-right" role="group">
									<a  class="btn btn-primary btn-sm" href="{{ url('/cg_admin/users/'.$type.'/'.$user->id) }}">View</a>
									<a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
										More <span class="caret"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item"  href="#" >Deactivate Account</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
        <nav aria-label="Page navigation example">
            @if($key && $key_word)
            {{$users->appends(['key' => $key, 'key_word' => $key_word])->links()}}
        @else
            {{$users->links()}}
        @endif
        </nav>
	</div>
</div>
@endsection
