@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<div>
		<h1 class="h2">{{ $user->name }} @if($user->is_author)<a target="_blank" href="{{ url($user->profile_url) }}" class="badge badge-info">{{ $user->user_profile->store_name }}</a>@endif </h1>
		<p>@if($user->status)<span class="badge-success badge">Active</span>@else<span class="badge-danger badge">In Active</span>@endif  @if($user->is_affiliate) <span class="text-capitalize badge-primary badge">Affiliate {{ $user->role }}</span> @else <span class="text-capitalize badge-warning badge">{{ $user->role }}</span> @endif</p>
	</div>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">

			<div class="btn-group float-right" role="group">
				<a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					More <span class="caret"></span>
				</a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"  href="#" >Deactivate Account</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">
		<div class="col-md-12">

			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/profile') }}" class="nav-link @if($tab==null || $tab=="profile") active @endif">Profile</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/messages') }}" class="nav-link @if($tab=="messages") active @endif">Messages</a>
				</li>
                @if($user->is_author)
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/products') }}" class="nav-link @if($tab=="products") active @endif">Products</a>
				</li>
                    <li class="nav-item">
                        <a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/sales') }}" class="nav-link @if($tab=="sales") active @endif">Sales History</a>
                    </li>
                @endif
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/purchase') }}" class="nav-link @if($tab=="purchase") active @endif">Purchase History</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/chat') }}" class="nav-link @if($tab=="chat") active @endif">Chat History</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/cg_admin/users/'.$type.'/'.$user->id.'/details') }}" class="nav-link @if($tab=="details") active @endif">Details</a>
				</li>
			</ul>
			<div class="tab-content py-4">
                @if($tab==null || $tab=="profile")
                    @include('admin.users.partials.profile')
                @elseif($tab=="messages")
                    @include('admin.users.partials.messages')
                @elseif($tab=="products" && $user->is_author)
                    @include('admin.users.partials.products')
                @elseif($tab=="purchase")
                    @include('admin.users.partials.purchase')
                @elseif($tab=="sales")
                    @include('admin.users.partials.sales')
                @elseif($tab=="chat")
                    @include('admin.users.partials.chat')
                @elseif($tab=="details")
                    @include('admin.users.partials.details')
                @else

                @endif

                    </div>
						</div>
						{{--         <div class="col-lg-4 order-lg-1 text-center">
							<img src="//placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="avatar">
							<h6 class="mt-2">Upload a different photo</h6>
							<label class="custom-file">
								<input type="file" id="file" class="custom-file-input">
								<span class="custom-file-control">Choose file</span>
							</label>
						</div> --}}

					</div>
				</div>
				@endsection
