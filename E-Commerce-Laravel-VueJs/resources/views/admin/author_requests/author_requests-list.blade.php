@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Author Requests</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
{{--                <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
            </div>

        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">User</th>
                <th scope="col">Role</th>
                <th scope="col">Status</th>
                <th scope="col">Request On</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($author_requests as $key=>$author_request)
            <tr>
                <td scope="row">{{(($author_requests->currentpage()-1)*$author_requests->perpage())+$key+1}}</td>
                <td scope="row">{{ $author_request->user->name }}</td>
                <td scope="row" class="text-capitalize">{{ $author_request->user->role }}</td>
                  <td scope="row">
                  @if($author_request->status ==1)
                      <a href="" class="badge-success badge">Approved</a>
                      @elseif($author_request->status ==0)
                        <a href="" class="badge-warning badge">Pending</a>
                    @elseif($author_request->status ==2)
                        <a href="" class="badge-danger badge">Rejected</a>
                      @else
                        <a href="" class="badge-danger badge">{{ $author_request->status }}</a>
                      @endif
                </td>
                <td scope="row">{{ date('j M Y', strtotime($author_request->created_at)) }}</td>

                <td scope="row">
                    {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/author_requests/'.$author_request->id.'/delete','id' => 'deleteForm'.$author_request->id, ]) !!}
                    <input type="hidden" name="page" value="{{ $author_requests->currentPage() }}">
                    {!! Form::close() !!}
                    <div class="btn-group" role="group">
                        <a  class="btn btn-primary btn-sm" href="{{ url('/cg_admin/users/'.$author_request->user->role.'/'.$author_request->user->id.'/details') }}">View</a>
                            <a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                More <span class="caret"></span>
                            </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                             <a class="dropdown-item"  href="{{ url('/cg_admin/author_requests/'.$author_request->id.'/review?page='.$author_requests->currentPage()) }}" >Change Status</a>
                             <a  class="dropdown-item" onclick="deleteConfirm{{$author_request->id}}()" href="">Delete</a>
                       </div>
                </div>
                </td>
            </tr>
         @endforeach
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
           {{ $author_requests->links() }}
        </nav>

    </div>
    <script>
        @foreach($author_requests as $author_request)
        function deleteConfirm{{$author_request->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this product?'))
            {
                document.getElementById("deleteForm{{$author_request->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$author_request->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach

    </script>
@endsection
