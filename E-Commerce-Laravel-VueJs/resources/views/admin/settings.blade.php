@extends('layouts.admin-app')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Settings</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        {{--                <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
                    </div>

                </div>
            </div>
<form>
  <div class="form-group">
    <label for="customFile">Site Logo</label>
    <div class="custom-file">
	  <input type="file" class="custom-file-input" id="customFile">
	  <label class="custom-file-label" for="customFile">Choose file</label>
	</div>
    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
  </div>

  <div class="form-group">
    <label for="customFile3">Meta Image</label>
    <div class="custom-file">
	  <input type="file" class="custom-file-input" id="customFile3">
	  <label class="custom-file-label" for="customFile3">Choose file</label>
	</div>
    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
  </div>


  <div class="form-group">
    <label for="">Twitter handle</label>
	<input type="text" class="form-control" id="" placeholder=" @publisher_handle">
  </div>


  <div class="form-group">
    <label for="customFile2">Preloader</label>
     <div class="custom-file">
	  <input type="file" class="custom-file-input" id="customFile2">
	  <label class="custom-file-label" for="customFile2">Choose file</label>
	</div>
  </div>

  <div class="form-group">
    <label for="">Theme Color</label>
	<input type="text" class="form-control" id="" placeholder="#6036FB">
  </div>

<div class="form-group">
    <label for="">Custom CSS</label>
    <textarea name="" id="" class="form-control lined" cols="30" rows="10"></textarea>

</div>


  <button type="submit" class="btn btn-primary">Save</button>
</form>

<br><br>

<div class="accordion" id="accordionExample">

  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Home Page Meta
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
<form>


  <div class="form-group">
    <label for="">Title</label>
	<input type="text" class="form-control" id="" placeholder="Maximum length 60-70 characters">
	<small id="emailHelp" class="form-text text-muted">Maximum length 60-70 characters</small>
  </div>

  <div class="form-group">
    <label for="">Description</label>
	<input type="text" class="form-control" id="" placeholder="Page description. No longer than 155 characters">
	<small id="emailHelp" class="form-text text-muted">Page description. No longer than 155 characters</small>
  </div>

  <div class="form-group">
    <label for="">keywords</label>
	<input type="text" class="form-control" id="" placeholder="keywords separated by comma ">
  </div>




  <button type="submit" class="btn btn-primary">Save</button>
</form>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Shop Page Meta
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
<form>


  <div class="form-group">
    <label for="">Title</label>
	<input type="text" class="form-control" id="" placeholder="Maximum length 60-70 characters">
	<small id="emailHelp" class="form-text text-muted">Maximum length 60-70 characters</small>
  </div>

  <div class="form-group">
    <label for="">Description</label>
	<input type="text" class="form-control" id="" placeholder="Page description. No longer than 155 characters">
	<small id="emailHelp" class="form-text text-muted">Page description. No longer than 155 characters</small>
  </div>

  <div class="form-group">
    <label for="">keywords</label>
	<input type="text" class="form-control" id="" placeholder="keywords separated by comma ">
  </div>




  <button type="submit" class="btn btn-primary">Save</button>
</form>
      </div>
    </div>
  </div>
</div>
</div>
</div>



@endsection


@push('footer_scripts')


@endpush
