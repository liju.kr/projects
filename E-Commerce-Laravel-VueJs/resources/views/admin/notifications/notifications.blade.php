@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 mb-2">Notifications</h1>
</div>
<div class="fluid-container">
  <div class="row">
    <div class="col-md-5">
      
      <h4 class="mb-2">Send Notification</h4>
      <form action="">
        <div class="form-group">
          <select class="form-control" id="">
            <option>All</option>
            <option>Authors</option>
            <option>Affiliates</option>
            <option>Users</option>
          </select>
        </div>
        <div class="form-group">
          <textarea class="form-control" id="" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Send</button> <br>
      </form>
    </div>
    <div class="col-md-7">
      <h4>Default Notifications</h4>
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Welcome Messages <span class="badge badge-success">Active</span></h5>
          <p class="text-muted"><span class="badge badge-info">All</span></p>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" data-toggle="modal" data-target="#editModal" class="btn btn-primary">Edit</button>
        </div>
      </div>
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Purchase Notification <span class="badge badge-success">Active</span></h5>
          <p class="text-muted"><span class="badge badge-info">Authors</span></p>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" data-toggle="modal" data-target="#editModal" class="btn btn-primary">Edit</button>
        </div>
      </div>
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Affilitaion purchase Notification <span class="badge badge-success">Active</span></h5>
          <p class="text-muted"><span class="badge badge-info">Affiliation</span></p>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" data-toggle="modal" data-target="#editModal" class="btn btn-primary">Edit</button>
        </div>
      </div>
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Purchased Product Updation <span class="badge badge-success">Active</span></h5>
          <p class="text-muted"><span class="badge badge-info">Users</span></p>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button type="button" data-toggle="modal" data-target="#editModal" class="btn btn-primary">Edit</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Purchased Product Updation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
          <div class="form-group">
            <input type="text" class="form-control" aria-describedby="">
          </div>
          <div class="form-group">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
              <label class="form-check-label" for="inlineCheckbox1">Users</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
              <label class="form-check-label" for="inlineCheckbox2">Authors</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
              <label class="form-check-label" for="inlineCheckbox3">Affiliates</label>
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control" id="" rows="3"></textarea>
          </div>
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Disable Notification</label>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection