@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Products</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                {{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
{{--                <form action="" method="get" class="form-inline">--}}
{{--                    <div class="form-group mx-sm-3 mb-2">--}}
{{--                        <select class="form-control" name="key" required>--}}
{{--                            <option @if($key && $key == 'product_name') selected @endif value="product_name">Product Name</option>--}}
{{--                            <option @if($key && $key == 'store_name') selected @endif value="store_name">Store Name</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="form-group mx-sm-3 mb-2">--}}
{{--                        <input type="text" class="form-control" name="key_word" @if($key_word) value="{{ $key_word }}" @endif  placeholder="Key Word" required>--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn btn-primary mb-2">Search</button>--}}
{{--                </form>--}}
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Purchased By</th>
                <th scope="col">Author</th>
                <th scope="col">Affiliate</th>
                <th scope="col">Market</th>
                <th scope="col">Sale On</th>


            </tr>
            </thead>
            <tbody>
            @foreach($sales as $key1=>$sale)
            <tr>
                <td scope="row">{{(($sales->currentpage()-1)*$sales->perpage())+$key1+1}}</td>
                <td scope="row"><a href="{{ url($sale->product->product_url) }}" target="_blank"> {{ $sale->product->name }}</a></td>
                <td scope="row"> ${{ $sale->total_price }}</td>
                <td scope="row"> <a href="{{ url('/cg_admin/users/'.$sale->purchase_user->role.'/'.$sale->purchase_user->id) }}" target="_blank"> {{ $sale->purchase_user->name }} </a></td>
                </td>
                <td scope="row">
                    ${{ ($sale->total_price / 100)*$sale->invoice->author_fee }} ({{ $sale->invoice->author_fee }}%) <br>
                    <a href="{{ url('/cg_admin/users/'.$sale->product->author->role.'/'.$sale->product->author->id) }}" target="_blank"> {{ $sale->product->author->user_profile->store_name }} @if($sale->product->author->affiliate_status) (Affiliate) @endif </a></td>
                <td scope="row">
                    ${{ ($sale->total_price / 100)*$sale->invoice->affiliate_fee }} ({{ $sale->invoice->affiliate_fee }}%) <br>
                  @if($sale->affiliate_id)  <a href="{{ url('/cg_admin/users/'.$sale->affiliate_user->role.'/'.$sale->affiliate_user->id) }}" target="_blank"> {{ $sale->affiliate_user->name }} @if($sale->affiliate_user->is_author) (Author) @endif </a>  @endif</td>
                </td>
                <td scope="row">
                    ${{ ($sale->total_price / 100)*$sale->invoice->market_fee }} ({{ $sale->invoice->market_fee }}%) <br>
               </td>
                <td scope="row">
                    {{ date('j M, Y, h:i A', strtotime($sale->created_at)) }}
                </td>

            </tr>
         @endforeach
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
        @if(false)  {{--    @if($key && $key_word)--}}
                {{$sales->appends(['key' => $key, 'key_word' => $key_word])->links()}}
            @else
                {{$sales->links()}}
            @endif
        </nav>

    </div>
    <script>
        @foreach($sales as $sale)
        function deleteConfirm{{$sale->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this product?'))
            {
                document.getElementById("deleteForm{{$sale->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$sale->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach

    </script>
@endsection
