@extends('layouts.admin-app')

@section('content')

    <!-- Portfolio Item Heading -->
    <div class="row">

        <div class="col-md-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Affiliate Request Detail</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a type="button" href="{{ url('/cg_admin/affiliate_requests?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-10">

            <h1 class="my-4">
                {{ $affiliate_request->user->name }}
                <span class="badge badge-primary text-capitalize">{{ $affiliate_request->user->role }}</span>
            </h1>
            <p  class="my-4">
                @if($affiliate_request->status ==1)
                    <a href="" class="badge-success badge">Approved</a>
                @elseif($affiliate_request->status ==0)
                    <a href="" class="badge-warning badge">Pending</a>
                @elseif($affiliate_request->status ==2)
                    <a href="" class="badge-danger badge">Rejected</a>
                @else
                    <a href="" class="badge-danger badge">{{ $affiliate_request->status }}</a>
                @endif
            </p>
        </div>
        <div class="col-md-2">
            <div class="dropdown my-4">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/affiliate_requests/'.$affiliate_request->id.'/delete','id' => 'deleteForm'.$affiliate_request->id, ]) !!}
                <input type="hidden" name="page" value="{{ request()->get('page') }}">
                {!! Form::close() !!}
                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                     <a class="dropdown-item"  href="{{ url('/cg_admin/affiliate_requests/'.$affiliate_request->id.'/review?page='.request()->get('page')) }}" >Change Status</a>
                    <a  class="dropdown-item" onclick="deleteConfirm{{$affiliate_request->id}}()" href="">Delete</a>

                </div>
            </div>

        </div>
    </div>
    <!-- Portfolio Item Row -->
    <div class="row">

        <div class="col-md-8">
         <h3>Social Media Links</h3>
            {{ $affiliate_request->social_media_links }}

            <br>

            <h3>Description</h3>
            {{ $affiliate_request->description }}
            @if($affiliate_request->message)
                <hr>
                <h3>Reason For Rejection / Pending </h3>
                {{ $affiliate_request->message }}
        @endif
        </div>



        <div class="col-md-4">
            <table class="table">
                <tbody>
               <tr>
                    <td>Affiliate Payout Email</td>
                    <td>{{ $affiliate_request->payout_email }}</td>
                </tr>
                @if($affiliate_request->website)
               <tr>
                   <td>Website</td>
                   <td>{{ $affiliate_request->website }}</td>
               </tr>
            @endif
               <tr>
                   <td>Date Requested</td>
                   <td>{{ date('j M Y', strtotime($affiliate_request->created_at)) }}</td>
               </tr>

                @if($affiliate_request->admin_id)
                    <tr>
                        <td>Date Updated</td>
                        <td>{{ date('j M Y', strtotime($affiliate_request->updated_at)) }}</td>
                    </tr>
                    <tr>
                        <td>Last Reviewed By</td>
                        <td>{{ $affiliate_request->admin->name }}</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>

    </div>
    <!-- /.row -->

    <script>
        function deleteConfirm{{$affiliate_request->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this product?'))
            {
                document.getElementById("deleteForm{{$affiliate_request->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$affiliate_request->id}}() {
            event.preventDefault();
           alert('Has many Relations')
        }

    </script>

@endsection


