@extends('layouts.admin-app')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Change Affiliate Request Status</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a type="button" href="{{ url('/cg_admin/affiliate_requests?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                    </div>

                </div>
            </div>

            <h6 class="text-capitalize"> {{ $affiliate_request->user->name }} || {{ $affiliate_request->user->role }}</h6>

            {{ Form::model($affiliate_request, array('url' => '/cg_admin/affiliate_requests/'.$affiliate_request->id.'/review',
                           'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                  <input type="hidden" value="{{ request()->get('page') }}" name="page">
            <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="0" @if($affiliate_request->status ==0) selected @endif>Pending</option>
                        <option value="1" @if($affiliate_request->status ==1) selected @endif>Approved</option>
                        <option value="2" @if($affiliate_request->status ==2) selected @endif>Rejected</option>
                        <option value="3" @if($affiliate_request->status ==3) selected @endif>Disabled</option>
                    </select>
                </div>

            <div class="form-group">
                <label for="message">Reason For Rejection</label>
                <textarea rows="3" class="form-control" name="message" id="message" placeholder="Reason For Rejection / Pending"></textarea>
            </div>

            <div class="form-group">
                <label for="mail_message">Reason For Rejection (Mail)</label>
                <textarea rows="3" class="form-control" name="mail_message" id="mail_message" placeholder="Reason For Rejection"></textarea>
            </div>

                <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}


        </div>
    </div>



@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('mail_message', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });

        CKEDITOR.replace('mail_message', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",

            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });
    </script>
