@extends('layouts.admin-app')
@section('content')
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Support</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
			<form class="form-inline">
				<div class="form-group mx-sm-3 mb-2">
					<input type="text" class="form-control" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-primary mb-2">Search</button>
			</form>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">
		<div class="col-md-12">
			<div class="mail-box">
				<aside class="sm-side">
					{{-- <div class="user-head">
						<a class="inbox-avatar" href="javascript:;">
							<img  width="64" hieght="60" src="http://bootsnipp.com/img/avatars/ebeb306fd7ec11ab68cbcaa34282158bd80361a7.jpg">
						</a>
						<div class="user-name">
							<h5><a href="#">Alireza Zare</a></h5>
							<span><a href="#">Info.Ali.Pci@Gmail.com</a></span>
						</div>
						<a class="mail-dropdown pull-right" href="javascript:;">
							<i class="fa fa-chevron-down"></i>
						</a>
					</div> --}}
					<ul class="inbox-nav inbox-divider">
						<li class="active">
							<a href="#"><i class=""><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-inbox-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M3.81 4.063A1.5 1.5 0 0 1 4.98 3.5h6.04a1.5 1.5 0 0 1 1.17.563l3.7 4.625a.5.5 0 0 1-.78.624l-3.7-4.624a.5.5 0 0 0-.39-.188H4.98a.5.5 0 0 0-.39.188L.89 9.312a.5.5 0 1 1-.78-.624l3.7-4.625z"/>
  <path fill-rule="evenodd" d="M.125 8.67A.5.5 0 0 1 .5 8.5h5A.5.5 0 0 1 6 9c0 .828.625 2 2 2s2-1.172 2-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .496.562l-.39 3.124a1.5 1.5 0 0 1-1.489 1.314H1.883a1.5 1.5 0 0 1-1.489-1.314l-.39-3.124a.5.5 0 0 1 .121-.393z"/>
</svg></i> All <span class="label label-danger pull-right">24566</span></a>
						</li>
						<li>
							<a href=""><i><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-app-indicator" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M5.5 2A3.5 3.5 0 0 0 2 5.5v5A3.5 3.5 0 0 0 5.5 14h5a3.5 3.5 0 0 0 3.5-3.5V8a.5.5 0 0 1 1 0v2.5a4.5 4.5 0 0 1-4.5 4.5h-5A4.5 4.5 0 0 1 1 10.5v-5A4.5 4.5 0 0 1 5.5 1H8a.5.5 0 0 1 0 1H5.5z"/>
  <path d="M16 3a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
</svg></i> Pending <span class="label label-danger pull-right">222</span></a>
						</li>
						<li>
							<a href="#"><i class="text-success"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
</svg></i> Sent <span class="label label-danger pull-right">434</span></a>
						</li>
						<li>
							<a href="#"><i class="text-danger">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<circle cx="8" cy="8" r="8"/>
								</svg>
							</i> High Priority</a>
						</li>
						<li>
							<a href="#"><i class="text-warning">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<circle cx="8" cy="8" r="8"/>
								</svg>
							</i> Medium Priority</a>
						</li>
						<li>
							<a href="#"><i class="text-info">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<circle cx="8" cy="8" r="8"/>
								</svg>
							</i> Low Priority</a>
						</li>
						<li>
							<a href="#"><i class=" fa fa-external-link"></i> Default Messages <span class="label label-info pull-right">30</span></a>
						</li>
					</ul>
					
				</aside>
				<aside class="lg-side">
					{{-- <div class="inbox-head">
						<h3>Inbox</h3>
						<form action="#" class="pull-right position">
							<div class="input-append">
								<input type="text" class="sr-input" placeholder="Search Mail">
								<button class="btn sr-btn" type="button"><i class="fa fa-search"></i></button>
							</div>
						</form>
					</div> --}}
					<div class="inbox-body">
						<div class="mail-option">
							
							<div class="btn-group hidden-phone">
								<a data-toggle="dropdown" href="#" class="btn mini blue" aria-expanded="false">
									Label
									<i class="fa fa-angle-down "></i>
								</a>
								<ul class="dropdown-menu p-1">
									<li><a href="#"><i class="text-danger">
										<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<circle cx="8" cy="8" r="8"/>
										</svg>
									</i> High</a></li>
									<li><a href="#"><i class="text-warning">
										<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<circle cx="8" cy="8" r="8"/>
										</svg>
									</i> Medium </a></li>
									<li><a href="#"><i class="text-info">
										<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<circle cx="8" cy="8" r="8"/>
										</svg>
									</i> Low </a></li>
								</ul>
							</div>
							<div class="btn-group">
								<a data-toggle="dropdown" href="#" class="btn mini blue">
									Assign to
									<i class="fa fa-angle-down "></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
									<li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
								</ul>
							</div>
							<ul class="unstyled inbox-pagination">
								<li><span>1-50 of 234</span></li>
								<li>
									<a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
								</li>
								<li>
									<a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
								</li>
							</ul>
						</div>
						<table class="table table-inbox table-hover">
							<tbody>
								<tr class="unread">
									<td class="inbox-small-cells">
										<input type="checkbox" class="mail-checkbox">
									</td>
									<td class="inbox-small-cells"></td>
									<td class="view-message  dont-show">UserName</td>
									<td class="view-message "><a href="/cg_admin/support/detail">Added a new class: Login Class Fast Site</a></td>
									<td class="view-message  inbox-small-cells"><i class="text-success">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<circle cx="8" cy="8" r="8"/>
								</svg></i></td>
									<td class="view-message  text-right">9:27 AM</td>
								</tr>
								<tr class="unread">
									<td class="inbox-small-cells">
										<input type="checkbox" class="mail-checkbox">
									</td>
									<td class="inbox-small-cells"></td>
									<td class="view-message dont-show">StoreName </td>
									<td class="view-message"><a href="/cg_admin/support/detail">Improve the search presence of WebSite</a></td>
									<td class="view-message inbox-small-cells"><i class="text-danger">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<circle cx="8" cy="8" r="8"/>
								</svg></i></td>
									<td class="view-message text-right">March 15</td>
								</tr>
								<tr class="">
									<td class="inbox-small-cells">
										<input type="checkbox" class="mail-checkbox">
									</td>
									<td class="inbox-small-cells"></td>
									<td class="view-message dont-show">JW Player</td>
									<td class="view-message"><a href="/cg_admin/support/detail">Last Chance: Upgrade to Pro for </a></td>
									<td class="view-message inbox-small-cells"></td>
									<td class="view-message text-right">March 15</td>
								</tr>
							</tbody>
						</table>


					</div>
				</aside>
			</div>
		</div>
	</div>
</div>
@endsection