@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<div>
	<h1 class="h2">
<span class="text-danger" data-toggle="tooltip" data-placement="top" title="High Priority"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <circle cx="8" cy="8" r="8"/>
</svg></span>
	User Name / User Name</h1>
	<p><small class="badge badge-primary mr-1">StoreName</small>
	<small class="badge badge-info mr-1">StoreName</small></p>
</div>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			
			<div class="btn-group float-right" role="group">
				<a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					More <span class="caret"></span>
				</a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"  href="#" target="_blank">View Store</a>
					<a class="dropdown-item"  href="#" target="_blank">View Account</a>
					<a class="dropdown-item"  href="#" >Block Users Communication</a>
					<a class="dropdown-item"  href="#" >Mark as Unread</a>
					<a class="dropdown-item"  href="#" >Mark as Closed</a>
					    <div class="dropdown-divider"></div>
					<a class="dropdown-item"  href="#" >High Priority</a>
					<a class="dropdown-item"  href="#" >Medium Priority</a>
					<a class="dropdown-item"  href="#" >Low Priority</a>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="message_detail">
	
	<div class="mesgs">
		<div class="msg_history">
			<div class="incoming_msg">
				<div class="received_msg">
					<div class="received_withd_msg">
						<p>Test which is a new approach to have all
						solutions</p>
						<span class="time_date"> 11:01 AM    |    June 9</span></div>
					</div>
				</div>
				<div class="outgoing_msg">
					<div class="sent_msg">
						<p>Test which is a new approach to have all
						solutions</p>
						<span class="time_date"> 11:01 AM    |    June 9</span> </div>
					</div>
					<div class="incoming_msg">
						
						<div class="received_msg">
							<div class="received_withd_msg">
								<p>Test, which is a new approach to have</p>
								<span class="time_date"> 11:01 AM    |    Yesterday</span></div>
							</div>
						</div>
					<div class="incoming_msg">
						
						<div class="received_msg">
							<div class="received_withd_msg">
								<p>Test, which is a new approach Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam rerum, consequatur dolore. Dolor libero delectus dolore, ut eaque, nostrum corporis, dolores vel asperiores beatae quae voluptates. Aliquid veniam maxime suscipit.to have</p>
								<span class="time_date"> 11:01 AM    |    Yesterday</span></div>
							</div>
						</div>
						<div class="outgoing_msg">
							<div class="sent_msg">
								<p>Apollo University, Delhi, India Test</p>
								<span class="time_date"> 11:01 AM    |    Today</span> </div>
							</div>
							<div class="incoming_msg">
								
								<div class="received_msg">
									<div class="received_withd_msg">
										<p>We work directly with our designers and suppliers,
											and sell direct to you, which means quality, exclusive
										products, at a price anyone can afford.</p>
										<span class="time_date"> 11:01 AM    |    Today</span></div>
									</div>
								</div>
					<div class="incoming_msg">
						
						<div class="received_msg">
							<div class="received_withd_msg">
								<p>Test, which is a new approach to have</p>
								<span class="time_date"> 11:01 AM    |    Yesterday</span></div>
							</div>
						</div>
							</div>
							<div class="type_msg">
								<div class="input_msg_write">
									<input type="text" class="write_msg" placeholder="Type a message" />
									<button class="msg_send_btn" type="button"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
										<path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
										<path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
									</svg></button>
								</div>
							</div>
						</div>
					</div>
					@endsection


@push('footer_scripts')

@endpush