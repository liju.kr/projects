@extends('layouts.admin-app')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">SEO Meta Tags</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a type="button" href="{{ url('/cg_admin/products?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                    </div>

                </div>
            </div>

            <h6> {{ $product->name }} || {{ $product->author->user_profile->store_name }}</h6>

            {{ Form::model($product, array('url' => '/cg_admin/products/'.$product->id.'/seo',
                           'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
            <input type="hidden" value="{{ request()->get('page') }}" name="page">

            <div class="form-group">
                <label for="meta_title">Meta Title</label>
                <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" value="{{ $product->meta_tag->meta_title }}">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="3" class="form-control" name="meta_description" id="meta_description" placeholder="Meta Description">{{ $product->meta_tag->meta_description }}</textarea>
            </div>

            <div class="form-group">
                <label for="meta_key_words">Meta Key Words</label>
                <textarea rows="3" class="form-control" name="meta_key_words" id="meta_key_words" placeholder="Meta Key Words">{{ $product->meta_tag->meta_key_words }}</textarea>
            </div>

            <div class="form-group">
                @if($product->meta_tag->meta_image) <img src="{{ $product->meta_tag->meta_image_path }}" class="img-thumbnail">  @endif
                <div class="custom-file">
                    <input type="file" name="meta_image" class="custom-file-input" id="meta_image">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}


        </div>
    </div>



@endsection

