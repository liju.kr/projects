@extends('layouts.admin-app')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Change Product Status</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a type="button" href="{{ url('/cg_admin/products?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                    </div>

                </div>
            </div>

            <h6> {{ $product->name }} || {{ $product->author->user_profile->store_name }}</h6>

            {{ Form::model($product, array('url' => '/cg_admin/products/'.$product->id.'/review',
                           'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                  <input type="hidden" value="{{ request()->get('page') }}" name="page">
            <div class="form-group">
                    <label for="status">Status </label>
                    <select class="form-control" name="status" id="status">
                        <option value="0" @if(old('status') == $product->status && old('status') == 0) selected @endif @if(old('status') != $product->status && $product->status == 0) selected @endif  @if(old('status') != $product->status && old('status') == 0) selected @endif>Pending</option>
                        <option value="1" @if(old('status') == $product->status && old('status') == 1) selected @endif @if(old('status') != $product->status && $product->status == 1) selected @endif  @if(old('status') != $product->status && old('status') == 1) selected @endif>Live</option>
                        <option value="2" @if(old('status') == $product->status && old('status') == 2) selected @endif @if(old('status') != $product->status && $product->status == 2) selected @endif  @if(old('status') != $product->status && old('status') == 2) selected @endif>Rejected</option>
                    </select>
                <input type="hidden" value="{{ $product->product_category->extract_status }}" name="extract_status">
                </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label>Include In Subscriptions</label><br>
                    <input type="radio" id="subscriptions_status_0" name="subscriptions_status" value="0" @if(old('subscriptions_status') == 0) checked @else @if($product->offer_status->subscriptions_status == 0) checked @endif @endif >
                    <label for="subscriptions_status_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="subscriptions_status_1" name="subscriptions_status" value="1"  @if(old('subscriptions_status') == 1) checked @else @if($product->offer_status->subscriptions_status == 1) checked @endif @endif>
                    <label for="subscriptions_status_1">Yes</label> &nbsp;&nbsp;
                </div>
                <div class="col-md-6">
                    <label>Author Can Edit</label><br>
                    <input type="radio" id="subscriptions_edit_0" name="subscriptions_edit" value="0"  @if(old('subscriptions_edit') == 0) checked @else @if($product->offer_status->subscriptions_edit == 0) checked @endif @endif>
                    <label for="subscriptions_edit_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="subscriptions_edit_1" name="subscriptions_edit" value="1" @if(old('subscriptions_edit') == 1) checked @else @if($product->offer_status->subscriptions_edit == 1) checked @endif @endif>
                    <label for="subscriptions_edit_1">Yes</label> &nbsp;&nbsp;

                </div>
                    </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label>Include In Freebies</label><br>
                    <input type="radio" id="freebies_status_0" name="freebies_status" value="0" @if(old('freebies_status') == 0) checked @else @if($product->offer_status->freebies_status == 0) checked @endif @endif>
                    <label for="freebies_status_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="freebies_status_1" name="freebies_status" value="1" @if(old('freebies_status') == 1) checked @else @if($product->offer_status->freebies_status == 1) checked @endif @endif>
                    <label for="freebies_status_1">Yes</label> &nbsp;&nbsp;
                </div>
                <div class="col-md-6">
                    <label>Author Can Edit</label><br>
                    <input type="radio" id="freebies_edit_0" name="freebies_edit" value="0" @if(old('freebies_edit') == 0) checked @else @if($product->offer_status->freebies_edit == 0) checked @endif @endif>
                    <label for="freebies_edit_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="freebies_edit_1" name="freebies_edit" value="1" @if(old('freebies_edit') == 1) checked @else @if($product->offer_status->freebies_edit == 1) checked @endif @endif>
                    <label for="freebies_edit_1">Yes</label> &nbsp;&nbsp;

                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label>Include In Offers</label><br>
                    <input type="radio" id="offers_status_0" name="offers_status" value="0" @if(old('offers_status') == 0) checked @else @if($product->offer_status->offers_status == 0) checked @endif @endif>
                    <label for="offers_status_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="offers_status_1" name="offers_status" value="1" @if(old('offers_status') == 1) checked @else @if($product->offer_status->offers_status == 1) checked @endif @endif>
                    <label for="offers_status_1">Yes</label> &nbsp;&nbsp;
                </div>
                <div class="col-md-6">
                    <label>Author Can Edit</label><br>
                    <input type="radio" id="offers_edit_0" name="offers_edit" value="0" @if(old('offers_edit') == 0) checked @else @if($product->offer_status->offers_edit == 0) checked @endif @endif>
                    <label for="offers_edit_0">No</label> &nbsp;&nbsp;
                    <input type="radio" id="offers_edit_1" name="offers_edit" value="1" @if(old('offers_edit') == 1) checked @else @if($product->offer_status->offers_edit == 1) checked @endif @endif>
                    <label for="offers_edit_1">Yes</label> &nbsp;&nbsp;

                </div>
            </div>

            <div class="form-group">
                <label for="message">Reason For Rejection</label>
                <textarea rows="3" class="form-control" name="message" id="message" placeholder="Reason For Rejection">@if(old('message')) {{ old('message') }} @else Your Product {{ $product->name }} has been Rejected @endif</textarea>
            </div>

            <div class="form-group">
                <label for="mail_message">Reason For Rejection (Mail)</label>
                <textarea rows="3" class="form-control" name="mail_message" id="mail_message" placeholder="Reason For Rejection">{{ old('mail_message') }}</textarea>
            </div>

            @if($product->product_category->extract_status && !$product->font_previews()->count())
            <div class="form-group">
                <label for="font_files">Font Files (TTF)</label>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dynamic_field">
                        <tr>
                            <td><input type="file" name="font_files[]"  class="form-control name_list" /></td>
                            <td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            @endif
            <div class="form-group">
                <label for="status_changed_on">Status Changed On</label>
                <input type="datetime-local" class="form-control" name="status_changed_on" value="{{ old('status_changed_on') }}" id="status_changed_on">
            </div>

                <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}


        </div>
    </div>

@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('mail_message', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });

        CKEDITOR.replace('mail_message', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });
    </script>
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var i=1;

            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="file" name="font_files[]"  class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
            });


            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
        });
    </script>

@endpush
