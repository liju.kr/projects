@extends('layouts.admin-app')

@section('content')

    <!-- Portfolio Item Heading -->
    <div class="row">
        <div class="col-md-12">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Product Details</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group mr-2">
                    <a type="button" href="{{ url('/cg_admin/products?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                </div>

            </div>
        </div>
       </div>

        <div class="col-md-10">

            <h1 class="my-4">
                {{ $product->name }}
                <a href="{{ url($product->author->profile_url) }}" target="_blank"> <span class="badge badge-dark">{{ $product->author->user_profile->store_name }}</span></a>
            </h1>
            <p  class="my-4">
                <a href="{{ url($product->product_url) }}" target="_blank" > <span class="badge badge-info">View In Store</span></a>

                <a href="" target="_blank" > <span class="badge badge-primary">View User Profile</span></a>
            </p>
        </div> <div class="col-md-2">
            <div class="dropdown my-4">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                {!! Form::open(['method' => 'DELETE', 'url' => ['delete', $product->id],'id' => 'deleteForm'.$product->id, ]) !!}
                <input type="hidden" name="page">
                {!! Form::close() !!}
                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    @if(Auth::user()->isSuperAdmin()||Auth::user()->isAdmin())
                    <a class="dropdown-item" href="{{ url('/cg_admin/download_by_admin?i='.$product->id."&j=".uniqid()) }}" target="_blank">Download File</a>
                    <a class="dropdown-item" href="{{ url('/cg_admin/products/'.$product->id.'/review?page='.request()->get('page')) }}">Change Status</a>
                     <a class="dropdown-item" href="{{ url('/cg_admin/products/'.$product->id.'/edit?page='.request()->get('page')) }}">Edit Product</a>
                    <a class="dropdown-item" onclick="deleteConfirm{{$product->id}}()" href="">Delete Product</a>
                    @endif
                    @if(Auth::user()->isSuperAdmin()||Auth::user()->isAdmin()||Auth::user()->isMarketingAdmin())
                    <a class="dropdown-item" href="{{ url('/cg_admin/products/'.$product->id.'/seo?page='.request()->get('page')) }}">SEO Meta Tags</a>
                    @endif


                </div>
            </div>

        </div>
    </div>
    <!-- Portfolio Item Row -->
    <div class="row">

        <div class="col-md-7">
            <img class="img-fluid" src="{{ $product->cover_photo_original_path }}" alt="{{ $product->name }}">

 <!-- Related Projects Row -->
    @if($product->preview_photos->count())
    <h3 class="my-4">Preview Images</h3>
    <div class="row">
        @php $i=1 @endphp
     @foreach($product->preview_photos as $preview_photo)
        <div class="col-md-3 col-sm-6 mb-4">
                <img class="img-fluid" src="{{ $preview_photo->preview_photo_path }}" alt="{{ $product->name }}">
        </div>
         @php
         if($i >= 4) break;
             $i = $i + 1; @endphp
        @endforeach
    </div>
        <!-- Button trigger modal -->
 @if($product->preview_photos->count() > 4)
<button type="button" class="btn btn-light btn-sm ml-3" data-toggle="modal" data-target="#exampleModal">
  View All Preview Images
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        @foreach($product->preview_photos as $preview_photo)

            <div class="col-md-3 col-sm-6 mb-4">
            <a href="#">
                <img class="img-fluid" src="{{ $preview_photo->preview_photo_path }}" alt="{{ $product->name }}">
            </a>
        </div>
        @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
 @endif
            @endif
        </div>

        <div class="col-md-5">
            <h3 class="my-3">Product Details</h3>
            <table class="table">
                <tbody>
{{--                    <tr>--}}
{{--                        <td>Sales</td>--}}
{{--                        <td><p class="mb-0"><span class="badge-primary badge">2340</span> <br><span class="badge-info badge">$345666</span></p></td>--}}
{{--                            <td></td>--}}
{{--                    </tr>--}}
{{--                <tr>--}}
                    <td>Status</td>
                    <td>
                        @if($product->status ==1)
                            <a href="" class="badge-success badge">Live</a>
                        @elseif($product->status ==0)
                            <a href="" class="badge-warning badge">Pending</a>
                        @elseif($product->status ==2)
                            <a href="" class="badge-danger badge">Rejected</a>
                        @else
                            <a href="" class="badge-danger badge">{{ $product->status }}</a>
                        @endif</td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td>{{ $product->product_sub_category->product_category->name }}</td>
                </tr>
                <tr>
                    <td>Sub Category</td>
                    <td>{{ $product->product_sub_category->name }}</td>
                </tr>
                <tr>
                    <td>File type</td>
                    <td>{{ $product->file_type }}</td>
                </tr>
                <tr>
                    <td>File Size</td>
                    <td>{{ $product->file_size }}</td>
                </tr>
                <tr>
                    <td>Include In Subscriptions</td>
                    <td>@if($product->offer_status->subscriptions_status)  <a href="" class="badge-success badge">Yes</a> @else  <a href="" class="badge-danger badge">No</a> @endif  - @if($product->offer_status->subscriptions_edit)  <small class="text-success">Author Can Edit</small> @else <small class="text-danger">Author Can't Edit </small> @endif </td>
                </tr>
                <tr>
                    <td>Include In Freebies</td>
                    <td>@if($product->offer_status->freebies_status) <a href="" class="badge-success badge">Yes</a> @else <a href="" class="badge-danger badge">No</a> @endif  - @if($product->offer_status->freebies_edit)  <small class="text-success">Author Can Edit</small> @else <small class="text-danger">Author Can't Edit </small> @endif </td>
                </tr>
                <tr>
                    <td>Include In Offers</td>
                    <td>@if($product->offer_status->offers_status) <a href="" class="badge-success badge">Yes</a> @else <a href="" class="badge-danger badge">No</a> @endif  - @if($product->offer_status->offers_edit)  <small class="text-success">Author Can Edit</small> @else <small class="text-danger">Author Can't Edit </small>@endif </td>
                </tr>
                <tr>
                    <td>Date Added</td>
                    <td>{{ $product->date_added }}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        {{ Form::model($product, array('url' => '/cg_admin/products/'.$product->id.'/enable_disable',
                          'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                        <input type="hidden" name="page" value="{{ request()->get('page') }}">
                        <input type="hidden" name="enable_status" value="{{ $product->enable_status }}">
                        @if($product->enable_status ==1)
                            <button type="submit"  class="badge-success badge">Enabled</button>
                        @else
                            <button type="submit" class="badge-danger badge">Disabled</button>
                        @endif
                        {!! Form::close() !!}
                    </td>
                </tr>
            @if($product->date_updated)
                <tr>
                    <td>Date Updated</td>
                    <td>{{ $product->date_updated }}</td>
                </tr>
            @endif
            @if($product->getActiveUpdatesCount())
                <tr>
                    <td>Live Updates</td>
                    <td><a href="{{ url('/cg_admin/product_updates/'.$product->id.'/active') }}" class="badge-success badge">{{ $product->getActiveUpdatesCount() }}</a></td>
                </tr>
            @endif
            @if($product->getPendingUpdatesCount())
                <tr>
                    <td>Pending Updates</td>
                    <td><a href="{{ url('/cg_admin/product_updates/'.$product->id.'/pending') }}" class="badge-warning badge">{{ $product->getPendingUpdatesCount() }}</a></td>
                </tr>
            @endif
            @if($product->getRejectedUpdatesCount())
                <tr>
                    <td>Pending Updates</td>
                    <td><a href="{{ url('/cg_admin/product_updates/'.$product->id.'/rejected') }}"  class="badge-danger badge">{{ $product->getRejectedUpdatesCount() }}</a></td>
                </tr>
            @endif
                @if($product->status_changed_on)
                    <tr>
                        <td>Last Reviewed On</td>
                        <td>{{ date('j M Y h:i A', strtotime($product->status_changed_on)) }}</td>
                    </tr>
                @endif
                    @if($product->admin_id)
                    <tr>
                        <td>Last Reviewed By</td>
                        <td>{{ $product->admin->name }}</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>

        <div class="col-md-12">

            <h3 class="my-3">Product Description</h3>
            <p>{!! $product->description !!}</p>


            @if($product->meta_tag->meta_title)
            <h3 class="my-3">Meta Title</h3>
            <p>{{ $product->getMetaTitle() }} </p>
            @endif
            @if($product->meta_tag->meta_description)
            <h3 class="my-3">Meta Description</h3>
            <p>{{ $product->getMetaDescription() }} </p>
            @endif
            @if($product->meta_tag->meta_key_words)
                <h3 class="my-3">Meta Key Words</h3>
                <p>{{ $product->getMetaKeyWords() }} </p>
            @endif
            @if($product->meta_tag->meta_image)
            <h3 class="my-3">Meta Image</h3>
            <p> <img class="img-fluid" src="{{ $product->getMetaImage() }}" alt="{{ $product->getMetaTitle() }}"></p>
            @endif
        </div>


    </div>
    <!-- /.row -->
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Purchase History</h5>--}}
{{--                            <table class="table table-sm table-hover table-striped">--}}
{{--                                <tbody>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <strong>Abby</strong> joined ACME Project Team in <strong>`Collaboration`</strong>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <strong>Gary</strong> deleted My Board1 in <strong>`Discussions`</strong>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <strong>Kensington</strong> deleted MyBoard3 in <strong>`Discussions`</strong>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <strong>John</strong> deleted My Board1 in <strong>`Discussions`</strong>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <strong>Skell</strong> deleted his post Look at Why this is.. in <strong>`Discussions`</strong>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- /.row -->

    <script>
        function deleteConfirm{{$product->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this product?'))
            {
                document.getElementById("deleteForm{{$product->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$product->id}}() {
            event.preventDefault();
           alert('Has many Relations')
        }

    </script>

@endsection


