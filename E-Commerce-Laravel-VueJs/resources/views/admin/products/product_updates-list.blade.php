@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Product Updates</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                  <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-outline-dark mb-2">Back</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">Name</th>
                <th scope="col">Store</th>
                <th scope="col">Status</th>
                <th scope="col">Update</th>
                <th scope="col">By</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product_updates as $key1=>$product_update)
            <tr  @if($product_update->status ==1) style="background-color: palegreen" @endif>
                <td scope="row">{{(($product_updates->currentpage()-1)*$product_updates->perpage())+$key1+1}}</td>
                <td scope="row"><a href="{{ url($product_update->product->product_url) }}" target="_blank"> {{ $product_update->product->name }}</a></td>
                <td scope="row"><a href="{{ url($product_update->product->author->profile_url) }}" target="_blank"> {{ $product_update->product->author->user_profile->store_name }} </a></td>
                <td scope="row">
                  @if($product_update->status ==1)
                      <a href="" class="badge-success badge">Live</a>
                      @elseif($product_update->status ==0)
                        <a href="" class="badge-warning badge">Pending</a>
                    @elseif($product_update->status ==2)
                        <a href="" class="badge-danger badge">Rejected</a>
                      @else
                        <a href="" class="badge-danger badge">{{ $product_update->status }}</a>
                      @endif

                </td>

                <td scope="row">
                    {{ Form::model($product_update, array('url' => '/cg_admin/product_updates/'.$product_update->id.'/edit',
          'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                    <input type="hidden" value="{{ request()->get('page') }}" name="page">
                    <input type="hidden" value="{{ $product_update->product_id }}" name="product_id">
                    <div class="row">
                        <div class="col-7">
                            <div class="custom-file">
                                <input type="file" required="required" accept="application/zip" name="product_file" class="custom-file-input" id="product_file">
                                <label class="custom-file-label"  for="product_file">Choose File</label>
                            </div>
                        </div>
                        <div class="col-5">
                            <input type="submit" class="btn btn-warning" value="Update">
                        </div>
                    </div>
                    {{ Form::close() }}
                </td>
                <td scope="row">
                    Requested On : {{ date('j M, Y h:i A', strtotime($product_update->created_at)) }}
                        @if($product_update->approved_on)  <br> Approved  On :  {{ date('j M, Y h:i A', strtotime($product_update->approved_on)) }}   @endif
                        @if($product_update->admin_id) <br> Last Updated By : {{ $product_update->admin->name }}  @endif

                </td>
                <td scope="row">
                    {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/product_updates/'.$product_update->id.'/delete','id' => 'deleteForm'.$product_update->id, ]) !!}
                    <input type="hidden" name="page" value="{{ $product_updates->currentPage() }}">
                    {!! Form::close() !!}
                    <div class="btn-group" role="group">
                        <a  class="btn btn-primary btn-sm" href="#Modal{{ $product_update->id }}"  type="button" data-toggle="modal" data-target="#ModalEdit{{ $product_update->id }}">Log</a>
                            <a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                More <span class="caret"></span>
                            </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url('/cg_admin/update_download_by_admin?i='.$product_update->id."&j=".uniqid()) }}" target="_blank"  >Download</a>
                             <a class="dropdown-item"  href="{{ url('/cg_admin/product_updates/'.$product_update->id.'/review?page='.$product_updates->currentPage()) }}" >Change Status</a>
                             <a class="dropdown-item"  href="{{ url('/cg_admin/products/'.$product_update->product->id) }}" >View</a>
                             <a  class="dropdown-item" onclick="deleteConfirm{{$product_update->id}}()" href="">Delete</a>
                       </div>
                </div>
                </td>
            </tr>
            <!-- Modal -->
            <div class="modal mod_ck_editor fade" id="ModalEdit{{ $product_update->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $product_update->id }}" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="topicModal">Change Log</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        {{ $product_update->change_logs }}
                        </div>
                    </div>
                </div>
            </div>
         @endforeach
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
                {{$product_updates->links()}}
        </nav>

    </div>
    <script>
        @foreach($product_updates as $product_update)
        function deleteConfirm{{$product_update->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this?'))
            {
                document.getElementById("deleteForm{{$product_update->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$product_update->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach

    </script>
@endsection
