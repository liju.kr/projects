@extends('layouts.admin-app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Edit Product</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        @if($product->product_file)  <a class="btn-sm btn btn-success mb-2" href="{{ url('/cg_admin/download_by_admin?i='.$product->id."&j=".uniqid()) }}" target="_blank"  >Download</a> <br> @endif
                    </div>
                    <div class="btn-group mr-2">
                        <a type="button" href="{{ url('/cg_admin/products?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                    </div>

                </div>
            </div>

            <h6> {{ $product->name }} || {{ $product->author->user_profile->store_name }}</h6>

            {{ Form::model($product, array('url' => '/cg_admin/products/'.$product->id.'/edit',
                           'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
            <input type="hidden" value="{{ request()->get('page') }}" name="page">

            <div class="form-group">
                <div class="row">
                    <div class="col-7">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $product->name }}" required>
                    </div>
                    <div class="col-2">
                        <label for="product_price">Price (USD)</label>
                        <input type="number" min="0" class="form-control" name="product_price" id="product_price"  value="{{ $product->price }}" required>
                    </div>
                    <div class="col-3">
                        <label for="product_license">License</label>
                        <select class="form-control" name="product_license" id="product_license" required>
                            <option value="" @if($product->product_license =="") selected @endif>Select License</option>
                            <option value="0" @if($product->product_license == 0) selected @endif>Single</option>
                            <option value="1" @if($product->product_license == 1) selected @endif>Multiple</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-5">
                        <label for="product_category">Category</label>
                        <input type="text" class="form-control" name="product_category" id="product_category" value="{{ $product->product_category->name }}" readonly>
                    </div>
                    <div class="col-5">
                        <label for="product_sub_category">Sub Category</label>
                        <label for="product_sub_category">Category</label>
                        <input type="text" class="form-control" name="product_sub_category" id="product_sub_category" value="{{ $product->product_sub_category->name }}" readonly>
                    </div>
                    <div class="col-2">
                        <label for=""> Edit Categories</label>
                    <input type="button" class="btn-outline-dark btn" data-toggle="modal" data-target="#ModalEditCategory" value="Click To Change">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        @if($product->product_sub_category->properties()->count())
                            <label for="">  <strong>Properties</strong></label><br>
                                @foreach($product->product_sub_category->properties as $property)
                                    <input type="checkbox" id="property{{ $property->id  }}" value="{{ $property->id  }}" name="properties[]"  {{in_array($property->id,$assignedProperties)?'checked':''}}>
                                    <label for="property{{ $property->id  }}">{{ $property->name }}</label> &nbsp;&nbsp;
                                @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        @if($product->product_sub_category->file_types()->count())
                            <label for="">  <strong>File Types</strong></label><br>
                            @foreach($product->product_sub_category->file_types as $file_type)
                                <input type="checkbox" id="file_type{{ $file_type->id  }}" value="{{ $file_type->id  }}" name="file_types[]"  {{in_array($file_type->id,$assignedFileTypes)?'checked':''}}>
                                <label for="file_type{{ $file_type->id  }}">{{ $file_type->name }}</label> &nbsp;&nbsp;
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        @if($product->product_sub_category->application_supports()->count())
                            <label for="">  <strong>Application Supports</strong></label><br>
                            @foreach($product->product_sub_category->application_supports as $application_support)
                                <input type="checkbox" id="application_support{{ $property->id  }}" value="{{ $application_support->id  }}" name="application_supports[]"  {{in_array($application_support->id,$assignedApplicationSupports)?'checked':''}}>
                                <label for="application_support{{ $application_support->id  }}">{{ $application_support->name }}</label> &nbsp;&nbsp;
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-4">
                        <label for="tag_line">Tag Line</label>
                        <input type="text" class="form-control" name="tag_line" id="tag_line"  value="{{ $product->tag_line }}" required>
                    </div>
                    <div class="col-4">
                        <label for="live_preview_link">Live Preview Link</label>
                        <input type="url" class="form-control" name="live_preview_link" id="live_preview_link"  value="{{ $product->external_url }}">
                    </div>
                    <div class="col-4">
                        <label for="product_tags">Product Tags (Comma Separated)</label>
                        <input type="text" class="form-control" name="product_tags" id="product_tags"  value="{{ $product->product_tags }}" required>
                    </div>
                </div>

            </div>


            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                <label for="description">Description</label>
                <textarea rows="5" class="form-control" name="description" id="description" required>{{ $product->description }}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-6">
                        @if($product->product_file) <img src="{{ asset('images/zip_preview.png') }}" class="img-thumbnail m-1" style="height: 100px;">  @endif
                <div class="custom-file">
                    <input type="file" name="product_file" accept="application/zip" class="custom-file-input" id="product_file">
                    <label class="custom-file-label" for="product_file">Choose Product File (Download & Backup Before Change)</label>
                </div>
                    </div>
                    <div class="col-6">
                        @if($product->cover_photo) <img src="{{ $product->cover_photo_path }}" class="img-thumbnail m-1" style="height: 100px;">  @endif
                        <div class="custom-file">
                            <input type="file" name="cover_image" accept="image/*" class="custom-file-input" id="cover_photo">
                            <label class="custom-file-label"  for="cover_image">Choose Cover Photo</label>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}
        </div>
        <div class="col-12">
            <hr><hr>
        </div>
        <div class="col-12">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Product Preview Images</h1>
                <div class="btn-toolbar mb-2 mb-md-0">

                    <div class="btn-group mr-2">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalAddPreview" class="btn btn-sm btn-outline-secondary mb-2">Add</button>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Photo</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product->preview_photos as $preview_photo)
                            <tr>
                                <td scope="row"> <img class="img-fluid" style="height: 100px;" src="{{ $preview_photo->preview_photo_path }}" alt="{{ $product->name }}"></td>
                                <td scope="row">
                                    {{ Form::model($preview_photo, array('url' => '/cg_admin/product_previews/'.$preview_photo->id.'/edit',
                          'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                                    <input type="hidden" value="{{ request()->get('page') }}" name="page">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="custom-file">
                                                <input type="file" required="required" accept="image/*" name="preview_image" class="custom-file-input" id="preview_image">
                                                <label class="custom-file-label"  for="preview_image">Choose Photo</label>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                         <input type="submit" class="btn btn-warning" value="Update Preview Image">
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </td>

                                <td scope="row">
                                    {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/product_previews/'.$preview_photo->id.'/delete','id' => 'deleteForm'.$preview_photo->id, ]) !!}
                                    <input type="hidden" value="{{ request()->get('page') }}" name="page">
                                    <a  class="btn-danger btn" onclick="deleteConfirm{{$preview_photo->id}}()" href="">Permanent Delete</a>
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>

        @if($product->product_category->extract_status)
            <div class="col-12">
                <hr><hr>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Product Font Previews</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">

                        <div class="btn-group mr-2">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalAddFontPreview" class="btn btn-sm btn-outline-secondary mb-2">Add</button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">File Name</th>
                                <th scope="col">Update</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product->font_previews as $font_preview)
                                <tr>
                                    <td scope="row"> {{ $font_preview->name }}</td>
                                    <td scope="row"> {{ $font_preview->file }}</td>
                                    <td scope="row">
                                        {{ Form::model($font_preview, array('url' => '/cg_admin/font_previews/'.$font_preview->id.'/edit',
                              'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                                        <input type="hidden" value="{{ request()->get('page') }}" name="page">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="custom-file">
                                                    <input type="file" required="required" name="font_preview" class="custom-file-input" id="font_preview">
                                                    <label class="custom-file-label"  for="font_preview">Choose File (TTF)</label>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <input type="submit" class="btn btn-warning" value="Update Font Preview">
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </td>

                                    <td scope="row">
                                        {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/font_previews/'.$font_preview->id.'/delete','id' => 'deleteFormFont'.$font_preview->id, ]) !!}
                                        <input type="hidden" value="{{ request()->get('page') }}" name="page">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <a  class="btn-danger btn" onclick="deleteConfirmFont{{$font_preview->id}}()" href="">Permanent Delete</a>
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        @endif



        <div class="modal mod_ck_editor fade" id="ModalAddPreview" tabindex="-1" role="dialog" aria-labelledby="ModalAddPreview" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="topicModal">Add Preview Images</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ Form::open(array('url' => 'cg_admin/product_previews','data-parsley-validate','novalidate',
                                    'files'=>true)) }}
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" required="required" accept="image/*" name="preview_image" class="custom-file-input" id="preview_image">
                                <label class="custom-file-label" for="preview_image">Choose Photo</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal mod_ck_editor fade" id="ModalAddFontPreview" tabindex="-1" role="dialog" aria-labelledby="ModalAddFontPreview" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Font Preview (TTF)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ Form::open(array('url' => 'cg_admin/font_previews','data-parsley-validate','novalidate',
                                    'files'=>true)) }}
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" required="required" name="font_preview" class="custom-file-input" id="font_preview">
                                <label class="custom-file-label"  for="font_preview">Choose File (TTF))</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal mod_ck_editor fade" id="ModalEditCategory" tabindex="-1" role="dialog" aria-labelledby="ModalEditCategory" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Product Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ Form::model($product, array('url' => '/cg_admin/product_category/'.$product->id.'/edit',
                             'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <div class="form-group">
                            <label for="product_category">Category</label>
                            <select class="form-control" name="product_category" id="product_category" required>
                                <option value="" @if($product->product_category_id =="") selected @endif>Select Category</option>
                                @foreach($product_categories as $product_category)
                                    <option value="{{ $product_category->id }}" @if($product_category->id == $product->product_category_id) selected @endif>{{ $product_category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="product_sub_category">Sub Category</label>
                            <select class="form-control" name="product_sub_category" id="product_sub_category" required>
                                <option value="" @if($product->product_sub_category_id =="") selected @endif>Select Category</option>
                                @foreach($product_categories as $product_category)
                                    <optgroup label="{{ $product_category->name }}">
                                        @foreach($product_category->product_sub_categories as $product_sub_category)
                                            <option value="{{ $product_sub_category->id }}" @if($product_sub_category->id == $product->product_sub_category_id) selected @endif>{{ $product_sub_category->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <script>
            @foreach($product->preview_photos as $preview_photo)
            function deleteConfirm{{$preview_photo->id}}() {
                event.preventDefault();
                if (confirm('Are you sure you want to delete this?'))
                {
                    document.getElementById("deleteForm{{$preview_photo->id}}").submit();
                }
                else {}
            }
            function deleteLocked{{$preview_photo->id}}() {
                event.preventDefault();
                alert('Has many Relations')
            }
            @endforeach
        </script>

        <script>
            @foreach($product->font_previews as $font_preview)
            function deleteConfirmFont{{$font_preview->id}}() {
                event.preventDefault();
                if (confirm('Are you sure you want to delete this?'))
                {
                    document.getElementById("deleteFormFont{{$font_preview->id}}").submit();
                }
                else {}
            }
            function deleteLockedFont{{$font_preview->id}}() {
                event.preventDefault();
                alert('Has many Relations')
            }
            @endforeach
        </script>

@endsection
        @push('footer_scripts')
        <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
        <script>
            CKEDITOR.replace('description', {
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                customConfig: '{{ asset('js/ckeditor/config.js') }}'
            });

            CKEDITOR.replace('description', {
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                customConfig: '{{ asset('js/ckeditor/config.js') }}'
            });
        </script>
    @endpush
