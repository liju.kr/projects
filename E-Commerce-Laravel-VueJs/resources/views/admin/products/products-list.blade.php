@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Products</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                {{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
                <form action="" method="get" class="form-inline">
                    <div class="form-group mx-sm-3 mb-2">
                        <select class="form-control" name="key" required>
                            <option @if($key && $key == 'product_name') selected @endif value="product_name">Product Name</option>
                            <option @if($key && $key == 'store_name') selected @endif value="store_name">Store Name</option>
                        </select>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" name="key_word" @if($key_word) value="{{ $key_word }}" @endif  placeholder="Key Word" required>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">Name</th>
                <th scope="col">Store</th>
                <th scope="col">Category</th>
                <th scope="col">Status</th>
                <th scope="col">Update Status</th>
                <th scope="col">Added On</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $key1=>$product)
            <tr @if($product->status ==1) style="background-color: palegreen" @endif>
                <td scope="row">{{(($products->currentpage()-1)*$products->perpage())+$key1+1}}</td>
                <td scope="row"><a href="{{ url($product->product_url) }}" target="_blank"> {{ $product->name }}</a></td>
                <td scope="row"><a href="{{ url($product->author->profile_url) }}" target="_blank"> {{ $product->author->user_profile->store_name }} </a></td>
                <td scope="row"> {{ $product->product_category->name }} >> {{ $product->product_sub_category->name }} </td>
                <td scope="row">
                  @if($product->status ==1)
                      <a href="" class="badge-success badge">Live</a>
                      @elseif($product->status ==0)
                        <a href="" class="badge-warning badge">Pending</a>
                    @elseif($product->status ==2)
                        <a href="" class="badge-danger badge">Rejected</a>
                      @else
                        <a href="" class="badge-danger badge">{{ $product->status }}</a>
                      @endif
                    @if($product->status_changed_on)
                       <br>  {{ date('j M Y h:i A', strtotime($product->status_changed_on)) }}
                        @endif

                </td>
                <td scope="row">
                    @if($product->getPendingUpdatesCount())
                    <a href="{{ url('/cg_admin/product_updates/'.$product->id) }}" class="badge-warning badge">Request Pending</a>
                        @else
                        <a href="{{ url('/cg_admin/product_updates/'.$product->id) }}" class="badge-success badge">No Request Pending</a>
                        @endif
                </td>
                <td scope="row">{{ $product->date_added }}</td>
                <td scope="row">
                    {{ Form::model($product, array('url' => '/cg_admin/products/'.$product->id.'/enable_disable',
                            'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                    <input type="hidden" name="page" value="{{ $products->currentPage() }}">
                    <input type="hidden" name="enable_status" value="{{ $product->enable_status }}">
                    @if($product->enable_status ==1)
                        <button type="submit"  class="badge-success badge">Enabled</button>
                    @else
                        <button type="submit" class="badge-danger badge">Disabled</button>
                    @endif
                    {!! Form::close() !!}
                </td>
                <td scope="row">
                    {!! Form::open(['method' => 'DELETE', 'url' => ['delete', $product->id],'id' => 'deleteForm'.$product->id, ]) !!}
                    <input type="hidden" name="page" value="{{ $products->currentPage() }}">
                    {!! Form::close() !!}
                    <div class="btn-group" role="group">
                        <a  class="btn btn-primary btn-sm" href="{{ url('/cg_admin/products/'.$product->id.'?page='.$products->currentPage()) }}">View</a>
                            <a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                More <span class="caret"></span>
                            </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                             @if(Auth::user()->isSuperAdmin()||Auth::user()->isAdmin())
                            <a class="dropdown-item" href="{{ url('/cg_admin/download_by_admin?i='.$product->id."&j=".uniqid()) }}" target="_blank"  >Download File</a>
                             <a class="dropdown-item"  href="{{ url('/cg_admin/products/'.$product->id.'/review?page='.$products->currentPage()) }}" >Change Status</a>
                            <a class="dropdown-item"  href="{{ url('/cg_admin/products/'.$product->id.'/edit?page='.$products->currentPage()) }}" >Edit Product</a>
                            <a  class="dropdown-item" onclick="deleteConfirm{{$product->id}}()" href="">Delete Product</a>
                             @endif
                                 @if(Auth::user()->isSuperAdmin()||Auth::user()->isAdmin()||Auth::user()->isMarketingAdmin())
                            <a class="dropdown-item"  href="{{ url('/cg_admin/products/'.$product->id.'/seo?page='.$products->currentPage()) }}" >SEO Meta Tags</a>
                                 @endif
                       </div>
                </div>
                </td>
            </tr>
         @endforeach
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
            @if($key && $key_word)
                {{$products->appends(['key' => $key, 'key_word' => $key_word])->links()}}
            @else
                {{$products->links()}}
            @endif
        </nav>

    </div>
    <script>
        @foreach($products as $product)
        function deleteConfirm{{$product->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this product?'))
            {
                document.getElementById("deleteForm{{$product->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$product->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach

    </script>
@endsection
