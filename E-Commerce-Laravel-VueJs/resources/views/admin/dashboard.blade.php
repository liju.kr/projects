@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Dashboard</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            {{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}
        </div>
    </div>
</div>
<div class="fluid-container">
    <div class="row">
        @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('cg_admin/users/author') }}">Authors
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a></div>
                <div class="card-body">
                 <div class="row">
                        <div class="col-md-6">

                    <h5 class="card-title">{{ $response['authors_count'] }}</h5>
                    <p class="text-muted"><small>Active accounts</small></p>
                        </div>
                        <div class="col-md-6">

                <a href="{{ url('/cg_admin/author_requests') }}" class="{{ $response['pending_author_requests_count'] ==  '0' ? 'text-success' : 'text-danger'  }}">
                    <h5 class="card-title">{{ $response['pending_author_requests_count'] }}</h5>
                    <p class="{{ $response['pending_author_requests_count'] ==  '0' ? 'text-success' : 'text-danger'  }}"><small>Pending Request</small></p>
                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('/cg_admin/users/user') }}">Users
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a></div>
                <div class="card-body">

                 <div class="row">
                        <div class="col-md-6">

                    <h5 class="card-title">{{ $response['users_count'] }}</h5>
                    <p class="text-muted"><small>Total User accounts</small></p>
                        </div>
                        <div class="col-md-6">

                    <h5 class="card-title">{{ $response['users_count_today'] }}</h5>
                    <p class="text-muted"><small>New Today</small></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('/cg_admin/users/affiliate') }}">Affiliates
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">

                            <h5 class="card-title">{{ $response['affiliates_count'] }}</h5>
                            <p class="text-muted"><small>Active Affiliates</small></p>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('/cg_admin/affiliate_requests') }}" class="{{ $response['affiliate_requests_count'] ==  '0' ? 'text-success' : 'text-danger'  }}">
                                <h5 class="card-title">{{ $response['affiliate_requests_count'] }}</h5>
                                <p class="{{ $response['affiliate_requests_count'] ==  '0' ? 'text-success' : 'text-danger'  }}"><small>Pending Requests</small></p>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('/cg_admin/products') }}">Products
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">

                            <h5 class="card-title">{{ $response['products_count'] }}</h5>
                            <p class="text-muted"><small>Total Products</small></p>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('/cg_admin/products') }}" class="{{ $response['pending_products_count'] ==  '0' ? 'text-success' : 'text-danger'  }}">
                                <h5 class="card-title">{{ $response['pending_products_count'] }}</h5>
                                <p class="{{ $response['pending_products_count'] ==  '0' ? 'text-success' : 'text-danger'  }}"><small>Pending Requests</small></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-4">
                <div class="card bg-light mb-4">
                    <div class="card-header">
                        <a href="{{ url('/cg_admin/product_updates') }}">Product Updates
                            <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-star" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                        <path d="M7.84 4.1a.178.178 0 0 1 .32 0l.634 1.285a.178.178 0 0 0 .134.098l1.42.206c.145.021.204.2.098.303L9.42 6.993a.178.178 0 0 0-.051.158l.242 1.414a.178.178 0 0 1-.258.187l-1.27-.668a.178.178 0 0 0-.165 0l-1.27.668a.178.178 0 0 1-.257-.187l.242-1.414a.178.178 0 0 0-.05-.158l-1.03-1.001a.178.178 0 0 1 .098-.303l1.42-.206a.178.178 0 0 0 .134-.098L7.84 4.1z"/>
                    </svg>
                        </span>
                        </a></div>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-4">
                                <a href="{{ url('/cg_admin/product_updates/pending') }}" class="{{ $response['updates_pending_count'] ==  '0' ? 'text-success' : 'text-danger'  }}">
                                    <h5 class="card-title">{{ $response['updates_pending_count'] }}</h5>
                                    <p class="{{ $response['updates_pending_count'] ==  '0' ? 'text-success' : 'text-danger'  }}"><small>Pending</small></p>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('/cg_admin/product_updates/active') }}">
                                <h5 class="card-title">{{ $response['updates_active_count'] }}</h5>
                                <p class="text-muted"><small>Live</small></p>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('/cg_admin/product_updates/rejected') }}">
                                <h5 class="card-title">{{ $response['updates_rejected_count'] }}</h5>
                                <p class="text-muted"><small>Rejected</small></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('/cg_admin/sales') }}">Sales
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">

                            <h5 class="card-title">{{ $response['downloads_count'] }}</h5>
                            <p class="text-muted"><small>Total Sales</small></p>
                        </div>
                        <div class="col-md-6">

                            <h5 class="card-title text-primary">${{ $response['total_sale_amount'] }}</h5>
                            <p class="text-primary"><small>Total Amount</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card bg-light mb-4">
                <div class="card-header">
                    <a href="{{ url('/cg_admin/sales') }}">Fees (Credit to Company Account)
                        <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                    </a></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h5 class="card-title">${{ $response['total_author_fee_credit_to_company'] }}</h5>
                            <p class="text-muted"><small>Author Fee</small></p>
                        </div>
                        <div class="col-md-4">
                            <h5 class="card-title">${{ $response['total_affiliate_fee_credit_to_company'] }}</h5>
                            <p class="text-mute"><small>Affiliate Fee</small></p>
                        </div>
                        <div class="col-md-4">
                            <h5 class="card-title">${{ $response['total_market_fee_credit_to_company'] }}</h5>
                            <p class="text-mute"><small>Market Fee</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isAccountingAdmin())
                <div class="col-md-4">
                    <div class="card bg-light mb-4">
                        <div class="card-header">
                            <a href="{{ url('cg_admin/invoices') }}">Invoices
                                <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                            </a></div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="card-title">$000.00</h5>
                                    <p class="text-muted"><small>Debit</small></p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title">$000.00</h5>
                                    <p class="text-mute"><small>Credit</small></p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() || Auth::user()->isMarketingAdmin())
            <div class="col-md-4">
                <div class="card bg-light mb-4">
                    <div class="card-header">
                        <a href="{{ url('/cg_admin/products') }}">Products
                            <span class="float-right">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
                            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        </span>
                        </a></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">

                                <h5 class="card-title">{{ $response['products_count'] }}</h5>
                                <p class="text-muted"><small>Total Products</small></p>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('/cg_admin/products') }}" class="{{ $response['pending_products_count'] ==  '0' ? 'text-success' : 'text-danger'  }}">
                                    <h5 class="card-title">{{ $response['pending_products_count'] }}</h5>
                                    <p class="{{ $response['pending_products_count'] ==  '0' ? 'text-success' : 'text-danger'  }}"><small>Pending Requests</small></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
