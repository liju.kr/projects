@extends('layouts.admin-app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ $product_property_title }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a type="button" href="{{ url('/cg_admin/product_properties/'.$property_name.'/create') }}" class="btn btn-sm btn-outline-secondary mb-2">Add</a>
            </div>

        </div>
    </div>
    <!-- Portfolio Item Heading -->
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">SL.No</th>
                <th scope="col">Name</th>
                <th scope="col">SVG Icon</th>
                <th scope="col">Default Status</th>
                <th scope="col">Status</th>
                <th scope="col">Added On</th>
                <th scope="col">Last Updated By</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product_properties as $key=>$product_property)
            <tr>
                <td scope="row">{{(($product_properties->currentpage()-1)*$product_properties->perpage())+$key+1}}</td>
                <td scope="row"> {{ $product_property->name }}</td>
                <td scope="row"> {!! $product_property->svg !!}</td>
                 <td scope="row">
                  @if($product_property->default_status ==1)
                      <a href="" class="badge-success badge">Default</a>
                      @else
                        <a href="" class="badge-danger badge">Not Default</a>
                      @endif
                </td>
                <td scope="row">
                    @if($product_property->status ==1)
                        <a href="" class="badge-success badge">Active</a>
                    @else
                        <a href="" class="badge-danger badge">In Active</a>
                    @endif
                </td>
                <td scope="row"> {{ date('d-m-y h:i A', strtotime($product_property->created_at)) }}</td>
                <td scope="row">{{ $product_property->admin->name }} <br>{{ date('d-m-y h:i A', strtotime($product_property->updated_at)) }}</td>
                <td scope="row">

                    {!! Form::open(['method' => 'DELETE', 'url' => '/cg_admin/product_properties/'.$property_name.'/'.$product_property->id.'/delete','id' => 'deleteForm'.$product_property->id, ]) !!}
                    <input type="hidden" name="page" value="{{ $product_properties->currentPage() }}">
                    {!! Form::close() !!}
                    <div class="btn-group" role="group">
                        <a  class="btn btn-warning btn-sm" href="{{ url('/cg_admin/product_properties/'.$property_name.'/'.$product_property->id.'/edit?page='.$product_properties->currentPage()) }}">Edit</a>
                            <a id="navbarDropdown" class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                More <span class="caret"></span>
                            </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if($product_property->product_categories()->count() || $product_property->products()->count())
                                 <a  class="dropdown-item" onclick="deleteLocked{{$product_property->id}}()" href="">Locked</a>
                                 @else
                                 <a  class="dropdown-item" onclick="deleteConfirm{{$product_property->id}}()" href="">Delete</a>
                                 @endif
                       </div>
                </div>

                </td>
            </tr>
         @endforeach
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
           {{ $product_properties->links() }}
        </nav>

    </div>
    <script>
        @foreach($product_properties as $product_property)
        function deleteConfirm{{$product_property->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this ?'))
            {
                document.getElementById("deleteForm{{$product_property->id}}").submit();
            }
            else {}
        }
        function deleteLocked{{$product_property->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach

    </script>
@endsection
