@extends('layouts.admin-app')

@section('content')
    <div class="row">
     <div class="col-md-12">
         <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
             <h1 class="h2">{{ $product_property_title }}</h1>
             <div class="btn-toolbar mb-2 mb-md-0">
                 <div class="btn-group mr-2">
                     <a type="button" href="{{ url('/cg_admin/product_properties/'.$property_name.'?page='.request()->get('page')) }}" class="btn btn-sm btn-outline-secondary mb-2">Back</a>
                 </div>

             </div>
         </div>
          @if(isset($product_property))
                {{ Form::model($product_property, array('url' => 'cg_admin/product_properties/'.$property_name.'/'.$product_property->id.'/update',
                         'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
            @else
                {{ Form::open(array('url' => 'cg_admin/product_properties/'.$property_name,'data-parsley-validate','novalidate',
                         'files'=>true)) }}
            @endif

                <input type="hidden" value="{{ request()->get('page') }}" name="page">
            <div class="form-group">
                <label for="name">Name</label>
               <input type="text" id="name" class="form-control" value="@if(isset($product_property)){{ $product_property->name }}@endif" name="name">
            </div>

         <div class="form-group">
             <label for="svg">SVG Icon</label>
             <textarea  id="svg" class="form-control" name="svg">@if(isset($product_property)){{ $product_property->svg }}@endif</textarea>
         </div>

         <div class="form-group">
             <label for="default_status">Default</label>
             <select class="form-control" id="default_status" name="default_status">
                 <option value="0" @if(isset($product_property) && $product_property->default_status ==0) selected @endif>Not Default</option>
                 <option value="1" @if(isset($product_property) && $product_property->default_status ==1) selected @endif>Default</option>
             </select>
         </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="1" @if(isset($product_property) && $product_property->status ==1) selected @endif>Active</option>
                    <option value="0" @if(isset($product_property) && $product_property->status ==0) selected @endif>In Active</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
            {{ Form::close() }}


        </div>
    </div>

@endsection
