@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Help Center</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#catModalAdd">
			New Category
			</button>
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#topicModalAdd">
			New Topic
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">
		<div class="col-md-4">
			<ul class="list-group">
                @foreach($help_categories as $help_category)
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<div>

						<button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#catModal{{ $help_category->id }}">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
							<path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
						</svg>
						</button>
						{{ $help_category->name }}
					</div>
					<span class="badge badge-primary badge-pill">{{ $help_category->help_topics->count() }}</span>
				</li>

                    <!-- Modal -->
                    <div class="modal fade" id="catModal{{ $help_category->id }}" tabindex="-1" role="dialog" aria-labelledby="catModal{{ $help_category->id }}" aria-hidden="true">

                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="catModal">Edit Category  </h5>

                                    {!! Form::open(['method' => 'DELETE', 'route' => ['help_categories.destroy', $help_category->id],'id' => 'deleteFormCat'.$help_category->id, ]) !!}
                                    <input type="hidden" name="page" value="{{ $help_categories->currentPage() }}">
                                    {!! Form::close() !!}
                                    @if($help_category->help_topics()->count() )
                                        <a  class="btn btn-danger btn-sm" onclick="deleteLockedCat{{$help_category->id}}()" href="">Locked</a>
                                    @else
                                        <a  class="btn btn-danger btn-sm" onclick="deleteConfirmCat{{$help_category->id}}()" href="">Delete</a>
                                    @endif
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                </div>
                                <div class="modal-body">
                                    {{ Form::model($help_category, array('route' => array('help_categories.update', $help_category->id),
                     'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Category Name" value="{{ $help_category->name }}">
                                        </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="support_to_mail" name="support_to_mail" placeholder="Support To Mail" value="{{ $help_category->support_to_mail }}">
                                    </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="description" id="description" cols="30" rows="5" placeholder="Category Description"> {{ $help_category->description }}</textarea>
                                        </div>

                                        <hr>
                                        <p>
                                            <a class="btn btn-light" data-toggle="collapse" href="#collapseAddCat{{$help_category->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                Meta Components
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </a>
                                        </p>
                                        <div class="collapse" id="collapseAddCat{{$help_category->id}}">
                                            <div class="card card-body mb-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" value="{{ $help_category->meta_title}}" >
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="meta_description" id="meta_description" cols="30" placeholder="Meta Description" rows="5">{{ $help_category->meta_description}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    @if($help_category->meta_image) <img src="{{ $help_category->getMetaImage() }}" class="img-thumbnail">  @endif
                                                    <div class="custom-file">
                                                        <input type="file" name="meta_image" class="custom-file-input" id="customFile">
                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    {{ Form::close() }}

                                </div>
                            </div>
                        </div>

                    </div>

                @endforeach

			</ul>
            <br>
            {{ $help_categories->links() }}
		</div>
		<div class="col-md-8">
            @foreach($help_topics as $help_topic)
			<div class="card mb-2">
				<div class="card-body">
					<h6 class="card-title mb-2 text-muted">{{ $help_topic->title }}</h6>
					<p class="text-muted"><small>{{ $help_topic->help_category->name }}</small></p>
					<a href="#topicModal{{ $help_topic->id }}"  type="button" data-toggle="modal" data-target="#topicModalEdit{{ $help_topic->id }}" class="card-link">Edit</a>
					<a href="{{ url($help_topic->getHelpTopicURL()) }}" target="_blank" class="card-link">Preview</a>
				</div>
			</div>


                <!-- Modal -->
                <div class="modal mod_ck_editor fade" id="topicModalEdit{{ $help_topic->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $help_topic->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="topicModal">Edit Help Topic</h5>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['help_topics.destroy', $help_topic->id],'id' => 'deleteFormTopic'.$help_topic->id, ]) !!}
                                <input type="hidden" name="page" value="{{ $help_topics->currentPage() }}">
                                {!! Form::close() !!}
                                @if(0)
                                    <a  class="btn btn-danger btn-sm" onclick="deleteLockedTopic{{$help_topic->id}}()" href="">Locked</a>
                                @else
                                    <a  class="btn btn-danger btn-sm" onclick="deleteConfirmTopic{{$help_topic->id}}()" href="">Delete</a>
                                @endif
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ Form::model($help_topic, array('route' => array('help_topics.update', $help_topic->id),
                       'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}
                                <div class="form-group">
                                    <select class="form-control" name="help_category" id="help_category">
                                        <option value="">Select Help Category</option>
                                        @foreach($help_categories_all as $help_category)
                                            <option value="{{ $help_category->id }}" @if($help_topic->help_category_id==$help_category->id) selected @endif>{{ $help_category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">

                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $help_topic->title }}">
                                </div>
                                <div class="form-group">
                                    {{-- <div id="editor"></div> --}}
                                    <textarea name="description" id="editor{{ $help_topic->id }}" placeholder="Description">{{ $help_topic->description }}</textarea>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="faq_status" @if($help_topic->faq_status) checked @endif value="1">
                                    <label class="form-check-label" for="faq_status">Add to FAQ Section</label>
                                </div>

                                <hr>
                                <p>
                                    <a class="btn btn-light" data-toggle="collapse" href="#collapseAddTopic{{ $help_topic->id }}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Meta Components
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                        </svg>
                                    </a>
                                </p>
                                <div class="collapse" id="collapseAddTopic{{ $help_topic->id }}">
                                    <div class="card card-body mb-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" value="{{ $help_topic->meta_title }}">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_description" id="meta_description" cols="30" rows="5" placeholder="Meta Description">{{ $help_topic->meta_description }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            @if($help_topic->meta_image) <img src="{{ $help_topic->getMetaImage() }}" class="img-thumbnail">  @endif
                                            <div class="custom-file">
                                                <input type="file" name="meta_image" class="custom-file-input" id="meta_image">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
			@endforeach
                {{ $help_topics->links() }}
		</div>
	</div>
</div>



<div class="modal fade" id="catModalAdd" tabindex="-1" role="dialog" aria-labelledby="catModalAdd" aria-hidden="true">
    {{ Form::open(array('url' => 'cg_admin/help_categories','data-parsley-validate','novalidate',
                         'files'=>true)) }}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="catModal">New Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">

                        <input type="text" class="form-control" id="name" name="name" placeholder="Category Name" >
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="description" id="description" cols="30" rows="5" placeholder="Category Description"></textarea>
                    </div>

                    <hr>
                    <p>
                        <a class="btn btn-light" data-toggle="collapse" href="#collapseAddCat" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Meta Components
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                            </svg>
                        </a>
                    </p>
                    <div class="collapse" id="collapseAddCat">
                        <div class="card card-body mb-2">
                            <div class="form-group">
                                <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title"  >
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="meta_description" id="meta_description" cols="30" placeholder="Meta Description" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" name="meta_image" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
<!-- Modal -->
<div class="modal mod_ck_editor fade" id="topicModalAdd" tabindex="-1" role="dialog" aria-labelledby="topicModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="topicModal">New Help Topic</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/help_topics','data-parsley-validate','novalidate',
                            'files'=>true)) }}
					<div class="form-group">
						<select class="form-control" name="help_category" id="help_category">
							<option value="">Select Help Category</option>
							@foreach($help_categories_all as $help_category)
                            <option value="{{ $help_category->id }}">{{ $help_category->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">

						<input type="text" class="form-control" id="title" name="title" placeholder="Title" >
					</div>
					<div class="form-group">
						{{-- <div id="editor"></div> --}}
						<textarea name="description" id="editor" placeholder="Description"></textarea>
					</div>
					<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="faq_status" value="1">
    <label class="form-check-label" for="faq_status">Add to FAQ Section</label>
  </div>

					<hr>
					<p>
						<a class="btn btn-light" data-toggle="collapse" href="#collapseAddTopic" role="button" aria-expanded="false" aria-controls="collapseExample">
							Meta Components
							<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
							</svg>
						</a>
					</p>
					<div class="collapse" id="collapseAddTopic">
						<div class="card card-body mb-2">
							<div class="form-group">
								<input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" >
							</div>
							<div class="form-group">
								<textarea class="form-control" name="meta_description" id="meta_description" cols="30" rows="5" placeholder="Meta Description"></textarea>
							</div>
							<div class="form-group">
								<div class="custom-file">
									<input type="file" name="meta_image" class="custom-file-input" id="meta_image">
									<label class="custom-file-label" for="customFile">Choose file</label>
								</div>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($help_categories as $help_category)
        function deleteConfirmCat{{$help_category->id}}() {
            event.preventDefault();
            if (confirm('Are you sure you want to delete this ?'))
            {
                document.getElementById("deleteFormCat{{$help_category->id}}").submit();
            }
            else {}
        }
        function deleteLockedCat{{$help_category->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
@endforeach

@foreach($help_topics as $help_topic)
function deleteConfirmTopic{{$help_topic->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteFormTopic{{$help_topic->id}}").submit();
    }
    else {}
}
        function deleteLockedTopic{{$help_topic->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }


        CKEDITOR.replace('editor{{ $help_topic->id }}', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });
        @endforeach

        CKEDITOR.replace('editor', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",

            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });
    </script>

@endpush
