@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Magic Pins</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <form action="" method="get" class="form-inline">
                <div class="form-group mx-sm-3 mb-2">
                    <select class="form-control" name="key" required>
                        <option @if($key && $key == 'all') selected @endif value="all">All</option>
                        <option @if($key && $key == 'active') selected @endif value="active">Active</option>
                        <option @if($key && $key == 'used') selected @endif value="used">Used</option>
                        <option @if($key && $key == 'not-used') selected @endif value="not-used">Not Used</option>
                        <option @if($key && $key == 'in-active') selected @endif value="in-active">InActive</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filter</button>
            </form>

        </div>
		<div class="mr-2">
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAdd">
			New Pin
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">

            @foreach($magic_pins as $magic_pin)
            <div class="col-md-3">
			<div class="card mb-2">
				<div class="card-body">
					<h6 class="card-title mb-2 text-muted">{{ $magic_pin->secret_pin }}</h6>
                    <p>
                        @if($magic_pin->status ==1)
                            <a href="" class="badge-success badge">Active</a>
                        @else
                            <a href="" class="badge-danger badge">InActive</a>
                        @endif
                            @if($magic_pin->user_id)
                             ||  <a target="_blank" href="{{ url('cg_admin/users/'.$magic_pin->user->role.'/'.$magic_pin->user->id) }}" class="badge-success badge">{{ $magic_pin->user->name }}</a>
                            @else
                              || <a href="" class="badge-danger badge">@if($magic_pin->assign_to) {{ $magic_pin->assign_to }} @else Not Used @endif</a>
                            @endif
                    </p>
					<a href="#Modal{{ $magic_pin->id }}"  type="button" data-toggle="modal" data-target="#ModalEdit{{ $magic_pin->id }}" class="card-link">Edit</a>
				</div>
			</div>


                <!-- Modal -->
                <div class="modal mod_ck_editor fade" id="ModalEdit{{ $magic_pin->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $magic_pin->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="topicModal">Edit Pin</h5>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['magic_pins.destroy', $magic_pin->id],'id' => 'deleteForm'.$magic_pin->id, ]) !!}
                                <input type="hidden" name="page" value="{{ $magic_pins->currentPage() }}">
                                {!! Form::close() !!}
                                @if($magic_pin->user_id)
                                    <a  class="btn btn-danger btn-sm" onclick="deleteLocked{{$magic_pin->id}}()" href="">Locked</a>
                                @else
                                    <a  class="btn btn-danger btn-sm" onclick="deleteConfirm{{$magic_pin->id}}()" href="">Delete</a>
                                @endif
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ Form::model($magic_pin, array('route' => array('magic_pins.update', $magic_pin->id),
                       'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

                                <div class="form-group">
                                    <input type="text" class="form-control" id="secret_pin" name="secret_pin" placeholder="Secret Pin" value="{{ $magic_pin->secret_pin }}">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" id="assign_to" name="assign_to" placeholder="Assign To" value="{{ $magic_pin->assign_to }}">
                                </div>

                                <div class="form-group">
                                    <select class="form-control" name="status">
                                        <option value="0" @if($magic_pin->status == 0) selected @endif>In Active</option>
                                        <option value="1" @if($magic_pin->status == 1) selected @endif>Active</option>
                                    </select>
                                </div>

                                <hr>
                                @if($magic_pin->user_id)
                                <p>
                                  Used By : <a class="btn btn-light" data-toggle="collapse" href="{{ url('/cg_admin/users/'.$magic_pin->user->role.'/'.$magic_pin->user->id) }}" role="button" > {{ $magic_pin->user->name }} </a>
                                </p>
                                @endif
                                @if($magic_pin->admin_id)
                                    <p>
                                        Last Updated By : <a class="btn btn-light" data-toggle="collapse" href="" role="button" > {{ $magic_pin->admin->name }} </a>
                                    </p>
                                @endif

                                <button type="submit" class="btn btn-primary">Submit</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			@endforeach
	</div>
    <div class="row">
        @if($key)
            {{$magic_pins->appends(['key' => $key])->links()}}
        @else
            {{$magic_pins->links()}}
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal mod_ck_editor fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="topicModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="topicModal">New Pin</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/magic_pins','data-parsley-validate','novalidate',
                            'files'=>true)) }}

					<div class="form-group">
						<input type="text" class="form-control" id="secret_pin" name="secret_pin" placeholder="Secret Pin" >
					</div>
                <div class="form-group">
                    <input type="text" class="form-control" id="assign_to" name="assign_to" placeholder="Assign To" >
                </div>

                <div class="form-group">
                    <select class="form-control" name="status">
                        <option value="0">In Active</option>
                        <option value="1">Active</option>
                    </select>
                </div>

					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($magic_pins as $magic_pin)
function deleteConfirm{{$magic_pin->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteForm{{$magic_pin->id}}").submit();
    }
    else {}
}
        function deleteLocked{{$magic_pin->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach


    </script>

@endpush
