@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Creativegoods Policies</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}<!-- Button trigger modal -->
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#topicModalAdd">
			New Policy
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">
		<div class="col-md-12">
            @foreach($policies as $policy)
			<div class="card mb-2">
				<div class="card-body">
					<h6 class="card-title mb-2 text-muted">{{ $policy->title }}</h6>
					<a href="#topicModal{{ $policy->id }}"  type="button" data-toggle="modal" data-target="#topicModalEdit{{ $policy->id }}" class="card-link">Edit</a>
					<a href="{{ url($policy->getPolicyURL()) }}" target="_blank" class="card-link">Preview</a>
				</div>
			</div>


                <!-- Modal -->
                <div class="modal mod_ck_editor fade" id="topicModalEdit{{ $policy->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $policy->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="topicModal">Edit Policy</h5>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['policies.destroy', $policy->id],'id' => 'deleteFormTopic'.$policy->id, ]) !!}
                                <input type="hidden" name="page" value="{{ $policies->currentPage() }}">
                                {!! Form::close() !!}
                                @if($policy->id == 1 || $policy->id == 2 || $policy->id == 3 || $policy->id == 4)
                                    <a  class="btn btn-danger btn-sm" onclick="deleteLockedTopic{{$policy->id}}()" href="">Locked</a>
                                @else
                                    <a  class="btn btn-danger btn-sm" onclick="deleteConfirmTopic{{$policy->id}}()" href="">Delete</a>
                                @endif
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ Form::model($policy, array('route' => array('policies.update', $policy->id),
                       'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

                                <div class="form-group">

                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $policy->title }}">
                                </div>
                                <div class="form-group">
                                    {{-- <div id="editor"></div> --}}
                                    <textarea name="description" id="editor{{ $policy->id }}" placeholder="Description">{{ $policy->description }}</textarea>
                                </div>

                                <hr>
                                <p>
                                    <a class="btn btn-light" data-toggle="collapse" href="#collapseAddTopic{{ $policy->id }}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Meta Components
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                        </svg>
                                    </a>
                                </p>
                                <div class="collapse" id="collapseAddTopic{{ $policy->id }}">
                                    <div class="card card-body mb-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" value="{{ $policy->meta_title }}">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="meta_description" id="meta_description" cols="30" rows="5" placeholder="Meta Description">{{ $policy->meta_description }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            @if($policy->meta_image) <img src="{{ $policy->getMetaImage() }}" class="img-thumbnail">  @endif
                                            <div class="custom-file">
                                                <input type="file" name="meta_image" class="custom-file-input" id="meta_image">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
			@endforeach
                {{ $policies->links() }}
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal mod_ck_editor fade" id="topicModalAdd" tabindex="-1" role="dialog" aria-labelledby="topicModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="topicModal">New Policy</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/policies','data-parsley-validate','novalidate',
                            'files'=>true)) }}

					<div class="form-group">
						<input type="text" class="form-control" id="title" name="title" placeholder="Title" >
					</div>
					<div class="form-group">
						{{-- <div id="editor"></div> --}}
						<textarea name="description" id="editor" placeholder="Description"></textarea>
					</div>

					<hr>
					<p>
						<a class="btn btn-light" data-toggle="collapse" href="#collapseAddTopic" role="button" aria-expanded="false" aria-controls="collapseExample">
							Meta Components
							<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
							</svg>
						</a>
					</p>
					<div class="collapse" id="collapseAddTopic">
						<div class="card card-body mb-2">
							<div class="form-group">
								<input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" >
							</div>
							<div class="form-group">
								<textarea class="form-control" name="meta_description" id="meta_description" cols="30" rows="5" placeholder="Meta Description"></textarea>
							</div>
							<div class="form-group">
								<div class="custom-file">
									<input type="file" name="meta_image" class="custom-file-input" id="meta_image">
									<label class="custom-file-label" for="customFile">Choose file</label>
								</div>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($policies as $policy)
function deleteConfirmTopic{{$policy->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteFormTopic{{$policy->id}}").submit();
    }
    else {}
}
        function deleteLockedTopic{{$policy->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }

        CKEDITOR.replace('editor{{ $policy->id }}', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });
        @endforeach

        CKEDITOR.replace('editor', {
            filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",

            customConfig: '{{ asset('js/ckeditor/config.js') }}'
        });

    </script>

@endpush
