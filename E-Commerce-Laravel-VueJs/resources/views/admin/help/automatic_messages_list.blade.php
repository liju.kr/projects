@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Automatic Message</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}<!-- Button trigger modal -->
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAdd">
              New  Automatic Message
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">

            @foreach($automatic_messages as $automatic_message)
            <div class="col-md-6">
			<div class="card mb-2">
				<div class="card-body">
					<h6 class="card-title mb-2 text-muted">{{ $automatic_message->message }}</h6>
                    <p>
                      <a href="" class="badge-success badge">{{ $automatic_message->slug }}</a>
                    </p>
					<a href="#Modal{{ $automatic_message->id }}"  type="button" data-toggle="modal" data-target="#ModalEdit{{ $automatic_message->id }}" class="card-link">Edit</a>
				</div>
			</div>


                <!-- Modal -->
                <div class="modal mod_ck_editor fade" id="ModalEdit{{ $automatic_message->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $automatic_message->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="topicModal">Edit Automatic Message</h5>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['automatic_messages.destroy', $automatic_message->id],'id' => 'deleteForm'.$automatic_message->id, ]) !!}
                                <input type="hidden" name="page" value="{{ $automatic_messages->currentPage() }}">
                                {!! Form::close() !!}
                                @if(1)
                                    <a  class="btn btn-danger btn-sm" onclick="deleteLocked{{$automatic_message->id}}()" href="">Locked</a>
                                @else
                                    <a  class="btn btn-danger btn-sm" onclick="deleteConfirm{{$automatic_message->id}}()" href="">Delete</a>
                                @endif
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ Form::model($automatic_message, array('route' => array('automatic_messages.update', $automatic_message->id),
                       'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

                                <div class="form-group">
                                    <input type="text" readonly class="form-control" id="slug" name="slug" placeholder="Slug" value="{{ $automatic_message->slug }}">
                                </div>

                                <div class="form-group">
                                  <textarea class="form-control" name="message" rows="3">{{ $automatic_message->message }}</textarea>
                                </div>


                                <hr>
                                @if($automatic_message->admin_id)
                                    <p>
                                        Last Updated By : <a class="btn btn-light" data-toggle="collapse" href="" role="button" > {{ $automatic_message->admin->name }} </a>
                                    </p>
                                @endif

                                <button type="submit" class="btn btn-primary">Submit</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			@endforeach
	</div>
    <div class="row">  {{ $automatic_messages->links() }}</div>
</div>

<!-- Modal -->
<div class="modal mod_ck_editor fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="ModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal">New Automatic Message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/automatic_messages','data-parsley-validate','novalidate',
                            'files'=>true)) }}

					<div class="form-group">
						<input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" >
					</div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="3"></textarea>
                </div>

					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($automatic_messages as $automatic_message)
function deleteConfirm{{$automatic_message->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteForm{{$automatic_message->id}}").submit();
    }
    else {}
}
        function deleteLocked{{$automatic_message->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach


    </script>

@endpush
