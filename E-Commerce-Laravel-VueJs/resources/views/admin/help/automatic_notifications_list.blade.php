@extends('layouts.admin-app')
@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Automatic Notification</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="mr-2">
			{{--  <a type="button" href="" class="btn btn-sm btn-info mb-2">Add</a>--}}<!-- Button trigger modal -->
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#ModalAdd">
              New  Automatic Notification
			</button>
		</div>
	</div>
</div>
<div class="fluid-container">
	<div class="row">

            @foreach($automatic_notifications as $automatic_notification)
            <div class="col-md-6">
			<div class="card mb-2">
				<div class="card-body">
					<h6 class="card-title mb-2 text-muted">{{ $automatic_notification->message }}</h6>
                    <p>
                      <a href="" class="badge-success badge">{{ $automatic_notification->slug }}</a>
                    </p>
					<a href="#Modal{{ $automatic_notification->id }}"  type="button" data-toggle="modal" data-target="#ModalEdit{{ $automatic_notification->id }}" class="card-link">Edit</a>
				</div>
			</div>


                <!-- Modal -->
                <div class="modal mod_ck_editor fade" id="ModalEdit{{ $automatic_notification->id }}" tabindex="-1" role="dialog" aria-labelledby="topicModalEdit{{ $automatic_notification->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="topicModal">Edit Automatic Notification</h5>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['automatic_notifications.destroy', $automatic_notification->id],'id' => 'deleteForm'.$automatic_notification->id, ]) !!}
                                <input type="hidden" name="page" value="{{ $automatic_notifications->currentPage() }}">
                                {!! Form::close() !!}
                                @if(1)
                                    <a  class="btn btn-danger btn-sm" onclick="deleteLocked{{$automatic_notification->id}}()" href="">Locked</a>
                                @else
                                    <a  class="btn btn-danger btn-sm" onclick="deleteConfirm{{$automatic_notification->id}}()" href="">Delete</a>
                                @endif
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ Form::model($automatic_notification, array('route' => array('automatic_notifications.update', $automatic_notification->id),
                       'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}

                                <div class="form-group">
                                    <input type="text" readonly class="form-control" id="slug" name="slug" placeholder="Slug" value="{{ $automatic_notification->slug }}">
                                </div>

                                <div class="form-group">
                                  <textarea class="form-control" name="message" rows="3">{{ $automatic_notification->message }}</textarea>
                                </div>


                                <hr>
                                @if($automatic_notification->admin_id)
                                    <p>
                                        Last Updated By : <a class="btn btn-light" data-toggle="collapse" href="" role="button" > {{ $automatic_notification->admin->name }} </a>
                                    </p>
                                @endif

                                <button type="submit" class="btn btn-primary">Submit</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			@endforeach
	</div>
    <div class="row">  {{ $automatic_notifications->links() }}</div>
</div>

<!-- Modal -->
<div class="modal mod_ck_editor fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="ModalAdd" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal">New Automatic Notification</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                {{ Form::open(array('url' => 'cg_admin/automatic_notifications','data-parsley-validate','novalidate',
                            'files'=>true)) }}

					<div class="form-group">
						<input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" >
					</div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="3"></textarea>
                </div>

					<button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer_scripts')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script>
@foreach($automatic_notifications as $automatic_notification)
function deleteConfirm{{$automatic_notification->id}}() {
    event.preventDefault();
    if (confirm('Are you sure you want to delete this ?'))
    {
        document.getElementById("deleteForm{{$automatic_notification->id}}").submit();
    }
    else {}
}
        function deleteLocked{{$automatic_notification->id}}() {
            event.preventDefault();
            alert('Has many Relations')
        }
        @endforeach


    </script>

@endpush
