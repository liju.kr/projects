@extends('layouts.app', ['page_class' => 'common-popup login-signup-page font1'])
@section('title', 'Login to Fontfolio')
@section('meta')
    <!-- Search Engine -->
    <meta name="description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity. We will add Best free Creative Goods Here, which Collected from leading Creative Marketplaces">
    <meta name="image" content="{{asset('images/cg-meta-logo.png')}}">
    <meta name="author"  content="{{ env('APP_NAME') }}">
    <meta name="copyright"  content="{{ env('APP_NAME') }}">
    <meta name="keywords" content="creativegoods, creative marketplace, premium creative goods, wordpress themes, premium fonts, free fonts, vectors for craft works,  premium templates, bootstrap, Webhance Studio network, Webhance Studio inc, html5, web devlopment, jquery animations, css3, jQuery, parallax, minimalist website, interactive html5, animated html5 websites, web design india, creativegoods.net, premium web development," />
    <!-- Schema.org for Google -->
    <meta  content="{{ env('APP_NAME') }}">
    <meta content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ env('APP_NAME') }}">
    <meta name="twitter:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="twitter:site" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:creator" content="@{{ env('APP_NAME') }}">
    <meta name="twitter:image:src" content="{{asset('images/cg-meta-banner.png')}}">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
   <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="og:description" content="We bring the best and Selected Creative Goods for accelerating your creativity and productivity.">
    <meta name="og:image" content="{{asset('images/cg-meta-banner.png')}}">
    <meta name="og:url" content="{{ url('/') }}">
    <meta name="og:site_name" content="{{ env('APP_NAME') }}">
    <meta name="fb:admins" content="814789562597194">
    <meta name="og:type" content="website">
@endsection
@section('content')
    <div class="cg-profile-over-view-wrap d-flex justify-content-center align-items-center">
        <div class="login-wrap text-center">
            <form id="form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="login-text-block ">
                    <h3 class="font1 cg-section-sml-heading w700">Login to Fontfolio</h3>
                </div>
                <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                    <div class="cg-login-inputs">
                        <label for="email">Email ID</label>
                        <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="row d-flex add-btm-xsml d-flex justify-content-center">
                    <div class="cg-login-inputs">
                        <label for="password">Password</label>
                        <div class="cg-pass-field-wrap">
                          <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                          <a href="#" onclick="showPassword()" class="cg-pass-field-wrap-eye"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="cg-main-login-buttons d-flex justify-content-center">
                    <input type="submit" value="Login Securely" name="submit" id="submit" />
                </div>
                <div class="row d-ﬂex add-btm-xsml d-ﬂex justify-content-center">
                    <span class="login-additional-links" >New to {{ env('APP_NAME') }}? <a href="{{ url('/register') }}">Sign up!</a></span>
                </div>
                <div class="row d-ﬂex add-btm-xsml d-ﬂex justify-content-center">
                    <span class="login-additional-links" > <a href="{{ url('/password/reset') }}">Forgot Password ?</a></span>
                </div>
            </form>
        </div>
    </div>
@endsection
