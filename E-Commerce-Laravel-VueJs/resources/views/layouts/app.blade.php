<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
    @yield('meta')
<link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">

    <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/libs/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/cg-main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/cg-styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/mobile.css')}}">
    <!-- Styles -->
@if(env('APP_ENV')=="production")
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194815133-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-194815133-1');
    </script>
    @endif
</head>
<body>
<div id="preloader2">
    <div id="status2">&nbsp;</div>
</div>
@include('partials.navigation')

<div id="master-wrap" class="master-wrap {{$page_class}}">


    @yield('content')

    @if(Session('message'))
    <div class="cg-toast-one">
      <ul class="d-flex justify-content-between">
        <li class="first"><p>{{ Session('message') }}</p></li>
        <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
      </ul>
    </div>
    @endif

    @if(Session('error'))
        <div class="cg-toast-one">
            <ul class="d-flex justify-content-between">
                <li class="first"><p>{{ Session('error') }}</p></li>
                <li class="first"><a class="kill-toast fa fa-times" aria-hidden="true"></a></li>
            </ul>
        </div>
    @endif



    @include('partials.footer')

    @if(Auth::check() && Auth::user()->getBotUnreadMessagesCount() && !request()->is('messages'))

        <div class="message-pop-wrap collapse show cg-admin-message-popup" id="cg-admin-msg-popup">

            <div class="cg-uni-popup text-center">
                <div class="chat-text-block cg-bot-chat">
                    <h3>{{ Auth::user()->getBot()->name }}</h3>
                    <div class="cg-bot-thumb">
                        <img src="{{ Auth::user()->getBot()->user_profile->image_path }}" class="img-fluid" alt="{{ Auth::user()->getBot()->name }}">
                    </div>
                </div>
                <div class="cg-bot-content">
                    <div class="row d-flex justify-content-start flex-column align-items-center text-left">
                        <span class="cg-bot-message">{{ Auth::user()->getBotlastMessage() }}</span>

                    </div>
                </div>
                <div class="cg-bot-button-wrap d-flex justify-content-end align-items-center">
                    <a class="cg-bot-button" href="{{ url(Auth::user()->user_url.'/messages')  }}">View</a>
                    <a class="cg-bot-closer" data-toggle="collapse" href="#cg-admin-msg-popup" role="button" aria-expanded="false" aria-controls="collapseExample">close</a>
                </div>
            </div>
            @if(Auth::user()->getBotUnreadMessagesCount() >= 1)<h6 class="bot-unread-count"><span>{{ Auth::user()->getBotUnreadMessagesCount() }}</span>Unread @if(Auth::user()->getBotUnreadMessagesCount() == 1) Message @else Messages @endif </h6> @endif
        </div>
    @endif

</div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
@if(env('APP_ENV')=="production")
<script>
    $(document).bind("contextmenu",function(e) {
        e.preventDefault();
    });
    $(document).keydown(function(e){
        if(e.which === 123){
            return false;
        }
    });
</script>
@endif

<script type="text/javascript" src="{{asset('javascripts/libs/navigation.js')}}"></script>
<script src="{{asset('javascripts/libs/plugins.js')}}"></script>
<script src="{{asset('javascripts/custom/focal.js')}}"></script>

@stack('footer_scripts')

</body>
</html>
