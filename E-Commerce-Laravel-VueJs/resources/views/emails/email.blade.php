<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
   <head>
      <!--[if gte mso 9]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
      <meta content="width=device-width" name="viewport"/>
      <!--[if !mso]><!-->
      <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
      <!--<![endif]-->
      <title></title>
      <!--[if !mso]><!-->
      <!--<![endif]-->
      <style type="text/css">
         body {
           min-height: 100vh;
         margin: 0;
         padding: 0;
         }
         .cg-email-body {
           margin-top: 5% !important;
         }
         .content-wrap {
     padding: 50px;
 }
 .email-clr-logo {
    margin-top: 10px;
}
 .headline-text {
   margin-top: 20px !important;
 }

         table,
         td,
         tr {
         vertical-align: top;
         border-collapse: collapse;
         }
         * {
         line-height: inherit;
         }
         a[x-apple-data-detectors=true] {
         color: inherit !important;
         text-decoration: none !important;
         }
      </style>
      <style id="media-query" type="text/css">
         @media (max-width: 740px) {
         .block-grid,
         .col {
         min-width: 320px !important;
         max-width: 100% !important;
         display: block !important;
         }
         .block-grid {
         width: 100% !important;
         }
         .col {
         width: 100% !important;
         }
         .col>div {
         margin: 0 auto;
         }
         img.fullwidth,
         img.fullwidthOnMobile {
         max-width: 100% !important;
         }
         .no-stack .col {
         min-width: 0 !important;
         display: table-cell !important;
         }
         .no-stack.two-up .col {
         width: 50% !important;
         }
         .no-stack .col.num4 {
         width: 33% !important;
         }
         .no-stack .col.num8 {
         width: 66% !important;
         }
         .no-stack .col.num4 {
         width: 33% !important;
         }
         .no-stack .col.num3 {
         width: 25% !important;
         }
         .no-stack .col.num6 {
         width: 50% !important;
         }
         .no-stack .col.num9 {
         width: 75% !important;
         }
         .video-block {
         max-width: none !important;
         }
         .mobile_hide {
         min-height: 0px;
         max-height: 0px;
         max-width: 0px;
         display: none;
         overflow: hidden;
         font-size: 0px;
         }
         .desktop_hide {
         display: block !important;
         max-height: none !important;
         }
         }
      </style>
      <style id="menu-media-query" type="text/css">
         @media (max-width: 740px) {
         .menu-checkbox[type="checkbox"]~.menu-links {
         display: none !important;
         padding: 5px 0;
         }
         .menu-checkbox[type="checkbox"]~.menu-links span.sep {
         display: none;
         }
         .menu-checkbox[type="checkbox"]:checked~.menu-links,
         .menu-checkbox[type="checkbox"]~.menu-trigger {
         display: block !important;
         max-width: none !important;
         max-height: none !important;
         font-size: inherit !important;
         }
         .menu-checkbox[type="checkbox"]~.menu-links>a,
         .menu-checkbox[type="checkbox"]~.menu-links>span.label {
         display: block !important;
         text-align: center;
         }
         .menu-checkbox[type="checkbox"]:checked~.menu-trigger .menu-close {
         display: block !important;
         }
         .menu-checkbox[type="checkbox"]:checked~.menu-trigger .menu-open {
         display: none !important;
         }
         #menussqqly~div label {
         border-radius: 50% !important;
         }
         #menussqqly:checked~.menu-links {
         background-color: #00d09c !important;
         }
         #menussqqly:checked~.menu-links a {
         color: #ffffff !important;
         }
         #menussqqly:checked~.menu-links span {
         color: #ffffff !important;
         }
         }
      </style>
   </head>
   <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff;">
      <!--[if IE]>
      <div class="ie-browser">
         <![endif]-->
         <table class="cg-email-body" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; width: 100%;" valign="top" width="100%">
            <tbody>
               <tr style="vertical-align: top;" valign="top">
                  <td style="word-break: break-word; vertical-align: top;" valign="top">
                     <!--[if (mso)|(IE)]>
                     <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                           <td align="center" style="background-color:#ffffff">
                              <![endif]-->
                              <div style="background-color:transparent;">
                                 <div class="block-grid two-up no-stack" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="360" style="background-color:transparent;width:360px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:20px; padding-bottom:20px;">
                                                                  <![endif]-->
                                                                  <div class="col num6" style="min-width: 320px; max-width: 360px; display: table-cell; vertical-align: top; width: 360px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <div align="left" class="img-container left fixedwidth" style="padding-right: 0px;padding-left: 25px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr style="line-height:0px">
                                                                                    <td style="padding-right: 0px;padding-left: 25px;" align="left">
                                                                                       <![endif]-->
                                                                                       <div style="font-size:1px;line-height:5px"> </div>
                                                                                       <img alt="Fontfolio Logo" border="0" class="left fixedwidth" src="https://www.fontfolio.net/images/logo.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 144px; display: block;" title="Logo" width="144"/>
                                                                                       <!--[if mso]>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                      <td align="center" width="360" style="background-color:transparent;width:360px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:20px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num6" style="min-width: 320px; max-width: 360px; display: table-cell; vertical-align: top; width: 360px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:23px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                                                                              <tr style="vertical-align: top;" valign="top">
                                                                                 <td align="right" style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 20px; text-align: right; font-size: 0px;" valign="top">
                                                                                    <div class="menu-links">
                                                                                       <!--[if mso]>
                                                                                       <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                          <tr>
                                                                                             <td style="padding-top:5px;padding-right:20px;padding-bottom:0px;padding-left:15px">
                                                                                                <![endif]--><a href="https://www.fontfolio.net/welcome-affiliate" style="padding-top:5px;padding-bottom:0px;padding-left:15px;padding-right:20px;display:inline;color:#006d77;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;font-size:14px;text-decoration:none;">Affiliate</a>
                                                                                                <!--[if mso]>
                                                                                             </td>
                                                                                             <td style="padding-top:5px;padding-right:20px;padding-bottom:0px;padding-left:15px">
                                                                                                <![endif]--><a href="https://www.fontfolio.net/login" style="padding-top:5px;padding-bottom:0px;padding-left:15px;padding-right:20px;display:inline;color:#006d77;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;font-size:14px;text-decoration:none;">Log In</a>
                                                                                                <!--[if mso]>
                                                                                             </td>
                                                                                          </tr>
                                                                                       </table>
                                                                                       <![endif]-->
                                                                                    </div>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div class="content-wrap" style="background-color:#f8f5ff;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f8f5ff;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="720" style="background-color:transparent;width:720px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:60px; padding-bottom:60px;">
                                                                  <![endif]-->
                                                                  <div class="col num12 main-content-image-wrap" style="min-width: 320px; max-width: 720px; display: table-cell; vertical-align: center; width: 720px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:60px; padding-bottom:60px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr style="line-height:0px">
                                                                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                                                                       <![endif]-->
                                                                                       <div style="font-size:1px;line-height:40px"> </div>
<!-- /////////////////////////////// Image area //////////////////////////// -->
                                                                                       <img align="center" alt="creativegoods" border="0" class="center fixedwidth" src="https://www.fontfolio.net/images/approve.svg" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 432px; display: block;" title="creativegoods" width="432"/>
<!-- /////////////////////////////// image area ends //////////////////////////// -->
                                                                                       <div style="font-size:1px;line-height:40px"> </div>
                                                                                       <!--[if mso]>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
<!-- /////////////////////////////// Text area //////////////////////////// -->
                                                                                    <div style="color:#000000;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                                                       <div style="line-height: 1.5; font-size: 12px; color: #000000; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 18px;">
                                                                                          <p class="headline-text" style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;"><strong>
                                                                                            <span style="font-size: 22px;">Your shop has been approved</span>
                                                                                          </strong></p>
                                                                                       </div>
                                                                                    </div>
<!-- /////////////////////////////// Text area ends //////////////////////////// -->
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 25px; padding-left: 25px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
<!-- /////////////////////////////// content paragrah //////////////////////////// -->
                                                                                    <div style="color:#393d47;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.8;padding-top:10px;padding-right:25px;padding-bottom:10px;padding-left:25px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; color: #393d47; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 22px;">
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 25px; margin: 0;">Congratulations, We have successfully approved your application.</p>
                                                                                       </div>
                                                                                    </div>
 <!-- /////////////////////////////// content paragrah ends here //////////////////////////// -->
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->

                                                                           <div align="center" class="cg-buttons button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                                 <tr>
                                                                                    <td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 20px" align="center">
                                                                                       <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:31.5pt; width:159.75pt; v-text-anchor:middle;" arcsize="22%" stroke="false" fillcolor="#006d77">
                                                                                          <w:anchorlock/>
                                                                                          <v:textbox inset="0,0,0,0">
                                                                                             <center style="color:#ffffff; font-family:Tahoma, sans-serif; font-size:12px">
                                                                                                <![endif]-->
 <!-- /////////////////////////////// Button //////////////////////////// -->
                                                                                                <div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#006d77;border-radius:9px;-webkit-border-radius:9px;-moz-border-radius:9px;width:auto; width:auto;;border-top:0px solid #8a3b8f;border-right:0px solid #8a3b8f;border-bottom:0px solid #8a3b8f;border-left:0px solid #8a3b8f;padding-top:5px;padding-bottom:5px;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:30px;padding-right:25px;font-size:12px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;"><span data-mce-style="font-size: 12px; line-height: 24px;" style="font-size: 12px; line-height: 24px;"><strong>View Dashboard</strong></span></span></span></div>
<!-- /////////////////////////////// Button ends //////////////////////////// -->
                                                                                                <!--[if mso]>
                                                                                             </center>
                                                                                          </v:textbox>
                                                                                       </v:roundrect>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>

                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:#ffffff;">
                                 <div class="block-grid three-up" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#ffffff;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:20px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#006d77;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; color: #006d77; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 19px; margin: 0;"><strong> Quick Links</strong></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="https://www.fontfolio.net/creativegoods/author-guide" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Author guide</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="https://www.fontfolio.net/creativegoods/privacy-policy" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Privacy policy</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="https://www.fontfolio.net/creativegoods/user-terms" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">User terms</a></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#006d77;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; color: #006d77; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 19px; margin: 0;"><strong>Contact</strong></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="https://www.fontfolio.net/help" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Help center</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="mailto:hello@fontfolio.net?subject=Say hello," style="text-decoration: none; color: #393d47;" title="hello@fontfolio.net">hello@fontfolio.net</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;"><a href="https://www.fontfolio.net/support" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Support Center</a></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <div align="left" class="img-container left fixedwidth" style="padding-right: 0px;padding-left: 20px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr style="line-height:0px">
                                                                                    <td style="padding-right: 0px;padding-left: 20px;" align="left">
                                                                                       <![endif]-->
                                                                                       <div style="font-size:1px;line-height:10px"> </div>
                                                                                       <img class="email-clr-logo" alt="Creativegoods Logo" border="0" class="left fixedwidth" src="https://www.fontfolio.net/images/logo1.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 96px; display: block;" title="Logo" width="96"/>
                                                                                       <!--[if mso]>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                                              <tbody>
                                                                                 <tr style="vertical-align: top;" valign="top">
                                                                                    <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                                                       <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 0px; width: 100%;" valign="top" width="100%">
                                                                                          <tbody>
                                                                                             <tr style="vertical-align: top;" valign="top">
                                                                                                <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p dir="rtl" style="font-size: 13px; line-height: 1.8; word-break: break-word; text-align: left; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 25px; margin: 0;">Owned and managed by UniqueEnroute Labs private limited</p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:#f1f1f1;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f1f1f1;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="720" style="background-color:transparent;width:720px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 720px; display: table-cell; vertical-align: top; width: 720px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#555555;font-family:Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; color: #555555; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 11px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 13px; margin: 0;"><span style="font-size: 11px;">© {{ date('Y') }} Fontfolio | All Rights Reserved.</span></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <!--[if (mso)|(IE)]>
                           </td>
                        </tr>
                     </table>
                     <![endif]-->
                  </td>
               </tr>
            </tbody>
         </table>
         <!--[if (IE)]>
      </div>
      <![endif]-->
   </body>
</html>
