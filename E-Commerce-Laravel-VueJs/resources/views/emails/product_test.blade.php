<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
   <head>
      <!--[if gte mso 9]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
      <meta content="width=device-width" name="viewport"/>
      <!--[if !mso]><!-->
      <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
      <!--<![endif]-->
      <title></title>
      <!--[if !mso]><!-->
      <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">
      <!--<![endif]-->
      <style type="text/css">
         body {
         margin: 0;
         padding: 0;
         font-family: 'Work Sans', sans-serif;
         }
         .email-clr-logo {
         margin-top: 15px;
         }
         .footer-body {
         margin-top: 20px;
         }
         .clean-body {
         max-width: 800px;
         }
         table,
         td,
         tr {
         vertical-align: top;
         border-collapse: collapse;
         }
         * {
         line-height: inherit;
         }
         a[x-apple-data-detectors=true] {
         color: inherit !important;
         text-decoration: none !important;
         }
      </style>
      <style id="media-query" type="text/css">
         @media (max-width: 720px) {
         .block-grid,
         .col {
         min-width: 320px !important;
         max-width: 100% !important;
         display: block !important;
         }
         .block-grid {
         width: 100% !important;
         }
         .col {
         width: 100% !important;
         }
         .col>div {
         margin: 0 auto;
         }
         img.fullwidth,
         img.fullwidthOnMobile {
         max-width: 100% !important;
         }
         .no-stack .col {
         min-width: 0 !important;
         display: table-cell !important;
         }
         .no-stack.two-up .col {
         width: 50% !important;
         }
         .no-stack .col.num4 {
         width: 33% !important;
         }
         .no-stack .col.num8 {
         width: 66% !important;
         }
         .no-stack .col.num4 {
         width: 33% !important;
         }
         .no-stack .col.num3 {
         width: 25% !important;
         }
         .no-stack .col.num6 {
         width: 50% !important;
         }
         .no-stack .col.num9 {
         width: 75% !important;
         }
         .video-block {
         max-width: none !important;
         }
         .mobile_hide {
         min-height: 0px;
         max-height: 0px;
         max-width: 0px;
         display: none;
         overflow: hidden;
         font-size: 0px;
         }
         .desktop_hide {
         display: block !important;
         max-height: none !important;
         }
         }
      </style>
   </head>
   <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #f9f9f9;">

         <table bgcolor="#f9f9f9" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f9f9f9; width: 100%;" valign="top" width="100%">
            <tbody>
               <tr style="vertical-align: top;" valign="top">
                  <td style="word-break: break-word; vertical-align: top;" valign="top">

                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">

                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                                              <tbody>
                                                                                 <tr style="vertical-align: top;" valign="top">
                                                                                    <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px;" valign="top">
                                                                                       <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 0px; width: 100%;" valign="top" width="100%">
                                                                                          <tbody>
                                                                                             <tr style="vertical-align: top;" valign="top">
                                                                                                <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </div>
                                                                     </div>
                                                                  </div>

                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:700px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr style="line-height:0px">
                                                                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                                                                   <![endif]-->
<!-- /////////////////////////////// LOGO here   //////////////////////////// -->
                                                                               <a href="{{ url('/') }}" target="_blank">
                                                                                        <img align="center" alt="creativegoods_logo" border="0" class="center fixedwidth" src="{{ asset('images/logo.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 140px; display: block;" title="creativegoods_logo" width="140"/>
                                                                               </a>
<!-- /////////////////////////////// LOGO ends here   //////////////////////////// -->
                                                                                       <!--[if mso]>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:700px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                                              <tbody>
                                                                                 <tr style="vertical-align: top;" valign="top">
                                                                                    <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px;" valign="top">
                                                                                       <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="5" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 5px; width: 100%;" valign="top" width="100%">
                                                                                          <tbody>
                                                                                             <tr style="vertical-align: top;" valign="top">
                                                                                                <td height="5" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #fff;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">

                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <div style="padding-top:60px; padding-bottom:0px; padding-right: 10px; padding-left: 10px;">
                                                                           <div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">

<!-- /////////////////////////////// Banner Image here   //////////////////////////// -->
                                                                                       <img align="center" alt="creativegoods" border="0" class="center fixedwidth mail-product-image" src="{{ $data['image'] }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 432px; display: block;" title="creativegoods" width="432"/>
<!-- /////////////////////////////// image ends here   //////////////////////////// -->
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>

                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:700px">
                                                   <tr class="layout-full-width" style="background-color:#ffffff">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="700" style="background-color:#ffffff;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 10px; padding-left: 10px; padding-top:20px; padding-bottom:25px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:25px; padding-right: 10px; padding-left: 10px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 40px; padding-left: 40px; padding-top: 35px; padding-bottom: 0px; font-family: 'Trebuchet MS', Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#000000; line-height:1.5;padding-top:35px;padding-right:40px;padding-bottom:0px;padding-left:40px;">
                                                                                       <div style="line-height: 1.5; font-size: 12px; color: #000000; mso-line-height-alt: 18px;">
<!-- /////////////////////////////// message subject here   //////////////////////////// -->
                                                                                          <p style="font-size: 22px; line-height: 1.5; text-align: center; word-break: break-word; mso-line-height-alt: 33px; margin: 0;"><span style="font-size: 22px;"><strong>{{ $data['title'] }}</strong></span></p>
<!-- /////////////////////////////// message subject here   //////////////////////////// -->
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 0px; padding-left: 0px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#000000;line-height:1.2;padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:0px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; color: #000000; mso-line-height-alt: 14px;">
<!-- /////////////////////////////// message intro starts here   //////////////////////////// -->
                                                                                          <p style="font-size: 14px; color: #666666; line-height: 1.2; text-align: center; word-break: break-word; mso-line-height-alt: 17px; margin: 0;"><span style="font-size: 14px;">{{ $data['message'] }}</span></p>
<!-- /////////////////////////////// message intro contents here   //////////////////////////// -->
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                                 <tr>
                                                                                    <td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="center">
                                                                                       <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:33pt; width:135pt; v-text-anchor:middle;" arcsize="10%" strokeweight="0.75pt" strokecolor="#006d77" fill="false">
                                                                                          <w:anchorlock/>
                                                                                          <v:textbox inset="0,0,0,0">
                                                                                             <center style="color:#006d77; font-family:'Trebuchet MS', Tahoma, sans-serif; font-size:14px">
                                                                                                <![endif]-->
  <!-- /////////////////////////////// Button //////////////////////////// -->
                                                                               <div style="text-decoration:none;display:inline-block;color:#ffffff; background-color:#006d77;border-radius:9px;-webkit-border-radius:9px;-moz-border-radius:9px;width:auto; width:auto;;border-top:0px solid #8a3b8f;border-right:0px solid #8a3b8f;border-bottom:0px solid #8a3b8f;border-left:0px solid #8a3b8f;padding-top:5px;padding-bottom:5px;text-align:center;mso-border-alt:none;word-break:keep-all;"><a href="{{ $data['button_url'] }}" target="_blank" style="color: #ffffff; "><span style="padding-left:30px;padding-right:25px;font-size:12px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;"><span data-mce-style="font-size: 12px; line-height: 24px;" style="font-size: 12px; line-height: 24px;"><strong>{{ $data['button'] }}</strong></span></span></span></a></div>
 <!-- /////////////////////////////// Button ends //////////////////////////// -->
                                                                                                <!--[if mso]>
                                                                                             </center>
                                                                                          </v:textbox>
                                                                                       </v:roundrect>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                                              <tbody>
                                                                                 <tr style="vertical-align: top;" valign="top">
                                                                                    <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 60%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 30px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                                                       <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid #DDDDDD; width: 100%;" valign="top" width="60%">
                                                                                          <tbody>
                                                                                             <tr style="vertical-align: top;" valign="top">
                                                                                                <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:700px">
                                                   <tr class="layout-full-width" style="background-color:#ffffff">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="700" style="background-color:#ffffff;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 10px; padding-left: 10px; padding-top:0px; padding-bottom:10px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:50px; padding-right: 10px; padding-left: 10px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 50px; padding-left: 50px; padding-top: 0px; padding-bottom: 0px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#666666;line-height:1.8;padding-top:0px;padding-right:50px;padding-bottom:0px;padding-left:50px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; color: #666666; mso-line-height-alt: 22px;">
  <!-- /////////////////////////////// Message //////////////////////////// -->
                                                                                           @if($data['status'])
                                                                                          <p style="line-height: 1.8; word-break: break-word; font-size: 14px; mso-line-height-alt: 25px; margin: 0;">
                                                                                            Dear , We are happy to announce the launch of the premium and exclusive marketplace for creatives. Our goal is to provide high quality usable components to help the ever growing design and graphic needs. We have seen your work and we know that your contribution will help our goal in making this collection. Sellers are exclusively invited.
                                                                                          </p>
                                                                                           @else

                                                                                               <p style="line-height: 1.8; word-break: break-word; font-size: 14px; mso-line-height-alt: 25px; margin: 0;">
                                                                                                   {!! $data['mail_message'] !!}
                                                                                                  </p>
                                                                                               @endif
<!-- /////////////////////////////// Message ends //////////////////////////// -->
                                                                                          <p style="color:#000000; margin-top: 30px; margin-bottom: 0px; line-height: 1.8; word-break: break-word; font-size: 14px; mso-line-height-alt: 25px;">Regards.</p>
                                                                                          <!-- <p style="line-height: 1.8; word-break: break-word; font-size: 14px; mso-line-height-alt: 25px; margin: 0;">Regards.</p> -->
                                                                                          <p style="color:#000000; line-height: 1.8; word-break: break-word; font-size: 14px; mso-line-height-alt: 25px; margin: 0;">Team Fontfolio</p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <!-- Footer -->
                              <div class="footer-body" style="background-color:#ffffff;">
                                 <div class="block-grid three-up" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#ffffff;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:20px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#006d77;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; color: #006d77; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 19px; margin: 0;"><strong> Quick Links</strong></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="{{ url(\App\Traits\HelperTrait::policyLink('author-guide')) }}" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Author Guide</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="{{ url(\App\Traits\HelperTrait::policyLink('privacy-policy'))  }}" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Privacy Policy</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="{{ url(\App\Traits\HelperTrait::policyLink('user-terms')) }}" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">User Terms</a></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#006d77;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; color: #006d77; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 19px; margin: 0;"><strong>Contact</strong></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="{{ url('/help') }}" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Help Center</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="mailto:hello@fontfolio.net?subject=Say hello," style="text-decoration: none; color: #393d47;" title="hello@fontfolio.net">hello@fontfolio.net</a></p>
                                                                                          <p style="font-size: 14px; line-height: 1.8; word-break: break-word; text-align: left; mso-line-height-alt: 25px; margin: 0;"><a href="{{ url('/support') }}" rel="noopener" style="text-decoration: none; color: #393d47;" target="_blank">Support Center</a></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                      <td align="center" width="240" style="background-color:transparent;width:240px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:35px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num4" style="max-width: 320px; min-width: 240px; display: table-cell; vertical-align: top; width: 240px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <div align="left" class="img-container left fixedwidth" style="padding-right: 0px;padding-left: 20px;">
                                                                              <!--[if mso]>
                                                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr style="line-height:0px">
                                                                                    <td style="padding-right: 0px;padding-left: 20px;" align="left">
                                                                                       <![endif]-->
                                                                                        <a href="{{ url('/') }}" target="_blank">
                                                                                       <img class="email-clr-logo" alt="creativegoods_logo" border="0" class="left fixedwidth" src="{{ asset('images/logo1.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 96px; display: block;" title="creativegoods_logo" width="96"/>
                                                                                        </a>
                                                                                            <!--[if mso]>
                                                                                    </td>
                                                                                 </tr>
                                                                              </table>
                                                                              <![endif]-->
                                                                           </div>
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#393d47;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:20px;">
                                                                                       <div style="line-height: 1.8; font-size: 12px; color: #393d47; mso-line-height-alt: 22px;">
                                                                                          <p class="owned-by-text" dir="rtl" style="font-size: 13px; line-height: 1.8; word-break: break-word; text-align: left !important; mso-line-height-alt: 25px; margin: 0;">Owned and managed by UniqueEnroute Labs private limited</p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <div style="background-color:#f1f1f1;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 720px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f1f1f1;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:720px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="720" style="background-color:transparent;width:720px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 720px; display: table-cell; vertical-align: top; width: 720px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <!--[if mso]>
                                                                           <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                              <tr>
                                                                                 <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, sans-serif">
                                                                                    <![endif]-->
                                                                                    <div style="color:#555555;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                                                       <div style="line-height: 1.2; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                                                          <p style="font-size: 11px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 13px; margin: 0;"><span style="font-size: 11px;">© {{ date('Y') }} Fontfolio | All Rights Reserved.</span></p>
                                                                                       </div>
                                                                                    </div>
                                                                                    <!--[if mso]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <!-- footer ends -->
                              <div style="background-color:transparent;">
                                 <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 700px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                       <!--[if (mso)|(IE)]>
                                       <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;">
                                          <tr>
                                             <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width:700px">
                                                   <tr class="layout-full-width" style="background-color:transparent">
                                                      <![endif]-->
                                                      <!--[if (mso)|(IE)]>
                                                      <td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                                         <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                               <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                                                                  <![endif]-->
                                                                  <div class="col num12" style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px;">
                                                                     <div style="width:100% !important;">
                                                                        <!--[if (!mso)&(!IE)]><!-->
                                                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                                           <!--<![endif]-->
                                                                           <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                                              <tbody>
                                                                                 <tr style="vertical-align: top;" valign="top">
                                                                                    <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                                                       <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="5" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 5px; width: 100%;" valign="top" width="100%">
                                                                                          <tbody>
                                                                                             <tr style="vertical-align: top;" valign="top">
                                                                                                <td height="5" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                           <!--[if (!mso)&(!IE)]><!-->
                                                                        </div>
                                                                        <!--<![endif]-->
                                                                     </div>
                                                                  </div>
                                                                  <!--[if (mso)|(IE)]>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                         <![endif]-->
                                                         <!--[if (mso)|(IE)]>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                       <![endif]-->
                                    </div>
                                 </div>
                              </div>
                              <!--[if (mso)|(IE)]>
                           </td>
                        </tr>
                     </table>
                     <![endif]-->
                  </td>
               </tr>
            </tbody>
         </table>
         <!--[if (IE)]>
      </div>
      <![endif]-->
   </body>
</html>
