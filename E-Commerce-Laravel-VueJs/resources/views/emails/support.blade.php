
<head>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">
  <style type="text/css">body{font-family: 'Work Sans', sans-serif;}</style>
</head>
<body style="margin: 0; padding: 0; font-size: 16px;">
  <table style="width: 40%; margin-left: 30%; margin-top: 70px;">
    <tr style="display: inline-block; border: 1px solid #ddd; width: 100%; border-radius: 10px; margin-top: 20px;">
      <td style="font-size: 16px; text-align: left; padding: 15px; color: #000">Name :</td>
      <td style="font-size: 16px; font-weight: 600; color: #000;">{{ $data['from_name'] }}</td>
    </tr>
    <tr style="display: inline-block; border: 1px solid #006d77; width: 100%; border-radius: 10px; margin-top: 20px;">
      <td style="font-size: 16px; text-align: left; padding: 15px; color: #006d77">Email :</td>
        <td style="font-size: 16px; font-weight: 600; color: #000;"><a style="font-size: 16px; font-weight: 600; color: #000;" href="mailto:{{ $data['from_email'] }}">{{ $data['from_email'] }}</a></td>
    </tr>
    <tr style="display: inline-block; background-color: #006d77; width: 100%; border-radius: 10px; margin-top: 20px;">
      <td style="font-size: 16px; color: #fff; text-align: left; padding: 15px;">Topic :</td>
      <td style="font-size: 16px; font-weight: 600; color: #000;">{{ $data['topic'] }}</td>
    </tr>
    <tr style="display: inline-block; border: 1px solid #ddd; width: 100%; border-radius: 10px; margin-top: 20px;">
      <td style="vertical-align: top; font-size: 16px; text-align: left; padding: 15px; padding: 50px 0px 50px 50px;">Message :</td>
      <td style="padding: 50px; font-size: 14px; font-weight: 400; color: #000; line-height: 28px;">{{ $data['message'] }}</td>
    </tr>
    @if($data['document'])
    <tr style="margin-bottom: 5%; display: inline-block; width: 100%; border-radius: 10px; margin-top: 20px;">
      <td style="font-size: 14px; font-weight: 600; color: #000;">Attachments Available</td>
    </tr>
    @endif
  </table>
</body>
