<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- The character set should be utf-8 -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600&display=swap" rel="stylesheet">
        <style type="text/css">body{font-family: 'Work Sans', sans-serif;}</style>
    </head>
</head>

<body>
<!-- <div style="width:767px; margin:0 auto;">
    <div style="width:667px; margin:0 auto; padding-top:20px; padding-bottom:20px;">

        <div style="clear:both; padding-bottom:40px;"></div>
        </section>
        <div style="float:left; width:33.3%; font-size:14px;">
            Date Issued: <strong>25-10-2020</strong><br/>
            Invoice No: <strong>123</strong>
        </div>
        <div style="float:left; width:33.3%;"></div>
        <div style="float:right; width:33.3%; text-align:right;">
            <span style="font-size:24px;">Liju</span><br/>
            email@mail.com<br/>
            thrissur
        </div>
        </section>
        <div style="clear:both;padding-bottom:30px;padding-top:30px;"></div>
        <section style="font-size:11px;">
            <div style="float:left; width:50%;">DESCRIPTION</div>
            <div style="float:left; width:50%; text-align:center;">
                <div style="float:left; width:33.3%;">RATE</div>
                <div style="float:left; width:33.3%;">No. OF UNITS</div>
                <div style="float:left; width:33.3%;">SUBTOTAL</div>
            </div>
        </section>
        <hr/>
        <div style="clear:both; padding-bottom:30px;"></div>
        <section style="font-weight:bold;font-size:16px;">
            <div style="float:left; width:50%; font-size:16px;">Standard Charge</div>
            <div style="float:left; width:50%; text-align:center; font-size:16px;">
                <div style="float:left; width:33.3%;">$34</div>
                <div style="float:left; width:33.3%;">1</div>
                <div style="float:left; width:33.3%;">$34</div>
            </div>
        </section>

    </div>
</div> -->
<table style="width: 50%; margin-left: 25%; margin-top: 70px;">
    <tr style="display: inline-block; border: 1px solid #ddd; width: 100%; background-color:#6136fb; margin-top: 20px; padding-top:10px; padding-bottom:10px;">
        <td style="font-size: 18px; font-weight: 600; display:block; text-align:center; padding: 12px; color: #fff">creativegoods.net</td>
        <td style="font-size: 14px;  display:block; padding-bottom: 15px; text-align:center; color: #fff;">Unique Enroute Labs PVT LTD.</td>
    </tr>
    <tr style="display: inline-block; border: 1px solid #ddd; width: 100%; margin-top: 20px;">
        <td style="font-size: 14px; display:block; text-align:left; padding-left: 15px; padding-top: 20px; padding-bottom: 10px;color: #000">Purchase ID : <span style="font-weight: 600;font-size: 16px;">CG0001</span></td>
        <td style="font-size: 14px;  display:block; padding-bottom: 20px; padding-left: 15px; text-align:left; color: #000;">Purchase Date : <span style="font-weight: 600;font-size: 16px;">29/11/2020</span></td>
    </tr>
</table>

<table style="border: 1px solid #ddd; border-radius:8px; border-collapse: collapse; width: 50%; margin-left: 25%; margin-top: 20px;">
    <tr>
      <th style="border: 1px solid #ddd; border-collapse: collapse; padding: 15px; text-align: left;">Product Name</th>
      <th style=" border: 1px solid #ddd; border-collapse: collapse; padding: 15px; text-align: left;">Price</th>
      <th style=" border: 1px solid #ddd; border-collapse: collapse; padding: 15px; text-align: left;">License</th>
    </tr>
    <tr>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">Aliquam Webfont</td>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">$12</td>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">Regular</td>
    </tr>
    <tr>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">Devant Webfont</td>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">$44</td>
      <td style="border: 1px solid #ddd; border-collapse: collapse; padding:15px;  font-size:14px;">Premium</td>
    </tr>
  </table>

  <table style="width: 50%; margin-left: 25%;">

      <tr style="display: inline-block; border: 1px solid #ddd; width: 100%; margin-top: 20px; margin-bottom: 20px; padding-bottom:20px; padding-top:20px;">
          <th style="font-size: 16px; font-weight: 600; display:block; text-align:left; padding-bottom: 20px; padding-left: 15px; color: #000">Billing Address</th>
          <td style="font-size: 14px;  display:block; padding-bottom: 10px; padding-left: 15px; text-align:left; color: #000;">Address line 1</td>
          <td style="font-size: 14px;  display:block; padding-bottom: 10px; padding-left: 15px; text-align:left; color: #000;">Address line 2</td>
          <td style="font-size: 14px;  display:block; padding-bottom: 10px; padding-left: 15px; text-align:left; color: #000;">Address line 3</td>
      </tr>

      <tr>
        <td style="border: 1px solid #ddd; font-weight: 600; display:block; text-align: right; border-collapse: collapse; padding: 15px;">Total : <span style="font-weight: 600;">$45</span></td>
      </tr>

  </table>
</body>
</html>
