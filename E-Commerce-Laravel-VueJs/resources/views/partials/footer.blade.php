 <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
        <!-- Footer : starts -->
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
        <section id="cg-footer-wrap" class="cg-footer-wrap d-flex justify-content-center align-items-center flex-column">
          <div class="subscribe-newsletter-block d-flex justify-content-center align-items-center flex-column">
            <h3>Don’t miss out on important updates, freebies and exciting offers. Subscribe to our newsletter.</h3>
              {{ Form::open(array('url' => 'news-letter-subscriptions','data-parsley-validate','novalidate',
                                 'files'=>true)) }}
            <input placeholder="Email Address" type="email" class="@error('mail_id') is-invalid @enderror" name="mail_id" @error('mail_id') autofocus @enderror id="mail_id" autocomplete="off" >
              @error('mail_id') <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span> @enderror
            <button type="submit" name="submit_news" class="down-to-more d-flex justify-content-center align-items-center cg-news-letter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
            </button>
              {{ Form::close() }}
          </div>
          <div class="cg-footer-bottom-nav container d-flex justify-content-center align-items-center">
            <div class="brand-ftr-block mr-auto">
              <a href="{{ url('/') }}"><img src="{{asset('images/logo.png')}}" class="img-fluid" alt="fontfolio"></a>
              <p class="footer-para w500 light-grey font1 width-70 f13 pad-top-xsml pad-btm-xsml"> Fontfolio is a highly optimized and focused marketplace for high quality digital assets. We aim to present the best designers and curated collection to get our customers upto speed they deserve.</p>
            </div>
            <div class="nav-ftr-block d-flex justify-content-center align-items-start">
              <ul>
                <li><a href="{{ url('/about-us') }}">About us<span></span></a></li>
                <li><a href="{{ url('/blog') }}">Blog<span></span></a></li>
                <li><a href="{{ url('/all-items?price=free') }}">Free Goods<span></span></a></li>
                <li><a href="{{ url('/handpicked') }}">Handpicked<span></span></a></li>
              </ul>
              <ul>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('user-terms')) }}">User Terms<span></span></a></li>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('author-terms')) }}">Author Terms<span></span></a></li>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('affiliate-terms')) }}">Affiliate Terms<span></span></a></li>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('privacy-policy')) }}">Privacy Policy<span></span></a></li>
              </ul>
              <ul>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('author-guide')) }}">Author Guide<span></span></a></li>
                <li><a href="{{ url(\App\Traits\HelperTrait::policyLink('affiliate-guide')) }}">Affiliate Guide<span></span></a></li>
                <li><a href="{{ url('/discussions') }}">Discussions<span></span></a></li>
                <li><a href="https://www.facebook.com/groups/creativegoods/" target="_blank">Community<span></span></a></li>
              </ul>
              <ul>
                @if(Auth::check() && Auth::user()->is_author)
                     <li><a href="{{ url(Auth::user()->profile_url) }}">My Shop<span></span></a></li>
                @else
                    <li><a href="{{ url('/welcome-shop') }}">Open a Shop<span></span></a></li>
                @endif
                    @if(Auth::check() && Auth::user()->is_affiliate)
                        <li><a href="{{ url(Auth::user()->user_url.'/affiliate') }}">Affiliate Dashboard<span></span></a></li>
                    @else
                        <li><a href="{{ url('/welcome-affiliate') }}"> Become an Affiliate<span></span></a></li>
                    @endif

                <li><a href="{{ url('/license') }}">Licenses<span></span></a></li>
                <li><a href="{{ url('/help') }}">Help Center<span></span></a></li>
              </ul>
            </div>
          </div>
        </section>

        <section class="cg-sub-footer-wrap">
            <div class="container">
                <div class="row d-flex justify-content-between cg-sub-footer-wrap">
                    <div class="cg-sub-footer-para-wrap  mr-auto">
                        <h6 class="first font1 f14 w300"> &copy; {{ date('Y') }} Fontfolio | All Rights Reserved.</h6>
                    </div>
                    <div class="cg-sub-footer-icon-wrap">
                        <ul class="cg-sub-footer-icons d-flex">
                            <li><a href="https://www.facebook.com/fontfolio.net" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
                            <li><a href="https://www.instagram.com/fontfolio_net/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
                            <li><a href="https://twitter.com/fontfolio" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
                            <li><a href="https://www.pinterest.com/fontfolio/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="currentColor" stroke="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><path class="stpin" d="M14.5,0.9c-2.1-1.1-4.4-1.1-6.6-0.5C4,1.4,1.7,5.2,2.5,9c0.3,1.4,0.9,2.6,2.2,3.4c0.5,0.3,0.6,0.3,0.7-0.3
                          		c0.1-0.3,0.2-0.7,0.2-1c0-0.2,0-0.5-0.1-0.6c-1.4-2.2-1-5,0.9-6.8c2.5-2.3,6.9-1.7,8.5,1c1.2,2.1,0.8,5.6-0.9,7.4
                          		c-0.5,0.5-1.1,0.9-1.8,1c-1.8,0.4-2.9-1-2.4-2.5c0.2-0.6,0.4-1.2,0.5-1.8c0.2-0.7,0.4-1.4,0.4-2c0.1-0.9-0.3-1.5-0.9-1.8
                          		c-0.7-0.3-1.5-0.1-2,0.5c-1,1.2-1,2.5-0.6,3.9c0.1,0.3,0.1,0.6,0,0.8C7,11.2,6.8,12.1,6.6,13c-0.5,2.1-1.1,4.2-0.8,6.4
                          		c0.1,0.2,0.3,0.9,1,0.3c1.3-1.8,1.7-3.9,2.2-6c0.7,0.8,1.4,1.2,2.4,1.4c0.9,0.1,1.9,0,2.7-0.4c2.4-1,3.6-3,4.1-5.4
                          		C19.1,5.7,17.6,2.5,14.5,0.9z"/></svg></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
