bm+d <section class="primary-navigation  @guest @else cg-signin-state @endguest">
    <!-- <section class="primary-navigation cg-signout-state"> -->
     @php
         $session_cart_items = Session::get('cart_array');
         $cart_count =0;
         if(Auth::check())$cart_count = \App\Models\Product\Cart::where('user_id', Auth::id())->count();
         if($session_cart_items || $cart_count)$cart_status = true;
         else $cart_status = false;
     @endphp
      <nav class="primary-nav navbar">
        <div class="container d-flex justify-content-between align-items-center pos-rel">

          <a class="cg-logo-wrap" href="{{ url('/') }}"><img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="creativegoods"></a>
          <ul class="top-nav-items d-flex justify-content-between align-items-center">
              @guest
                  @else

                  @php
                      $notifications = Auth::user()->is_have_notifications;
                      $notifications_count = Auth::user()->is_have_unread_notifications;
                      $messages = Auth::user()->is_have_messages;
                      $messages_count = Auth::user()->is_have_unread_messages;
                  @endphp
                 @if($notifications)
              <li class="cg-tiny-nav-icon"  data-tooltip-content="#cg-notify-panel">
             @if($notifications_count) <span class="dot-light"></span>@endif
                 <a href="{{ url(Auth::user()->user_url.'/notifications/all') }}"> <i class="dull-white f16 fas fa-bell"></i></a>
              </li>
           @endif

                  @if($messages)
            <li class="cg-tiny-nav-icon">
                @if($messages_count)  <span class="dot-light"></span> @endif
                <a href="{{ url(Auth::user()->user_url.'/messages') }}" ><i class="dull-white f16 fas fa-envelope"></i></a>
            </li>
                  @endif
              @endguest
            <li class="cg-tiny-nav-icon"><a id="cart" href="{{ url('/cart') }}">@if($cart_status)<span class="dot-light"></span>@endif
                <span class="dull-white f16 fas fa-shopping-cart"></span></a></li>

                  @guest
{{--            <li >--}}
{{--              <a href="{{ url('/account/login') }}" class="account-info-bar d-flex justify-content-between align-items-center">--}}
{{--              <span class="cg-account-sml-login dull-white f16 fas fa-sign-in-alt" href="#"></span>--}}
{{--              <h3 class="first font1 f14 w600">Login</h3>--}}
{{--              </a>--}}
{{--            </li>--}}
                  @else

                      @php
                      $unique_id = Auth::user()->unique_id;
                      @endphp

                      <li class="account-info-bar d-flex justify-content-between align-items-center">
                          <div class="dropdown">
                              <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              </button>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                              <div class="dropdown-menu font1" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item f16 w600 first cg-menu-account-name" href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/profile') }} @endif">{{ \Illuminate\Support\Str::limit(Auth::user()->name, 10, $end='...') }}</a>
                                  <a class="dropdown-item f14" href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/dashboard/my_products') }} @else {{ url(Auth::user()->user_url.'/dashboard/downloads') }} @endif">My Account</a>
                                  @if(Auth::user()->is_author)

                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/my_products') }}">My Items</a>
                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/my_sales') }}">Sales</a>
                                  @endif
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/downloads') }}">Downloads</a>
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/favourites') }}/">Favourites</a>
                                  @if(Auth::user()->is_affiliate)
                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/affiliate') }}">Affiliate</a>
                                  @endif
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/settings') }}">Settings</a>
                                 <a href="{{ route('logout') }}" class="dropdown-item f14 w600 logout-button" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                 </div>
                          </div>
                          <a class="cg-account-sml-dp" href="#"><img src="{{ Auth::user()->user_profile->image_path }}" class="img-fluid" alt="{{ Auth::user()->name }}"></a>
{{--                          <h3 class="first font1 f14 w600">$000.00</h3>--}}
                      </li>
          </ul>
                  @endguest


          <ul class="login-nav-items d-flex justify-content-between align-items-center">
            <li class="cg-login-btn"><a class="f14" href="{{ url('/account/login') }}">Sign In</a></li>
            <li class="cg-register-btn"><a class="f14"href="{{ url('/account/register') }}">Register</a></li>
          </ul>

        </div>
      </nav>
@php
   $product_categories = \App\Models\Product\ProductCategory::active()->get()
@endphp

     @if(request()->segment(count(request()->segments())) != "login" && request()->segment(count(request()->segments())) != "register")

     @if($product_categories->count())
      <!-- category-navigation -->
      <nav class="cg-category-nav">
        <ul class="cg-category-nav-item d-flex justify-content-center align-items-center font1">
          <!-- <li><span class="tooltip first" title="This is my span's tooltip message!">Some text</span></li> -->
         @foreach($product_categories as $product_category)
           <li>
             <a class="first font1 cg-submenu-item" data-tooltip-content="#tooltip_content{{ $product_category->id  }}" href="{{ url('/shop/'.$product_category->slug) }}">{{ $product_category->name  }}</a>
               @if($product_category->product_sub_categories()->count())
             <div class="tooltip_templates">
               <div id="tooltip_content{{ $product_category->id  }}">
                 <ul class="font1">
                     @foreach($product_category->product_sub_categories as $product_sub_category)
                   <li class="f14 cg-tt-drop-item"><a href="{{ url('/shop/'.$product_category->slug.'/'.$product_sub_category->slug) }}" class="grey">{{ $product_sub_category->name  }}</a></li>
                     @endforeach
                 </ul>
               </div>
             </div>
               @endif
           </li>
            @endforeach


        </ul>
      </nav>
     @endif

     @endif

    </section>


    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
    <!-- Mobile Navigation : starts -->
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
    <div class="cg-mob-menu-wrap">
        <div class="cg-mob-content-wrap">

          <ul class="col-md-12 menu-ico-style bd-highlight d-flex align-items-center">
            <li class="mr-auto">
                <a class="cg-mob-logo-wrap" href="{{ url('/') }}">
                    <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="creativegoods">
                </a></li>
              @guest
                  @else
                  @if($notifications)
            <li class="cg-tiny-nav-icon"  data-tooltip-content="#cg-notify-panel">
                @if($notifications_count)  <span class="dot-light"></span>@endif
              <a href="{{ url('user/'.Auth::user()->unique_id.'/notifications/all') }}"> <i class="dull-white f16 fas fa-bell"></i></a>
            </li>
              @endif
                      @if($messages)
                          <li class="cg-tiny-nav-icon">
                              @if($messages_count)  <span class="dot-light"></span> @endif
                              <a href="{{ url('user/'.Auth::user()->unique_id.'/messages') }}" ><i class="dull-white f16 fas fa-envelope"></i></a>
                          </li>
                      @endif
              @endguest
              <li class="cg-tiny-nav-icon"><a id="cart" href="{{ url('/cart') }}">@if($cart_status)<span class="dot-light"></span>@endif
                      <span class="dull-white f16 fas fa-shopping-cart"></span></a></li>
            <li id="nav-icon1"><span></span><span></span><span></span> </li>
          </ul>

        </div>


        <!-- Contents -->
        <div class="mob-menu-wrap">
           <div class="container">
            <div class="cg-container">

              <div class="cg-mob-secondary-search">
                  <form action="{{ url('/shop') }}" method="GET" id="search_form">
                <input class="font1 grey f12 w600 add-btm-xsml" id="search" @if(request()->query('key_word')) value="{{ request()->query('key_word') }}" @endif name="key_word" placeholder="Search here" data-placeholder="Search">
                <i class="fas fa-search f12 grey" onclick="document.getElementById('search_form').submit();"></i>
                  </form>
              </div>
            @guest
              <!-- show when a new user comes or user without login -->
              <div class="cg-mob-log-sign-wrap">
                  <a href="{{url('/account/login')}}"> <button class="first font1 w600 12">Login</button></a>
                  <a href="{{url('/account/register')}}">  <button class="first font1 w600 12">Signup</button></a>
              </div>
              @else
              <!-- ends -->

              <!-- Show when a user signedin only and hide above section cg-mob-log-sign-wrap -->
              <div class="cg-mob-menu-accont-section cg-mob-menu-items-border">
                <h4 class="no-margin pad-btm-sml font1 grey w600 f14 text-left">
                    <a href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/profile') }} @endif">
                        <img src="{{ Auth::user()->user_profile->image_path }}" class="img-fluid cg-account-sml-dp" alt="{{ Auth::user()->name }}">
                        {{ Auth::user()->name }}  <!-- <span class="pad-left-mdm">$000.00</span> -->
                    </a>
                </h4>
                <ul>
                    <li><a class="font1 first f14 w500" href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/dashboard/my_products') }} @else {{ url(Auth::user()->user_url.'/dashboard/downloads') }} @endif">My Account</a></li>

                    @if(Auth::user()->is_author)

                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/my_products') }}">My Items</a></li>
                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/my_sales') }}">Sales</a></li>
                    @endif
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/downloads') }}">Downloads</a></li>
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/favourites') }}/">Favourites</a></li>
                    @if(Auth::user()->is_affiliate)
                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/affiliate') }}">Settings</a></li>
                    @endif
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/settings') }}">Settings</a></li>
                </ul>
              </div>
              <!-- ends -->
            @endguest

                @if($product_categories->count())
              <div class="cg-mob-menu-items-border">
                <h4 class="font1 grey w600 f14 text-left">Categories</h4>
                  @foreach($product_categories as $product_category)
                <div class="dropdown">

                   @if($product_category->product_sub_categories()->count())
                    <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                        <h4 class="no-margin font1 first w500 f14 text-left">{{ $product_category->name }}<i class="drop-ico fas fa-chevron-down f10 add-left-sml first"></i></h4>
                    </button>
                    @else
                       <a href="{{ url($product_category->product_category_url) }}">
                        <button class="dropdown-toggle" type="button" >
                            <h4 class="no-margin font1 first w500 f14 text-left">{{ $product_category->name }}</h4>
                        </button>
                       </a>
                    @endif

                    @if($product_category->product_sub_categories()->count())
                    <div class="dropdown-menu">
                      <ul>
                          @foreach($product_category->product_sub_categories as $product_sub_category)
                              <li><a href="{{ url($product_sub_category->product_sub_category_url) }}" class="font1 grey f14 w500">{{ $product_sub_category->name  }}</a></li>
                          @endforeach
                      </ul>
                    </div>
                    @endif
                </div>
                @endforeach
              </div>
                @endif

              <div class="pad-btm-mdm pad-top-mdm">

                <ul>
                  <li class="font1 first f14 w500">Blog</li>
                  <li class="font1 first f14 w500">Free Goods</li>
                  <li class="font1 first f14 w500">Terms & Conditions</li>
                  <li class="font1 first f14 w500">Help Center</li>
                </ul>

              </div>

                @guest

                @else
             <!-- if user signed in display this section -->
              <div class="cg-mob-logout-wrap">

                  <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <button class="first font1 w600 f14">Log out</button></a>
              </div>
              <!-- ends -->
                @endguest



              </div>
            </div>
        </div>
    </div>
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
        <!--  Mobile Menu Ends -->
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
