 <section class="cg-landing">
          <div class="cg-landing-overlay d-flex justify-content-center align-items-center text-center">
            <h3 class="first font1 w600 f30 pad-top-mdm">Unique & Handpicked collection<br> of Beautiful Fonts.</h3>
              <p>Let's make your project more attractive by choosing Fontfolio.</p>
          </div>
          <div class="cg-landing-search d-flex justify-content-center align-items-center">
              <div class="cg-search-bar">
                  <form action="{{ url('/all-items') }}" method="GET" id="search_form2">
                <input class="font1 cg-home-search-input-section second w600 add-btm-xsml" @if(request()->query('key_word')) value="{{ request()->query('key_word') }}" @endif id="search" name="key_word" placeholder="Search here" data-placeholder="Search">
                <a class="cg-home-search-ico" href="#"  onclick="document.getElementById('search_form2').submit();"><i class="fas fa-search grey"></i></a>
                  </form>

              </div>
          </div>
        </section>
