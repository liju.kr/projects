 <section class="primary-navigation  @guest @else cg-signin-state @endguest">
     @php
         $session_cart_items = Session::get('cart_array');
         $cart_count =0;
         if(Auth::check())$cart_count = \App\Models\Product\Cart::where('user_id', Auth::id())->count();
         if($session_cart_items || $cart_count)$cart_status = true;
         else $cart_status = false;
     @endphp
      <nav class="primary-nav navbar">
        <div class="container d-flex justify-content-between align-items-center pos-rel">

          <a class="cg-logo-wrap" href="{{ url('/') }}">
              @if(request()->is('help*'))
                  {{-- Help Logo  --}}
              <img src="{{asset('images/logo-help.png')}}" class="img-fluid" alt="fontfolio">
                  @elseif(request()->is('read*'))
                  {{-- Privacy Policy Logo  --}}
                  <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="fontfolio">
              @else
                    {{-- Common Logo  --}}
                  <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="fontfolio">
              @endif
          </a>
          <ul class="top-nav-items d-flex justify-content-between align-items-center">
              @guest
                  @else

                  @php
                      $notifications = Auth::user()->is_have_notifications;
                      $notifications_count = Auth::user()->is_have_unread_notifications;
                      $messages = Auth::user()->is_have_messages;
                      $messages_count = Auth::user()->is_have_unread_messages;
                  @endphp
                 @if($notifications && !Auth::user()->is_admin)
              <li class="cg-tiny-nav-icon"  data-tooltip-content="#cg-notify-panel">
             @if($notifications_count) <span class="dot-light"></span>@endif
                 <a href="{{ url(Auth::user()->user_url.'/notifications/all') }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg></a>
              </li>
           @endif

                  @if($messages && !Auth::user()->is_admin)
            <li class="cg-tiny-nav-icon">
                @if($messages_count)  <span class="dot-light"></span> @endif
                <a href="{{ url(Auth::user()->user_url.'/messages') }}" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg></a>
            </li>
                  @endif
              @endguest
            <li class="cg-tiny-nav-icon"><a id="cart" href="{{ url('/cart') }}">@if($cart_status)<span class="dot-light"></span>@endif
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg></a></li>

                  @guest
{{--            <li >--}}
{{--              <a href="{{ url('/account/login') }}" class="account-info-bar d-flex justify-content-between align-items-center">--}}
{{--              <span class="cg-account-sml-login dull-white f16 fas fa-sign-in-alt" href="#"></span>--}}
{{--              <h3 class="first font1 f14 w600">Login</h3>--}}
{{--              </a>--}}
{{--            </li>--}}
                  @else

                      <li class="account-info-bar d-flex justify-content-between align-items-center">
                          <div class="dropdown">
                              <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              </button>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                              <div class="dropdown-menu font1" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item f16 w600 first cg-menu-account-name" href="@if(Auth::user()->is_author) {{ url(Auth::user()->profile_url) }} @endif">{{ Auth::user()->account_short_user_name }}</a>
                                  @if(!Auth::user()->is_admin)
                                  <a class="dropdown-item f14" href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/dashboard/my-products') }} @else {{ url(Auth::user()->user_url.'/dashboard/downloads') }} @endif">My Account</a>
                                  @if(Auth::user()->is_author)

                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/my-products') }}">My Items</a>
                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/my-sales') }}">Sales</a>
                                          <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/create-product') }}">Create Product</a>
                                  @endif
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/downloads') }}">Downloads</a>
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/favourites') }}/">Favourites</a>
                                  @if(Auth::user()->is_affiliate)
                                      <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/affiliate') }}">Affiliate</a>
                                  @endif
                                  <a class="dropdown-item f14" href="{{ url(Auth::user()->user_url.'/dashboard/settings') }}">Settings</a>
                                  @endif
                                 <a href="{{ route('logout') }}" class="dropdown-item f14 w600 logout-button" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                 </div>
                          </div>
                          <a class="cg-account-sml-dp" href="#"><img src="{{ Auth::user()->user_profile->image_path }}" class="img-fluid" alt="{{ Auth::user()->account_short_user_name }}"></a>
                           @if(Auth::user()->is_affiliate || Auth::user()->is_author)<h3 class="first font1 f14 w600">${{ Auth::user()->getWalletBalance() }}</h3> @endif
                      </li>
          </ul>
                  @endguest


          <ul class="login-nav-items d-flex justify-content-between align-items-center">
            <li class="cg-login-btn"><a class="f14" href="{{ url('/login') }}">Sign In</a></li>
            <li class="cg-register-btn"><a class="f14"href="{{ url('/register') }}">Register</a></li>
          </ul>

        </div>
      </nav>
@php
   $product_categories = \App\Models\Product\ProductSubCategory::order()->active()->where('product_category_id', env('PRODUCT_CATEGORY_ID'))->get()
@endphp

     @if(!request()->is('login') && !request()->is('register') && !request()->is('help*') && !request()->is('read*') && !request()->is('license*'))

     @if($product_categories->count())
      <!-- category-navigation -->
      <nav class="cg-category-nav">
        <ul class="cg-category-nav-item d-flex justify-content-center align-items-center font1">
         @foreach($product_categories as $product_category)

         <li class="cg-new-category">
           <div class="dropdown">
             <button onclick="location.href='{{ url($product_category->product_sub_category_url) }}'" class="cg-n-ct btn btn-secondary dropdown-toggle" type="button" id="cg-new-category-navigation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 {{ $product_category->name  }}
             </button>
           </div>
         </li>
            @endforeach
        </ul>
      </nav>
     @endif

     @endif

    </section>


    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
    <!-- Mobile Navigation : starts -->
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->

    <div class="cg-mob-content-wrap">

      <ul class="col-md-12 menu-ico-style bd-highlight d-flex align-items-center">
        <li class="mr-auto">
            <a class="cg-mob-logo-wrap" href="{{ url('/') }}">
                @if(request()->is('help*'))
                    {{-- Help Logo  --}}
                    <img src="{{asset('images/logo-help.png')}}" class="img-fluid" alt="fontfolio">
                @elseif(request()->is('read*'))
                    {{-- Privacy Policy Logo  --}}
                    <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="fontfolio">
                @else
                    {{-- Common Logo  --}}
                    <img src="{{asset('images/logo1.png')}}" class="img-fluid" alt="fontfolio">
                @endif
            </a>
        </li>
          @guest
              @else
              @if($notifications && !Auth::user()->is_admin)
        <li class="cg-tiny-nav-icon"  data-tooltip-content="#cg-notify-panel">
            @if($notifications_count)  <span class="dot-light"></span>@endif
          <a href="{{ url(Auth::user()->user_url.'/notifications/all') }}"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg></a>
        </li>
          @endif
                  @if($messages && !Auth::user()->is_admin)
                      <li class="cg-tiny-nav-icon">
                          @if($messages_count)  <span class="dot-light"></span> @endif
                          <a href="{{ url(Auth::user()->user_url.'/messages') }}" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg></a>
                      </li>
                  @endif
          @endguest
          <li class="cg-tiny-nav-icon"><a id="cart" href="{{ url('/cart') }}">@if($cart_status)<span class="dot-light"></span>@endif
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#777" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg></a></li>
        <li id="nav-icon1"><span></span><span></span><span></span> </li>
      </ul>

    </div>

    <div class="cg-mob-menu-wrap">

        <!-- Contents -->
        <div class="mob-menu-wrap">
           <div class="container execute-right-padding">
            <div class="cg-container">

              <div class="cg-mob-secondary-search">
                  <form action="{{ url('/all-items') }}" method="GET" id="search_form">
                <input class="font1 grey f12 w600 add-btm-xsml" id="search" @if(request()->query('key_word')) value="{{ request()->query('key_word') }}" @endif name="key_word" placeholder="Search here" data-placeholder="Search">
                <div class="cg-mobile-menu-search-icon"><i class="fas fa-search f12 grey" onclick="document.getElementById('search_form').submit();"></i></div>
                  </form>
              </div>
            @guest
              <!-- show when a new user comes or user without login -->
              <div class="cg-mob-log-sign-wrap">
                  <a href="{{url('/login')}}"> <button class="cg-mobile-login-button first font1 w600 12">Login</button></a>
                  <a href="{{url('/register')}}">  <button class="cg-mobile-signup-button first font1 w600 12">Signup</button></a>
              </div>
              @else
              <!-- ends -->

              <!-- Show when a user signedin only and hide above section cg-mob-log-sign-wrap -->
              <div class="cg-mob-menu-accont-section cg-mob-menu-items-border">
                <h4 class="no-margin pad-btm-sml font1 grey w600 f14 text-left">
                    <a href="@if(Auth::user()->is_author) {{ url(Auth::user()->profile_url) }} @endif">
                        <img src="{{ Auth::user()->user_profile->image_path }}" class="img-fluid cg-account-sml-dp" alt="{{ Auth::user()->account_short_user_name }}">
                        {{ Auth::user()->account_short_user_name }}
                    </a>
                </h4>
                <ul class="cg-mob-acc-mini-menu">
                    @if(!Auth::user()->is_admin)
                        @if(Auth::user()->is_affiliate || Auth::user()->is_author )   <li class="cg-account-balance-opt"><a>${{ Auth::user()->getWalletBalance() }}</a></li> @endif
                    <li><a class="font1 first f14 w500" href="@if(Auth::user()->is_author) {{ url(Auth::user()->user_url.'/dashboard/my-products') }} @else {{ url(Auth::user()->user_url.'/dashboard/downloads') }} @endif">My Account</a></li>

                    @if(Auth::user()->is_author)

                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/my-products') }}">My Items</a></li>
                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/my-sales') }}">Sales</a></li>
                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/create-product') }}">Create Product</a></li>

                    @endif
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/downloads') }}">Downloads</a></li>
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/favourites') }}/">Favourites</a></li>
                    @if(Auth::user()->is_affiliate)
                        <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/affiliate') }}">Affiliate</a></li>
                    @endif
                    <li> <a class="font1 first f14 w500" href="{{ url(Auth::user()->user_url.'/dashboard/settings') }}">Settings</a></li>
                    @endif
                    <li class="cg-mobile-logout-btn"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"> <button class="first font1 w600 f14">Log out</button></a></li>
                </ul>
              </div>
              <!-- ends -->
            @endguest

                <div class="cg-mobile-category-nav-wrap cg-mob-menu-items-border">
                    <h4 class="font1 grey w600 f14 text-left">Categories</h4>
                    @foreach($product_categories as $product_category)
                    <div class="dropdown">
                        <button  onclick="location.href='{{ url($product_category->product_sub_category_url) }}'" class="cg-mob-category-item dropdown-toggle" type="button" data-toggle="dropdown">
                            <h4 class="no-margin font1 first w500 f14 text-left"> {{ $product_category->name  }}</h4>
                        </button>
                    </div>
                    @endforeach
                </div>


              <div class="cg-mobile-extra-links">
                <ul>
                  <li class="font1 first f14 w500"><a href="{{ url('blog') }}">Blog</a> </li>
                  <li class="font1 first f14 w500"><a href="{{ url('/all-items?price=free') }}">Free Goods</a></li>
                  <li class="font1 first f14 w500"><a href="">Terms & Conditions</a></li>
                  <li class="font1 first f14 w500"><a href="{{ url('/help') }}">Help Center</a></li>
                </ul>

              </div>

                @guest

                @else
             <!-- if user signed in display this section -->
              <div class="cg-mob-logout-wrap">


              </div>
              <!-- ends -->
                @endguest



              </div>
            </div>
        </div>
    </div>
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
        <!--  Mobile Menu Ends -->
    <!-- ●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬● -->
