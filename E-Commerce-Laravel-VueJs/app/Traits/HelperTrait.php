<?php
namespace App\Traits;
use App\Models\Help\Policy;
use App\Models\Message\AutomaticMessage;
use App\Models\Message\AutomaticNotification;
use App\Models\User\Message;
use App\Models\User\MessageUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

trait HelperTrait
{
    //
public function createSlug($str){

    $delimiter = '-';
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;

}

    public function createSlugDownload($str){

        $delimiter = '_';
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

    public  function sentAutomaticMessage($receiver_id, $message_slug, $message, $automatic_status)
    {
        $me = User::findOrFail(env('CG_BOT_ID'));
        $stranger = User::findOrFail($receiver_id);

        $message_user_me_count = MessageUser::where('my_id', $me->id)
                                ->where('stranger_id', $stranger->id)
                                ->count();



        $message_user_stranger_count = MessageUser::where('my_id', $stranger->id)
            ->where('stranger_id', $me->id)
            ->count();



        if(!$message_user_me_count)
        {
            $message_user_me = new MessageUser();
            $message_user_me->my_id = $me->id;
            $message_user_me->stranger_id = $stranger->id;
            $message_user_me->status =1;
            $message_user_me->save();
        }

        if(!$message_user_stranger_count)
        {
            $message_stranger_me = new MessageUser();
            $message_stranger_me->my_id = $stranger->id;
            $message_stranger_me->stranger_id = $me->id;
            $message_stranger_me->status =1;
            $message_stranger_me->save();
        }

        $message_user_me = MessageUser::where('my_id', $me->id)
            ->where('stranger_id', $stranger->id)
            ->first();
        $message_user_me->last_message_time =date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $message_user_me->save();

        $message_user_stranger = MessageUser::where('my_id', $stranger->id)
            ->where('stranger_id', $me->id)
            ->first();
        $message_user_stranger->last_message_time = date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $message_user_stranger->save();

        if($automatic_status)
        {
             $automatic_message = AutomaticMessage::where('slug',$message_slug)->first();
             if($automatic_message)$message_text = $automatic_message->message;
             else $message_text = "";

        }
        else
        {
            $message_text = $message;
        }


        $message= new Message();
        $message->sender_id = env('CG_BOT_ID');
        $message->receiver_id = $receiver_id;
        $message->message = $message_text;
        $message->save();


        if($message)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public  function automaticToast($message_slug)
    {

          $automatic_toast = AutomaticNotification::where('slug',$message_slug)->first();

        if($automatic_toast)
        {
            return $automatic_toast->message;
        }
        else
        {
            return "";
        }

    }

    public  function automaticMessageText($message_slug)
    {

        $automatic_message = AutomaticMessage::where('slug',$message_slug)->first();
        if($automatic_message)$message_text = $automatic_message->message;
        else $message_text = "";

        return $message_text;

    }

    public static function policyLink($slug)
    {

        $policy = Policy::where('slug',$slug)->first();

        if($policy)
        {
            return $policy->getPolicyURL();
        }
        else
        {
            return "";
        }

    }


    public  function invoiceMail($response)
    {
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(\View::make('pdf.email_invoice')->with('data', $response)->render());
            $pdf_path = storage_path() . '/invoice/' . $response['bookingId'] . '.pdf';
            $mpdf->Output($pdf_path, 'F');


            $data = [
                'from_name' => "Liju",
                'from_email' => "liju@gmail.com",
                'topic' => "Topic",
                'message' => "message",
                'document' => $pdf_path,
            ];
            $to_email = "ljuquarous@gmailcom";
            \Mail::to($to_email)->send(new \App\Mail\ProductInvoice($data));

    }

}
