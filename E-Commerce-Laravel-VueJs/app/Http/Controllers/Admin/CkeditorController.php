<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;

class CkeditorController extends Controller
{
     /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
          if($request->hasFile('upload')) {
//            $originName = $request->file('upload')->getClientOriginalName();
//            $fileName = pathinfo($originName, PATHINFO_FILENAME);
//            $extension = $request->file('upload')->getClientOriginalExtension();
//            $fileName = $fileName.'_'.time().'.'.$extension;
//            $request->file('upload')->move(public_path('storage/uploads/texteditor'), $fileName);

            $cover_image = $request->file('upload');
              $cover_image_name = time().'.'.$cover_image->extension();
              $img = Image::make($cover_image);
              if(Storage::disk('public')->put('uploads/text_editor/'.$cover_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
              {
                  $CKEditorFuncNum = $request->input('CKEditorFuncNum');
                  //$url = asset('storage/uploads/texteditor/'.$fileName);
                  $url = Storage::disk('public')->url('/uploads/text_editor/'.$cover_image_name);
                  $msg = 'Image uploaded successfully';
                  $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
                  @header('Content-type: text/html; charset=utf-8');
                  echo $response;
              }


        }
    }
}
