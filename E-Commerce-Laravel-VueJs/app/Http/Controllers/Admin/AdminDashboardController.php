<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Transaction;
use App\Models\Product\Product;
use App\Models\Product\ProductUpdationFile;
use App\Models\User\AffiliateRequest;
use App\Models\User\AuthorRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    //
    public function getAdminDashboardData()
    {

        $users_count = User::users()->count();
        $authors_count = User::authors()->count();

        $affiliates_count = User::affiliates()->count();
        $affiliate_requests_count = AffiliateRequest::where('status', 0)->count();

        $pending_author_requests_count = AuthorRequest::where('status', 0)->count();

        $users_count_today = User::users()->whereDate('created_at', Carbon::today())->count();
        $authors_count_this_week = User::authors()->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->count();

        $products_count = Product::active()->count();
        $pending_products_count = Product::pending()->count();

        $downloads_count = Download::count();
        $total_sale_amount = Download::sum('total_price');

        $total_author_fee_credit_to_company = Transaction::where('type', 'author_fee')->where('account_type', 'credit')->sum('amount');
        $total_affiliate_fee_credit_to_company = Transaction::where('type', 'affiliate_fee')->where('account_type', 'credit')->sum('amount');
        $total_market_fee_credit_to_company = Transaction::where('type', 'market_fee')->where('account_type', 'credit')->sum('amount');

        $updates_active_count = ProductUpdationFile::active()->count();
        $updates_pending_count = ProductUpdationFile::pending()->count();
        $updates_rejected_count = ProductUpdationFile::rejected()->count();

        $response= [
            'users_count' => $users_count,
            'authors_count' => $authors_count,
            'affiliates_count' => $affiliates_count,
            'affiliate_requests_count' => $affiliate_requests_count,
            'pending_author_requests_count' => $affiliate_requests_count,
            'users_count_today' => $users_count_today,
            'authors_count_this_week' => $authors_count_this_week,
            'products_count' => $products_count,
            'pending_products_count' => $pending_products_count,
            'updates_active_count' => $updates_active_count,
            'updates_pending_count' => $updates_pending_count,
            'updates_rejected_count' => $updates_rejected_count,
            'downloads_count' => $downloads_count,
            'total_sale_amount' => $total_sale_amount,
            'total_author_fee_credit_to_company' => $total_author_fee_credit_to_company,
            'total_affiliate_fee_credit_to_company' => $total_affiliate_fee_credit_to_company,
            'total_market_fee_credit_to_company' => $total_market_fee_credit_to_company,
        ];
       return view('admin/dashboard', compact('response'));
    }

}
