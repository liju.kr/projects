<?php

namespace App\Http\Controllers\Admin\Sales;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index(Request $request)
    {
        $sales = Download::latest()->paginate(10);
        return view('admin/sales/sales-list', compact('sales'));
    }
}
