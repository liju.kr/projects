<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteSettingsController extends Controller
{
    //
    public function getSiteSettingsData()
    {
        return view('admin/settings');
    }

}
