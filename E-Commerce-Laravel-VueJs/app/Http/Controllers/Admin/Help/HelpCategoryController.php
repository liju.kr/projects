<?php

namespace App\Http\Controllers\Admin\Help;

use App\Http\Controllers\Controller;
use App\Models\Help\HelpCategory;
use App\Models\Help\HelpTopic;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;


class HelpCategoryController extends Controller
{
use HelperTrait;
    //

    public function index()
    {
        $help_categories = HelpCategory::latest()->paginate(10);
        $help_categories_all = HelpCategory::latest()->get();
        $help_topics = HelpTopic::latest()->paginate(5);
        return view('admin/help/help_list', compact('help_categories', 'help_topics', 'help_categories_all'));
    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|string|max:150|unique:help_categories',
            'description'=>'required',
            'support_to_mail'=>'email|required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',
        ]);

        $help_category = new HelpCategory();
        $help_category->name = $request->name;
        $help_category->slug = $this->createSlug($request->name);
        $help_category->description = $request->description;
        $help_category->support_to_mail = $request->support_to_mail;
        $help_category->status = 1;
        $help_category->admin_id = Auth::id();
        $help_category->meta_title = $request->meta_title;
        $help_category->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/help/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {

                $help_category->meta_image = $file_name;
            }
        }
         $help_category->save();
         return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'=>'required|string|max:150|unique:help_categories,name,'.$id,
            'description'=>'required',
            'support_to_mail'=>'email|required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',

        ]);

        $help_category = HelpCategory::findOrFail($id);
        $help_category->name = $request->name;
        $help_category->slug = $this->createSlug($request->name);
        $help_category->support_to_mail = $request->support_to_mail;
        $help_category->description = $request->description;
        $help_category->status = 1;
        $help_category->admin_id = Auth::id();
        $help_category->meta_title = $request->meta_title;
        $help_category->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/help/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/help/seo/' . $help_category->meta_image))
                {
                    Storage::disk('public')->delete('uploads/help/seo/' . $help_category->meta_image);
                }
                $help_category->meta_image = $file_name;
            }
        }
        $help_category->save();
        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $help_category = HelpCategory::findOrFail($id);
        $help_category->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
