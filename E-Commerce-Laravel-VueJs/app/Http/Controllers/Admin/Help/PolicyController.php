<?php

namespace App\Http\Controllers\Admin\Help;

use App\Http\Controllers\Controller;
use App\Models\Help\Policy;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class PolicyController extends Controller
{
    use HelperTrait;

    public function index()
    {
        $policies = Policy::latest()->paginate(10);
        return view('admin/help/policy_list', compact('policies'));

    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title'=>'required|string|max:150|unique:policies',
            'description'=>'required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',
        ]);

        $policy = new Policy();
        $policy->title = $request->title;
        $policy->slug = $this->createSlug($request->title);
        $policy->description = $request->description;
        $policy->status = 1;
        $policy->admin_id = Auth::id();
        $policy->meta_title = $request->meta_title;
        $policy->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/policy/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                $policy->meta_image = $file_name;
            }
        }
        $policy->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'=>'required|string|max:150|unique:policies,title,'.$id,
            'description'=>'required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',
        ]);

        $policy = Policy::findOrFail($id);
        $policy->title = $request->title;
        $policy->slug = $this->createSlug($request->title);
        $policy->description = $request->description;
        $policy->status = 1;
        $policy->admin_id = Auth::id();
        $policy->meta_title = $request->meta_title;
        $policy->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/policy/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/policy/seo/'.$policy->meta_image))
                {
                Storage::disk('public')->delete('uploads/policy/seo/'.$policy->meta_image);
                }
                $policy->meta_image = $file_name;
            }
        }
        $policy->save();
        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $policy = Policy::findOrFail($id);
        $policy->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
