<?php

namespace App\Http\Controllers\Admin\Help;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AutomaticMessageController extends Controller
{
    public function index()
    {
        $automatic_messages = AutomaticMessage::latest()->paginate(20);
        return view('admin/help/automatic_messages_list', compact('automatic_messages'));

    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'slug'=>'required|string|unique:automatic_messages',
            'message'=>'required',
        ]);

        $automatic_message= new AutomaticMessage();
        $automatic_message->slug = $request->slug;
        $automatic_message->message = $request->message;
        $automatic_message->admin_id = Auth::id();
        $automatic_message->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'slug'=>'required|string|unique:automatic_messages,slug,'.$id,
            'message'=>'required',
        ]);

        $automatic_message = AutomaticMessage::findOrFail($id);
        $automatic_message->slug = $request->slug;
        $automatic_message->message = $request->message;
        $automatic_message->admin_id = Auth::id();
        $automatic_message->save();

        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $automatic_message = AutomaticMessage::findOrFail($id);
        $automatic_message->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
