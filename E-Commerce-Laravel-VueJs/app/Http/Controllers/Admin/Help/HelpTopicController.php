<?php

namespace App\Http\Controllers\Admin\Help;

use App\Http\Controllers\Controller;
use App\Models\Help\HelpTopic;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class HelpTopicController extends Controller
{
    //
    use HelperTrait;

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title'=>'required|string|max:150|unique:help_topics',
            'help_category'=>'required',
            'description'=>'required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',
        ]);

        $help_topic = new HelpTopic();
        $help_topic->title = $request->title;
        $help_topic->help_category_id = $request->help_category;
        $help_topic->slug = $this->createSlug($request->title);
        $help_topic->description = $request->description;
        $help_topic->status = 1;
        $help_topic->faq_status = 1;
        $help_topic->admin_id = Auth::id();
        $help_topic->meta_title = $request->meta_title;
        $help_topic->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/help/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                $help_topic->meta_image = $file_name;
            }
        }
        $help_topic->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'=>'required|string|max:150|unique:help_topics,title,'.$id,
            'help_category'=>'required',
            'description'=>'required',
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',

        ]);

        $help_topic = HelpTopic::findOrFail($id);
        $help_topic->title = $request->title;
        $help_topic->help_category_id = $request->help_category;
        $help_topic->slug = $this->createSlug($request->title);
        $help_topic->description = $request->description;
        $help_topic->status = 1;
        $help_topic->faq_status = 1;
        $help_topic->admin_id = Auth::id();
        $help_topic->meta_title = $request->meta_title;
        $help_topic->meta_description = $request->meta_description;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/help/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/help/seo/' . $help_topic->meta_image))
                {
                    Storage::disk('public')->delete('uploads/help/seo/' . $help_topic->meta_image);
                }
                $help_topic->meta_image = $file_name;
            }
        }

        $help_topic->save();
        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $help_topic = HelpTopic::findOrFail($id);
        $help_topic->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
