<?php

namespace App\Http\Controllers\Admin\Help;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AutomaticNotificationController extends Controller
{
    public function index()
    {
        $automatic_notifications = AutomaticNotification::latest()->paginate(20);
        return view('admin/help/automatic_notifications_list', compact('automatic_notifications'));

    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'slug'=>'required|string|unique:automatic_notifications',
            'message'=>'required',
        ]);

        $automatic_notification= new AutomaticNotification();
        $automatic_notification->slug = $request->slug;
        $automatic_notification->message = $request->message;
        $automatic_notification->admin_id = Auth::id();
        $automatic_notification->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'slug'=>'required|string|unique:automatic_notifications,slug,'.$id,
            'message'=>'required',
        ]);

        $automatic_notification = AutomaticNotification::findOrFail($id);
        $automatic_notification->slug = $request->slug;
        $automatic_notification->message = $request->message;
        $automatic_notification->admin_id = Auth::id();
        $automatic_notification->save();

        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $automatic_notification = AutomaticNotification::findOrFail($id);
        $automatic_notification->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
