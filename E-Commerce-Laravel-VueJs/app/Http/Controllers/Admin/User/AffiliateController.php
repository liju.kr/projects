<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticMail;
use App\Models\User\AffiliateRequest;
use App\Models\User\UserProfile;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AffiliateController extends Controller
{
    use HelperTrait;
    //
    public function index()
    {
        $affiliate_requests = AffiliateRequest::with('user', 'admin')
            ->latest()
            ->paginate(10);
        return view('admin/affiliate_requests/affiliate_requests-list', compact('affiliate_requests'));
    }

    public function show(Request $request, $id)
    {
        $affiliate_request = AffiliateRequest::findOrFail($id);
        return view('admin/affiliate_requests/affiliate_requests-detail', compact('affiliate_request'));
    }

    public function getReviewDetail(Request $request, $id)
    {
        $affiliate_request = AffiliateRequest::findOrFail($id);
        return view('admin/affiliate_requests/affiliate_requests-review', compact('affiliate_request'));
    }

    public function updateAffiliateStatus(Request $request, $id)
    {

        $this->validate($request, [
            'status'=>'required',
            'message'=>'required_if:status,2',
            'mail_message'=>'required_if:status,2'
        ]);

        $affiliate_request = AffiliateRequest::findOrFail($id);

        $affiliate_request->status = $request->status;
        if($request->message)$affiliate_request->message = $request->message;
        $affiliate_request->admin_id = Auth::id();
        $affiliate_request->save();

        if($request->status ==1)
        {
            $user = User::findOrFail($affiliate_request->user_id);
            $user->affiliate_status=1;
            $user->save();
            $user_profile =UserProfile::where('user_id',$affiliate_request->user_id)->first();
            $user_profile->affiliate_payout_email = $affiliate_request->payout_email;
            $user_profile->save();
            $this->sentAutomaticMessage($affiliate_request->user_id, "affiliate_approved", "", true);
            $data = [
                'name' => $user->name,
                'image' => asset('images/email_status/approve.png'),
                'subject' => "Your Affiliate Application has been Approved",
                'main_head' => "Affiliate Application Approved",
                'sub_head' => "Yeah! You got a Green flag.",
                'title' => "Congratulations ".$user->name.",",
                'message' => "Set up your affiliate account and start promoting Fontfolio assets",
                'status' => "1",
                'mail_message' => "",
                'button' => "View Dashboard",
                'button_url' => url($user->user_url.'/affiliate'),
            ];
            $to_email = $user->email;
            \Mail::to($to_email)->send(new \App\Mail\ShopNotification($data));
        }
        elseif($request->status ==2)
        {
            $user = User::findOrFail($affiliate_request->user_id);
            $user->affiliate_status=$request->status;
            $user->save();
            $this->sentAutomaticMessage($affiliate_request->user_id, "", $request->message, false);

            $automatic_mail = new AutomaticMail();
            $automatic_mail->user_id = $user->id;
            $automatic_mail->admin_id = Auth::id();
            $automatic_mail->email = $user->email;
            $automatic_mail->subject = "Your Affiliate Application has been Rejected";
            $automatic_mail->mail_message = $request->mail_message;
            $automatic_mail->save();

            $data = [
                'name' => $user->name,
                'image' => asset('images/email_status/reject.png'),
                'subject' => "Your Affiliate Application has been Rejected",
                'main_head' => "Affiliate Application Rejected",
                'sub_head' => "oops! it's rejection.",
                'title' => "Sorry ".$user->name.",",
                'message' => "Unfortunately, Your affiliate application has been rejected. Your portfolio doesn't meet the quality defined by Fontfolio.",
                'status' => "0",
                'mail_message' => $request->mail_message,
                'button' => "Go To Home",
                'button_url' => url('/'),
            ];
            $to_email = $user->email;
            \Mail::to($to_email)->send(new \App\Mail\ShopNotification($data));

        }
        else
        {

        }

        if($affiliate_request)
            return redirect('/cg_admin/affiliate_requests?page='.$request->page)->with('flash_message_success', 'Affiliate Status Updated');
        else
            return redirect('/cg_admin/affiliate_requests?page='.$request->page)->with('flash_message_error', 'Please Try Again Later');
    }


    public function destroy(Request $request, $id)
    {
        $product_category = AffiliateRequest::findOrFail($id);
        $product_category->delete();
        return redirect('/cg_admin/affiliate_requests?page='.$request->page)->with('flash_message_success', 'Deleted Successfully');

    }
}
