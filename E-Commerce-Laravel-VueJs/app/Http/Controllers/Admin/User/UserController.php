<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticMail;
use App\Models\Order\Download;
use App\Models\Product\Product;
use App\Models\User\AuthorRequest;
use App\Models\User\Notification;
use App\Models\User\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Traits\HelperTrait;

class UserController extends Controller
{
    use HelperTrait;
    public function getUsers(Request $request, $type)
    {
        $users = User::latest();
        if(isset($request->key) && isset($request->key_word))
        {
            $key=$request->key;
            $key_word=$request->key_word;

            if($key=="name")
            {
                $users = $users->where('name', 'like', '%' .$key_word . '%');
            }
            elseif($key=="store_name")
            {
                $ids = UserProfile::where('store_name', 'like', '%' .$key_word . '%')->pluck('user_id');
                $users->whereIn('id', $ids);
            }
            else {
            }
        }
        else
        {
            $key_word = null;
            $key = null;
        }

        if($type=="user")
        {
            $users = $users->users();
            $page_title = "Users";
        }
        elseif($type=="author")
        {
            $users = $users->authors();
            $page_title = "Authors";
        }
        elseif($type=="affiliate")
        {
            $users = $users->affiliates();
            $page_title = "Affiliates";
        }
        else
        {
            return abort(404);
        }

        $users = $users->paginate(10);
        return view('admin/users/users-list', compact('users', 'page_title', 'type', 'key', 'key_word'));
    }


    public function getUserDetail($type, $user_id, $tab=null)
    {
        $user = User::findOrFail($user_id);

        if($tab==null || $tab=="profile") {
            $notifications = Notification::with('product','owner','user.user_profile' )
                ->where('owner_id',$user->id )->latest()->paginate(10);
            $purchase_count = Download::where('user_id', $user->id)->count();
            $sales_count = Download::where('author_id', $user->id)->count();
            $active_products_count = Product::where('author_id', $user->id)->active()->count();
            return view('admin/users/user-view', compact('user', 'type', 'tab', 'notifications', 'purchase_count', 'sales_count', 'active_products_count'));
        }
        elseif($tab=="messages")
        {
            return view('admin/users/user-view', compact('user', 'type', 'tab'));
        }
        elseif($tab=="products")
        {
            $active_products = Product::where('author_id', $user->id)->active()->paginate(6);
            $pending_products = Product::where('author_id', $user->id)->pending()->paginate(6);
            $rejected_products = Product::where('author_id', $user->id)->rejected()->paginate(6);
            return view('admin/users/user-view', compact('user', 'type', 'tab', 'active_products', 'pending_products', 'rejected_products'));
        }
        elseif($tab=="purchase")
        {
            $purchases = Download::where('user_id', $user->id)->latest()->paginate(10);
            return view('admin/users/user-view', compact('user', 'type', 'tab', 'purchases'));
        }
        elseif($tab=="sales")
        {
            $sales = Download::where('author_id', $user->id)->latest()->paginate(10);
            return view('admin/users/user-view', compact('user', 'type', 'tab', 'sales'));
        }
        elseif($tab=="chat")
        {
            return view('admin/users/user-view', compact('user', 'type', 'tab'));
        }
        elseif($tab=="details")
        {
            return view('admin/users/user-view', compact('user', 'type', 'tab'));
        }
        else
        {
            return abort(404);
        }



    }


    public function getAuthorRequests()
    {
        $author_requests = AuthorRequest::where('status', '!=', 1)
            ->latest()
            ->paginate(10);
        return view('admin/author_requests/author_requests-list', compact('author_requests'));
    }

    public function getAuthorReviewDetail(Request $request, $id)
    {
        $author_request = AuthorRequest::findOrFail($id);
        return view('admin/author_requests/author_requests-review', compact('author_request'));
    }

    public function updateUserAuthorStatus(Request $request, $id)
    {

        $this->validate($request, [
            'status'=>'required',
            'message'=>'required_if:status,2',
            'mail_message'=>'required_if:status,2'
        ]);

        $author_request = AuthorRequest::findOrFail($id);

        $author_request->status = $request->status;
        if($request->message)$author_request->message = $request->message;
        $author_request->admin_id = Auth::id();


        if($request->status ==1)
        {
            $author_request->author_update_on = Carbon::now();
            $user = User::findOrFail($author_request->user_id);
            $user->role="author";
            $user->save();
            $this->sentAutomaticMessage($author_request->user_id, "author_approved", "", true);

            $data = [
                'name' => $user->name,
                'image' => asset('images/email_status/approve.png'),
                'subject' => "Your Shop has been Approved",
                'main_head' => "Shop Approved",
                'sub_head' => "Yeah! You got a Green flag.",
                'title' => "Congratulations ".$user->name.",",
                'message' => "Set up your shop and start publishing your assets.",
                'status' => "1",
                'mail_message' => "",
                'button' => "View Dashboard",
                'button_url' => url($user->user_url.'/dashboard/my-products'),
            ];
            $to_email = $user->email;
            \Mail::to($to_email)->send(new \App\Mail\ShopNotification($data));

        }
        elseif($request->status ==2)
        {
            $user = User::findOrFail($author_request->user_id);
            $author_request->author_update_on = Carbon::now();
            $this->sentAutomaticMessage($author_request->user_id, "", $request->message, false);

            $automatic_mail = new AutomaticMail();
            $automatic_mail->user_id = $user->id;
            $automatic_mail->admin_id = Auth::id();
            $automatic_mail->email = $user->email;
            $automatic_mail->subject = "Your shop has been Rejected";
            $automatic_mail->mail_message = $request->mail_message;
            $automatic_mail->save();

            $data = [
                'name' => $user->name,
                'image' => asset('images/email_status/reject.png'),
                'subject' => "Your Shop has been Rejected",
                'main_head' => "Shop Rejected",
                'sub_head' => "oops! it's rejection.",
                'title' => "Sorry ".$user->name.",",
                'message' => "Unfortunately, Your store application has been rejected. Your portfolio doesn't meet the quality defined by Fontfolio.",
                'status' => "0",
                'mail_message' => $request->mail_message,
                'button' => "Go To Home",
                'button_url' => url('/'),
            ];
            $to_email = $user->email;
            \Mail::to($to_email)->send(new \App\Mail\ShopNotification($data));
        }
        else
        {

        }
//        if($request->status !=1)
//        {
//            $user = User::findOrFail($author_request->id);
//            $user->role="user";
//            $user->save();
//        }

        $author_request->save();
        if($author_request)
            return redirect('/cg_admin/author_requests?page='.$request->page)->with('flash_message_success', 'Author Status Updated');
        else
            return redirect('/cg_admin/author_requests?page='.$request->page)->with('flash_message_error', 'Please Try Again Later');
    }

    public function enableDisable(Request $request, $type, $id)
    {
        $user = User::findOrFail($id);
        $status = 1;
        if($request->status == 1) $status = 0;

        $user->status = $status;
        $user->save();

        if($user)
            return redirect()->back()->with('flash_message_success', 'Successfully Updated');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');
    }


}
