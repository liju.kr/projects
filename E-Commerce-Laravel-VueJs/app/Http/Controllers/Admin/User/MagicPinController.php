<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\User\MagicPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MagicPinController extends Controller
{

    public function index(Request $request)
    {
        $magic_pins = MagicPin::orderBy('id', 'desc');
        if(isset($request->key))
        {
            $key=$request->key;

            if($key=="all")
            {

            }
            elseif($key=="active")
            {
                $magic_pins = $magic_pins->where('status', 1);
            }
            elseif($key=="in-active")
            {
                $magic_pins = $magic_pins->where('status', 0);
            }
            elseif($key=="used")
            {
                $magic_pins = $magic_pins->where('user_id', '!=', null)->where('status', 1);
            }
            elseif($key=="not-used")
            {
                $magic_pins = $magic_pins->where('user_id', null)->where('status', 1);
            }
            else{
            }
        }
        else
        {
            $key=null;
        }
        $magic_pins = $magic_pins->paginate(16);
        return view('admin/help/magic_pin_list', compact('magic_pins', 'key'));

    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'secret_pin'=>'required|numeric|min:10001|max:99999|unique:magic_pins',
            'status'=>'required',
        ]);

        $magic_pin= new MagicPin();
        $magic_pin->secret_pin = $request->secret_pin;
        $magic_pin->assign_to = $request->assign_to;
        $magic_pin->status = $request->status;
        $magic_pin->admin_id = Auth::id();
        $magic_pin->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'secret_pin'=>'required|numeric|min:10001|max:99999|unique:magic_pins,secret_pin,'.$id,
            'status'=>'required',
        ]);

        $magic_pin = MagicPin::findOrFail($id);
        $magic_pin->secret_pin = $request->secret_pin;
        $magic_pin->assign_to = $request->assign_to;
        $magic_pin->status = $request->status;
        $magic_pin->admin_id = Auth::id();
        $magic_pin->save();

        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $magic_pin = MagicPin::findOrFail($id);
        $magic_pin->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
