<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Http\Controllers\Controller;
use App\Models\Order\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::latest()->paginate(10);
        return view('admin/accounts/invoice_list', compact('invoices'));

    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title'=>'required',
            'account_type'=>'required',
            'bill_date'=>'date|required',
            'amount'=>'required|numeric|min:0',
        ]);

        $invoice= new Invoice();
        $invoice->title = $request->title;
        $invoice->description = $request->description;
        $invoice->amount = $request->amount;
        $invoice->type = "manual";
        $invoice->account_type = $request->account_type;
        $invoice->bill_no = $request->bill_no;
        $invoice->bill_date = $request->bill_date;
        $invoice->admin_id = Auth::id();
        $invoice->save();
        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'=>'required',
            'account_type'=>'required',
            'bill_date'=>'date|required',
            'amount'=>'required|numeric|min:0',
        ]);

        $invoice = Invoice::findOrFail($id);
        $invoice->title = $request->title;
        $invoice->description = $request->description;
        $invoice->amount = $request->amount;
        $invoice->type = "manual";
        $invoice->account_type = $request->account_type;
        $invoice->bill_no = $request->bill_no;
        $invoice->bill_date = $request->bill_date;
        $invoice->admin_id = Auth::id();
        $invoice->save();

        return redirect()->back()->with('flash_message_success', 'Update Successfully');
    }

    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
