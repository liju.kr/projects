<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\ApplicationSupport;
use App\Models\Product\FileType;
use App\Models\Product\ProductCategory;
use App\Models\Product\Property;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCategoryController extends Controller
{
use HelperTrait;
    public function index()
    {
        //
        $product_categories = ProductCategory::order()->paginate(10);
        return view('admin/product_categories/product_categories-list', compact('product_categories'));

    }


    public function create()
    {
        //
        $properties = Property::active()->latest()->get();
        $file_types = FileType::active()->latest()->get();
        $application_supports = ApplicationSupport::active()->latest()->get();
        $sort_last = ProductCategory::max('sort');
        $sort = $sort_last + 10;
        return view('admin/product_categories/product_categories-add_edit', compact('properties', 'file_types', 'application_supports', 'sort'));
    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|string|max:150|unique:product_categories',
            'external_link_status'=>'required',
            'extract_status'=>'required',
            'sub_title_1'=>'required',
            'sub_title_2'=>'required',
            'status'=>'required',
            'sort'=>'required|numeric|min:0',
        ]);

        $product_category = new ProductCategory();
        $product_category->name = $request->name;
        $product_category->slug = $this->createSlug($request->name);
        $product_category->external_link_status = $request->external_link_status;
        $product_category->extract_status = $request->extract_status;
        $product_category->sub_title_1 = $request->sub_title_1;
        $product_category->sub_title_2 = $request->sub_title_2;
        $product_category->status = $request->status;
        $product_category->sort = $request->sort;
        $product_category->admin_id = Auth::id();
        $product_category->save();
        $product_category->properties()->attach($request->properties);
        $product_category->file_types()->attach($request->file_types);
        $product_category->application_supports()->attach($request->application_supports);
        return redirect()->back()->with('flash_message_success', 'Added Successfully');


    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $product_category = ProductCategory::findOrFail($id);
        $properties = Property::active()->latest()->get();
        $file_types = FileType::active()->latest()->get();
        $application_supports = ApplicationSupport::active()->latest()->get();
        $assignedProperties  = $product_category->admin_properties->pluck('id')->toArray();
        $assignedFileTypes  = $product_category->admin_file_types->pluck('id')->toArray();
        $assignedApplicationSupports  = $product_category->admin_application_supports->pluck('id')->toArray();
        $sort_last = ProductCategory::max('sort');
        $sort = $sort_last + 10;

        return view('admin/product_categories/product_categories-add_edit', compact( 'product_category', 'properties', 'file_types', 'application_supports', 'assignedApplicationSupports', 'assignedFileTypes', 'assignedProperties', 'sort'));

    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name'=>'required|string|max:150|unique:product_categories,name,'.$id,
            'external_link_status'=>'required',
            'extract_status'=>'required',
            'sub_title_1'=>'required',
            'sub_title_2'=>'required',
            'status'=>'required',
            'sort'=>'required|numeric|min:0',
        ]);
        $product_category = ProductCategory::findOrFail($id);
        $product_category->name = $request->name;
        $product_category->slug = $this->createSlug($request->name);
        $product_category->external_link_status = $request->external_link_status;
        $product_category->extract_status = $request->extract_status;
        $product_category->sub_title_1 = $request->sub_title_1;
        $product_category->sub_title_2 = $request->sub_title_2;
        $product_category->status = $request->status;
        $product_category->sort = $request->sort;
        $product_category->admin_id = Auth::id();
        $product_category->properties()->sync($request->properties);
        $product_category->file_types()->sync($request->file_types);
        $product_category->application_supports()->sync($request->application_supports);
        $product_category->save();
        return redirect('/cg_admin/product_categories?page='.$request->page)->with('flash_message_success', 'Updated Successfully');

    }


    public function destroy($id)
    {
        $product_category = ProductCategory::findOrFail($id);
        $product_category->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');

    }
}
