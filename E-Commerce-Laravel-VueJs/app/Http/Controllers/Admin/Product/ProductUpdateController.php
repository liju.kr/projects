<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticMail;
use App\Models\Product\FontPreview;
use App\Models\Product\Product;
use App\Models\Product\ProductUpdationFile;
use App\Models\User\UserProfile;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductUpdateController extends Controller
{
    use HelperTrait;
    //
    public function index($product_id=null, $status=null)
    {
        $product_updates = ProductUpdationFile::with('product');

        if($product_id && $status)
        {
            $product_updates = $product_updates->where('product_id', $product_id);
            if($status=="active") $product_updates = $product_updates->active();
            if($status=="pending") $product_updates = $product_updates->pending();
            if($status=="rejected") $product_updates = $product_updates->rejected();
        }
        if($product_id)
        {

            if($product_id=="active") $product_updates = $product_updates->active();
            elseif($product_id=="pending") $product_updates = $product_updates->pending();
            elseif($product_id=="rejected") $product_updates = $product_updates->rejected();
            else $product_updates->where('product_id', $product_id);
        }

        $product_updates = $product_updates->latest()
            ->paginate(10);
        return view('admin/products/product_updates-list', compact('product_updates'));
    }

    public function getReviewDetail(Request $request, $id)
    {
        $product_update = ProductUpdationFile::findOrFail($id);
        return view('admin/products/product_updates-review', compact('product_update'));
    }

    public function updateProductStatus(Request $request, $id)
    {
        $this->validate($request, [
            'status'=>'required',
            'message'=>'required_if:status,2',
            'mail_message'=>'required_if:status,2',
        ]);

        $product_update = ProductUpdationFile::findOrFail($id);
        $product_update->status = $request->status;
        $product_update->admin_id = Auth::id();
        $product_update->approved_on = Carbon::now();
        $product_update->save();

        if($product_update->status==1)
        {
            $this->sentAutomaticMessage($product_update->product->author->id, "", "Yeah! Your Product ".$product_update->product->name." Update has been Approved", false);
            $data = [
                'name' => $product_update->product->author->name,
                'image' => $product_update->product->cover_photo_path,
                'subject' => "Your Product Update has been Approved",
                'main_head' => "",
                'sub_head' => "",
                'title' => "Your Product Update has been Approved",
                'message' => "Hi ".$product_update->product->author->name.", Your ".$product_update->product->name." Update has been Approved.",
                'status' => "1",
                'mail_message' => "",
                'button' => "View Product",
                'button_url' => url($product_update->product->author->user_url.'/dashboard/my-products/live'),
            ];
            $to_email = $product_update->product->author->email;
            \Mail::to($to_email)->send(new \App\Mail\ProductNotification($data));
        }

        if($product_update->status==2)
        {
            $this->sentAutomaticMessage($product_update->product->author->id, "", $request->message, false);
            $automatic_mail = new AutomaticMail();
            $automatic_mail->user_id = $product_update->product->author->id;
            $automatic_mail->product_id = $product_update->product->id;
            $automatic_mail->admin_id = Auth::id();
            $automatic_mail->email = $product_update->product->author->email;
            $automatic_mail->subject = "Your Product Update has been Rejected";
            $automatic_mail->mail_message = $request->mail_message;
            $automatic_mail->save();
            $data = [
                'name' => $product_update->product->author->name,
                'image' => $product_update->product->cover_photo_path,
                'subject' => "Your Product Update has been Rejected",
                'main_head' => "",
                'sub_head' => "",
                'title' => "Your Product Update has been Rejected",
                'message' => "Hi ".$product_update->product->author->name.", Your ".$product_update->product->name." Update has been Rejected.",
                'status' => "0",
                'mail_message' => $request->mail_message,
                'button' => "View Product",
                'button_url' => url($product_update->product->author->user_url.'/dashboard/my-products/live'),
            ];
            $to_email = $product_update->product->author->email;
            \Mail::to($to_email)->send(new \App\Mail\ProductNotification($data));
        }

        if($product_update)
            return redirect('cg_admin/products/'.$product_update->product->id)->with('flash_message_success', 'Product Status Updates');
        else
            return redirect('cg_admin/products/'.$product_update->product->id)->with('flash_message_error', 'Please Try Again Later');
    }

    public function updateProductUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'product_file' => 'required|mimes:zip|max:2048000',
        ]);
        $product_file = $request->file('product_file');
        $product_id = $request->get('product_id');

        if($request->hasfile('product_file'))
        {
            $product_file_name = time().'.'.$product_file->extension();
            $product_update = ProductUpdationFile::findOrFail($id);
            if(Storage::disk('public')->put('uploads/products/product_files_updated/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/products/product_files_updated/'.$product_update->product_file))
                {
                    Storage::disk('public')->delete('uploads/products/product_files_updated/'.$product_update->product_file);
                }
                $product_update->admin_id = Auth::id();
                $product_update->product_file = $product_file_name;
            }

            $product_update->save();
        }

        if($product_update)
            return redirect()->back()->with('flash_message_success', 'Successfully Updated');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }

    public function destroy(Request $request, $id)
    {
        $product_update = ProductUpdationFile::findOrFail($id);
        $product_update->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');
    }

}
