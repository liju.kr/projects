<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\ApplicationSupport;
use App\Models\Product\FileType;
use App\Models\Product\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductPropertyController extends Controller
{
    //
   public function  index($property_name)
   {
       if($property_name=="application_supports")
       {
           $product_properties = ApplicationSupport::latest();
           $product_property_title = "Application Supports";
       }
       elseif ($property_name=="file_types")
       {
           $product_properties = FileType::latest();
           $product_property_title = "File Types";
       }
       elseif ($property_name=="properties")
       {
           $product_properties = Property::latest();
           $product_property_title = "Properties";
       }
       else
       {

       }

       $product_properties = $product_properties->paginate(10);
       return view('admin/product_properties/product_properties-list', compact('product_properties', 'product_property_title', 'property_name'));
   }

    public function  create($property_name)
    {
        if($property_name=="application_supports")
        {
            $product_property_title = "Application Supports";
        }
        elseif ($property_name=="file_types")
        {
            $product_property_title = "File Types";
        }
        elseif ($property_name=="properties")
        {
            $product_property_title = "Properties";
        }
        else
        {

        }
        return view('admin/product_properties/product_properties-add_edit', compact('product_property_title', 'property_name'));
    }

    public function  edit(Request $request, $property_name, $id)
    {
        if($property_name=="application_supports")
        {
            $product_property = ApplicationSupport::findOrFail($id);
            $product_property_title = "Application Supports";
        }
        elseif ($property_name=="file_types")
        {
            $product_property = FileType::findOrFail($id);
            $product_property_title = "File Types";
        }
        elseif ($property_name=="properties")
        {
            $product_property = Property::findOrFail($id);
            $product_property_title = "Properties";
        }
        else
        {

        }
        return view('admin/product_properties/product_properties-add_edit', compact('product_property', 'product_property_title', 'property_name'));
    }

    public function store(Request $request, $property_name)
    {
        $this->validate($request, [
            'name'=>'required|string|max:150',
            'status'=>'required',
            'svg'=>'required',
            'default_status'=>'required',
        ]);

        if($property_name=="application_supports")
        {
            $product_property = new ApplicationSupport();
        }
        elseif ($property_name=="file_types")
        {
            $product_property = new FileType();
        }
        elseif ($property_name=="properties")
        {
            $product_property = new Property();
        }
        else
        {

        }
        $product_property->name = $request->name;
        $product_property->status = $request->status;
        $product_property->default_status = $request->default_status;
        $product_property->svg = $request->svg;
        $product_property->admin_id = Auth::id();
        $product_property->save();

        return redirect()->back()->with('flash_message_success', 'Added Successfully');
    }

    public function  update(Request $request, $property_name, $id)
    {
        $this->validate($request, [
            'name'=>'required|string|max:150',
            'status'=>'required',
            'svg'=>'required',
            'default_status'=>'required',
        ]);

        if($property_name=="application_supports")
        {
            $product_property = ApplicationSupport::findOrFail($id);
        }
        elseif ($property_name=="file_types")
        {
            $product_property = FileType::findOrFail($id);
        }
        elseif ($property_name=="properties")
        {
            $product_property = Property::findOrFail($id);
        }
        else
        {

        }
        $product_property->name = $request->name;
        $product_property->status = $request->status;
        $product_property->default_status = $request->default_status;
        $product_property->svg = $request->svg;
        $product_property->admin_id = Auth::id();
        $product_property->save();

        return redirect('/cg_admin/product_properties/'.$property_name.'?page='.$request->page)->with('flash_message_success', 'Updated Successfully');
    }


    public function destroy(Request $request, $property_name, $id)
    {

        if($property_name=="application_supports")
        {
            $product_property = ApplicationSupport::findOrFail($id);
        }
        elseif ($property_name=="file_types")
        {
            $product_property = FileType::findOrFail($id);
        }
        elseif ($property_name=="properties")
        {
            $product_property = Property::findOrFail($id);
        }
        else
        {

        }
        $product_property->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');
    }
}
