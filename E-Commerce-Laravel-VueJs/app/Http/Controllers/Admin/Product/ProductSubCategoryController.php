<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\ApplicationSupport;
use App\Models\Product\FileType;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductSubCategory;
use App\Models\Product\Property;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductSubCategoryController extends Controller
{
    use HelperTrait;
    public function index()
    {
        $product_categories = ProductCategory::order()->paginate(10);
        return view('admin/product_sub_categories/product_sub_categories-list', compact('product_categories'));

    }


    public function create()
    {
        $product_categories = ProductCategory::order()->get();
        $properties = Property::active()->latest()->get();
        $file_types = FileType::active()->latest()->get();
        $application_supports = ApplicationSupport::active()->latest()->get();
        $sort_last = ProductSubCategory::max('sort');
        $sort = $sort_last + 10;
        return view('admin/product_sub_categories/product_sub_categories-add_edit', compact('product_categories', 'properties', 'file_types', 'application_supports', 'sort'));

    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'product_category'=>'required',
            'name'=>'required|string|max:150|unique:product_sub_categories',
            'status'=>'required',
            'sort'=>'required|numeric|min:0',
        ]);

        $product_sub_category = new ProductSubCategory();
        $product_sub_category->name = $request->name;
        $product_sub_category->slug = $this->createSlug($request->name);
        $product_sub_category->status = $request->status;
        $product_sub_category->sort = $request->sort;
        $product_sub_category->product_category_id = $request->product_category;
        $product_sub_category->admin_id = Auth::id();
        $product_sub_category->save();
        $product_sub_category->properties()->attach($request->properties);
        $product_sub_category->file_types()->attach($request->file_types);
        $product_sub_category->application_supports()->attach($request->application_supports);
        return redirect()->back()->with('flash_message_success', 'Added Successfully');

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $product_categories = ProductCategory::latest()->get();
        $product_sub_category = ProductSubCategory::findOrFail($id);
        $properties = Property::active()->latest()->get();
        $file_types = FileType::active()->latest()->get();
        $application_supports = ApplicationSupport::active()->latest()->get();
        $assignedProperties  = $product_sub_category->admin_properties->pluck('id')->toArray();
        $assignedFileTypes  = $product_sub_category->admin_file_types->pluck('id')->toArray();
        $assignedApplicationSupports  = $product_sub_category->admin_application_supports->pluck('id')->toArray();

        $sort_last = ProductSubCategory::max('sort');
        $sort = $sort_last + 10;

        return view('admin/product_sub_categories/product_sub_categories-add_edit', compact('product_sub_category', 'product_categories', 'properties', 'file_types', 'application_supports', 'assignedApplicationSupports', 'assignedFileTypes', 'assignedProperties', 'sort'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_category'=>'required',
            'name'=>'required|string|max:150|unique:product_sub_categories,name,'.$id,
            'status'=>'required',
            'sort'=>'required|numeric|min:0',
        ]);

        $product_sub_category = ProductSubCategory::findOrFail($id);
        $product_sub_category->name = $request->name;
        $product_sub_category->slug = $this->createSlug($request->name);
        $product_sub_category->status = $request->status;
        $product_sub_category->sort = $request->sort;
        $product_sub_category->product_category_id = $request->product_category;
        $product_sub_category->admin_id = Auth::id();
        $product_sub_category->properties()->sync($request->properties);
        $product_sub_category->file_types()->sync($request->file_types);
        $product_sub_category->application_supports()->sync($request->application_supports);
        $product_sub_category->save();
        return redirect('/cg_admin/product_sub_categories?page='.$request->page)->with('flash_message_success', 'Updated Successfully');


    }


    public function destroy($id)
    {
        //
        $product_sub_category = ProductSubCategory::findOrFail($id);
        $product_sub_category->delete();
        return redirect()->back()->with('flash_message_success', 'Deleted Successfully');
    }
}
