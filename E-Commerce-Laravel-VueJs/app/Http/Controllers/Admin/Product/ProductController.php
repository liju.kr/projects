<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Message\AutomaticMail;
use App\Models\Order\Download;
use App\Models\Product\ApplicationSupport;
use App\Models\Product\FileType;
use App\Models\Product\FontPreview;
use App\Models\Product\Preview;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductSubCategory;
use App\Models\Product\ProductUpdationFile;
use App\Models\Product\Property;
use App\Models\User\UserProfile;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response as Downloads;
use Illuminate\Support\Facades\Storage;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Illuminate\Support\Facades\Input;
use ZipArchive;
use Image;
use DB;


class ProductController extends Controller
{
    use HelperTrait;
    //
    public function index(Request $request)
    {
        $products = Product::with('product_sub_category.product_category', 'product_category', 'author', 'admin');
        if(isset($request->key) && isset($request->key_word))
        {
            $key=$request->key;
            $key_word=$request->key_word;

            if($key=="product_name")
            {
                $products = $products->where('name', 'like', '%' .$key_word . '%');
            }
            elseif($key=="store_name")
            {
                $ids = UserProfile::where('store_name', 'like', '%' .$key_word . '%')->pluck('user_id');
                $products->whereIn('author_id', $ids);
            }
            else {
            }
        }
        else
        {
            $key_word = null;
            $key = null;
        }

        $products = $products->latest()
                            ->paginate(10);
        return view('admin/products/products-list', compact('products', 'key', 'key_word'));
    }

    public function show(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        return view('admin/products/products-detail', compact('product'));
    }

    public function edit(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product_categories = ProductCategory::with('product_sub_categories')->active()->get();
        $properties = Property::active()->latest()->get();
        $file_types = FileType::active()->latest()->get();
        $application_supports = ApplicationSupport::active()->latest()->get();

        $assignedProperties  = $product->properties->pluck('id')->toArray();
        $assignedFileTypes  = $product->file_types->pluck('id')->toArray();
        $assignedApplicationSupports  = $product->application_supports->pluck('id')->toArray();

        return view('admin/products/products-edit', compact('product', 'product_categories', 'properties', 'assignedProperties', 'file_types', 'assignedFileTypes', 'application_supports', 'assignedApplicationSupports'));
    }

    public function getMetaDetail(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        return view('admin/products/products-seo', compact('product'));
    }


    public function getReviewDetail(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        return view('admin/products/products-review', compact('product'));
    }

    public function updateProductStatus(Request $request, $id)
    {

        $this->validate($request, [
            'status'=>'required',
            'message'=>'required_if:status,2|required_if:status,0',
            'mail_message'=>'required_if:status,2|required_if:status,0',
            'font_files' => 'sometimes',
            'font_files.*' => 'max:10240',
        ]);
        $product = Product::findOrFail($id);
        $old_product_status = $product->status;
        if($request->status == 1 && $product->product_category->extract_status == 1 && !$product->font_previews()->count() && !$request->file('font_files')) return redirect()->back()->with('flash_message_error', 'Please Select Font Files (TTF)')->withInput($request->input());

        $font_files = $request->file('font_files');
        if(!empty($font_files))
        {
            foreach ($font_files as $key=>$font_file)
            {
                if($font_file->getClientOriginalExtension() =='ttf' || $font_file->getClientOriginalExtension() =='TTF')
                {
                }
                else
                {
                    return redirect()->back()->with('flash_message_error', 'Please Select TTF File (Font Files)')->withInput($request->input());
                }
            }
        }

        $product->status = $request->status;
        $product->admin_id = Auth::id();
        if($request->status_changed_on)
        {
           $converted_date_time = date('Y-m-d H:i:s', strtotime($request->status_changed_on));
            $product->status_changed_on = $converted_date_time;
        }
        else
        {
            $converted_date_time = date('Y-m-d H:i:s', strtotime(Carbon::now()));
            $product->status_changed_on = $converted_date_time;
        }
        $product->save();

        $offer_status = $product->offer_status;
        $offer_status->subscriptions_status = $request->subscriptions_status;
        $offer_status->subscriptions_edit = $request->subscriptions_edit;
        $offer_status->freebies_status = $request->freebies_status;
        $offer_status->freebies_edit = $request->freebies_edit;
        $offer_status->offers_status = $request->offers_status;
        $offer_status->offers_edit = $request->offers_edit;
        $offer_status->user_id =Auth::id();
        $offer_status->save();


        if($product->status==1 && $product->product_category->extract_status==1 && $old_product_status != 1 && !$product->font_previews()->count())
        {
            /* file extraction
            $za = new ZipArchive();
            $zipFileName = $product->product_file;
            // $file = storage_path().'/app/public/uploads/products/product_files/'.$product->product_file;
            $file = Storage::disk('public')->url('uploads/products/product_files/'.$product->product_file);
            //$preview = storage_path().'/app/public/uploads/products/preview';
            $preview = Storage::disk('public')->url('uploads/products/preview');
            $dir = $preview.'/__MACOSX/';
            $za->open($file);

               for($i = 0; $i < $za->numFiles; $i++)
                {
                    $OnlyFileName = $za->getNameIndex($i);
                    $FullFileName = $za->statIndex($i);
                    if ($FullFileName['name'][strlen($FullFileName['name'])-1] =="/")
                    {
                        if ( !file_exists( $preview."/".$FullFileName['name'] ) && !is_dir( $preview."/".$FullFileName['name'] ) )
                         {
                           mkdir($preview."/".$FullFileName['name'], 0700);
                         }
                 }
                }
                for($i = 0; $i < $za->numFiles; $i++)
                 {
                     $OnlyFileName = $za->getNameIndex($i);
                     $FullFileName = $za->statIndex($i);

                      if (!($FullFileName['name'][strlen($FullFileName['name'])-1] =="/") && !glob("__MACOSX"))
                            {
                                if (preg_match('#\.(otf|ttf|fnt|woff)$#i', $OnlyFileName))
                                {
                                   copy('zip://'. $file .'#'. $OnlyFileName , $preview."/".$FullFileName['name'] );
                                    // $za->extractTo($preview);
                                }
                            }
                 }
                    // to remove __MACOSX hidden files
                    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
                    $files = new RecursiveIteratorIterator($it,
                                 RecursiveIteratorIterator::CHILD_FIRST);
                    foreach($files as $file) {
                        if ($file->isDir()){
                            rmdir($file->getRealPath());
                        } else {
                            unlink($file->getRealPath());
                        }
                    }
                    rmdir($dir);
                    // to remove __MACOSX hidden files
                $za->close();
              file extraction   */

            if(!empty($font_files))
            {
                foreach ($font_files as $key=>$font_file)
                {
                   // return $font_file->getClientMimeType();
                     $preview_name = time().$key.'.'.$font_file->getClientOriginalExtension();
                    $name_array = explode('.', $font_file->getClientOriginalName());
                    $name = $name_array[0];
                          if(Storage::disk('public')->put('uploads/products/font_previews/'.$product->id.'/'.$preview_name, file_get_contents($font_file), ['visibility' => 'public']))
                    {
                        $font_preview= new FontPreview();
                        $font_preview->product_id = $product->id;
                        $font_preview->file = $preview_name;
                        $font_preview->name = $name;
                        $font_preview->admin_id = Auth::id();
                        $font_preview->save();
                    }

                }
            }

        }

        if($product->status==1 && $old_product_status !=1)
        {
            $this->sentAutomaticMessage($product->author->id, "", "Yeah! Your Product ".$product->name." has been Approved", false);
            $data = [
                'name' => $product->author->name,
                'image' => $product->cover_photo_path,
                'subject' => "Your Product has been Approved",
                'main_head' => "",
                'sub_head' => "",
                'title' => "Your Product has been Approved",
                'message' => "Hi ".$product->author->name.", Your ".$product->name." has been Approved.",
                'status' => 1,
                'mail_message' => "",
                'button' => "View Product",
                'button_url' => url($product->author->user_url.'/dashboard/my-products/live'),
            ];
            $to_email = $product->author->email;
            \Mail::to($to_email)->send(new \App\Mail\ProductNotification($data));
        }

        if($product->status == 2 && $old_product_status != 2)
        {
            $this->sentAutomaticMessage($product->author->id, "", $request->message, false);
            $automatic_mail = new AutomaticMail();
            $automatic_mail->user_id = $product->author->id;
            $automatic_mail->product_id = $product->id;
            $automatic_mail->admin_id = Auth::id();
            $automatic_mail->email = $product->author->email;
            $automatic_mail->subject = "Your Product has been Rejected";
            $automatic_mail->mail_message = $request->mail_message;
            $automatic_mail->save();
            $data = [
                'name' => $product->author->name,
                'image' => $product->cover_photo_path,
                'subject' => "Your Product has been Rejected",
                'main_head' => "",
                'sub_head' => "",
                'title' => "Your Product has been Rejected",
                'message' => "Hi ".$product->author->name.", Your ".$product->name." has been Rejected.",
                'status' => 0,
                'mail_message' => $request->mail_message,
                'button' => "View Product",
                'button_url' => url($product->author->user_url.'/dashboard/my-products/rejected'),
            ];
            $to_email = $product->author->email;
            \Mail::to($to_email)->send(new \App\Mail\ProductNotification($data));
        }

                if($product->status == 0 && $old_product_status != 0)
                {
                $automatic_mail = new AutomaticMail();
                $automatic_mail->user_id = $product->author->id;
                $automatic_mail->product_id = $product->id;
                $automatic_mail->admin_id = Auth::id();
                $automatic_mail->email = $product->author->email;
                $automatic_mail->subject = "Your Product is Under Review";
                $automatic_mail->mail_message = $request->mail_message;
                $automatic_mail->save();
            $this->sentAutomaticMessage($product->author->id, "", $request->message, false);
            $data = [
                'name' => $product->author->name,
                'image' => $product->cover_photo_path,
                'subject' => "Your Product is Under Review",
                'main_head' => "",
                'sub_head' => "",
                'title' => "Your Product is Under Review",
                'message' => "Hi ".$product->author->name.", Your ".$product->name." is Under Review",
                'status' => 0,
                'mail_message' => $request->mail_message,
                'button' => "View Product",
                'button_url' => url($product->author->user_url.'/dashboard/my-products/pending'),
            ];
            $to_email = $product->author->email;
            \Mail::to($to_email)->send(new \App\Mail\ProductNotification($data));
        }


        if($product)
        return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_success', 'Successfully Updated');
        else
        return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_error', 'Please Try Again Later');
    }

    public function updateProductMetaStatus(Request $request, $id)
    {

        $this->validate($request, [
            'meta_title'=>'max:100',
            'meta_description'=>'max:200',
            'meta_image'=>'mimes:jpeg,jpg,png,gif|max:1024',
        ]);
        $product = Product::findOrFail($id);
        $meta_tag = $product->meta_tag;
        $meta_tag->meta_title = $request->meta_title;
        $meta_tag->meta_description = $request->meta_description;
        $meta_tag->meta_key_words = $request->meta_key_words;
        if($request->hasfile('meta_image'))
        {
            $file = $request->file('meta_image');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file);
            if(Storage::disk('public')->put('uploads/products/seo/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('/uploads/products/seo/' . $meta_tag->meta_image))
                {
                    Storage::disk('public')->delete('uploads/products/seo/'.$meta_tag->meta_image);
                }
                $meta_tag->meta_image = $file_name;
            }
        }
        $meta_tag->save();

        if($meta_tag)
            return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_success', 'Successfully Updated');
        else
            return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_error', 'Please Try Again Later');

    }

    public function downloadByAdmin(Request $request)
    {
        $user = Auth::user();
        $product = Product::find($request->i);
        if($user->isAnyAdmin())
        {
            $headers = [
                'Content-Type'        => 'Content-Type: application/zip',
                'Content-Disposition' => 'attachment; filename="FF_'.$this->createSlugDownload($product->name).'_'.date('d-m-Y').'_'.date('H:i').'.'.$product->file_type.'"',
            ];
            return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files/'.$product->product_file), 200, $headers);
            //  return response()->json($product->product_file_path);
        }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast('try_again_later'));
        }

    }

    public function updateDownloadByAdmin(Request $request)
    {
        $user = Auth::user();
        $product_update = ProductUpdationFile::find($request->i);
        if($user->isAnyAdmin())
        {
            $headers = [
                'Content-Type'        => 'Content-Type: application/zip',
                'Content-Disposition' => 'attachment; filename="cg_'.$this->createSlugDownload($product_update->product->name).'_update_on_'.date('d-m-Y', strtotime($product_update->created_at)).'_'.date('H:i', strtotime($product_update->created_at)).'.'.$product_update->file_type.'"',
            ];
            return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files_updated/'.$product_update->product_file), 200, $headers);
            //  return response()->json($product->product_file_path);
        }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast('try_again_later'));
        }

    }


    public function updateProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|unique:products,name,'.$product->id,
            'product_price' => 'required|numeric|min:0',
            'tag_line' => 'required',
            'product_license' => 'required',
            'description' => 'required|string|min:250',
            //'product_tags' => 'required',
            'cover_image' => 'mimes:jpeg,jpg,png,gif|max:2048',
            'product_file' => 'mimes:zip|max:2048000',
        ]);
        $cover_image = $request->file('cover_image');
        $product_file = $request->file('product_file');

        $product->name = $request->get('name');
        $product->slug = $this->createSlug($request->get('name'));
        $product->product_license = $request->get('product_license');
        $product->price = $request->get('product_price');
        $product->description = $request->get('description');
        if($request->get('product_tags') == null || $request->get('product_tags') == "" || $request->get('product_tags') == "null")$product->product_tags = NULL; else $product->product_tags = $request->get('product_tags');
        if($request->get('live_preview_link') == null || $request->get('live_preview_link') == "" || $request->get('live_preview_link') == "null")$product->external_url = NULL; else $product->external_url = $request->get('live_preview_link');
        $product->tag_line = $request->get('tag_line');

        if($request->get('properties'))
        {
            $product->properties()->sync($request->get('properties'));
        }
        if($request->get('application_supports'))
        {
            $product->application_supports()->sync($request->get('application_supports'));
        }
        if($request->get('file_types'))
        {
            $product->file_types()->sync($request->get('file_types'));
        }

        if($request->hasfile('product_file')) {
                $product_file_name = time().'.'.$product_file->extension();
                if(Storage::disk('public')->put('uploads/products/product_files/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
                {
//                    if(Storage::disk('public')->exists('uploads/products/product_files/'.$product->product_file))
//                    {
//                        Storage::disk('public')->delete('uploads/products/product_files/'.$product->product_file);
//                    }
                    $product->product_file = $product_file_name;
                }
        }


        if($request->hasfile('cover_image'))
        {
            $cover_image_name = time().'.'.$cover_image->extension();
            $img = Image::make($cover_image)->fit(2038, 1359, function ($constraint) { });
            $thumb_nail = Image::make($cover_image)->fit(400, 267, function ($constraint) { });
            if(Storage::disk('public')->put('uploads/products/cover_photos/'.$cover_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/products/cover_photos/'.$product->cover_photo))
                {
                    Storage::disk('public')->delete('uploads/products/cover_photos/'.$product->cover_photo);
                }
                if(Storage::disk('public')->exists('uploads/products/cover_photos/thumb/'.$product->cover_photo))
                {
                    Storage::disk('public')->delete('uploads/products/cover_photos/'.$product->cover_photo);
                }
                if(Storage::disk('public')->put('uploads/products/cover_photos/thumb/'.$cover_image_name, $thumb_nail->stream()->__toString(), ['visibility' => 'public']))
                {
                    $product->cover_photo = $cover_image_name;
                }
            }
        }

        $product->save();
        if($product)
            return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_success', 'Successfully Updated');
        else
            return redirect('/cg_admin/products?page='.$request->page)->with('flash_message_error', 'Please Try Again Later');

    }

    public function updateProductPreview(Request $request, $id)
    {
        $preview = Preview::findOrFail($id);
        $this->validate($request, [
            'preview_image' => 'required|mimes:jpeg,jpg,png,gif|max:2048',
        ]);
        $preview_image = $request->file('preview_image');


        if($request->hasfile('preview_image'))
        {
            $preview_image_name = time().'.'.$preview_image->extension();
            $img = Image::make($preview_image)->fit(2038, 1359, function ($constraint) { });
            if(Storage::disk('public')->put('uploads/products/preview_photos/'.$preview_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/products/preview_photos/'.$preview->preview_image))
                {
                    Storage::disk('public')->delete('uploads/products/preview_photos/'.$preview->preview_image);
                }
                $preview->preview_image =$preview_image_name;
            }
        }

        $preview->save();
        if($preview)
            return redirect()->back()->with('flash_message_success', 'Successfully Updated');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }

    public function storeProductPreview(Request $request)
    {

        $this->validate($request, [
            'preview_image' => 'required|mimes:jpeg,jpg,png,gif|max:2048',
        ]);
        $preview_image = $request->file('preview_image');

        if($request->hasfile('preview_image'))
        {
            $preview_image_name = time().'.'.$preview_image->extension();
            $img = Image::make($preview_image)->fit(2038, 1359, function ($constraint) { });
            if(Storage::disk('public')->put('uploads/products/preview_photos/'.$preview_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                $preview = new Preview();
                $preview->preview_image =$preview_image_name;
                $preview->product_id = $request->get('product_id');
                $preview->save();
            }
        }

        if($preview)
            return redirect()->back()->with('flash_message_success', 'Successfully Add');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }


    public function destroyProductPreview(Request $request, $id)
    {
        $preview = Preview::findOrFail($id);
        if(Storage::disk('public')->exists('uploads/products/preview_photos/'.$preview->preview_image))
        {
            Storage::disk('public')->delete('uploads/products/preview_photos/'.$preview->preview_image);
        }
        $preview->forceDelete();
        if($preview)
            return redirect()->back()->with('flash_message_success', 'Successfully Deleted');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }


    public function updateFontPreview(Request $request, $id)
    {
        $font = FontPreview::findOrFail($id);
        $this->validate($request, [
            'font_preview' => 'required|max:1024',
        ]);
        $font_preview = $request->file('font_preview');
        $product_id = $request->get('product_id');

        if($request->hasfile('font_preview'))
        {
            $preview_name = time().'.'.$font_preview->getClientOriginalExtension();
            $name_array = explode('.', $font_preview->getClientOriginalName());
            $name = $name_array[0];
            if(Storage::disk('public')->put('uploads/products/font_previews/'.$product_id.'/'.$preview_name, file_get_contents($font_preview), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/products/font_previews/'.$product_id.'/'.$font->file))
                {
                    Storage::disk('public')->delete('uploads/products/font_previews/'.$product_id.'/'.$font->file);
                }
                $font->file = $preview_name;
                $font->name = $name;
                $font->admin_id = Auth::id();
                $font->save();
            }
        }

        if($font)
            return redirect()->back()->with('flash_message_success', 'Successfully Updated');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }

    public function storeFontPreview(Request $request)
    {
        $this->validate($request, [
            'font_preview' => 'required|max:1024',
        ]);
        $font_preview = $request->file('font_preview');
        $product_id = $request->get('product_id');

        if($request->hasfile('font_preview'))
        {
            $preview_name = time().'.'.$font_preview->getClientOriginalExtension();
            $name_array = explode('.', $font_preview->getClientOriginalName());
            $name = $name_array[0];
            if(Storage::disk('public')->put('uploads/products/font_previews/'.$product_id.'/'.$preview_name, file_get_contents($font_preview), ['visibility' => 'public']))
            {
                $font = new FontPreview();
                $font->file = $preview_name;
                $font->name = $name;
                $font->product_id = $product_id;
                $font->admin_id = Auth::id();
                $font->save();
            }
        }

        if($font)
            return redirect()->back()->with('flash_message_success', 'Successfully Add');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }




    public function destroyFontPreview(Request $request, $id)
    {
        $font = FontPreview::findOrFail($id);
        $product_id = $request->get('product_id');
        if(Storage::disk('public')->exists('uploads/products/font_previews/'.$product_id.'/'.$font->file))
        {
            Storage::disk('public')->delete('uploads/products/font_previews/'.$product_id.'/'.$font->file);
        }
        $font->forceDelete();
        if($font)
            return redirect()->back()->with('flash_message_success', 'Successfully Deleted');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');

    }


    public function updateProductCategory(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'product_category' => 'required',
            'product_sub_category' => 'required',
        ]);

        $category_id = $request->get('product_category');
        $sub_category_id = $request->get('product_sub_category');

        $category = ProductCategory::findOrFail($category_id);
        $sub_category = ProductSubCategory::findOrFail($sub_category_id);

        if($category->id == $sub_category->product_category_id)
        {

            DB::table('product_properties')->where('product_id', $id)->delete();
            DB::table('product_file_types')->where('product_id', $id)->delete();
            DB::table('product_application_supports')->where('product_id', $id)->delete();

            $product->product_category_id = $category_id;
            $product->product_sub_category_id = $sub_category_id;
            $product->save();
            if($product)
                return redirect()->back()->with('flash_message_success', 'Successfully Updated');
            else
                return redirect()->back()->with('flash_message_error', 'Please Try Again Later');
        }
        else
        {
            return redirect()->back()->with('flash_message_error', 'Category Mismatch');
        }



    }

    public function enableDisable(Request $request, $id)
    {
      $product = Product::findOrFail($id);
      $status = 1;
      if($request->enable_status == 1) $status = 0;

      $product->enable_status = $status;
      $product->save();

        if($product)
            return redirect()->back()->with('flash_message_success', 'Successfully Updated');
        else
            return redirect()->back()->with('flash_message_error', 'Please Try Again Later');
    }


}
