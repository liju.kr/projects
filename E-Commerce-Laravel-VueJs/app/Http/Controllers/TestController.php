<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class TestController extends Controller
{
    public function create()
    {
        return view('main/test');
    }

    public function fileUpload(Request $request)
    {
        $this->validate($request, ['image' => 'required']);
        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $name=time().$file->getClientOriginalName();
            $filePath = 'images/' . $name;


            //Storage::disk('public')->put($filePath, file_get_contents($file));
            Storage::disk('public')->put($filePath, fopen($file, 'r+'));
            return back()->with('success','Image Uploaded successfully');
        }
    }
}
