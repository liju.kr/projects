<?php

namespace App\Http\Controllers;

use App\Models\Product\AffiliateClick;
use App\Models\Product\Product;
use App\Models\User\AffiliateUser;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    use HelperTrait;

    public function __construct()
    {
       // $this->middleware('auth');
    }


    public function index(Request $request)
    {

        if(isset($request->u))
        {
            $affiliate = User::where('affiliate_code',$request->u)->first();
            if($affiliate)
            {
                $affiliate_of = $affiliate->id;
                $affiliate_click = new AffiliateClick();
                $affiliate_click->clicks =1;
                $affiliate_click->user_id =$affiliate->id;
                $affiliate_click->save();

                if(Auth::check())
                {
                    $affiliate_user = AffiliateUser::where('user_id', Auth::id())->first();

                    if($affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $datetime1 = new DateTime($affiliate_user->updated_at);
                        $datetime2 = new DateTime( Carbon::now());
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');
                        if($days > 30)
                        {
                            $affiliate_user->affiliate_of = $affiliate_of;
                            $affiliate_user->save();
                        }

                    }
                    else if(!$affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $affiliate_user = new AffiliateUser();
                        $affiliate_user->user_id = Auth::id();
                        $affiliate_user->affiliate_of = $affiliate_of;
                        $affiliate_user->save();
                    }
                    else
                    {

                    }
                }
                else
                {
                    Session::put('affiliate_of', $affiliate_of);
                }
            }
        }

        $featured_product_count = Product::with('author.user_profile')
            ->active()
            ->valid()
            ->demo()
            ->enable()
            ->paid()
            ->new()
            ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
            ->count();

        $most_downloaded_product_count = Product::with('author.user_profile')
            ->active()
            ->valid()
            ->demo()
            ->enable()
            ->paid()
            ->withCount('downloads')
            ->orderBy('downloads_count', 'desc')
            ->old()
            ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
            ->count();

        return view('main/home', compact('featured_product_count', 'most_downloaded_product_count'));
    }

    public function getFeatured(Request $request)
    {

      $product_items = Product::with('author.user_profile', 'product_sub_category', 'product_category')
                                  ->active()
                                  ->valid()
                                  ->demo()
                                 ->enable()
                                  ->paid()
                                  ->new()
                                  ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
                                  ->paginate(12);

        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);
    }

    public function getMostDownloaded(Request $request)
    {
        $product_items = Product::with('author.user_profile', 'product_sub_category', 'product_category')
            ->active()
            ->valid()
            ->paid()
            ->demo()
            ->enable()
            ->withCount('downloads')
            ->orderBy('downloads_count', 'desc')
            ->old()
            ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
            ->paginate(12);

        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);
    }
}
