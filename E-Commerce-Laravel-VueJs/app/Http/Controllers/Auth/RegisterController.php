<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Product\Cart;
use App\Models\Product\Product;
use App\Models\User\AffiliateUser;
use App\Models\User\UserProfile;
use App\Models\User\MagicPin;
use App\Providers\RouteServiceProvider;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Registered;
use Auth;

class RegisterController extends Controller
{
    use HelperTrait;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
       // $this->middleware(['guest', 'check_magic_pin']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:150'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user_last_id = User::max('id');
        $user_name = "ffuser".($user_last_id + 1);
        $random = Str::random(3);
        $affiliate_code = "FF".$random.($user_last_id + 1);



        $user = User::create([
            'name' => $data['name'],
            'user_name' => $user_name,
            'affiliate_code' => $affiliate_code,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => "user",
            'status' => 1,
        ]);

        $user_profile = new UserProfile();
        $user_profile->user_id = $user->id;
        $user_profile->contact_email = $user->email;
        $user_profile->save();

        $this->sentAutomaticMessage($user->id, "welcome_message", "", true);

        $affiliate_of = Session::get('affiliate_of');
        if($affiliate_of && $user->role == 'user' || $user->role == 'author')
        {
            $affiliate_user = AffiliateUser::where('user_id', $user->id)->first();

            if($affiliate_user && $affiliate_of != $user->id)
            {
                $affiliate_user->affiliate_of = $affiliate_of;
                $affiliate_user->save();
            }
            else if(!$affiliate_user && $affiliate_of !=$user->id)
            {
                $affiliate_user = new AffiliateUser();
                $affiliate_user->user_id = $user->id;
                $affiliate_user->affiliate_of = $affiliate_of;
                $affiliate_user->save();
            }
            else
            {

            }
        }

     if(!$user->isAnyAdmin())
        {
            $session_cart_items = Session::get('cart_array');
            if($session_cart_items)
            {
                foreach ($session_cart_items as $key => $val)
                {
                    $session_cart_count = Cart::where('product_id', $val["product_id"])
                        ->where('user_id', $user->id)
                        ->count();
                    if(!$session_cart_count)
                    {
                        $product = Product::findOrFail($val["product_id"]);
                        if(!$product->is_my_product)
                        {
                            $cart = new Cart();
                            $cart->user_id = $user->id;
                            $cart->product_id = $val["product_id"];
                            $cart->license_name =$val["license_name"];
                            $cart->license_value = $val["license_value"];
                            $cart->date_added = date("Y-m-d H:i:s", strtotime($val["date_added"]));
                            $cart->save();
                        }

                    }
                }
                Session::forget('direct_buy_status');
                Session::forget('cart_items');
                $cart_items = Cart::where('user_id', $user->id)
                    ->get();
                Session::put('direct_buy_status', false);
                Session::put('cart_items', $cart_items);
                Session::forget('cart_array');
            }
        }

        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect()->intended($this->redirectPath());
    }
}
