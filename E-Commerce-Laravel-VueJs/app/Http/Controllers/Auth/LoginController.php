<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Product\Cart;
use App\Models\Product\Product;
use App\Models\User\AffiliateUser;
use App\Providers\RouteServiceProvider;
use App\Traits\HelperTrait;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use Auth;
use DateTime;
class LoginController extends Controller
{
    use HelperTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  //  protected $redirectTo = RouteServiceProvider::HOME;

    // Register Controller also


    public function showLoginForm()
    {
      $intended_url = Session::get('url.intended');
      Session::forget('last_url_intended');
      if($intended_url) Session::put('last_url_intended', $intended_url);
        return view('auth.login');
    }


    protected function redirectTo()
    {

        $affiliate_of = Session::get('affiliate_of');
        if($affiliate_of && auth()->user()->role == 'user' || auth()->user()->role == 'author')
        {
            $affiliate_user = AffiliateUser::where('user_id', Auth::id())->first();

            if($affiliate_user && $affiliate_of !=Auth::id())
            {
                $datetime1 = new DateTime($affiliate_user->updated_at);
                $datetime2 = new DateTime( Carbon::now());
                $interval = $datetime1->diff($datetime2);
                $days = $interval->format('%a');
                if($days > 30)
                {
                    $affiliate_user->affiliate_of = $affiliate_of;
                    $affiliate_user->save();
                }

            }
            else if(!$affiliate_user && $affiliate_of !=Auth::id())
            {
                $affiliate_user = new AffiliateUser();
                $affiliate_user->user_id = Auth::id();
                $affiliate_user->affiliate_of = $affiliate_of;
                $affiliate_user->save();
            }
            else
            {

            }
        }


        session()->flash('message', $this->automaticToast("welcome_toast"));
        if (auth()->user()->is_user)
        {
            $this->sessionCartToDb();
            return auth()->user()->user_url.'/dashboard/downloads';
        }
        elseif(auth()->user()->is_author)
        {
            $this->sessionCartToDb();
            return auth()->user()->user_url.'/dashboard/my-products';
        }
        elseif(auth()->user()->is_admin)
        {
            return 'cg_admin/dashboard/';
        }
        else
        {

        }

    }


    public function sessionCartToDb()
    {
        if(Auth::check() && !Auth::user()->isAnyAdmin())
        {
            $session_cart_items = Session::get('cart_array');
            if($session_cart_items)
            {
                foreach ($session_cart_items as $key => $val)
                {
                    $session_cart_count = Cart::where('product_id', $val["product_id"])
                        ->where('user_id', Auth::id())
                        ->count();
                    if(!$session_cart_count)
                    {
                        $product = Product::findOrFail($val["product_id"]);
                        if(!$product->is_my_product)
                        {
                            $cart = new Cart();
                            $cart->user_id = Auth::user()->id;
                            $cart->product_id = $val["product_id"];
                            $cart->license_name =$val["license_name"];
                            $cart->license_value = $val["license_value"];
                            $cart->date_added = date("Y-m-d H:i:s", strtotime($val["date_added"]));
                            $cart->save();
                        }

                    }
                }
                Session::forget('direct_buy_status');
                Session::forget('cart_items');
                $cart_items = Cart::where('user_id', Auth::id())
                    ->get();
                Session::put('direct_buy_status', false);
                Session::put('cart_items', $cart_items);
                Session::forget('cart_array');
            }
        }
    }



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
