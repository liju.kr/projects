<?php

namespace App\Http\Controllers\Main;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Invoice;
use App\Models\Order\Transaction;
use App\Models\User\AffiliateUser;
use App\Models\User\Notification;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Auth;
use Session;
use Stripe;


class StripePaymentController extends Controller
{
    use HelperTrait;

    public function stripe()
    {

        $user = Auth::user();
        $direct_buy_status = Session::get('direct_buy_status');

        if($direct_buy_status)
        {
            $product = Session::get('product');
            if($product->is_my_product)
            {
                return  redirect($product->product_url);
            }
        }
        if($user->user_profile->update_status) {
            return view('main.stripe-payment');
        }
        else
            {
                $previous_url = str_replace('/buy-now', '',  url()->previous());
                Session::forget('previous_url');
                Session::put('previous_url', $previous_url);
                return redirect($user->user_url.'/dashboard/settings')
                    ->with('message', $this->automaticToast("complete_profile"));
            }
    }


    public function stripePost(Request $request)
    {

         $user = Auth::user();
        $direct_buy_status = Session::get('direct_buy_status');


         $affiliate = AffiliateUser::where('user_id', $user->id)->first();
        if($affiliate && $affiliate->affiliate_of) $affiliate_of = User::findOrFail($affiliate->affiliate_of); else $affiliate_of = false;


        if($direct_buy_status)
        {
            $product = Session::get('product');
            $license_value = Session::get('license_value');
            $license_name = Session::get('license_name');
            $total_price =$product->price * $license_value;
        }
        else
        {
            $cart_items = Session::get('cart_items');
            $total_price = 0;
            foreach ($cart_items as $cart_tiem)
            {
                $total_price = $total_price + ($cart_tiem->product->price * $cart_tiem->license_value);
            }
        }
        try {
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            $customer = Stripe\Customer::create(array(
                'email' => $user->email,
                'name' => $user->name,
                'address' => [
                    'line1' => $user->user_profile->address_line_1,
                    'line2' => $user->user_profile->address_line_2,
                    'postal_code' => $user->user_profile->zip_code,
                    'city' => $user->user_profile->city,
                    'state' => $user->user_profile->state,
                    'country' => $user->user_profile->country,
//                    'country' => "US",
//                    'line1' => 'Kuttur',
//                    'postal_code' => '680013',
//                    'city' => 'Thrissur',
//                    'state' => 'KL',
//                    'country' => 'IN',
                ],
                'shipping' => [
                    'name' => $user->name,
                    'address' => [
                        'line1' => $user->user_profile->address_line_1,
                        'line2' => $user->user_profile->address_line_2,
                        'postal_code' => $user->user_profile->zip_code,
                        'city' => $user->user_profile->city,
                        'state' => $user->user_profile->state,
                        'country' => $user->user_profile->country,
//                        'country' => "US",
//                        'line1' => '510 Townsend St',
//                       'postal_code' => '98140',
//                       'city' => 'San Francisco',
//                       'state' => 'CA',
//                       'country' => 'US',
                    ],
                ],
                'source' => $request->stripeToken
            ));

            $charge = Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount' => $total_price * 100,
                'currency' => 'usd',
                'description' => 'Fontfolio Payment for product',
                'source' => $customer->source,
            ));

            if($charge)
            {
                // chk in cart buy also
                if($affiliate_of)
                {
                    $first_day_of_the_current_month = Carbon::today()->startOfMonth();
                    $last_day_of_the_current_month = $first_day_of_the_current_month->copy()->endOfMonth();
                    $first = date('Y-m-d H:i:s', strtotime($first_day_of_the_current_month));
                    $second = date('Y-m-d H:i:s', strtotime($last_day_of_the_current_month));
                    $total_month_sale = Download::where('affiliate_id',$affiliate_of->id)->whereBetween('created_at',[$first, $second])->sum('total_price');

                    if($total_month_sale + $total_price >=250)
                    {
                        $af_bonus = 1;
                        $mf_bonus = -1;
                    }
                    elseif ($total_month_sale + $total_price >=500)
                    {
                        $af_bonus = 2;
                        $mf_bonus = -2;
                    }
                    elseif ($total_month_sale + $total_price >=750)
                    {
                        $af_bonus = 3;
                        $mf_bonus = -3;
                    }
                    elseif ($total_month_sale + $total_price >=1000)
                    {
                        $af_bonus = 5;
                        $mf_bonus = -5;
                    }
                    else
                    {
                        $af_bonus = 0;
                        $mf_bonus = 0;
                    }

                }
                else
                {
                    $af_bonus = 0;
                    $mf_bonus = 0;
                }

                if($direct_buy_status)
                {
                    $author = $product->author;
                    if($author->affiliate_status) $author_affiliate = true; else $author_affiliate = false;

                    if($author_affiliate) // author is affiliate
                    {
                        if($affiliate_of) // user is affiliate of someone
                        {
                            $author_fee = 50;
                            $market_fee = 30 + $mf_bonus;
                            $affiliate_fee = 20 + $af_bonus;

                        }
                        else // no affiliate relation
                        {
                            $author_fee = 50;
                            $market_fee = 50+ $mf_bonus;
                            $affiliate_fee = 0+ $af_bonus;

                        }
                    }
                    else // author not affiliate
                    {
                        if($affiliate_of) // user is affiliate of someone
                        {
                            $author_fee = 50;
                            $market_fee = 30 + $mf_bonus;
                            $affiliate_fee = 20 + $af_bonus;
                        }
                        else // no affiliate relation
                        {
                            $author_fee = 70;
                            $market_fee = 30+ $mf_bonus;
                            $affiliate_fee = 0+ $af_bonus;
                        }
                    }

                    $download = new Download();
                    $download->user_id = $user->id;
                    if($affiliate_of) $download->affiliate_id = $affiliate_of->id;
                    $download->license_name = $license_name;
                    $download->license_value = $license_value;
                    $download->total_price = $total_price;
                    $download->product_id = $product->id;
                    $download->author_id = $product->author_id;
                    $download->price = $product->price;
                    $download->save();

                    $user_last_id = Invoice::max('id');
                    $invoice_no = "FF".($user_last_id + 1);
                    $invoice = new Invoice();
                    $invoice->download_id = $download->id;
                    $invoice->title = 'Fontfolio Product';
                    $invoice->description = 'Fontfolio Digital Goods';
                    $invoice->bill_date = date("Y-m-d");
                    $invoice->amount = $total_price;
                    $invoice->type = "automatic";
                    $invoice->account_type = "credit";
                    $invoice->author_fee = $author_fee;
                    $invoice->affiliate_fee = $affiliate_fee;
                    $invoice->market_fee = $market_fee;
                    $invoice->transaction_id = $charge->id;
                    if($affiliate_of) $invoice->affiliate_id = $affiliate_of->id;
                    $invoice->invoice_no =$invoice_no;
                    $invoice->save();

                    $transaction = new Transaction();
                    $transaction->invoice_id = $invoice->id;
                    $transaction->description = "Market Fee";
                    $transaction->amount =  ($total_price/100)*$market_fee;
                    $transaction->type = "market_fee";
                    $transaction->account_type = "credit";
                    $transaction->amount_type = "cash";
                    $transaction->status = "success";
                    $transaction->save();

                    $transaction = new Transaction();
                    $transaction->invoice_id = $invoice->id;
                    $transaction->user_id = $author->id;
                    $transaction->description = "Author Fee";
                    $transaction->amount =  ($total_price/100)*$author_fee;
                    $transaction->type = "author_fee";
                    $transaction->account_type = "credit";
                    $transaction->amount_type = "cash";
                    $transaction->status = "success";
                    $transaction->save();

                    if($affiliate_of)
                    {
                        $transaction = new Transaction();
                        $transaction->invoice_id = $invoice->id;
                        $transaction->user_id = $affiliate_of->id;
                        $transaction->description = "Affiliate Fee";
                        $transaction->amount =  ($total_price/100)*$affiliate_fee;
                        $transaction->type = "affiliate_fee";
                        $transaction->account_type = "credit";
                        $transaction->amount_type = "cash";
                        $transaction->status = "success";
                        $transaction->save();

                        $notification = new Notification();
                        $notification->message = "Affiliate";
                        $notification->owner_id = $affiliate_of->id;
                        $notification->user_id = $user->id;
                        $notification->product_id = $product->id;
                        $notification->type = "affiliate";
                        $notification->save();
                    }

                    $notification = new Notification();
                    $notification->message = "Purchased Your Product";
                    $notification->owner_id = $product->author_id;
                    $notification->user_id = $user->id;
                    $notification->product_id = $product->id;
                    $notification->type = "sale";
                    $notification->save();

//                    $response = array('order_id' => '123', 'name' => 'Saurabh', 'email' => 'contact@coding4developers.com', 'city' => 'Gurgaon', 'unitPrice' => '340', 'paidUnit' => '340', 'subTotal' => '340', 'bookingId' => '1');
//                  $this->invoiceMail($response);

                }
                else
                {
                    foreach ($cart_items as $cart_item)
                    {
                        // chk in direct buy also
                        if($affiliate_of)
                        {
                            $first_day_of_the_current_month = Carbon::today()->startOfMonth();
                            $last_day_of_the_current_month = $first_day_of_the_current_month->copy()->endOfMonth();
                            $first = date('Y-m-d H:i:s', strtotime($first_day_of_the_current_month));
                            $second = date('Y-m-d H:i:s', strtotime($last_day_of_the_current_month));
                            $total_month_sale = Download::where('affiliate_id',$affiliate_of->id)->whereBetween('created_at',[$first, $second])->sum('total_price');

                            if(($total_month_sale + $cart_item->license_value * $cart_item->product->price) >=250 )
                            {
                                $af_bonus = 1;
                                $mf_bonus = -1;
                            }
                            elseif (($total_month_sale + $cart_item->license_value * $cart_item->product->price) >= 500)
                            {
                                $af_bonus = 2;
                                $mf_bonus = -2;
                            }
                            elseif (($total_month_sale + $cart_item->license_value * $cart_item->product->price) >= 750)
                            {
                                $af_bonus = 3;
                                $mf_bonus = -3;
                            }
                            elseif (($total_month_sale + $cart_item->license_value * $cart_item->product->price) >= 1000)
                            {
                                $af_bonus = 5;
                                $mf_bonus = -5;
                            }
                            else
                            {
                                $af_bonus = 0;
                                $mf_bonus = 0;
                            }

                        }
                        else
                        {
                            $af_bonus = 0;
                            $mf_bonus = 0;
                        }

                        $author = $cart_item->product->author;
                        $product = $cart_item->product;
                        if($author->affiliate_status) $author_affiliate = true; else $author_affiliate = false;


                        if($author_affiliate) // author is affiliate
                        {
                            if($affiliate_of) // user is affiliate of someone
                            {
                                $author_fee = 50;
                                $market_fee = 30 + $mf_bonus;
                                $affiliate_fee = 20 + $af_bonus;

                            }
                            else // no affiliate relation
                            {
                                $author_fee = 50;
                                $market_fee = 50+ $mf_bonus;
                                $affiliate_fee = 0+ $af_bonus;

                            }
                        }
                        else // author not affiliate
                        {
                            if($affiliate_of) // user is affiliate of someone
                            {
                                $author_fee = 50;
                                $market_fee = 30 + $mf_bonus;
                                $affiliate_fee = 20 + $af_bonus;
                            }
                            else // no affiliate relation
                            {
                                $author_fee = 70;
                                $market_fee = 30+ $mf_bonus;
                                $affiliate_fee = 0+ $af_bonus;
                            }
                        }


                        $download = new Download();
                        $download->user_id = $cart_item->user_id;
                        if($affiliate_of) $download->affiliate_id = $affiliate_of->id;
                        $download->product_id = $cart_item->product_id;
                        $download->license_name = $cart_item->license_name;
                        $download->license_value = $cart_item->license_value;
                        $download->total_price = $cart_item->license_value * $cart_item->product->price;
                        $download->author_id = $cart_item->product->author_id;
                        $download->price = $cart_item->product->price;
                        $download->save();

                        $user_last_id = Invoice::max('id');
                        $invoice_no = "FF".($user_last_id + 1);

                        $invoice = new Invoice();
                        $invoice->download_id = $download->id;
                        $invoice->title = 'Fontfolio Product';
                        $invoice->description = 'Fontfolio Digital Goods';
                        $invoice->bill_date = date("Y-m-d");
                        $invoice->amount = $download->total_price;
                        $invoice->type = "automatic";
                        $invoice->account_type = "credit";
                        $invoice->author_fee = $author_fee;
                        $invoice->affiliate_fee = $affiliate_fee;
                        $invoice->market_fee = $market_fee;
                        $invoice->transaction_id = $charge->id;
                        if($affiliate_of) $invoice->affiliate_id = $affiliate_of->id;
                        $invoice->invoice_no =$invoice_no;
                        $invoice->save();

                        $transaction = new Transaction();
                        $transaction->invoice_id = $invoice->id;
                        $transaction->description = "Market Fee";
                        if($download->total_price) $transaction->amount = ($download->total_price/100)*$market_fee;  else $transaction->amount = 0 ;
                        $transaction->type = "market_fee";
                        $transaction->account_type = "credit";
                        $transaction->amount_type = "cash";
                        $transaction->status = "success";
                        $transaction->save();

                        $transaction = new Transaction();
                        $transaction->invoice_id = $invoice->id;
                        $transaction->user_id = $author->id;
                        $transaction->description = "Author Fee";
                        if($download->total_price) $transaction->amount = ($download->total_price/100)*$author_fee;  else $transaction->amount = 0 ;
                        $transaction->type = "author_fee";
                        $transaction->account_type = "credit";
                        $transaction->amount_type = "cash";
                        $transaction->status = "success";
                        $transaction->save();

                        if($affiliate_of)
                        {
                            $transaction = new Transaction();
                            $transaction->invoice_id = $invoice->id;
                            $transaction->user_id = $affiliate_of->id;
                            $transaction->description = "Affiliate Fee";
                            if($download->total_price) $transaction->amount = ($download->total_price/100)*$affiliate_fee;  else $transaction->amount = 0 ;
                            $transaction->type = "affiliate_fee";
                            $transaction->account_type = "credit";
                            $transaction->amount_type = "cash";
                            $transaction->status = "success";
                            $transaction->save();

                            $notification = new Notification();
                            $notification->message = "Affiliate";
                            $notification->owner_id = $affiliate_of->id;
                            $notification->user_id = $user->id;
                            $notification->product_id = $cart_item->product_id;
                            $notification->type = "affiliate";
                            $notification->save();
                        }

                        $notification = new Notification();
                        $notification->message = "Purchased Your Product";
                        $notification->owner_id = $cart_item->product->author_id;
                        $notification->user_id = $cart_item->user_id;
                        $notification->product_id = $cart_item->product_id;
                        $notification->type = "sale";
                        $notification->save();

                        $cart_item->delete();

                    }

                }
                $this->sentAutomaticMessage($user->id, "payment_success", "", true);
                $success = true;
                $message = $this->automaticToast("payment_success");
                return view('main.purchase', compact('success', 'message'));
            }
            else
            {
                $this->sentAutomaticMessage($user->id, "payment_declined", "", true);
                $success = false;
                $message = $this->automaticToast("payment_declined");
                return view('main.purchase', compact('success', 'message'));
            }



        } catch (\Exception $ex) {

            $this->sentAutomaticMessage($user->id, "payment_declined", "", true);
            $success = false;
            $message =  $ex->getMessage();;
            return view('main.purchase', compact('success', 'message'));
        }


    }
}
