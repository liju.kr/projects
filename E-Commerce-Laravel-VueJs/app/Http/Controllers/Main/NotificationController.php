<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\User\Notification;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Carbon;

class NotificationController extends Controller
{
    //
    public  function getNotifications()
    {

        $user = Auth::user();
        return view('main/notifications', compact('user'));
    }

    public function getMyNotifications(Request $request)
    {
        $user = Auth::user();
        $notifications = Notification::with('product','owner','user.user_profile' )
            ->where('owner_id',$user->id )
            ->latest();

        if($request->status == 'favorite')
        {
            $notifications =  $notifications->where('type', 'favorite');
        }
        elseif($request->status == 'rating')
        {
            $notifications =  $notifications->where('type', 'rating');
        }
        elseif($request->status == 'sale')
        {
            $notifications =  $notifications->where('type', 'sale');
        }
        elseif($request->status == 'follow')
        {
            $notifications =  $notifications->where('type', 'follow');
        }

        else{}

        $notification_messages =  $notifications->paginate(6);

        foreach ($notification_messages as $notification_message)
        {
            $notification_message->status = 1;
            $notification_message->save();
        }

        $response = [
            'pagination' => [
                'total' => $notification_messages->total(),
                'per_page' => $notification_messages->perPage(),
                'current_page' => $notification_messages->currentPage(),
                'last_page' => $notification_messages->lastPage(),
                'from' => $notification_messages->firstItem(),
                'to' => $notification_messages->lastItem()
            ],
            'data' => $notification_messages
        ];

        return response()->json($response);
    }

    public function getMyFollowers(Request $request)
    {
        $user = Auth::user();

        $followers = $user->followers()->with('user_profile')->paginate(6);

          $response = [
            'pagination' => [
                'total' => $followers->total(),
                'per_page' => $followers->perPage(),
                'current_page' => $followers->currentPage(),
                'last_page' => $followers->lastPage(),
                'from' => $followers->firstItem(),
                'to' => $followers->lastItem()
            ],
            'data' => $followers
        ];

        return response()->json($response);
    }

}
