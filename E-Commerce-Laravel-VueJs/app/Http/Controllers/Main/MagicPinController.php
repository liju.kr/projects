<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\User\MagicPin;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Integer;

class MagicPinController extends Controller
{
  use HelperTrait;
    public function checkMagicPin(Request $request)
    {
        $number = $request->pin_1.$request->pin_2.$request->pin_3.$request->pin_4.$request->pin_5;

        $magic_pin = MagicPin::where('secret_pin', $number)->where('user_id', null)->where('status', 1)->first();
        if($magic_pin)
        {
            Session::put('magic_pin_status', true);
            Session::put('magic_pin_id', $magic_pin->id);
            return redirect('/welcome-cg-shop')->with('message', $this->automaticToast("magic_pin_success"));
        }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast("magic_pin_failed"));
        }

    }
}
