<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\Review;
use App\Models\User\Notification;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
use HelperTrait;
    public function rateMyItem(Request $request)
    {

        $review = Review::where('user_id', Auth::id())
                        ->where('product_id', $request->product_id)
                        ->first();
        $product= Product::findOrFail($request->product_id);

        if ($review )
        {
            $review->ratings = $request->rating;
            $review->save();
        }
        else
        {
            $review_new = new Review();
            $review_new->user_id = Auth::id();
            $review_new->product_id = $request->product_id;
            $review_new->ratings = $request->rating;
            $review_new->save();
        }


        if($review || $review_new)
        {
            $notification = new Notification();
            $notification->message = "Rated Your Product";
            $notification->owner_id = $product->author_id;
            $notification->user_id = Auth::id();
            $notification->product_id = $product->id;
            $notification->type = "rating";
            $notification->save();

            return response()->json([
                'status' => 'success',
                'message' => $this->automaticToast("rate"),
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => $this->automaticToast("try_again_later"),
            ]);
        }

    }
}
