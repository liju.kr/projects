<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Transaction;
use App\Models\Product\Product;
use App\Models\User\AffiliateRequest;
use App\Models\User\AffiliateUser;
use App\Models\User\AuthorRequest;
use App\Models\User\Notification;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;
use Image;
use Session;

class UserController extends Controller
{
    //
use HelperTrait;

    public function updateUserProfile(Request $request)
    {
//return $request->contact;

        $validator =  Validator::make($request->all(), [
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'contact_email' => 'required|email',
            'city' => 'required|string|max:50',
            'address_line_1' => 'required|string|max:50',
            'zip_code' => 'required|string|max:50',
            'state' => 'required|string|max:50',
            'country' => 'required|string|max:50',
        ]);


        if($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user =  User::findOrFail(Auth::id());
            $user->name = $request->first_name;
            $user->save();

            if($user) {

                $user_profile = $user->user_profile;
                $user_profile->last_name = $request->last_name;
                if($user_profile->contact_email == NULL)$user_profile->contact_email = $request->contact_email;
                $user_profile->city = $request->city;
                $user_profile->address_line_1 = $request->address_line_1;
                $user_profile->address_line_2 = $request->address_line_2;
                $user_profile->zip_code = $request->zip_code;
                $user_profile->state = $request->state;
                $user_profile->country = $request->country;
                $user_profile->update_status = 1;
                $user_profile->save();

                $previous_url = Session::get('previous_url');
                if($previous_url)
                {
                    Session::forget('previous_url');
                    return response()->json([
                        'status' => 'success',
                        'previous_url' => $previous_url,
                        'message' => $this->automaticToast("update_success"),
                        'user_id' => $user->id,
                        //'token' => Auth::user()->createToken('Access Token')->accessToken,

                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => 'success',
                        'previous_url' => '',
                        'message' => $this->automaticToast("update_success"),
                        'user_id' => $user->id,
                        //'token' => Auth::user()->createToken('Access Token')->accessToken,

                    ]);

                }

            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("try_again_later"),
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }
        }

        //return $request->customer_id;
    }


    public function updateAuthorProfile(Request $request)
    {
        $user =  User::findOrFail(Auth::id());

        $validator =  Validator::make($request->all(), [
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'contact_email' => 'required|email',
            'payout_email' => 'required|email',
            'payout_provider' => 'required',
            'city' => 'required|string|max:50',
            'address_line_1' => 'required|string|max:50',
            'zip_code' => 'required|string|max:50',
            'state' => 'required|string|max:50',
            'country' => 'required|string|max:50',
            'store_name' => 'required|string|max:30|unique:user_profiles,store_name,'.$user->user_profile->id,
            'store_slug' => 'required|alpha_dash|max:50|unique:user_profiles,store_slug,'.$user->user_profile->id,
//            'website_link' => 'nullable|url',
//            'facebook' => 'string|nullable',
//            'instagram' => 'string|nullable',
//            'twitter' => 'string|nullable',
//            'youtube' => 'string|nullable',
//            'whatsapp' => 'string|nullable',
//            'pinterest' => 'string|nullable',
            'description' => 'required|string|min:250',
        ]);


        if($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user->name = $request->first_name;
            $user->save();
            $user_profile = $user->user_profile;
            if($user_profile) {
                $user_profile->last_name = $request->last_name;
                $user_profile->city = $request->city;
                $user_profile->address_line_1 = $request->address_line_1;
                $user_profile->address_line_2 = $request->address_line_2;
                $user_profile->zip_code = $request->zip_code;
                $user_profile->state = $request->state;
                $user_profile->country = $request->country;
                $user_profile->store_name = $request->store_name;
                $user_profile->store_slug = $request->store_slug;
                $user_profile->website_link = $request->website_link;
                $user_profile->facebook = $request->facebook;
                $user_profile->instagram = $request->instagram;
                $user_profile->twitter = $request->twitter;
                $user_profile->youtube = $request->youtube;
                $user_profile->whatsapp = $request->whatsapp;
                $user_profile->pinterest = $request->pinterest;
                $user_profile->description = $request->description;
                $user_profile->update_status = 1;
                $user_profile->save();


                $previous_url = Session::get('previous_url');
                if($previous_url)
                {
                    Session::forget('previous_url');
                    return response()->json([
                        'status' => 'success',
                        'previous_url' => $previous_url,
                        'message' => $this->automaticToast("update_success"),
                        'user_id' => $user->id,
                        //'token' => Auth::user()->createToken('Access Token')->accessToken,

                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => 'success',
                        'previous_url' => '',
                        'message' => $this->automaticToast("update_success"),
                        'user_id' => $user->id,
                        //'token' => Auth::user()->createToken('Access Token')->accessToken,

                    ]);

                }

            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("try_again_later"),
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }
        }

        //return $request->customer_id;
    }





    public function becomeSeller()
    {
        $user = User::with('user_profile')->findOrFail(Auth::id());
        $author_request = $user->author_request;

        if($user->is_author)
        {
            $author_request_status = 1;
        }
        elseif($author_request&&$author_request->status == 0)
        {
            $author_request_status = 0;
        }
        elseif($author_request&&$author_request->status == 2)
        {
            $author_request_status = 2;
        }
        else
        {
            $author_request_status = 4;
        }

        return view('main/seller-application', compact('user', 'author_request_status'));
    }


    public function welcomeShop()
    {

        if(Auth::check())
        {
            $user = User::with('user_profile')->findOrFail(Auth::id());
            if($user->is_afuthor)  return redirect($user->user_url.'/profile');
            $afuthor_request = AuthorRequest::where('user_id', Auth::id())->first();
            if($afuthor_request)
            {
                return redirect($user->user_url.'/seller-application');
            }
            else
            {
                return view('main/welcome_shop-page');
            }

        }
        else
        {
            return view('main/welcome_shop-page');
        }


    }

    public function updateUserToAuthor(Request $request, $user_name, $user)
    {

        $user = User::with('user_profile')->findOrFail(Auth::id());

        Validator::make($request->all(), [
            'contact_email' => 'required|email',
            'payout_provider' => 'required',
            'payout_email' => 'required|email',
            'store_name' => 'required|string|max:30|unique:user_profiles,store_name,'.$user->user_profile->id,
            'store_slug' => 'required|alpha_dash|max:50|unique:user_profiles,store_slug,'.$user->user_profile->id,
//            'website' => 'nullable|url',
            'description' => 'required|string|min:250',

        ])->validate();

        $user->save();

        if($user)
        {
            $user_profile =$user->user_profile;
            $user_profile->store_name = $request->store_name;
            $user_profile->store_slug = $request->store_slug;
            $user_profile->website_link = $request->website;
            $user_profile->contact_email = $request->contact_email;
            $user_profile->payout_email = $request->payout_email;
            $user_profile->payout_type = $request->payout_provider;
            $user_profile->description = $request->description;
            $user_profile->save();
        }

        if($user->author_request)
        {

        }
        else
        {
            $author_request = new AuthorRequest();
            $author_request->user_id = Auth::id();
            $author_request->status = 0;
            $author_request->save();
        }

        if($user && $user_profile && $author_request)
        {
            return back()->with('message', $this->automaticToast("request_submitted"));
        }
        else{
            return back()->with('message', $this->automaticToast("try_again_later"));
        }

    }





    public function followUserRequest(Request $request)
    {
        $user = User::find($request->user_id);
        $auth_user = User::find(Auth::id());
        auth()->user()->toggleFollow($user);
        $response = $auth_user->isFollowing($user);

        $notification = new Notification();
        if($response)
        {
            $notification->message = "Started Following You";
            $notification->owner_id = $user->id;
            $notification->user_id = $auth_user->id;
            $notification->type = "follow";
            $notification->save();
            $message = $this->automaticToast("follow");
        }
        else
        {
            $message = $this->automaticToast("un_follow");
        }

       return response()->json([
           'success'=>$response,
           'message'=>$message,
       ]);
    }

    public function favoriteProduct(Request $request)
    {
        $user = User::find(Auth::id());
        $product = Product::find($request->product_id);
        $user->toggleFavorite($product);
        $response = $product->isFavoritedBy($user);

        $notification = new Notification();
        if($response)
        {
            $notification->message = "Favorite Your Product";
            $message = $this->automaticToast("favorite");
        }
        else
        {
            $notification->message = "UnFavorite Your Product";
            $message = $this->automaticToast("un_favorite");
        }
        $notification->owner_id = $product->author->id;
        $notification->user_id = $user->id;
        $notification->product_id = $product->id;
        $notification->type = "favorite";
        $notification->save();

        return response()->json([
            'success'=>$response,
            'message'=>$message,
            ]);
    }





    public function updateProfilePic(Request $request, $url_unique_id)
    {
        $user = User::findOrFail(Auth::id());

        Validator::make($request->all(), [
            'profile_photo' => 'mimes:jpeg,jpg,png,gif|required|max:1024',
        ])->validate();

        $user_profile = $user->user_profile;


        if($request->hasfile('profile_photo')) {

            $file = $request->file('profile_photo');
            $file_name = time().'.'.$file->extension();
            $img = Image::make($file)->fit(250, 250, function ($constraint) { });

            if(Storage::disk('public')->put('uploads/user/profile/'.$file_name, $img->stream()->__toString(), ['visibility' => 'public']))
            {
                if(Storage::disk('public')->exists('uploads/user/profile/'.$user_profile->image))
                {
                    Storage::disk('public')->delete('uploads/user/profile/'.$user_profile->image);
                }
                $user_profile->image = $file_name;
            }

        }
        $user_profile->save();

        if($user_profile)
        {
            return back()->with('success', $this->automaticToast("update_success"));
        }
        else{
            return back()->with('error', $this->automaticToast("try_again_later"));
        }


    }

    public function updatePassword(Request $request)
    {
        $validator =   Validator::make($request->all(), [
            'old_password' => 'required|string|min:8',
            'password' => 'required|string|min:8|confirmed',

        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user =  User::findOrFail($request->user_id);

          if(Hash::check($request->old_password, $user->password))
            {
                $user->password = Hash::make($request->password);
                $user->save();

                return response()->json([
                    'status' => 'success',
                    'message' => $this->automaticToast("password_update_success"),
                    'user_id' => $user->id,
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("password_wrong"),
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }


        }

    }

}
