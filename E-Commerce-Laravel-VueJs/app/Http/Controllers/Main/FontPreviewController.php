<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use Illuminate\Http\Request;

class FontPreviewController extends Controller
{
    public function changeFontText(Request $request)
    {
        $product = Product::findOrfail($request->product_id);
        $font_previews = $product->font_previews;
        $font_text = $request->font_text;
        $data = view('main.ajax.font_preview',compact('font_previews', 'font_text'))->render();
        return response()->json(['options'=>$data,'success'=>true]);
    }

}
