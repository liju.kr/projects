<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Invoice;
use App\Models\Order\Transaction;
use App\Models\Product\AffiliateClick;
use App\Models\Product\Product;
use App\Models\User\AffiliateRequest;
use App\Models\User\AffiliateUser;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Auth;
use Validator;
use function GuzzleHttp\Psr7\str;

class AffiliateController extends Controller
{
    //
use HelperTrait;

    public function becomeAffiliate()
    {
        $user = User::with('user_profile')->findOrFail(Auth::id());
        $affiliate_request = $user->affiliate_request;

        if($user->is_affiliate)
        {
            $affiliate_request_status = 1;
        }
        elseif($affiliate_request&&$affiliate_request->status == 0)
        {
            $affiliate_request_status = 0;
        }
        elseif($affiliate_request&&$affiliate_request->status == 2)
        {
            $affiliate_request_status = 2;
        }
        else
        {
            $affiliate_request_status = 4;
        }

        return view('main/affiliate-application', compact('user', 'affiliate_request_status'));
    }

    public function welcomeAffiliate()
    {

        if(Auth::check())
        {
            $user = User::with('user_profile')->findOrFail(Auth::id());
            if($user->is_affiliate)  return redirect($user->user_url.'/affiliate');

            $affiliate_request = $user->affiliate_request;
            if($affiliate_request)
            {
                return redirect('/affiliate-application');
            }
            else
            {
                return view('main/welcome_affiliate-page');
            }

        }
        else
        {
            return view('main/welcome_affiliate-page');
        }

    }

    public function updateUserToAffiliate(Request $request, $user_name, $user)
    {

        $user = User::with('user_profile')->findOrFail(Auth::id());

        Validator::make($request->all(), [
            'payout_email' => 'required|email',
            'payout_provider' => 'required',
            'description' => 'required|string|min:250',
            'social_media_links' => 'required|string',
        ])->validate();

        $affiliate = new AffiliateRequest();
        $affiliate->payout_email = $request->payout_email;
        $affiliate->payout_type = $request->payout_provider;
        $affiliate->description = $request->description;
        $affiliate->website = $request->website;
        $affiliate->social_media_links = $request->social_media_links;
        $affiliate->user_id = Auth::id();
        $affiliate->save();


        if($affiliate)
        {
            return redirect('/affiliate-application');
        }
        else{
            return back()->with('error', $this->automaticToast("try_again_later"));
        }


    }

    public function getAffiliateDashboard($user_name, $user_id)
    {

        $user = User::with('user_profile')->findOrFail(Auth::id());

        $total_sold = Download::where('affiliate_id', $user->id)->sum('total_price');
        $total_earned = Transaction::where('user_id', $user->id)->where('account_type', 'credit')->where('type', 'affiliate_fee')->sum('amount');
        $total_paid =0;
        $total_balance =$user->getWalletBalance();


        $first_day_of_the_current_month = Carbon::today()->startOfMonth();
        $first_day_of_month_1 = $first_day_of_the_current_month->copy()->subMonth();
        $last_day_of_month_1 = $first_day_of_month_1->copy()->endOfMonth();
        $first = date('Y-m-d H:i:s', strtotime($first_day_of_month_1));
        $second = date('Y-m-d H:i:s', strtotime($last_day_of_month_1));

        $today_sales = Download::where('affiliate_id', $user->id)->whereDate('created_at', Carbon::today())->count();
        $today_clicks = AffiliateClick::where('user_id', $user->id)->whereDate('created_at', Carbon::today())->count();
        $today_users = AffiliateUser::where('affiliate_of', $user->id)->whereDate('created_at', Carbon::today())->count();
        $today_earnings = Download::where('affiliate_id', $user->id)->whereDate('created_at', Carbon::today())->sum('total_price');

        $last_month_sales = Download::where('affiliate_id', $user->id)->whereBetween('created_at',[$first, $second] )->count();
        $last_month_clicks = AffiliateClick::where('user_id', $user->id)->whereBetween('created_at',[$first, $second] )->count();
        $last_month_users = AffiliateUser::where('affiliate_of', $user->id)->whereBetween('created_at',[$first, $second] )->count();
        $last_month_earnings = Download::where('affiliate_id', $user->id)->whereBetween('created_at',[$first, $second] )->sum('total_price');

        $all_time_sales = Download::where('affiliate_id', $user->id)->count();
        $all_time_clicks = AffiliateClick::where('user_id', $user->id)->count();
        $all_time_users = AffiliateUser::where('affiliate_of', $user->id)->count();
        $all_time_earnings = Download::where('affiliate_id', $user->id)->sum('total_price');

        $earnings = [
            'today' =>[
                'sales' => $today_sales,
                'clicks' => $today_clicks,
                'users' => $today_users,
                'earnings' => $today_earnings,
            ],
            'last_month' =>[
                'sales' => $last_month_sales,
                'clicks' => $last_month_clicks,
                'users' => $last_month_users,
                'earnings' => $last_month_earnings,
            ],
            'all_time' =>[
                'sales' => $all_time_sales,
                'clicks' => $all_time_clicks,
                'users' => $all_time_users,
                'earnings' => $all_time_earnings,
            ],
        ];

        $total= [
            'total_sold' => $total_sold,
            'total_earned' => $total_earned,
            'total_paid' => $total_paid,
            'total_balance' => $total_balance,
        ];
        $earnings = json_encode($earnings);
        return view('main/affiliate-user', compact('user', 'total', 'earnings'));
    }
    public function affiliateEarnings(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date|after:date_from',
        ]);


        if($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user = User::with('user_profile')->findOrFail(Auth::id());
            $first = Date('Y-m-d', strtotime($request->date_from));
            $second = Date('Y-m-d', strtotime($request->date_to));

            $custom_sales = Download::where('affiliate_id', $user->id)->whereBetween('created_at',[$first, $second] )->count();
            $custom_clicks = AffiliateClick::where('user_id', $user->id)->whereBetween('created_at',[$first, $second] )->count();
            $custom_users = AffiliateUser::where('affiliate_of', $user->id)->whereBetween('created_at',[$first, $second] )->count();
            $custom_earnings = Download::where('affiliate_id', $user->id)->whereBetween('created_at',[$first, $second] )->sum('total_price');


                $custom =[
                    'sales' => $custom_sales,
                    'clicks' => $custom_clicks,
                    'users' => $custom_users,
                    'earnings' => $custom_earnings,
                    ];

                return response()->json([
                        'status' => 'success',
                        'custom' => $custom,
                    ]);
        }

    }


    public function getAffiliateSalesAll(Request $request)
    {
        $user = User::with('user_profile')->findOrFail(Auth::id());

        $products = Product::whereHas('downloads', function ($query) use ($user) {
            $query->where('affiliate_id', $user->id);
        })->with('product_category')->paginate(6);


        foreach ($products as $key=>$product_item)
        {
            $download_count = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->count();
            $sum_of_price = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->sum('total_price');
            //  $download_ids = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->pluck('id');
            $products[$key]['affiliate_download_count'] = $download_count;
            $products[$key]['affiliate_product_total_price'] = $sum_of_price;
        }

//        if($request->sort == 'asc')
//        {
//            usort($products, function($a, $b) {
//                return $a->affiliate_product_total_price > $b->affiliate_product_total_price ? -1 : 1;
//            });
//        }
//        elseif($request->sort == 'desc')
//        {
//            usort($products, function($a, $b) {
//                return $a->affiliate_product_total_price < $b->affiliate_product_total_price ? -1 : 1;
//            });
//        }
//        else{}

        $response = [
            'pagination' => [
                'total' => $products->total(),
                'per_page' => $products->perPage(),
                'current_page' => $products->currentPage(),
                'last_page' => $products->lastPage(),
                'from' => $products->firstItem(),
                'to' => $products->lastItem()
            ],
            'data' => $products
        ];

        return response()->json($response);
    }

    public function getAffiliateSalesMost(Request $request)
    {
        $user = User::with('user_profile')->findOrFail(Auth::id());

        $products = Product::whereHas('downloads', function ($query) use ($user) {
            $query->where('affiliate_id', $user->id);
        })->with('product_category')->withCount('downloads')
            ->orderBy('downloads_count', 'desc')->paginate(6);


        foreach ($products as $key=>$product_item)
        {
            $download_count = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->count();
            $sum_of_price = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->sum('total_price');
            //  $download_ids = Download::where('product_id', $product_item->id)->where('affiliate_id', $user->id)->pluck('id');
            $products[$key]['affiliate_download_count'] = $download_count;
            $products[$key]['affiliate_product_total_price'] = $sum_of_price;
        }

//        if($request->sort == 'asc')
//        {
//            usort($products, function($a, $b) {
//                return $a->affiliate_product_total_price > $b->affiliate_product_total_price ? -1 : 1;
//            });
//        }
//        elseif($request->sort == 'desc')
//        {
//            usort($products, function($a, $b) {
//                return $a->affiliate_product_total_price < $b->affiliate_product_total_price ? -1 : 1;
//            });
//        }
//        else{}

        $response = [
            'pagination' => [
                'total' => $products->total(),
                'per_page' => $products->perPage(),
                'current_page' => $products->currentPage(),
                'last_page' => $products->lastPage(),
                'from' => $products->firstItem(),
                'to' => $products->lastItem()
            ],
            'data' => $products
        ];

        return response()->json($response);
    }
}
