<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Help\HelpCategory;
use App\Models\NewsLetter\NewsLetter;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;


class SupportController extends Controller
{
    //
    use HelperTrait;
    public function getSupportPage($category=null)
    {
        $help_categories = HelpCategory::whereHas('help_topics')->get();
        if($category)
        {
            $help_category = HelpCategory::where('slug', $category)->first();
            $category_id = $help_category->id;
        }
        else
        {
            $category_id = null;
        }
        return view('main/support', compact('help_categories', 'category_id'));
    }

    public function sendSupportMail(Request $request)
    {

        $title = $request->file('title');

        // Get the uploades file with name document
        $document = $request->file('document');

        // Required validation
        $request->validate([
            'name' => 'required|max:255',
            'email_id' => 'required|email',
            'topic' => 'required',
            'message' => 'required',
            'document' => 'max:10240'
        ]);

        $category = HelpCategory::findOrFail($request->topic);

        $data = [
            'from_name' => $request->name,
            'from_email' => $request->email_id,
            'topic' => $category->name,
            'message' => $request->message,
            'document' => $document,
        ];

        // If upload was successful
        // send the email
        $to_email = $category->support_to_mail;

      \Mail::to($to_email)->send(new \App\Mail\Support($data));
      return redirect()->back()->with('message', $this->automaticToast("sent_support_mail_success"));

    }

    public function subscribeNewsLetter(Request $request)
    {
        $this->validate($request, [
            'mail_id'=>'required|email|unique:news_letters',
        ]);
        $news_letter = new NewsLetter();
        $news_letter->mail_id = $request->mail_id;
        $news_letter->save();
        if($news_letter)
        {
            return redirect()->back()->with('message', $this->automaticToast("news_subscription_success"));
        }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast("try_again_later"));
        }
    }
}
