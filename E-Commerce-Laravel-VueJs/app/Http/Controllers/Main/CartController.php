<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Product\Cart;
use App\Models\Product\Product;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Session;

class CartController extends Controller
{
    use HelperTrait;

    public function addToCart(Request $request)
    {

        $cart_array =[];
        $license_name = env($request->license);
        $license_value = env($request->license.'_VALUE');
        if (Auth::check()) {

            $cart_check = Cart::where('user_id', Auth::id())
                                ->where('product_id', $request->product_id)->count();

            if($cart_check)
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("cart_exist"),
                ]);
            }
            else
            {
                $cart = new Cart();
                $cart->user_id = Auth::id();
                $cart->product_id = $request->product_id;
                $cart->license_name = $license_name;
                $cart->license_value = $license_value;
                $cart->date_added = date("Y-m-d H:i:s", strtotime(Carbon::now()));
                $cart->save();

                if($cart)
                {

                    return response()->json([
                        'status' => 'success',
                        'message' => $this->automaticToast("add_cart"),
                    ]);

                }
                else
                {
                    return response()->json([
                        'status' => 'error',
                        'message' => $this->automaticToast("try_again_later"),
                    ]);
                }

            }


        }
        else {
            if($request->session()->get('cart_array'))
            {
                $cart_array = $request->session()->get('cart_array');
            }
            else
            {
                $cart_array =[];
            }

            if (in_array($request->product_id, array_column($cart_array, 'product_id'))) {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("cart_exist"),
                ]);
            }
            else
            {
                array_push($cart_array, array("product_id"=>$request->product_id, "license_name"=>$license_name, "license_value"=>$license_value, "date_added"=>Carbon::now()->toDateTimeString()));
                $request->session()->put('cart_array', $cart_array);

                return response()->json([
                    'status' => 'success',
                    'message' => $this->automaticToast("add_cart"),
                ]);
            }

        }


    }


    public function getCartItems(Request $request)
    {

        $cart_items = Cart::where('user_id', Auth::id())
            ->get();
        foreach ($cart_items as $cart_item)
        {
            $download_count = Download::where('product_id',$cart_item->product_id)->where('user_id',$cart_item->user_id)->count();
            if($download_count) $cart_item->delete();
        }

        if (Auth::check()) {
            Session::forget('direct_buy_status');
            Session::forget('cart_items');
           $cart_items = Cart::where('user_id', Auth::id())
                                ->get();
            Session::put('direct_buy_status', false);
            Session::put('cart_items', $cart_items);
            return view('main/cart', compact('cart_items'));
        }
        else {

            if($request->session()->get('cart_array'))
            {
                $session_cart_items = $request->session()->get('cart_array');
            }
            else
            {
                $session_cart_items = [];
            }

           // return $session_cart_items[0]['product_id'];

            return view('main/cart', compact('session_cart_items'));
        }

    }


    public function removeFromCart(Request $request)
    {

        if (Auth::check()) {
            $cart_item = Cart::where('user_id', Auth::id())
                ->where('product_id', $request->product_id)
                ->first();
            $cart_item->delete();
            if($cart_item)
            {
            return response()->json([
                'status' => 'success',
                'message' => $this->automaticToast("remove_cart"),
            ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("try_again_later"),
                ]);
            }
        }
        else {

            $session_cart_items = $request->session()->get('cart_array');

            foreach ($session_cart_items as $key => $val)
            {
                if ($val["product_id"] == $request->product_id) {
                    unset($session_cart_items[$key]);
                }
            }

          $request->session()->put('cart_array', $session_cart_items);

                return response()->json([
                    'status' => 'success',
                    'message' => $this->automaticToast("remove_cart"),
                ]);


        }

    }


}
