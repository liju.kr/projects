<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Product\AffiliateClick;
use App\Models\Product\Product;
use App\Models\User\AffiliateUser;
use App\Models\User\Country;
use App\Models\User\UserProfile;
use App\Traits\HelperTrait;
use Illuminate\Support\Carbon;
use App\User;
use Auth;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;
use function GuzzleHttp\Psr7\str;

class DashboardController extends Controller
{
    use HelperTrait;
    public function getDashboardData($user_name, $user_id)
    {
         $user = User::with('user_profile', 'products')->findOrFail($user_id);
         $countries = Country::get();
         $profile_page_status = false;
        if(!$user->is_it_me) return abort(404);

        $profile_page = false;
        return view('main/dashboard', compact('user', 'profile_page_status', 'profile_page', 'countries'));
    }


    public function getUserProfile(Request $request, $message_follow=null)
    {

        if(isset($request->u))
        {
            $affiliate = User::where('affiliate_code',$request->u)->first();
            if($affiliate)
            {
                $affiliate_of = $affiliate->id;
                $affiliate_click = new AffiliateClick();
                $affiliate_click->clicks =1;
                $affiliate_click->user_id =$affiliate->id;
                $affiliate_click->save();

                if(Auth::check())
                {
                    $affiliate_user = AffiliateUser::where('user_id', Auth::id())->first();

                    if($affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $datetime1 = new DateTime($affiliate_user->updated_at);
                        $datetime2 = new DateTime( Carbon::now());
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');
                        if($days > 30)
                        {
                            $affiliate_user->affiliate_of = $affiliate_of;
                            $affiliate_user->save();
                        }

                    }
                    else if(!$affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $affiliate_user = new AffiliateUser();
                        $affiliate_user->user_id = Auth::id();
                        $affiliate_user->affiliate_of = $affiliate_of;
                        $affiliate_user->save();
                    }
                    else
                    {

                    }
                }
                else
                {
                    Session::put('affiliate_of', $affiliate_of);
                }
            }
        }

       if($message_follow)
       {
             $store_slug = request()->segment(1);
       }
       else
       {
            $store_slug = request()->segment(count(request()->segments()));
       }

        $user_profile = UserProfile::where('store_slug', $store_slug)->first();
        $user_id = $user_profile->user_id;
        $user = User::with('user_profile', 'products')->findOrFail($user_id);
        $profile_page_status = true;

        if($user->is_user || $user->isAnyAdmin()) return abort(404);
        if($user->is_author && !$user->status) return abort(404);
        if($user->is_it_me && !$user->user_profile->update_status)
        {
            return redirect($user->user_url.'/dashboard/settings')
                ->with('message', $this->automaticToast("complete_profile"));
        }
        $profile_page = true;
        $countries = Country::get();
        return view('main/dashboard', compact('user', 'profile_page_status', 'profile_page', 'countries'));
    }

    public function getMyItems(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        $product_items = Product::with('author.user_profile')
                              ->where('author_id',$user->id )
                             ->enable();
        if($request->status == 'active')
        {
            $product_items =  $product_items->active();
        }
        elseif($request->status == 'pending')
        {
            $product_items =  $product_items->pending();
        }
        elseif($request->status == 'rejected')
        {
            $product_items =  $product_items->rejected();
        }
        else{}

        $product_items =  $product_items->latest()->paginate(10);
        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);
    }


    public function getMyDownloads(Request $request)
   {
    $user = User::findOrFail(Auth::id());
    $downloads = Download::with('product.author')
        ->where('user_id',$user->id );



    $product_items =  $downloads->latest()->paginate(6);
    $response = [
        'pagination' => [
            'total' => $product_items->total(),
            'per_page' => $product_items->perPage(),
            'current_page' => $product_items->currentPage(),
            'last_page' => $product_items->lastPage(),
            'from' => $product_items->firstItem(),
            'to' => $product_items->lastItem()
        ],
        'data' => $product_items
    ];

    return response()->json($response);
}

    public function getMyFavorites(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        $product_items = $user->favorites()->withType(Product::class)->paginate(6);

        foreach ($product_items as $key=>$product_item)
        {
            $product = Product::with('author')->find($product_item->favoriteable_id);
            $product_items[$key]['product'] = $product;
            $product_items[$key]['date_added'] = date('j M Y', strtotime($product_item->created_at));
        }

        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);
    }



    public function getMySalesAuthor(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        $sales = Download::with('product.author', 'invoice')
            ->where('author_id',$user->id )
            ->latest();

        if($request->status == 'this_month')
        {
            $first_day_of_the_current_month = Carbon::today()->startOfMonth();
            $last_day_of_the_current_month = $first_day_of_the_current_month->copy()->endOfMonth();

            $first = date('Y-m-d H:i:s', strtotime($first_day_of_the_current_month));
            $second = date('Y-m-d H:i:s', strtotime($last_day_of_the_current_month));

            $sales =  $sales->whereBetween('created_at',[$first, $second] );
        }
        elseif($request->status == 'last_month')
        {

            $first_day_of_the_current_month = Carbon::today()->startOfMonth();

            $first_day_of_month_1 = $first_day_of_the_current_month->copy()->subMonth();
            $last_day_of_month_1 = $first_day_of_month_1->copy()->endOfMonth();

            $first = date('Y-m-d H:i:s', strtotime($first_day_of_month_1));
            $second = date('Y-m-d H:i:s', strtotime($last_day_of_month_1));

            $sales =  $sales->whereBetween('created_at',[$first, $second] );
        }
        elseif($request->status == 'last_year')
        {
            $today = Carbon::today();
            $last_year_date = $today->copy()->subYear();

            $first = date('Y-m-d H:i:s', strtotime($last_year_date));
            $second = date('Y-m-d 23:59:59', strtotime($today));


            $sales =  $sales->whereBetween('created_at',[$first, $second] );
        }
        else{}

        $total_amount = $sales->sum('total_price');
        $product_items =  $sales ->paginate(6);
        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'total_amount' => $total_amount,
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);
    }




}
