<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Invoice;
use App\Models\Order\Transaction;
use App\Models\Product\Product;
use App\Models\User\AffiliateUser;
use App\Models\User\Notification;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response as Downloads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;



class PurchaseController extends Controller
{
   use HelperTrait;


    public function buyNowFreeCart(Request $request)
    {
        $user = Auth::user();
        if($user->user_profile->update_status)
        {
          $cart_items = $user->cart_items()->with('product')->get();

          foreach ($cart_items as $cart_item)
          {
              $author = $cart_item->product->author;
              $product = $cart_item->product;
              $affiliate = AffiliateUser::where('user_id', $user->id)->first();
              if($affiliate && $affiliate->affiliate_of) $affiliate_of = User::findOrFail($affiliate->affiliate_of); else $affiliate_of = false;


              $download = new Download();
              $download->user_id = $cart_item->user_id;
              if($affiliate_of) $download->affiliate_id = $affiliate->affiliate_of;
              $download->product_id = $cart_item->product_id;
              $download->license_name = $cart_item->license_name;
              $download->license_value = $cart_item->license_value;
              $download->total_price = 0;
              $download->author_id = $cart_item->product->author_id;
              $download->price = 0;
              $download->save();

              $user_last_id = Invoice::max('id');
              $invoice_no = "FF".($user_last_id + 1);

              $invoice = new Invoice();
              $invoice->download_id = $download->id;
              $invoice->amount = $download->total_price;
              $invoice->type = "automatic";
              $invoice->title = 'Fontfolio Product';
              $invoice->description = 'Fontfolio Digital Goods';
              $invoice->bill_date = date("Y-m-d");
              $invoice->account_type = "credit";
              $invoice->author_fee = 0;
              $invoice->affiliate_fee = 0;
              $invoice->market_fee = 0;
              if($affiliate_of) $invoice->affiliate_id = $affiliate_of->id;
              $invoice->invoice_no =$invoice_no;
              $invoice->save();

              $transaction = new Transaction();
              $transaction->invoice_id = $invoice->id;
              $transaction->description = "Market Fee";
              $transaction->amount = 0 ;
              $transaction->type = "market_fee";
              $transaction->account_type = "credit";
              $transaction->amount_type = "cash";
              $transaction->status = "success";
              $transaction->save();

              $transaction = new Transaction();
              $transaction->invoice_id = $invoice->id;
              $transaction->user_id = $author->id;
              $transaction->description = "Author Fee";
              $transaction->amount = 0 ;
              $transaction->type = "author_fee";
              $transaction->account_type = "credit";
              $transaction->amount_type = "cash";
              $transaction->status = "success";
              $transaction->save();

              if($affiliate_of)
              {
                  $transaction = new Transaction();
                  $transaction->invoice_id = $invoice->id;
                  $transaction->user_id = $affiliate_of->id;
                  $transaction->description = "Affiliate Fee";
                  $transaction->amount = 0 ;
                  $transaction->type = "affiliate_fee";
                  $transaction->account_type = "credit";
                  $transaction->amount_type = "cash";
                  $transaction->status = "success";
                  $transaction->save();

                  $notification = new Notification();
                  $notification->message = "Affiliate";
                  $notification->owner_id = $affiliate_of->id;
                  $notification->user_id = $user->id;
                  $notification->product_id = $cart_item->product_id;
                  $notification->type = "affiliate";
                  $notification->save();
              }

              $notification = new Notification();
              $notification->message = "Purchased Your Product";
              $notification->owner_id = $cart_item->product->author_id;
              $notification->user_id = $cart_item->user_id;
              $notification->product_id = $cart_item->product_id;
              $notification->type = "sale";
              $notification->save();

              $cart_item->delete();

          }

            return redirect($user->user_url.'/dashboard/downloads')
                ->with('message', $this->automaticToast("download_success"));
        }
        else
        {
            return redirect($user->user_url.'/dashboard/settings')
                ->with('message', $this->automaticToast("complete_profile"));
        }

    }


    public function buyNowFree(Request $request)
    {
        $user = Auth::user();
        $direct_buy_status = Session::get('direct_buy_status');
        if($direct_buy_status)
        {
            $product = Session::get('product');
            if($product->is_my_product)
            {
                return  redirect($product->product_url);
            }
        }

        if($user->user_profile->update_status)
        {

            $product = Product::findOrFail($request->product_id);
            $author = $product->author;
            $affiliate = AffiliateUser::where('user_id', $user->id)->first();
            if($affiliate && $affiliate->affiliate_of) $affiliate_of = User::findOrFail($affiliate->affiliate_of); else $affiliate_of = false;

            $download = new Download();
            $download->user_id = $user->id;
            if($affiliate_of) $download->affiliate_id = $affiliate->affiliate_of;
            $download->license_name = $request->license_name;
            $download->license_value = $request->license_value;
            $download->total_price = 0;
            $download->product_id = $product->id;
            $download->author_id = $product->author_id;
            $download->price = 0;
            $download->save();

            $user_last_id = Invoice::max('id');
            $invoice_no = "FF".($user_last_id + 1);

            $invoice = new Invoice();
            $invoice->download_id = $download->id;
            $invoice->amount = 0;
            $invoice->title = 'Fontfolio Product';
            $invoice->description = 'Fontfolio Digital Goods';
            $invoice->bill_date = date("Y-m-d");
            $invoice->type = "automatic";
            $invoice->account_type = "credit";
            $invoice->author_fee = 0;
            $invoice->affiliate_fee = 0;
            $invoice->market_fee = 0;
            if($affiliate_of) $invoice->affiliate_id = $affiliate_of->id;
            $invoice->invoice_no =$invoice_no;
            $invoice->save();

            $transaction = new Transaction();
            $transaction->invoice_id = $invoice->id;
            $transaction->description = "Market Fee";
            $transaction->amount =  0;
            $transaction->type = "market_fee";
            $transaction->account_type = "credit";
            $transaction->amount_type = "cash";
            $transaction->status = "success";
            $transaction->save();

            $transaction = new Transaction();
            $transaction->invoice_id = $invoice->id;
            $transaction->user_id = $author->id;
            $transaction->description = "Author Fee";
            $transaction->amount =  0;
            $transaction->type = "author_fee";
            $transaction->account_type = "credit";
            $transaction->amount_type = "cash";
            $transaction->status = "success";
            $transaction->save();

            if($affiliate_of)
            {
                $transaction = new Transaction();
                $transaction->invoice_id = $invoice->id;
                $transaction->user_id = $affiliate_of->id;
                $transaction->description = "Affiliate Fee";
                $transaction->amount =  0;
                $transaction->type = "affiliate_fee";
                $transaction->account_type = "credit";
                $transaction->amount_type = "cash";
                $transaction->status = "success";
                $transaction->save();

                $notification = new Notification();
                $notification->message = "Affiliate";
                $notification->owner_id = $affiliate_of->id;
                $notification->user_id = $user->id;
                $notification->product_id = $product->id;
                $notification->type = "affiliate";
                $notification->save();
            }

            $notification = new Notification();
            $notification->message = "Purchased Your Product";
            $notification->owner_id = $product->author_id;
            $notification->user_id = $user->id;
            $notification->product_id = $product->id;
            $notification->type = "sale";
            $notification->save();


            return redirect($user->user_url.'/dashboard/downloads')
                ->with('message', $this->automaticToast("download_success"));

       }
        else
        {
            return redirect($user->user_url.'/dashboard/settings')
                ->with('message', $this->automaticToast("complete_profile"));
        }

    }


    public function downloadMyItem(Request $request)
    {
        $user = Auth::user();
        $product = Product::find($request->i);

        $download = Download::where('user_id', $user->id)
                                ->where('product_id', $product->id)
                                ->count();
        if($download)
        {
            if($product->getActiveUpdatesCount())
            {
                $product_update = $product->product_updation_files()->latest()->active()->first();
                $headers = [
                    'Content-Type'        => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="fontfolio_net_'.$this->createSlugDownload($product_update->product->name).'.'.$product_update->file_type.'"',
                ];
                return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files_updated/'.$product_update->product_file), 200, $headers);
            }
            else
            {
                $headers = [
                    'Content-Type'        => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="fontfolio_net_'.$this->createSlugDownload($product->name).'.'.$product->file_type.'"',
                ];
                return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files/'.$product->product_file), 200, $headers);

            }


          }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast('try_again_later'));
        }

    }

    public function downloadMyOwnItem(Request $request)
    {
        $user = Auth::user();
        $product = Product::find($request->i);

        if($product->author_id == $user->id)
        {
            if($product->getActiveUpdatesCount())
            {
                $product_update = $product->product_updation_files()->latest()->active()->first();
                $headers = [
                    'Content-Type'        => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="fontfolio_net_'.$this->createSlugDownload($product_update->product->name).'.'.$product_update->file_type.'"',
                ];
                return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files_updated/'.$product_update->product_file), 200, $headers);

            }
            else
            {
                $headers = [
                    'Content-Type'        => 'Content-Type: application/zip',
                    'Content-Disposition' => 'attachment; filename="fontfolio_net_'.$this->createSlugDownload($product->name).'.'.$product->file_type.'"',
                ];
                return Downloads::make(Storage::disk('public')->get('/uploads/products/product_files/'.$product->product_file), 200, $headers);

            }
        }
        else
        {
            return redirect()->back()->with('message', $this->automaticToast('try_again_later'));
        }

    }
}
