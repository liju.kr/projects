<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Help\Policy;
use Illuminate\Http\Request;

class PrivacyPolicyController extends Controller
{
    //
    public function getPolicyPage($policy_slug)
    {

        $policy = Policy::where('slug', $policy_slug)->first();
        $related_policies =  Policy::where('id', '!=', $policy->id)
                                    ->inRandomOrder()->get();
        return view('main/privacy_policy-page', compact( 'policy', 'related_policies'));
    }

    public function getAboutUsPage()
    {
        return view('main/about_us-page');
    }

    public function getLicensePage()
    {
        return view('main/license-page');
    }


}
