<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Product\AffiliateClick;
use App\Models\Product\FontPreview;
use App\Models\Product\MetaTag;
use App\Models\Product\OfferStatus;
use App\Models\Product\Preview;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductSubCategory;
use App\Models\Product\ProductUpdationFile;
use App\Models\Product\Property;
use App\Models\Product\Review;
use App\Models\User\AffiliateUser;
use App\Models\User\UserProfile;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Validator;
use Image;
use Session;
use DateTime;
class ProductController extends Controller
{
    use HelperTrait;
    public function uploadForm(Request $request)
    {
        $user = Auth::user();
        if(!Auth::user()->is_author) return abort(404);
        if(!$user->user_profile->update_status)
        {
            return redirect($user->user_url.'/dashboard/settings')
                ->with('message', $this->automaticToast("complete_profile"));

        }
        return view('main/create-product', compact('user'));
    }

    public function updateForm(Request $request, $store_slug, $product_slug)
    {
        $prod = Product::where('slug', $product_slug)->first();
        $product_id = $prod->id;

        $product = Product::with('author.user_profile', 'product_sub_category.product_category', 'product_category', 'preview_photos', 'offer_status')
            ->findOrFail($product_id);

        if(Auth::user()->is_user) return abort(404);
        if(!$product->is_my_product) return abort(404);

        $product_category = $product->product_sub_category->product_category;
        $product_sub_category = ProductSubCategory::findOrFail($product->product_sub_category_id);

        $get_properties = $product_sub_category->properties()->active()->get();
        $get_file_types = $product_sub_category->file_types()->active()->get();
        $get_application_supports = $product_sub_category->application_supports()->active()->get();

        $assigned_properties  =json_encode( $product->properties->pluck('id')->toArray());
        $assigned_file_types  = json_encode($product->file_types->pluck('id')->toArray());
        $assigned_application_supports  = json_encode($product->application_supports->pluck('id')->toArray());

        return view('main/edit-product', compact( 'product', 'get_properties', 'get_file_types', 'get_application_supports', 'assigned_properties', 'assigned_file_types', 'assigned_application_supports'));
    }

    public function getProductCategories()
    {
        $data = ProductCategory::active()->whereHas('product_sub_categories')->order()->get();
        return response()->json($data);

    }

    public function getProductSubCategories(Request $request)
    {
        $product_category_id = env('PRODUCT_CATEGORY_ID');
        if ($product_category_id !="") {
            $product_category = ProductCategory::findOrFail($product_category_id);
            $product_sub_categories = $product_category->product_sub_categories()->active()->order()->get();
            $properties = $product_category->properties()->active()->normal()->get();
            $file_types = $product_category->file_types()->active()->normal()->get();
            $application_supports = $product_category->application_supports()->active()->normal()->get();

        } else {
            $product_sub_categories = [];
            $properties = [];
            $file_types = [];
            $application_supports = [];
            $product_category=[];

        }

        return response()->json([
            'product_sub_categories' =>$product_sub_categories,
            'properties' =>$properties,
            'file_types' =>$file_types,
            'application_supports' =>$application_supports,
            'product_category' =>$product_category,

        ]);
    }

    public function getProductProperties(Request $request)
    {

        if ($request->product_sub_category_id !="") {
            $product_sub_category = ProductSubCategory::findOrFail($request->product_sub_category_id);
            $properties = $product_sub_category->properties()->active()->normal()->get();
            $file_types = $product_sub_category->file_types()->active()->normal()->get();
            $application_supports = $product_sub_category->application_supports()->active()->normal()->get();

        } else {
            $properties = [];
            $file_types = [];
            $application_supports = [];
        }

        return response()->json([
            'properties' =>$properties,
            'file_types' =>$file_types,
            'application_supports' =>$application_supports,

        ]);
    }

    public function createProduct(Request $request)
    {

        $validator =   Validator::make($request->all(), [
            'name' => 'required|string|max:250|unique:products',
//            'product_category' => 'required',
            'product_sub_category' => 'required',
            'product_price' => 'required|numeric|min:0',
            'tag_line' => 'required',
            'product_license' => 'required',
            'description' => 'required|string|min:250',
            'product_tags' => 'required',
            'cover_image' => 'required|mimes:jpeg,jpg,png,gif|max:1024',
            'preview_images' => 'required',
            'preview_images.*' => 'mimes:jpeg,jpg,png,gif|max:1024',
            'product_file' => 'required|mimes:zip|max:2048000',

        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $product_last_id = Product::max('id');
            $random = Str::random(12);
            $serial_key = "CG".$random.($product_last_id + 1);

            $cover_image = $request->file('cover_image');
            $preview_images =$request->file('preview_images');
            $product_file = $request->file('product_file');

            $product= new Product();
            $product->author_id = Auth::id();
            $product->name = $request->get('name');
            $product->slug = $this->createSlug($request->get('name'));
            $product->serial_key = $serial_key;
            $product->product_category_id = env('PRODUCT_CATEGORY_ID');
            $product->product_sub_category_id = $request->get('product_sub_category');
            $product->price = $request->get('product_price');
            $product->product_license = $request->get('product_license');
            $product->description = $request->get('description');
            $product->tag_line = $request->get('tag_line');
            if($request->get('product_tags') == null || $request->get('product_tags') == "" || $request->get('product_tags') == "null")$product->product_tags = NULL; else $product->product_tags = $request->get('product_tags');
            if($request->get('live_preview_link') == null || $request->get('live_preview_link') == "" || $request->get('live_preview_link') == "null")$product->external_url = NULL; else $product->external_url = $request->get('live_preview_link');
            $product->admin_id = null;
            $product->status = 0;
            $product->update_status =0;

            if($request->hasfile('product_file')) {

                $product_file_name = time().'.'.$product_file->extension();

//                if(Storage::disk('public')->put('uploads/products/product_files/'.$product_file_name, file_get_contents($product_file), ['visibility' => 'public']))
//                {
//                    $product->product_file = $product_file_name;
//                }
             if(Storage::disk('public')->put('uploads/products/product_files/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
             {
                 $product->product_file = $product_file_name;
             }
               // $product_file->move(storage_path().'/app/public/uploads/products/product_files/', $product_file_name);

            }



            if($request->hasfile('cover_image'))
            {
                $cover_image_name = time().'.'.$cover_image->extension();
              //  $cover_image->move(storage_path().'/app/public/uploads/products/cover_photos/', $cover_image_name);
                $img = Image::make($cover_image)->fit(2038, 1359, function ($constraint) { });
                $thumb_nail = Image::make($cover_image)->fit(400, 267, function ($constraint) { });
                if(Storage::disk('public')->put('uploads/products/cover_photos/'.$cover_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
                {
                    if(Storage::disk('public')->put('uploads/products/cover_photos/thumb/'.$cover_image_name, $thumb_nail->stream()->__toString(), ['visibility' => 'public']))
                    {
                        $product->cover_photo =$cover_image_name;
                    }

                }

            }

            $product->save();

            if($request->get('properties'))
            {

                $product->properties()->attach(json_decode($request->get('properties')));

            }
            if($request->get('application_supports'))
            {
                $product->application_supports()->attach(json_decode($request->get('application_supports')));
            }
            if($request->get('file_types'))
            {
                $product->file_types()->attach(json_decode($request->get('file_types')));
            }

            $meta_tag = new MetaTag();
            $meta_tag->product_id = $product->id;
            $meta_tag->save();

            $offer_status = new OfferStatus();
            $offer_status->product_id = $product->id;
            $offer_status->subscriptions_status = $request->get('subscriptions_status');
            $offer_status->freebies_status = $request->get('freebies_status');
            $offer_status->offers_status = $request->get('offers_status');
            $offer_status->user_id = Auth::id();
            $offer_status->save();

            if(!empty($preview_images))
            {

                foreach ($preview_images as $key=>$preview_image)
                {
                     $preview_name = time().$key.'.'.$preview_image->extension();
                   // $preview_image->move(storage_path().'/app/public/uploads/products/preview_photos/', $preview_name);
                    $img = Image::make($preview_image)->fit(2038, 1359, function ($constraint) { });
                    if(Storage::disk('public')->put('uploads/products/preview_photos/'.$preview_name, $img->stream()->__toString(), ['visibility' => 'public']))
                    {
                        $preview= new Preview();
                        $preview->product_id = $product->id;
                        $preview->preview_image = $preview_name;
                        $preview->save();
                    }

                }
            }


            if($product)
            {
                return response()->json([
                    'status' => 'success',
                    'unique_id' => Auth::user()->unique_id,
                    'product_url' => $product->product_url,
                    'message' => $this->automaticToast("product_created"),
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $this->automaticToast("try_again_later"),
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }



        }

    }

    public function getShopPage(Request $request, $category_slug=null, $sub_category_slug=null)
    {

        if(isset($request->u))
        {
            $affiliate = User::where('affiliate_code',$request->u)->first();
            if($affiliate)
            {
                $affiliate_of = $affiliate->id;
                $affiliate_click = new AffiliateClick();
                $affiliate_click->clicks =1;
                $affiliate_click->user_id =$affiliate->id;
                $affiliate_click->save();

                if(Auth::check())
                {
                    $affiliate_user = AffiliateUser::where('user_id', Auth::id())->first();

                    if($affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $datetime1 = new DateTime($affiliate_user->updated_at);
                        $datetime2 = new DateTime( Carbon::now());
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');
                        if($days > 30)
                        {
                            $affiliate_user->affiliate_of = $affiliate_of;
                            $affiliate_user->save();
                        }

                    }
                    else if(!$affiliate_user && $affiliate_of !=Auth::id())
                    {
                        $affiliate_user = new AffiliateUser();
                        $affiliate_user->user_id = Auth::id();
                        $affiliate_user->affiliate_of = $affiliate_of;
                        $affiliate_user->save();
                    }
                    else
                    {

                    }
                }
                else
                {
                    Session::put('affiliate_of', $affiliate_of);
                }
            }
        }

        if($request->key_word)
        {
            $key_word = $request->key_word;
        }
        else
        {
            $key_word = null;
        }

        if(isset($request->price))
        {
            if($request->price=='free')
            {
                $price_type = "free";
            }
            elseif($request->price=='paid')
            {
                $price_type = "paid";
            }
            else
            {
                $price_type = null;
            }
        }
        else
        {
            $price_type = null;
        }

       if($category_slug !=null)
       {
           $product_category = ProductCategory::where('slug', $category_slug)->with('properties','file_types','application_supports')->first();
           $product_categories = [];
           $product_categories = json_encode($product_categories);
           $product_sub_categories = ProductSubCategory::with('product_category')
               ->where('product_category_id',$product_category->id )
               ->active()->order()->get();
       }
       else
       {
           $product_sub_categories = ProductSubCategory::where('product_category_id', env('PRODUCT_CATEGORY_ID'))->get();
       }

        if($sub_category_slug !=null)
        {
            $product_sub_category = ProductSubCategory::where('slug', $sub_category_slug)->first();
            $sub_category_id = $product_sub_category->id;
        }
        else
        {
            $sub_category_id = 0;
        }

        //single gategory
        $product_category = ProductCategory::where('id',env('PRODUCT_CATEGORY_ID'))->with('properties','file_types','application_supports')->first();
        $product_categories = [];
        $product_categories = json_encode($product_categories);

        return view('main/shop', compact('category_slug', 'sub_category_slug', 'sub_category_id', 'product_sub_categories', 'product_categories', 'product_category', 'key_word', 'price_type'));
    }

    public function getShopProductCategories(Request $request)
    {
        $request->category_slug;
        $product_category = ProductCategory::with('properties', 'file_types', 'application_supports', 'product_sub_categories')->where('slug', $request->category_slug)->first();
        return response()->json($product_category);
    }
    public function getShopProducts(Request $request)
    {
         $products = Product::with('author.user_profile', 'product_sub_category.product_category', 'preview_photos', 'product_category');

         $request_sub_category_ids = json_decode($request->product_sub_category_ids);
        $request_property_ids = json_decode($request->properties);
        $request_file_type_ids = json_decode($request->file_types);
        $request_application_support_ids = json_decode($request->application_supports);
        $product_sub_category_id_list=[];
        $product_category_ids=[];

        if($request->category_slug !=null && $request->sub_category_slug !=null)
        {
            $product_sub_category = ProductSubCategory::where('slug', $request->sub_category_slug)->active()->first();
            $product_sub_category_id_list[0]=$product_sub_category->id;

            $product_category = ProductCategory::where('slug', $request->category_slug)->active()->first();
            $product_category_ids[0]=$product_category->id;
        }
        elseif($request->category_slug !=null && $request->sub_category_slug ==null)
        {
            $product_category = ProductCategory::where('slug', $request->category_slug)->active()->first();
            $product_sub_category_id_list = ProductSubCategory::where('product_category_id', $product_category->id)->active()
                ->pluck('id');
            $product_category_ids[0]=$product_category->id;

        }
        else
        {
//            $product_sub_category_id_list =[];
//            $product_category_ids = ProductCategory::active()
//                ->pluck('id');
            $product_category = ProductCategory::where('id',env('PRODUCT_CATEGORY_ID'))->active()->first();
            $product_sub_category_id_list = ProductSubCategory::where('product_category_id', $product_category->id)->active()
                ->pluck('id');
            $product_category_ids[0]=$product_category->id;

        }

         if(count($request_sub_category_ids))
         {
          $product_sub_category_ids =  $request_sub_category_ids;
         }
         else
         {
             $product_sub_category_ids =  $product_sub_category_id_list;
         }


        if(count($product_category_ids))
        {
            $products = $products->whereIn('product_category_id',$product_category_ids );
        }
        //search
        if($request->key_word)
        {
            $author_ids_list = UserProfile::where('store_name', 'like',  '%' .$request->key_word . '%')
                ->pluck('user_id');
            $products = $products ->where('name', 'like',  '%' .$request->key_word . '%');
            $products = $products ->orWhereIn('author_id',$author_ids_list);
        }
        if($request->price_type =="free")
        {
            $products = $products->free();
        }
        elseif ($request->price_type =="paid")
        {
            $products = $products->paid();
        }
        else
        {
            $products = $products->paid();
        }
        if(count($product_sub_category_ids))
        {
            $products = $products->whereIn('product_sub_category_id',$product_sub_category_ids );
        }

        $rating_product_ids = array();
//        $property_product_ids = array();
//        $file_type_product_ids = array();
//        $application_supports_product_ids = array();
        $product_ids=array();

        //rating
        if($request->star_rating !=0)
        {
            $product_group_ids = Review::get()->groupBy('product_id');

            foreach ($product_group_ids as $product_group)
            {
                $ratings_sum = 0;
                $avg = 0;
                foreach ($product_group as $product)
                {
                    $ratings_sum = $ratings_sum+$product->ratings;
                    $product_id = $product->product_id;
                }
                $avg = round($ratings_sum / count($product_group), 0);
                if($avg == $request->star_rating)
                {
                    array_push($rating_product_ids, $product_id);
                }
            }
            $products = $products->whereIn('id',$rating_product_ids );
        }

        //property
        if(count($request_property_ids))
        {
            $property_product_ids =  DB::table('product_properties')->whereIn('property_id',$request_property_ids)
                ->pluck('product_id');

            $products = $products->whereIn('id',$property_product_ids );
        }

        //file_type
        if(count($request_file_type_ids))
        {
            $file_type_product_ids =  DB::table('product_file_types')->whereIn('file_type_id',$request_file_type_ids)
                ->pluck('product_id');
            $products = $products->whereIn('id',$file_type_product_ids );
        }

        //file_type
        if(count($request_application_support_ids))
        {
            $application_supports_product_ids =  DB::table('product_application_supports')->whereIn('application_support_id',$request_application_support_ids)
                ->pluck('product_id');
            $products = $products->whereIn('id',$application_supports_product_ids );
        }



        if($request->sort_by == 'popular')
        {
            $products = $products->withCount('downloads')->orderBy('downloads_count', 'desc')->new();
        }
        elseif ($request->sort_by == 'price_low')
        {
            $products = $products->orderBy('price', 'asc');
        }
        elseif ($request->sort_by == 'price_high')
        {
            $products = $products->orderBy('price', 'desc');
        }
        elseif ($request->sort_by == 'new_arrival')
        {
            $products = $products->new();
        }
        else
        {

        }


        $product_items = $products->active()->valid()->demo()->enable()->paginate(18);
        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);


    }

    public function getProfileShopProducts(Request $request)
    {


        $product_items = Product::with('author.user_profile', 'product_sub_category.product_category', 'preview_photos', 'product_category')
            ->where('author_id', $request->author_id)
            ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
            ->active()
            ->valid()
            ->paid()
            ->demo()
            ->enable()
            ->new()
            ->paginate(12);
        $response = [
            'pagination' => [
                'total' => $product_items->total(),
                'per_page' => $product_items->perPage(),
                'current_page' => $product_items->currentPage(),
                'last_page' => $product_items->lastPage(),
                'from' => $product_items->firstItem(),
                'to' => $product_items->lastItem()
            ],
            'data' => $product_items
        ];

        return response()->json($response);


    }


    public function getProductDetail(Request $request, $store_slug, $product_slug)
    {

        $prod = Product::where('slug', $product_slug)->first();
        $product_id = $prod->id;

        if(isset($request->u))
        {
            $affiliate = User::where('affiliate_code',$request->u)->first();
            if($affiliate)
            {
            $affiliate_of = $affiliate->id;
            $affiliate_click = new AffiliateClick();
            $affiliate_click->clicks =1;
            $affiliate_click->user_id =$affiliate->id;
            $affiliate_click->save();

            if(Auth::check())
            {
                $affiliate_user = AffiliateUser::where('user_id', Auth::id())->first();

                if($affiliate_user && $affiliate_of !=Auth::id())
                {
                    $datetime1 = new DateTime($affiliate_user->updated_at);
                    $datetime2 = new DateTime( Carbon::now());
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');
                    if($days > 30)
                    {
                        $affiliate_user->affiliate_of = $affiliate_of;
                        $affiliate_user->save();
                    }

                }
                else if(!$affiliate_user && $affiliate_of !=Auth::id())
                {
                    $affiliate_user = new AffiliateUser();
                    $affiliate_user->user_id = Auth::id();
                    $affiliate_user->affiliate_of = $affiliate_of;
                    $affiliate_user->save();
                }
                else
                {

                }
            }
            else
            {
                Session::put('affiliate_of', $affiliate_of);
            }
            }
        }

        $product = Product::with('author.user_profile', 'product_sub_category.product_category', 'preview_photos')
            ->findOrFail($product_id);

        $product_unique_id = $product->product_unique_id;
        $unique_id = $product->author->unique_id;

        $product_category_id = $product->product_sub_category->product_category_id;
        $product_sub_category_ids = ProductSubCategory::where('product_category_id', $product_category_id)
            ->pluck('id');
        $similar_products = Product::whereIn('product_sub_category_id', $product_sub_category_ids)
            ->where('id', '!=', $product_id)
            ->where('product_category_id',env('PRODUCT_CATEGORY_ID'))
            ->active()
            ->valid()
            ->paid()
            ->demo()
            ->enable()
            ->new()
            ->with('author.user_profile', 'product_sub_category.product_category', 'preview_photos', 'product_category')
            ->limit(4)
            ->get();

        if($product->status !=1)
        {
            if(Auth::check() && Auth::user()->is_author && Auth::user()->is_it_me || Auth::check() && Auth::user()->isAnyAdmin())
            {
                return view('main/product-details', compact('unique_id','product_unique_id', 'product', 'similar_products'));
            }
            else
            {
                abort(404);
            }

        }
        else
        {
            return view('main/product-details', compact('unique_id','product_unique_id', 'product', 'similar_products'));
        }

    }


    public function buyNow(Request $request)
    {
        Session::forget('product');
        Session::forget('license_value');
        Session::forget('license_name');
        Session::forget('direct_buy_status');
        $license_name = env($request->license);
        $license_value = env($request->license.'_VALUE');
        $product = Product::with('author.user_profile', 'product_sub_category.product_category', 'product_category', 'preview_photos')
            ->findOrFail($request->product_id);

        Session::put('product', $product);
        Session::put('license_value', $license_value);
        Session::put('license_name', $license_name);
        Session::put('direct_buy_status', true);

        return view('main/direct-buy', compact('product', 'license_name', 'license_value'));
    }


    public function updateProduct(Request $request)
    {


        $product = Product::findOrFail($request->product_id);
        if($product->is_my_product)
        {
            $validator =   Validator::make($request->all(), [
                'product_price' => 'required|numeric|min:0',
                'product_license' =>'required',
                'tag_line' => 'required',
                'description' => 'required|string|min:250',
                'product_tags' => 'required',
                'cover_image' => 'nullable|mimes:jpeg,jpg,png,gif|max:1024',
                'preview_images.*' => 'mimes:jpeg,jpg,png,gif|max:1024',
                'product_file' => 'nullable|mimes:zip|max:2048000',

            ]);

            if($validator->fails())
            {
                return response()->json([
                    'status' => 'validation_error',
                    'errors' => $validator->errors(),
                ]);
            }
            else
            {
                $cover_image = $request->file('cover_image');
                $preview_images =$request->file('preview_images');
                $removed_previews = json_decode($request->get('removed_previews'));
                $product_file = $request->file('product_file');
                $product->product_license = $request->get('product_license');
                $product->price = $request->get('product_price');
                $product->description = $request->get('description');
                if($request->get('product_tags') == null || $request->get('product_tags') == "" || $request->get('product_tags') == "null")$product->product_tags = NULL; else $product->product_tags = $request->get('product_tags');
                if($request->get('live_preview_link') == null || $request->get('live_preview_link') == "" || $request->get('live_preview_link') == "null")$product->external_url = NULL; else $product->external_url = $request->get('live_preview_link');

                $product->tag_line = $request->get('tag_line');
                $product->update_status =0;

                if($request->get('properties'))
                {
                    $product->properties()->sync(json_decode($request->get('properties')));
                }
                if($request->get('application_supports'))
                {
                    $product->application_supports()->sync(json_decode($request->get('application_supports')));
                }
                if($request->get('file_types'))
                {
                    $product->file_types()->sync(json_decode($request->get('file_types')));
                }

                if($request->hasfile('product_file')) {

                    if($product->status ==1)
                    {
                        $product_update = ProductUpdationFile::where('product_id', $product->id)->latest()->first();
                        if($product_update && $product_update->status != 1)
                        {
                            $product_file_name = time().'.'.$product_file->extension();
//                            if(Storage::disk('public')->put('uploads/products/product_files_updated/'.$product_file_name, file_get_contents($product_file), ['visibility' => 'public']))
//                            {
//                                if(Storage::disk('public')->exists('uploads/products/product_files_updated/'.$product_update->product_file))
//                                {
//                                    Storage::disk('public')->delete('uploads/products/product_files_updated/'.$product_update->product_file);
//                                }
//                                $product_update->product_file = $product_file_name;
//                            }
                            if(Storage::disk('public')->put('uploads/products/product_files_updated/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
                            {
                                if(Storage::disk('public')->exists('uploads/products/product_files_updated/'.$product_update->product_file))
                                {
                                    Storage::disk('public')->delete('uploads/products/product_files_updated/'.$product_update->product_file);
                                }
                                $product_update->product_file = $product_file_name;
                            }

                            $product_update->change_logs =  $request->get('change_logs');
                            $product_update->status = 0;
                            $product_update->save();
                        }
                       else
                       {

                           $product_file_name = time().'.'.$product_file->extension();

//                           if(Storage::disk('public')->put('uploads/products/product_files_updated/'.$product_file_name, file_get_contents($product_file), ['visibility' => 'public']))
//                           {
//                               $product_update = new ProductUpdationFile();
//                               $product_update->product_file = $product_file_name;
//                               $product_update->product_id = $product->id;
//                               $product_update->change_logs =  $request->get('change_logs');
//                               $product_update->save();
//                           }
                        if(Storage::disk('public')->put('uploads/products/product_files_updated/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
                        {
                            $product_update = new ProductUpdationFile();
                            $product_update->product_file = $product_file_name;
                            $product_update->product_id = $product->id;
                            $product_update->change_logs =  $request->get('change_logs');
                            $product_update->save();
                        }

                       }


                    }
                    else
                    {
                        $product_file_name = time().'.'.$product_file->extension();

//                        if(Storage::disk('public')->put('uploads/products/product_files/'.$product_file_name, file_get_contents($product_file), ['visibility' => 'public']))
//                        {
//                            if(Storage::disk('public')->exists('uploads/products/product_files/'.$product->product_file))
//                            {
//                                Storage::disk('public')->delete('uploads/products/product_files/'.$product->product_file);
//                            }
//                            $product->product_file = $product_file_name;
//                        }
                    if(Storage::disk('public')->put('uploads/products/product_files/'.$product_file_name, fopen($product_file, 'r+'), ['visibility' => 'public']))
                    {
                        if(Storage::disk('public')->exists('uploads/products/product_files/'.$product->product_file))
                        {
                            Storage::disk('public')->delete('uploads/products/product_files/'.$product->product_file);
                        }
                        $product->product_file = $product_file_name;
                    }



                    }


                }

                if($request->hasfile('cover_image'))
                {
                    $cover_image_name = time().'.'.$cover_image->extension();
                    $img = Image::make($cover_image)->fit(2038, 1359, function ($constraint) { });
                    $thumb_nail = Image::make($cover_image)->fit(400, 267, function ($constraint) { });
                    if(Storage::disk('public')->put('uploads/products/cover_photos/'.$cover_image_name, $img->stream()->__toString(), ['visibility' => 'public']))
                    {
                        if(Storage::disk('public')->exists('uploads/products/cover_photos/'.$product->cover_photo))
                        {
                            Storage::disk('public')->delete('uploads/products/cover_photos/'.$product->cover_photo);
                        }
                        if(Storage::disk('public')->exists('uploads/products/cover_photos/thumb/'.$product->cover_photo))
                        {
                            Storage::disk('public')->delete('uploads/products/cover_photos/thumb/'.$product->cover_photo);
                        }
                        if(Storage::disk('public')->put('uploads/products/cover_photos/thumb/'.$cover_image_name, $thumb_nail->stream()->__toString(), ['visibility' => 'public']))
                        {
                            $product->cover_photo = $cover_image_name;
                        }
                    }

                }

                 if($product->status !=1) $product->status = 0;
                 $product->save();



                    $offer_status = $product->offer_status;
                    $offer_status->product_id = $product->id;
                    if($product->offer_status->subscriptions_edit)
                    {
                        $offer_status->subscriptions_status = $request->get('subscriptions_status');
                    }
                    if($product->offer_status->freebies_edit)
                    {
                        $offer_status->freebies_status = $request->get('freebies_status');
                    }
                    if($product->offer_status->offers_edit)
                    {
                        $offer_status->offers_status = $request->get('offers_status');
                    }

                    $offer_status->save();



                if(!empty($preview_images))
                {

                    foreach ($preview_images as $key=>$preview_image)
                    {
                        $preview_name = time().$key.'.'.$preview_image->extension();
                        $img = Image::make($preview_image)->fit(2038, 1359, function ($constraint) { });
                        if(Storage::disk('public')->put('uploads/products/preview_photos/'.$preview_name, $img->stream()->__toString(), ['visibility' => 'public']))
                        {
                            $preview= new Preview();
                            $preview->product_id = $product->id;
                            $preview->preview_image = $preview_name;
                            $preview->save();
                        }
                    }
                }

                if(count($removed_previews))
                {
                    foreach ($removed_previews as $removed_preview_id)
                    {
                        $removed_preview = Preview::findOrFail($removed_preview_id);
                        if(Storage::disk('public')->exists('uploads/products/preview_photos/'.$removed_preview->preview_image))
                        {
                            Storage::disk('public')->delete('uploads/products/preview_photos/'.$removed_preview->preview_image);
                        }
                        $removed_preview->forceDelete();
                    }
                }

                if($product)
                {
                    if($product->status ==1 && $request->hasfile('product_file'))
                    {
                        return response()->json([
                            'status' => 'success',
                            'message' => $this->automaticToast("product_updated"),
                            //'token' => Auth::user()->createToken('Access Token')->accessToken,
                        ]);
                    }
                    else{
                        return response()->json([
                            'status' => 'success',
                            'message' => $this->automaticToast("update_success"),
                            //'token' => Auth::user()->createToken('Access Token')->accessToken,
                        ]);
                    }

                }
                else
                {
                    return response()->json([
                        'status' => 'error',
                        'message' => $this->automaticToast("try_again_later"),
                        //'token' => Auth::user()->createToken('Access Token')->accessToken,

                    ]);
                }



            }

        }

    }

    public function fetchFonts(Request $request)
    {

        $product = Product::where('slug', $request->product_id)->first();
        $font_previews = FontPreview::with('product.author')
            ->where('product_id',$product->id )->get();

        $response = [
            'data' => $font_previews
        ];

        return response()->json($response);
    }



}
