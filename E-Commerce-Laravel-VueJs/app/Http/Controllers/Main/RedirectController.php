<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Session;

class RedirectController extends Controller
{
    use HelperTrait;
    public function productDetail(Request $request)
    {
        return redirect($request->product_url)
            ->with('message',$request->message);
    }

    public function settingsMessage(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        return redirect($user->user_url.'/dashboard/settings')
            ->with('message',$request->message);
    }

    public function settingsComplete(Request $request)
    {
        $user = $user = Auth::user();
        $previous_url = str_replace('/favorite', '',  url()->previous());
        Session::forget('previous_url');
        Session::put('previous_url', $previous_url);
        return redirect($user->user_url.'/dashboard/settings')
            ->with('message', $this->automaticToast("complete_profile"));
    }
}
