<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Help\HelpCategory;
use App\Models\Help\HelpTopic;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    //
    public function getHelpPage()
    {
        $help_categories = HelpCategory::latest()->get();
        $faqs = HelpTopic::where('faq_status',1)->latest()->get();
        return view('main/help-page', compact('help_categories', 'faqs'));
    }

    public function getHelpCategoryPage($category_slug)
    {
        $help_category = HelpCategory::where('slug', $category_slug)->first();
        $search_status = false;
        $key_word = "";
        $search_results=[];
        return view('main/help-category-page', compact('help_category', 'search_status', 'search_results', 'key_word', 'category_slug'));
    }

    public function getHelpDetailPage($category_slug, $topic_slug)
    {
        $help_category = HelpCategory::where('slug', $category_slug)->first();
        $help_topic = HelpTopic::where('slug', $topic_slug)->first();
        $related_help_topics =  HelpTopic::where('help_category_id', $help_category->id)
                                             ->where('id', '!=', $help_topic->id)
                                             ->inRandomOrder()->limit(10)->get();
        return view('main/help-detail-page', compact('help_category', 'help_topic', 'related_help_topics'));
    }

    public function getHelpSearchPage(Request $request, $category_slug=null)
    {
        $search_results = HelpTopic::latest();
        if($request->key_word)
        {
            $key_word = $request->key_word;
            $search_results = $search_results->where('title', 'like',  '%' .$key_word . '%');
        }
        else
        {
            $key_word = " ";
        }

        if($category_slug !=null) {
            $help_category = HelpCategory::where('slug', $category_slug)->first();
            $search_results = $search_results->where('help_category_id', $help_category->id);
        }
        else
        {
            $help_category=[];
        }
        $search_results = $search_results->get();
        $search_status = true;
        return view('main/help-category-page', compact('help_category', 'search_status', 'search_results', 'key_word', 'category_slug'));
    }


}
