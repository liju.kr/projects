<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Order\Download;
use App\Models\Order\Refund;
use App\Models\Product\Product;
use App\Models\User\Message;
use App\Models\User\MessageUser;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Carbon;

class MessageController extends Controller
{
    //

    public  function getMessages()
    {
        $user = Auth::user();
        return view('main/messages', compact('user'));
    }



    public function getMyMessages(Request $request)
    {
        $me = Auth::user();
        $user = User::with('user_profile')->findOrFail($request->receiver_id);

        $messages = Message::where([
                ['sender_id', '=', $user->id],
                ['receiver_id', '=', $me->id],
               ])
            ->orWhere([
                ['sender_id', '=', $me->id],
                ['receiver_id', '=', $user->id],
            ])
//            ->latest()
               ->orderBy('id', 'DESC')
            ->get();

        foreach ($messages as $message)
        {
            $message->status = 1;
            $message->save();
        }

        $response = [
//            'pagination' => [
//                'total' => $messages->total(),
//                'per_page' => $messages->perPage(),
//                'current_page' => $messages->currentPage(),
//                'last_page' => $messages->lastPage(),
//                'from' => $messages->firstItem(),
//                'to' => $messages->lastItem()
//            ],
            'data' => $messages,
            'active_sender' => $user,
            'last_message_date' =>date('d M y', strtotime($messages[0]->created_at))
        ];

        return response()->json($response);

    }

    public function getMyMessageUsers(Request $request)
    {
        $me = Auth::user();

        $message_users = MessageUser::with('stranger.user_profile', 'me.user_profile')
                                        ->where('my_id', $me->id)
                                         ->orderBy('last_message_time', 'DESC')
                                        ->paginate(10000);

        foreach ($message_users as $key=>$message_user)
        {
            $unread_count = Message::where('receiver_id',$message_user->my_id)
                                        ->where('sender_id',$message_user->stranger_id)
                                        ->unread()
                                        ->count();

            $message = Message::where([
                                ['sender_id', '=', $message_user->stranger_id],
                                ['receiver_id', '=', $me->id],
                                 ])
                                ->orWhere([
                                    ['sender_id', '=', $me->id],
                                    ['receiver_id', '=', $message_user->stranger_id],
                                ])
                                ->latest()->first();
            if($unread_count)
            {
                $message_users[$key]['unread'] = true;
            }
            else
            {
                $message_users[$key]['unread'] = false;
            }
            $message_users[$key]['last_message_date'] = date('d M y', strtotime($message->created_at));
        }

        $message_users_active =  MessageUser::where('my_id', $me->id)
                                            ->orderBy('last_message_time', 'DESC')
                                             ->first();

        $active_sender = User::with('user_profile')
                            ->where('id', $message_users_active->stranger_id)
                            ->first();



        $response = [
            'pagination' => [
                'total' => $message_users->total(),
                'per_page' => $message_users->perPage(),
                'current_page' => $message_users->currentPage(),
                'last_page' => $message_users->lastPage(),
                'from' => $message_users->firstItem(),
                'to' => $message_users->lastItem()
            ],
            'data' => $message_users,
            'active_sender' => $active_sender,
        ];

        return response()->json($response);


    }


    public  function sentMessage(Request $request)
    {

        $me = Auth::user();
        $stranger = User::findOrFail($request->receiver_id);

       $message_user_me_count = MessageUser::where('my_id', $me->id)
                                        ->where('stranger_id', $stranger->id)
                                        ->count();

        $message_user_stranger_count = MessageUser::where('my_id', $stranger->id)
            ->where('stranger_id', $me->id)
            ->count();

        if(!$message_user_me_count)
        {
            $message_user_me = new MessageUser();
            $message_user_me->my_id = $me->id;
            $message_user_me->stranger_id = $stranger->id;
            $message_user_me->status =1;
            $message_user_me->save();
        }

        if(!$message_user_stranger_count)
        {
            $message_stranger_me = new MessageUser();
            $message_stranger_me->my_id = $stranger->id;
            $message_stranger_me->stranger_id = $me->id;
            $message_stranger_me->status =1;
            $message_stranger_me->save();
        }

        $message_user_me = MessageUser::where('my_id', $me->id)
            ->where('stranger_id', $stranger->id)
            ->first();
        $message_user_me->last_message_time = date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $message_user_me->save();

        $message_user_stranger = MessageUser::where('my_id', $stranger->id)
            ->where('stranger_id', $me->id)
            ->first();
        $message_user_stranger->last_message_time = date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $message_user_stranger->save();

        if($request->subject) $subject = $request->subject; else $subject = NULL;
        if($request->product){
            $product = Product::findOrFail($request->product);
            $product_name = " - ".$product->name;
        }else { $product_name = NULL;}
        if($request->reason) $reason = " - ".$request->reason; else $reason = NULL;

        $message_subject = $subject.$product_name.$reason;

        if($request->subject !="refund")
        {
            $message= new Message();
            $message->sender_id = $me->id;
            $message->receiver_id = $stranger->id;
            $message->message = $request->message_text;
            if($request->product) $message->product_id = $product->id;
            $message->subject = $message_subject;
            $message->save();
            if($message)
            {
                return response()->json([
                    'status' => 'success',
                    'inbox_url' => $me->user_url.'/messages',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'inbox_url' => $me->user_url.'/messages',
                ]);
            }
        }
        else
        {
            $new_refund_complaint = new Refund();
            $new_refund_complaint->user_id = $me->id;
            $new_refund_complaint->product_id = $product->id;
            $new_refund_complaint->reason = $request->reason;
            $new_refund_complaint->message = $request->message_text;
            $new_refund_complaint->save();
            if($new_refund_complaint)
            {
                return response()->json([
                    'status' => 'success',
                    'inbox_url' => $stranger->user_url.'/profile',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'inbox_url' => $me->user_url.'/messages',
                ]);
            }

        }




    }


    public  function messageSubject(Request $request)
    {
        $me = Auth::user();
        $stranger_author = User::findOrFail($request->receiver_id);
        $subject = $request->subject;

        if($subject =="support")
        {
            $downloads = Download::with('product.author')
                ->where('user_id',$me->id )
                ->where('author_id',$stranger_author->id )
                ->get();
            $data = view('main.ajax.downloads',compact('downloads'))->render();
            return response()->json(['options'=>$data]);
        }
        elseif($subject == "refund")
        {
            $date = \Carbon\Carbon::today()->subDays(7);
            $refunds = Download::with('product.author')
                ->where('user_id',$me->id )
                ->where('author_id',$stranger_author->id )
                ->where('created_at', '>=', $date)
                ->get();
            $data = view('main.ajax.refunds',compact('refunds'))->render();
            return response()->json(['options'=>$data]);
        }
        else
        {
            return response()->json(['options'=>""]);
        }






    }

    }
