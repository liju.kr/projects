<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CheckMagicPin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $magic_pin_status = Session::get('magic_pin_status');
        $magic_pin_id = Session::get('magic_pin_id');
        if($magic_pin_status)
        {
            return $next($request);
        }
        return redirect('/');
    }
}
