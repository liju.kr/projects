<?php

namespace App\Http\Middleware;

use Closure;

class CheckMarketingAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isMarketingAdmin() || auth()->user()->isAdmin() || auth()->user()->isSuperAdmin()) {
            return $next($request);
        }
        return redirect('/cg_admin/dashboard');
    }
}
