<?php

namespace App\Http\Middleware;

use App\Traits\HelperTrait;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuthorSettings
{
use HelperTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role == 'admin' || auth()->user()->role == 'super_admin') {
            return $next($request);
        }
        return redirect('/cg_admin/dashboard');

    }
}
