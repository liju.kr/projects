<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['sender_id', 'receiver_id', 'status', 'message', 'subject', 'product_id'];
    protected $appends = [ 'date_added', 'is_sent', 'is_received' ];
    protected $visible = ['id', 'sender_id', 'receiver_id', 'status', 'message', 'date_added', 'sender','receiver', 'is_sent', 'is_received', 'is_sent', 'is_received', 'subject', 'product_id'];
    protected $dates = ['deleted_at'];

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }
    public function getDateAddedAttribute()
    {
        return $this->created_at->diffForHumans();
    }
    public function scopeUnread($query)
    {
        return $query->where('status',0);
    }
    public function scopeRead($query)
    {
        return $query->where('status',1);
    }
    public function getIsSentAttribute()
    {
        if($this->sender_id == Auth::id())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function getIsReceivedAttribute()
    {
        if($this->receiver_id == Auth::id())
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
