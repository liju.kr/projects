<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AffiliateRequest extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['admin_id', 'user_id', 'message', 'status' ];
    protected $visible = ['id', 'admin_id', 'user_id', 'message', 'status'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
