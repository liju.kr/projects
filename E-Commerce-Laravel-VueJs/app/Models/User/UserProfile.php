<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class UserProfile extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['last_name', 'city', 'state', 'country', 'description', 'website_link', 'contact_email', 'payout_email', 'affiliate_payout_email', 'facebook', 'instagram', 'twitter', 'youtube', 'whatsapp', 'pinterest', 'image', 'user_id', 'update_status', 'payout_type', 'affiliate_payout_type', 'store_slug' ];
    protected $appends = ['image_path'];
    protected $visible = ['id','last_name', 'city', 'state', 'country', 'description', 'website_link', 'contact_email', 'payout_email', 'affiliate_payout_email', 'facebook', 'instagram', 'twitter', 'youtube', 'whatsapp', 'pinterest', 'image', 'user_id', 'store_name', 'image_path', 'update_status', 'payout_type', 'affiliate_payout_type', 'store_slug', 'address_line_1', 'address_line_2', 'zip_code'];
    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }



    public function getImagePathAttribute()
    {
       // return url('storage/uploads/user/profile_photos/'.$this->image);
        if($this->user_id == env('CG_BOT_ID'))
        {
            return asset('images/cg-bot.png');
        }
        else
        {
            if($this->image)
            {
                return  Storage::disk('public')->url('/uploads/user/profile/'.$this->image);
            }
            else
            {
                return asset('images/avatar.png');
            }
        }

    }



}
