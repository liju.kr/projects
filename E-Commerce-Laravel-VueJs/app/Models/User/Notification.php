<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['owner_id', 'user_id', 'product_id', 'type', 'message', 'status'];
    protected $appends = [ 'date_added' ];
    protected $visible = ['id', 'owner_id', 'user_id', 'product_id', 'type', 'message', 'date_added', 'product','owner','user', 'status'];
    protected $dates = ['deleted_at'];


    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product' );
    }

    public function getDateAddedAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function scopeUnread($query)
    {
        return $query->where('status',0);
    }
    public function scopeRead($query)
    {
        return $query->where('status',1);
    }
}
