<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MagicPin extends Model
{
    use SoftDeletes;
    protected $fillable=['secret_pin', 'admin_id', 'user_id', 'assign_to', 'status'];
    protected $visible = ['id', 'secret_pin', 'admin_id', 'user_id', 'assign_to', 'status'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
