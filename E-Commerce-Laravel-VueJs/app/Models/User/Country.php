<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $visible = ['id', 'phone_code', 'country_code', 'country_name', 'status'];
}
