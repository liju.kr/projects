<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuthorRequest extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['user_id', 'admin_id', 'status', 'message', 'author_updated_on'];
    protected $visible = ['id','user_id', 'admin_id', 'status', 'message', 'author_updated_on', 'admin', 'user' ];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }
}
