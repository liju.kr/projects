<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AffiliateUser extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['affiliate_of', 'user_id', 'message', 'status' ];
    protected $visible = ['id', 'affiliate_of', 'user_id', 'status'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function affiliate_of()
    {
        return $this->belongsTo('App\User', 'affiliate_of');
    }
}
