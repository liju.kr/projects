<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageUser extends Model
{
    use SoftDeletes;

    protected $fillable=['my_id', 'stranger_id', 'status'];

    protected $visible = ['id', 'my_id', 'stranger_id', 'status', 'stranger', 'me', 'unread', 'last_message_date', 'last_message_time'];
    protected $dates = ['deleted_at'];

    public function stranger()
    {
        return $this->belongsTo('App\User', 'stranger_id');
    }

    public function me()
    {
        return $this->belongsTo('App\User', 'my_id');
    }

}
