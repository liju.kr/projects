<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class OfferStatus extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['subscriptions_status', 'freebies_status', 'offers_status', 'product_id', 'user_id', 'subscriptions_edit', 'freebies_edit', 'offers_edit'];
    protected $visible = ['id', 'subscriptions_status', 'freebies_status', 'offers_status', 'product_id', 'user_id', 'product', 'user', 'subscriptions_edit', 'freebies_edit', 'offers_edit'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function admin()
    {
        return $this->belongsTo('App\User');
    }



}
