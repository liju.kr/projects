<?php

namespace App\Models\Product;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;



class ProductCategory extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['name', 'slug', 'admin_id', 'extract_status', 'external_link_status', 'sub_title_1', 'sub_title_2', 'sort'];
    protected $appends=['product_category_url'];
    protected $visible = ['id', 'name', 'admin_id', 'slug', 'product_sub_categories', 'products', 'admin', 'properties', 'application_supports', 'file_types', 'extract_status', 'external_link_status', 'product_category_url', 'sub_title_1', 'sub_title_2', 'sort'];
    protected $dates = ['deleted_at'];


    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('sort','ASC');
    }

    public function admin()
    {
        return $this->belongsTo('App\User');
    }

    public function product_sub_categories()
    {
        return $this->hasMany('App\Models\Product\ProductSubCategory')->order();
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product\Product');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Models\Product\Property', 'product_category_properties')->withPivot('property_id')->normal();
    }

    public function application_supports()
    {
        return $this->belongsToMany('App\Models\Product\ApplicationSupport', 'product_category_application_supports')->withPivot('application_support_id')->normal();
    }

    public function file_types()
    {
        return $this->belongsToMany('App\Models\Product\FileType', 'product_category_file_types')->withPivot('file_type_id')->normal();
    }

    public function admin_properties()
    {
        return $this->belongsToMany('App\Models\Product\Property', 'product_category_properties')->withPivot('property_id');
    }

    public function admin_application_supports()
    {
        return $this->belongsToMany('App\Models\Product\ApplicationSupport', 'product_category_application_supports')->withPivot('application_support_id');
    }

    public function admin_file_types()
    {
        return $this->belongsToMany('App\Models\Product\FileType', 'product_category_file_types')->withPivot('file_type_id');
    }

    public function getProductCategoryUrlAttribute()    {
        return '/all-items/'.$this->slug;
    }

}
