<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductSubCategory extends Model
{
    use SoftDeletes;
    protected $fillable=['name', 'status', 'slug', 'admin_id', 'sort'];
    protected $appends=['product_sub_category_url'];
    protected $visible = ['id', 'status', 'name', 'slug', 'admin_id', 'product_category_id', 'product_category', 'products', 'admin', 'product_sub_category_url', 'properties', 'application_supports', 'file_types', 'sort'];
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function scopeOrder($query)
    {
        return $query->orderBy('sort','ASC');
    }

    public function admin()
    {
        return $this->belongsTo('App\User');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Models\Product\ProductCategory');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product\Product')->active();
    }
    public function properties()
    {
        return $this->belongsToMany('App\Models\Product\Property', 'product_sub_category_properties')->withPivot('property_id')->normal();
    }

    public function application_supports()
    {
        return $this->belongsToMany('App\Models\Product\ApplicationSupport', 'product_sub_category_application_supports')->withPivot('application_support_id')->normal();
    }

    public function file_types()
    {
        return $this->belongsToMany('App\Models\Product\FileType', 'product_sub_category_file_types')->withPivot('file_type_id')->normal();
    }

    public function admin_properties()
    {
        return $this->belongsToMany('App\Models\Product\Property', 'product_sub_category_properties')->withPivot('property_id');
    }

    public function admin_application_supports()
    {
        return $this->belongsToMany('App\Models\Product\ApplicationSupport', 'product_sub_category_application_supports')->withPivot('application_support_id');
    }

    public function admin_file_types()
    {
        return $this->belongsToMany('App\Models\Product\FileType', 'product_sub_category_file_types')->withPivot('file_type_id');
    }
    public function getProductSubCategoryUrlAttribute()    {
        return '/all-items/'.$this->product_category->slug.'/'.$this->slug;
    }
}
