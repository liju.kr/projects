<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AffiliateClick extends Model
{
    use SoftDeletes;
    protected $fillable=['user_id', 'clicks' ];
    protected $visible = ['id', 'user_id', 'clicks'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
