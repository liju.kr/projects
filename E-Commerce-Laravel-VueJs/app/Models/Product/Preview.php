<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Preview extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['preview_image', 'product_id'];
    protected $appends = ['preview_photo_path'];
    protected $visible = ['id', 'preview_image', 'product_id','product', 'preview_photo_path'];

    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product');
    }
    public function getPreviewPhotoPathAttribute()
    {
        return Storage::disk('public')->url('/uploads/products/preview_photos/'.$this->preview_image);
    }
}
