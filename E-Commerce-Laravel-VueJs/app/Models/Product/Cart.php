<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    //
   // use SoftDeletes;

    protected $fillable=['user_id', 'product_id' ];
    protected $appends = ['cart_added'];
    protected $visible = ['id', 'user_id', 'product_id', 'product', 'user', 'cart_added'];
   // protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product');
    }

    public function getCartAddedAttribute()
    {
        return date('j M Y', strtotime($this->date_added));
    }


}
