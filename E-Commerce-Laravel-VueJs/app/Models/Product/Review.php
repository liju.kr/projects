<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    // use SoftDeletes;

    protected $fillable=['user_id', 'product_id', 'ratings' ];
    protected $appends = ['cart_added'];
    protected $visible = ['id', 'user_id', 'product_id', 'ratings', 'product', 'user',];
    // protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product');
    }
}
