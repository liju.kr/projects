<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['name', 'status', 'admin_id'];
    protected $visible = ['id','name', 'status', 'admin_id'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User');
    }

    public function product_categories()
    {
        return $this->belongsToMany('App\Models\Product\ProductCategory', 'product_category_properties')->withPivot('product_category_id');
    }

    public function product_sub_categories()
    {
        return $this->belongsToMany('App\Models\Product\ProductSubCategory', 'product_sub_category_properties')->withPivot('product_sub_category_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product\Product', 'product_properties')->withPivot('product_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function scopeDefault($query)
    {
        return $query->where('default_status',1);
    }
    public function scopeNormal($query)
    {
        return $query->where('default_status',0);
    }
    public function scopeAll($query)
    {
        return $query->orWhere('default_status',1);
    }
}
