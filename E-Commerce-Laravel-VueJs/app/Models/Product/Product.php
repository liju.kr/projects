<?php

namespace App\Models\Product;

use App\Models\Order\Download;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Overtrue\LaravelFavorite\Traits\Favoriteable;
use File;

class Product extends Model
{
    //
    use SoftDeletes, Favoriteable;


      protected $fillable=['name', 'product_sub_category_id', 'product_category_id', 'price', 'tag_line', 'description', 'status','external_url', 'update_status', 'product_file', 'cover_photo', 'subscriptions_status', 'freebies_status', 'offers_status', 'product_tags', 'product_license', 'serial_key', 'slug', 'status_changed_on'];
      protected $appends = ['product_unique_id', 'product_file_path', 'cover_photo_path', 'cover_photo_original_path', 'date_added', 'date_updated', 'file_type', 'file_size', 'is_my_product', 'is_downloaded', 'total_average_rating', 'user_average_rating', 'product_url', 'downloads_count', 'product_affiliate_url', 'is_carted'];
      protected $visible = ['id', 'name', 'admin_id', 'author_id','product_sub_category', 'product_category','author','admin', 'preview_photos', 'product_sub_category_id', 'product_category_id', 'status', 'price', 'tag_line', 'description', 'external_url', 'update_status', 'cover_photo', 'product_unique_id', 'product_file_path', 'cover_photo_path', 'cover_photo_original_path', 'date_added', 'date_updated', 'file_type', 'file_size', 'is_my_product', 'is_downloaded', 'reviews', 'total_average_rating', 'user_average_rating', 'product_url', 'properties', 'application_supports', 'file_types', 'subscriptions_status', 'freebies_status', 'offers_status', 'offer_status', 'product_tags', 'downloads', 'downloads_count', 'product_affiliate_url', 'is_carted', 'font_previews', 'product_license', 'serial_key', 'slug', 'status_changed_on', 'affiliate_download_count', 'affiliate_product_total_price', 'affiliate_fee_total'];
      protected $dates = ['deleted_at'];


    public function preview_photos()
    {
        return $this->hasMany('App\Models\Product\Preview');
    }


    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }


    public function product_sub_category()
    {
        return $this->belongsTo('App\Models\Product\ProductSubCategory');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Models\Product\ProductCategory');
    }


    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }


    public function cart_items()
    {
        return $this->hasMany('App\Models\Product\Cart');
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\Order\Download', 'product_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Product\Review', 'product_id');
    }

    public function product_updation_files()
    {
        return $this->hasMany('App\Models\Product\ProductUpdationFile', 'product_id');
    }

    public function meta_tag()
    {
        return $this->hasOne('App\Models\Product\MetaTag');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Models\Product\Property', 'product_properties')->withPivot('property_id');
    }

    public function file_types()
    {
        return $this->belongsToMany('App\Models\Product\FileType', 'product_file_types')->withPivot('file_type_id');
    }

    public function application_supports()
    {
        return $this->belongsToMany('App\Models\Product\ApplicationSupport', 'product_application_supports')->withPivot('application_support_id');
    }
    public function font_previews()
    {
        return $this->hasMany('App\Models\Product\FontPreview', 'product_id');
    }

    public function offer_status()
    {
        return $this->hasOne('App\Models\Product\OfferStatus');
    }



    public function getProductUniqueIdAttribute()
    {
        return $this->product_sub_category->product_category->slug."-".$this->id;
    }


    public function getProductFilePathAttribute()
    {
        $product_update = $this->product_updation_files()->latest()->active()->first();
        if($product_update)
        {
          //  return Storage::disk('public')->url('/uploads/products/product_files_updated/'.$product_update->product_file);
            return "";
        }
        else
            {
           // return Storage::disk('public')->url('/uploads/products/product_files/'.$this->product_file);
                return "";
        }

    }


    public function getCoverPhotoPathAttribute()
    {
        return Storage::disk('public')->url('/uploads/products/cover_photos/thumb/'.$this->cover_photo);
    }

    public function getCoverPhotoOriginalPathAttribute()
    {
        return Storage::disk('public')->url('/uploads/products/cover_photos/'.$this->cover_photo);
    }

    public function getDateAddedAttribute()
    {
        return date('j M Y', strtotime($this->created_at));
    }


    public function getDateUpdatedAttribute()
    {
        $product_update = $this->product_updation_files()->latest()->active()->first();

        if($product_update)
        return date('j M Y', strtotime($product_update->approved_on));
        else
            return "";
    }

    public function getDownloadsCountAttribute(){
        return $this->downloads()->count();
    }

    public function getFileTypeAttribute()
    {
        $product_update = $this->product_updation_files()->latest()->active()->first();
        if($product_update)
        {
            return pathinfo(Storage::disk('public')->url('/uploads/products/product_files_updated/'.$product_update->product_file), PATHINFO_EXTENSION);
        }
        else
        {
            return pathinfo(Storage::disk('public')->url('/uploads/products/product_files/'.$this->product_file), PATHINFO_EXTENSION);
        }

    }
    public function getFileSizeAttribute()
    {
        $product_update = $this->product_updation_files()->latest()->active()->first();
        if($product_update)
        {
            $size = Storage::disk('public')->size('/uploads/products/product_files_updated/'.$product_update->product_file);
        }
        else
        {
            $size = Storage::disk('public')->size('/uploads/products/product_files/'.$this->product_file);
        }
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $bytes = max($size, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
       // Uncomment one of the following alternatives
         $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));
        return round($bytes, 2) . ' ' . $units[$pow];

    }

    public function getIsMyProductAttribute()    {
        if(Auth::check() && Auth::user()->is_author)
        {
            if($this->author_id === Auth::user()->id){ return true; }
            else { return false; }
        }
        else
        {
            return false;
        }

    }


    public function getIsDownloadedAttribute()    {
        if(Auth::check())
        {
            $downloads = Download::where('user_id',Auth::id())->where('product_id', $this->id)->get();
            if($downloads->count()){ return true; }
            else { return false; }
        }
        else
        {
            return false;
        }

    }

    public function getIsCartedAttribute()    {
        if(Auth::check())
        {
            $carts = Cart::where('user_id',Auth::id())->where('product_id', $this->id)->get();
            if($carts->count()){ return true; }
            else { return false; }
        }
        else
        {
            return false;
        }

    }

    public function getUserAverageRatingAttribute()    {
        if(Auth::check())
        {
           return round($this->reviews()->where('user_id', Auth::user()->id)->average('ratings'),2);
        }
        else
        {
            return 0;
        }
    }

    public function getTotalAverageRatingAttribute()    {
        return round($this->reviews()->average('ratings'),2);
    }

    public function getProductUrlAttribute()    {
       // return $this->author->user_url.'/products/'.$this->product_category->slug.'/'.$this->product_sub_category->slug.'/'.$this->id;
        return '/'.$this->author->user_profile->store_slug.'/item/'.$this->slug;
    }

    public function getProductAffiliateUrlAttribute()    {
        if(Auth::check() && Auth::user()->is_affiliate)
        {
            //return $this->author->user_url.'/products/'.$this->product_category->slug.'/'.$this->product_sub_category->slug.'/'.$this->id.'?u='.Auth::user()->affiliate_code;
            return '/'.$this->author->user_profile->store_slug.'/item/'.$this->slug.'?u='.Auth::user()->affiliate_code;
        }
        else
        {
            return "";
        }

    }


    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function scopePending($query)
    {
        return $query->where('status',0);
    }
    public function scopeRejected($query)
    {
        return $query->where('status',2);
    }

    public function scopeFree($query)
    {
        return $query->where('price',0);
    }
    public function scopePaid($query)
    {
        return $query->where('price','!=',0);
    }

    public function scopeValid($query)
    {
        return $query->whereHas('author', function ($query) {
                $query->where('status', 1);
            })
            ->whereHas('product_category', function ($query) {
                $query->where('status', 1);
            })
            ->whereHas('product_sub_category', function ($query) {
                $query->where('status', 1);
            });
    }

    public function scopeEnable($query)
    {
        //return $query->where('author_id', Auth::id());
        return $query->where('enable_status', 1);
    }

    public function scopeDemo($query)
    {
        //return $query->where('author_id', Auth::id());
       return $query->where('author_id', '!=', env('CG_BOT_ID'));
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('status_changed_on','ASC');
    }
    public function scopeNew($query)
    {
        return $query->orderBy('status_changed_on','DESC');
    }
    public function scopeOld($query)
    {
        return $query->orderBy('status_changed_on','ASC');
    }

    //meta

    public function getMetaTitle()
    {
       if($this->meta_tag->meta_title)
       {
           return $this->meta_tag->meta_title;
       }
       else
       {
           return $this->name;
       }
    }

    public function getMetaDescription()
    {
        if($this->meta_tag->meta_description)
        {
            return $this->meta_tag->meta_description;
        }
        else
        {
            return strip_tags($this->description);
        }
    }

    public function getMetaKeyWords()
    {
        if($this->meta_tag->meta_key_words)
        {
            return $this->meta_tag->meta_key_words;
        }
        else
        {
            return $this->product_tags;
        }
    }

    public function getMetaImage()
    {
        if($this->meta_tag->meta_image)
        {
            return $this->meta_tag->meta_image_path;
        }
        else
        {
            return $this->cover_photo_original_path;
        }
    }


    public function getPendingUpdatesCount()
    {
        return $this->product_updation_files()->pending()->count();
    }
    public function getRejectedUpdatesCount()
    {
        return $this->product_updation_files()->rejected()->count();
    }
    public function getActiveUpdatesCount()
    {
        return $this->product_updation_files()->active()->count();
    }

}
