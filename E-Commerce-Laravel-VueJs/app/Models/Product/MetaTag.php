<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class MetaTag extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['product_id', 'meta_title', 'meta_image', 'meta_description', 'meta_image_path','meta_key_words'];
     protected $appends = ['meta_image_path'];
    protected $visible=['id', 'product_id', 'meta_title', 'meta_image', 'meta_description', 'meta_image_path', 'meta_key_words'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function getMetaImagePathAttribute()
    {

        if($this->meta_image)
        {
            return  Storage::disk('public')->url('uploads/products/seo/'.$this->meta_image);
        }
        else
        {
            return "";
        }


    }

}
