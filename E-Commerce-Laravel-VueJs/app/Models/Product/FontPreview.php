<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class FontPreview extends Model
{
    use SoftDeletes;
    protected $fillable=['name', 'file', 'admin_id', 'status'];
    protected $appends = ['font_path'];
    protected $visible = ['id', 'name', 'file', 'product_id', 'admin_id', 'status', 'font_path'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }

    public function getFontPathAttribute()
    {
       // return Storage::disk('public')->url('/uploads/products/font_previews/'.$this->product_id.'/'.$this->file);
        return Storage::disk('public')->url('/');
    }

    public function getFontPathAttributeUrl()
    {
       return Storage::disk('public')->url('/uploads/products/font_previews/'.$this->product_id.'/'.$this->file);
        //return Storage::disk('public')->url('/');
    }
}
