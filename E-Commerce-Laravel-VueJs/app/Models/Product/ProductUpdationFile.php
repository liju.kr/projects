<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ProductUpdationFile extends Model
{
    //

    use SoftDeletes;

    protected $fillable=['product_id', 'change_logs', 'product_file', 'status' ];
    protected $appends = ['product_file_path','file_type'];
    protected $visible = ['id', 'product_id', 'change_logs', 'product_file', 'status', 'product_file_path', 'file_type'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product');
    }
    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function getProductFilePathAttribute()
    {
        return Storage::disk('public')->url('/uploads/products/product_files_updated/'.$this->product_file);
    }
    public function getFileTypeAttribute()
    {
        return pathinfo(Storage::disk('public')->url('/uploads/products/product_files_updated/'.$this->product_file), PATHINFO_EXTENSION);
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function scopePending($query)
    {
        return $query->where('status',0);
    }
    public function scopeRejected($query)
    {
        return $query->where('status',2);
    }

}
