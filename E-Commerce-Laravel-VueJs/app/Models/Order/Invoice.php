<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['title', 'admin_id', 'description', 'bill_date', 'bill_no', 'amount', 'account_type', 'type','download_id','author_fee','affiliate_fee','market_fee','transaction_id', 'affiliate_id', 'invoice_no'];
    protected $visible = ['id', 'title', 'admin_id', 'description', 'bill_date', 'bill_no', 'amount', 'account_type', 'type','download_id','author_fee','affiliate_fee','market_fee','transaction_id', 'affiliate_id', 'invoice_no'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\Order\Download');
    }
    public function transactions()
    {
        return $this->hasMany('App\Models\Order\Transaction');
    }
    public function affiliate_user()
    {
        return $this->belongsTo('App\User', 'affiliate_id');
    }
    public function purchase_user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
