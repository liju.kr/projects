<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $fillable=['invoice_id', 'user_id', 'description', 'amount', 'account_type', 'amount_type', 'status'];
    protected $visible = ['id', 'invoice_id', 'user_id', 'description', 'amount', 'type', 'account_type', 'amount_type', 'status'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Order\Invoice');
    }
}
