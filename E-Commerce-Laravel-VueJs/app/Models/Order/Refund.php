<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Refund extends Model
{
    use SoftDeletes;
    protected $fillable=['reason', 'product_id', 'admin_id', 'user_id', 'message', 'status'];
    protected $visible = ['id', 'reason', 'product_id', 'admin_id', 'user_id', 'message', 'status'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }
}
