<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Download extends Model
{
    use SoftDeletes;

    protected $fillable=['user_id', 'product_id', 'author_id', 'price', 'total_price', 'license_name', 'license_value', 'author_fee', 'market_fee', 'affiliate_fee'];
    protected $appends = ['date_added'];
    protected $visible = ['id','user_id', 'product_id', 'author_id', 'price', 'product', 'purchase_user', 'sale_author','date_added', 'total_price', 'license_name', 'license_value', 'author_fee', 'market_fee', 'affiliate_fee', 'invoice'];
    protected $dates = ['deleted_at'];


    public function purchase_user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function affiliate_user()
    {
        return $this->belongsTo('App\User', 'affiliate_id');
    }


    public function sale_author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product');
    }

    public function invoice()
    {
        return $this->hasOne('App\Models\Order\Invoice');
    }

    public function getDateAddedAttribute()
    {
        return date('j M Y', strtotime($this->created_at));
    }

}
