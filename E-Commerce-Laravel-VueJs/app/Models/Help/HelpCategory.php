<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class HelpCategory extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['name', 'description', 'support_to_mail', 'slug', 'admin_id', 'status', 'meta_title', 'meta_description', 'meta_image'];
    protected $visible = ['id',  'name', 'description', 'support_to_mail', 'slug', 'admin_id', 'status', 'meta_title', 'meta_description', 'meta_image', 'help_topics'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User');
    }

    public function help_topics()
    {
        return $this->hasMany('App\Models\Help\HelpTopic');
    }

    public function getHelpCategoryURL()
    {
       return "/help/".$this->slug;
    }

    public function getMetaTitle()
    {
        if($this->meta_title)
        {
            return $this->meta_title;
        }
        else
        {
            return $this->name;
        }
    }

    public function getMetaDescription()
    {
        if($this->meta_description)
        {
            return $this->meta_description;
        }
        else
        {
            return $this->description;
        }
    }

    public function getMetaImage()
    {
        if($this->meta_image)
        {
            return  Storage::disk('public')->url('uploads/help/seo/'.$this->meta_image);
        }
        else
        {
            return asset('images/cg-meta-banner.png');
        }
    }

}
