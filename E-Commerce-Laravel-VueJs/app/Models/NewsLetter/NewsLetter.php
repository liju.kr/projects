<?php

namespace App\Models\NewsLetter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsLetter extends Model
{
    use SoftDeletes;
    protected $fillable=['email', 'status'];
    protected $visible = ['id','email', 'status'];
    protected $dates = ['deleted_at'];
}
