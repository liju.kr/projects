<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AutomaticMessage extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['slug', 'admin_id', 'message', 'status'];
    protected $visible = ['id','slug', 'admin_id', 'message', 'status'];
    protected $dates = ['deleted_at'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }
}
