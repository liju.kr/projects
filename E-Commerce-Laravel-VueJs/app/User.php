<?php

namespace App;

use App\Models\Order\Transaction;
use App\Models\User\Message;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Overtrue\LaravelFollow\Followable;
use Overtrue\LaravelFavorite\Traits\Favoriter;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, Followable, Favoriter;

    /**
     * The attributes that are mass assignable.
     **
     * @var array
     */
    protected $fillable = [
        'name', 'user_name', 'email', 'password', 'role', 'status', 'affiliate_code', 'founding_member_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $appends = ['unique_id', 'is_author', 'is_admin', 'is_user', 'is_affiliate', 'is_it_me', 'profile_url', 'user_url', 'is_have_unread_messages', 'is_have_messages', 'is_have_unread_notifications', 'is_have_notifications', 'short_user_name', 'account_short_user_name'];

    protected $visible = ['unique_id','products', 'product_categories', 'product_sub_categories', 'is_author', 'is_admin', 'is_user', 'is_affiliate', 'name', 'user_name', 'email', 'id', 'role', 'user_profile', 'cart_items', 'purchases', 'sales', 'is_it_me', 'profile_url', 'user_url', 'notifications', 'is_have_unread_messages', 'is_have_messages', 'is_have_unread_notifications', 'is_have_notifications', 'message_users', 'my_message_users', 'affiliate_users', 'status', 'affiliate_code', 'author_request', 'affiliate_request', 'approved_authors', 'short_user_name', 'founding_member_status', 'account_short_user_name' ];

    public function getIsAuthorAttribute()    {
        if($this->role === 'author'){ return true; }
        else { return false; }
    }
    public function getIsUserAttribute()    {
        if($this->role === 'user'){ return true; }
        else { return false; }
    }
    public function getIsAffiliateAttribute()    {
        if($this->affiliate_status ==1){ return true; }
        else { return false; }
    }
    public function getIsAdminAttribute()    {
        if($this->role === 'admin' || $this->role === 'super_admin' || $this->role === 'accounting_admin' || $this->role === 'marketing_admin' ){ return true; }
        else { return false; }
    }


    public function getIsItMeAttribute()    {
        if(Auth::check())
        {
            if($this->id === Auth::user()->id){ return true; }
            else { return false; }
        }
        else
        {
            return false;
        }

    }

    public function scopeUsers($query)
    {
        return $query->where('role','user');
    }
    public function scopeAuthors($query)
    {
        return $query->where('role','author');
    }
    public function scopeAffiliates($query)
    {
        return $query->where('affiliate_status',1);
    }

    public function isAnyAdmin()    {
        return $this->role === 'admin' || $this->role === 'super_admin' || $this->role === 'accounting_admin' || $this->role === 'marketing_admin';
    }

    public function isAdmin()    {
        return $this->role === 'admin';
    }

    public function isSuperAdmin()    {
        return $this->role === 'super_admin';
    }

    public function isMarketingAdmin()    {
        return $this->role === 'marketing_admin';
    }

    public function isAccountingAdmin()    {
        return $this->role === 'accounting_admin';
    }

    public function product_categories()
    {
        return $this->hasMany('App\Models\Product\ProductCategory')->active();
    }

    public function product_sub_categories()
    {
        return $this->hasMany('App\Models\Product\ProductSubCategory')->active();
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product\Product' ,'author_id')->active();
    }

    public function cart_items()
    {
        return $this->hasMany('App\Models\Product\Cart');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Product\Review');
    }

    public function purchases()
    {
        return $this->hasMany('App\Models\Order\Download', 'user_id');
    }

    public function affiliate_purchases()
    {
        return $this->hasMany('App\Models\Order\Download', 'affiliate_id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Order\Invoice', 'user_id');
    }

    public function affiliate_invoices()
    {
        return $this->hasMany('App\Models\Order\Invoice', 'affiliate_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Order\Transaction');
    }

    public function sales()
    {
        return $this->hasMany('App\Models\Order\Download', 'author_id');
    }

    public function user_profile()
    {
        return $this->hasOne('App\Models\User\UserProfile');
    }

    public function author_request()
    {
        return $this->hasOne('App\Models\User\AuthorRequest');
    }
    public function affiliate_request()
    {
        return $this->hasOne('App\Models\User\AffiliateRequest');
    }
    public function approved_authors()
    {
        return $this->hasMany('App\Models\User\AuthorRequest', 'admin_id');
    }
    public function properties()
    {
        return $this->hasMany('App\Models\Product\Property');
    }

    public function file_types()
    {
        return $this->hasMany('App\Models\Product\FileType');
    }

    public function application_supports()
    {
        return $this->hasMany('App\Models\Product\ApplicationSupport');
    }
    public function offer_statuses()
    {
        return $this->hasMany('App\Models\Product\OfferStatus');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\User\Notification', 'owner_id');
    }

    public function sent_messages()
    {
        return $this->hasMany('App\Models\User\Message', 'sender_id');
    }

    public function received_messages()
    {
        return $this->hasMany('App\Models\User\Message', 'receiver_id');
    }

    public function message_users()
    {
        return $this->hasMany('App\Models\User\MessageUser', 'stranger_id');
    }

    public function my_message_users()
    {
        return $this->hasMany('App\Models\User\MessageUser', 'my_id');
    }

    public function help_categories()
    {
        return $this->hasMany('App\Models\Help\HelpCategory');
    }

    public function help_topics()
    {
        return $this->hasMany('App\Models\Help\HelpTopic');
    }

    public function policies()
    {
        return $this->hasMany('App\Models\Help\Policy');
    }
    public function affiliate_users()
    {
        return $this->hasMany('App\Models\User\AffiliateUser', 'affiliate_of');
    }

    public function getUniqueIdAttribute()
    {
        return $this->user_name;
    }

    public function getIsHaveUnreadMessagesAttribute()
    {

            if($this->received_messages()->unread()->count())
            {
                return true;
            }
            else
            {
                return false;
            }

    }

    public function getIsHaveMessagesAttribute()
    {

        if($this->received_messages()->count() || $this->sent_messages()->count())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function getIsHaveUnreadNotificationsAttribute()
    {

        if($this->notifications()->unread()->count())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function getIsHaveNotificationsAttribute()
    {

        if($this->notifications()->count())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUserUrlAttribute()    {
        return '/user/'.$this->user_name.'/'.$this->id;
    }
    public function getUserProfileUrlAttribute()    {

    }
    public function getProfileUrlAttribute()    {
        if($this->is_author && $this->user_profile->store_slug)
           // return '/user/'.$this->user_name.'/'.$this->id.'/profile';
            return '/'.$this->user_profile->store_slug;
        else
            return '';

    }

    public function getNameAttribute($value)
    {
        if($this->id == env('CG_BOT_ID'))  return "CG Bot";
         return $value;
    }

    public function getShortUserNameAttribute()
    {
     $name = ltrim($this->name);
     if(strlen($name) >10)
     {
            $name_array = explode( " ", $name);
            if(strlen($name_array[0]) > 10)
            {
                $first_name = \Illuminate\Support\Str::limit($name_array[0], 7, $end='...');
            }
            else
            {
               $first_name = $name_array[0];
            }
     }
     else
     {
         $first_name = $name;
     }
      return $first_name;
    }

    public function getAccountShortUserNameAttribute()
    {
        if($this->is_author && $this->user_profile->store_name)
        {
            $name = ltrim($this->user_profile->store_name);

            if(strlen($name) >13)
            {
                $name_array = explode( " ", $name);
                if(strlen($name_array[0]) > 13)
                {
                    $first_name = \Illuminate\Support\Str::limit($name_array[0], 10, $end='...');
                }
                else
                {
                    $first_name = $name_array[0];
                }
            }
            else
            {
                $first_name = $name;
            }

            return $first_name;
        }
        else
        {
            return $this->short_user_name;
        }
    }

    public function getBotMessagesCount()
    {
        return Message::where('sender_id', env('CG_BOT_ID'))->where('receiver_id', Auth::id())->count();
    }

    public function getBotUnreadMessagesCount()
    {
        return Message::where('sender_id', env('CG_BOT_ID'))->where('receiver_id', Auth::id())->unread()->count();
    }

    public function getBotLastMessage()
    {
       $last_message = Message::where('sender_id', env('CG_BOT_ID'))->where('receiver_id', Auth::id())->unread()->first();
       $last_message->status = 1;
        $last_message->save();
       return $last_message->message;
    }

    public function getBot()
    {
        return User::findOrFail(env('CG_BOT_ID'));
    }


    public function getWalletBalance()
    {
        $credit = Transaction::where('user_id', $this->id)->where('account_type', 'credit')->sum('amount');
        $debit = Transaction::where('user_id', $this->id)->where('account_type', 'debit')->sum('amount');
        return round($credit-$debit, 2);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
