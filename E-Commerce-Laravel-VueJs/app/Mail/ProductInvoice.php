<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductInvoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data=[])
    {
        $this->data = $data;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        if($this->data['document'])
        {
            return $this->view('emails.invoice_mail', compact('data'))
                ->subject($data['topic']." - ".$data['from_name']."(".$data['from_email'].")")
                ->attach($this->data['document'],
                    [
                        'as' => $this->data['document']->getClientOriginalName(),
                        'mime' => $this->data['document']->getClientMimeType(),
                    ]);
            unlink(data['document']);
        }
        else
        {
            return $this->view('emails.invoice_mail', compact('data'))
                ->subject($data['topic']." - ".$data['from_name']."(".$data['from_email'].")");
        }

    }
}
