<?php

/**
 * アプリケーション全体の文言設定を管理するファイル
 */
return [
    'name' => 'Fungol24', // 全体名(メールのタイトルなどに使用) 

    // 公開画面で使用する設定
    'front' => [
        'login_url' => 'http://fungol-dev.creer-web.com',
    ]
 ];