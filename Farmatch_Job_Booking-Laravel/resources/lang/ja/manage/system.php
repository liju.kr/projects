<?php

return [
	
	// システム名
    "name" => "Fungol24",
    
    // default: デフォルト設定
    // tag-title : タイトルタグ
    // page-header : 管理画面のページヘッダー
    // menu: メニュー
    // index: 一覧画面パネル
    // create: 登録画面
    // update: 編集画面パネル

    "login" => [
        "default" => "ログイン",  
    ],
    "manager" => [
        "default" => "管理者",
        "index" => "管理者一覧",
    ],
    "shop" => [
        "default" => "店舗管理",  
        "index" => "店舗一覧",
    ],
    "booth" => [
        "default" => "ブース管理",  
        "index" => "ブース一覧",
    ],
    "member_division" => [
        "default" => "会員区分管理",  
        "index" => "会員区分一覧",
    ],
    "member" => [
        "default" => "会員管理",  
        "index" => "会員一覧",
    ],
    "reservation" => [
        "default" => "予約管理",  
        "index" => "予約一覧",
    ],
    "information" => [
        "default" => "お知らせ管理",  
        "index" => "お知らせ一覧",
    ],
    "login_history" => [
        "default" => "ログイン履歴",  
    ],
	
    
];