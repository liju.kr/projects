<?php

return [
	
	// システム名
    "name" => "Fungol24 予約サイト",
    
    // default: デフォルト設定
    // tag-title : タイトルタグ
    // page-header : 管理画面のページヘッダー
    // menu: メニュー
    // index: 一覧画面パネル
    // create: 登録画面
    // update: 編集画面パネル

    "home" => [
        "default" => "",  
    ],
    "login" => [
        "default" => "ログイン",  
    ],
    "profile" => [
        "default" => "プロフィール編集",  
        "index" => "プロフィール編集",
    ],
    "reservation" => [
        "default" => "予約",  
        "index" => "予約一覧",
    ],
    "information" => [
        "default" => "お知らせ",  
        "index" => "お知らせ一覧",
    ],
    "remind_password" => [
        "default" => "パスワードの再設定",  
    ],
    "reset_password" => [
        "default" => "パスワードの再設定",  
    ],
	
    
];