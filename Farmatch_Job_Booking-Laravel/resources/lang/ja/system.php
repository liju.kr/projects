<?php

return [
	"management" => "管理",
	"action" => [
		"index" => "一覧",
		"create" => "新規登録",
		"edit" => "編集",
		"delete" => "削除",
		"copy" => "コピー",
	],
	"button" => [
		"next" => "次へ",
		"save" => "保存",
		"create" => "登録",
		"update" => "更新",
		"back" => "戻る",
		"back_to_index" => "一覧へ戻る",
		"search" => "検索",
        "show" => "表示",
        "submit" => "送信",
	],
    "form" => [
        "select" => [
            "empty" => "選択してください",
            "empty-search" => "未指定",
        ],
        "radio" => [
            "empty" => "未選択",
            "empty-search" => "未指定",
        ],
    ],
    "list" => [
        "no_data" => "-",
    ],
    // 単位
    "unit" => [
        "price" => '円',
    ],
		
];