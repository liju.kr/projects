<?php

return array(

	'errors_any'	=> 'エラー内容を確認してください。',
	'success'		=> '正常に処理が終了しました。',
	'save_error'	=> 'エラーが発生した為、処理を中断しました。',
	'empty_data'	=> '保存されているデータがありませんでした。',
	'api_error'	=> 'API呼び出しでエラーが発生したため、プロセスが中断されました。',

	// パスワード変更
	'password_incorrect'	=> '現在のパスワードが正しくありません。',
	// Mutiple Upload
	'multiple_upload_empty_data' => 'アップロードデータが取得できませんでした。',
);
