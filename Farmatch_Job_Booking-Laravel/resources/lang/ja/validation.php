<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'filled'               => 'The :attribute field is required.',
	
    'json'                 => 'The :attribute must be a valid JSON string.',
	
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
	
    'string'               => 'The :attribute must be a string.',
	

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

	
	/*
	|--------------------------------------------------------------------------
	| バリデーション言語行
	|--------------------------------------------------------------------------
	|
	| 以下の言語行はバリデタークラスにより使用されるデフォルトのエラー
	| メッセージです。サイズルールのようにいくつかのバリデーションを
	| 持っているものもあります。メッセージはご自由に調整してください。
	|
	*/

	"accepted"             => "承認してください。",
	"active_url"           => "有効なURLではありません。",
	"after"                => "日付の範囲に誤りがあります。",
	"alpha"                => "アルファベッドのみがご利用できます。",
	"alpha_dash"           => "英数字とダッシュ(-)及び下線(_)がご利用できます。",
	"alpha_num"            => "英数字がご利用できます。",
	"array"                => "配列でなくてはなりません。",
	"before"               => "日付の範囲に誤りがあります。",
	"between"              => array(
		"numeric" => ":minから:maxの間で指定してください。",
		"file"    => ":min kBから、:max kBの間で指定してください。",
		"string"  => ":min文字から、:max文字の間で指定してください。",
		"array"   => ":min個から:max個の間で指定してください。",
	),
	"boolean"              => "trueかfalseを指定してください。",
//	"confirmed"            => "確認フィールドと、一致していません。",
	"confirmed"            => "確認用項目と値が一致していません。",
	"date"                 => "有効な日付を指定してください。",
	"date_format"          => ":format形式で指定してください。",
	"different"            => ":otherには、異なった内容を指定してください。",
	"digits"               => ":digits桁で指定してください。",
	"digits_between"       => ":min桁から:max桁の間で指定してください。",
	"email"                => "有効なメールアドレスを指定してください。",
	"exists"               => "選択されたデータは正しくありません。",
    'filled'               => ':attribute は必ず指定してください',
	"image"                => "画像ファイルを指定してください。",
	"in"                   => "選択された:attributeは正しくありません。",
//	"integer"              => "整数でご指定ください。",
	"integer"              => "半角数字を入力してください。",
	"ip"                   => "有効なIPアドレスをご指定ください。",
    'json'                 => 'The :attributeはJson形式の文字列を入力してください。',
	"max"                  => array(
//		"numeric" => ":max以下の数字をご指定ください。",
		"numeric" => ":max以下の半角数字をご指定ください。",
		"file"    => ":max kB以下のファイルをご指定ください。",
		"string"  => ":max文字以下でご指定ください。",
		"array"   => ":max個以下ご指定ください。",
	),
	"mimes"                => ":valuesタイプのファイルを指定してください。",
	"min"                  => array(
//		"numeric" => ":min以上の数字をご指定ください。",
		"numeric" => ":min以上の半角数字をご指定ください。",
		"file"    => ":min kB以上のファイルをご指定ください。",
		"string"  => ":min文字以上でご指定ください。",
		"array"   => ":min個以上ご指定ください。",
	),
	"not_in"               => "選択された:attributeは正しくありません。",
//	"numeric"              => "数字を指定してください。",
	"numeric"              => "半角数字を入力してください。",
	"regex"                => "正しい形式をご指定ください。",
	"required"             => "必ず指定してください。",
	"required_if"          => "必ず指定してください。",
    'required_unless'      => '必ず指定してください。',
	"required_with"        => "必ず指定してください。",
	"required_with_all"    => "必ず指定してください。",
	"required_without"     => "必ず指定してください。",
	"required_without_all" => "必ず指定してください。",
//	"same"                 => ":attributeと:otherには同じ値を指定してください。",
	"same"                 => "一致しませんでした。もう一度入力してください。",
	"size"                 => array(
		"numeric" => ":sizeを指定してください。",
		"file"    => "ファイルは、:sizeキロバイトでなくてはなりません。",
		"string"  => ":size文字で指定してください。",
		"array"   => ":size個ご指定ください。",
	),
    'string'               => '文字列を指定してください。',
	"timezone"             => "有効なタイムゾーンを指定してください。",
	"unique"               => "すでに存在しているため、登録することができません。",
	"url"                  => "正しい形式をご指定ください。",
    
    "alpha_ex"             => "半角アルファベットを入力してください。",
    "alpha_dash_ex"        => "半角アルファベット、ダッシュ(-)、下線(_)のいずれかを入力してください。",
    "alpha_num_ex"         => "半角英数字を入力してください。",
    "alpha_dash_num_ex"    => "半角英数字、ダッシュ(-)、下線(_)のいずれかを入力してください。",
    "single_byte"          => "半角文字を入力してください。",
    "kana"     			   => "カタカナを入力してください。",

	/*
	|--------------------------------------------------------------------------
	| カスタムバリデーション言語行
	|--------------------------------------------------------------------------
	|
	| "属性.ルール"の規約でキーを指定することでカスタムバリデーション
	| メッセージを定義できます。指定した属性ルールに対する特定の
	| カスタム言語行を手早く指定できます。
	|
	*/

	'custom' => array(
		'os' => array(
			'in' => '配信中の媒体が存在する為、OSは変更できません',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| カスタムバリデーション属性名
	|--------------------------------------------------------------------------
	|
	| 以下の言語行は、例えば"email"の代わりに「メールアドレス」のように、
	| 読み手にフレンドリーな表現でプレースホルダーを置き換えるために指定する
	| 言語行です。これはメッセージをよりきれいに表示するために役に立ちます。
	|
	*/

	'attributes' => array(
	),
	
	
];
