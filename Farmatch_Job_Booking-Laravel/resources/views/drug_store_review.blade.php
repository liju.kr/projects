@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-chat"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        評価
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬剤師</th>
                            <th>薬局/病院等</th>
                            <th>Eメール</th>
                            <th>電話番号</th>
                            <th>勤務日</th>
                            <th>薬局/病院等の雰囲気</th>
                            <th>事務員の対応</th>
                            <th>薬剤師の対応</th>
                            <th>コンプライアンス遵守の程度</th>
                            <th>合計</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allReviews as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->chemist }}</td>
                            <td>{{ $row->drugStore }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->postDate }}</td>
                            <td><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">{{ $row->atmosphere }}</span></td>
                            <td><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">{{ $row->clerk_adaption }}</span></td>
                            <td><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">{{ $row->pharmacist_adaption }}</span></td>
                            <td><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">{{ $row->compliance_level }}</span></td>
                            <td><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">{{ ($row->atmosphere + $row->clerk_adaption + $row->pharmacist_adaption + $row->compliance_level)/4 }}</span></td>
                            <td>
                                <a href="#" class="btn btn-lg btn-clean btn-icon btn-icon-md drug-store-review-details" title="View Details" data-id="{{ $row->id }}">
                                    <i class="la la-eye" style="font-size: 2.3rem;"></i>
						        </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
<!--begin::Modal-->
<div class="modal fade" id="kt_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">評価</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body review-details-modal">
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">とじる</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var drugStoreReviewDetails = "{{ route('drug-store-review-details') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/reviews/reviews.js') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
