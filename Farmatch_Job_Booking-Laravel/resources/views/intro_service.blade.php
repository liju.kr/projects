@extends('layouts.app')
@section('page-content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{ @$page_title }} </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Dashboard 1-->
        <!--Begin::Row-->
        <div class="row">
            <!--begin::Portlet-->
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                {{ @$page_title }}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <div class="container">
                            <div class="row">
                              <div class="col-xs-12 w-100">
                                <table id="kt_table_1" class="table table-bordered table-hover dt-responsive">
                                  {{-- <caption class="text-left">案件受付チャート案件</caption> --}}
                                  <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>薬局名</th>
                                        <th>所在地</th>
                                        <th>受付年月日</th>
                                        <th>最新更新日</th>
                                        <th>ステータス</th>
                                        <th style="width:auto">アクション</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  @php $i=1; @endphp
                                  @foreach($drugStores as $row)
                                      @php
                                          $employee_intro_service_checking_1 = App\EmployeeIntroService::where('drug_store_id', $row->id)->first();
                                          if(date('m') >= 4)
                                              {
                                                  $min_date = date('Y')."-04-01";
                                              }
                                              else
                                                  {
                                                      $min_date = (date('Y')-1)."-04-01";
                                                  }
                                      @endphp
                                      @if($employee_intro_service_checking_1)
                                          <tr>
                                              <td>{{ $i }}</td>
                                              <td>{{ $row->name }}</td>
                                              <td>{{ $row->address }}</td>
                                              <td>{{ $row->employee_intro_date }}</td>
                                              <td>{{ $employee_intro_service_checking_1->date }}</td>
                                              <td>@if((strtotime($row->employee_intro_date) >= strtotime($min_date)) && $row->employee_intro_service )<span class="kt-font-bold kt-font-success">アクティブ</span> @else <span class="kt-font-bold kt-font-danger">利用停止中</span> @endif</td>

                                              <td nowrap>
                                                  <a href="{{ url('/intro-test/'.$row->id) }}" target="_blank"><button class="btn btn-success btn-sm mr-2">ダウンロード</button></a>
                                                  <button type="button" class="btn btn-primary btn-sm delete-intro" data-id="{{ $row->id}}">削除</button>
                                              </td>
                                          </tr>

                                          @php $i=$i+1; @endphp
                                      @endif
                                  @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                    </div>
                </div>
            </div>


        </div>
        <!--End::Dashboard 1-->
    </div>
    <!-- end:: Content -->
</div>

@endsection
@section('script')
<script>
    var routeDelete       = '{{ route('delete-intro') }}';;
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/intro/intro.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
@endsection
