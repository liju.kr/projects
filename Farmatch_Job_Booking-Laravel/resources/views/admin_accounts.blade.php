@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{ @$page_title }}
                </h3>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label col-md-12">
                    <h3 class="kt-portlet__head-title col-md-12">
                        {{ @$page_title }}
                        <a href="{{ route('view-admin-actions') }}" class="btn btn-primary pull-right ml-2">
                            アクション履歴を⾒る
                        </a>
                        <a href="{{ route('add-new-admin') }}" class="btn btn-success pull-right">
                            <i class="flaticon2-add-1 ml-2" style="font-size: 1rem; float: right;"></i> 新しい管理者を追加
                        </a>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>姓</th>
                            <th>名</th>
                            <th>役職・役割</th>
                            <th>所在地</th>
                            <th>連絡先</th>
                            <th>Eメール</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->last_name }}</td>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->company_name }}</td>
                            <td>{{ $row->location }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->email }}</td>
                            <td nowrap>
                                <a href="view-admin-details/{{ $row->id }}" class="btn btn-lg btn-clean btn-icon btn-icon-md edit-prefecture" title="Chat">
                                    <i class="la la-eye" style="font-size: 2.3rem;"></i>
						        </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/accounts/account.js') }}" type="text/javascript"></script>
@endsection
