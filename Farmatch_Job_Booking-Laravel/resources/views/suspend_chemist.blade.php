@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Aside Mobile Toggle-->
            <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                <i class="la la-close"></i>
            </button>
            <!--End:: App Aside Mobile Toggle-->
            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
                <!--begin:: Widgets/Applications/User/Profile1-->
                <div class="kt-portlet ">
                    <div class="kt-portlet__head  kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-1">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img src="{{ ($chemist->image) ? asset('images/chemist/'.$chemist->image) : asset('images/no-image.png') }}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                        {{ $chemist->last_name }} {{ $chemist->first_name }}
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        </a>
                                        <span class="kt-widget__subtitle">
                                            薬剤師
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">Eメール:</span>
                                        <a href="#" class="kt-widget__data">{{ $chemist->email }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">電話番号:</span>
                                        <a href="#" class="kt-widget__data">{{ $chemist->phone }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">薬剤師名簿登録番号:</span>
                                        <span class="kt-widget__data">{{ $chemist->licence_number }}</span>
                                    </div>
                                    <div class="kt-align-right">
                                        <button type="button" class="btn btn-danger btn-elevate" id="kt_sweetalert_demo_10" {{ ($chemist->status == 'Suspended') ? "disabled" : "" }}>
                                            <i class="fa fa-arrow-right"></i>{{ ($chemist->status == 'Suspended') ? "停職済み" : "停職" }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
                <!--end:: Widgets/Applications/User/Profile1-->
            </div>
            <!--End:: App Aside-->
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">薬剤師一般情報</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                </div>
                            </div>
                            <form class="kt-form kt-form--label-right" id="kt_pending_form">
                                <div class="kt-portlet__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">プロフィール写真</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--danger" id="kt_user_avatar_4">
                                                        <div class="kt-avatar__holder" style="background-image: url({{ asset('images/chemist/'.$chemist->image) }})"></div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="プロフィールを変更する">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="main_image">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                    </div>
                                                    <span class="form-text text-muted">許可されるファイルタイプ：png、jpg、jpeg</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">姓</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="last_name" value="{{ $chemist->last_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">名</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" name="id" id="id" type="text" value="{{ $chemist->id }}" style="display: none;">
                                                    <input class="form-control" type="text" name="first_name" value="{{ $chemist->first_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">メイ</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="first_furigana" value="{{ $chemist->first_furigana }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">性別</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-radio-inline">
                                                        <label class="kt-radio">
                                                            <input type="radio" name="gender" value="Male" {{ ($chemist->gender == 'Male') ? "checked" : "" }}> 男
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" name="gender" value="Female" {{ ($chemist->gender == 'Female') ? "checked" : "" }}> 女
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">生年月日</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="dob" value="{{ $chemist->dob }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">郵便番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="post_code" value="{{ $chemist->post_code }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">電話番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="phone" value="{{ $chemist->phone }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">都道府県</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="prefecture" value="{{ $chemist->prefecture }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">市区都</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="city" value="{{ $chemist->city }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">町村〜番地</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="town" value="{{ $chemist->town }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">セイ</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="furikana" value="{{ $chemist->furikana }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Eメール</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="email" value="{{ $chemist->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬剤師名簿登録番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="licence" value="{{ $chemist->licence_number }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">保険薬剤師番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" name="insurance" type="text" value="{{ $chemist->insurance_number }}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">扶養家族数(配偶者を除く)</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" readonly type="text" name="no_of_dependents" value="{{ $chemist->no_of_dependents }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">配偶者</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-radio-inline">
                                                        <label class="kt-radio">
                                                            <input type="radio" readonly name="spouse" value="Yes" {{ ($chemist->spouse == 'Yes') ? "checked" : "" }}> 有
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" readonly name="spouse" value="No" {{ ($chemist->spouse == 'No') ? "checked" : "" }}> 無
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">配偶者の扶養義務</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-radio-inline">
                                                        <label class="kt-radio">
                                                            <input type="radio" readonly name="spousal_support" value="Yes" {{ ($chemist->spousal_support == 'Yes') ? "checked" : "" }}> 有
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" readonly name="spousal_support" value="No" {{ ($chemist->spousal_support == 'No') ? "checked" : "" }}> 無
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">返答時間目安</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="response_time" value="{{ $chemist->response_time }}" readonly>
                                                </div>
                                            </div>
                                           <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">自己紹介文</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea name="self_introduction" class="form-control" rows="5" readonly>{{ $chemist->self_introduction }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">志望動機・特技・好きな学科など</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea name="skills" class="form-control" rows="5" readonly>{{ $chemist->skills }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">本人希望記入欄</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea name="desired_entry" class="form-control" rows="5" readonly>{{ $chemist->desired_entry }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬剤師名簿登録年</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="registration_year" value="{{ $chemist->registration_year }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬剤師名簿登録月</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="registration_month" value="{{ $chemist->registration_month }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-3 col-xl-3">
                                            </div>
                                            <div class="col-lg-9 col-xl-9">
                                                <button type="submit" id="kt_pending_update" class="btn btn-success">更新</button>&nbsp;
                                                <button type="reset" class="btn btn-secondary">キャンセル</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--begin::Page Scripts(used by this page) -->
<script>
    var route        = "{{ route('update-chemist') }}";
    var suspendRoute = "{{ route('suspend-chemist-request') }}";
</script>
<script src="{{ asset('js/pages/custom/chemist/chemist.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js?ver4') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
