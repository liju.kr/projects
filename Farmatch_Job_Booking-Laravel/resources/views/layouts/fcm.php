<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js"></script>
<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.0/firebase-database.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCPQmyIUMiQmyWcU4ZRAY1vW-pwIMZVF88",
    authDomain: "farmatch-ce7f6.firebaseapp.com",
    databaseURL: "https://farmatch-ce7f6.firebaseio.com",
    projectId: "farmatch-ce7f6",
    storageBucket: "farmatch-ce7f6.appspot.com",
    messagingSenderId: "96784333247",
    appId: "1:96784333247:web:a7a4f3ea6da4b7536f890a",
    measurementId: "G-TWN82G2XKV"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>