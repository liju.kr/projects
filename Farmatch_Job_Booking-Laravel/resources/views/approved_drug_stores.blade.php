@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    薬局/病院等 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-supermarket"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        薬局/病院等
                    </h3>
                </div>
                <div class="kt-searchbar pt-3">
                    <div class="input-group">
                        <select name="cities" id="cities" class="form-control">
                            <option value="">都市を選択</option>
                            @foreach($cities as $row)
                            <option value="{{ $row->id }}">{{  $row->city }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button" id="serach_by_city" data-status="Approved">検索</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>店舗名</th>
                            <th>担当者名</th>
                            <th>電話番号</th>
                            <th>Eメール</th>
                            <th>登録済み状況</th>
                            <th>ステータス</th>
                            <th>オンライン状況</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($drugStores as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->in_charge }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                            <td>1</td>
                            <td>{{ ($row->online_status == 'Online') ? 1 : 2 }}</td>
                            <td nowrap>
                                <a href="{{route('chat')}}?store={{$row->id}}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                    <i class="la la-wechat" style="font-size: 2.3rem;"></i>
						        </a>
						        <a href="suspend-drug-store/{{ $row->id }}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="View Chemist">
                                    <i class="la la-eye" style="font-size: 2.3rem;"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var searchRoute     = "{{ route('search-drug-store') }}";
    var locationRoute   = "{{ route('location-search-drug-store') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/pending.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/drug_store/drug_store.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>
@endsection