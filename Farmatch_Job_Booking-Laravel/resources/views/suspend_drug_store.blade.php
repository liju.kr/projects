@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
             <!--Begin:: App Aside Mobile Toggle-->
            <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                <i class="la la-close"></i>
            </button>
            <!--End:: App Aside Mobile Toggle-->
            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
                <!--begin:: Widgets/Applications/User/Profile1-->
                <div class="kt-portlet ">
                    <div class="kt-portlet__head  kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-1">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img src="{{ ($drugStore->main_image) ? asset('images/drug_store/'.$drugStore->main_image) : asset('images/no-image.png') }}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            {{ $drugStore->name }}
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        </a>
                                        <span class="kt-widget__subtitle">
                                            薬局/病院等
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">メールアドレス:</span>
                                        <a href="#" class="kt-widget__data">{{ $drugStore->email }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">担当者電話番号:</span>
                                        <a href="#" class="kt-widget__data">{{ $drugStore->phone }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">担当者:</span>
                                        <span class="kt-widget__data">{{ $drugStore->in_charge }}</span>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label"></span>
                                        <img src="{{ asset('images/qr-code.png') }}" alt="image" style="width: 50px;">
                                        {{-- <span class="kt-widget__data"><a href="{{ route('drug-store-qr-code', $drugStore->id) }}">View Qr Code</a></span> --}}
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label"></span>
                                        <span class="kt-widget__data" style="font-size: .75em"><a href="{{ route('drug-store-qr-code', $drugStore->id) }}">QRコードを<br>ダウンロード</a></span>
                                    </div>
                                    <div class="kt-align-right">
                                        <button type="button" class="btn btn-danger btn-elevate" id="kt_sweetalert_demo_12" {{ ($drugStore->status == 'Suspended') ? "disabled" : "" }}>
                                            <i class="fa fa-arrow-right"></i>{{ ($drugStore->status == 'Suspended') ? "停職済み" : "停職" }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
                <!--end:: Widgets/Applications/User/Profile1-->
            </div>
            <!--End:: App Aside-->
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">薬局/病院等 一般情報</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                </div>
                            </div>
                            <form class="kt-form kt-form--label-right" id="kt_pending_form">
                                <div class="kt-portlet__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">外観写真</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--danger" id="kt_user_avatar_4">
                                                        <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->main_image) }})"></div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="プロフィールを変更する">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="main_image">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                    </div>
                                                    <span class="form-text text-muted">許可されるファイルタイプ：png、jpg、jpeg</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">補足写真</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                        <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->sub_image1) }})"></div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="プロフィールを変更する">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="sub_image1">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                        <span class="form-text text-muted">png、jpg、jpeg</span>
                                                    </div>
                                                    <div class="kt-avatar kt-avatar--outline pl-5" id="kt_user_avatar1">
                                                        <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->sub_image2) }})"></div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="プロフィールを変更する">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="sub_image2">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                        <span class="form-text text-muted">png、jpg、jpeg</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬局/病院等</label>
                                                <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="id" id="id" type="text" value="{{ $drugStore->id }}" style="display:none">
                                                    <input class="form-control" type="text" name="name" value="{{ $drugStore->name }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">所在地</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="address" value="{{ $drugStore->address }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">代表者名</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="manager_company" value="{{ $drugStore->manager_company }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬歴の機種</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="kt-radio-inline">
                                                        <label class="kt-radio">
                                                            <input type="radio" name="history_model" value="EM system" {{ ($drugStore->history_model == 'EM system' ) ? "checked": "" }}> EM system
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" name="history_model" value="Gooco" {{ ($drugStore->history_model == 'Gooco' ) ? "checked": "" }}> Gooco
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" name="history_model" value="Melphin" {{ ($drugStore->history_model == 'Melphin' ) ? "checked": "" }}> Melphin
                                                            <span></span>
                                                        </label>
                                                        <label class="kt-radio">
                                                            <input type="radio" name="history_model" value="Others" {{ ($drugStore->history_model == 'Others' ) ? "checked": "" }}> その他
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    {{-- <input class="form-control" type="text" name="history_model" value="{{ $drugStore->history_model }}"> --}}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">担当者電話番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="mobile" value="{{ $drugStore->phone }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">施設電話番号</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="alter_mobile" value="{{ $drugStore->alter_phone }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">メールアドレス</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="email" value="{{ $drugStore->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">担当者</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="in_charge" value="{{ $drugStore->in_charge }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">店舗情報</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea rows="5" name="introduction" class="form-control">{{ $drugStore->introduction }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">事務員</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="clerks" value="{{ $drugStore->clerks }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">薬剤師</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control" type="text" name="chemists" value="{{ $drugStore->chemists }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">返答目安時間</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select name="response_speed" id="response_speed" class="form-control">
                                                        <option value="1日以内" {{ ($drugStore->respond_speed == '1日以内') ? "selected" : "" }}>1日以内</option>
                                                        <option value="２日以内" {{ ($drugStore->respond_speed == '２日以内') ? "selected" : "" }}>２日以内</option>
                                                        <option value="３日以内" {{ ($drugStore->respond_speed == '３日以内') ? "selected" : "" }}>３日以内</option>
                                                        <option value="３日以上" {{ ($drugStore->respond_speed == '３日以上') ? "selected" : "" }}>３日以上</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">その他情報</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea rows="5" name="other_informations" class="form-control">{{ $drugStore->other_information }}</textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-3 col-md-3"></div>
                                                <div class="col-xl-9 col-md-9">
                                                    <div class="form-group row">
                                                        <div class="col-xl-6 col-lg-6 col-form-label" style="font-family: cursive;">ふぁーまっち利用年数 : {{ @$drugStoreExperience }} 年
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-form-label" style="font-family: cursive;">勤務回数 : {{ @$completedWorks }}
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-form-label" style="font-family: cursive;">累計勤務人数 : {{ @$chemistWorked }}
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-form-label" style="font-family: cursive;">薬剤師リピート人数 : {{ @$chemistRepeated }}
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-form-label" style="font-family: cursive;">直前キャンセル件数: {{ @$suddenCancellation }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-3 col-xl-3">
                                            </div>
                                            <div class="col-lg-9 col-xl-9">
                                                <button type="submit" id="kt_pending_update" class="btn btn-success">更新</button>&nbsp;
                                                <button type="reset" class="btn btn-secondary">キャンセル</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--begin::Page Scripts(used by this page) -->
<script>
    var route             = "{{ route('update-drug-store') }}";
    var suspendStoreRoute = "{{ route('suspend-drug-store-request') }}";
</script>
<script src="{{ asset('js/pages/custom/drug_store/drug_store.js?ver1') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js?ver4') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
