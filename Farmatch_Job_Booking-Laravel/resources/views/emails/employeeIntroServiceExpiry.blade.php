
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head></head>

<body>
  <style></style>
  <div class="W(100%) Mx(a) Bgc($white) Fl(left) Py(30px)"
    style="background-color:#fff;margin-left:auto;margin-right:auto;padding-bottom:30px;padding-top:30px;width:100%">
    <div class="Maw(600px) H(a) Bgc(#fff) Mx(a) My(15px) Bds(s) Bdc($gray) Bdw(2px) Ov(h)"
      style="background-color:#fff;border-color:#EBEBEB;border-style:solid;border-width:2px;height:auto;margin-bottom:15px;margin-left:auto;margin-right:auto;margin-top:15px;max-width:600px;overflow:hidden">
      <div class="P(30px)" style="padding:30px"><img src="https://i.ibb.co/XWs5tXP/logo-color.png" alt=""
          class="W(80px) D(b) Mstart(0) Mb(30px)" style="display:block;margin-bottom:30px;margin-left:0;width:80px">
        <p class="Py(15px) My(0px) Fz(16px) C(#333)"
          style="color:#333;font-size:16px;margin-bottom:0;margin-top:0;padding-bottom:15px;padding-top:15px">{{$data['username']}}　<b class="Fw(400)" style="font-weight:400">さん</b></p>
        <p class="Py(15px) My(0px) Fz(16px) C(#333)"
          style="color:#333;font-size:16px;margin-bottom:0;margin-top:0;padding-bottom:15px;padding-top:15px">
            {{$data['title']}}</p>
        <p class="Py(15px) My(0px) Fz(16px) C(#333)"
          style="color:#333;font-size:16px;margin-bottom:0;margin-top:0;padding-bottom:15px;padding-top:15px">
            {{$data['message']}}</p>
{{--        <p class="Py(15px) My(0px) Fz(16px) C(#333) Pb(0px)"--}}
{{--          style="color:#333;font-size:16px;margin-bottom:0;margin-top:0;padding-bottom:0;padding-top:15px">--}}
{{--          {{$hours}} 時間後にシステムにより自動で勤務内容が確定されますので、お早めに勤務内容の確認を行ってくださいますようお願い致します。</p>--}}
      </div>
      <div class="Bgc($primary) Jc(sb) D(b) W(100%) Fl(start) Pstart(30px) Pend(30px) Bxz(bb) Pt(10px) Pb(10px)"
        style="background-color:#F9EEC8;box-sizing:border-box;display:block;float:left;justify-content:space-between;padding-bottom:10px;padding-left:30px;padding-right:30px;padding-top:10px;width:100%">
        <div class="Fl(start) Py(10px) Ta(c) W(100%)"
          style="float:left;padding-bottom:10px;padding-top:10px;text-align:center;width:100%">ふぁーまっち運営本部</div>
        <div class="Fl(end) Py(10px) Ta(c) W(100%)"
          style="float:right;padding-bottom:10px;padding-top:10px;text-align:center;width:100%">{{ date('Y') }} All copyrights
          reserved ふぁーまっち</div>
      </div>
    </div>
  </div>
</body>

</html>
