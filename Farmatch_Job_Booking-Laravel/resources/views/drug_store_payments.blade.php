@extends('layouts.app')
@section('page-content')
<link rel="stylesheet" href="{{asset('js/wickedpicker.min.css')}}">
<style type="text/css">
    body{ font-family: 'Poppins', sans-serif; font-size: 14px; }
    ul{ list-style: none; padding-left: 0; }
    h4.tophead{ display: inline-block; font-size: 14px; margin-right: 100px; margin-bottom: 20px;}
    .modal-top-area ul{ list-style: none;padding: 0;} .modal-top-area li{ display: inline-block; padding-right: 34px; }
    .modal-content-area li { display: inline-block;  padding: 5px 20px 0 0; }  .modal-ontent-area ul{ display: inline-block; margin-bottom: 0!important; }
    .modal-dialog {
    max-width: 950px!important;
    margin: 1.75rem auto;
    }
    .modal-content{ padding: 15px 40px; }
    .btn-green{ padding: 6px 25px; background-color: #00B480; border-color: #59BF71;color: #fff; }
    .btn-red{ padding: 6px 25px; background-color: #FF5A5A; border-color: #FC4949; color: #fff;}
    .modal-header .close{ background-color: #3C3C3C; color: #fff; width: 30px; height: 30px; border-radius: 50px; margin: 0; padding: 4px 10px; }
    .form-row .col{ padding-left: 20px; }
    .pt-50{ padding-top: 50px; }
    .w-40{ width: 28%; }
    .middle-content p, .form-check, .col { line-height: 38px; }
    a {
        color: #161618 !important;
    }
    .numberCircle {
        border-radius: 50%;
        width: 36px;
        height: 36px;
        padding: 8px;

        background: #fff;
        border: 2px solid #666;
        color: #666;
        text-align: center;

        font: 32px Arial, sans-serif;
    }
    .modal .modal-content .modal-header .close:before {
        line-height: 22px;
    }

    .modal .modal-content .modal-header .close {
        background: red !important;
    }

    .middle-content.pt-50 {
        width: 100%;
    }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="#" class="kt-subheader__breadcrumbs-link">
                        {{-- {{ @$page_title }} </a> --}}
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-price-tag"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        薬局/病院等 請求情報
                    </h3>
                    <div class="kt-searchbar pt-3 mr-3 ml-5">
                        <select class="form-control text-right" name="year" id="year">
                            <option value="">年を選択</option>
                            @for($i = 2020; $i <= date('Y'); $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="kt-searchbar pt-3 mr-1">
                        <select class="form-control text-right" name="month" id="month">
                            {{-- <option value="">月を選択</option>
                            @for($i = 1; $i <= date('m')-1; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor --}}
                        </select>
                    </div>
                    <div class="kt-searchbar pt-3 mr-3">
                        <button type="button" class="btn btn-success btn-elevate" id="kt_sweetalert_demo_19">
                            <i class="fa fa-arrow-right"></i>検索
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_19">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬局/病院等</th>
                            <th>代表者名</th>
                            <th>開始日</th>
                            <th>締め日</th>
                            <th>勤務時間合計</th>
                            <th>マッチング手数料</th>
                            <th>直前キャンセル</th>
                            <th>総計</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($totalPayments as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row['drug_store'] }}</td>
                            <td>{{ $row['manager_company'] }}</td>
                            <td>{{ date('d/m/Y', strtotime($row['from_date'])) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row['end_date'])) }}</td>
                            <td>{{ $row['total_hours'] }}</td>
                            <td>{{ $row['total_wage'] }}</td>
                            <td>{{ $row['cancel_count'] }}</td>
                            <td>{{ round($row['sub_total']) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
              <div class="modal-header border-0">
                 <h5 class="modal-title" id="staticBackdropLabel"><b>詳細</b></h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 0 0 0 0px;color: white;background-color: #0b0b0b;">
                 <span aria-hidden="true">&times;</span>
                 </button>
              </div>
              <div class="modal-body">
                 <div class="modal-condent">
                    <div class="modal-top-area">
                        <input type="text" id="invoice-id" style="display: none;">
                       <h4 class="tophead">薬局/病院等名</h4>
                       <h4 class="tophead" id="drug-store-name"></h4>
                       <ul>
                          <li>登録日</li>
                          <li id="reg-date"></li>
                          <li>掲載開始日</li>
                          <li id="first-post-date"></li>
                       </ul>
                    </div>
                    <div class="modal-content-area middle-content">
                       <div class="row">
                          <div class="col-md-6">
                             <p>マッチング手数料</p>
                             <p>ふぁーまっち月間掲載料</p>
                          </div>
                          <div class="col-md-6 text-right">
                             <p id="total-wage"></p>
                             <p>2200</p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-md-6">
                             <div class="form-check form-check-inline">
                                <input class="form-check-input mr-3" type="checkbox" id="inlineCheckbox3" value="option3" onclick="subscribtionAddRemove()">
                                <label class="form-check-label">掲載料割引</label>
                             </div>
                          </div>
                          <div class="col-md-6 text-right">
                             <p>-2200</p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-md-4">
                             <div class="form-check form-check-inline">
                                <input class="form-check-input mr-3" type="checkbox" id="inlineCheckbox4" value="option4" onclick="inputValue()">
                                <label class="form-check-label">自由記入</label>
                             </div>
                          </div>
                          <div class="col-md-8 text-right">
                             <form>
                                <div class="form-row">
                                   <div class="col">
                                      <input type="text" class="form-control" id="input-text" style="width: 150px" placeholder="品目">
                                   </div>
                                   <div class="col" style="padding-left: 2px;">
                                      <input type="text" class="form-control new-input" id="qty" maxlength="2" style="width: 100px" placeholder="数量">
                                   </div>
                                   <div class="col" style="padding-left: 0px;">
                                      <input type="text" class="form-control new-input" id="amount" maxlength="6" style="width: 100px" placeholder="価格">
                                   </div>
                                </div>
                             </form>
                          </div>
                       </div>

                       <!-- <div class="row">
                        <div class="middle-content pt-50">
                            <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col">Matching Details </th>
                                    <th scope="col"> Chemist Name </th>
                                    <th scope="col"> Work Date </th>
                                    <th scope="col"> Work Hours </th>
                                    <th scope="col">Hourly Wage </th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody id="complete_posts">
                            </tbody>
                            </table>
                        </div>
                       </div>
                       <div class="row">
                        <div class="middle-content pt-50">
                            <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col">直前キャンセル</th>
                                    <th scope="col"> 薬剤師名</th>
                                    <th scope="col"> 勤務日</th>
                                    <th scope="col"> 勤務時間</th>
                                    <th scope="col">時給</th>
                                </tr>
                            </thead>
                            <tbody id="cancel-posts">
                            </tbody>
                            </table>
                            <table class="table table-borderless">
                                <tr>
                                    <td height="20px"></td>
                                </tr>
                                <tr>
                                    <th scope="row" colspan="4"><b>合計請求額</b></th>
                                    <td align="right" id="grand-total"><b></b></td>
                                    {{-- <p class="text-right d-inline-block float-right" id="grand-total"><b></b></p> --}}
                                <input type="text" name="grand-total-hidden" id="grand-total-hidden" style="display: none;">
                                <input type="text" name="input-total-hidden" id="input-total-hidden" value="0" style="display: none;">
                                </tr>
                            </table>
                        </div>
                       </div> -->
                       
                       {{-- <div class="row pt-50">
                          <div class="col-md-12" id="cancel-jobs">
                           <ul class="pb-4">
                             <li class="w-40">直前キャンセル</li>
                             <li>薬剤師名</li>
                             <li>勤務日</li>
                             <li>勤務時間</li>
                             <li>時給</li>
                           </ul>
                           <div id="">

                           </div>
                             <br>
                             <br>
                             <span>
                                <p class="d-inline-block"><b>合計請求額</b></p>
                             </span>
                             <span>
                                <p class="text-right d-inline-block float-right" id="grand-total"><b></b></p>
                                <input type="text" name="grand-total-hidden" id="grand-total-hidden" style="display: none;">
                                <input type="text" name="input-total-hidden" id="input-total-hidden" value="0" style="display: none;">
                             </span>
                             <br>
                             <br>
                          </div>
                       </div> --}}
                       <div class="row mt-5">
                            <div class="col-md-3">
                            <ul>
                                <li>振込先銀⾏</li>
                            </ul>
                            </div>
                            <div class="col-md-9 pl-0 mb-5">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input mr-3" type="radio" name="bank-details" value="Non Default">
                                    <label class="form-check-label">⼤分みらい信⽤⾦庫 本店</label>
                                </div>
                                <div class="form-check form-check-inline ml-5">
                                    <input class="form-check-input mr-3" type="radio" name="bank-details" value="Default" checked>
                                    <label class="form-check-label">⼤分銀⾏ 別府⽀店</label>
                                </div>
                            </div>
                       </div>
                       <div class="row">
                          <div class="col-md-3">
                             <ul>
                                <li>備考</li>
                             </ul>
                          </div>
                          <div class="col-md-9 pl-0">
                             <form>
                                <div class="form-group">
                                   <textarea class="form-control" rows="3" id="remarks"></textarea>
                                   <button type="submit" class="btn btn-green my-4 mr-4" id="saveAdditions">反映する</button>
                                   <button type="submit" class="btn btn-red my-4" id="resetModel">クリア</button>
                                </div>
                             </form>
                          </div>
                       </div>
                       <div class="row justify-content-end ">
                        <button class="showTable btn btn-secondary btn-sm" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down-right"><line x1="7" y1="7" x2="17" y2="17"></line><polyline points="17 7 17 17 7 17"></polyline></svg>
                        </button>
                        <button class="hideTable btn btn-secondary btn-sm" type="button" style="display: none">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-left"><line x1="17" y1="17" x2="7" y2="7"></line><polyline points="7 17 7 7 17 7"></polyline></svg>
                        </button>
                       </div>

                    <div class="toggleTable" style="display:none">
                       <div class="row">
                        <div class="middle-content pt-50">
                            <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col"> マッチング詳細 </th>
                                    <th scope="col"> 薬剤師名 </th>
                                    <th scope="col"> 勤務日 </th>
                                    <th scope="col"> 勤務時間 </th>
                                    <th scope="col"> 時給 </th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody id="complete_posts">
                            </tbody>
                            </table>
                        </div>
                       </div>
                       <div class="row">
                        <div class="middle-content pt-50">
                            <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col">直前キャンセル</th>
                                    <th scope="col"> 薬剤師名</th>
                                    <th scope="col"> 勤務日</th>
                                    <th scope="col"> 勤務時間</th>
                                    <th scope="col"> 時給</th>
                                </tr>
                            </thead>
                            <tbody id="cancel-posts">
                            </tbody>
                            </table>
                            <table class="table table-borderless">
                                <tr>
                                    <td height="20px"></td>
                                </tr>
                                <tr>
                                    <th scope="row" colspan="4"><b>合計請求額</b></th>
                                    <td align="right" id="grand-total"><b></b></td>
                                    {{-- <p class="text-right d-inline-block float-right" id="grand-total"><b></b></p> --}}
                                <input type="text" name="grand-total-hidden" id="grand-total-hidden" style="display: none;">
                                <input type="text" name="input-total-hidden" id="input-total-hidden" value="0" style="display: none;">
                                </tr>
                            </table>
                        </div>
                       </div>
                    </div>
                       
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
</div>
@endsection
@section('script')
<script>
    var chemistReviewDetails            = "{{ route('chemist-review-details') }}";
    var changeDrugStorePaidStatus       = "{{ route('change-drug-store-paid-status') }}";
    var searchDrugStorePayment          = "{{ route('search-drug-store-payment') }}";
    var generateDrugstoreInvoice        = "{{ route('generate-drugstore-invoice') }}";
    var generateAllDrugstoreInvoice     = "{{ route('generate-all-drugstore-invoice') }}";
    var changeDrugstoreInvoiceStatus    = "{{ route('change-drugstore-invoice-status') }}";
    var generateInvoicePopup            = "{{ route('generate-invoice-popup') }}";
    var saveAdditionalDetails           = "{{ route('save-invoice-additional-details') }}"
    var downloadAllDrugstoreInvoice     = "{{ route('download-all-drugstore-invoice') }}";
    var updateWorkingHoursCompletedJobs = "{{ route('update-completed-working-hours') }}"

</script>
<script>
    $("#year").change(function(){

        var year        = $(this).val();
        var currentYear = new Date().getFullYear();
        if(year == currentYear){
            var date       = new Date();
            var monthCount = date.getMonth();
            var option = '<option value="">月を選択</option>';
            for(var i= 1; i<= monthCount; i++){
                option += '<option value='+i+'>'+i+'</option>';
            }
            $("#month").html(option);
        }
        else{
            var option = '<option value="">月を選択</option>';
            for(var i= 1; i<= 12; i++){
                option += '<option value='+i+'>'+i+'</option>';
            }
            $("#month").html(option);
        }
    })
    $('[name=bank-details]').click(function(){
        $('[name=bank-details]').not(this).prop('checked', false);
   	});
    $("#resetModel").click(function(){
        $("#input-text").val('');
        $("#qty").val('');
        $("#amount").val('');
        $("#remarks").val('');
        var grandTotalHidden = parseFloat($("#grand-total-hidden").val())
        if($("#inlineCheckbox3").prop('checked') == true){
            $("#inlineCheckbox3").prop('checked', false);
            $("#grand-total-hidden").val(parseFloat(grandTotalHidden+parseInt(2200)))
        }
        if($("#inlineCheckbox4").prop('checked') == true){
            $("#inlineCheckbox4").click();
        }
        var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
        $("#grand-total").html("<b>"+grandTotal+"</b>")
    })

    function resetModal(){

        $("#input-text").val('');
        $("#qty").val('');
        $("#amount").val('');
        $("#remarks").val('');
        var grandTotalHidden = parseFloat($("#grand-total-hidden").val())
        if($("#inlineCheckbox3").prop('checked') == true){
            $("#inlineCheckbox3").prop('checked', false);
        }
        if($("#inlineCheckbox4").prop('checked') == true){
            $("#inlineCheckbox4").prop('checked', false);
            $("#input-total-hidden").val(0);
        }
    }
    $("#saveAdditions").click(function(){
        if($("#inlineCheckbox4").prop('checked') == true){
            if($("#input-text").val() == ''){
                $("#input-text").css("border-color", "red");
            }
            if($("#qty").val() == ''){
                $("#qty").css("border-color", "red");
            }
            if($("#amount").val() == ''){
                $("#amount").css("border-color", "red");
            }
            if($("#input-text").val() == '' || $("#qty").val() == '' || $("#amount").val() == ''){
                return false;
            }else{
                saveInputs($("#input-text").val(), $("#qty").val(), $("#amount").val());
            }
        }else{
            saveInputs(0,0,"");
        }
    })

    function saveInputs(inputText, quantity, amount ){

        var remarks         = $("#remarks").val();
        var invoiceId       = $("#invoice-id").val();
        var bankDetails     = $('input[name="bank-details"]:checked').val();
        var discount        = 0
        if($("#inlineCheckbox3").prop('checked') == true){
            discount = 2200;
        }
        $.ajax({
            url: saveAdditionalDetails,
            method: 'POST',
            data: {
                invoice_id : invoiceId,
                discount : discount,
                remarks:remarks,
                input_text: inputText,
                quantity: quantity,
                amount:amount,
                ban_details: bankDetails
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response, status, xhr, $form) {
                if (response.status == true) {
                    swal.fire(
                        '完了',
                        response.message,
                        'success'
                    )
                    setTimeout(function(){
                        $("#staticBackdrop").modal("toggle");
                    }, 3000);

                } else {
                    swal.fire(
                        '',
                        response.message,
                        'error'
                    )
                }
            }
        });
    }

    function subscribtionAddRemove(){

        var grandTotalHidden = parseFloat($("#grand-total-hidden").val())
        if($("#inlineCheckbox3").prop('checked') == true){
            $("#grand-total-hidden").val(parseFloat(grandTotalHidden-parseInt(2200)))
            var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
            $("#grand-total").html("<b>"+grandTotal+"</b>")
        }else{

            $("#grand-total-hidden").val(parseFloat(grandTotalHidden+parseInt(2200)))
            var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
            $("#grand-total").html("<b>"+grandTotal+"</b>")
        }
    }

    function inputValue(){

        if($("#inlineCheckbox4").prop('checked') == true){
            var qty        = $("#qty").val();
            var amount     = $("#amount").val();
            qty = (qty == null) ? 0 : qty;
            amount = (amount == null) ? 0 : amount;
            var total      = qty * amount;
            $("#input-total-hidden").val(parseFloat(parseInt(total)))
            var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
            $("#grand-total").html("<b>"+grandTotal+"</b>")
        }else{
            var qty    = $("#qty").val();
            var amount = $("#amount").val();
            qty = (qty == null) ? 0 : qty;
            amount = (amount == null) ? 0 : amount;
            var total  = qty * amount;
            $("#input-total-hidden").val(0)
            var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
            $("#grand-total").html("<b>"+grandTotal+"</b>")
        }
    }

    $("#qty").bind("keypress", function (e) {

        var keyCode = e.which ? e.which : e.keyCode
        if (!(keyCode >= 48 && keyCode <= 57)) {
            return false;
        }
    });

    $("#amount").bind("keypress", function (e) {

        var keyCode = e.which ? e.which : e.keyCode
        if (!((keyCode >= 48 && keyCode <= 57) || keyCode == 43 || keyCode == 45)) {
            return false;
        }
    });

    $('#qty, #amount').keyup(function(){

        if($("#inlineCheckbox4").prop('checked') == true){
            var qty        = $("#qty").val();
            var amount     = $("#amount").val();
            qty = (qty == null) ? 0 : qty;
            amount = (amount == null) ? 0 : amount;
            var total      = qty * amount;
            $("#input-total-hidden").val(parseFloat(parseInt(total)))
            var grandTotal = parseFloat($("#input-total-hidden").val())+parseFloat($("#grand-total-hidden").val());
            $("#grand-total").html("<b>"+grandTotal+"</b>")
        }
    });

    function modalPopup(drug_store_id, from_date, end_date, total_wage, commision, sub_total, tax, grand_total){

        $.ajax({
            url: generateInvoicePopup,
            method: 'POST',
            data: {
                drug_store_id : drug_store_id,
                from_date : from_date,
                end_date:end_date,
                total_wage: total_wage,
                commision: commision,
                sub_total:sub_total,
                tax:tax,
                grand_total:grand_total
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response, status, xhr, $form) {

                if (response.status == true) {
                    resetModal();
                    $("#reg-date").text(response.message.registered_date);
                    $("#first-post-date").text(response.message.first_job_date)
                    $("#drug-store-name").text(response.message.drug_store_name)
                    $("#total-wage").text(response.message.total_wage)
                    $("#grand-total-hidden").val(response.message.grand_total)
                    $("#grand-total").html("<b>"+response.message.grand_total+"</b>")
                    $("#cancel-posts").html(response.message.cancel_posts)
                    $('#complete_posts').html(response.message.complete_posts)
                    $("#invoice-id").val(response.message.invoice_id)
                    $("#staticBackdrop").modal();
                } else {
                    swal.fire(
                        '',
                        response.message,
                        'error'
                    )
                }
            }
        });
    }

    function generateInvoice(drug_store_id, from_date, end_date, total_wage, commision, sub_total, tax, grand_total){

        $.ajax({
            url: generateDrugstoreInvoice,
            method: 'POST',
            data: {
                drug_store_id : drug_store_id,
                from_date : from_date,
                end_date:end_date,
                total_wage: total_wage,
                commision: commision,
                sub_total:sub_total,
                tax:tax,
                grand_total:grand_total
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response, status, xhr, $form) {
                if (response.status == true) {
                    swal.fire(
                        '完了',
                        response.message,
                        'success'
                    )
                    $("#kt_sweetalert_demo_19").click();
                } else {
                    swal.fire(
                        '',
                        response.message,
                        'error'
                    )
                }
            }
        });
    }
    function chengeDrugStorePaidStatus(drug_store_id, from_date, end_date, total_wage){
        $.ajax({
            url: changeDrugstoreInvoiceStatus,
            method: 'POST',
            data: {
                drug_store_id : drug_store_id,
                from_date : from_date,
                end_date:end_date,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response, status, xhr, $form) {
                if (response.status == true) {
                    swal.fire(
                        '完了',
                        response.message,
                        'success'
                    )
                    $("#kt_sweetalert_demo_19").click();
                } else {
                    swal.fire(
                        '',
                        response.message,
                        'error'
                    )
                }
            }
        });
    }
    "use strict";
    var KTDatatablesBasicHeaders = function() {

        var initTable1 = function() {
            var table = $('#kt_table_19');

            // begin first table
            table.DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Japanese.json"
                }
            });
        };

        return {

            //main function to initiate the module
            init: function() {
                initTable1();
            },

        };

    }();

    jQuery(document).ready(function() {
        KTDatatablesBasicHeaders.init();
    });

    function checkUncheck(e){

        if($(e).prop('checked') == true){
            $('.send-invoice').prop("checked", true);
        }else{
            $('.send-invoice').prop("checked", false);
        }
     }

     function sendInvoice(){
        var favorite = [];
        $.each($("input[name='send-invoice']:checked"), function(){
            favorite.push($(this).val());
        });
        if (favorite.length === 0) {
            swal.fire(
                '',
                'Please select an invoice to send',
                'error'
            )
        }
        else {
            $.ajax({
                url: generateAllDrugstoreInvoice,
                method: 'POST',
                data: {
                    invoices : favorite
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response, status, xhr, $form) {
                    if (response.status == true) {
                        swal.fire(
                            '完了',
                            response.message,
                            'success'
                        )
                        $("#kt_sweetalert_demo_19").click();
                    } else {
                        swal.fire(
                            '',
                            response.message,
                            'error'
                        )
                    }
                }
            });
        }
     }

     function downloadMultiple(){

        $(".se-pre-con").fadeIn("slow");
        var favorite = [];
        $.each($("input[name='send-invoice']:checked"), function(){
            favorite.push($(this).val());
        });
        if (favorite.length === 0) {
            swal.fire(
                '',
                'Please select an invoice to download',
                'error'
            )
        }
        else {
            $.ajax({
                url: downloadAllDrugstoreInvoice,
                method: 'POST',
                data: {
                    invoices : favorite
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response, status, xhr, $form) {
                    if (response.status == true) {
                        $("#kt_sweetalert_demo_19").click();
                        var paths = response.paths;
                        paths.forEach(function(item) {
                            window.open(item, '_blank');
                        });
                        $(".se-pre-con").fadeOut("slow");
                    } else {
                        swal.fire(
                            '',
                            response.message,
                            'error'
                        )
                    }
                }
            });
        }
     }

     function gethrValues(uid, id) {
         let whr = $('#whr'+uid).val()
         let wmin = $('#wmin'+uid).val()
         
    swal.fire({
        title: '本当によろしいですか？',
        text: "本当に勤務時間を更新してもよろしいですか？",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '更新',
        cancelButtonText: 'キャンセル',
        reverseButtons: true
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                url: updateWorkingHoursCompletedJobs,
                method: 'POST',
                data: {
                    id,
                    whr,
                    wmin
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response, status, xhr, $form) {
                    if (response.status == true) {
                        swal.fire(
                            '',
                            response.message,
                            'success'
                        )
                    } else {
                        swal.fire(
                            '',
                            response.message,
                            'error'
                        )
                    }
                }
            });
        } else if (result.dismiss === 'cancel') {
            swal.fire(
                'キャンセルしました',
                '',
                'error'
            )
        }
    });
        // $.ajax({
        //     url: updateWorkingHoursCompletedJobs,
        //     method: 'POST',
        //     data: {
        //         id,
        //         whr,
        //         wmin
        //     },
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     },
        //     success: function(response, status, xhr, $form) {
        //         if (response.status == true) {
        //             swal.fire(
        //                 '',
        //                 response.message,
        //                 'success'
        //             )
        //         } else {
        //             swal.fire(
        //                 '',
        //                 response.message,
        //                 'error'
        //             )
        //         }
        //     }
        // });
     }
</script>

<script>

    $(".showTable").on('click', function() {
        $('.toggleTable').fadeIn()
        $(".showTable").hide()
        $(".hideTable").show()
        $(".modal").animate({ scrollTop: $(document).height() }, "slow");
    })
    $(".hideTable").on('click', function() {
        $('.toggleTable').fadeOut()
        $('.showTable').show()
        $('.hideTable').hide()
    })
</script>

<!-- jQuery -->
<!-- <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script> -->
<!-- Timepicker Js -->
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js?ver1') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
