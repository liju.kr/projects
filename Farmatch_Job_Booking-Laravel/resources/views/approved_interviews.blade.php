@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理
                </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-businesswoman"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        {{ @$page_title }}
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬局/病院等</th>
                            <th>薬剤師</th>
                            <th>電話番号</th>
                            <th>Eメール</th>
                            <th>見学曜日</th>
                            <th>開始時間</th>
                            <th>終了時間</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                        @foreach($approvedInterviews as $key=>$row)
                            @php
                            $interview = App\ChemistInterviewTiming::where('id', $row->id)->first();
                            $chemist_applications = App\ChemistJobApplication::select('hourly_wage', 'transportation_cost')->where('chemist_id', $interview->chemist_id)->where('job_post_id', $interview->job_post_id)->first();
                            @endphp
{{--                           @if($chemist_applications)--}}
                        <tr>
{{--                            <td>{{ $loop->iteration }}</td>--}}
                            <td>{{ $i }}</td>
                            <td>{{ $row->drug_store->name }} </td>
                            <td>{{ $row->chemist->last_name." ".$row->chemist->first_name }}</td>
                            <td>{{ $row->chemist->phone }}</td>
                            <td>{{ $row->chemist->email }}</td>
                            <td>{{ $row->day }}</td>
                            <td>{{ date('H:i', strtotime($row->time_from)) }}</td>
                            <td>{{ date('H:i', strtotime($row->time_to)) }}</td>
                            <td nowrap>
                                <button type="button" class="btn btn-success kt_sweetalert_demo_30" data-id="{{ $row->id }}" data-drug-store="{{ $row->drug_store->name }}" data-chemist="{{ $row->chemist->last_name." ".$row->chemist->first_name }}">再設定</button>
                            </td>
                        </tr>
                            @php $i=$i+1; @endphp
{{--                            @endif--}}
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
<!-- Modal -->
<div class="modal fade" id="wageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">2
      <div class="modal-content" style="width: 88%;">
        <div class="modal-body">
            <h3 style="color: black;margin-bottom: 10px;" class="modal-title" id="exampleModalLongTitle">時給と交通費の設定</h3>
            <h5 style="color: black;">薬局／病院等 : <span id="drug-store"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 薬剤師 : <span id="chemist"></span></h5>
            <form class="kt-form" id="wage-form">
                <table class="table" style="width: 65%;margin-left: 18%;">
                    <tbody>
                    <tr>
                        <td style="border-top: none;width: 80px;">時給 </td>
                        <td style="border-top: none"><input class="form-control" id="hourly_wage" name="hourly_wage" type="text"></td>
                        <td style="border-top: none;vertical-align: bottom;">円</td>
                    </tr>
                    <tr>
                        <td style="border-top: none">交通費</td>
                        <td style="border-top: none"><input class="form-control" id="transportation_cost" name="transportation_cost" type="text"></td>
                        <td style="border-top: none;vertical-align: bottom;">円</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-top: none">
                            <button type="button" style="background-color: #aaa;color:white;width: 48%;" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                            <button type="button" style="width: 48%;" class="btn btn-primary" id="update-wage" data-interview-id="">設定する</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
      </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var getHourlyWage          = "{{ route('get-hourly-wage') }}";
    var updateHourlyWage       = "{{ route('update-hourly-wage') }}";
    var changeInterviewStatus  = "{{ route('change-interview-status') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
@endsection
