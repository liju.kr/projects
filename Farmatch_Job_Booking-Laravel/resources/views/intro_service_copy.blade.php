@extends('layouts.app')
@section('page-content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            {{ @$page_title }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ @$page_title }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin: Datatable -->
                    <table id="kt_table_1" class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ドラッグストア名</th>
                            <th>住所</th>
                            <th>出願日</th>
                            <th>最終編集日</th>
                            <th>サトゥス</th>
                            <th>アクション</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i=1; @endphp
                        @foreach($drugStores as $row)
                            @php
                                $employee_intro_service_checking_1 = App\EmployeeIntroService::where('drug_store_id', $row->id)->first();
                                if(date('m') >= 4)
                                    {
                                        $min_date = date('Y')."-04-01";
                                    }
                                    else
                                        {
                                            $min_date = (date('Y')-1)."-04-01";
                                        }
                            @endphp
                            @if($employee_intro_service_checking_1)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->address }}</td>
                                <td>{{ $row->employee_intro_date }}</td>
                                <td>{{ $employee_intro_service_checking_1->date }}</td>
                                <td>@if(strtotime($row->employee_intro_date) >= strtotime($min_date))<span class="kt-font-bold kt-font-success">アクティブ</span> @else <span class="kt-font-bold kt-font-danger">非活性</span> @endif</td>

                                <td nowrap>
                                       <button type="button" class="btn btn-danger kt_sweetalert_demo_17" data-id="{{ $row->id }}">拒否</button>
                                </td>
                            </tr>

                            @php $i=$i+1; @endphp
                             @endif
                        @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>

@endsection
@section('script')
    <script>
        var changeInterviewStatus  = "{{ route('change-interview-status') }}";
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
@endsection
