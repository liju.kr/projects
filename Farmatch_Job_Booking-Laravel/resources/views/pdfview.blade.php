<!DOCTYPE html>
<html lang="ja" >
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style>


      body {
      /* background: rgb(204,204,204);  */
      }

      .font-size-normal{
         font-size:10px;
      }
      .font-bold{
         font-weight:bold;
      }

      .column{
         /* max-height:650px !important; */
      }

      .vertical{
         /* -webkit-text-orientation: mixed;
         -webkit-writing-mode: vertical-rl;
         writing-mode: vertical-rl;
         white-space: nowrap;
         text-orientation: upright; */
         /* white-space: nowrap; */
      }
      p{
         margin-bottom:2px !important;
      }
      ul{
         margin-bottom:0px !important;
      }
      table{
         font-size:9px;
         border:1px solid #000 !important;
         margin-left: auto;
         margin-right: auto;
         width: 100%;
         /* table-layout:fixed; */
      }
      td,tr{
         vertical-align:middle;
         /* text-align:center; */
      }
      th{
         vertical-align:middle;
         text-align:center;
      }

      .page {
      background: white;
      display: block;
      size: auto;
      margin: 0mm !important;
      }


      .pie-chart {
            width: 600px;
            height: 400px;
            margin: 0 auto;
        }
		textarea {
		width:100%
		}
        .bd-example {
            position: relative;
			border:1px solid red;
        }
        .form-group label {
            font-size: 11px;
            color: #3c3c3c;
        }
        .bd-example input.form-control, .bd-example select.form-control {
            height:auto;
            font-size: 12px;
            margin-top: 0;
			max-width: 100%;
        }
        .form-section .form-section-column .form-label {
            display: block;
            -webkit-text-orientation: mixed;
            -webkit-writing-mode: vertical-lr;
            white-space: nowrap;
            margin-right: 5px;
            padding-right: 5px;
            width: 44px;
            align-items: center;
		}
        .form-section-column .content-box {
            border-left: solid #f8f9fa;
            border-width: 0.1rem;
            -webkit-flex: 1;
            flex: 1;
        }
        .d-flex {
            display: -webkit-box; /* wkhtmltopdf uses this one */
            display: flex;
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
			-ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
		.container {
            max-width: 1200px;
            margin-left: auto;
            margin-right: auto;
        }
        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
		.input-group {
			position: relative;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			-webkit-box-align: stretch;
			-ms-flex-align: stretch;
			align-items: stretch;
			width: 100%;
		}
		.input-group-append {
			margin-left: -1px;
		}
		.input-group-prepend, .input-group-append {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.m-0 {
		margin:0!important;
		}
		.flex-nowrap {
		flex-wrap:nowrap!important
		}
        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
            position: relative;
            padding-right: 10px;
            padding-left: 10px;
        }
		.col-sm-2 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 16.66667%;
			flex: 0 0 16.66667%;
			max-width: 16.66667%;
		}
		.col-sm-3 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 25%;
			flex: 0 0 25%;
			max-width: 25%;
		}
        .col-lg-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-lg-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.33333%;
            flex: 0 0 33.33333%;
            max-width: 33.33333%;
        }
		.col-sm-4 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 33.33333%;
			flex: 0 0 33.33333%;
			max-width: 33.33333%;
		}
		.col-sm-8 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 66.66667%;
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
        .col-lg-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.66667%;
            flex: 0 0 41.66667%;
            max-width: 41.66667%;
        }
        .col-lg-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.33333%;
            flex: 0 0 58.33333%;
            max-width: 58.33333%;
        }
        .col-sm-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.66667%;
            flex: 0 0 16.66667%;
            max-width: 16.66667%;
        }
        .col-sm-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.66667%;
            flex: 0 0 66.66667%;
            max-width: 66.66667%;
        }
		.col-sm-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
		.col-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
        .col-form-label {
            padding-top: calc(0.25rem + 1px);
            padding-bottom: calc(0.25rem + 1px);
            margin-bottom: 0;
            font-size: inherit;
            line-height: 1.5;
        }
        .flex-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }
        .dropdown-divider {
            height: 0;
            margin: 0.5rem 0;
            overflow: hidden;
            border-top: 1px solid #e9ecef;
        }
        .text-center {
            text-align: center !important;
        }
        .pt-5, .py-5 {
            padding-top: 3rem !important;
        }
        .bg-white {
            background-color: #fff;
        }
        .p-5 {
            padding: 3rem !important;
        }
        .mb-5, .my-5 {
            margin-bottom: 3rem !important;
        }
        .mt-3, .my-3 {
            margin-top: 1rem !important;
        }
        .container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
            width: 100%;
            padding-right: 10px;
            padding-left: 10px;
            margin-right: auto;
            margin-left: auto;
        }
        .align-items-center {
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
        }
        .justify-content-center {
            -webkit-box-pack: center; /* wkhtmltopdf uses this one */
            justify-content: center;
        }
        .mx-0 {
            margin-left: 0;
            margin-right: 0
        }

        .mb-0 { margin-bottom: 0 }

        .w-100 { width:100% }
		.input-section .form-control {
		max-width:100px}

      @media print {

         @page {
            margin-top: 0;
            margin-bottom: 0;
         }

         @page :footer {
            display: none !important;
         }

         @page :header {
            display: none !important;
         }

         div.page
         {
            page-break-after: always;
            page-break-inside: avoid;
         }
      }


    </style>

    </script>
</head>

<body>



      <div class="container-fluid">



         <!-- first page starts -->
         <div class="row pt-1" style="flex-wrap:nowrap;">
               <div class="col-12 col-lg-12 col-sm-12 col-md-12">
                     <h5 align="center"><b>求人票</b></h5>
                     <p class="font-bold font-size-normal">受付年月日 {{$employee_intro_service_1->date}}</p>
               </div>
         </div>


         <!-- first row starts -->
         <div class="row" >
            <!--  Column 1 start --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column" style="max-height:1000px !important;" >
                  <!--  Table 1 start --->
                  <span class="font-bold font-size-normal">１ 求人事業所</span>
                  <table class="table table-responsive table-bordered" height="200px">
                     <tr>
                        <th width="10%"><span class="vertical">事<br>務<br>所<br>名</span></th>
                        <td width="90%">
                           <p>{{$employee_intro_service_1->furigana}}</p>
                           <p>{{$employee_intro_service_1->office_name_furigana}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所<br>在<br>地</span></th>
                        <td>
                           <p>{{$employee_intro_service_1->office_location_code}}</p>
                           <p>{{$employee_intro_service_1->office_location}}</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->

                  <!--  Table 2 start --->
                  <span class="font-bold font-size-normal">2 仕事内容</span>
                  <table class="table table-responsive table-bordered" height="670px">
                     <tr>
                        <th width="10%"><span class="vertical"><span class="vertical">職<br>種</span></th>
                        <td width="90%" colspan="2">
                           <p>{{$employee_intro_service_1->occupation}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">仕<br>事<br>内<br>容</span></th>
                        <td colspan="2">
                           <p>{{$employee_intro_service_1->job_description}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th rowspan="2"><span class="vertical">雇<br>用<br>形<br>態</span></th>
                        <td colspan="2">{{$employee_intro_service_1->employment_status}}</td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <p><span style="padding-right:10%;">正社員登用</span>  {{$employee_intro_service_1->appointment_of_full_time_employee}}</p>

                              @if($employee_intro_service_1->appointment_of_full_time_employee == "実績あり")
                                 <p>{{$employee_intro_service_1->appointment_of_full_time_employee_description}}</p>
                              @endif
                        </td>
                     </tr>

                     <tr>
                        <th rowspan="2">
    <ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">派</br>遣</br>・</li>
    <li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
    <li style="display:inline-block;vertical-align: middle;">非</br>常</br>勤</li>
    </ul>


    </th>
                           <td>就業形態</td>
                           <td>{{$employee_intro_service_1->working_style}}</td>
                     </tr>
                     <tr>
                        <td>労働者派遣事業の許可番号</td>
                        <td>
                           @if($employee_intro_service_1->working_style == "派遣・請負")
                              {{$employee_intro_service_1->worker_dispatch_business_permit_number}}
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th width="5%"><span class="vertical">雇<br>用<br>期<br>間</span></th>
                        <td width="95%" colspan="2">
                           <p>{{$employee_intro_service_1->employment_period}}</p><br><br>
                           @if($employee_intro_service_1->employment_period=="雇用期間の定めあり")
                              <p>契約更新の条件</p>
                              <p>{{$employee_intro_service_1->contract_renewal_conditions}}</p>
                           @endif
                        </td>
                     </tr>

                  </table>
                  <!--  Table 2 discontinued --->
            </div>
            <!--  Column 1 ends --->


            <!--  Column 2 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column" style="max-height:1000px !important;">
                  <span class="font-bold font-size-normal">&nbsp;&nbsp;&nbsp;</span>
                  <!--  Table 2 continues --->
                  <table class="table table-responsive table-bordered" height="910px">
                     <tr>
                        <th width="10%"><span class="vertical">就<br>業<br>場<br>所</span></th>
                        <td width="90%" colspan="3">
                           <p>〒 &nbsp;&nbsp;&nbsp;&nbsp; {{$employee_intro_service_1->work_place_code}}</p>
                           <p>{{$employee_intro_service_1->work_place}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th>
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">マ</br>イ</br>カ</br>|</li>
    <li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
    <li style="display:inline-block;vertical-align: middle;">通</br>勤</li>
    </ul>
</th>
                        <td width="14%" style="padding:1px">
                           <p>{{$employee_intro_service_1->my_car_commute}}</p>
                           <p>駐車場  &nbsp;{{$employee_intro_service_1->parking}}</p>
                        </td>
                        <th>
                           <span class="vertical">転<br>勤<br>の<br>可<br>能<br>性</span>
                        </th>
                        <td>
                           <p>{{$employee_intro_service_1->possibility_of_transfer}}</p>
                           @if($employee_intro_service_1->possibility_of_transfer == "あり")
                           <p>{{$employee_intro_service_1->possibility_of_transfer_details}}</p>
                           @endif
                        </td>
                     </tr>

                     <tr>
                        <th><span class="vertical">年<br>齢</span></th>
                        <td colspan="3">
                           <p>正社員登用&nbsp;{{$employee_intro_service_1->age_promotion}}</p>
                           @if($employee_intro_service_1->age_promotion == "あり")
                              <p>年齢制限該当事由 &nbsp; {{$employee_intro_service_1->age_limit_reason}}</p>
                              @if($employee_intro_service_1->age_limit_reason == "上限あり")
                                 {{$employee_intro_service_1->age_limit_reason_details}}
                              @endif
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">学<br>歴</span></th>
                        <td  colspan="3">
                           <p>必須&nbsp;{{$employee_intro_service_1->academic_background}}</p>
                           <p>{{$employee_intro_service_1->academic_background_details}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">必</br>要</br>な</li>
<li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">経</br>験</br>等</li>
</ul>
</th>
                        <td colspan="3">
                           <p>必要な経験・知識・技術等  &nbsp; {{$employee_intro_service_1->knowledge_skills}}</p>
                           @if($employee_intro_service_1->knowledge_skills == "条件あり")
                              <p>{{$employee_intro_service_1->knowledge_skills_details}}</p>
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th >
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">必</br>要</br>な</li>
<li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">P</br>C</br>ス</br>キ</br>ル</li>
</ul>
</th>
                        <td colspan="3" colspan="3">
                           <p>{{$employee_intro_service_1->pc_skills}}</p>
                           @if($employee_intro_service_1->pc_skills == "必要")
                              <p>{{$employee_intro_service_1->pc_skills_details}}</p>
                           @endif
                        </td>
                     </tr>

                     <tr>
                        <th><span class="vertical">必<br>要<br>な<br>免<br>許<br>・<br>資<br>格</span></th>
                        <td colspan="3">
                           <p>薬剤師免許 (必須)</p>
                           <p>{{$employee_intro_service_1->pharmacist_license}}</p>
                           <p>普通自動車運転免許 &nbsp;{{$employee_intro_service_1->ordinary_car_drivers_license}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">試<br>用<br>期<br>間</span></th>
                        <td colspan="3">
                           <p>{{$employee_intro_service_1->trial_period}}</p>
                           @if($employee_intro_service_1->trial_period == "試用期間あり")
                              <p>試用期間中の労働条件</p>
                              <p>{{$employee_intro_service_1->trial_period_details}}</p>
                           @endif
                        </td>
                     </tr>


                  </table>
                  <!--  Table 2 end --->
            </div>
            <!--  Column 2 ends --->


            <!--  Column 3 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column" style="max-height:1000px !important;">
                  <span class="font-bold font-size-normal">３ 賃金・手当</span>
                  <!--  Table 3 start --->
                  <table class="table table-responsive table-bordered" height="870px">
                     <tr>
                        <th width="10%" rowspan="5">
    <ul style="padding:5px 15px;list-style-type:none;display: contents">
    <li style="display:inline-block;vertical-align: middle;"> 賃</br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>金   </li>
    </ul>
    <span class="vertical"></span></th>
                        <td colspan="2">
                           <p>月額（a+b) : {{$employee_intro_service_1->monthly_amount_a}}円
 ~ {{$employee_intro_service_1->monthly_amount_b}}円</p>

                           <p>※（固定残業代がある場合はa+b+c)</p>
                        </td>
                     </tr>
                     <tr>
                        <td width="10%" style="text-align: center;">
                           <ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">基</br>本</br>給</li>
<li  style="display:inline-block;vertical-align: middle;">(a)</li>
</ul>
                        </td>
                        <td width="90%">
                        <p>基本月給(月平均)又は時間額: &nbsp; 月平均労働日数 {{$employee_intro_service_1->average_working_days_per_month}}日</p>

                        <p>{{$employee_intro_service_1->basic_pay_a}}円&nbsp;〜&nbsp;{{$employee_intro_service_1->basic_pay_b}}円</p>
                        </td>
                     </tr>
                     <tr>
                        <td style="padding:0px;text-align: center;">
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">定</br>期</br>的</br>に</li>
    <li style="display:inline-block;vertical-align: middle;">支</br>払</br>わ</br>れ</br>る</br>手</br>当</li>
<li  style="display:inline-block;vertical-align: middle;">(b)</li>
    </ul>
                        </td>
                        <td>
                           <div class="row">
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>{{$employee_intro_service_1->fixed_amount_1}}  &nbsp;<span >手当</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_2}}   &nbsp;<span >手当</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_3}}   &nbsp;<span >手当</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_4}}   &nbsp;<span >手当</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_5}}  &nbsp; <span >手当</span></p>
                              </div>
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>{{$employee_intro_service_1->fixed_amount_1_field_1}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_2_field_1}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_3_field_1}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_4_field_1}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_5_field_1}}  &nbsp; <span >円</span></p>
                              </div>
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>{{$employee_intro_service_1->fixed_amount_1_field_2}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_2_field_2}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_3_field_2}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_4_field_2}}  &nbsp; <span >円</span></p>
                                 <p>{{$employee_intro_service_1->fixed_amount_5_field_2}}  &nbsp; <span >円</span></p>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td style="text-align: center;">

<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">固</br>定</br>残</br>業</br>代</li>
<li  style="display:inline-block;vertical-align: middle;">(c)</li>
    </ul>

                        </td>
                        <td>
                           <p>{{$employee_intro_service_1->overtime_pay}}: <span>{{$employee_intro_service_1->overtime_pay_1}}円 ~{{$employee_intro_service_1->overtime_pay_2}}円</span></p>
                           <p>固定残業代に関する特記事項</p>
                           <p>{{$employee_intro_service_1->overtime_pay_note}}</p>
                        </td>
                     </tr>
                     <tr>
                        <td  style="padding:0px;text-align: center;">
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">付</br>事</br>項</li>
    <li style="display:inline-block;vertical-align: middle;">そ</br>の</br>他</br>手</br>当</li>
<li  style="display:inline-block;vertical-align: middle;">(d)</li>
    </ul>
                        </td>
                        <td>
                           <p>{{$employee_intro_service_1->articles_with_allowance}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th >
    <ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">賃</br>金</li>
<li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">形</br>態</br>等</li>
</ul>
 </th>
                        <td colspan="3">
                        <div class="row">
                              <div class="col-2 col-lg-2 col-sm-2 col-md-2" style="padding: 0px 0px 0px 4px;">
                                 <p>月給</p>
                                 <p>その他内容</p>
                              </div>
                              <div class="col-10 col-lg-10 col-sm-10 col-md-10">
                                 <p>なし</p>
                                 <p>{{$employee_intro_service_1->other_contents}}</p>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <th >
    <ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">通</br>勤</li>
<li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">手</br>当</li>
</ul>
 </th>
                        <td colspan="3">
                           <p>実費支給 &nbsp;&nbsp;&nbsp; {{$employee_intro_service_1->actual_expense_payment}} &nbsp;&nbsp;&nbsp;月額 &nbsp;&nbsp; {{$employee_intro_service_1->actual_expense_payment_monthly}}円</p>
                        </td>
                     </tr>
                     <tr>
                        <th >
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">賃</br>金</li>
<li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">締</br>切</br>日</li>
</ul>
</th>
                        <td colspan="3">
                        <p>{{$employee_intro_service_1->wages}} &nbsp;毎月 &nbsp; {{$employee_intro_service_1->wages_details}}日</p>
                        <p>{{$employee_intro_service_1->wages_notes}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th >
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">賃</br>金</li>
<li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">支</br>払</br>い</br>日</li>
</ul>
</th>
                        <td colspan="3">
                           <p>{{$employee_intro_service_1->payment_date}} &nbsp;&nbsp;&nbsp;毎月 &nbsp;&nbsp; {{$employee_intro_service_1->payment_date_details}}日</p>
                           <p>{{$employee_intro_service_1->payment_date_notes}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th >
<ul style="padding:5px 15px;list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">昇</br>給</li>
</ul>
</th>
                        <td colspan="3">
                           <p>{{$employee_intro_service_1->salary_raise}}</p>
                           <p>{{$employee_intro_service_1->salary_raise_details}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th >
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">賞</br>与</li>
</ul>
   </th>
                        <td colspan="3">
                           <p>{{$employee_intro_service_1->bonus}}</p>
                           <p>{{$employee_intro_service_1->bonus_details}}</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 3 ends --->
         </div>
         <!-- first row ends --->
          <!-- first page ends --->



      <!-- Second page starts  -->

      <div class="row page pt-2" style="flex-wrap:nowrap; ">
               <div class="col-12 col-lg-12 col-sm-12 col-md-12" >
                     <h5 align="center"><b>求人票</b></h5>
               </div>
         </div>
         <!-- Second row starts  -->
         <div class="row">
            <!--  Column 1 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column" style="max-height:1000px !important;">
                  <span class="font-bold font-size-normal">４ 労働時間</span>
                  <!--  Table 4 start --->
                  <table class="table table-responsive table-bordered" height="500px" style="margin-bottom:0px">
                     <tr>
                        <th rowspan="2" width="10%">
                        <ul style="list-style-type:none;display: inline-table">
                        <li style="display:inline-block;vertical-align: middle;">就</br>業</br>時</br>間</li>
                        </ul>
                     </th>
                        <td width="90%">
                           <p>(1)&nbsp; &nbsp; &nbsp; &nbsp; {{$employee_intro_service_2->working_hours_1_from}} 〜 {{$employee_intro_service_2->working_hours_1_to}}</p>
                           <p>(2)&nbsp; &nbsp; &nbsp; &nbsp; {{$employee_intro_service_2->working_hours_2_from}} 〜 {{$employee_intro_service_2->working_hours_2_to}}</p>
                           <p>(3)&nbsp; &nbsp; &nbsp; &nbsp; {{$employee_intro_service_2->working_hours_3_from}} 〜 {{$employee_intro_service_2->working_hours_3_to}}</p>
                           <p>又は&nbsp; &nbsp; &nbsp; &nbsp; {{$employee_intro_service_2->working_hours_or_from}} 〜 {{$employee_intro_service_2->working_hours_or_to}}  の間の  &nbsp;&nbsp;&nbsp;{{$employee_intro_service_2->working_hours_between}}時間 </p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p>就業時間に関する特記事項</p>
                           <p>{{$employee_intro_service_2->working_hours_notes}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th rowspan="3">

    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">時</br>間</br>外</br>労</br>働</br>時</br>間</li>
</ul>
    </th>
                        <td>
<p>{{$employee_intro_service_2->overtime_work_hours}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 月平均 :{{$employee_intro_service_2->overtime_work_hours_details}} 時間 </p>
                        </td>
                     </tr>
<tr>
<td>

<p>36協定における特別条項: &nbsp;&nbsp; {{$employee_intro_service_2->special_provisions}}</p>
<p>特別な事情・期間等</p>
</td>
</tr>
                     <tr>
                        <td>
                           <p>{{$employee_intro_service_2->special_provisions_details}}</p>
                        </td>
                     </tr>


                     <tr>
                        <th>
<ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">休</br>憩</li>
<li style="display:inline-block;vertical-align: middle;">時</br>間</li>
</ul>
                        </th>
                        <td>
                           <p>{{$employee_intro_service_2->time_break}}分 &nbsp;&nbsp;&nbsp; &nbsp; 年間休日数
&nbsp;&nbsp;&nbsp; {{$employee_intro_service_2->annual_holidays}}日</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
<ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">休</br>日</br>等</li>
</ul>

                        </th>
                        <td>
                           <p>{{$employee_intro_service_2->holidays_etc}}</p>
                           <p>6ヶ月経過後の年次有給休暇日数 &nbsp;&nbsp;&nbsp;&nbsp; {{$employee_intro_service_2->annual_paid_vacation_days_after_6_months}}日</p>
                        </td>
                     </tr>

                  </table>
                  <!--  Table 4 end --->


                  <span class="font-bold font-size-normal">５ その他の労働条件等</span>
                  <!--  Table 5  start --->
                  <table class="table table-responsive table-bordered" height="400px">
                     <tr>
                        <th rowspan="2" width="10%">
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">加</br>入</li>
    <li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
    <li style="display:inline-block;vertical-align: middle;">保</br>険</li>
</ul>
  </th>
                        <td rowspan="2">
                           <p>{{in_array("雇用",explode(', ', $employee_intro_service_2->insurance_coverage))?'雇用':''}}{{in_array("労災",explode(', ', $employee_intro_service_2->insurance_coverage))?', 労災':''}}{{in_array("公災",explode(', ', $employee_intro_service_2->insurance_coverage))?', 公災':''}}{{in_array("健康",explode(', ', $employee_intro_service_2->insurance_coverage))?', 健康':''}}{{in_array("厚生",explode(', ', $employee_intro_service_2->insurance_coverage))?', 厚生':''}}{{in_array("財形",explode(', ', $employee_intro_service_2->insurance_coverage))?', 財形':''}}{{in_array("その他",explode(', ', $employee_intro_service_2->insurance_coverage))?', その他':''}}</p>
                           <p>{{in_array("その他",explode(', ', $employee_intro_service_2->insurance_coverage))? $employee_intro_service_2->insurance_coverage_details :''}}</p>
                        </td>
                        <td>
                           <p>退職金共済</p>
                        </td>
                        <td>
                           <p>退職金制度</p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p>{{$employee_intro_service_2->retirement_benefits}} </p>
                        </td>
                        <td>
                           <p>{{$employee_intro_service_2->retirement_system}} </p>
                        </td>
                     </tr>

                     <tr>
                        <th>
<ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">企業</li>
    <li style="display:inline-block;vertical-align: middle;">&nbsp;</li>
    <li style="display:inline-block;vertical-align: middle;">年金</li>
</ul>
 </th>
                        <td colspan="3">
                           <p>{{$employee_intro_service_2->annuities_corporate}}</p>
                        </td>
                     </tr>

                     <tr>
                        <td colspan="4">
                           <div class="row">
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>定年制</p>
                                 <p>{{$employee_intro_service_2->retirement_age}}</p>
                              </div>
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>再雇用制度</p>
                                 <p>{{$employee_intro_service_2->reemployment_system}}</p>
                              </div>
                              <div class="col-4 col-lg-4 col-sm-4 col-md-4">
                                 <p>勤務延長</p>
                                 <p>{{$employee_intro_service_2->duty_extension}}</p>
                              </div>
                           </div>
                        </td>
                     </tr>

                     <tr>
                        <td colspan="4">
                           <div class="row">
                              <div class="col-2 col-lg-2 col-sm-2 col-md-2" style="padding:0px 0px 0px 4px;">
                                 <p>入居可能住宅</p>
                              </div>
                              <div class="col-2 col-lg-2 col-sm-2 col-md-2" style="padding:0px 0px 0px 4px;">
                                 <p><b>単身用</b>&nbsp;{{$employee_intro_service_2->housing_for_singles}}</p>
                                 <p><b>世帯用</b>&nbsp;{{$employee_intro_service_2->housing_for_households}}</p>
                              </div>
                              <div class="col-8 col-lg-8 col-sm-8 col-md-8">
                                 <p>{{$employee_intro_service_2->housing_details}}</p>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="4">
                           <p>利用可能託児施設 &nbsp;&nbsp;単身用:&nbsp;&nbsp;{{$employee_intro_service_2->childcare_facilities}}</p>
                           <p>{{$employee_intro_service_2->childcare_facilities_details}}</p>
                        </td>
                     </tr>



                  </table>
                  <!--  Table 5 end --->
            </div>
            <!--  Column 1 ends --->


            <!--  Column 2 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column"  style="max-height:1000px !important;">
                  <span class="font-bold font-size-normal">６ 会社の情報</span>
                  <!--  Table 3 start --->
                  <table class="table table-responsive table-bordered" height="650px">
                     <tr>
                        <th width="10%" rowspan="3">
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">就</br>業</br>場</br>所</li>
</ul>
   </th>
                        <td rowspan="3">
                           <p>従業員数&nbsp;&nbsp;&nbsp;&nbsp;{{$employee_intro_service_3->number_of_employees}}人</p>
                           <p>就業場所&nbsp;&nbsp;&nbsp;&nbsp;{{$employee_intro_service_3->work_place_name}}人</p>
                           <p>うち女性&nbsp;&nbsp;&nbsp;&nbsp;{{$employee_intro_service_3->of_which_female}}人</p>
                           <p>うちパート&nbsp;&nbsp;&nbsp;&nbsp;{{$employee_intro_service_3->of_which_part}}人</p>
                        </td>
                        <td>
                           <p>設立年&nbsp;&nbsp;&nbsp;{{$employee_intro_service_3->japanese_era}} {{$employee_intro_service_3->full_name}}年</p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p>資本金 &nbsp;&nbsp;&nbsp; {{$employee_intro_service_3->capital}}</p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p>労働組合 &nbsp;&nbsp;&nbsp; {{$employee_intro_service_3->union}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
<ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">事</br>業</br>内</br>容</li>
</ul>
</th>
                        <td colspan="2">
                           <p>{{$employee_intro_service_3->business_content}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">会</br>社</br>の</li>
    <li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
    <li style="display:inline-block;vertical-align: middle;">特</br>徴</li>
</ul>
</th>
                        <td colspan="2">
                           <p>{{$employee_intro_service_3->features_of_the_company}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th style="padding: 0px;">役職／ <br> 代表者名</th>
                        <td style="width:60%">
                           <p>{{$employee_intro_service_3->position_name}}&nbsp;{{$employee_intro_service_3->representative_name}}</p>
                        </td>
                        <td style="width:40%">
                           <p>{{$employee_intro_service_3->corporate_number}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th style="padding: 0px;">就業規則</th>
                        <td colspan="2">
                           <div class="row">
                              <div class="col-6 col-lg-6 col-sm-6 col-md-6">
                                 <p><b>フルタイム</b></p>
                                 <p>{{$employee_intro_service_3->full_time}}</p>
                              </div>
                              <div class="col-6 col-lg-6 col-sm-6 col-md-6">
                                 <p><b>パートタイム</b></p>
                                 <p>{{$employee_intro_service_3->part_time}}</p>
                              </div>
                           </div>
                        </td>
                     </tr>

                     <tr>
                        <td colspan="3">
                           <p>育児休業取得実績 : {{$employee_intro_service_3->achievements_of_taking_childcare_leave}}, &nbsp;介護休業取得実績 : {{$employee_intro_service_3->nursing_care_leave_acquisition_record}}, &nbsp; 看護休暇取得実績 : {{$employee_intro_service_3->nursing_leave_acquisition_record}}</p>
                        </td>
                     </tr>

                     <tr>
                        <td colspan="3"><p> 外国人雇用実績 : {{$employee_intro_service_3->foreign_employment_record}} </p>
                        </td>
                     </tr>


                  </table>
                  <!--  Table 1 end --->

                  <table class="table table-responsive table-bordered" height="260px">
                     <tr height="10%">
                        <th>求人に関する特記事項</th>
                     </tr>
                     <tr>
                        <td>{{$employee_intro_service_3->special_notes_on_job_vacancies}}</td>
                     </tr>
                  </table>
            </div>
            <!--  Column 2 ends --->



            <!--  Column 3 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column" style="max-height:1000px !important;">
                  <span class="font-bold font-size-normal">7 選考等</span>
                  <!--  Table 3 start --->
                  <table class="table table-responsive table-bordered">
                     <tr>
                        <th width="15%">
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">採</br>用</li>
<li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">人</br>数</li>
</ul>
</th>
                        <td width="85%">
                           <div class="row">
                              <div class="col-3 col-lg-3 col-sm-3 col-md-3">
                                 {{$employee_intro_service_3->number_of_people_recruitment}}人
                              </div>
                              <div class="col-3 col-lg-3 col-sm-3 col-md-3">
                                 <p>募集理由</p>
                              </div>
                              <div class="col-6 col-lg-6 col-sm-6 col-md-6">
                                 <p>{{$employee_intro_service_3->reason_for_recruitment}}</p>
                              </div>
                           </div>

                        </td>
                     </tr>

                     <tr>
                        <th>
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">選</br>考</li>
<li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">方</br>法</li>
</ul>
</th>
                        <td>
                           <p>{{in_array("書類選考",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'書類選考':''}}{{in_array("面接",explode(', ', $employee_intro_service_3->methodology_elective_exam))?', 面接  予定 '.$employee_intro_service_3->schedule .' 回':''}}{{in_array("筆記試験",explode(', ', $employee_intro_service_3->methodology_elective_exam))?', 筆記試験':''}}{{in_array("その他",explode(', ', $employee_intro_service_3->methodology_elective_exam))?', その他':''}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">通</br> 知 </li>
<li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">結</br>果</li>
</ul>
 </th>
                        <td>
                           <p>{{in_array("即決",explode(', ', $employee_intro_service_3->result_announcement))?'即決':''}}{{in_array("書類選考結果通知",explode(', ', $employee_intro_service_3->result_announcement))?', 書類選考結果通知':''}}{{in_array("面接選考結果通知",explode(', ', $employee_intro_service_3->result_announcement))?', 面接選考結果通知':''}}{{in_array("書類到着後",explode(', ', $employee_intro_service_3->result_announcement))?', 書類到着後 '.$employee_intro_service_3->after_the_book .' 日以内':''}}{{in_array("面接後",explode(', ', $employee_intro_service_3->result_announcement))?', 面接後 '.$employee_intro_service_3->after_the_face .' 日以内':''}}{{in_array("その他",explode(', ', $employee_intro_service_3->result_announcement))?', その他':''}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th>
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">通</br>知</li>
<li style="display:inline-block;vertical-align: middle;"> &nbsp;</li>
<li style="display:inline-block;vertical-align: middle;">方</br>法</li>
</ul>
</th>
                        <td>
                           <p>{{$employee_intro_service_3->method_notification}}</p>
                        </td>
                     </tr>


                     <tr>
                        <th>
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">日</br>時</li>
</ul></th>
                        <td>
                           <div class="row">
                              <div class="col-3 col-lg-3 col-sm-3 col-md-3">
                                 <p>随時</p>
                              </div>
                              <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                 <p>{{$employee_intro_service_3->as_needed}}</p>
                              </div>
                           </div>
                        </td>
                     </tr>

                     <tr>
                        <th width="15%">
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">選</br>考</br>場</br>所</li>
</ul>
    </th>
                        <td width="85%">
                           <p>〒{{$employee_intro_service_3->selection_place_code}}</p>
                           <p>{{$employee_intro_service_3->selection_place}}</p>
                           <p>{{$employee_intro_service_3->string_1}}&nbsp; 線 &nbsp;&nbsp; {{$employee_intro_service_3->string_2}}&nbsp; 駅 &nbsp;&nbsp;徒歩 &nbsp;
 {{$employee_intro_service_3->on_foot}}分</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="15%">
    <ul style="list-style-type:none;display: inline-table">
    <li style="display:inline-block;vertical-align: middle;">担</br>当</br>者</li>
</ul>
    </th>
                        <td width="85%">
                           <p>{{$employee_intro_service_3->furigana_name}}</p>
                           <p>{{$employee_intro_service_3->name_furigana}}</p>
                           <p><b>電話番号:</b> {{$employee_intro_service_3->telephone_number}}</p>
                           <p><b>FAX</b>{{$employee_intro_service_3->fax}}</p>
                           <p><b>Eメール</b>{{$employee_intro_service_3->email}}</p>
                        </td>
                     </tr>


                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 3 ends --->




         </div>
         <!-- Second row ends  -->

      <!-- Second page ends -->











   </div>


</body>
</html>
