@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">よくある質問 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ @$page_title }}
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="kt-form" id="city-form">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>種類</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="">選択</option>
                                    <option value="Chemist">薬剤師</option>
                                    <option value="Drug Store">薬局/病院等</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>質問</label>
                                <input type="text" name="faq_id" id="faq_id" class="form-control" style="display: none;">
                                <input type="text" name="question" id="question" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>回答</label>
                                <textarea name="answer" id="answer" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="submit" id="faq-submit" class="btn btn-primary faq-submit" data-action="add-faq" data-id="">設定</button>
                                <button type="reset" class="btn btn-secondary">キャンセル</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-map-location"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        よくある質問
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>種類</th>
                            <th>質問</th>
                            <th>回答</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($faqs as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ($row->type == 'Chemist') ? "薬剤師" : "薬局/病院等" }}</td>
                            <td>{{ $row->question }}</td>
                            <td>{{ Str::limit($row->answer, 100) }}</td>
                            <td>
                                <a href="#" class="btn btn-lg btn-clean btn-icon btn-icon-md edit-faq" data-id="{{ $row->id}}" title="">
                                    <i class="la la-edit" style="font-size: 2.3rem;"></i>
						        </a>
                                <a class="btn btn-lg btn-clean btn-icon btn-icon-md delete-faq" data-id="{{ $row->id}}" title="">
                                    <i class="la la-trash" style="font-size: 2.3rem;"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var route             = '{{ route('add-faq') }}';
    var routeDelete       = '{{ route('delete-faq') }}';
    var routeEdit         = '{{ route('edit-faq') }}';
    var routeEditAction   = '{{ route('edit-faq-action') }}';
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/faq/faq.js?ver=1') }}" type="text/javascript"></script>
@endsection
