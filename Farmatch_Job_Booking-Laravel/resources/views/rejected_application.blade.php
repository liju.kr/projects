@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    薬剤師 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Approve / Reject Application </a>
                        
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-supermarket"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        応募者
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬剤師</th>
                            <th>薬局/病院等</th>
                            <th>Day</th>
                            <th>業務開始時間:</th>
                            <th>業務終了時間:</th>
                            <th>休憩時間:</th>
                            <th>交通費:</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applications as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->first_name." ".$row->last_name }}</td>
                            <td>{{ $row->drug_store_name }}</td>
                            <td>{{ $row->day }}</td>
                            <td>{{ date("h:i A", strtotime($row->time_from)) }}</td>
                            <td>{{ date("h:i A", strtotime($row->time_to)) }}</td>
                            <td>{{ $row->rest_time }} hr</td>
                            <td>{{ $row->transportation_cost }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/pending.js') }}" type="text/javascript"></script>
@endsection