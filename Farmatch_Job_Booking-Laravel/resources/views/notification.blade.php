@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    管理者宛の通知   </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">

                <!--Begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                管理者宛の通知
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-notes">
                            <div class="kt-notes__items">
                                @foreach($notificationArray as $row)
                                <div class="kt-notes__item">
                                    <div class="kt-notes__media">
                                        <span class="kt-notes__icon">
                                            <i class="{{ ($row['type'] == 'Chemist') ? 'flaticon2-avatar kt-font-brand' : 'flaticon2-shopping-cart kt-font-success' }}"></i>
                                        </span>
                                    </div>
                                    <div class="kt-notes__content">
                                        <div class="kt-notes__section">
                                            <div class="kt-notes__info">
                                                <a href="#" class="kt-notes__title">
                                                    新しい {{ ($row['type'] == 'Chemist') ? "薬剤師" : "薬局/病院等" }}が追加されました
                                                </a>
                                                <span class="kt-notes__desc">
                                                    <!-- 9:30AM 16 June, 2015 -->
                                                    {{ date('H:i Y-m-d', strtotime($row['created_at'])) }}
                                                </span>
                                            </div>
                                        </div>
                                        <span class="kt-notes__body">
                                            名前 : <span class="kt-font-info">{{ $row['name'] }}</span>
                                            電話番号 : <span class="kt-font-info">{{ $row['phone'] }}</span>
                                            Eメール : <span class="kt-font-info">{{ $row['email'] }}</span>
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <!--End::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
