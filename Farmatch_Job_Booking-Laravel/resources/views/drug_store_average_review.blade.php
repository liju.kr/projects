@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-chat"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                      薬局/病院等平均評価
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬局/病院等</th>
                            <th>Eメール</th>
                            <th>電話番号</th>
                            <th>薬局/病院等の雰囲気</th>
                            <th>事務員の対応</th>
                            <th>薬剤師の対応</th>
                            <th>コンプライアンス遵守の程度</th>
                            <th>合計</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($avgReviews as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->drug_store }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->atmosphere }}</td>
                            <td>{{ $row->clerk_adaption }}</td>
                            <td>{{ $row->pharmacist_adaption }}</td>
                            <td>{{ $row->compliance_level }}</td>
                            <td>{{ ($row->atmosphere + $row->clerk_adaption + $row->pharmacist_adaption + $row->compliance_level)/4 }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var chemistReviewDetails = "{{ route('chemist-review-details') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/reviews/reviews.js') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
