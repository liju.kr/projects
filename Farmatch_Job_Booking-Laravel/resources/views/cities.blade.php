@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    都市 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ @$page_title }}
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="kt-form" id="city-form">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>都道府県</label>
                                <select name="prefecture" id="prefecture" class="form-control" placeholder="Prefecture">
                                    <option value="">都道府県を選択する</option>
                                    @foreach($prefectures as $row)
                                    <option value="{{ $row->id }}">{{ $row->prefecture }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>都市</label>
                                <input type="text" name="city_id" id="city_id" class="form-control" aria-describedby="emailHelp" style="display: none;">
                                <input type="text" name="city" id="city" class="form-control" aria-describedby="emailHelp" placeholder="都市">
                            </div>
                            <div class="form-group">
                                <label for="staticEmail" class=""> 位置情報を入力 </label>
                                <div style="display: none">
                                    <input id="pac-input" class="controls" type="text" placeholder="位置情報を入力する" style="margin-top: 10px; width: 387px; height: 40px;">
                                </div>
                                <div id="map" style="height: 500px;width: 100%;border-radius: 4px;border-color: red;"></div>
                                <div id="infowindow-content">
                                    <span id="place-name"  class="title"></span><br>
                                    <strong></strong> <span id="place-id"></span><br>
                                    <span id="place-address"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" id="latitude" name="latitude" class="form-control" placeholder="緯度">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="longitude" name="longitude" value="" class="form-control" placeholder="経度">
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="submit" id="city-submit" class="btn btn-primary city-submit" data-action="add-city" data-id="">設定</button>
                                <button type="reset" class="btn btn-secondary">キャンセル</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-map-location"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        都市
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>都道府県</th>
                            <th>都市</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cities as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->prefecture }}</td>
                            <td>{{ $row->city }}</td>
                            <td nowrap>
                                <a href="#" class="btn btn-lg btn-clean btn-icon btn-icon-md edit-city" data-id="{{ $row->id}}" title="">
                                    <i class="la la-edit" style="font-size: 2.3rem;"></i>
						        </a>
                                <a class="btn btn-lg btn-clean btn-icon btn-icon-md delete-city" data-id="{{ $row->id}}" title="">
                                    <i class="la la-trash" style="font-size: 2.3rem;"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>

    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var route             = '{{ route('add-city') }}';
    var routeDelete       = '{{ route('delete-city') }}';
    var routeEdit         = '{{ route('edit-city') }}';
    var routeEditAction   = '{{ route('edit-city-action') }}';

</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/city/city.js') }}" type="text/javascript"></script>
<script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAwWz0oCfPS01cm74-5p2wYx2T7-sUKDT0&libraries=places&callback=initMap" async defer ></script>
@endsection
