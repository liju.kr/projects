<!DOCTYPE html>
<html lang="ja" >
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style>


      body {
      /* background: rgb(204,204,204);  */
      }
     
      .font-size-normal{
         font-size:12px;
      }
      .font-bold{
         font-weight:bold;
      }

      .column{
         /* max-height:650px !important; */
      }

      .vertical{
         /* -webkit-text-orientation: mixed;
         -webkit-writing-mode: vertical-rl;
         writing-mode: vertical-rl;
         white-space: nowrap;
         text-orientation: upright; */
         /* white-space: nowrap; */
      }
      p{
         margin-bottom:2px !important;
      }
      table{
         font-size:10px;
         border:2px solid #000 !important;
         /* height:100% !important; */
         margin-left: auto;
         margin-right: auto;
         width: 100%;
         table-layout:fixed;;
      }
      td,tr{
         vertical-align:middle;
         /* text-align:center; */
      }
      th{
         vertical-align:middle;
         text-align:center;
      }

      .page {
      background: white;
      display: block;
      size: auto;
      margin: 0mm !important;
      }
      

      .pie-chart {
            width: 600px;
            height: 400px;
            margin: 0 auto;
        }
		textarea {
		width:100%
		}
        .bd-example {
            position: relative;
			border:1px solid red;
        }
        .form-group label {
            font-size: 11px;
            color: #3c3c3c;
        }
        .bd-example input.form-control, .bd-example select.form-control {
            height:auto;
            font-size: 12px;
            margin-top: 0;
			max-width: 100%;
        }
        .form-section .form-section-column .form-label {
            display: block;
            -webkit-text-orientation: mixed;
            -webkit-writing-mode: vertical-lr;
            white-space: nowrap;
            margin-right: 5px;
            padding-right: 5px;
            width: 44px;
            align-items: center;
		}
        .form-section-column .content-box {
            border-left: solid #f8f9fa;
            border-width: 0.1rem;
            -webkit-flex: 1;
            flex: 1;
        }
        .d-flex {
            display: -webkit-box; /* wkhtmltopdf uses this one */
            display: flex;
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
			-ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
		.container {
            max-width: 1200px;
            margin-left: auto;
            margin-right: auto;
        }
        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
		.input-group {
			position: relative;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			-webkit-box-align: stretch;
			-ms-flex-align: stretch;
			align-items: stretch;
			width: 100%;
		}
		.input-group-append {
			margin-left: -1px;
		}
		.input-group-prepend, .input-group-append {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.m-0 {
		margin:0!important;
		}
		.flex-nowrap {
		flex-wrap:nowrap!important
		}
        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
            position: relative;
            padding-right: 10px;
            padding-left: 10px;
        }
		.col-sm-2 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 16.66667%;
			flex: 0 0 16.66667%;
			max-width: 16.66667%;
		}
		.col-sm-3 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 25%;
			flex: 0 0 25%;
			max-width: 25%;
		}
        .col-lg-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-lg-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.33333%;
            flex: 0 0 33.33333%;
            max-width: 33.33333%;
        }
		.col-sm-4 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 33.33333%;
			flex: 0 0 33.33333%;
			max-width: 33.33333%;
		}
		.col-sm-8 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 66.66667%;
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
        .col-lg-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.66667%;
            flex: 0 0 41.66667%;
            max-width: 41.66667%;
        }
        .col-lg-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.33333%;
            flex: 0 0 58.33333%;
            max-width: 58.33333%;
        }
        .col-sm-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.66667%;
            flex: 0 0 16.66667%;
            max-width: 16.66667%;
        }
        .col-sm-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.66667%;
            flex: 0 0 66.66667%;
            max-width: 66.66667%;
        }
		.col-sm-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
		.col-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
        .col-form-label {
            padding-top: calc(0.25rem + 1px);
            padding-bottom: calc(0.25rem + 1px);
            margin-bottom: 0;
            font-size: inherit;
            line-height: 1.5;
        }
        .flex-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }
        .dropdown-divider {
            height: 0;
            margin: 0.5rem 0;
            overflow: hidden;
            border-top: 1px solid #e9ecef;
        }
        .text-center {
            text-align: center !important;
        }
        .pt-5, .py-5 {
            padding-top: 3rem !important;
        }
        .bg-white {
            background-color: #fff;
        }
        .p-5 {
            padding: 3rem !important;
        }
        .mb-5, .my-5 {
            margin-bottom: 3rem !important;
        }
        .mt-3, .my-3 {
            margin-top: 1rem !important;
        }
        .container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
            width: 100%;
            padding-right: 10px;
            padding-left: 10px;
            margin-right: auto;
            margin-left: auto;
        }
        .align-items-center {
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
        }
        .justify-content-center {
            -webkit-box-pack: center; /* wkhtmltopdf uses this one */
            justify-content: center;
        }
        .mx-0 {
            margin-left: 0;
            margin-right: 0
        }
        
        .mb-0 { margin-bottom: 0 }
        
        .w-100 { width:100% }
		.input-section .form-control {
		max-width:100px}

      @media print {

         @page {
            margin-top: 0;
            margin-bottom: 0;
         }

         @page :footer {
            display: none !important;
         }
      
         @page :header {
            display: none !important;
         }

         div.page
         {
            page-break-after: always;
            page-break-inside: avoid;
         }
      }
         
     
    </style>
    
    </script>
</head>

<body>



      <div class="container-fluid">



         <!-- first page starts -->
         <div class="row" style="flex-wrap:nowrap">
               <div class="col-12 col-lg-12 col-sm-12 col-md-12">
                     <h5 align="center"><b>求人事業</b></h5>
                     <p class="font-bold font-size-normal">受付年月日 {{$employee_intro_service_1->date}}</p>
               </div>
         </div>

         <!-- first page ends --->
         <!-- first row starts -->
         <div class="row">
            <!--  Column 1 start --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <!--  Table 1 start --->
                  <span class="font-bold font-size-normal">１ 求人事業所</span>
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">事<br>務<br>所<br>名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>{{$employee_intro_service_1->office_name_furigana}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所<br>在<br>地</span></th>
                        <td>
                           <p>{{$employee_intro_service_1->office_location_code}}</p>
                           <p>{{$employee_intro_service_1->office_location}}</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->

                  <!--  Table 2 start --->
                  <span class="font-bold font-size-normal">2 仕事内容</span>
                  <table class="table table-bordered">
                     <tr height="5%">
                        <th width="10%"><span class="vertical"><span class="vertical">職<br>種</span></th>
                        <td width="90%" colspan="2">
                           <p>{{$employee_intro_service_1->occupation}}</p>
                        </td>
                     </tr>
                     <tr height="20%">
                        <th ><span class="vertical">仕<br>事<br>内<br>容</span></th>
                        <td colspan="2">
                           <p>{{$employee_intro_service_1->job_description}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th rowspan="2"><span class="vertical">雇<br>用<br>形<br>態</span></th>
                        <td colspan="2">{{$employee_intro_service_1->employment_status}}</td>
                     </tr>
                     <tr height="20%">
                        <td colspan="2">
                           <p><span style="padding-right:10%;">正社員登用</span>  {{$employee_intro_service_1->appointment_of_full_time_employee}}</p>
                           
                              @if($employee_intro_service_1->appointment_of_full_time_employee == "実績あり")
                                 <p>{{$employee_intro_service_1->appointment_of_full_time_employee_description}}</p>
                              @endif
                        </td>
                     </tr>

                     <tr>
                        <th rowspan="3"><span class="vertical">雇用<br>期間</span></th>
                        <tr>
                           <td>就業形態</td>
                           <td>{{$employee_intro_service_1->working_style}}</td>
                        </tr>
                     </tr>
                     <tr>
                        <td>労働者派遣事業の許可番号</td>
                        <td>
                           @if($employee_intro_service_1->working_style == "派遣・請負")
                              {{$employee_intro_service_1->worker_dispatch_business_permit_number}}
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th width="5%"><span class="vertical">雇<br>用<br>期<br>間</span></th>
                        <td width="95%" colspan="2">
                           <p>契約更新の条件</p>
                           <p>{{$employee_intro_service_1->contract_renewal_conditions}}</p>
                        </td>
                     </tr>

                  </table>
                  <!--  Table 2 discontinued --->
            </div>
            <!--  Column 1 ends --->


            <!--  Column 2 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <span class="font-bold font-size-normal">&nbsp;&nbsp;&nbsp;</span>
                  <!--  Table 2 continues --->
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">就<br>業<br>場<br>所</span></th>
                        <td width="90%" colspan="3">
                           <p>〒 &nbsp;&nbsp;&nbsp;&nbsp; {{$employee_intro_service_1->work_place_code}}</p>
                           <p>{{$employee_intro_service_1->work_place}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th  width="10%"><span class="vertical">マイカー<br>通勤</span></th>
                        <td style="width:40%;">
                           <p>{{$employee_intro_service_1->my_car_commute}}</p>
                           <p>駐車場  &nbsp;&nbsp;&nbsp;{{$employee_intro_service_1->parking}}</p>
                        </td>
                        <th style="width:10%;">
                           <span class="vertical">転<br>勤<br>の<br>可<br>能<br>性</span>
                        </th>
                        <td style="width:40%;">
                           <p>{{$employee_intro_service_1->possibility_of_transfer}}</p>
                           @if($employee_intro_service_1->possibility_of_transfer == "あり")
                           <p>{{$employee_intro_service_1->possibility_of_transfer_details}}</p>                           
                           @endif
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">年<br>齢</span></th>
                        <td width="90%" colspan="3">
                           <p>正社員登用&nbsp;&nbsp;&nbsp;{{$employee_intro_service_1->age_promotion}}</p>
                           @if($employee_intro_service_1->age_promotion == "あり")
                              <p>年齢制限該当事由 &nbsp; &nbsp; &nbsp; {{$employee_intro_service_1->age_limit_reason}}</p>
                              @if($employee_intro_service_1->age_limit_reason == "上限あり")
                                 {{$employee_intro_service_1->age_limit_reason_details}}
                              @endif
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">学<br>歴</span></th>
                        <td  colspan="3">
                           <p>必須&nbsp;&nbsp;&nbsp;{{$employee_intro_service_1->academic_background}}</p>
                           <p>{{$employee_intro_service_1->academic_background_details}}</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="5%"><span class="vertical">必要な<br> 経験等</span></th>
                        <td width="95%" colspan="3">
                           <p>必要な経験・知識・技術等  &nbsp;&nbsp;&nbsp; {{$employee_intro_service_1->knowledge_skills}}</p>
                           @if($employee_intro_service_1->knowledge_skills == "条件あり")
                              <p>{{$employee_intro_service_1->knowledge_skills_details}}</p>
                           @endif
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">必要な <br>PCスキル</span></th>
                        <td colspan="3" colspan="3">
                           <p>{{$employee_intro_service_1->pc_skills}}</p>
                           @if($employee_intro_service_1->pc_skills == "必要")
                              <p>{{$employee_intro_service_1->pc_skills_details}}</p>
                           @endif
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">必<br>要<br>な<br>免<br>許<br>・<br>資<br>格</span></th>
                        <td width="90%" colspan="3">
                           <p>薬剤師免許 (必須)</p>
                           <p>{{$employee_intro_service_1->pharmacist_license}}</p>
                           <p>普通自動車運転免許 &nbsp;&nbsp;&nbsp;{{$employee_intro_service_1->ordinary_car_drivers_license}}</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">試<br>用<br>期<br>間</span></th>
                        <td colspan="3">
                           <p>試用期間あり &nbsp;&nbsp;&nbsp;{{$employee_intro_service_1->trial_period}}</p>
                           @if($employee_intro_service_1->trial_period == "試用期間あり")
                           <p>試用期間中の労働条件</p>
                           <p>{{$employee_intro_service_1->trial_period_details}}</p>
                           @endif
                        </td>
                     </tr>

                     
                  </table>
                  <!--  Table 2 end --->
            </div>
            <!--  Column 2 ends --->

            
            <!--  Column 3 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <span class="font-bold font-size-normal">３ 賃金・手当</span>
                  <!--  Table 3 start --->
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 3 ends --->
         </div>
         <!-- first row ends --->
          <!-- first page ends --->



      <!-- Second page starts  -->
         <!-- Second row starts  -->
         <div class="row page">
            <!--  Column 1 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <span class="font-bold font-size-normal">１ 求人事業所</span>
                  <!--  Table 3 start --->
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 1 ends --->


            <!--  Column 2 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <span class="font-bold font-size-normal">１ 求人事業所</span>
                  <!--  Table 3 start --->
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 2 ends --->



            <!--  Column 3 starts --->
            <div class="col-4 col-lg-4 col-sm-4 col-md-4 column">
                  <span class="font-bold font-size-normal">１ 求人事業所</span>
                  <!--  Table 3 start --->
                  <table class="table table-bordered">
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     
                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>

                     <tr>
                        <th width="10%"><span class="vertical">事務所名</span></th>
                        <td width="90%">
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                     <tr>
                        <th ><span class="vertical">所在地</span></th>
                        <td>
                           <p>フリガナ</p>
                           <p>事務所名事務所名 事務所名</p>
                        </td>
                     </tr>
                  </table>
                  <!--  Table 1 end --->
            </div>
            <!--  Column 3 ends --->




         </div>
         <!-- Second row ends  -->

      <!-- Second page ends -->











   </div>


</body>
</html>