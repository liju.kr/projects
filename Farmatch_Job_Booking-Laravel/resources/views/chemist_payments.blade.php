@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-price-tag"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        お支払い
                    </h3>
                    <div class="kt-searchbar pt-3 mr-3 ml-5">
                        <select class="form-control text-right" name="year" id="year">
                            <option value="">年を選択</option>
                            @for($i = 2020; $i <= 2050; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="kt-searchbar pt-3 mr-1">
                        <select class="form-control text-right" name="month" id="month">
                            <option value="">月を選択</option>
                            @for($i = 1; $i <= 12; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="kt-searchbar pt-3 mr-3">
                        <button type="button" class="btn btn-success btn-elevate" id="kt_sweetalert_demo_20">
                            <i class="fa fa-arrow-right"></i>検索
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_20">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬剤師</th>
                            <th>開始日</th>
                            <th>締め日</th>
                            <th>給与額</th>
                            <th>交通費</th>
                            <th>合計</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($chemistPayments as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row['chemist'] }}</td>
                            <td>{{ date('d/m/Y', strtotime($row['from_date'])) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row['end_date'])) }}</td>
                            <td>{{ $row['total_wage'] }}</td>
                            <td>{{ $row['total_transportation_cost'] }}</td>
                            <td>{{ $row['total'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var chemistReviewDetails   = "{{ route('chemist-review-details') }}";
    var searchChemistPayment   = "{{ route('search-chemist-payment') }}";
    var generateChemistInvoice = "{{ route('generate-chemist-invoice') }}";
    function chengeChemistPaidStatus(chemist_id, from_date, end_date, total_wage, transportation_cost, consumption_tax, total){
        $.ajax({
            url: generateChemistInvoice,
            method: 'POST',
            data: {
                chemist_id : chemist_id,
                from_date : from_date,
                end_date:end_date,
                total_wage: total_wage,
                transportation_cost: transportation_cost,
                consumption_tax: consumption_tax,
                total:total
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response, status, xhr, $form) {
                if (response.status == true) {
                    swal.fire(
                        'Success',
                        response.message,
                        'success'
                    )
                    $("#kt_sweetalert_demo_20").click();
                } else {
                    swal.fire(
                        'Error',
                        response.message,
                        'error'
                    )
                }
            }
        });
    }
    "use strict";
    var KTDatatablesBasicHeaders = function() {

        var initTable1 = function() {
            var table = $('#kt_table_20');

            // begin first table
            table.DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Japanese.json"
                }
            });
        };

        return {

            //main function to initiate the module
            init: function() {
                initTable1();
            },

        };

    }();

    jQuery(document).ready(function() {
        KTDatatablesBasicHeaders.init();
    });
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js?ver1') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
