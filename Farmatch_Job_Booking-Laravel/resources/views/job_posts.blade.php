@extends('layouts.app')
@section('page-content')
<style>
    .fc-event-solid-orange{
        background : #E47F25 !important;
    }
    .fc-unthemed .fc-event .fc-title, .fc-unthemed .fc-event-dot .fc-title{
        color: #FFFFFF !important;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    募集案件 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
       <div class="row">
          <div class="col-lg-12">
             <!--begin::Portlet-->
             <div class="kt-portlet" id="kt_portlet">
                <div class="kt-portlet__head">
                   <div class="kt-portlet__head-label">
                      <span class="kt-portlet__head-icon">
                      <i class="flaticon2-website"></i>
                      </span>
                      <h3 class="kt-portlet__head-title">
                        募集案件
                      </h3>
                   </div>
                </div>
                <div class="kt-portlet__body">
                   <div id="kt_calendar"></div>
                </div>
             </div>
             <!--end::Portlet-->
          </div>
       </div>
    </div>
    <!-- end:: Content -->
 </div>
@endsection
@section('script')
<script>
    var approveApplication = "{{ route('approve-application') }}";
    var rejectApplication  = "{{ route('reject-application') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales/ja.js" type="text/javascript"></script>
<script>
    "use strict";
    var KTCalendarBasic = function() {

        return {
            //main function to initiate the module
            init: function() {
                var todayDate = moment().startOf('day');
                var YM = todayDate.format('YYYY-MM');
                var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
                var TODAY = todayDate.format('YYYY-MM-DD');
                var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

                var calendarEl = document.getElementById('kt_calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

                    isRTL: KTUtil.isRTL(),
                    header: {
                        left: 'prev,next today',
                        center: '',
                        right: 'title '
                    },

                    height: 600,
                    contentHeight: 600,
                    aspectRatio: 3, // see: https://fullcalendar.io/docs/aspectRatio

                    nowIndicator: true,
                    now: TODAY + 'T09:25:00', // just for demo

                    defaultView: 'dayGridMonth',
                    defaultDate: TODAY,
                    locale: 'ja',
                    disableDragging: true,
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    navLinks: true,
                    events: {!! $calenderArray !!},
                    lang: 'ja',
                    eventRender: function(info) {
                        var element = $(info.el);
                        if (info.event.extendedProps && info.event.extendedProps.description) {
                            if (element.hasClass('fc-day-grid-event')) {
                                element.data('content', info.event.extendedProps.description);
                                element.data('placement', 'top');
                                KTApp.initPopover(element);
                            } else if (element.hasClass('fc-time-grid-event')) {
                                element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            } else if (element.find('.fc-list-item-title').lenght !== 0) {
                                element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            }
                        }
                    }
                });
                calendar.render();
            }
        };
    }();
    jQuery(document).ready(function() {
        KTCalendarBasic.init();
    });
</script>
{{-- <script src="{{ asset('js/pages/components/calendar/basic.js') }}" type="text/javascript"></script> --}}
@endsection
