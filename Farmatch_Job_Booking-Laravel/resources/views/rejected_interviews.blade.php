@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理
                </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-businesswoman"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        {{ @$page_title }}
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬局/病院等</th>
                            <th>薬剤師</th>
                            <th>電話番号</th>
                            <th>Eメール</th>
                            <th>見学曜日</th>
                            <th>開始時間</th>
                            <th>終了時間</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                        @foreach($rejectedInterviews as $row)
                            @php
                                $interview = App\ChemistInterviewTiming::where('id', $row->id)->first();
                                $chemist_applications = App\ChemistJobApplication::select('hourly_wage', 'transportation_cost')->where('chemist_id', $interview->chemist_id)->where('job_post_id', $interview->job_post_id)->first();
                            @endphp
{{--                            @if($chemist_applications)--}}
                        <tr>
                            {{--                            <td>{{ $loop->iteration }}</td>--}}
                            <td>{{ $i }}</td>
                            <td>{{ $row->drug_store->name }}</td>
                            <td>{{ $row->chemist->last_name." ".$row->chemist->first_name }}</td>
                            <td>{{ $row->chemist->phone }}</td>
                            <td>{{ $row->chemist->email }}</td>
                            <td>{{ $row->day }}</td>
                            <td>{{ date('H:i', strtotime($row->time_from)) }}</td>
                            <td>{{ date('H:i', strtotime($row->time_to)) }}</td>
                        </tr>
                        @php $i=$i+1; @endphp
{{--                            @endif--}}
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var changeInterviewStatus  = "{{ route('change-interview-status') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
@endsection
