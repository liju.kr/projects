<!DOCTYPE html>
<html>
<head>
	<title>Farmatch | QR Code</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style>
body {
  background: rgb(204,204,204) !important;
}
.qr_logo{
	width: 70px;
	height: auto;
}
.qr_img{
	/* box-shadow: .1cm .1cm .1cm .1cm rgba(0,0,0,0.1); */
	width: 140px;
	height: 140px;
	padding: 7px;

}
p.para{
	padding: 10px 0;

}
@media print {
    html, body {
        width: 5.5cm;
        height: 9cm;
        border: 1px dashed #f2720c;
    }
}
</style>
</head>
<body>
<div class="row mt-4" style="page-break-after: auto;">
    <div class="col-md-9 page">
        <div class="page_a4">
            <div class="qr_logo mx-auto">
                <img src="{{ asset('images/logo.png') }}" class="img-fluid">
            </div>
            <p class="text-center h6 mb-2 mt-2">{{ @$name }}</p>
            <div class="qr_img mx-auto">
                {!! QrCode::size(130)->generate($drugstoreId); !!}
            </div>
            <p class="para text-center mt-2" style="font-size: 0.45em;font-weight: 400;">このバーコードを読み込んで勤務内容を <br> 確認してください</p>
        </div>
    </div>
</div>
<script>
    window.print();
    window.onafterprint = function(event) {
        history.back();
    };
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
