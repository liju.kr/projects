@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    薬剤師 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                    承認待ちの薬剤師 </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-users-1"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        薬剤師
                    </h3>
                </div>
                {{-- <div class="kt-searchbar pt-3">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Dates" id="kt_daterangepicker_1" style="padding: 15px;">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button" id="serach_by_dates">Search</button>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>姓</th>
                            <th>名</th>
                            <th>連絡先</th>
                            <th>Eメール</th>
                            <th>登録済み状況</th>
                            <th>ステータス</th>
                            <th>オンライン状況</th>
                            <th>アクション</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($chemists as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->last_name }}</td>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ date('d/m/Y',strtotime($row->created_at)) }}</td>
                            <td>2</td>
                            <td>{{ ($row->online_status == 'Online') ? 1 : 2 }}</td>
                            <td nowrap>
                                <a href="{{route('chemist-chat-admin')}}?chemist={{$row->id}}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                    <i class="la la-wechat" style="font-size: 2.3rem;"></i>
						        </a>
						        <a href="approve-chemist/{{ $row->id }}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="View Chemist">
                                    <i class="la la-eye" style="font-size: 2.3rem;"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var searchRoute = "{{ route('search-chemist') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/chemist/chemist.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
