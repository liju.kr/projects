@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    薬剤師    </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                    {{ $jobPost->job_name }} </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-supermarket"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        申請
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <h6><span style="font-weight: 700;"> 募集人数</span> : {{ $jobPost->no_of_vaccancies }}</h6>
                <h6><span style="font-weight: 700;">業務内容</span> : {{ $jobPost->content }}</h6>
                <h6><span style="font-weight: 700;">注意事項</span> : {{ $jobPost->precautions }}</h6>
                <h6><span style="font-weight: 700;">募集理由</span> : {{ $jobPost->reason }}</h6>
                <div class="row">
                    @foreach($allApplications as $row)
                    <div class="col-xl-3">
                        <!--Begin::Portlet-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <!--begin::Widget -->
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__media">
                                            <img src="{{ ($row->image) ? asset('images/chemist/'.$row->image) : asset('images/no-image.png') }}" style="width: 100px;">
                                        </div>
                                        <div class="kt-widget__info">
                                            <a href="#" class="kt-widget__username">
                                                {{ $row->chemist }}
                                            </a>
                                        </div>
                                    </div>
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">メール:</span>
                                                <a href="#" class="kt-widget__data">{{ $row->email }}</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">電話番号:</span>
                                                <a href="#" class="kt-widget__data">{{ $row->phone }}</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">業務開始時間:</span>
                                                <a href="#" class="kt-widget__data">{{ date('H:i', strtotime($row->time_from)) }}</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">業務終了時間:</span>
                                                <a href="#" class="kt-widget__data">{{ date('H:i', strtotime($row->time_to)) }}</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">休憩時間:</span>
                                                <span class="kt-widget__data">{{ $row->rest_time }} 分</span>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">交通費:</span>
                                                <span class="kt-widget__data">{{ $row->transportation_cost }}</span>
                                            </div>
                                            @if($row->status == 'Rejected')
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">理由:</span>
                                                <span class="kt-widget__data">{{ $row->reason_for_rejection }}</span>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="kt-widget__footer">
                                    <button type="button" class="btn {{ ($row->status == 'Rejected') ? 'btn-label-danger' : 'btn-label-success' }} btn-lg btn-upper">{{ $row->status_text  }}</button>
                                    </div>
                                </div>
                                <!--end::Widget -->
                            </div>
                        </div>
                        <!--End::Portlet-->
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var searchRoute = "{{ route('search-chemist') }}";
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/chemist/chemist.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection
