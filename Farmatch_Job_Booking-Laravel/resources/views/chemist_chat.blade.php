@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                </div>
                    薬剤師チャット
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="fab" id="masterfab"><span>+</span></div>
        <div id="snackbar">新着のメッセージ <a href="{{route('chemist-chat-admin')}}">再読み込み</a></div>
        <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="javascript:;" id="startNewChat">
                        <div class="modal-header">
                            <h5 class="modal-title">新しいチャット</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <select class="modal-chemist form-control" style="width: 300px;" name="chemist"></select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">チャットを開始</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">とじる</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Aside Mobile Toggle-->
            <button class="kt-app__aside-close" id="kt_chat_aside_close">
                <i class="la la-close"></i>
            </button>
            <!--End:: App Aside Mobile Toggle-->
            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit" id="kt_chat_aside">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last">
                    <div class="kt-portlet__body">
                        <div class="kt-searchbar">
                            <form action="">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="検索" aria-describedby="basic-addon1" name="search" value="{{$search}}">
                                </div>
                            </form>
                        </div>
                        <div class="kt-widget kt-widget--users kt-mt-20">
                            <div class="kt-scroll kt-scroll--pull">
                                <div class="kt-widget__items">
                                    @foreach ($chatheads as $head)
                                        @php
                                            $count = \App\AdminChemistChat::where('admin_chemist_chat_head_id',$head->id)->where('admin_read_status',0)->count();
                                        @endphp
                                        <div class="kt-widget__item" style="cursor:pointer;" onclick="loadChat('{{$head->chemist->id}}')">
                                            <span class="kt-media kt-media--circle">
                                                @php
                                                $image = asset('store/images/profile.jpg');
                                                if(isset($head->chemist->image) && $head->chemist->image){
                                                    $image = asset('images/chemist/'.$head->chemist->image);
                                                }
                                                @endphp
                                                <img src="{{$image}}" alt="image">
                                            </span>
                                            <div class="kt-widget__info">
                                                <div class="kt-widget__section">
                                                    <a href="#" class="kt-widget__username">{{$head->chemist->full_name}}</a>
                                                    <!-- <span class="kt-badge kt-badge--success kt-badge--dot"></span> -->
                                                    <span class="message-count" id="message-count-{{$head->id}}">@if($count){{$count}}@endif</span>
                                                </div>
                                                <span class="kt-widget__desc">
                                                {!! \Illuminate\Support\Str::limit($head->last_message, 65, $end='...') !!}
                                                </span>
                                            </div>
                                            <div class="kt-widget__action">
                                                <span class="kt-widget__date" data-datetime="{{strtotime($head->updated_at)}}">{{\Carbon\Carbon::createFromTimeStamp(strtotime($head->updated_at))->locale('ja')->diffForHumans()}}</span>
                                                {{-- <span class="kt-badge kt-badge--success kt-font-bold">7</span> --}}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--End:: App Aside-->
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content" style="display:none;">
                <div class="kt-chat">
                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                        <div class="kt-portlet__head">
                            <div class="kt-chat__head ">
                                <div class="kt-chat__center">
                                    <div class="kt-chat__label">
                                        <a href="#" class="kt-chat__title"></a>
                                        <span class="kt-chat__status">
                                            <span class="kt-badge kt-badge--dot kt-badge--success"></span> アクティブ
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body" style="height: calc(100vh - 450px);">
                            <div class="kt-scroll kt-scroll--pull" data-mobile-height="300" id="kt-scroll">
                                <div class="kt-chat__messages">
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-chat__input">
                                <div class="kt-chat__editor">
                                    <textarea name="chat" style="height: 50px" id="chat" placeholder="ここにメッセージを入力してください..."></textarea>
                                </div>
                                <div class="kt-chat__toolbar">
                                    <div class="kt_chat__tools">
                                    </div>
                                    <div class="kt_chat__actions">
                                        <button type="button" onclick="chatSubmit();" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">返信</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->
    </div>
    <!-- end:: Content -->
</div>
@include('layouts.fcm')
@endsection
@section('script')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script>
    $('.modal-chemist').select2({
        placeholder: "薬剤師を選択する",
        // allowClear: true,
        theme: "bootstrap",
        ajax: {
            url: '{{route("pick-a-chemist-admin")}}',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                return data
            }
        },
        language: {
            noResults: function () {
                // default: 'No results found'
                return '結果が見つかりません.'
            }
        }

    });
    $('#startNewChat').on('submit', function() {
        $(this).find('button').attr('disabled', true);
        var data = $(this).serialize();
        $.ajax({
            url: '{{route("start-new-chemist-chat-admin")}}',
            dataType: 'json',
            data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if(data.status){
                    window.location.reload();
                }
                $(this).find('button').attr('disabled', false);
            }
        });
    });

    "use strict";
    var placeholder = base_url + "/store/images/profile.jpg";
    var messageRef = {};
    var chemist_id = "";
    var chemist_image = "";
    var chemistname = "";

    function loadChat(chemist) {
        if (chemist_id == chemist) {
            return false;
        }
        chemist_id = chemist;

        $('#kt_chat_content').show();
        $('.kt-chat__messages').html('');

        $.ajax({
            url: '{{route("get-chemist-details-admin")}}',
            type: 'get',
            data: {
                chemist: chemist_id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {

                $.ajax({
                    url: '{{route("admin-mark-read-chemist")}}',
                    type: 'post',
                    data: {
                        id: chemist_id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if(data.head){
                            $(`#message-count-${data.head}`).html("");
                        }
                    }
                });

                $('.kt-chat__title').html(data.chemist.last_name + " " + data.chemist.first_name);
                chemistname = data.chemist.last_name + " " + data.chemist.first_name ;

                if (data.chemist.image) {
                    chemist_image = base_url + '/images/chemist/' + data.chemist.image
                } else {
                    chemist_image = placeholder;
                }

                messageRef = firebase.database().ref(`/admin/chemist/${chemist}`);

                //ondatachange
                messageRef.on('child_added', function(childSnapshot) {
                    var childKey = childSnapshot.key;
                    var childData = childSnapshot.val();

                    var right = '';
                    var user = '';
                    var temp_image = placeholder;

                    if (childData.user == 1) {
                        right = 'kt-chat__message--right';
                        user = 'あなた';
                        temp_image = admin_image;
                    } else if (childData.user == 0) {
                        temp_image = chemist_image;
                        user = chemistname;
                    } else if (childData.user == 2) {
                        temp_image = store_image;
                        user = storename;
                    }
                    moment.locale('ja');
                    var timestamp = moment.unix(childData.timestamp).fromNow();
                    // console.log({childData});
                    var html = `
                        <div class="kt-chat__message ${right}">
                            <div class="kt-chat__user">
                                <span class="kt-media kt-media--circle kt-media--sm">
                                <img src="${temp_image}" alt="image">
                                </span>
                                <a href="#" class="kt-chat__username">${user}</a>
                                <span class="kt-chat__datetime" data-datetime="${childData.timestamp}">${timestamp}</span>
                            </div>
                            <div class="kt-chat__text kt-bg-light-success">
                                ${childData.message.replace(/\r\n|\r|\n/g,'<br />')}
                            </div>
                        </div>
                    `;
                    $('.kt-chat__messages').append(html);
                    var d = $('#kt-scroll');
                    d.scrollTop(d.prop("scrollHeight"));
                });
            }
        });
    }

    var chemistid = '{{$chemist}}';
    if(chemistid){
        loadChat(chemistid);
    }

    setInterval(() => {
        $('.kt-widget__date').each(function(index, value) {
           var datetime = $(this).data('datetime');
           var timestamp = moment.unix(datetime).fromNow();
           $(this).html(timestamp);
        });
        $('.kt-chat__datetime').each(function(index, value) {
           var datetime = $(this).data('datetime');
           var timestamp = moment.unix(datetime).fromNow();
           $(this).html(timestamp);
        });
    }, 60000);

    function chatSubmit() {
        var chat = $('textarea[name="chat"]').val(); // .replace(/\r\n|\r|\n/g,'<br />');
        if (chat.trim()) {
            messageRef.push({
                user: 1,
                message: chat,
                timestamp: Math.round((new Date()).getTime() / 1000)
            });
            $.ajax({
                url: '{{route("save-chemistchat-admin")}}',
                type: 'post',
                data: {
                    message: chat,
                    chemist: chemist_id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('textarea[name="chat"]').val('')
                    $('textarea[name="chat"]').focus();
                }
            })
            $('textarea[name="chat"]').val('');
        }
    }

    var listner2 = firebase.database().ref(`/admin/chemist`);
    listner2.on('child_changed', function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        console.log(childData);
        showSnackMessage();
    });

    // var chat = document.getElementById("chat");
    // chat.addEventListener("keydown", function (e) {
    //     if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
    //         chatSubmit();
    //     }
    // });
</script>
@endsection
