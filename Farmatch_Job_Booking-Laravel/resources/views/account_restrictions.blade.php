@extends('layouts.app')
@section('page-content')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    アクセス制限 </h3>
                {{-- <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Change Password </a>
                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div> --}}
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Aside Mobile Toggle-->
            <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
                <i class="la la-close"></i>
            </button>
            <!--End:: App Aside Mobile Toggle-->
            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
                <!--begin:: Widgets/Applications/User/Profile1-->
                <div class="kt-portlet ">
                    <div class="kt-portlet__head  kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-y">
                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-1">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img src="{{ asset('images/profile/'.$profile->image) }}" alt="image">
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="#" class="kt-widget__username">
                                            {{ @$profile->last_name }}  {{ @$profile->first_name }}
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        </a>
                                        <span class="kt-widget__subtitle">
                                            @if($profile->role_id == 1)
                                                スーパー権限者
                                            @else
                                                管理者アカウント
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">メールアドレス</span>
                                        <a href="#" class="kt-widget__data">{{ @$profile->email }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">連絡先</span>
                                        <a href="#" class="kt-widget__data">{{ @$profile->phone }}</a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label">所在地</span>
                                        <span class="kt-widget__data">{{ @$profile->location }}</span>
                                    </div>
                                    <div class="kt-align-right">
                                        <a href="#" id="kt_sweetalert_demo_21" class="btn btn-danger">
                                            アカウント削除
                                        </a>
                                    </div>
                                </div>
                                <div class="kt-widget__items">
                                    <a href="{{ route('view-admin-details', @$profile->id) }}" class="kt-widget__item">
                                        <span class="kt-widget__section">
                                            <span class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    </g>
                                                </svg> </span>
                                            <span class="kt-widget__desc">
                                                個人情報
                                            </span>
                                        </span>
                                    </a>
                                    <a href="{{ route('account-change-password', $profile->id) }}" class="kt-widget__item">
                                        <span class="kt-widget__section">
                                            <span class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg> </span>
                                            <span class="kt-widget__desc">
                                                パスワード再設定
                                            </span>
                                        </span>
                                    </a>
                                    <a href="{{ route('account-access-restrictions', @$profile->id) }}" class="kt-widget__item kt-widget__item--active">
                                        <span class="kt-widget__section">
                                            <span class="kt-widget__icon">
                                                <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2020-11-19-154210/theme/html/demo1/dist/../src/media/svg/icons/Communication/Shield-thunder.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3"/>
                                                        <polygon fill="#000000" opacity="0.3" points="11.3333333 18 16 11.4 13.6666667 11.4 13.6666667 7 9 13.6 11.3333333 13.6"/>
                                                    </g>
                                                </svg><!--end::Svg Icon--></span></span>
                                            <span class="kt-widget__desc">
                                                アクセス制限
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
                <!--end:: Widgets/Applications/User/Profile1-->
            </div>
            <!--End:: App Aside-->
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">アクセス制限を更新する</h3>
                                </div>
                            </div>
                            <form class="kt-form kt-form--label-right" id="kt_restriction_form">
                                <div class="kt-portlet__body">
                                    <table class="table table-striped- table-hover" id="kt_table_1">
                                        <thead>
                                            <tr>
                                                <th>セクション名</th>
                                                <th>アクション内容</th>
                                                <th>割り当て状況</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ダッシュボード</td>
                                                <td>閲覧</td>
                                                <td>
                                                    <input type="text" name="id" value="{{ @$restriction->user_id}}" style="display: none;">
                                                    <label class="switch">
                                                        <input type="checkbox" name="dashboard" {{ (@$restriction->dashboard == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>薬局/病院等タブ</td>
                                                <td>承認,拒否,停職,更新</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="drug_store" {{ (@$restriction->drug_store == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>薬剤師タブ</td>
                                                <td>承認,拒否,停職,更新</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="chemist" {{ (@$restriction->chemist == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>見学タブ</td>
                                                <td>承認,拒否,時給・交通費設定</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="interview" {{ (@$restriction->interview == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>薬局/病院等 請求書情報</td>
                                                <td>編集,送信,ダウンロード</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="invoice" {{ (@$restriction->payment == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>チャットタブ</td>
                                                <td>閲覧・メッセージ送信</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="chat" {{ (@$restriction->chat == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>お知らせタブ</td>
                                                <td>プッシュ通知送信</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="notification" {{ (@$restriction->notification == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>都道府県/都市/よくある質問</td>
                                                <td>設定</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" name="settings" {{ (@$restriction->settings == 'Active') ? 'checked' : "" }}>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-3 col-xl-9">
                                            </div>
                                            <div class="col-lg-9 col-xl-3">
                                                <button type="submit" id="kt_restriction_submit" class="btn btn-brand btn-bold btn-success">保存</button>&nbsp;
                                                <button type="reset" class="btn btn-secondary">クリア</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--begin::Page Scripts(used by this page) -->
<script>
    var updateUserRestriction  = '{{ route('update-user-restriction') }}'
    var adminAccounts          = '{{ route('admin-accounts') }}'
</script>
<script src="{{ asset('js/pages/custom/accounts/account.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/components/extended/sweetalert2.js?ver5') }}" type="text/javascript"></script>
@endsection
