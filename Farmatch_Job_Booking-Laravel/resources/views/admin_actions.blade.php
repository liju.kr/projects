@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{ @$page_title }}
                </h3>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">

                <!--Begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ @$page_title }}
                            </h3>
                            <form action="{{ route('view-admin-actions') }}" method="get" id="search-form">
                                <div class="row">
                                    <div class="kt-searchbar pt-3 mr-3 ml-5">
                                        <select class="form-control text-right" name="year" id="year">
                                            <option value="">年を選択</option>
                                            @for($i = 2020; $i <= date('Y'); $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="kt-searchbar pt-3 mr-1">
                                        <select class="form-control text-right" name="month" id="month">
                                            <option value="">月を選択	</option>
                                            @for($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="kt-searchbar pt-3 mr-3">
                                        <button type="submit" class="btn btn-success btn-elevate" id="kt_sweetalert_demo_19">
                                            <i class="fa fa-arrow-right"></i>検索
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form action="{{ route('view-admin-actions') }}" method="get" id="keyword-form">
                            <div class="row mb-4" style="margin-left: 70%">
                                <div class="kt-searchbar pt-3 mr-1">
                                    <input type="text" name="keyword" class="form-control">
                                </div>
                                <div class="kt-searchbar pt-3 mr-3">
                                    <button type="submit" class="btn btn-success btn-elevate" id="kt_sweetalert_demo_19">
                                        <i class="fa fa-arrow-right"></i>検索
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="kt-notes">
                            <div class="kt-notes__items">
                                @foreach($actions as $row)
                                <div class="kt-notes__item">
                                    <div class="kt-notes__media">
                                        <span class="kt-notes__icon">
                                            <i class="{{ $row->icon }}"></i>
                                        </span>
                                    </div>
                                    <div class="kt-notes__content">
                                        <div class="kt-notes__section">
                                            <div class="kt-notes__info">
                                                <a href="#" class="kt-notes__title">
                                                    {{ $row->message }}
                                                </a>
                                                <span class="kt-notes__desc">
                                                   {{-- 9:30AM 16 June, 2015 --}}
                                                    {{ date('H:i Y-m-d', strtotime($row['created_at'])) }}
                                                </span>
                                                <span class="kt-notes__desc">
                                                    {{-- 9:30AM 16 June, 2015 --}}
                                                    管理者名 : {{ $row->user->last_name." ".$row->user->first_name }}
                                                </span>
                                            </div>
                                        </div>
                                        <span class="kt-notes__body">
                                            @if($row->receiver == 'DrugStore')
                                            名前 : <span class="kt-font-info">{{ $row->drugStore->name }}</span>
                                            電話番号 : <span class="kt-font-info">{{ $row->drugStore->phone }}</span>
                                            Eメール : <span class="kt-font-info">{{ $row->drugStore->email }}</span>
                                            @endif
                                            @if($row->receiver == 'Chemist')
                                            名前 : <span class="kt-font-info">{{ $row->chemist->last_name }} {{ $row->chemist->first_name }}</span>
                                            電話番号 : <span class="kt-font-info">{{ $row->chemist->phone }}</span>
                                            Eメール : <span class="kt-font-info">{{ $row->chemist->email }}</span>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <!--End::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var form = $('#search-form');
    form.validate({
        rules: {
            year: {
                required: true
            },
            month: {
                required: true,
            },
        },
        messages: {
            year: {
                required: ""
            },
            month: {
                required: ""
            }
        },
    });

    var form = $('#keyword-form');
    form.validate({
        rules: {
            keyword: {
                required: true
            }
        },
        messages: {
            keyword: {
                required: ""
            }
        },
    });
</script>
@endsection
