@extends('layouts.app')
@section('page-content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    基本管理 </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ @$page_title }} </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ @$page_title }}
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="kt-form" id="drug-store-form">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>薬局/病院等</label>
                                <select name="drug_store" id="drug_store" class="form-control kt-selectpicker" placeholder="drug_store" data-live-search="true">
                                    <option value="">選択</option>
                                    <option value="All">すべて選択</option>
                                    @foreach($drugStores as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>タイトル</label>
                                <input type="text" name="title" id="title" class="form-control" aria-describedby="emailHelp" placeholder="タイトル">
                            </div>
                            <div class="form-group">
                                <label>メッセージ</label>
                                <textarea type="text" name="message" id="message" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="submit" class="btn btn-primary drug-store-notification-send">送信</button>
                                <button type="reset" class="btn btn-secondary"> キャンセル</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon-map-location"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        お知らせ
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>薬局/病院等</th>
                            <th>タイトル</th>
                            <th>メッセージ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notifications as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->drug_store }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->message }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>

    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    var sendDrugStoreNotification   = '{{ route('add-drug-store-notification') }}';
    // Class definition

    var KTBootstrapSelect = function () {

        // Private functions
        var demos = function () {
            // minimum setup
            $('.kt-selectpicker').selectpicker();
        }

        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTBootstrapSelect.init();
    });
</script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/notification/notification.js') }}" type="text/javascript"></script>
<script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAwWz0oCfPS01cm74-5p2wYx2T7-sUKDT0&libraries=places&callback=initMap" async defer ></script>
@endsection
