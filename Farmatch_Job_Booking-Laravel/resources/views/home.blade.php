@extends('layouts.app')
@section('page-content')
<style>
    .amcharts-chart-div a {
        display:none !important;
    }
    .export-main{
        display: none !important;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    ダッシュボード </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Dashboard 1-->
        <!--Begin::Row-->
        <div class="row">
            <!--begin::Portlet-->
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                案件受付チャート
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_3" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                アクティブ薬局/病院等 チャート
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_5" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                アクティブ薬剤師 チャート
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_6" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                薬局/病院等評価
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_1" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                薬剤師評価
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_7" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hidden">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                合計登録数 (過去12ヶ月)
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_amcharts_2" style="height: 500px;"></div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            {{-- <div class="col-lg-12">
                <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app" style="margin-bottom: 20px;">
                    <!--Begin:: App Aside Mobile Toggle-->
                    <button class="kt-app__aside-close" id="kt_chat_aside_close">
                        <i class="la la-close"></i>
                    </button>
                    <!--End:: App Aside Mobile Toggle-->
                    <!--Begin:: App Aside-->
                    <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit" id="kt_chat_aside">
                        <!--begin::Portlet-->
                        <div class="kt-portlet kt-portlet--last">
                            <div class="kt-portlet__body">
                                <div class="kt-searchbar">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <div class="kt-widget kt-widget--users kt-mt-20">
                                    <div class="kt-scroll kt-scroll--pull ps ps--active-y" style="height: 176px; overflow: hidden;">
                                        <div class="kt-widget__items">
                                            <div class="kt-widget__item">
                                                <span class="kt-media kt-media--circle"> 
                                                    <img src="/metronic/themes/metronic/theme/default/demo1/dist/assets/media/users/300_9.jpg" alt="image">
                                                </span>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__section">
                                                        <a href="#" class="kt-widget__username">Matt Pears</a>
                                                        <span class="kt-badge kt-badge--success kt-badge--dot"></span>
                                                    </div>
                
                                                    <span class="kt-widget__desc">
                                                        Head of Development 
                                                    </span>
                                                </div>
                                                <div class="kt-widget__action">
                                                    <span class="kt-widget__date">36 Mines</span>
                                                    <span class="kt-badge kt-badge--success kt-font-bold">7</span>
                                                </div>
                                            </div>
                
                                            <div class="kt-widget__item">
                                                <span class="kt-media kt-media--circle"> 
                                                    <img src="/metronic/themes/metronic/theme/default/demo1/dist/assets/media/users/100_7.jpg" alt="image">
                                                </span>
                
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__section">
                                                        <a href="#" class="kt-widget__username">Charlie Stone</a>
                                                        <span class="kt-badge kt-badge--success kt-badge--dot"></span>
                                                    </div>
                
                                                    <span class="kt-widget__desc">
                                                        Art Director 
                                                    </span>
                                                </div>
                
                                                <div class="kt-widget__action">
                                                    <span class="kt-widget__date">5 Hours</span>
                                                    <span class="kt-badge kt-badge--success kt-font-bold">2</span>
                                                </div>
                                            </div>
                
                                            <div class="kt-widget__item">
                                                <span class="kt-media kt-media--circle"> 
                                                    <img src="/metronic/themes/metronic/theme/default/demo1/dist/assets/media/users/100_12.jpg" alt="image">
                                                </span>
                
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__section">
                                                        <a href="#" class="kt-widget__username">Jason Muller</a>
                                                        <span class="kt-badge kt-badge--success kt-badge--dot"></span>
                                                    </div>
                
                                                    <span class="kt-widget__desc">
                                                        Python Developer
                                                    </span>
                                                </div>
                
                                                <div class="kt-widget__action">
                                                    <span class="kt-widget__date">2 Days</span>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="ps__rail-x" style="left: 0px; bottom: -683px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 683px; height: 176px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 95px; height: 40px;"></div></div></div>
                                </div>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                    <!--End:: App Aside-->
                    <!--Begin:: App Content-->
                    <div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content">
                        <div class="kt-chat">
                            <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                                <div class="kt-portlet__body">
                                    <div class="kt-scroll kt-scroll--pull ps ps--active-y" data-mobile-height="300" style="height: 292px; overflow: hidden;">
                                        
                                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 292px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 87px;"></div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End:: App Content-->
                </div>
            </div> --}}
            <div class="col-xl-8 col-lg-12 order-lg-3 order-xl-1">
                <!--begin:: Widgets/Best Sellers-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                新規薬局/病院等
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
                                <div class="kt-widget5">
                                    @foreach($drugStores as $row)
                                    <div class="kt-widget5__item">
                                        <div class="kt-widget5__content">
                                            <div class="kt-widget5__pic">
                                                <img class="kt-widget7__img" src="{{ ($row->main_image) ? asset('images/drug_store/'.$row->main_image) : asset('images/no-image.png') }}" alt="">
                                            </div>
                                            <div class="kt-widget5__section">
                                                <a href="{{ ($row->status == 'Approved') ?  "suspend-drug-store/".$row->id : "approve-drug-store/".$row->id }}" class="kt-widget5__title">
                                                    {{ $row->name }}
                                                </a>
                                                <p class="kt-widget5__desc">
                                                    登録済み状況 : {{ date('d/m/Y', strtotime($row->created_at)) }}
                                                </p>
                                                <div class="kt-widget5__info">
                                                    <span>連絡先:</span>
                                                    <span class="kt-font-info">{{ $row->phone }}</span>
                                                    <span>Eメール:</span>
                                                    <span class="kt-font-info">{{ $row->email }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget5__content">
                                            <a href="{{route('chat')}}?store={{$row->id}}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                                <i class="la la-wechat" style="font-size: 3.3rem;"></i>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Best Sellers-->
            </div>
            <div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">
                <!--begin:: Widgets/New Users-->
                <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                新規薬剤師
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget4_tab1_content">
                                <div class="kt-widget4">
                                    @foreach($chemists as $row)
                                    <div class="kt-widget4__item">
                                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                                            <img src="{{ ($row->image) ? asset('images/chemist/'.$row->image) : asset('images/no-image.png') }}" alt="">
                                        </div>
                                        <div class="kt-widget4__info">
                                            <a href="{{ ($row->status == 'Approved') ?  "suspend-chemist/".$row->id : "approve-chemist/".$row->id }}" class="kt-widget4__username">
                                                {{ $row->first_name }} {{ $row->last_name }}
                                            </a>
                                            <span class="kt-font-info">
                                                {{ $row->phone }}
                                            </p>
                                        </div>
                                        <div class="kt-widget5__content">
                                            <a href="{{route('chemist-chat-admin')}}?chemist={{$row->id}}" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                                <i class="la la-wechat" style="font-size: 3rem;"></i>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/New Users-->
            </div>
        </div>
        <!--End::Dashboard 1-->
    </div>
    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script src="{{ asset('js/pages/components/charts/amcharts/charts.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/chat/chat.js') }}" type="text/javascript"></script>
<script>
    var registartion     = {!! $registrations !!};
    
    console.log({registartion});

    var acceptanceReport        = {!! $jobReport !!};
    var chemistReport           = {!! $chemistReport !!}
    var storeReport             = {!! $storeReport !!}
    var chemistReviewReport     = {!! $chemistReviewReport !!}
    var drugStoreReviewReport   = {!! $drugStoreReviewReport !!}
    "use strict";
    var KTamChartsChartsDemo4 = function() {
    var demo4 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_1", {
                    rtl: KTUtil.isRTL(),
                    type: "serial",
                    theme: "light",
                    dataProvider: drugStoreReviewReport,
                    valueAxes: [{
                        gridColor: "#FFFFFF",
                        gridAlpha: .2,
                        dashLength: 0
                    }],
                    gridAboveGraphs: !0,
                    startDuration: 1,
                    graphs: [{
                        balloonText: "[[category]]: <b>[[value]]</b>",
                        fillAlphas: .8,
                        lineAlpha: .2,
                        type: "column",
                        valueField: "visits"
                    }],
                    chartCursor: {
                        categoryBalloonEnabled: !1,
                        cursorAlpha: 0,
                        zoomable: !1
                    },
                    categoryField: "country",
                    categoryAxis: {
                        gridPosition: "start",
                        gridAlpha: 0,
                        tickPosition: "start",
                        tickLength: 20
                    },
                    export: {
                        enabled: !0
                    }
                })
    }
    return {
        // public functions
        init: function() {
            demo4();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo4.init();
});

"use strict";
    var KTamChartsChartsDemo5 = function() {
    var demo5 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_7", {
                    rtl: KTUtil.isRTL(),
                    type: "serial",
                    theme: "light",
                    dataProvider: chemistReviewReport,
                    valueAxes: [{
                        gridColor: "#FFFFFF",
                        gridAlpha: .2,
                        dashLength: 0
                    }],
                    gridAboveGraphs: !0,
                    startDuration: 1,
                    graphs: [{
                        balloonText: "[[category]]: <b>[[value]]</b>",
                        fillAlphas: .8,
                        lineAlpha: .2,
                        type: "column",
                        valueField: "visits"
                    }],
                    chartCursor: {
                        categoryBalloonEnabled: !1,
                        cursorAlpha: 0,
                        zoomable: !1
                    },
                    categoryField: "country",
                    categoryAxis: {
                        gridPosition: "start",
                        gridAlpha: 0,
                        tickPosition: "start",
                        tickLength: 20
                    },
                    export: {
                        enabled: !0
                    }
                })
    }
    return {
        // public functions
        init: function() {
            demo5();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo5.init();
});
    "use strict";
    var KTamChartsChartsDemo = function() {
    var demo2 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_2", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "addClassNames": true,
            "theme": "light",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "balloon": {
                "adjustBorderColor": false,
                "horizontalPadding": 10,
                "verticalPadding": 8,
                "color": "#ffffff"
            },

            "dataProvider": registartion,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:12px;'>[[title]]  [[country]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "fillAlphas": 1,
                "title": "薬剤師",
                "type": "column",
                "valueField": "income",
                "dashLengthField": "dashLengthColumn"
            }, {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>[[title]]  [[country]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "薬局/病院等",
                "valueField": "expenses",
                "dashLengthField": "dashLengthLine"
            }],
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            },
            "export": {
                "enabled": true
            }
        });
    }
    return {
        // public functions
        init: function() {
            demo2();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});

var registartion = {!! $registrations !!};
    "use strict";
    var KTamChartsChartsDemo1 = function() {
    var demo3 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_3", {
                    theme: "light",
                    type: "serial",
                    dataProvider: acceptanceReport,
                    startDuration: 1,
                    graphs: [{
                        balloonText: "[[category]]年合計案件申請数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2004",
                        type: "column",
                        valueField: "year2004"
                    }, {
                        balloonText: "[[category]]年合計案件承認数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2005",
                        type: "column",
                        clustered: !1,
                        columnWidth: .5,
                        valueField: "year2005"
                    }],
                    plotAreaFillAlphas: .1,
                    categoryField: "country",
                    categoryAxis: {
                        gridPosition: "start"
                    },
                    export: {
                        enabled: !0
                    }
                })
    }
    return {
        // public functions
        init: function() {
            demo3();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo1.init();
});

var registartion = {!! $registrations !!};
    "use strict";
    var KTamChartsChartsDemo2 = function() {
    var demo5 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_5", {
                    theme: "light",
                    type: "serial",
                    dataProvider: chemistReport,
                    startDuration: 1,
                    graphs: [{
                        balloonText: "[[category]]年薬局/病院等合計登録者数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2004",
                        type: "column",
                        valueField: "year2004"
                    }, {
                        balloonText: "[[category]]年合計薬局/病院等アクティブ数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2005",
                        type: "column",
                        clustered: !1,
                        columnWidth: .5,
                        valueField: "year2005"
                    }],
                    plotAreaFillAlphas: .1,
                    categoryField: "country",
                    categoryAxis: {
                        gridPosition: "start"
                    },
                    export: {
                        enabled: !0
                    }
                })
    }
    return {
        // public functions
        init: function() {
            demo5();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo2.init();
});

var registartion = {!! $registrations !!};
    "use strict";
    var KTamChartsChartsDemo3 = function() {
    var demo6 = function() {
        var chart = AmCharts.makeChart("kt_amcharts_6", {
                    theme: "light",
                    type: "serial",
                    dataProvider: storeReport,
                    startDuration: 1,
                    graphs: [{
                        balloonText: "[[category]]年薬剤師合計登録者数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2004",
                        type: "column",
                        valueField: "year2004"
                    }, {
                        balloonText: "[[category]]年以降薬剤師アクティブ数: <b>[[value]]</b>",
                        fillAlphas: .9,
                        lineAlpha: .2,
                        title: "2005",
                        type: "column",
                        clustered: !1,
                        columnWidth: .5,
                        valueField: "year2005"
                    }],
                    plotAreaFillAlphas: .1,
                    categoryField: "country",
                    categoryAxis: {
                        gridPosition: "start"
                    },
                    export: {
                        enabled: !0
                    }
                })
    }
    return {
        // public functions
        init: function() {
            demo6();
        }
    };
}();
jQuery(document).ready(function() {
    KTamChartsChartsDemo3.init();
});
</script>

@endsection