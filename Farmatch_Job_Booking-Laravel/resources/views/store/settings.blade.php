@extends('layouts.store')
@section('page-content')
<style>
.swal2-content{
    text-align: left;
}
.fc-event-solid-gray{
    background-color: #8080809e !important;
}
.tooltip-inner {
    max-width: 500px !important;
}
.fc-event-primary{
    background-color: #E37A1E !important;
}
.fc-event-secondary{
    background-color: #43A885 !important;
}
.fc-event-success{
    background-color: #43A885 !important;
}
.fc-unthemed .fc-event .fc-title, .fc-unthemed .fc-event-dot .fc-title{
    color: #ffff !important;
}
.fc-event-solid-gray .fc-content:before {
    background: #afb0b2 !important;
}
.fc-event-success .fc-content:before {
    background: #ffff !important;
}
.square-orange {
  height: 22px;
  width: 22px;
  background-color: #E37A1E;
}
.square-green {
  height: 22px;
  width: 22px;
  background-color: #43A885;
}
.circle {
    background: #ffff;
    border-radius: 100%;
    height: 9px;
    width: 9px;
    position: relative;
    top: 7px;
    left: 7px;
}
</style>
@php
$service_check = App\DrugStore::first();
@endphp
@if($service_check->employee_intro_service==1 && $service_check->employee_intro_renewed==0)


<script type="text/javascript">
    $(window).on('load', function() {
        $('#postModal').modal('show');
        // alert("helllo");
    });
</script>

      <!-- Start Modal -->
      <div class="modal fade bd-example-modal-lg" id="postModal" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" data-dismiss="modal" class="close-header">
                        <svg class="icon">
                            <use xlink:href="#check-cross"> </use>
                        </svg>
                    </button>
                    <h3 class="modal-title" id="addeventLabel"> 常勤やパート紹介希望の利用期間が終了しました。 本日から来年3月31日までの利用を更新しますか？  </h3>
                    <form id="post-form" class="mt-4">

                        <div class="form-group row ">
                            <div class="col-sm-6">
                                <a href="/drug-store/intro-update/{{$service_check->id}}"><button class="btn btn-primary-default btn-block mr10" type="button"> 更新する </button></a>
                            </div>
                            <div class="col-sm-6">
                            <a href="/drug-store/intro-inactive/{{$service_check->id}}"><button class="btn btn-secondary-default btn-block" type="button"> しない </button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

@endif

<div class="container container-wide">
   <div class="row">
      <div class="col-12 col-md-12 col-lg-3">
         <div class="nav flex-column nav-pills s-left-nav" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link d-flex align-items-center active" id="v-pills-calendar-tab" data-toggle="pill" href="#v-pills-calendar" role="tab" aria-controls="v-pills-calendar" aria-selected="false">
            <svg width="22" height="22" class="mr-3" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15 4V2M15 4V6M15 4H10.5M3 10V19C3 20.1046 3.89543 21 5 21H19C20.1046 21 21 20.1046 21 19V10H3Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M3 10V6C3 4.89543 3.89543 4 5 4H7" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M7 2V6" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M21 10V6C21 4.89543 20.1046 4 19 4H18.5" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>

                カレンダー
            </a>
            {{-- <a class="nav-link" id="v-pills-inspections-tab" data-toggle="pill" href="#v-pills-inspections" role="tab" aria-controls="v-pills-inspections" aria-selected="false"> 申請中の⾒学 </a> --}}
            <a class="nav-link d-flex align-items-center" id="v-pills-job-applications-tab" data-toggle="pill" href="#v-pills-job-applications" role="tab" aria-controls="v-pills-job-applications" aria-selected="false">
            <svg width="22" height="22" class="mr-3" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9 2L15 2" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M12 10L12 14" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M12 22C16.4183 22 20 18.4183 20 14C20 9.58172 16.4183 6 12 6C7.58172 6 4 9.58172 4 14C4 18.4183 7.58172 22 12 22Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>

                承認待ちの申請
            </a>
            <a class="nav-link d-flex align-items-center" id="v-pills-accepted-applications-tab" data-toggle="pill" href="#v-pills-accepted-application" role="tab" aria-controls="v-pills-accepted-application" aria-selected="false">
            <svg width="22" height="22" class="mr-3" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17 10H20M23 10H20M20 10V7M20 10V13" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M1 20V19C1 15.134 4.13401 12 8 12V12C11.866 12 15 15.134 15 19V20" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M8 12C10.2091 12 12 10.2091 12 8C12 5.79086 10.2091 4 8 4C5.79086 4 4 5.79086 4 8C4 10.2091 5.79086 12 8 12Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>

                承認済み薬剤師の詳細
            </a>
            <a class="nav-link d-flex align-items-center" id="v-pills-cancelled-application-tab" data-toggle="pill" href="#v-pills-cancelled-application" role="tab" aria-controls="v-pills-cancelled-application" aria-selected="false">
            <svg width="22" height="22" class="mr-3" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
            <path d="M18.6213 12.1213L20.7426 10M22.864 7.87868L20.7426 10M20.7426 10L18.6213 7.87868M20.7426 10L22.864 12.1213" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M1 20V19C1 15.134 4.13401 12 8 12V12C11.866 12 15 15.134 15 19V20" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M8 12C10.2091 12 12 10.2091 12 8C12 5.79086 10.2091 4 8 4C5.79086 4 4 5.79086 4 8C4 10.2091 5.79086 12 8 12Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            </g>
            <defs>
            <clipPath id="clip0">
            <rect width="24" height="24" stroke-width="1.5" fill="white"/>
            </clipPath>
            </defs>
            </svg>

                キャンセルされた申請
            </a>
             <a class="nav-link d-flex align-items-center" id="v-pills-pending-interviews-tab" data-toggle="pill" href="#v-pills-pending-interviews" role="tab" aria-controls="v-pills-pending-interviews" aria-selected="false">
             <svg xmlns="http://www.w3.org/2000/svg" class="mr-3" width="22" height="22" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
                 申請中の見学
             </a>
            <a class="nav-link d-flex align-items-center" id="v-pills-payment-info-tab" data-toggle="pill" href="#v-pills-payment-info" role="tab" aria-controls="v-pills-payment-info" aria-selected="false">
            <svg width="20" height="20" class="mr-3" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6 6L18 6" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M6 10H18" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M12 14L18 14" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M12 18L18 18" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M2 21.4V2.6C2 2.26863 2.26863 2 2.6 2H21.4C21.7314 2 22 2.26863 22 2.6V21.4C22 21.7314 21.7314 22 21.4 22H2.6C2.26863 22 2 21.7314 2 21.4Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M6 18V14H8V18H6Z" fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>

                お支払い情報
            </a>
            {{-- <a class="nav-link d-flex align-items-center" id="v-pills-interview-timing-tab" data-toggle="pill" href="#v-pills-interview-timing" role="tab" aria-controls="v-pills-interview-timing" aria-selected="false"> 面談適時の選択 </a> --}}
         </div>
      </div>
      <div class="col-12 col-md-12 col-lg-9">
         <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane show active" id="v-pills-calendar" role="tabpanel" aria-labelledby="v-pills-calendar-tab">
               <div class="tab__container">

                   <div class="kt-portlet__head-toolbar mb20 text-right">
                       @if(Auth::user()->normal_farmatch==1)
                       <a href="#" class="btn btn-primary-default" data-toggle="modal" data-target="#addevent">
                           新規投稿
                       </a>
                       @else
                           <a href="#" class="btn btn-primary-default" data-toggle="modal" data-target="#normalFarmatchModal">
                               新規投稿
                           </a>
                       @endif
                   </div>

                   <!-- Modal -->
                   <div class="modal fade bd-example-modal-lg" id="addevent" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
                       <div class="modal-dialog" role="document">
                           <div class="modal-content">
                               <div class="modal-body">
                                   <button type="button" data-dismiss="modal" class="close-header">
                                       <svg class="icon">
                                           <use xlink:href="#check-cross"> </use>
                                       </svg>
                                   </button>
                                   <h3 class="modal-title" id="addeventLabel"> 新規投稿  </h3>
                                   <form id="post-form">
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 募集タイトル </label>
                                           <div class="col-sm-9">
                                               <input type="text" class="form-control" id="job_name" name="job_name">
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 日付 </label>
                                           <div class="col-sm-9">
                                               <input type="text" class="form-control" id="kt_datepicker_1" name="dates" readonly placeholder="日付を選択" />
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 業務開始時間</label>
                                           <div class="col-sm-9">
                                               <input type="text" class="form-control" id="kt_timepicker_1" name="work_from" readonly>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label">業務終了時間</label>
                                           <div class="col-sm-9">
                                               <input type="text" class="form-control" id="kt_timepicker_1_modal" name="work_to" readonly>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 募集人数 </label>
                                           <div class="col-sm-9">
                                               <input type="number" class="form-control" id="no_of_vaccancies" name="no_of_vaccancies">
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label">業務内容</label>
                                           <div class="col-sm-9">
                                               <div class="kt-checkbox-inline">
                                                   <label class="kt-checkbox style--rounded kt-checkbox--solid kt-checkbox--success">
                                                       <input type="checkbox" name="content[]" value="調剤・監査" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">調剤・監査</font></font><span class="style-rounded"></span>
                                                   </label>

                                                   <label class="kt-checkbox style--rounded kt-checkbox--solid kt-checkbox--success">
                                                       <input type="checkbox" name="content[]" value="投薬"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">投薬</font></font><span class="style-rounded"></span>
                                                   </label>
                                                   <label class="kt-checkbox style--rounded kt-checkbox--solid kt-checkbox--success">
                                                       <input type="checkbox" name="content[]" value="病棟業務"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">病棟業務</font></font><span class="style-rounded"></span>
                                                   </label>
                                                   <label class="kt-checkbox style--rounded kt-checkbox--solid kt-checkbox--success">
                                                       <input type="checkbox" name="content[]" id="business-content" value="その他"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">その他</font></font><span class="style-rounded"></span>
                                                   </label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group row" id="other_content" style="display: none">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> その他 </label>
                                           <div class="col-sm-9">
                                               <textarea class="form-control" name="content_others_1" id="content_others" name="content_others"></textarea>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 注意事項 </label>
                                           <div class="col-sm-9">
                                               <textarea class="form-control" id="precautions" name="precautions"></textarea>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> 勤務時間調節 </label>
                                           <div class="col-sm-9">
                                               <select class="form-control" id="is_editable" name="is_editable">
                                                   <option value="Editable"> 希望時間に対応可能 </option>
                                                   <option value="Not Editable"> 希望時間に対応不可 </option>
                                               </select>
                                           </div>
                                       </div>
                                       <div class="form-group row">
                                           <label for="exampleSelect1" class="col-sm-3 col-form-label" >理由</label>
                                           <div class="col-sm-9">
                                               <select class="form-control" id="reason" name="reason">
                                                   <option value="">オプションを選択</option>
                                                   <option value="Lack of Workers "> 人手不足 </option>
                                                   <option value="Busy Time"> 繁忙期 </option>
                                                   <option value="Absence of Chemist"> 職員の休暇 </option>
                                                   <option value="Others"> その他 </option>
                                               </select>
                                           </div>
                                       </div>
                                       <div class="form-group row" style="display: none;" id="reson_group">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"> その他 </label>
                                           <div class="col-sm-9">
                                               <textarea class="form-control" id="others" name="others"></textarea>
                                           </div>
                                       </div>
                                       <div class="form-group row ">
                                           <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                                           <div class="col-sm-9">
                                               <button class="btn btn-primary-default mr10" type="submit"> 投稿 </button>
                                               <button class="btn btn-secondary-default " type="reset" onclick="resetForm()"> クリア </button>
                                           </div>
                                       </div>
                                   </form>
                               </div>
                           </div>
                       </div>
                   </div>

                  <!--begin::Portlet-->
                  <div class="kt-portlet" id="kt_portlet">
                     <div class="kt-portlet__body">
                        <div id="kt_calendar"></div>
                        <h1 class="mt-3"></h1>
                        <div class="row">
                            <div class="square-orange ml-3"></div><span class="mt-1 ml-2" >確定済みの案件</span>
                            <div class="square-green ml-3"></div><span class="mt-1 ml-2" >募集中の案件</span>
                            <div class="square-green ml-3"><div class="circle"></div></div><span class="mt-1 ml-2" >評価記載待ち</span>
                        </div>
                     </div>
                  </div>
                  <!--end::Portlet-->
               </div>
            </div>
            <div class="tab-pane" id="v-pills-inspections" role="tabpanel" aria-labelledby="v-pills-inspections-tab">
                <div class="tab__container">
                   <h5 class="mb25"> 申請中の⾒学 </h5>
                   <div class="row">
                      @if($pendingInspections->count() > 0)
                      @foreach($pendingInspections as $row)
                        <div class="col-md-6" id="inspection-{{ $row->id }}">
                         <div class="card card1">
                            <div class="media">
                               <img class="align-self-start link-curser image-popup" data-id="{{ $row->chemist_id }}" src="{{ ($row->chemist->image) ? asset('images/chemist/'.$row->chemist->image) : asset('store/images/profile.jpg') }}" alt="" >
                               <div class="media-body">
                                  <p> {{ $row->chemist->last_name }} {{ $row->chemist->first_name }}</p>
                                  <p class="sm"> 性別 : <b> {{ ($row->chemist->gender == 'Male') ? '男性' : '女性' }} </b></p>
                                  <p class="sm"> 希望面談曜日 : <b> {{ $row->day }} </b></p>
                                  <p class="sm"> 希望面談時間 : <b> {{ date('H:i', strtotime($row->time_from)) }} - {{ date('H:i', strtotime($row->time_to)) }}</b></p>
                                  <div class="btn-cover">
                                     <a href="#" class="cancel-inspection" data-id="{{ $row->id }}">
                                        <div class="btn btn-secondary-default btn-round btn-sm">
                                            <svg class="icon">
                                                <use xlink:href="#check-cross"> </use>
                                            </svg>
                                           削除
                                        </div>
                                     </a>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      @endforeach
                      @else
                         <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                      @endif
                   </div>
                </div>
             </div>
            <div class="tab-pane" id="v-pills-pending-interviews" role="tabpanel" aria-labelledby="v-pills-pending-interviews-tab">
               <div class="tab__container">
                  <h5 class="mb25"> 申請中の見学 </h5>
                  <div class="row">
                     @if($pendingInterviews->count() > 0)
                     @foreach($pendingInterviews as $row)
                     <div class="col-md-6">
                        <div class="card card1">
                           <div class="media">
                              <img class="align-self-start link-curser image-popup" data-id="{{ $row->chemist_id }}" src="{{ ($row->chemist->image) ? asset('images/chemist/'.$row->chemist->image) : asset('store/images/profile.jpg') }}" alt="" >
                              <div class="media-body">
                                 <p> {{ $row->chemist->last_name }} {{ $row->chemist->first_name }}</p>
                                 <p class="sm"> 性別 : <b> {{ ($row->chemist->gender == 'Male') ? '男性' : '女性' }} </b></p>
                                 <p class="sm"> 希望面談曜日 : <b> {{ $row->day }} </b></p>
                                 <p class="sm"> 希望面談時間  : <b> {{ date('H:i', strtotime($row->time_from)) }} - {{ date('H:i', strtotime($row->time_to)) }}</b></p>
                                 <div class="btn-cover">
                                    <a href="{{route('chemist-chat')}}?chemist={{ $row->chemist_id }}">
                                       <div class="btn btn-secondary-default btn-round btn-sm">
                                          <svg class="icon">
                                             <use xlink:href="#chat-icon"> </use>
                                          </svg>
                                          チャット
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @else
                        <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                     @endif
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="v-pills-job-applications" role="tabpanel" aria-labelledby="v-pills-job-applications-tab">
                <div class="tab__container">
                    <h5 class="mb25"> 承認待ちの案件  <div class="mb25 text-right" style="margin-top: -18px"> 承認待ち数 {{ count(json_decode($pendingApplications)) }} </div></h5>
                    <!--begin::Portlet-->
                    <div class="kt-portlet" id="kt_portlet_pending">
                      <div class="kt-portlet__body">
                         <div id="kt_calendar_pending"></div>
                      </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <div class="tab-pane" id="v-pills-accepted-application" role="tabpanel" aria-labelledby="v-pills-accepted-applications-tab">
               <div class="tab__container">
                  <h5 class="mb25"> 承認済み薬剤師の詳細 </h5>
                  <div class="row">
                     @if($acceptedApplications->count() > 0)
                     @foreach($acceptedApplications as $row)
                     <div class="col-md-6">
                        <div class="card card1">
                           <div class="media">
                              <img class="align-self-start image-popup" data-id="{{ $row->chemist_id }}" src="{{ ($row->image) ? asset('images/chemist/'.$row->image) : asset('store/images/profile.jpg') }}" alt="">
                              <div class="media-body">
                                 <p> {{ $row->last_name }} {{ $row->first_name }}</p>
                                 <p class="sm split-xl"> <span> 募集タイトル : </span> <b>{{ $row->job_name }}</b> </p>
                                 <p class="sm split-xl"> <span> 性別  : </span> <b>{{ ($row->gender == 'Male') ? "男性" : "女性" }}</b> </p>
                                 <p class="sm split-xl"> <span> 勤務希望日 :</span> <b>{{ $row->date }} </b></p>
                                 <p class="sm split-xl"> <span> 希望勤務時間 : </span> <b> {{ date("H:i",strtotime($row->time_from)) }}  - {{ date("H:i",strtotime($row->time_to)) }}  </b></p>
                                 <p class="sm split-xl"> <span> 休憩時間  : </span> <b> {{ $row->rest_time }} 分 </b></p>
                                 {{-- <p class="sm split-xl"> <span> 時給 :</span> <b> {{ $row->hourly_wage }}  円</b></p>
                                 <p class="sm split-xl"> <span> 交通費 : </span> <b> {{ $row->transportation_cost }} 円</b></p> --}}
                                 <p class="sm split-xl"> <span> ステータス : </span> <b  @if($row->actual_status=="Accepted") style="color: green" @elseif($row->actual_status=="Chemist Accept") style="color: orange" @endif> {{ $row->status }}</b></p>
                                 <div class="btn-cover">
                                    <a href="{{route('chemist-chat')}}?chemist={{ $row->chemist_id }}">
                                       <div class="btn btn-secondary-default btn-round btn-sm">
                                          <svg class="icon">
                                             <use xlink:href="#chat-icon"> </use>
                                          </svg>
                                          チャット
                                       </div>
                                    </a>
                                    <a href="#" >
                                       <div class="btn btn-primary btn-round btn-sm cancel-application" data-id="{{ $row->id }}">
                                          <svg class="icon">
                                             <use xlink:href="#check-cross"> </use>
                                          </svg>
                                          キャンセル
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @else
                        <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                     @endif
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="v-pills-cancelled-application" role="tabpanel" aria-labelledby="v-pills-cancelled-application-tab">
               <div class="tab__container">
                  <div class="d-flex justify-content-between">
                     <h5 class="mb25"> キャンセルされた申請 </h5>
                     <div>
                        <a href="#" class="cancel-all-applications">
                           <div class="btn btn-secondary-default btn-round btn-sm mb20">
                              <svg class="icon">
                                 <use xlink:href="#check-cross"> </use>
                              </svg>
                              すべて削除
                           </div>
                        </a>
                     </div>
                  </div>
                  <div class="row">
                     @if($cancelledApplications->count() > 0)
                     @foreach($cancelledApplications as $row)
                     <div class="col-md-6">
                        <div class="card card1" style="background-color: #80808014;" id="{{ $row->id }}">
                           <div class="media">
                              <img class="align-self-start image-popup" data-id="{{ $row->chemist_id }}" src="{{ ($row->image) ? asset('images/chemist/'.$row->image) : asset('store/images/profile.jpg') }}" alt="">
                              <div class="media-body">
                                 <p> {{ $row->last_name }} {{ $row->first_name }}  </p>
                                 <p class="sm split-xl"> <span> 募集タイトル : </span> <b>{{ $row->job_name }}</b> </p>
                                 <p class="sm split-xl"> <span>  性別  : </span> <b>{{ ($row->gender == 'Male') ? "男性" : "女性" }}</b> </p>
                                 <p class="sm split-xl"> <span>  勤務日 : </span> <b> {{ $row->date }} </b></p>
                                 <p class="sm split-xl"> <span>  勤務時間 :</span> <b> {{ date("H:i", strtotime($row->time_from))}} - {{ date("H:i", strtotime($row->time_to))}} </b></p>
                                 <p class="sm split-xl"> <span>  休憩時間 : </span> <b> {{ $row->rest_time }} 分 </b></p>
                                 {{-- <p class="sm split-xl"> <span>  時給 :</span> <b> {{ $row->hourly_wage }}  円</b></p>
                                 <p class="sm split-xl"> <span>  交通費 : </span> <b> {{ $row->transportation_cost }}  円</b></p> --}}
                                 <div class="btn-cover">
                                    <a href="{{route('chemist-chat')}}?chemist={{$row->chemist_id}}">
                                       <div class="btn btn-secondary-default btn-round btn-sm">
                                          <svg class="icon">
                                             <use xlink:href="#chat-icon"> </use>
                                          </svg>
                                          チャット
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @else
                        <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                     @endif
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="v-pills-payment-info" role="tabpanel" aria-labelledby="v-pills-payment-info-tab">
               <div class="tab__container">
                  <h5 class="mb25"> お支払い情報 </h5>
                  <div class="row">
                     @if($paymentInformations->count() > 0)
                     @foreach($paymentInformations as $row)
                     <div class="col-md-6">
                        <div class="card card1">
                           <div class="media">
                              <div class="media-body">
                                 <p class="sm split-sm"> <span> 請求書 No. : </span> <b> {{ $row->id }} </b></p>
                                 <div class="row">
                                    <div class="col">
                                       <p class="sm split-sm"> <span> 期間 : </span> <b>{{ date('Y-m-d', strtotime($row->from_date)) }} ~ {{ date('Y-m-d', strtotime($row->to_date)) }} </b></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p class="sm split-sm"> <span> お振込期限 : </span> <b> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->to_date)->addMonths(1)->format('Y-m')."-10" }}</b></p>
                                     </div>
                                </div>
                                <p class="sm split-sm"> <span> 金額 : </span> <b class="large"> ¥ {{ round((($row->total_price + $row->total_input) + $row->reduce_amount)) }}</b></p>
                                 <div class="btn-cover row no-gutters justify-content-between">
                                    <div class="btn {{ ($row->status == 'Hold') ? 'btn-pending' : 'btn-paid' }} btn-tag mr3">
                                       {{ ($row->status == 'Hold') ? '待機中' : '支払済' }}
                                    </div>
                                    <a href="{{route('download-invoice', $row->id)}}" target="_blank" class="btn btn-dark btn-tag " style="  cursor: pointer;">
                                       <div class="">
                                          <svg class="icon">
                                             <use xlink:href="#download"> </use>
                                          </svg>
                                          請求書ダウンロード
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @else
                        <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                     @endif
                  </div>
               </div>
            </div>
            <div class="tab-pane" id="v-pills-interview-timing" role="tabpanel"
               aria-labelledby="v-pills-interview-timing-tab">
               <div class="tab__container">
                    <h5 class="mb25"> 面談適時の選択 </h5>
                    <form id="interview-timing">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label">  面談可能曜日 </label>
                            <div class="col-sm-9">
                            <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Monday"> 月曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Tuesday"> 火曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Wednesday"> 水曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Thursday"> 木曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Friday"> 金曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Saturday"> 土曜日
                                <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                <input type="checkbox" name="days[]" value="Sunday"> 日曜日
                                <span></span>
                                </label>
                            </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label"> 開始時間 </label>
                            <div class="col-sm-5">
                            <input class="form-control" id="kt_timepicker_1" name="time_from" readonly placeholder="Select time" type="text" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label"> 終了時間</label>
                            <div class="col-sm-5">
                            <input class="form-control" id="kt_timepicker_1_modal" name="time_to" readonly placeholder="Select time" type="text" />
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                            <button class="btn btn-primary-default " type="submit">{{ count($drugStoreTiming) ? "保存" : "保存" }}</button>
                            </div>
                        </div>
                    </form>
                    <hr class="mb50 mt50">
                    <div class="table-responsive interview-timings">
                    </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- -------------- -->
<div class="modal fade bd-example-modal-lg" id="image-pop" tabindex="-1" role="dialog" aria-labelledby="image-popLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <button type="button" data-dismiss="modal" class="close-header">
               <svg class="icon">
                  <use xlink:href="#check-cross"> </use>
               </svg>
            </button>
            <!-- <h3 class="modal-title" id="image-popLabel"> New Job </h3> -->
            <div class="media mb30">
               <img class="br9" id="popup_image" src="{{ asset('store/images/profile.jpg') }}" alt="" data-toggle="modal" data-target="#image-pop">
               <div class="media-body">
                  <ul class="user-listing normal small_sec">
                     <li> 名前 : <span id="popup_name"> たなか みこ </span></li>
                     <li> 性別 : <span id="popup_gender"> 女性 </span></li>
                     <li> 年齢 : <span id="popup_age"> 30代 </span></li>
                  </ul>
               </div>
            </div>
            <div class="user-listing">
                     自己紹介文
            </div>
            <p id="popup-introduction">
            </p>
            <div class="row">
               <div class="col-12">
                  <div class="user-listing">
                     住所
                     <span id="popup-address"></span>
                  </div>
               </div>
               <div class="col-md-auto">
                  <div class="user-listing">
                     返答時間目安
                     <span id="popup-response"> </span>
                  </div>
               </div>
               <div class="col-md-auto">
                  <div class="user-listing">
                     資格
                     <span id="popup-qualification"> </span>
                  </div>
               </div>
               <div class="col-12">
                  <div class="user-listing">
                     子供の有無
                     <span id="popup-child">  </span>
                  </div>
               </div>
            </div>
            <div class="user-listing">
               本人希望記入欄
               <p id="popup-skills"> </p>
            </div>
            <hr>
            <div class="row">
               <div class="col-6">
                  <div class="user-listing">
                     ふぁーまっち利用年数
                     <span id="popup-experience"></span>
                  </div>
               </div>
               <div class="col-6">
                  <div class="user-listing">
                     最低勤務時間
                     <span> 05 時間 </span>
                  </div>
               </div>
               <div class="col-6">
                  <div class="user-listing">
                     勤務回数
                     <span id="popup-completed"> 20 </span>
                  </div>
               </div>
               <div class="col-6">
                  <div class="user-listing">
                     累計職場回数
                     <span id="popup-no-of-drug-stores">  </span>
                  </div>
               </div>
               <div class="col-6">
                  <div class="user-listing">
                     リピート店舗回数
                     <span id="popup-repeated">  </span>
                  </div>
               </div>
               <div class="col-6">
                  <div class="user-listing">
                     直前キャンセル件数
                     <span id="sudden_cancellation"> </span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade bd-example-modal-lg" id="add-rejection" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-body">
         <button type="button" data-dismiss="modal" class="close-header">
           <svg class="icon">
             <use xlink:href="#check-cross"> </use>
           </svg>
         </button>
         <h3 class="modal-title" id="addeventLabel" style="margin-bottom:10px;"> 申請拒否の理由</h3>
         <form id="post-form">
           <div class="form-group row">
             <label for="staticEmail" class="col-sm-3 col-form-label"> 理由 : </label>
             <div class="col-sm-9">
               <textarea class="form-control" id="reject_reason" name="reject_reason"></textarea>
               <input type="text" id="reject_application_id" value="" style="display: none;">
             </div>
           </div>
           <div class="form-group row ">
             <label for="staticEmail" class="col-sm-3 col-form-label"></label>
             <div class="col-sm-9">
               <button class="btn btn-primary-default mr10 reject-application-reason" type="button"> 提出 </button>
               <button class="btn btn-secondary-default" data-dismiss="modal"> キャンセル </button>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="normalFarmatchModal" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="close-header">
                    <svg class="icon">
                        <use xlink:href="#check-cross"> </use>
                    </svg>
                </button>
                <h3 class="modal-title" id="addeventLabel">勤務案件の投稿は有料プランでご利用になれます。ご利用を希望の方は一般情報からふぁーまっち利用を有効にしてください</h3>
                <form id="post-form" class="mt-4">

                    <div class="form-group row ">
                        <div class="col-sm-6">
                            <a class="btn btn-primary-default btn-block mr10"  onclick="redirectToSettings()" > 一般情報に移動する </a>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-secondary-default btn-block" data-dismiss="modal"> キャンセル </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- -------------- -->




@endsection
@section('script')
<!--end::Global Theme Bundle -->
<!--begin::Calendar -->
<script src="{{ asset('store/js/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script>
   var route                     = '{{ route('update-store-profile') }}';
   var route2                    = '{{ route('update-store-profile2') }}';
   var routeTiming               = '{{ route('add-timing') }}';
   var routeTimingDelete         = '{{ route('delete-timing') }}';
   var getTimings                = '{{ route('get-timings') }}';
   var rejectChemistApplication  = '{{ route('reject-chemist-application') }}';
   var getChemistInforamtion     = '{{ route('get-chemist-information') }}';
   var acceptJobApplication      = '{{ route('accept-job-application') }}';
   var cancelJobApplication      = '{{ route('cancel-job-application') }}';
   var cancelAllApplications     = '{{ route('cancel-all-applications') }}';
   var cancelInspection          = '{{ route('cancel-inspection') }}';
   var markAsReadCancelled       = '{{ route('mark-as-read-cancelled') }}';
   var route_add_job_post        = '{{ route('add-store-job-post') }}';
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{ asset('store/js/custom/settings.js?v=7') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/custom/dashboard_home.js?v=7') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('story/js/dropzonejs.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales/ja.js" type="text/javascript"></script>

<script src="{{ asset('store/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/bootstrap-jp-datepicker.js') }}" type="text/javascript"></script>
<script>
    $('#kt_datepicker_1, #kt_datepicker_1_validate').datepicker({
        todayHighlight: true,
        multidate: true,
        startDate: new Date(),
        format: 'yyyy-mm-dd',
        endDate: '+182d',
    });
</script>

<script>

  function redirectToIntro() {
   window.open('/drug-store/employee-intro-service', '_self');
   }

  function redirectToSettings() {
      window.open('/drug-store/store-settings#normal_farmatch', '_self');
  }

</script>
<!--end::Page Scripts -->
<script>
   $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

   document.addEventListener('DOMContentLoaded', function() {
   "use strict";
   var KTCalendarBasic = function() {

       return {
           //main function to initiate the module
           init: function() {
               var todayDate = moment().startOf('day');222222222
               var YM = todayDate.format('YYYY-MM');
               var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
               var TODAY = todayDate.format('YYYY-MM-DD');
               var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

               var calendarEl = document.getElementById('kt_calendar');
               var calendar = new FullCalendar.Calendar(calendarEl, {
                   plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

                   isRTL: KTUtil.isRTL(),
                   header: {
                       left: 'prev,next today',
                       center: '',
                       right: 'title '
                   },

                   height: 600,
                   contentHeight: 600,
                   aspectRatio: 3, // see: https://fullcalendar.io/docs/aspectRatio

                   nowIndicator: true,
                   now: TODAY + 'T09:25:00', // just for demo

                   defaultView: 'dayGridMonth',
                   defaultDate: TODAY,
                   locale: 'ja',
                   disableDragging: false,
                   editable: false,
                   eventLimit: true, // allow "more" link when too many events
                   navLinks: false,
                   events: {!! $calenderArray !!},
                   eventRender: function(info) {

                       var element = $(info.el);
                       element.tooltip({ html: true, title: info.event.extendedProps.description });
                    //    if (info.event.extendedProps && info.event.extendedProps.description) {
                    //        if (element.hasClass('fc-day-grid-event')) {
                    //            element.data('content', info.event.extendedProps.description);
                    //            element.data('placement', 'top');
                    //            KTApp.initPopover(element);
                    //        } else if (element.hasClass('fc-time-grid-event')) {
                    //            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                    //        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                    //            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                    //        }
                    //    }
                   }
               });
               calendar.render();
               $("#v-pills-calendar-tab").click(function(){
                  calendar.render();
                  setTimeout(function(){
                     calendar.render();
                  },100);
               });
           }
       };
   }();

   jQuery(document).ready(function() {
       KTCalendarBasic.init();
   });

   "use strict";
   var KTCalendarBasic1 = function() {

       return {
           //main function to initiate the module
           init: function() {
               var todayDate = moment().startOf('day');
               var YM = todayDate.format('YYYY-MM');
               var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
               var TODAY = todayDate.format('YYYY-MM-DD');
               var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

               var calendarEl = document.getElementById('kt_calendar_pending');
               var calendar1 = new FullCalendar.Calendar(calendarEl, {
                   plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

                   isRTL: KTUtil.isRTL(),
                   header: {
                       left: 'prev,next today',
                       center: '',
                       right: 'title '
                   },

                   height: 600,
                   contentHeight: 600,
                   aspectRatio: 3, // see: https://fullcalendar.io/docs/aspectRatio

                   nowIndicator: true,
                   now: TODAY + 'T09:25:00', // just for demo

                   defaultView: 'dayGridMonth',
                   defaultDate: TODAY,
                   locale: 'ja',
                   disableDragging: false,
                   editable: false,
                   eventLimit: true, // allow "more" link when too many events
                   navLinks: false,
                   events: {!! $pendingApplications !!},

                   eventRender: function(info) {
                       var element = $(info.el);
                       if (info.event.extendedProps && info.event.extendedProps.description) {
                           if (element.hasClass('fc-day-grid-event')) {
                               element.data('content', info.event.extendedProps.description);
                               element.data('placement', 'top');
                               KTApp.initPopover(element);
                           } else if (element.hasClass('fc-time-grid-event')) {
                               element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                           } else if (element.find('.fc-list-item-title').lenght !== 0) {
                               element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                           }
                       }
                   }
               });
               calendar1.render();
               $("#v-pills-job-applications-tab").click(function(){
                  calendar1.render();
                  setTimeout(function(){
                     calendar1.render();
                  },100);
               });
            }
       };
   }();

   jQuery(document).ready(function() {
      KTCalendarBasic1.init();
   });
});

var url = document.location.toString();
if (url.match('#')) {
    $("#"+url.split('#')[1]).click();
    $.ajax({
        type: "POST",
        url : markAsReadCancelled,
        data:{
            notificationId : url.split('#')[3]
        },
        dataType: "json",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            if(data.status == 'success'){
                $("#"+url.split('#')[2]).css("background-color", "white");
            }
        }
    });
}



</script>
@endsection
