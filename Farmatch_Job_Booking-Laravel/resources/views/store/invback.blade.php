<!doctype html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Invoice</title>
      <style type="text/css">
        .table td, .table th {
          vertical-align: center;
          border: 1px solid #000;
        }
        @font-face{ 
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            /* src:url('{{ storage_path('fonts/ipag.ttf') }}'); */
        }
        body {
            font-family: ipag;
        }
     </style>
   </head>
   <body>
      <table width="530">
         <tr align="right">
            <td>
               2020/3/25 
            </td>
         </tr>
         <tr align="right">
            <td>
                請求書番号: 01
            </td>
         </tr>
      </table>
      <table width="530">
        <tr align="center">
           <td colspan="2" >
            <font color="#000" size="6">請求書</font>
           </td>
        </tr>
     </table>
      <table width="530" cellpadding="15">
         <thead>
            <tr align="center">
               <td colspan="2" height="100">
                  <font color="#000" size="6"></font>
               </td>
            </tr>
         </thead>
         <tbody>
            <table width="530">
               <tr>
                  <td>
                     <table>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              ●●薬局 様</font>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              件名：ふぁーまっち利用料として </font>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              下記のとおりご請求申し上げます。 </font>
                           </td>
                        </tr>
                        <tr>
                           <td style="text-decoration: underline;">
                              <font color="#000" size="2">
                              ご請求金額： ¥5,390- </font>
                           </td>
                        </tr>
                        <tr>
                           <td height="15"></td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              お支払い期限：2020/4/10</font>
                           </td>
                        </tr>
                     </table>
                  </td>
                  <td>
                     <table align="right">
                        <tr>
                           <td><font color="#000" size="2">
                              株式会社薬けん </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              代表薬剤師　野中牧  </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              〒874-0909 </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              大分県別府市田の湯町3番7号  </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              アライアンスタワーZ4階 </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              Tel:090-8400-8989 info@yakuken.work  </font>
                           </td>
                        </tr>
                     </table>
                  </td>
                  <td>
                     <table align="right">
                        <tr>
                           <td >
                              <img src="images/img_01.png" width="80">
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <img src="images/img_02.jpeg" width="80">  
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </tbody>
         <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
            <thead>
               <tr>
                  <td bgcolor="#ccc">品番・品名</th>
                  <td bgcolor="#ccc">数量</th>
                  <td bgcolor="#ccc">単価</th>
                  <td bgcolor="#ccc">金額(税込)</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td align="left">ふぁーまっち月間掲載料</th>
                  <td align="right">1式</td>
                  <td align="right">2,200</td>
                  <td align="right">2,200</td>
               </tr>
               <tr align="left">
                  <td>マッチング手数料</th>
                  <td align="right">29 時間</td>
                  <td align="right">110</td>
                  <td align="right">3190</td>
               </tr>
               <tr>
                  <td  colspan = "2" align="right"></td>
                  <td  align="right">合計</td>
                  <td align="right">5,390</td>
               </tr>
            </tbody>
         </table>
      </table>
      <br><br>
      <font color="#000" size="4">
        マッチング詳細  </font>
      <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
         <thead>
            <tr>
               <td align="center" bgcolor="#f9c7c7">薬剤師勤務日・薬剤師名</th>
               <td align="center" bgcolor="#f9c7c7">勤務時間</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td align="left">2020. 2.6 宮井 智史</td>
               <td align="right">6時間</td>
            </tr>
            <tr>
               <td align="left">2020. 2.11 野中 牧</td>
               <td align="right">7時間</td>
            </tr>
            <tr>
               <td align="left">2020. 2.12 宮井 智史</td>
               <td align="right">4時間</td>
            </tr>
            <tr>
               <td align="left">2020. 2.19 宮井 智史</td>
               <td align="right">5時間</td>
            </tr>
            <tr>
               <td  colspan = "1" align="right">合計</td>
               <td  colspan = "1" align="right">29時間</td>
            </tr>
         </tbody>
      </table>
      <br>
      <br>
      <br>
      <br>
      <br>
      <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <font color="#000"  size="2">お振込先：     </font> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <font color="#000" size="2">大分みらい信用金庫　本店　普通９４５８９１２　株式会社 薬けん
                            </font> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
   </body>
</html>