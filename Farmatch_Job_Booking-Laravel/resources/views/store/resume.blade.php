<html>
   <head>
      <meta http-equiv=Content-Type content="text/html; charset=utf-8">
      <meta name=ProgId content=Word.Document>
      <meta name=Generator content="Microsoft Word 15">
      <meta name=Originator content="Microsoft Word 15">
      <style>
         @page {
         margin: 30px;
         }
         @font-face {
         font-family: ipag;
         font-style: normal;
         font-weight: normal;
         /* src:url('{{ storage_path('fonts/ipag.ttf') }}'); */
         }
         body {
         font-family: ipag;
         }
      </style>
   </head>
   <body lang=EN-IN style='tab-interval:36.0pt; margin:0 auto;'>
      <div class="WordContainer" style="margin:0 auto; display:table">
         <div class=WordSection1 style='width:100%'>
            <table class=MsoTable15Plain4 border=0 cellspacing=0 cellpadding=0 width="60%" style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:
               0cm 0cm 0cm 0cm'>
               <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes;
                  mso-yfti-lastrow:yes'>
                  <td width="100%" valign=top style='width:100.0%;padding:0cm 0cm 0cm 0cm'>
                     <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:
                        solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:14.2pt'>
                           <td width=99 style='width:64.5pt;border:solid windowtext 1.0pt;mso-border-alt:
                              solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 ふりがな 
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=431 colspan=5 style='width:323.4pt;border:solid windowtext 1.0pt;
                              border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                              solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 <o:p>&nbsp;{{$chemist->furikana}}</o:p>
                              </p>
                           </td>
                        </tr>
                        <tr style='mso-yfti-irow:1;height:14.2pt'>
                           <td width=99 style='width:64.5pt;border:solid windowtext 1.0pt;border-top:
                              none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 <span style='mso-spacerun:yes'> </span>氏　名
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=431 colspan=5 style='width:323.4pt;border-top:none;border-left:
                              none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 <o:p>&nbsp;{{$chemist->full_name}}</o:p>
                              </p>
                           </td>
                        </tr>
                        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:14.2pt'>
                           <td width=99 style='width:64.5pt;border:solid windowtext 1.0pt;border-top:
                              none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 <span lang=EN-US></span>
                                 <o:p>昭和・平成</o:p>
                              </p>
                           </td>
                           @php
                           $dob = $chemist->dob;
                           $year = "";
                           $month = "";
                           $day = "";
                           $age = "";
                           if($dob){
                           $dob = explode('-',$dob);
                           $year = $dob[0];
                           $month = $dob[1];
                           $day = $dob[2];
                           $age = \Carbon\Carbon::parse($chemist->dob)->age;
                           }
                           @endphp
                           <td width=61 style='width:45.95pt;border-top:none;border-left:none;
                              border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 {{$year}} 年
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=66 style='width:49.35pt;border-top:none;border-left:none;
                              border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 {{$month}} 月
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=58 style='width:43.8pt;border-top:none;border-left:none;
                              border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 {{$day}} 日生　
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=58 style='width:43.75pt;border-top:none;border-left:none;
                              border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 {{$age}} 歳)
                                 <o:p></o:p>
                              </p>
                           </td>
                           <td width=187 style='width:20.55pt;border-top:none;border-left:none;
                              border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                              mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;
                              height:14.2pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                                 @if($chemist->gender == "Male")男 @else 女< @endif 
                                 <o:p>
                                 </o:p>
                              </p>
                           </td>
                        </tr>
                     </table>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal;mso-yfti-cnfc:5'>
                        <b>
                           <o:p></o:p>
                        </b>
                     </p>
                  </td>
                  <td width="100%" valign=top style='width:100.0%;padding:0cm 0cm 0cm 0cm'>
                     <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=151 style='width:4.0cm;margin-left:11.0pt;border-collapse:collapse;border:none;
                        mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
                        0cm 5.4pt 0cm 5.4pt'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
                           height:104.55pt'>
                           <td width=153 valign=top style='width:114.85pt;border:solid windowtext 1.0pt;
                              mso-border-alt:solid windowtext .5pt;
                              height:104.55pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
                                 line-height:normal'>
                              </p>
                              @if($chemist->image)
                              @php
                              $path = asset('images/chemist/'.$chemist->image);
                              $base64 = "";
                              if(file_exists($path)){
                                 $type = pathinfo($path, PATHINFO_EXTENSION);
                                 $data = file_get_contents($path);
                                 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                              }
                              @endphp
                              <img src="<?php echo $base64 ?>" width="100%" height="100%" />
                              @endif
                           </td>
                        </tr>
                     </table>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal;mso-yfti-cnfc:1'>
                        <b>
                           <o:p></o:p>
                        </b>
                     </p>
                  </td>
               </tr>
            </table>
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            
            <table width="100%" cellpadding="15" cellspacing="0" style="border:2px solid #333333;">
					<tr>
						<td colspan="2" cellpadding="0" cellspacing="0" style="border-right:2px solid #333333;border-bottom:2px solid #333333;"> ふりがな </td>
						<td colspan="1" cellpadding="0" cellspacing="0" style="border-bottom:2px solid #333333;">{{$chemist->furikana}}</td>
					</tr>
					<tr>
						<td colspan="2" cellpadding="0" cellspacing="0" style="border-right:2px solid #333333;border-bottom:2px solid #333333;"> ふりがな </td>
						<td colspan="1" cellpadding="0" cellspacing="0" style="border-bottom:2px solid #333333;"></td>
					</tr>				
					<tr >
						<td cellpadding="0" cellspacing="0" style="border-right:2px solid #333333; border-bottom:2px solid #333333;" width="80"> 現住所 </td>
						<td cellpadding="0" cellspacing="0" style="border-right:2px solid #333333; border-bottom:2px solid #333333;" width="80"> 
							ふりがな <br/>
							{{$chemist->post_code}} 
						</td>
						<td cellpadding="0" cellspacing="0" style="border-bottom:2px solid #333333;">
							@if($chemist->city)
                        {{$chemist->city->city}}<br />
                        @endif
							{{$chemist->town}}
						</td>
					</tr>

					<tr>
						<td colspan="3" cellpadding="0" cellspacing="0" style="border-right:2px solid #333333;border-bottom:2px solid #333333;"> 
							メールアドレス <br/> 
							{{$chemist->email}}
						</td>
					</tr>

					<tr>
						<td colspan="3" cellpadding="0" cellspacing="0" style="border-right:2px solid #333333;border-bottom:2px solid #333333;"> 
							携帯電話番号 <br/>
							{{$chemist->phone}}
						</td>
					</tr>
					 
            </table>

				<!-- <table width="100%" height="300" cellpadding="20">
					<tr>
						<td>
						
						</td>
					</tr>
				</table>

            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:14.2pt'>
                  <td width="66%" valign=top style='width:66.38%;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'> ふりがな </p>
                     <p>
                        {{$chemist->furikana}}
                     </p>
                  </td>
                  <![if !supportMisalignedRows]>
                  <td style='height:14.2pt;border:none' width=0 height=19></td>
                  <![endif]>
               </tr>
               <tr style='mso-yfti-irow:1;height:14.2pt'>
                  <td width="66%" rowspan=2 valign=top style='width:66.38%;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'> 現住所 </p>
                     <p>
                        @if($chemist->city)
                        {{$chemist->city->city}}<br />
                        @endif
                        {{$chemist->town}}
                     </p>
                  </td>
                  <span style='font-size:11.0pt;font-family:"Calibri",sans-serif;mso-ascii-theme-font:
                     minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
                     mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Kartika;mso-bidi-theme-font:
                     minor-bidi;mso-ansi-language:EN-IN;mso-fareast-language:EN-US;mso-bidi-language:
                     ML'>
                     {{-- <![if !supportMisalignedRows]> --}}
                     <td style='height:14.2pt;border:none' width=0 height=19></td>
                     {{-- <![endif]> --}}
                  </span>
               </tr>
               <tr style='mso-yfti-irow:2;height:14.2pt'>
                  {{-- <![if !supportMisalignedRows]> --}}
                  <td style='height:14.2pt;border:none' width=0 height=19></td>
                  {{-- <![endif]> --}}
               </tr>
               <tr style='mso-yfti-irow:3;height:14.2pt'>
                  <td width="66%" valign=top style='width:66.38%;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'><b>Pin code</b></p>
                     <p>
                        {{$chemist->post_code}}
                     </p>
                  </td>
                  <span style='font-size:11.0pt;font-family:"Calibri",sans-serif;mso-ascii-theme-font:
                     minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
                     mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Kartika;mso-bidi-theme-font:
                     minor-bidi;mso-ansi-language:EN-IN;mso-fareast-language:EN-US;mso-bidi-language:
                     ML'>
                     {{-- <![if !supportMisalignedRows]> --}}
                     <td style='height:14.2pt;border:none' width=0 height=19></td>
                     {{-- <![endif]> --}}
                  </span>
               </tr>
               <tr style='mso-yfti-irow:4;height:14.2pt'>
                  <td width="100%" valign=top style='width:100.0%;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>メールアドレス</p>
                     <p>
                        {{$chemist->email}}
                     </p>
                  </td>
                  <![if !supportMisalignedRows]>
                  <td style='height:14.2pt;border:none' width=0 height=19></td>
                  <![endif]>
               </tr>
               <tr style='mso-yfti-irow:4;height:14.2pt'>
                  <td width="100%" valign=top style='width:100.0%;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt;height:14.2pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>携帯電話番号</p>
                     <p>
                        {{$chemist->phone}}
                     </p>
                  </td>
                  <![if !supportMisalignedRows]>
                  <td style='height:14.2pt;border:none' width=0 height=19></td>
                  <![endif]>
               </tr>
            </table> -->
            @if($chemist->education->count() > 0)
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width=104 valign=top style='width:77.75pt;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;background:#BFBFBF;mso-background-themecolor:
                     background1;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>年</p>
                  </td>
                  <td width=104 valign=top style='width:77.95pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#BFBFBF;mso-background-themecolor:background1;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>月</p>
                  </td>
                  <td width=393 valign=top style='width:295.1pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#BFBFBF;mso-background-themecolor:background1;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>学歴・職歴(各別にまとめて書く)</p>
                  </td>
               </tr>
               @php
               $i =0;
               @endphp
               @foreach ($chemist->education as $education)
               @php
               ++$i;
               $last_row = "";
               if($i == $chemist->education->count()){
               $last_row = "mso-yfti-lastrow:yes";
               }
               @endphp
               <tr style='mso-yfti-irow:{{$i}};{{$last_row}}'>
                  <td width=104 valign=top style='width:77.75pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$education->year}}</o:p>
                     </p>
                  </td>
                  <td width=104 valign=top style='width:77.95pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$education->month}}</o:p>
                     </p>
                  </td>
                  <td width=393 valign=top style='width:295.1pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$education->content}}</o:p>
                     </p>
                  </td>
               </tr>
               @endforeach
            </table>
            @endif
            @if($chemist->licence_number)
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width=104 valign=top style='width:77.75pt;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:
                     background2;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>LICENSE REGISTRATION DATE</p>
                  </td>
                  <td width=66 valign=top style='width:49.6pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>年</p>
                  </td>
                  <td width=94 valign=top style='width:70.85pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>月</p>
                  </td>
                  <td width=337 valign=top style='width:252.6pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>薬剤師免許証登録番号</p>
                  </td>
               </tr>
               <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                  <td width=104 valign=top style='width:77.75pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$chemist->registration_year}}/{{$chemist->registration_month}}</o:p>
                     </p>
                  </td>
                  <td width=66 valign=top style='width:49.6pt;border-top:none;border-left:none;
                     border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$chemist->registration_year}}</o:p>
                     </p>
                  </td>
                  <td width=94 valign=top style='width:70.85pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$chemist->registration_month}}</o:p>
                     </p>
                  </td>
                  <td width=337 valign=top style='width:252.6pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$chemist->licence_number}}</o:p>
                     </p>
                  </td>
               </tr>
            </table>
            @endif
            @if($chemist->qualification->count())
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width=85 valign=top style='width:63.55pt;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:
                     background2;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>年</p>
                  </td>
                  <td width=123 valign=top style='width:92.15pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>月</p>
                  </td>
                  <td width=393 valign=top style='width:295.1pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                     mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>免許・資格</p>
                  </td>
               </tr>
               @php
               $i =0;
               @endphp
               @foreach ($chemist->qualification as $qualification)
               @php
               ++$i;
               $last_row = "";
               if($i == $chemist->qualification->count()){
               $last_row = "mso-yfti-lastrow:yes";
               }
               @endphp
               <tr style='mso-yfti-irow:{{$i}};{{$last_row}}'>
                  <td width=85 valign=top style='width:63.55pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$qualification->year}}</o:p>
                     </p>
                  </td>
                  <td width=123 valign=top style='width:92.15pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$qualification->month}}</o:p>
                     </p>
                  </td>
                  <td width=393 valign=top style='width:295.1pt;border-top:none;border-left:
                     none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;{{$qualification->content}}</o:p>
                     </p>
                  </td>
               </tr>
               @endforeach
            </table>
            @endif
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width=301 valign=top style='width:225.4pt;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;</o:p>
                     </p>
                  </td>
                  <td width=301 colspan=4 valign=top style='width:225.4pt;border:solid windowtext 1.0pt;
                     border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                     solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>Response time : {{$chemist->response_time}}</p>
                  </td>
               </tr>
               <tr style='mso-yfti-irow:4'>
                  <td width=301 valign=top style='width:225.4pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;</o:p>
                     </p>
                  </td>
                  <td width=301 colspan=4 valign=top style='width:225.4pt;border-top:none;
                     border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>Number of dependents (excluding spouse) people : {{$chemist->no_of_dependents}}</p>
                  </td>
               </tr>
               <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes'>
                  <td width=301 valign=top style='width:225.4pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        <o:p>&nbsp;</o:p>
                     </p>
                  </td>
                  <td width=150 colspan=2 valign=top style='width:112.7pt;border-top:none;
                     border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal;tab-stops:58.5pt'>Spouse<span style='mso-tab-count:1'> </span></p>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal;tab-stops:58.5pt'>{{$chemist->spouse}}</p>
                  </td>
                  <td width=150 colspan=2 valign=top style='width:112.7pt;border-top:none;
                     border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                     mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>Spousal support obligation</p>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>{{$chemist->spousal_support}}</p>
                  </td>
               </tr>
               {{-- <![if !supportMisalignedColumns]> --}}
               <tr height=0>
                  <td width=353 style='border:none'></td>
                  <td width=118 style='border:none'></td>
                  <td width=59 style='border:none'></td>
                  <td width=59 style='border:none'></td>
                  <td width=118 style='border:none'></td>
               </tr>
               {{-- <![endif]> --}}
            </table>
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
               mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
               <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width=601 valign=top style='width:450.8pt;border:solid windowtext 1.0pt;
                     mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>Desired entry field</p>
                  </td>
               </tr>
               <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes;height:24.6pt'>
                  <td width=601 valign=top style='width:450.8pt;border:solid windowtext 1.0pt;
                     border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                     padding:0cm 5.4pt 0cm 5.4pt;height:24.6pt'>
                     <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                        normal'>
                        {{$chemist->desired_entry}}
                     </p>
                  </td>
               </tr>
            </table>
            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
               <o:p>&nbsp;</o:p>
            </p>
         </div>
      </div>
   </body>
</html>