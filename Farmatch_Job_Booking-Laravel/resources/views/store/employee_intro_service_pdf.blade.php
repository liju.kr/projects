<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
	*, *::before, *::after {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

body {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
}

html, body {
    height: 100%;
    margin: 0px;
    padding: 0px;
    font-size: 13px;
    font-weight: 300;
    font-family: Poppins, Helvetica, sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

        .pie-chart {
            width: 600px;
            height: 400px;
            margin: 0 auto;
        }
		textarea {
		width:100%;
		border:0;
		}
        .bd-example {
            position: relative;
			border:1px solid #252525;
        }
        .form-group label {
            font-size: 11px;
            color: #3c3c3c;
			letter-spacing: -0.5px;
        }
        .bd-example input.form-control, .bd-example select.form-control {
            height:auto;
            font-size: 12px;
            margin-top: 0;
			width:100%;
			border:0;
        }
        .form-section .form-section-column .form-label {
            display: block;
            -webkit-text-orientation: mixed;
            -webkit-writing-mode: vertical-lr;
            white-space: nowrap;
            margin-right: 5px;
            padding-right: 5px;
            width: 44px;
            align-items: center;
		}
        .form-section-column .content-box {
            border-left: solid #252525;
            border-width: 0.1rem;
            -webkit-flex: 1;
            flex: 1;
        }
        .d-flex {
            display: -webkit-box; /* wkhtmltopdf uses this one */
            display: flex;
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
        }
		.container {
            max-width: 1200px;
            margin-left: auto;
            margin-right: auto;
        }
        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
		.input-group {
			position: relative;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;

			-webkit-box-align: stretch;
			-ms-flex-align: stretch;
			align-items: stretch;
			width: 100%;
		}
		.input-group-append {
			margin-left: -1px;
		}
		.input-group-prepend, .input-group-append {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			white-space:nowrap;
		}
		.m-0 {
		margin:0!important;
		}
		.flex-nowrap {
		flex-wrap:nowrap!important
		}
        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
            position: relative;
            padding-right: 5px;
            padding-left: 5px;
        }
		.col-sm-2 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 16.66667%;
			flex: 0 0 16.66667%;
			max-width: 16.66667%;
		}
		.col-sm-3 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 25%;
			flex: 0 0 25%;
			max-width: 25%;
		}
        .col-lg-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-lg-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.33333%;
            flex: 0 0 33.33333%;
            max-width: 33.33333%;
        }
		.col-sm-4 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 33.33333%;
			flex: 0 0 33.33333%;
			max-width: 33.33333%;
		}
		.col-sm-8 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 66.66667%;
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
		.col-sm-5 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 41.66667%;
			flex: 0 0 41.66667%;
			max-width: 41.66667%;
		}
        .col-lg-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.66667%;
            flex: 0 0 41.66667%;
            max-width: 41.66667%;
        }
		.col-lg-6 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 50%;
			flex: 0 0 50%;
			max-width: 50%;
		}
		.col-sm-7 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 58.33333%;
			flex: 0 0 58.33333%;
			max-width: 58.33333%;
		}
        .col-lg-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.33333%;
            flex: 0 0 58.33333%;
            max-width: 58.33333%;
        }
		.col-lg-8 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 66.66667%;
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
        .col-sm-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.66667%;
            flex: 0 0 16.66667%;
            max-width: 16.66667%;
        }
        .col-sm-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.66667%;
            flex: 0 0 66.66667%;
            max-width: 66.66667%;
        }
		.col-lg-9 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 75%;
			flex: 0 0 75%;
			max-width: 75%;
		}
		.col-sm-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
		.col-12 {
			-webkit-box-flex: 0;
			-ms-flex: 0 0 100%;
			flex: 0 0 100%;
			max-width: 100%;
		}
        .col-form-label {
            padding-top: calc(0.25rem + 1px);
            padding-bottom: calc(0.25rem + 1px);
            margin-bottom: 0;
            font-size: inherit;
            line-height: 1.5;
			white-space:nowrap;
        }
        .flex-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }
        .dropdown-divider {
            height: 0;
            margin: 0.5rem 0;
            overflow: hidden;
            border-top: 1px solid #e9ecef;
        }
        .text-center {
            text-align: center !important;
        }
        .pt-5, .py-5 {
            padding-top: 3rem !important;
        }
        .bg-white {
            background-color: #fff;
        }
        .p-5 {
            padding: 3rem !important;
        }
        .mb-5, .my-5 {
            margin-bottom: 3rem !important;
        }
        .mt-3, .my-3 {
            margin-top: 1rem !important;
        }
        .container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
            width: 100%;
            padding-right: 10px;
            padding-left: 10px;
            margin-right: auto;
            margin-left: auto;
        }
        .align-items-center {
            -webkit-align-content: center;
            -webkit-align-items: center;
            align-items: center;
        }
        .justify-content-center {
            -webkit-box-pack: center; /* wkhtmltopdf uses this one */
            justify-content: center;
        }
        .mx-0 {
            margin-left: 0;
            margin-right: 0
        }
        
        .mb-0 { margin-bottom: 0 }
        
        .w-100 { width:100% }
		
		.pb-3, .py-3 {
			padding-bottom: 0.5rem !important;
		}
		.pt-3, .py-3 {
			padding-top: 0.5rem !important;
		}
    </style>
    {{-- make sure you are using http, and not https --}}
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
        function init() {
            google.load("visualization", "1.1", {
                packages: ["corechart"],
                callback: 'drawChart'
            });
        }

        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Pizza');
            data.addColumn('number', 'Populartiy');
            data.addRows([
                ['Pepperoni', 33],
                ['Hawaiian', 26],
                ['Mushroom', 22],
                ['Sausage', 10], // Below limit.
                ['Anchovies', 9] // Below limit.
            ]);

            var options = {
                title: 'Popularity of Types of Pizza',
                sliceVisibilityThreshold: .2
            };

            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>
</head>

<body onload="init()">


<div class="container">

<div class="row">

                  <div class="col-lg-4">
                     <div class="bd-example form_container">
					 <h5 for="staticEmail" class="col-sm-4 col-form-label">４ 労働時間</h5>
                              <div class="form-section position-static">
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">就業時間</div>
                                    <div class="row content-box flex-column m-0">
									
                                       .<div class="col-sm-12">
									   <div class="form-group row flex-nowrap m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(1)</label>
                                          <div class="c">
                                             <div class="row m-0 align-items-center flex-nowrap">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_1_from" name="working_hours_1_from" placeholder="00:00" maxlength="5">
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_1_to" name="working_hours_1_to" placeholder="00:00">
                                             </div>
                                             </div>
                                          </div>
                                       </div>
									   
									   <div class="form-group row flex-nowrap m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(2)</label>
                                          <div class="c">
                                             <div class="row m-0 align-items-center flex-nowrap">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_2_from" name="working_hours_2_from" placeholder="00:00" maxlength="5">
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_2_to" name="working_hours_2_to" placeholder="00:00">
                                             </div>
                                             </div>
                                          </div>
                                       </div>
									   
									   <div class="form-group row flex-nowrap m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(3)</label>
                                          <div class="c">
                                             <div class="row m-0 align-items-center flex-nowrap">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_3_from" name="working_hours_3_from" placeholder="00:00" maxlength="5">
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_3_to" name="working_hours_3_to" placeholder="00:00">
                                             </div>
                                             </div>
                                          </div>
                                       </div>
									   
									   <div class="form-group row flex-nowrap m-0 py-3 w-100">
                                          <label class="col col-form-label" for="" style="white-space: nowrap;">又は</label>
                                          <div class="col-auto">
                                             <div class="row align-items-center flex-nowrap">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_or_from" name="working_hours_or_from" placeholder="00:00">
                                                </div>
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_or_to" name="working_hours_or_to" placeholder="00:00">
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                                <div class="col-5">
                                                   <div class="d-flex align-items-center">
                                                      <label class="mr-3 col-form-label" style="white-space: nowrap" for="">の間の</label>
														<div class="input-group">
                                                            <input type="text" class="form-control form-control-sm number_field" id="working_hours_between" name="working_hours_between">
                                                            <div class="input-group-append">
                                                               <div class="input-group-text">時間</div>
                                                             </div>
                                                          </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
									   
									   </div>

                                       <div class="col-12 py-3">
                                          <label class="col-12 col-form-label" for="">就業時間に関する特記事項</label>
                                          <textarea name="working_hours_notes" id="working_hours_notes" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">時間外労働時間</div>

                                    <div class="row flex-column content-box m-0">
                                       <div class="form-group row m-0 pb-3 align-items-center flex-nowrap">
                                          <div class="col-lg-8">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="overtime_work_hours" value="時間外労働あり"> 時間外労働あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="overtime_work_hours" value="時間外労働なし"> 時間外労働なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="overtime_work_hours-error" class="form-error" for="overtime_work_hours"></label>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="row flex-nowrap align-items-center">
                                                <label class="col-auto mr-2 col-form-label" for="">月平均</label>
                                                <input type="text" class="col form-control form-control-sm" id="overtime_work_hours_details" name="overtime_work_hours_details">
                                                <label class="col-auto ml-2 col-form-label" for="">時間</label>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="dropdown-divider my-0"></div>

                                       <div class="form-group row align-items-center m-0 pb-3">
                                          <label class="col-lg-6 col-form-label" for="">36協定における特別条項</label>
                                          <div class="col-sm-6">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="special_provisions" value="あり"> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="special_provisions" value="なし"> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="special_provisions-error" class="form-error" for="special_provisions"></label>
                                          </div>

                                          <div class="dropdown-divider my-0"></div>

                                       <div class="col-12 py-3">
                                          <label class="col-12 col-form-label" for="">特別な事情・期間等</label>
                                          <textarea id="special_provisions_details" name="special_provisions_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                       </div>

                                       </div>


                                    </div>

                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
								 
								 <div class="form-label position-relative">時間　休憩</div>
                                       <div class="form-group row flex-nowrap content-box m-0">
                                          <div class="col-lg-6">
                                             <div class="input-group">
                                                <label class="col-form-label mr-3" for=""></label>
                                                <input type="text" class="form-control form-control-sm number_field" id="time_break" name="time_break" placeholder="">
                                                <div class="input-group-append">
                                                   <div class="input-group-text">分</div>
                                                 </div>
                                              </div>
                                              <label id="time_break-error" class="form-error" for="time_break"></label>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="input-group">
                                                <label for="" class="col-form-label mr-3">年間休日数</label>
                                                <input type="text" class="form-control form-control-sm number_field" id="annual_holidays" name="annual_holidays" placeholder="">
                                                <div class="input-group-append">
                                                   <div class="input-group-text">日</div>
                                                 </div>
                                              </div>
                                              <label id="annual_holidays-error" class="form-error" for="annual_holidays"></label>
                                          </div>
                                       </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">休日等</div>

                                    <div class="row flex-column content-box m-0">
                                       <div class="form-group row m-0">
                                          <div class="col-12 py-3">
                                             <textarea name="holidays_etc" id="holidays_etc" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>
                                       </div>

                                       <div class="form-group row m-0">
                                          <div class="col-sm-7">
                                             <label class="col-12 col-form-label" for="">6ヶ月経過後の年次有給休暇日数</label>
                                          </div>
                                          <div class="col-sm-5">
                                             <div class="input-group mb-2">
                                                <input type="text" class="form-control form-control-sm number_field" id="annual_paid_vacation_days_after_6_months" name="annual_paid_vacation_days_after_6_months" placeholder="">
                                                <div class="input-group-append">
                                                   <div class="input-group-text">日</div>
                                                 </div>
                                              </div>
                                              <label id="annual_paid_vacation_days_after_6_months-error" class="form-error" for="annual_paid_vacation_days_after_6_months"></label>
                                          </div>
                                       </div>

                                    </div>
                                 </div>
                              </div>
							  
							  <div class="dropdown-divider my-0"></div>
								<h5 for="staticEmail" class="col-sm-4 col-form-label">５ その他の労働条件等</h5>
                              <div class="form-section position-static mt-5">
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">保険加入</div>
                                    <div class="form-group row m-0 w-100 content-box py-3">
                                       <div class="col-sm-4">

                                          <div class="kt-checkbox-list row">
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="雇用"> 雇用
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="労災"> 労災
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="公災"> 公災
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="健康"> 健康
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="厚生"> 厚生
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="財形"> 財形
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" id="insurance_coverage_other" name="insurance_coverage[]" value="その他"> その他
                                                <span></span>
                                             </label>
                                         </div>

                                         <label id="insurance_coverage-error" class="form-error" for="insurance_coverage"></label>

                                          <textarea disabled id="insurance_coverage_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="row">
                                             <label class="col-form-label col-12" for="">退職金共済</label>
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_benefits" value="加入"> 加入
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_benefits" value="未加入"> 未加入
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="retirement_benefits-error" class="form-error" for="retirement_benefits"></label>
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="row">
                                             <label class="col-form-label col-12" for="">退職金制度</label>
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_system" value="あり"> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_system" value="なし"> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="retirement_system-error" class="form-error" for="retirement_system"></label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">年金　企業</div>
                                    <div class="form-group row flex-column m-0 w-100 content-box py-3">
                                       <div class="col-sm-12 mb-3">

                                          <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]"> 厚生年金機構
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]"> 確定拠出機構
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]"> 確定給付年金
                                             <span></span>
                                             </label>
                                         </div>

                                         <label id="annuities_corporate[]-error" class="form-error" for="annuities_corporate[]"></label>

                                          <!-- <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pension_corporations" value="EM system"> 厚生年金機構
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pension_corporations" value="Gooco"> 確定拠出機構
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="pension_corporations" value="Gooco"> 確定給付年金
                                                <span></span>
                                                </label>
                                          </div> -->

                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-group row m-0 mb-0 w-100 py-3">
                                       <div class="col-lg-8">
                                          <div class="row">
                                             <div class="col-lg-4">
                                                <div class="row">
                                                   <label class="col-form-label col-lg-12" for="">定年制</label>
                                                   <div class="col-sm-12">
                                                      <div class="kt-radio-inline">
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="retirement_age" value="あり"> あり
                                                         <span></span>
                                                         </label>
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="retirement_age" value="なし"> なし
                                                         <span></span>
                                                         </label>
                                                      </div>
                                                      <label id="retirement_age-error" class="form-error" for="retirement_age"></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-4">
                                                <div class="row">
                                                   <label class="col-form-label col-lg-12" for="">再雇用制度</label>
                                                   <div class="col-sm-12">
                                                      <div class="kt-radio-inline">
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="reemployment_system" value="あり"> あり
                                                         <span></span>
                                                         </label>
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="reemployment_system" value="なし"> なし
                                                         <span></span>
                                                         </label>
                                                      </div>
                                                      <label id="reemployment_system-error" class="form-error" for="reemployment_system"></label>
                                                   </div>
                                                </div>
                                             </div>
											 
											 <div class="col-lg-4">
                                          <div class="row">
                                             <label class="col-form-label col-lg-12" for="">勤務延長</label>
                                             <div class="col-sm-12">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="duty_extension" value="あり"> あり
                                                   <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="duty_extension" value="なし"> なし
                                                   <span></span>
                                                   </label>
                                                </div>
                                                <label id="duty_extension-error" class="form-error" for="duty_extension"></label>
                                             </div>
                                          </div>
                                       </div>
									   
                                          </div>
                                       </div>

                                       

                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row m-0 mb-0 w-100 py-3">
                                    <div class="col-lg-12">
                                       <div class="row align-items-center">
                                          <div class="col-lg-4">
                                             <div class="row m-0 flex-nowrap">
                                                <label class="col-lg-12 col-form-label" for="">一律</label>
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="retirement_age_details" name="retirement_age_details" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳</div>
                                                      </div>
                                                </div>
                                                <label id="retirement_age_details-error" class="form-error" for="retirement_age_details"></label>
                                             </div>
                                          </div>
                                          <div class="col-lg-4">
                                             <div class="row m-0 flex-nowrap">
                                                <label class="col-lg-12 col-form-label" for="">上限</label>
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="reemployment_system_details" name="reemployment_system_details" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳まで</div>
                                                      </div>
                                                </div>
                                                <label id="reemployment_system_details-error" class="form-error" for="reemployment_system_details"></label>
                                             </div>
                                          </div>
                                          <div class="col-lg-4">
                                             <div class="row m-0 flex-nowrap">
                                                <label class="col-form-label col-lg-12" for="">上限</label>
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="duty_extension_details" name="duty_extension_details" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳まで</div>
                                                    </div>
                                                </div>
                                                <label id="duty_extension_details-error" class="form-error" for="duty_extension_details"></label>
                                             </div>
                                          </div>
                                          </div>
                                       </div>

                                    </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row align-items-center m-0 mb-0 w-100 py-3">

                                    <label class="col-lg-2 col-form-label" for="">入居可能住宅</label>

                                    <div class="col-lg-10">
                                       <div class="row align-items-center content-box flex-nowrap">
                                          <div class="col-lg-6">
                                             <div class="row flex-column">
                                                <label class="col-lg-12 col-form-label" for="">単身用</label>
                                                <div class="col-lg-12">
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_singles" value="あり"> あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_singles" value="なし"> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="housing_for_singles-error" class="form-error" for="housing_for_singles"></label>
                                                </div>
                                                <div class="dropdown-divider"></div>
                                                <label class="col-lg-12 col-form-label" for="">世帯用</label>
                                                <div class="col-lg-12">
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_households" value="あり"> あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_households" value="なし"> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="housing_for_households-error" class="form-error" for="housing_for_households"></label>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="col-lg-6">
                                             <textarea name="housing_details" id="housing_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row align-items-center m-0 mb-0 w-100 py-3">

                                    <label class="col-lg-6 col-form-label" for="">利用可能託児施設</label>

                                    <div class="col-lg-6">
                                       <div class="row">
                                          <label class="col-lg-4 col-form-label" for="">単身用</label>
                                          <div class="col-lg-8">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="childcare_facilities" value="あり"> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="childcare_facilities" value="なし"> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="childcare_facilities-error" class="form-error" for="childcare_facilities"></label>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="col-12">
                                       <textarea id="childcare_facilities_details" name="childcare_facilities_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                    </div>

                                 </div>
                              </div>
                           </div>
                        </div>
                      </div>




                      <div class="col-lg-4">
                        <div class="bd-example form_container">
						<h5 for="staticEmail" class="col-sm-4 col-form-label">６ 会社の情報</h5>
                                 <div class="form-section position-static">
                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">就業場所</div>

                                       <div class="row flex-column content-box w-100 m-0">
                                          <div class="form-group align-items-center row m-0 w-100 py-3">
                                             <div class="col-sm-3">
                                                <div class="row flex-nowrap">
												<label class="col-auto col-form-label" for="">従業員数</label>
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="number_of_employees" name="number_of_employees" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
												</div>
                                             </div>
                                             <label class="col-auto col-form-label" for="">設立年</label>
                                             <div class="col-sm-4">
                                                <div class="row flex-nowrap align-items-center">
                                                   <div class="col-4 form-group mr-2">
                                                      <div class="row flex-nowrap">
													  <label for="exampleFormControlSelect1" style="white-space: nowrap;">日本の年号</label>
                                                      <select class="form-control form-control-sm px-0" id="japanese_era" name="japanese_era">
                                                        <option value="大正">大正</option>
                                                        <option value="昭和">昭和</option>
                                                        <option value="平成">平成</option>
                                                        <option value="令和">令和</option>
                                                      </select>
													  <div class="input-group" style="width:120px">
                                                         <input type="text" class="form-control form-control-sm" id="full_name" name="full_name">
                                                         <div class="input-group-append">
                                                            <div class="input-group-text">年</div>
                                                          </div>
                                                       </div>
													   </div>
                                                    </div>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="form-group align-items-center row m-0 w-100 py-3">
                                             <label class="col-auto col-form-label" for="">就業場所</label>
                                             <div class="col-sm-3">
                                                <div class="input-group mb-2">
                                                   <input type="text" name="number_of_employees" class="form-control form-control-sm number_field" id="number_of_employees" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                                <label id="number_of_employees-error" class="form-error" for="number_of_employees"></label>
                                             </div>
                                             <label class="col-auto col-form-label" for="">資本金</label>
                                             <div class="col-sm-5">
                                                <textarea name="address" id="address" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                             </div>
                                          </div>

                                          <div class="form-group align-items-center row m-0 w-100 flex-nowrap py-3">
                                             
											 <div class="col-sm-6">
											 <label class="col-auto col-form-label" for="">うち女性</label>
												<div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="of_which_female" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                             <label class="col-auto col-form-label" for="">うちパート</label>
											 <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="of_which_part" name="of_which_part" placeholder="">
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
											 </div>
											 
											 <div class="col-sm-6">
											 <label class="col-lg-4 col-form-label" for="">労働組合</label>
                                             <div class="col-sm-12">
                                                <textarea name="address" id="address" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                             </div>
											 </div>
                                          </div>
                                          <div class="dropdown-divider my-0"></div>
                                       </div>

                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">事業内容</div>
                                       <div class="form-group row m-0 content-box w-100 py-3">
                                          <div class="col-sm-12">
                                             <textarea name="business_content" id="business_content" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">特徴　会社の</div>
                                       <div class="form-group row m-0 content-box w-100 py-3">
                                          <div class="col-sm-12">
                                             <textarea name="features_of_the_company" id="features_of_the_company" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>


                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-group row flex-column m-0 py-3 w-100">

                                          <div class="form-group row mx-0 m-0 w-100">
                                             <label class="col-sm-4 col-form-label" for="">役職／ 代表者名</label>
                                             <div class="col-sm-4 mb-3">
                                                <div class="row flex-column">
                                                   <input type="text" class="form-control form-control-sm mb-3" id="position_name" name="position_name" placeholder="">
                                                   <input type="text" class="form-control form-control-sm" id="representative_name" name="representative_name" placeholder="">
                                                </div>
                                             </div>
                                             <div class="col-lg-4">
                                                <input type="text" class="form-control form-control-sm" id="corporate_number" name="corporate_number" placeholder="">
                                             </div>
                                          </div>

                                          <div class="dropdown-divider my-0"></div>

                                          <div class="form-group row mx-0 mb-0 w-100 pt-3">
                                             <label class="col-sm-2 col-form-label" for="">就業規則</label>
                                             <div class="col-sm-5">
                                                <div class="row align-items-center m-0">
                                                   <label class="col-form-label mr-lg-3" for="">フルタイム</label>
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="full_time" value="上限あり"> 上限あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="full_time" value="なし"> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="full_time-error" class="form-error" for="full_time"></label>
                                                </div>
                                             </div>
                                             <div class="col-sm-5">
                                                <div class="row align-items-center m-0">
                                                   <label class="col-form-label mr-lg-3" for="">パートタイム</label>
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="part_time" value="上限あり"> 上限あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="part_time" value="なし"> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="part_time-error" class="form-error" for="part_time"></label>
                                                </div>
                                             </div>
                                       </div>

                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-group row flex-column m-0 py-3 w-100">

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">育児休業取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="achievements_of_taking_childcare_leave" value="あり"> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="achievements_of_taking_childcare_leave" value="なし"> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="achievements_of_taking_childcare_leave-error" class="form-error" for="achievements_of_taking_childcare_leave"></label>
                                          </div>
                                       </div>

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">介護休業取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_care_leave_acquisition_record" value="あり"> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_care_leave_acquisition_record" value="なし"> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="nursing_care_leave_acquisition_record-error" class="form-error" for="nursing_care_leave_acquisition_record"></label>
                                          </div>
                                       </div>

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">看護休暇取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_leave_acquisition_record" value="あり"> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_leave_acquisition_record" value="なし"> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="nursing_leave_acquisition_record-error" class="form-error" for="nursing_leave_acquisition_record"></label>
                                          </div>
                                       </div>

                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row m-0 py-3 w-100">
                                    <label class="col-sm-4 col-form-label" for="">外国人雇用実績</label>
                                    <div class="col-sm-8">
                                       <div class="kt-radio-inline">
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                          <input type="radio" name="foreign_employment_record" value="あり"> あり
                                          <span></span>
                                          </label>
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                          <input type="radio" name="foreign_employment_record" value="不問"> 不問
                                          <span></span>
                                          </label>
                                       </div>
                                       <label id="foreign_employment_record-error" class="form-error" for="foreign_employment_record"></label>
                                    </div>
                                 </div>
                              </div>


                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex flex-column">
                                 <label class="col-sm-12 col-form-label" for="">求人に関する特記事項</label>
                                 <div class="col-lg-12 pb-3">
                                    <textarea name="special_notes_on_job_vacancies" id="special_notes_on_job_vacancies" class="form-control form-control-sm" rows="6" placeholder="ここに入力してください"></textarea>
                                 </div>
                              </div>

                                 </div>
                         </div>
                     </div>








                     <div class="col-lg-4">
                        <div class="bd-example form_container">
                                 <div class="form-section position-static">
                                    <h5 for="staticEmail" class="col-sm-4 col-form-label">7 選考等</h5>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative pr-0">人数</div>
                                          <div class="position-relative pr-0">採用</div>
                                       </div>
                                       <div class="form-group row align-items-center content-box m-0 py-3 w-100">
                                          <div class="col-sm-4">
                                             <div class="input-group mb-2">
                                                <input type="text" class="form-control form-control-sm" id="number_of_people_recruitment" name="number_of_people_recruitment" placeholder="">
                                                <div class="input-group-append">
                                                   <div class="input-group-text">人</div>
                                                 </div>
                                             </div>
                                             <label id="number_of_people_recruitment-error" class="form-error" for="number_of_people_recruitment"></label>
                                          </div>

                                          <label class="col-lg-3 col-form-label" for="">募集理由</label>

                                          <div class="col-lg-5">
                                             <textarea name="reason_for_recruitment" id="reason_for_recruitment" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>

                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">方法選考</div>
                                       <div class="form-group row content-box m-0 py-3 w-100">
                                          <div class="row m-0 w-100">
                                             <div class="col-12">

                                                   <div class="row kt-checkbox-list row d-flex-wrap align-items-center radio-checkbox m-0 w-100">

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="面接"> 書類選考
                                                         <span></span>
                                                      </label>

                                                      <div class="col-auto kt-box mx-3 d-flex align-items-center">
                                                         <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                            <input type="checkbox" id="methodology_elective_exam_input" name="methodology_elective_exam[]" value="面接"> 面接
                                                            <span></span>
                                                         </label>

                                                         <label class="mr-3 mb-0" for="">予定</label>
                                                         <div class="col">
                                                            <div class="input-group" style="width:100px">
                                                               <input disabled type="text" class="form-control form-control-sm" id="schedule" name="schedule" placeholder="">
                                                               <div class="input-group-append">
                                                                  <div class="input-group-text">回</div>
                                                               </div>
                                                            </div>
                                                            <label id="schedule-error" class="form-error" for="schedule"></label>
                                                         </div>
                                                      </div>

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="筆記試験"> 筆記試験
                                                         <span></span>
                                                      </label>

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="その他"> その他
                                                         <span></span>
                                                      </label>

                                                  </div>

                                                  <label id="methodology_elective_exam[]-error" class="form-error" for="methodology_elective_exam[]"></label>

                                             </div>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative">結果</div>
                                          <div class="position-relative pr-0">通知</div>
                                       </div>

                                       <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                          <label class="col-form-label col-12" for=""></label>
                                          <div class="col-lg-12">

                                             <div class="kt-checkbox-list row align-items-center">
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="即決"> 即決
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="書類選考結果通知"> 書類選考結果通知
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                <input type="checkbox" name="result_announcement[]" value="面接選考結果通知"> 面接選考結果通知
                                                <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="面接選考結果通知"> 面接選考結果通知
                                                   <span></span>
                                                </label>

                                                <div class="mr-3 d-flex align-items-center">
                                                   <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                      <input type="checkbox" id="after_the_book_toggle" name="result_announcement[]" value="書類到着後"> 書類到着後
                                                      <span></span>
                                                   </label>
                                                   <div class="input-group d-inline-flex" style="width:120px">
                                                      <input disabled type="text" class="form-control form-control-sm" id="after_the_book" name="after_the_book" placeholder="0">
                                                      <div class="input-group-append">
                                                         <div class="input-group-text">日以内</div>
                                                       </div>
                                                   </div>
                                                </div>

                                                <div class="mr-3 d-flex align-items-center">
                                                   <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                      <input type="checkbox" id="after_the_face_toggle" name="result_announcement[]" value="面接後"> 面接後
                                                      <span></span>
                                                   </label>
                                                   <div class="input-group d-inline-flex" style="width:120px">
                                                      <input disabled type="text" class="form-control form-control-sm" id="after_the_face" name="after_the_face" placeholder="0">
                                                      <div class="input-group-append">
                                                         <div class="input-group-text">日以内</div>
                                                       </div>
                                                   </div>
                                                </div>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="その他"> その他
                                                   <span></span>
                                                </label>

                                            </div>

                                            <label id="result_announcement[]-error" class="form-error" for="result_announcement[]"></label>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative">方法</div>
                                          <div class="position-relative pr-0">通知</div>
                                       </div>

                                       <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                          <div class="col-lg-12">


                                             <div class="kt-checkbox-list row align-items-center">
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="求職者マイページに連絡"> 求職者マイページに連絡
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="郵送"> 郵送
                                                   <span></span>
                                                </label>
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                <input type="checkbox" name="method_notification[]" value="電話"> 電話
                                                <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="Eメール"> Eメール
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="その他"> その他
                                                   <span></span>
                                                </label>

                                            </div>

                                            <label id="method_notification[]-error" class="form-error" for="method_notification[]"></label>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">日時</div>
                                       <div class="form-group row content-box m-0 py-3 w-100">
                                          <label class="col-sm-4 col-form-label" for="">随時</label>
                                          <div class="col-lg-8">
                                             <textarea name="as_needed" id="as_needed" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                          </div>
                                       </div>

                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">選考場所</div>
                                       <div class="form-group row m-0 pt-3 w-100 content-box">
                                          <div class="col-lg-12 pt-3">
                                             <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                   <div class="input-group-text">〒</div>
                                                 </div>
                                                <input type="text" class="form-control form-control-sm number_field" id="selection_place_code" name="selection_place_code" placeholder="">
                                              </div>
                                          </div>

                                          <div class="col-lg-12 pt-3">
                                             <input type="text" class="form-control form-control-sm" id="selection_place" name="selection_place" placeholder="">
                                          </div>

                                          <div class="col-lg-12 pt-3">
                                             <div class="row">
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">線</label>
                                                      <div class="col-lg-12"><input type="text" class="form-control form-control-sm" id="string_1" name="string_1" placeholder=""></div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">駅</label>
                                                      <div class="col-lg-12"><input type="text" class="form-control form-control-sm" id="string_2" name="string_2" placeholder=""></div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">徒歩</label>
                                                      <div class="col-lg-12">
                                                         <div class="input-group mb-2">
                                                            <input type="text" class="form-control form-control-sm" id="on_foot" name="on_foot" placeholder="">
                                                            <div class="input-group-append">
                                                               <div class="input-group-text">分</div>
                                                             </div>
                                                         </div>
                                                         <label id="on_foot-error" class="form-error" for="on_foot"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">応募書類等</div>
                                       <div class="form-group row flex-column content-box m-0 w-100">
                                          <div class="col-lg-12">
                                             <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                   <div class="input-group-text">〒</div>
                                                 </div>
                                                <input type="text" class="form-control form-control-sm" id="documents" name="documents" placeholder="">
                                             </div>
                                             <label id="documents-error" class="form-error" for="documents"></label>
                                          </div>
                                       </div>
                                    </div>

                                 <div class="dropdown-divider"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">担当者</div>
                                    <div class="form-group row flex-column content-box m-0 w-100">

                                       <label class="col-form-label col-12" for="">フリガナ</label>

                                       <div class="col-sm-12">
                                          <input type="text" class="form-control form-control-sm" id="name_furigana" name="name_furigana" placeholder="氏名">
                                       </div>

                                       <div class="col-12 pt-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">電話番号</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="telephone_number" name="telephone_number" placeholder="">
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 pt-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">FAX</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="fax" name="fax" placeholder="">
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 pt-3 pb-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">Eメール</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="email" name="email" placeholder="">
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 pt-3">
                                          <textarea name="contact_address" id="contact_address" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください"></textarea>
                                       </div>

                                 </div>
                              </div>

                                 </div>
                         </div>
                     </div>

                  </div>
	</div>




</body>
</html>