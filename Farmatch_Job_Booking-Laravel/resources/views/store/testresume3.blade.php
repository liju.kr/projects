<!doctype html>
<html lang="en">
   <head>
      <style>
         table, th, td {
         border: 1px solid black;
         border-collapse: collapse;
         }
         th, td {
         padding: 12px;
         }
         table{ margin-bottom: 30px; }
         @font-face{
         font-family: ipag;
         font-style: normal;
         font-weight: normal;
         src:url('{{ storage_path('fonts/ipag.ttf') }}');
         }
         body, th {
            font-family: ipag;
         }
      </style>
      <title>Resume</title>
   </head>
   <body>
    @php
        $dob = $chemist->dob;
        $year = "";
        $month = "";
        $day = "";
        $age = "";
        if($dob){
            $dob = explode('-',$dob);
            $year = $dob[0];
            $month = $dob[1];
            $day = $dob[2];
            $age = \Carbon\Carbon::parse($chemist->dob)->age;
        }
    @endphp
      <table id="top_left" style="width: 580px; border:none;float: :left;" cellpadding="0" cellspacing="0">
         <thead>
            <tr>

               <td border="0" colspan="3" align="left" style="border:0;"><font color="#000" size="1">履 歴 書</font></td>
               {{-- <td align="right" style="border:0;"><font color="#000" size="2"></font></td>
               <td align="right" style="border:0;"><font color="#000" size="2" style="float: right;"></font></td> --}}
               <td align="right" colspan="3" style="border:0;"><font color="#000" size="2">{{date('Y')}}年 {{date('m')}}月    {{date('d')}}日現在</font></td>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td><font color="#000" size="1">ふりがな </font></td>
               <td colspan="5"> <font color="#000" size="2">{{$chemist->last_furigana}} {{$chemist->first_furigana}}</font></td>
            </tr>
            <tr>
               <td><font color="#000" size="1">氏 名 </font></td>
               <td colspan="5"><font color="#000" size="2"> {{$chemist->full_name}}</font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2"> 昭和・平成 </font></td>
               <td align="right"><font color="#000" size="2">年</font></td>
               <td><font color="#000" size="2">{{$month}} 月 </font></td>
               <td><font color="#000" size="2">{{$day}} 日生</font> </td>
               <td><font color="#000" size="2">満({{$age}}歳)</font></td>
               <td><font color="#000" size="2">@if($chemist->gender == "Male")男 @else 女 @endif </font></td>
            </tr>
         </tbody>
      </table>
      <table id="top_right" style="border: 0px;float: right;">
         <tr>
            <td style="padding:0;border: 0px" >
                @if($chemist->image)
                    <img src="images/chemist/{{ $chemist->image }}" style="margin-top: 50px;border: 0px" width="100" height="100">
                @endif
            </td>
         </tr>
      </table>

      <div style="height: 200px"></div>
      <table id="top_left" style="width:70%;float: left;" cellpadding="0" cellspacing="0"  >
         <tbody>
            <tr>
               <td><font color="#000" size="2">ふりがな {{$chemist->furikana}} </font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2">  〒 {{$chemist->post_code}} </font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2"> 現住所 @if($chemist->prefecture){{$chemist->prefecture}}@endif @if($chemist->city){{$chemist->city}}@endif {{$chemist->town}} </font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2">Eメール {{$chemist->email}}</font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2">(現住所以外に連絡を希望する場合のみ記入) </font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2">ふりがな </font></td>
            </tr>
            <tr>
               <td><font color="#000" size="2">連絡先  〒 </font></td>
            </tr>
         </tbody>
      </table>
        <table id="top_left" style="width:28%; 1px solid black;" align="right">
            <tbody>
                <tr>
                <td><font color="#000" size="2">自宅電話 </font></td>
                </tr>
                <tr>
                <td><font color="#000" size="2"> 携帯電話 {{$chemist->phone}} </font></td>
                </tr>
                <tr>
                <td><font color="#000" size="2"> 連絡先電話 </font></td>
                </tr>
            </tbody>
        </table>
        <div style="height: 330px"></div>
      <table style="width:100%;" cellpadding="0" cellspacing="0">
         <tr>
            <td bgcolor="#ccc"><font color="#000" size="2">年</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">月</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">学歴・職歴(各別にまとめて書く)</font></td>
         </tr>
         @foreach ($chemist->education as $education)
         <tr>
            <td><font color="#000" size="2">{{$education->year}}</font></td>
            <td><font color="#000" size="2">{{$education->month}}</font></td>
            <td><font color="#000" size="2">{{$education->content}}</font></td>
         </tr>
         @endforeach
         @if(count($chemist->education) == 0)
         <tr>
            <td></td>
            <td></td>
            <td></td>
         </tr>
         @endif
      </table>
      <table style="width:100%;"  cellpadding="0" cellspacing="0">
         <tr>
            <td bgcolor="#ccc"><font color="#000" size="2">年</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">月</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">薬剤師免許証登録番号</font></td>
         </tr>
         <tr>
            <td><font color="#000" size="2">{{$chemist->registration_year}}</font></td>
            <td><font color="#000" size="2">{{$chemist->registration_month}}</font></td>
            <td><font color="#000" size="2">{{$chemist->licence_number}}</font></td>
         </tr>
      </table>
      <table style="width:100%;" cellpadding="0" cellspacing="0">
         <tr>
            <td bgcolor="#ccc"><font color="#000" size="2">年</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">月</font></td>
            <td bgcolor="#ccc"><font color="#000" size="2">免許・資格</font></td>
         </tr>
         @foreach ($chemist->qualification as $qualification)
         <tr>
            <td><font color="#000" size="2">{{$qualification->year}}</font></td>
            <td><font color="#000" size="2">{{$qualification->month}}</font></td>
            <td><font color="#000" size="2">{{$qualification->content}}</font></td>
         </tr>
         @endforeach
         @if(count($chemist->qualification) == 0)
         <tr>
            <td></td>
            <td></td>
            <td></td>
         </tr>
         @endif
      </table>
      <table style="width:100%;page-break-inside: avoid;" cellpadding="0" cellspacing="0">
         <tr>
            <td rowspan="6" width="40%" valign="top">
                <font color="#000" size="2">志望動機・特技・好きな学科など</font>
               <p><font color="#000" size="2">{{$chemist->skills}}</font></p>
            </td>
            <td colspan="4"> <font color="#000" size="2">通勤時間 </font></td>
         </tr>
         <tr>
            <td colspan="2"><font color="#000" size="2">約 </font></td>
            <td align="right"><font color="#000" size="2">時間 </font></td>
            <td align="right"><font color="#000" size="2">分 </font></td>
         </tr>
         <tr>
            <td colspan="4"><font color="#000" size="2">最寄り駅 </font></td>
         </tr>
         <tr>
            <td colspan="3" align="right"><font color="#000" size="2">線 </font></td>
            <td align="right"><font color="#000" size="2">駅</font> </td>
         </tr>
         <tr>
            <td colspan="4">
               <font color="#000" size="2">
               扶養家族数(配偶者を除く)</font>
               <font color="#000" size="2" style="margin-left: 70px;">
                {{$chemist->no_of_dependents}} 人</font>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <font size="1">配偶者 {{$chemist->spouse}}</font>
               {{-- <font style="margin-top: 10px;margin-left: 70px" size="1">{{$chemist->spousal_support}}</font> --}}
            </td>
            <td>
               <font size="1">配偶者の扶養義務 </font>
               <font style="margin-top: 10px;" size="0">{{$chemist->spousal_support}}</font>
            </td>
         </tr>
      </table>
      <table style="width: 100%;">
         <tr>
            <td><font color="#000" size="2">本人希望記入欄(特に給料・職種・勤務時間・勤務地・その他について希望などがあれば記入) </font></td>
         </tr>
         <tr>
            <td><font color="#000" size="2">{{$chemist->desired_entry}}</font></td>
         </tr>
      </table>
   </body>
</html>
