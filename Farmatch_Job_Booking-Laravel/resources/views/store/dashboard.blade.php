@extends('layouts.store')
@section('page-content')
<div class="container container-wide container-full">
  <div class="row">
      <div class="col-lg-3 col-md-12">
          <h2 class="side-headding"> 本日の募集案件</h2>
          <div class="scroll_container">
          @foreach($jobPosts as $row)
          <div class="card card1 card-joblist">
              <h3> {{ $row->job_name }} </h3>
              <h6> {{ $row->job_title }} </h6>
              <p> <svg class="icon">
                  <use xlink:href="#calander"> </use>
                  </svg> {{ date('Y/m/d', strtotime($row->date))  }}
              </p>
              <p> {{ $row->content }}
              </p>
              <a href="{{ route('job-post-details', $row->id) }}" id=""> 詳細を見る</a>
          </div>
          @endforeach
      </div>
  </div>
  <div class="col-lg-9 col-md-12">
    <div class="tab__container">
      <!--begin::Portlet-->
      <div class="kt-portlet" id="kt_portlet">
        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
          </div>
          <div class="kt-portlet__head-toolbar mb20">
            <a href="#" class="btn btn-primary-default" data-toggle="modal" data-target="#addevent">
              新規投稿
            </a>
          </div>
        </div>
        <div class="kt-portlet__body">
          <div id="kt_calendar"></div>
        </div>
      </div>
      <!--end::Portlet-->
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade bd-example-modal-lg" id="addevent" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" data-dismiss="modal" class="close-header">
            <svg class="icon">
              <use xlink:href="#check-cross"> </use>
            </svg>
          </button>
          <h3 class="modal-title" id="addeventLabel"> 新規投稿 </h3>
          <form id="post-form">
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 募集タイトル </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="job_name" name="job_name">
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 日付 </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="kt_datepicker_1" name="dates" readonly placeholder="日付を選択" />
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 業務開始時間</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="kt_timepicker_1" name="work_from" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label">業務終了時間</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="kt_timepicker_1_modal" name="work_to" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 募集人数 </label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="no_of_vaccancies" name="no_of_vaccancies">
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 業務内容 </label>
              <div class="col-sm-9">
                <textarea class="form-control" name="content" id="content" name="content"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 注意事項 </label>
              <div class="col-sm-9">
                <textarea class="form-control" id="precautions" name="precautions"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-3 col-form-label"> 勤務時間調節 </label>
              <div class="col-sm-9">
                <select class="form-control" id="is_editable" name="is_editable">
                  <option value="Editable"> 希望時間に対応可能 </option>
                  <option value="Not Editable"> 希望時間に対応不可 </option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="exampleSelect1" class="col-sm-3 col-form-label" >理由</label>
              <div class="col-sm-9">
                <select class="form-control" id="reason" name="reason">
                  <option value="">オプションを選択</option>
                  <option value="Lack of Workers "> 人手不足 </option>
                  <option value="Busy Time"> 繁忙期 </option>
                  <option value="Absence of Chemist"> 職員の休暇 </option>
                  <option value="Others"> その他 </option>
                </select>
              </div>
            </div>
            <div class="form-group row" style="display: none;" id="reson_group">
              <label for="staticEmail" class="col-sm-3 col-form-label"> その他 </label>
              <div class="col-sm-9">
                <textarea class="form-control" id="others" name="others"></textarea>
              </div>
            </div>
            <div class="form-group row ">
              <label for="staticEmail" class="col-sm-3 col-form-label"></label>
              <div class="col-sm-9">
                <button class="btn btn-primary-default mr10" type="submit"> 投稿 </button>
                <button class="btn btn-secondary-default " type="reset" onclick="resetForm()"> クリア </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('script')
  <script>
    //  calenderValues   = [];
     var route        = '{{ route('add-store-job-post') }}';
    //  calenderValues   = '{!! $calenderArray !!}'
 </script>
  <!--begin::Calendar -->
  <script src="{{ asset('store/js/fullcalendar.bundle.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales/ja.js" type="text/javascript"></script>
  <script>
    "use strict";
    var KTCalendarBasic = function() {

        return {
            //main function to initiate the module
            init: function() {
                var todayDate = moment().startOf('day');
                var YM = todayDate.format('YYYY-MM');
                var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
                var TODAY = todayDate.format('YYYY-MM-DD');
                var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

                var calendarEl = document.getElementById('kt_calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

                    isRTL: KTUtil.isRTL(),
                    header: {
                        left: 'prev,next today',
                        center: '',
                        right: 'title '
                    },

                    height: 600,
                    contentHeight: 600,
                    aspectRatio: 3, // see: https://fullcalendar.io/docs/aspectRatio

                    nowIndicator: true,
                    now: TODAY + 'T09:25:00', // just for demo

                    defaultView: 'dayGridMonth',
                    defaultDate: TODAY,
                    locale: 'ja',
                    disableDragging: false,
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    navLinks: false,
                    events: {!! $calenderArray !!},
                    lang: 'ja',
                    eventRender: function(info) {
                        var element = $(info.el);
                        if (info.event.extendedProps && info.event.extendedProps.description) {
                            if (element.hasClass('fc-day-grid-event')) {
                                element.data('content', info.event.extendedProps.description);
                                element.data('placement', 'top');
                                KTApp.initPopover(element);
                            } else if (element.hasClass('fc-time-grid-event')) {
                                element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            } else if (element.find('.fc-list-item-title').lenght !== 0) {
                                element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            }
                        }
                    }
                });
                calendar.render();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTCalendarBasic.init();
    });
  </script>
  <!--end::Calendar -->
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
 <script src="{{ asset('store/js/custom/dashboard.js?ver4') }}" type="text/javascript"></script>
 <script src="{{ asset('store/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
 <script src="{{ asset('store/js/bootstrap-jp-datepicker.js') }}" type="text/javascript"></script>
 <script>
    $('#kt_datepicker_1, #kt_datepicker_1_validate').datepicker({
        todayHighlight: true,
        multidate: true,
        startDate: new Date(),
        format: 'yyyy-mm-dd',
        endDate: '+182d',
    });
 </script>
@endsection
