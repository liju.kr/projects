@extends('layouts.store')
@section('page-content')
<div class="container container-wide">
    @foreach($notifications as $row)
    <div class="notification-card">
      <h3 style="{{ ($row->message_from == 'Admin') ? "color:#e37a1e" : "" }}"> {{ $row->title }}</h3>
      <p> {{ $row->message }}</p>
      @if($row->reason && $row->accept_or_cancel=="Other")<p>理由:@if($row->reason=="Others") その他 : {{ $row->reason_other }} @else {{ $row->reason }} @endif</p>@endif
      <ul class="n-details">
        <li>
          <svg class="icon"> <use xlink:href="#calander"> </use> </svg> {{ date('Y-m-d', strtotime($row->created_at)) }}
        </li>
        <li>
          <svg class="icon"> <use xlink:href="#clock"> </use> </svg> {{ date('H:i:s', strtotime($row->created_at)) }}
        </li>
        @if($row->hyperlink == 'Yes')
        <li class="pull-right">
            <a href="@if($row->accept_or_cancel == 'Accept'){{ route('drug-store-accepted-application', ['postId' => $row->job_post_id, 'applicationId' => $row->job_application_id, 'notificationId' => $row->id ]) }}@elseif($row->accept_or_cancel == 'Cancel'){{ route('store-home-dashboard-cancel', [ 'application_id' => $row->job_application_id, 'notification_id' => $row->id ]) }} @else#@endif" style="color: {{ ($row->hyperlink_read_status == 'Not Read') ? "blue" : "gray" }}"><u>詳細を見る</u></a>
        </li>
        @endif
      </ul>
    </div>
    @endforeach
</div>
@endsection
