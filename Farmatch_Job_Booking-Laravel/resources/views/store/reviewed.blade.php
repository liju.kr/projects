@extends('layouts.store')
@section('page-content')
         <div class="container container-wide">
            <div class="job-details-card">
               <div class="user-listing mb30">
                 <h5 class="text-right">
                    募集⼈数 2 ⼈
                 </h5>
                  募集タイトル
                  <h4> 薬剤師経験3年以上の薬剤師さんの募集です。</h4>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="card card1">
                        <div class="media">
                           <img class="align-self-start" src="{{ asset('store/images/profile.jpg')}}" alt="" data-toggle="modal"
                              data-target="#image-pop">
                           <div class="media-body">
                              <p> 野中　牧 </p>
                              <p class="sm split"> <span>  勤務希望日 :  </span> <b>2020 - 05 - 21 </b></p>
                              <p class="sm split"> <span>  勤務希望時間 :   </span> <b> 10:00 AM - 06:00 PM </b></p>
                              <p class="sm split"> <span>  時給 :　　 </span> <b> 12.00 円 </b></p>
                              <p class="sm split"> <span>  交通費 :　  </span> <b> 254.00 円 </b></p>
                              <p class="sm split"> <span>  休憩時間 : </span> <b> 1:00 時間 </b></p>
                              <div class="btn-cover">
                                 <div class="btn btn-primary-default btn-round mr10">
                                    Cancel
                                 </div>
                                 <div class="btn btn-desable-default btn-round">
                                    Reviewed
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="card card1">
                        <div class="media">
                           <img class="align-self-start" src="{{ asset('store/images/profile.jpg')}}" alt="" data-toggle="modal"
                              data-target="#image-pop">
                           <div class="media-body">
                              <p> 森永由美子 </p>
                              <p class="sm split"> <span>  勤務希望日 :  </span> <b>2020 - 05 - 21 </b></p>
                              <p class="sm split"> <span>  勤務希望時間 :  </span> <b> 10:00 AM - 06:00 PM </b></p>
                              <p class="sm split"> <span>  時給 :　　  </span> <b> 12.00 円</b></p>
                              <p class="sm split"> <span>  交通費 :　  </span> <b> 254.00 円 </b></p>
                              <p class="sm split"> <span>  休憩時間 :  </span> <b> 1:00 時間 </b></p>
                              <div class="btn-cover">
                                 <div class="btn btn-primary-default btn-round mr10">
                                    Cancel
                                 </div>
                                 <div class="btn btn-desable-default btn-round">
                                    Reviewed
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="card card1">
                        <div class="media">
                           <img class="align-self-start" src="{{ asset('store/images/profile.jpg')}}" alt="" data-toggle="modal"
                              data-target="#image-pop">
                           <div class="media-body">
                              <p> 今田屋 耕一郎 </p>
                              <p class="sm split"> <span>  勤務希望日 :  </span> <b>2020 - 05 - 21 </b></p>
                              <p class="sm split"> <span>  勤務希望時間 :  </span> <b> 10:00 AM - 06:00 PM </b></p>
                              <p class="sm split"> <span>  時給 :　　  </span> <b> 12.00 円</b></p>
                              <p class="sm split"> <span>  交通費 :　  </span> <b> 254.00 円 </b></p>
                              <p class="sm split"> <span>  休憩時間 : </span> <b> 1:00 時間 </b></p>
                              <div class="btn-cover">
                                 <div class="btn btn-primary-default btn-round mr10">
                                    Cancel
                                 </div>
                                 <div class="btn btn-desable-default btn-round">
                                    Reviewed
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="card card1">
                        <div class="media">
                           <img class="align-self-start" src="{{ asset('store/images/profile.jpg')}}" alt="" data-toggle="modal"
                              data-target="#image-pop">
                           <div class="media-body">
                              <p> 佐藤　里華 </p>
                              <p class="sm split"> <span>  勤務希望日 :  </span> <b>2020 - 05 - 21 </b></p>
                              <p class="sm split"> <span>  勤務希望時間 :  </span> <b> 10:00 AM - 06:00 PM </b></p>
                              <p class="sm split"> <span>  時給 :　　  </span> <b> 12.00 円</b></p>
                              <p class="sm split"> <span>  交通費 :　  </span> <b> ¥ 254.00 円 </b></p>
                              <p class="sm split"> <span>  休憩時間 : </span> <b> 1:00 時間 </b></p>
                              <div class="btn-cover">
                                 <div class="btn btn-primary-default btn-round mr10">
                                    Cancel
                                 </div>
                                 <div class="btn btn-desable-default btn-round">
                                    Reviewed
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade bd-example-modal-lg" id="addevent" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <button type="button" data-dismiss="modal" class="close-header">
                     <svg class="icon">
                        <use xlink:href="#check-cross"> </use>
                     </svg>
                  </button>
                  <h3 class="modal-title mb30" id="addeventLabel"> Add New Job </h3>
                  <form>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Job Name </label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="inputPassword">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Date </label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Select date" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Job Title </label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="inputPassword">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Work Time from </label>
                        <div class="col-sm-9">
                           <input class="form-control" id="kt_timepicker_1" placeholder="Select time" type="text" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Work Time to </label>
                        <div class="col-sm-9">
                           <input class="form-control" id="kt_timepicker_1" placeholder="Select time" type="text" />
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Number of Vacancies </label>
                        <div class="col-sm-9">
                           <input type="number" class="form-control" id="inputPassword">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Work Cntents </label>
                        <div class="col-sm-9">
                           <textarea id="my-textarea" class="form-control" name="" rows="5" spellcheck="false"></textarea>
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label"> Precautions </label>
                        <div class="col-sm-9">
                           <textarea id="my-textarea" class="form-control" name="" rows="5" spellcheck="false"></textarea>
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="exampleSelect1" class="col-sm-3 col-form-label" >Reason for Job Post</label>
                        <div class="col-sm-9">
                           <select class="form-control" id="exampleSelect1">
                              <option>Select Option</option>
                              <option> Luck of Workers </option>
                              <option> Busy Time </option>
                              <option> Absence of Chemist </option>
                              <option> Others </option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group row ">
                        <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-9">
                           <button class="btn btn-primary-default mr10" type="button"> Post jobs </button>
                           <button class="btn btn-secondary-default " type="button"> Clear </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- -------------- -->
      <div class="modal fade bd-example-modal-lg" id="image-pop" tabindex="-1" role="dialog" aria-labelledby="image-popLabel" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <button type="button" data-dismiss="modal" class="close-header">
                     <svg class="icon">
                        <use xlink:href="#check-cross"> </use>
                     </svg>
                  </button>
                  <!-- <h3 class="modal-title" id="image-popLabel"> New Job </h3> -->
                  <div class="media mb30">
                     <img class="br9" src="{{ asset('store/images/profile.jpg') }}" alt="" data-toggle="modal" data-target="#image-pop">
                     <div class="media-body">
                        <ul class="user-listing normal small_sec">
                           <li> 名前 : <span> 野中　牧 </span></li>
                           <li> 性別 : <span> 女性 </span></li>
                           <li> 年齢 : <span> 30代 </span></li>
                        </ul>
                     </div>
                  </div>

                  <div class="user-listing">
                           自己紹介文
                  </div>
                  <p> 現在無職で子育て中です。
                     いますぐ働くわけではないが、今後検討したい
                  </p>
                  <div class="row">
                     <div class="col-12">
                        <div class="user-listing">
                           住所
                           <span> 大分市三芳1214-1 </span>
                        </div>
                     </div>
                     <div class="col-md-auto">
                        <div class="user-listing">
                           返答時間目安
                           <span> 2:00  時間 </span>
                        </div>
                     </div>
                     <div class="col-md-auto">
                        <div class="user-listing">
                           資格
                           <span> なし </span>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="user-listing">
                           子供の有無
                           <span> 有 </span>
                        </div>
                     </div>
                  </div>
                  <div class="user-listing">
                     本人希望記入欄
                     <p> 希望勤務地は
                        大分市内, 別府市内, 中津市・宇佐市, 佐伯市・津久見市
                        希望時間は
                        9時から18時 </p>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-6">
                        <div class="user-listing">
                           ふぁーまっち利用年数
                           <span> 01 年 02 ヶ月</span>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="user-listing">
                           最低勤務時間
                           <span> 05 時間 </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="user-listing">
                           勤務回数
                           <span> 20 </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="user-listing">
                           累計職場回数
                           <span> 20 </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="user-listing">
                           リピート店舗回数
                           <span> 20 </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="user-listing">
                           直前キャンセル件数
                           <span> 20 </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- -------------- -->
      <div class="modal fade bd-example-modal-lg" id="review" tabindex="-1" role="dialog" aria-labelledby="reviewLabel"
         aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <button type="button" data-dismiss="modal" class="close-header">
                     <svg class="icon">
                        <use xlink:href="#check-cross"> </use>
                     </svg>
                  </button>
                  <h3 class="modal-title mb30" id="addeventLabel"> Review </h3>
                  <div class="user-listing">
                     Chemist Name
                     <h3> Chemist Name </h3>
                  </div>
                  <ul class="normal rating__grid">
                     <li>
                        <span> Attitude Towards Customers </span>
                        <div class="star__rating">
                           <form class="rating">
                              <label>
                              <input type="radio" name="stars" value="1" />
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="2" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="3" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="4" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="5" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                           </form>
                        </div>
                     </li>
                     <li>
                        <span> Adapting Towards other Workers </span>
                        <div class="star__rating">
                           <form class="rating">
                              <label>
                              <input type="radio" name="stars" value="1" />
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="2" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="3" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="4" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="5" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                           </form>
                        </div>
                     </li>
                     <li>
                        <span> Work Speed </span>
                        <div class="star__rating">
                           <form class="rating">
                              <label>
                              <input type="radio" name="stars" value="1" />
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="2" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="3" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="4" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="5" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                           </form>
                        </div>
                     </li>
                     <li>
                        <span> Value for Money </span>
                        <div class="star__rating">
                           <form class="rating">
                              <label>
                              <input type="radio" name="stars" value="1" />
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="2" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="3" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="4" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                              <label>
                              <input type="radio" name="stars" value="5" />
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              <span class="icon">★</span>
                              </label>
                           </form>
                        </div>
                     </li>
                  </ul>
                  <div class="form-group">
                     <label for="exampleInputEmail1">Others </label>
                     <textarea id="my-textarea" class="form-control" name="" rows="10" spellcheck="false"></textarea>
                  </div>
                  <div class="button__row">
                     <button class="btn btn-primary-default mr10 " type="button"> Submit </button>
                     <button class="btn btn-secondary-default " type="button"> Clear </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
@endsection
