@extends('layouts.store')
@section('page-content')
         <div class="container container-wide">
            <h5 class="text-left">
            <a href="{{ route('store-home-dashboard-pending') }}" >
                <div class="btn btn-secondary-default btn-round btn-sm mt-3">
                <svg class="icon">
                    <use xlink:href="#Icon_feather-arrow-left"> </use>
                </svg>
                承認待ちの案件
                </div>
            </a>
            </h5>
            <div class="job-details-card">
                @if($isCancel)
                    <input class="form-control" type="hidden" name="post_id" id="post_id" value="{{ $postId }}" style="display: none;">
                    <div class="text-right">
                        <button class="btn btn-primary-default register-click" type="submit" onclick="cancelJobPost()" style="
                 position: relative;
                 top: -20px;
                 right: -20px;
                 border-radius: 200px;
                 background: #F44336;
                 border-color: #F44336;
                 box-shadow: 0px 3px 6px rgba(244, 67, 54, 0.48);
                "> 取り下げる </button>
                    </div>
                @endif
               <div class="user-listing mb30">
                  募集タイトル
                  <h4> {{ $jobName }}</h4><br>
                   <h5>
                       募集⼈数 {{ $vaccancies }} ⼈
                   </h5>
               </div>
               <div class="row">
                @if($pendingApplications->count() > 0)
                @foreach($pendingApplications as $row)
                <div class="col-md-4">
                   <div class="card card1">
                      <div class="media">
                         <img class="align-self-start image-popup" data-id="{{ $row->chemist_id }}" src="{{ ($row->image) ? asset('images/chemist/'.$row->image) : asset('store/images/profile.jpg') }}" alt="">
                         <div class="media-body">
                            <p> {{ $row->last_name }} {{ $row->first_name }}</p>
                            <p class="sm split-xl"> <span> 性別  : </span> <b>{{ ($row->gender == 'Male' ) ? "男性" : "女性" }}</b> </p>
                            <p class="sm split-xl"> <span> 勤務希望日 :</span> <b>{{ $row->date }} </b></p>
                            <p class="sm split-xl"> <span> 希望勤務時間 : </span> <b> {{ date("H:i",strtotime($row->time_from)) }}  - {{ date("H:i",strtotime($row->time_to)) }}  </b></p>
                            <p class="sm split-xl"> <span> 休憩時間  : </span> <b> {{ $row->rest_time }} 分 </b></p>
                            {{-- <p class="sm split-xl"> <span> 時給 :</span> <b> {{ $row->hourly_wage }}  円</b></p>
                            <p class="sm split-xl"> <span> 交通費 : </span> <b> {{ $row->transportation_cost }} 円</b></p> --}}
                         </div>
                      </div>
                      <hr>
                      <div class="btn-cover btn_cover___three text-center">
                         <a href="{{route('chemist-chat')}}?chemist={{$row->chemist_id}}">
                            <div class="btn btn-secondary-default btn-round btn-sm mr3">
                               <svg class="icon">
                                  <use xlink:href="#chat-icon"> </use>
                               </svg>
                               チャット
                            </div>
                         </a>
                         <div class="btn btn-primary-default btn-round btn-sm mr3 accept-application" data-id="{{ $row->id }}">
                            <svg class="icon">
                               <use xlink:href="#check"> </use>
                            </svg>
                            承認
                         </div>
                         <div class="btn btn-red-default btn-round btn-sm mr3 reject-application" data-id="{{ $row->id }}">
                            <svg class="icon">
                               <use xlink:href="#check-cross"> </use>
                            </svg>
                            拒否
                         </div>
                      </div>
                   </div>
                </div>
                @endforeach
                @else
                   <p class="text-center" style="width: 100%;color: red">表示するデータが現在ありません</p>
                @endif
             </div>
            </div>
         </div>
      </div>
      <!-- -------------- -->
      <div class="modal fade bd-example-modal-lg" id="image-pop" tabindex="-1" role="dialog" aria-labelledby="image-popLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
           <div class="modal-content">
              <div class="modal-body">
                 <button type="button" data-dismiss="modal" class="close-header">
                    <svg class="icon">
                       <use xlink:href="#check-cross"> </use>
                    </svg>
                 </button>
                 <div class="media mb30">
                    <img class="br9" id="popup_image" src="{{ asset('store/images/profile.jpg') }}" alt="" data-toggle="modal" data-target="#image-pop">
                    <div class="media-body">
                       <ul class="user-listing normal small_sec">
                          <li> 名前 : <span id="popup_name"> たなか みこ </span></li>
                          <li> 性別 : <span id="popup_gender"> 女性 </span></li>
                          <li> 年齢 : <span id="popup_age"> 30代 </span></li>
                       </ul>
                    </div>
                 </div>
                 <div class="user-listing">
                          自己紹介文
                 </div>
                 <p id="popup-introduction">
                 </p>
                 <div class="row">
                    <div class="col-12">
                       <div class="user-listing">
                          住所
                          <span id="popup-address"></span>
                       </div>
                    </div>
                    <div class="col-md-auto">
                       <div class="user-listing">
                          返答時間目安
                          <span id="popup-response"> </span>
                       </div>
                    </div>
                    <div class="col-md-auto">
                       <div class="user-listing">
                          資格
                          <span id="popup-qualification"> </span>
                       </div>
                    </div>
                    <div class="col-12">
                       <div class="user-listing">
                          子供の有無
                          <span id="popup-child">  </span>
                       </div>
                    </div>
                 </div>
                 <div class="user-listing">
                    本人希望記入欄
                    <p id="popup-skills"> </p>
                 </div>
                 <hr>
                 <div class="row">
                    <div class="col-6">
                       <div class="user-listing">
                          ふぁーまっち利用年数
                          <span id="popup-experience"></span>
                       </div>
                    </div>
                    <div class="col-6">
                       <div class="user-listing">
                          最低勤務時間
                          <span> 05 時間 </span>
                       </div>
                    </div>
                    <div class="col-6">
                       <div class="user-listing">
                          勤務回数
                          <span id="popup-completed"> 20 </span>
                       </div>
                    </div>
                    <div class="col-6">
                       <div class="user-listing">
                          累計職場回数
                          <span id="popup-no-of-drug-stores">  </span>
                       </div>
                    </div>
                    <div class="col-6">
                       <div class="user-listing">
                          リピート店舗回数
                          <span id="popup-repeated">  </span>
                       </div>
                    </div>
                    <div class="col-6">
                       <div class="user-listing">
                          直前キャンセル件数
                          <span id="sudden_cancellation"> </span>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     <div class="modal fade bd-example-modal-lg" id="add-rejection" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" data-dismiss="modal" class="close-header">
                <svg class="icon">
                  <use xlink:href="#check-cross"> </use>
                </svg>
              </button>
              <h3 class="modal-title" id="addeventLabel" style="margin-bottom:10px;"> この申請を受け付けない理由を選択</h3>
              <form id="post-form">
                  <div class="form-group row">
                      <label for="staticEmail" class="col-sm-3 col-form-label"> 理由： <span style="color: red">*</span></label>
                      <div class="col-sm-9">
                          <select name="reject_reason" id="reject_reason" class="form-control">
                              <option value="">理由を選択</option>
                              <option value="既存の職員が勤務できるようになったため">既存の職員が勤務できるようになったため</option>
                              <option value="Others">その他</option>
                          </select>
                      </div>
                  </div>
                <div class="form-group row" id="other_content" style="display: none;">
                  <label for="staticEmail" class="col-sm-3 col-form-label"> その他： <span style="color: red">*</span> </label>
                  <div class="col-sm-9">
                    <textarea class="form-control" id="reject_reason_other" name="reject_reason_other_1"></textarea>
                    <input type="text" id="reject_application_id" value="" style="display: none;">
                  </div>
                </div>
                <div class="form-group row ">
                  <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-9">
                    <button class="btn btn-primary-default mr10 reject-application-reason" type="button"> 提出 </button>
                    <button class="btn btn-secondary-default" data-dismiss="modal"> キャンセル </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('script')
<script>
   var getChemistInforamtion     = '{{ route('get-chemist-information') }}';
   var rejectChemistApplication  = '{{ route('reject-chemist-application') }}';
   var acceptJobApplication      = '{{ route('accept-job-application') }}';
   var cancelPost  = '{{ route('cancel-job-post') }}';
</script>
<script src="{{ asset('store/js/custom/settings.js?ver1') }}" type="text/javascript"></script>
<script>
    function cancelJobPost() {

        var postId = $("#post_id").val();
        Swal.fire({
            title: 'キャンセルしますか？',
            html: '<br><p>■キャンセルポリシー</p>'+
                '<p>当日~7日前までのキャンセルについては、以下のキャンセル料が発生します。</p><br>'+

                '<h6>○ 7日前〜4日前まで：勤務要請の承認された額の５０％</h6><br>'+

                '<h6>○ 3日前〜当日まで：勤務要請の承認された額の１００％</h6><br>'+

                '<p>※ 運営会社側からの掲載料・手数料の請求時に合わせてご請求させていただきます。</p>'+
                '<p>※振込手数料は、勤務要請した薬局・病院様のご負担になります。</p>'+
                '<p>※薬剤師への振り込みは、振込手数料を差し引いた額でお振込いたします。</p>'+


                '<p>キャンセル料は、勤務確定済みの薬剤師がすでにいる場合にのみ発生します。</p>',
            icon: '',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '取り下げる',
            cancelButtonText: 'キャンセル'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: cancelPost,
                    data: {
                        postId: postId
                    },
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        $(".btn-primary-default").text("キャンセル..");
                    },
                    success: function(data) {
                        if (data.status == true) {
                            $(".btn-primary-default").hide();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 3000
                            })
                            postForm.resetForm();
                        } else {
                            //$(".btn-primary").text("Save");
                            if (data.message) {
                                Swal.fire({
                                    icon: 'error',
                                    title: '',
                                    text: data.message,
                                })
                            }
                        }
                    },
                    error: function() {}
                });
                return false;

            } else if (result.dismiss === 'cancel') {
                // swal.fire(
                //     'Cancelled',
                //     'Canclelled the action',
                //     'error'
                // )
            }
        })
    }
</script>



@endsection
