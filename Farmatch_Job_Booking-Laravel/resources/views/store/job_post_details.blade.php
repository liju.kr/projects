@extends('layouts.store')
@section('page-content')
<style>
    .swal2-content{
        text-align: left;
    }
</style>
<div class="container">
    <div class="account_container">
        <div class="inner-card">
            @if($isCancel)
            <div class="text-right">
                <button class="btn btn-primary-default register-click" type="submit" onclick="cancelJobPost()" style="
                 position: relative;
                 top: -43px;
                 right: -63px;
                 border-radius: 200px;
                 background: #F44336;
                 border-color: #F44336;
                 box-shadow: 0px 3px 6px rgba(244, 67, 54, 0.48);
                "> 取り下げる </button>
            </div>
            @endif
            <h4 class="text-center"> 募集案件詳細 </h4>
            <form id="job-detail-form" method="" action="" class="text-center form-top-pad">
                <div class="form-group text-center">
                    <label>募集タイトル</label>
                    <input class="form-control" type="text" name="post_id" id="post_id" value="{{ $postDetails->id }}" style="display: none;">
                    <input class="form-control" type="text" name="job_name" placeholder="" value="{{ $postDetails->job_name }}" >
                </div>
                <div class="form-group text-center">
                    <label>日付</label>
                    <input class="form-control" type="text" name="dates" placeholder="" value="{{ date('Y/m/d', strtotime($postDetails->date)) }}" readonly style="background-color: #e0e0e0;">
                </div>
                <div class="form-group text-center">
                    <label>業務開始時間</label>
                    <input class="form-control" type="text" name="work_from" placeholder="" value="{{ date('H:i', strtotime($postDetails->work_from)) }}" readonly style="background-color: #e0e0e0;">
                </div>
                <div class="form-group text-center">
                    <label>業務終了時間</label>
                    <input class="form-control" type="text" name="work_to" placeholder="" value="{{ date('H:i', strtotime($postDetails->work_to)) }}" readonly style="background-color: #e0e0e0;">
                </div>
                <div class="form-group text-center">
                    <label>募集人数</label>
                    <input class="form-control" type="text" name="no_of_vaccancies" placeholder="" value="{{ $postDetails->no_of_vaccancies }}">
                </div>
                <div class="form-group text-center">
                    <label>理由</label>
                    <select class="form-control" id="reason" name="reason" style="text-align-last: center;">
                        <option value="Lack of Workers" {{ ($postDetails->reason == 'Lack of Workers') ? "selected" : "" }}> 人手不足 </option>
                        <option value="Busy Time" {{ ($postDetails->reason == 'Busy Time') ? "selected" : "" }}> 繁忙期 </option>
                        <option value="Absence of Chemist" {{ ($postDetails->reason == 'Absence of Chemist') ? "selected" : "" }}> 職員の休暇 </option>
                        <option value="Others" {{ ($postDetails->reason == 'Others') ? "selected" : "" }}> その他 </option>
                    </select>
                </div>
                <div class="form-group text-center">
                    <label>その他</label>
                    <textarea class="form-control" name="others" rows="5" placeholder="" >{{ $postDetails->others }}</textarea>
                </div>
                <div class="form-group text-center">
                    <label>勤務時間調節</label>
                    <select class="form-control" id="reason" name="reason" style="text-align-last: center;background-color: #e0e0e0;" disabled>
                        <option value="Editable" {{ ($postDetails->is_editable == 'Editable') ? "selected" : "" }}> 希望時間に対応可能 </option>
                        <option value="Not Editable" {{ ($postDetails->is_editable == 'Not Editable') ? "selected" : "" }}> 希望時間に対応不可 </option>
                    </select>
                </div>
                <div class="form-group text-center">
                    <label>業務内容</label>
                    <textarea class="form-control" name="content" rows="5" placeholder="">{{ $postDetails->content }}</textarea>
                </div>
                <div class="form-group text-center">
                    <label>注意事項</label>
                    <textarea class="form-control" name="precautions" rows="5" placeholder="">{{ $postDetails->precautions }}</textarea>
                </div>
                <button class="btn btn-primary-default btn-block register-click" type="submit"> 更新 </button>
            </form>
        </div>
  </div>
</div>
@endsection
@section('script')
 <script>
     var route       = '{{ route('update-store-job-post') }}';
     var cancelPost  = '{{ route('cancel-job-post') }}';
 </script>
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
 <script src="{{ asset('store/js/custom/dashboard.js?ver2') }}" type="text/javascript"></script>
@endsection
