@extends('layouts.store')
@section('page-content')
<div class="container container-wide">
   <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
      <!-- begin:: Content -->
      <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid p0">
         {{-- <div class="fab" id="masterfab"><span>+</span></div> --}}
         <div id="snackbar">新着のメッセージ <a href="{{route('group-chat')}}">再読み込み</a></div>
         <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <form action="javascript:;" id="startNewChat">
                     <div class="modal-header">
                        <h5 class="modal-title">Start New Chat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <div class="col-md-12">
                           <input type="hidden" name="store" value="{{Auth::user()->id}}">
                           <select class="modal-chemist form-control" style="width: 300px;" name="chemist"></select>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Start Chat</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>

         <!--Begin::App-->
         <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Aside Mobile Toggle-->
            <button class="kt-app__aside-close" id="kt_chat_aside_close">
               <i class="la la-close"></i>
            </button>
            <!--End:: App Aside Mobile Toggle-->
            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit p20 bg-white" id="kt_chat_aside">
               <!--begin::Portlet-->
               <div class="kt-portlet kt-portlet--last boxshadow-none">
                  <div class="kt-portlet__body">
                     <div class="kt-searchbar">
                        <form action="">
                           <div class="input-group">
                              <div class="input-group-prepend">
                                 <span class="input-group-text" id="basic-addon1">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <rect x="0" y="0" width="24" height="24"></rect>
                                          <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                          <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                       </g>
                                    </svg>
                                 </span>
                              </div>
                              <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" name="search" value="{{$search}}">
                           </div>
                        </form>
                     </div>
                     <div class="kt-widget kt-widget--users kt-mt-20">
                        <div class="kt-scroll kt-scroll--pull ps ps--active-y" style="overflow: hidden;">
                           <div class="kt-widget__items">
                              @foreach ($chatheads as $head)
                              @php
                                 $count = \App\GroupChat::where('group_chat_head_id',$head->id)->where('store_read_status',0)->count();
                              @endphp
                              <div class="kt-widget__item" onclick="loadChat('{{$head->chemist_id}}')">
                                 <span class="kt-media kt-media--circle">
                                    @if($head->chemist && $head->chemist->image)
                                       <img src="{{asset('images/chemist/'.$head->chemist->image)}}" alt="image">
                                    @else
                                       <img src="{{ asset('store/images/profile.jpg') }}" alt="image">
                                    @endif
                                 </span>
                                 <span class="kt-media kt-media--circle ml-2">
                                    @php
                                       $admin_image = \App\User::first()->image;
                                       if($admin_image){
                                          $admin_image = url('/')."/images/profile/".$admin_image;
                                       }else{
                                          $admin_image = url('/')."/store/images/profile.jpg";
                                       }
                                    @endphp
                                    <img src="{{$admin_image}}" alt="image">
                                 </span>
                                 <div class="kt-widget__info">
                                    <div class="kt-widget__section">
                                       <a href="javascript:;" class="kt-widget__username">{{$head->chemist->full_name}} - 管理者</a>
                                       <!-- <span class="kt-badge kt-badge--success kt-badge--dot"></span> -->
                                       <span class="message-count" id="message-count-{{$head->id}}">@if($count){{$count}}@endif</span>
                                    </div>
                                    <span class="kt-widget__desc">
                                       {{$head->last_message}}
                                    </span>
                                 </div>
                                 <div class="kt-widget__action">
                                    <span class="kt-widget__date" data-datetime="{{strtotime($head->updated_at)}}">{{\Carbon\Carbon::createFromTimeStamp(strtotime($head->updated_at))->locale('ja')->diffForHumans()}}</span>
                                    {{-- <span class="kt-badge kt-badge--success kt-font-bold">7</span> --}}
                                 </div>
                              </div>
                              @endforeach
                           </div>
                           <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                              <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                           </div>
                           <div class="ps__rail-y" style="top: 0px; height: 682px; right: -2px;">
                              <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 300px;"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end::Portlet-->
            </div>
            <!--End:: App Aside-->
            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content p20 bg-white" id="kt_chat_content" style="display:none;">
               <div class="kt-chat">
                  <div class="kt-portlet kt-portlet--head-lg kt-portlet--last boxshadow-none">
                     <div class="kt-portlet__head">
                        <div class="kt-chat__head ">
                           <div class="kt-chat__left">
                           </div>
                           <div class="kt-chat__center">
                              <div class="kt-chat__label">
                                 <a href="#" class="kt-chat__title">野中　牧
                                 </a>
                                 <span class="kt-chat__status">
                                    <span class="kt-badge kt-badge--dot kt-badge--success"></span> アクティブ
                                 </span>
                              </div>
                           </div>
                           <div class="kt-chat__right">
                           </div>
                        </div>
                     </div>
                     <div class="kt-portlet__body">
                        <div class="kt-scroll kt-scroll--pull ps ps--active-y" data-mobile-height="300" style="height: 504px;" id="kt-scroll">
                           <div class="kt-chat__messages"></div>
                        </div>
                     </div>
                     <div class="kt-portlet__foot">
                        <div class="kt-chat__input">
                           <div class="kt-chat__editor">
                              <textarea style="height: 30px" name="chat" id="chat" placeholder="メッセージを入力する …."></textarea>
                           </div>
                           <div class="kt-chat__toolbar">
                              <div class="kt_chat__tools">
                              </div>
                              <div class="kt_chat__actions">
                                 <button type="button" onclick="chatSubmit();" class="btn btn-brand btn-md btn-upper btn-bold">返信</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--End:: App Content-->
         </div>
         <!--End::App-->
      </div>
      <!-- end:: Content -->
   </div>
</div>
@include('layouts.fcm')
@endsection
@section('script')
<script>
   "use strict";

   $('.modal-chemist').select2({
      placeholder: "Select a chemist",
      // allowClear: true,
      theme: "bootstrap",
      ajax: {
         url: '{{route("pick-a-chemist")}}',
         dataType: 'json',
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(data) {
            return data
         }
      }
   });


   $('#startNewChat').on('submit', function() {
      $(this).find('button').attr('disabled', true);
      var data = $(this).serialize();
      $.ajax({
         url: '{{route("start-new-group-chat")}}',
         dataType: 'json',
         data,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(data) {
            if (data.status) {
               window.location.reload();
            }
            $(this).find('button').attr('disabled', false);
         }
      });
   });


   var placeholder = base_url + "/store/images/profile.jpg";
   var messageRef = {};

   var user_id = '{{auth()->user()->id}}';
   var chemist_id = '';
   var profile_image = '{{auth()->user()->main_image}}';
   var store_image = placeholder;

   if (profile_image) {
      store_image = base_url + "/images/drug_store/" + profile_image;;
   }

   var chemist_image = "";
   var chemistname = "";

   function loadChat(chemist) {
      if (chemist_id == chemist) {
         return false;
      }
      chemist_id = chemist;
      $('#kt_chat_content').show();
      $('.kt-chat__messages').html('');

      $.ajax({
         url: '{{route("get-chemist-details")}}',
         type: 'get',
         data: {
            id: chemist_id
         },
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(data) {

            $.ajax({
               url: '{{route("mark-read-group")}}',
               type: 'post',
               data: {
                  chemist: chemist_id
               },
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function(data) {
                  if(data.head){
                     $(`#message-count-${data.head}`).html("");
                  }
               }
            });

            $('.kt-chat__title').html(data.chemist.last_name +" "+data.chemist.first_name);
            var temp_image = profile_image;
            chemistname = data.chemist.last_name+" "+data.chemist.first_name;
            chemist_image = placeholder;
            if (data.chemist.image) {
               chemist_image = base_url + '/images/chemist/' + data.chemist.image;
            }

            messageRef = firebase.database().ref('drugstore').child(user_id).child(chemist_id);

            //ondatachange
            messageRef.on('child_added', function(childSnapshot) {
               var childKey = childSnapshot.key;
               var childData = childSnapshot.val();
               var right = ''
               var temp_image = placeholder;
               var user = '';
               if (childData.user == 2) {
                  right = 'kt-chat__message--right';
                  user = 'あなた';
                  temp_image = store_image;
               } else if (childData.user == 1) {
                  user = '管理者';
                  temp_image = admin_image;
               } else {
                  user = chemistname;
                  temp_image = chemist_image;
               }
               moment.locale('ja');
               var timestamp = moment.unix(childData.timestamp).fromNow();
               // console.log({childData});
               var html = `
                    <div class="kt-chat__message ${right}">
                        <div class="kt-chat__user">
                            <span class="kt-media kt-media--circle kt-media--sm">
                            <img src="${temp_image}" alt="image">
                            </span>
                            <a href="#" class="kt-chat__username">${user}</a>
                            <span class="kt-chat__datetime" data-datetime="${childData.timestamp}">${timestamp}</span>
                        </div>
                        <div class="kt-chat__text kt-bg-light-success">
                            ${childData.message}
                        </div>
                    </div>
                    `;
               $('.kt-chat__messages').append(html);
               var d = $('#kt-scroll');
               d.scrollTop(d.prop("scrollHeight"));
            });
         }
      })
   }

   var chemistid = '{{$chemist}}';
   if(chemistid){
      loadChat(chemistid);
   }

   setInterval(() => {
        $('.kt-widget__date').each(function(index, value) {
           var datetime = $(this).data('datetime');
           var timestamp = moment.unix(datetime).fromNow();
           $(this).html(timestamp);
        });
        $('.kt-chat__datetime').each(function(index, value) {
           var datetime = $(this).data('datetime');
           var timestamp = moment.unix(datetime).fromNow();
           $(this).html(timestamp);
        });
    }, 60000);

   function chatSubmit() {
      var chat = $('textarea[name="chat"]').val();
      if (chat.trim()) {
         messageRef.push({
            user: 2,
            message: chat,
            timestamp: Math.round((new Date()).getTime() / 1000)
         });
         $.ajax({
            url: '{{route("save-group-chat")}}',
            type: 'post',
            data: {
               message: chat,
               id: chemist_id
            },
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
               console.log(data);
            }
         })
         $('textarea[name="chat"]').val('');
      }
   }

   var listner2 = firebase.database().ref(`/drugstore/{{Auth::user()->id}}`);
   listner2.on('child_changed', function(childSnapshot) {
      showSnackMessage();
   });

   var chat = document.getElementById("chat");
   chat.addEventListener("keydown", function (e) {
      if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
         chatSubmit();
      }
   });
</script>
@endsection
