@extends('layouts.store')
@section('page-content')

<style>
   .form-error {
      margin-top: 5px;
      font-size: 13px;
      color: #ff5757!important;
   }
   .form-control.form-error {
      border-color: #ff5757!important;
   }

   /* .ip-box . */
</style>

   <div class="container-fluid container-wide">

      <form id="form" action="{{ route('save-employee-intro-service') }}" method="POST">

          {!! csrf_field() !!}

         <div class="p-5 bg-white rounded shadow mb-5 mt-3" id="tabs">

            <div id="myTab1Content" class="tab-content tab-validate">

               <div class="row">

                  <div class="col-lg-3">
                     <div class="bd-example form_container">
                              <div class="form-group d-flex pt-3">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 受付年月日 <span style="color: red">*</span> </label>
                                <div class="col-sm-8">
      {{--                          <input readonly type="date" class="form-control form-control-sm" id="datePicker">--}}
                                    <input readonly type="text" class="form-control form-control-sm" value="{{ date('Y-m-d') }}" name="date" id="date">
                                </div>
                              </div>

                              <div class="form-section position-static">
                                 <h5 for="staticEmail" class="col-sm-4 col-form-label">１ 求人事業所</h5>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">事務所名</div>
                                    <div class="form-group row m-0 pb-3 content-box">
                                       <!-- <label for="staticEmail" class="col-sm-12 col-form-label">フリガナ</label> -->

                                       <div class="col-sm-12">
                                       <input type="text" class="form-control form-control-sm" id="furigana" name="furigana"
                                              @if($employee_intro_service_1) value="{{ $employee_intro_service_1->furigana }}" @endif placeholder="フリガナ">
      <br>
                                       <input type="text" class="form-control form-control-sm" id="office_name_furigana" name="office_name_furigana"
                                              @if($employee_intro_service_1) value="{{ $employee_intro_service_1->office_name_furigana }}" @endif>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dropdown-divider my-0"></div>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">所在地</div>
                                    <div class="form-group row m-0 flex-column w-100 content-box pt-3">
                                       <div class="col-sm-12 mb-3">
                                          <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                               <div class="input-group-text">〒</div>
                                             </div>
                                             <input type="text" class="form-control form-control-sm number_field" id="office_location_code" placeholder="" name="office_location_code"
                                                    @if($employee_intro_service_1) value="{{ $employee_intro_service_1->office_location_code }}" @endif>
                                           </div>
                                           <label id="office_location_code-error" class="form-error" for="office_location_code"></label>
                                       </div>
                                       <div class="col-sm-12">
                                       <input type="text" class="form-control form-control-sm" id="office_location" name="office_location" placeholder=""
                                              @if($employee_intro_service_1) value="{{ $employee_intro_service_1->office_location }}" @endif>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="form-section position-static mt-5">
                                 <h5 for="staticEmail" class="col-sm-4 col-form-label">2 仕事内容</h5>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">職種</div>
                                    <div class="form-group row m-0 w-100 content-box py-3">
                                       <div class="col-sm-12">
                                          <input type="text" class="form-control form-control-sm" id="occupation" name="occupation" placeholder=""
                                                 @if($employee_intro_service_1) value="{{ $employee_intro_service_1->occupation }}" @endif>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="dropdown-divider my-0"></div>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">仕事内容</div>
                                    <div class="form-group row m-0 py-3 w-100">
                                       <div class="col-12">
                                          <textarea name="job_description" id="job_description" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->job_description }}@endif</textarea>
                                       </div>
                                     </div>
                                    <!-- <div class="form-group row flex-column m-0 w-100 content-box py-3">
                                       <div class="col-sm-12 mb-3">
                                          <textarea name="address" id="address" class="form-control form-control-sm" rows="4">DESCRIPTION IS HERE SAMPLE TEXT SAMPLE TEXT SAMPLE TEXT SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT SAMPLE TEXT　SAMPLE TEXT </textarea>
                                       </div>
                                    </div> -->
                                 </div>
                                 <div class="dropdown-divider my-0"></div>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">雇用形態</div>
                                    <div class="form-group row m-0 flex-column mb-0 w-100 content-box py-3">
                                       <div class="col-sm-12 mb-3">
                                          <div class="kt-radio-inline" name="employment_type">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="employment_status" value="正社員" @if($employee_intro_service_1 && $employee_intro_service_1->employment_status=="正社員") checked @endif> 正社員
                                               <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="employment_status" value="非常勤" @if($employee_intro_service_1 && $employee_intro_service_1->employment_status=="非常勤") checked @endif> 非常勤
                                               <span></span>
                                             </label>
                                           </div>
                                           <label id="employment_status-error" class="form-error" for="employment_status"></label>
                                       </div>
                                       <div class="dropdown-divider my-0"></div>
                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">正社員登用</label>
                                          <div class="col-sm-8">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="appointment_of_full_time_employee" value="実績あり" @if($employee_intro_service_1 && $employee_intro_service_1->appointment_of_full_time_employee=="実績あり") checked @endif> 実績あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="appointment_of_full_time_employee" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->appointment_of_full_time_employee=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="appointment_of_full_time_employee-error" class="form-error" for="appointment_of_full_time_employee"></label>
                                          </div>
                                          <div class="col-12">
                                             <textarea  @if($employee_intro_service_1 && $employee_intro_service_1->appointment_of_full_time_employee=="なし" || !$employee_intro_service_1) disabled @endif name="appointment_of_full_time_employee_description" id="appointment_of_full_time_employee_description" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->appointment_of_full_time_employee_description }}@endif</textarea>
                                          </div>
                                    </div>

                                 </div>
                              </div>



                              <div class="dropdown-divider my-0"></div>



                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label d-flex justify-content-center flex-column">
                                    <div class="position-relative pr-0">派遣・</div>
                                    <div class="position-relative pr-0">非常勤</div>
                                 </div>
                                 <div class="form-group row m-0 flex-column mb-0 w-100 content-box py-3">

                                    <div class="form-group row mx-0 mb-0 w-100 py-3">
                                       <label class="col-sm-4 col-form-label" for="">就業形態</label>
                                       <div class="col-sm-8">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="working_style" value="派遣・請負" @if($employee_intro_service_1 && $employee_intro_service_1->working_style=="派遣・請負") checked @endif> 派遣・請負
                                               <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="working_style" value="派遣・請負ではない" @if($employee_intro_service_1 && $employee_intro_service_1->working_style=="派遣・請負ではない") checked @endif> 派遣・請負ではない
                                               <span></span>
                                             </label>
                                           </div>
                                           <label id="working_style-error" class="form-error" for="working_style"></label>
                                       </div>
                                    </div>


                                    <div class="form-group row mx-0 mb-0 w-100 py-3">
                                       <label class="col-sm-4 col-form-label" for="">労働者派遣事業の許可番号</label>
                                       <div class="col-sm-8">
                                          <input  @if($employee_intro_service_1 && $employee_intro_service_1->working_style=="派遣・請負ではない" || !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm" id="worker_dispatch_business_permit_number" name="worker_dispatch_business_permit_number" placeholder=""
                                                 @if($employee_intro_service_1) value="{{ $employee_intro_service_1->worker_dispatch_business_permit_number }}" @endif>
                                       </div>
                                    </div>

                                 </div>


                           </div>



                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label position-relative">雇用期間</div>
                                 <div class="form-group row flex-column m-0 w-100 content-box py-3">
                                    <div class="col-sm-12 mb-3">
                                       <div class="kt-radio-inline">
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                            <input type="radio" name="employment_period" value="雇用期間の定めあり" @if($employee_intro_service_1 && $employee_intro_service_1->employment_period=="雇用期間の定めあり") checked @endif> 雇用期間の定めあり
                                            <span></span>
                                          </label>
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                            <input type="radio" name="employment_period" value="雇用期間の定めなし" @if($employee_intro_service_1 && $employee_intro_service_1->employment_period=="雇用期間の定めなし") checked @endif> 雇用期間の定めなし
                                            <span></span>
                                          </label>
                                        </div>
                                        <label id="employment_period-error" class="form-error" for="employment_period"></label>
                                    </div>
                                    <div class="dropdown-divider my-0"></div>
                                    <div class="form-group row mx-0 mb-0 w-100 py-3">
                                       <label class="col-sm-12 col-form-label" for="">契約更新の条件</label>
                                       <div class="col-12">
                                          <textarea  @if($employee_intro_service_1 && $employee_intro_service_1->employment_period=="雇用期間の定めなし" || !$employee_intro_service_1) disabled @endif id="contract_renewal_conditions" name="contract_renewal_conditions" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->contract_renewal_conditions }}@endif</textarea>
                                       </div>
                                 </div>

                              </div>
                           </div>

                              </div>


                              <!-- <h5 class="mb25"> 見学適時の選択 </h5>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label">  見学可能曜日 </label>
                                        <div class="col-sm-9">
                                        <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 月曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 火曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 水曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 木曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 金曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 土曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]"> 日曜日
                                            <span></span>
                                            </label>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label"> 開始時間 </label>
                                        <div class="col-sm-5">
                                        <input class="form-control form-control-sm" id="kt_timepicker_1" name="time_from" readonly placeholder="Select time" type="text"/>
                                        </div>
                                    </div> -->
                      </div>
                  </div>

                  <div class="col-lg-4">
                     <div class="bd-example form_container">
                              <div class="form-section position-static">
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">就業場所</div>
                                    <div class="form-group row m-0 content-box w-100 py-3">
                                       <div class="col-sm-12">
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <div class="input-group-text">〒</div>
                                              </div>
                                             <input type="text" class="form-control form-control-sm number_field" id="work_place_code" name="work_place_code" placeholder=""
                                                    @if($employee_intro_service_1) value="{{ $employee_intro_service_1->work_place_code }}" @endif>
                                           </div>
                                           <label id="work_place_code-error" class="form-error" for="work_place_code"></label>
                                       </div>
                                       <div class="col-sm-12 mt-3">
                                          <input type="text" class="form-control form-control-sm" id="work_place" name="work_place" placeholder=""
                                                 @if($employee_intro_service_1) value="{{ $employee_intro_service_1->work_place }}" @endif>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="d-flex">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative">マイカー</div>
                                          <div class="position-relative">通勤</div>
                                       </div>
                                       <div class="form-group row m-0 content-box w-100 py-3">
                                          <div class="col-sm-12 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="my_car_commute" value="可" @if($employee_intro_service_1 && $employee_intro_service_1->my_car_commute=="可") checked @endif> 可
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="my_car_commute" value="不可" @if($employee_intro_service_1 && $employee_intro_service_1->my_car_commute=="不可") checked @endif> 不可
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="my_car_commute-error" class="form-error" for="my_car_commute"></label>
                                          </div>
                                          <div class="form-group row m-0 w-100">
                                             <label class="col-sm-4 col-form-label" for=""> 駐車場</label>
                                             <div class="col-sm-8">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                     <input @if($employee_intro_service_1 && $employee_intro_service_1->my_car_commute=="不可" || !$employee_intro_service_1) disabled @endif type="radio" name="parking" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->parking=="あり") checked @endif> あり
                                                     <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                     <input @if($employee_intro_service_1 && $employee_intro_service_1->my_car_commute=="不可" || !$employee_intro_service_1) disabled @endif type="radio" name="parking" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->parking=="なし") checked @endif> なし
                                                     <span></span>
                                                   </label>
                                                 </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="d-flex align-items-center">
                                          <div class="form-label position-relative mr-1">転勤の可能性</div>
                                       </div>
                                       <div class="form-group row m-0 w-100 content-box">
                                          <div class="col-sm-12">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="possibility_of_transfer" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->possibility_of_transfer=="あり") checked @endif> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="possibility_of_transfer" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->possibility_of_transfer=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="possibility_of_transfer-error" class="form-error" for="possibility_of_transfer"></label>
                                          </div>
                                          <div class="col-sm-12 mb-3">
                                             <textarea id="possibility_of_transfer_details" name="possibility_of_transfer_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->possibility_of_transfer_details }}@endif</textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>


                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">年齢</div>
                                    <div class="form-group row flex-column m-0 content-box py-3 w-100">

                                       <div class="form-group row mx-0 m-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">正社員登用</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="age_promotion" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->age_promotion=="あり") checked @endif> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="age_promotion" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->age_promotion=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="age_promotion-error" class="form-error" for="age_promotion"></label>
                                          </div>
                                       </div>

                                       <div class="dropdown-divider my-0"></div>

                                       <div class="fieldset" id="age_options" @if($employee_intro_service_1 && $employee_intro_service_1->age_promotion=="なし" || !$employee_intro_service_1) disabled @endif>
                                          <div class="form-group row mx-0 mb-0 w-100">
                                             <label class="col-sm-4 col-form-label" for="">年齢制限該当事由</label>
                                             <div class="col-sm-8">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                     <input type="radio" name="age_limit_reason" value="上限あり" @if($employee_intro_service_1 && $employee_intro_service_1->age_limit_reason=="上限あり") checked @endif> 上限あり
                                                     <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                     <input type="radio" name="age_limit_reason" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->age_limit_reason=="なし") checked @endif> なし
                                                     <span></span>
                                                   </label>
                                                 </div>
                                                 <label id="age_limit_reason-error" class="form-error" for="age_limit_reason"></label>
                                             </div>
                                             <div class="col-12 pb-3">
                                                <textarea id="age_limit_reason_details" name="age_limit_reason_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->age_limit_reason_details }}@endif</textarea>
                                             </div>
                                          </div>
                                       </div>

                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label position-relative">学歴</div>
                                 <div class="form-group row flex-column m-0 content-box py-3 w-100">

                                    <div class="form-group row mx-0 mb-0 w-100">
                                       <label class="col-sm-4 col-form-label" for="">必須</label>
                                       <div class="col-sm-8 mb-3">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="academic_background" value="大学卒以上" @if($employee_intro_service_1 && $employee_intro_service_1->academic_background=="大学卒以上") checked @endif> 大学卒以上
                                               <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                               <input type="radio" name="academic_background" value="その他" @if($employee_intro_service_1 && $employee_intro_service_1->academic_background=="その他") checked @endif> その他
                                               <span></span>
                                             </label>
                                           </div>
                                       </div>
                                       <div class="col-12">
                                          <textarea id="academic_background_details" name="academic_background_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->academic_background_details }}@endif</textarea>
                                          <label id="academic_background_details-error" class="form-error" for="academic_background_details"></label>
                                       </div>
                                    </div>
                              </div>
                           </div>

                           <div class="dropdown-divider my-0"></div>

                           <div class="form-section-column position-relative d-flex align-items-center">
                              <div class="form-label d-flex justify-content-center flex-column">
                                 <div class="position-relative pr-0">必要な</div>
                                 <div class="position-relative pr-0">経験等</div>
                              </div>
                              <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                 <div class="col-12">
                                    <div class="row">
                                       <label class="col-sm-6 col-form-label" for="">必要な経験・知識・技術等</label>
                                       <div class="col-sm-6 mb-3">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="knowledge_skills" value="条件あり" @if($employee_intro_service_1 && $employee_intro_service_1->knowledge_skills=="条件あり") checked @endif> 条件あり
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="knowledge_skills" value="不問" @if($employee_intro_service_1 && $employee_intro_service_1->knowledge_skills=="不問") checked @endif> 不問
                                             <span></span>
                                             </label>
                                          </div>
                                          <label id="knowledge_skills-error" class="form-error" for="knowledge_skills"></label>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="col-12">
                                    <textarea id="knowledge_skills_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください" name="knowledge_skills_details">@if($employee_intro_service_1){{ $employee_intro_service_1->knowledge_skills_details }}@endif</textarea>
                                 </div>
                              </div>
                           </div>


                           <div class="dropdown-divider my-0"></div>

                           <div class="form-section-column position-relative d-flex align-items-center">
                              <div class="form-label d-flex justify-content-center flex-column">
                                 <div class="position-relative pr-0">必要な</div>
                                 <div class="position-relative pr-0">PCスキル</div>
                              </div>
                              <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                 <div class="col-12">
                                    <div class="row">
                                       <div class="col-sm-12 mb-3">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pc_skills" value="必要" @if($employee_intro_service_1 && $employee_intro_service_1->pc_skills=="必要") checked @endif> 必要
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pc_skills" value="不要" @if($employee_intro_service_1 && $employee_intro_service_1->pc_skills=="不要") checked @endif> 不要
                                             <span></span>
                                             </label>
                                          </div>
                                          <label id="pc_skills-error" class="form-error" for="pc_skills"></label>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="col-12">
                                    <textarea id="pc_skills_details" name="pc_skills_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->pc_skills_details }}@endif</textarea>
                                 </div>
                              </div>
                           </div>


                           <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label position-relative">必要な免許・資格</div>
                                 <div class="form-group row flex-column content-box m-0 py-3 w-100">

                                    <div class="form-group row mx-0 mb-0 w-100">
                                       <label class="col-sm-12 col-form-label" for="">薬剤師免許 (必須)</label>
                                       <div class="col-12">
                                          <textarea name="pharmacist_license" id="pharmacist_license" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->pharmacist_license }}@endif</textarea>
                                       </div>
                                    </div>

                                    <div class="form-group row mx-0 mb-0 w-100">
                                       <label class="col-sm-4 col-form-label" for="">普通自動車運転免許</label>
                                       <div class="col-sm-8">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="ordinary_car_drivers_license" value="必須" @if($employee_intro_service_1 && $employee_intro_service_1->ordinary_car_drivers_license=="必須") checked @endif> 必須
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="ordinary_car_drivers_license" value="不要" @if($employee_intro_service_1 && $employee_intro_service_1->ordinary_car_drivers_license=="不要") checked @endif> 不要
                                             <span></span>
                                             </label>
                                          </div>
                                          <label id="ordinary_car_drivers_license-error" class="form-error" for="ordinary_car_drivers_license"></label>
                                       </div>
                                    </div>

                              </div>
                           </div>


                           <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label position-relative">試用期間</div>
                                 <div class="form-group row flex-column content-box m-0 py-3 w-100">

                                    <div class="form-group row mx-0 mb-0 w-100">
                                       <div class="col-sm-8">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="trial_period" value="試用期間あり" @if($employee_intro_service_1 && $employee_intro_service_1->trial_period=="試用期間あり") checked @endif> 試用期間あり
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="trial_period" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->trial_period=="なし") checked @endif> なし
                                             <span></span>
                                             </label>
                                          </div>
                                          <label id="trial_period-error" class="form-error" for="trial_period"></label>
                                       </div>
                                       <div class="col-12">
                                          <label class="col-sm-12 col-form-label" for="">試用期間中の労働条件</label>
                                          <textarea id="trial_period_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください" name="trial_period_details">@if($employee_intro_service_1){{ $employee_intro_service_1->trial_period_details }}@endif</textarea>
                                       </div>
                                    </div>


                              </div>
                           </div>


                              </div>
                      </div>
                  </div>












                  <div class="col-lg-5">
                     <div class="bd-example form_container">
                              <div class="form-section position-static">
                                 <h5 for="staticEmail" class="col-sm-4 col-form-label">３ 賃金・手当</h5>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-group row m-0 py-3 w-100">
                                       <label class="col-sm-4 col-form-label" for="">月額（a+b)</label>
                                       <div class="col-sm-8">
                                          <div class="d-flex align-items-center">
                                             <div class="col">
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="monthly_amount_a" placeholder="" name="monthly_amount_a"
                                                          @if($employee_intro_service_1) value="{{ $employee_intro_service_1->monthly_amount_a }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">円</div>
                                                    </div>
                                                 </div>
                                                 <label id="monthly_amount_a-error" class="form-error" for="monthly_amount_a"></label>
                                             </div>
                                              <div class="d-inline-block mx-2">〜</div>
                                              <div class="col">
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="monthly_amount_b" placeholder="" name="monthly_amount_b"
                                                          @if($employee_intro_service_1) value="{{ $employee_intro_service_1->monthly_amount_b }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">円</div>
                                                    </div>
                                                 </div>
                                                 <label id="monthly_amount_b-error" class="form-error" for="monthly_amount_b"></label>
                                              </div>
                                          </div>
                                       </div>

                                       <div class="col-sm-12 mt-3 px-0">
                                          <small>※（固定残業代がある場合はa+b+c)</small>

                                          <div class="d-flex align-items-center">
                                             <div class="form-label position-relative">賃</div>

                                                <div class="row m-0 flex-1 content-box ip-box">

                                                   <div class="form form-a d-flex">
                                                      <div class="d-flex align-items-center justify-content-center">

                                                         <div class="d-flex justify-content-end align-items-center pr-1">
                                                            <div class="form-label d-flex flex-column w-auto mr-0 pr-0">
                                                               <div class="position-relative" style="visibility: hidden">定期的に</div>
                                                               <div class="position-relative">基本給</div>
                                                            </div>
                                                            <div class="position-relative" style="writing-mode: initial;">(a)</div>
                                                         </div>

                                                      </div>

                                                      <div class="form-group row content-box m-0 py-3 w-100">
                                                         <div class="col-12 px-0">
                                                            <div class="row py-3 m-0">
                                                               <label class="col-sm-6 col-form-label" for="">基本月給(月平均)又は時間額</label>
                                                               <div class="col-lg-6">
                                                                  <div class="row align-items-center flex-nowrap">
                                                                     <label class="col-sm-6 col-form-label" for="">月平均労働日数</label>
                                                                     <div class="col">
                                                                        <div class="input-group">
                                                                           <input type="text" class="form-control form-control-sm number_field" id="average_working_days_per_month" placeholder="" name="average_working_days_per_month"
                                                                                  @if($employee_intro_service_1) value="{{ $employee_intro_service_1->average_working_days_per_month }}" @endif>
                                                                           <div class="input-group-append">
                                                                              <div class="input-group-text">日</div>
                                                                           </div>
                                                                        </div>
                                                                        <label id="average_working_days_per_month-error" class="form-error" for="average_working_days_per_month"></label>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-sm-12">
                                                            <div class="d-flex m-0 align-items-center">
                                                               <div class="col">
                                                                  <div class="input-group mb-2">
                                                                     <input type="text" class="form-control form-control-sm number_field" id="basic_pay_a" name="basic_pay_a" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->basic_pay_a }}" @endif>
                                                                     <div class="input-group-append">
                                                                        <div class="input-group-text">円</div>
                                                                      </div>
                                                                   </div>
                                                                   <label id="basic_pay_a-error" class="form-error" for="basic_pay_a"></label>
                                                               </div>
                                                                <div class="d-inline-block mx-2">〜</div>
                                                                <div class="col">
                                                                  <div class="input-group mb-2">
                                                                     <input type="text" class="form-control form-control-sm number_field" id="basic_pay_b" name="basic_pay_b" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->basic_pay_b }}" @endif>
                                                                     <div class="input-group-append">
                                                                        <div class="input-group-text">円</div>
                                                                      </div>
                                                                   </div>
                                                                   <label id="basic_pay_b-error" class="form-error" for="basic_pay_b"></label>
                                                                </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div class="dropdown-divider w-100"></div>

                                                   <div class="form form-b d-flex">
                                                      <div class="d-flex justify-content-end align-items-center pr-1">
                                                         <div class="form-label d-flex flex-column w-auto mr-0 pr-0">
                                                            <div class="position-relative">定期的に</div>
                                                            <div class="position-relative">支払われる手当て</div>
                                                         </div>
                                                         <div class="position-relative" style="writing-mode: initial;">(b)</div>
                                                      </div>

                                                      {{-- <div class="form-label d-flex justify-content-center flex-column">
                                                         <div class="position-relative">定期的に支払われる手当</div>
                                                         <div class="position-relative" style="writing-mode: initial;">(b)</div>
                                                      </div> --}}
                                                      <div class="form-group row content-box m-0 py-4 w-100">
                                                         <div class="col-12">

                                                            <div class="row align-items-center" id="fixed_amount1">
                                                               <div class="col-sm-3">
                                                                  <input type="text" class="form-control form-control-sm inputVal" id="fixed_amount_1" name="fixed_amount_1"
                                                                         @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_1 }}" @endif>
                                                               </div>
                                                               <div class="col-sm-2">
                                                                  <input type="text" readonly class="form-control form-control-sm" id="fixed_amount_1_allowance" name="fixed_amount_1_allowance" placeholder="手当">
                                                               </div>
                                                               <div class="col-lg-7 d-flex m-0 align-items-center">
                                                                  <div class="input-group">
                                                                     <input  @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_1_field_1 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_1_field_1" name="fixed_amount_1_field_1" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_1_field_1 }}" @endif>
                                                                     <div class="input-group-append">
                                                                        <div class="input-group-text">円</div>
                                                                      </div>
                                                                   </div>
                                                                   <label id="fixed_amount_1_field_1-error" class="form-error" for="fixed_amount_1_field_1"></label>
                                                                   <div class="d-inline-block mx-2">〜</div>
                                                                   <div class="input-group">
                                                                     <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_1_field_2 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_1_field_2" name="fixed_amount_1_field_2" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_1_field_2 }}" @endif>
                                                                     <div class="input-group-append">
                                                                        <div class="input-group-text">円</div>
                                                                      </div>
                                                                   </div>
                                                               </div>
                                                            </div>

                                                            <div class="dropdown-divider"></div>

                                                             <div class="row align-items-center" id="fixed_amount2">
                                                                 <div class="col-sm-3">
                                                                     <input type="text" class="form-control form-control-sm inputVal" id="fixed_amount_2" name="fixed_amount_2" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_2 }}" @endif>
                                                                 </div>
                                                                 <div class="col-sm-2">
                                                                     <input type="text" readonly class="form-control form-control-sm" id="fixed_amount_2_allowance" name="fixed_amount_2_allowance" placeholder="手当">
                                                                 </div>
                                                                 <div class="col-lg-7 d-flex m-0 align-items-center">
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_2_field_1 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_2_field_1" name="fixed_amount_2_field_1" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_2_field_1 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                     <label id="fixed_amount_2_field_1-error" class="form-error" for="fixed_amount_2_field_1"></label>
                                                                     <div class="d-inline-block mx-2">〜</div>
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_2_field_2 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_2_field_2" name="fixed_amount_2_field_2" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_2_field_2 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                            <div class="dropdown-divider"></div>

                                                             <div class="row align-items-center" id="fixed_amount3">
                                                                 <div class="col-sm-3">
                                                                     <input type="text" class="form-control form-control-sm inputVal" id="fixed_amount_3" name="fixed_amount_3" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_3 }}" @endif>
                                                                 </div>
                                                                 <div class="col-sm-2">
                                                                     <input type="text" readonly class="form-control form-control-sm" id="fixed_amount_3_allowance" name="fixed_amount_3_allowance" placeholder="手当">
                                                                 </div>
                                                                 <div class="col-lg-7 d-flex m-0 align-items-center">
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_3_field_1 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_3_field_1" name="fixed_amount_3_field_1" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_3_field_1 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                     <label id="fixed_amount_3_field_1-error" class="form-error" for="fixed_amount_3_field_1"></label>
                                                                     <div class="d-inline-block mx-2">〜</div>
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_3_field_2 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_3_field_2" name="fixed_amount_3_field_2" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_3_field_2 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                            <div class="dropdown-divider"></div>

                                                             <div class="row align-items-center" id="fixed_amount4">
                                                                 <div class="col-sm-3">
                                                                     <input type="text" class="form-control form-control-sm inputVal" id="fixed_amount_4" name="fixed_amount_4" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_4 }}" @endif>
                                                                 </div>
                                                                 <div class="col-sm-2">
                                                                     <input type="text" readonly class="form-control form-control-sm" id="fixed_amount_4_allowance" name="fixed_amount_4_allowance" placeholder="手当">
                                                                 </div>
                                                                 <div class="col-lg-7 d-flex m-0 align-items-center">
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_4_field_1 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_4_field_1" name="fixed_amount_4_field_1" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_4_field_1 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                     <label id="fixed_amount_4_field_1-error" class="form-error" for="fixed_amount_4_field_1"></label>
                                                                     <div class="d-inline-block mx-2">〜</div>
                                                                     <div class="input-group">
                                                                         <input @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_4_field_2 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_4_field_2" name="fixed_amount_4_field_2" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_4_field_2 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                            <div class="dropdown-divider"></div>

                                                             <div class="row align-items-center" id="fixed_amount5">
                                                                 <div class="col-sm-3">
                                                                     <input type="text" class="form-control form-control-sm inputVal" id="fixed_amount_5" name="fixed_amount_5" placeholder=""
                                                                            @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_5 }}" @endif>
                                                                 </div>
                                                                 <div class="col-sm-2">
                                                                     <input type="text" readonly class="form-control form-control-sm" id="fixed_amount_5_allowance" name="fixed_amount_5_allowance" placeholder="手当">
                                                                 </div>
                                                                 <div class="col-lg-7 d-flex m-0 align-items-center">
                                                                     <div class="input-group">
                                                                         <input  @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_5_field_1 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_5_field_1" name="fixed_amount_5_field_1" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_5_field_1 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                     <label id="fixed_amount_5_field_1-error" class="form-error" for="fixed_amount_5_field_1"></label>
                                                                     <div class="d-inline-block mx-2">〜</div>
                                                                     <div class="input-group">
                                                                         <input  @if(($employee_intro_service_1 && !$employee_intro_service_1->fixed_amount_5_field_2 )|| !$employee_intro_service_1) disabled @endif type="text" class="form-control form-control-sm number_field" id="fixed_amount_5_field_2" name="fixed_amount_5_field_2" placeholder=""
                                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->fixed_amount_5_field_2 }}" @endif>
                                                                         <div class="input-group-append">
                                                                             <div class="input-group-text">円</div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div class="dropdown-divider w-100"></div>

                                                   <div class="form form-c d-flex">

                                                      <div class="d-flex justify-content-end align-items-center pr-1">
                                                            <div class="form-label d-flex flex-column w-auto mr-0 pr-0">
                                                               <div class="position-relative" style="visibility: hidden">定期的に</div>
                                                               <div class="position-relative">固定残業代</div>
                                                            </div>
                                                            <div class="position-relative" style="writing-mode: initial;">(c)</div>
                                                         </div>


                                                      <div class="form-group m-0 flex-column mb-0 w-100 content-box py-3">
                                                            <div class="row m-0 flex-nowrap align-items-center">
                                                               <div class="col-auto">
                                                                  <div class="kt-radio-inline">
                                                                     <label class="kt-radio kt-radio--solid kt-radio--success">
                                                                     <input type="radio" name="overtime_pay" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->overtime_pay=="あり") checked @endif> あり
                                                                     <span></span>
                                                                     </label>
                                                                     <label class="kt-radio kt-radio--solid kt-radio--success">
                                                                     <input type="radio" name="overtime_pay" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->overtime_pay=="なし") checked @endif> なし
                                                                     <span></span>
                                                                     </label>
                                                                  </div>
                                                                  <label id="overtime_pay-error" class="form-error" for="overtime_pay"></label>
                                                               </div>

                                                               <div class="col">
                                                                  <div class="d-flex m-0 align-items-center" id="overtime_pay_details">
                                                                        <div class="col">
                                                                           <div class="input-group">
                                                                              <input @if($employee_intro_service_1 && $employee_intro_service_1->overtime_pay=="なし") disabled @endif type="text" class="form-control form-control-sm number_field" id="overtime_pay_1" name="overtime_pay_1" placeholder=""
                                                                                     @if($employee_intro_service_1) value="{{ $employee_intro_service_1->overtime_pay_1 }}" @endif>
                                                                              <div class="input-group-append">
                                                                                 <div class="input-group-text">円</div>
                                                                               </div>
                                                                            </div>
                                                                            <label id="overtime_pay_1-error" class="form-error" for="overtime_pay_1"></label>
                                                                        </div>
                                                                         <div class="d-inline-block mx-2">〜</div>
                                                                         <div class="col">
                                                                           <div class="input-group">
                                                                              <input @if($employee_intro_service_1 && $employee_intro_service_1->overtime_pay=="なし") disabled @endif  type="text" class="form-control form-control-sm number_field" id="overtime_pay_2" name="overtime_pay_2" placeholder=""
                                                                                     @if($employee_intro_service_1) value="{{ $employee_intro_service_1->overtime_pay_2 }}" @endif>
                                                                              <div class="input-group-append">
                                                                                 <div class="input-group-text">円</div>
                                                                               </div>
                                                                            </div>
                                                                            <label id="overtime_pay_2-error" class="form-error" for="overtime_pay_2"></label>
                                                                         </div>
                                                                        </div>
                                                               </div>
                                                         </div>

                                                      <div class="col-12 py-3">
                                                         <small>固定残業代に関する特記事項</small>
                                                      </div>

                                                      <div class="col-12">
                                                         <textarea @if($employee_intro_service_1 && $employee_intro_service_1->overtime_pay=="なし") disabled @endif  id="overtime_pay_note" name="overtime_pay_note" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->overtime_pay_note }}@endif</textarea>
                                                      </div>

                                                      </div>
                                                   </div>

                                                </div>
                                             </div>
                                          </div>

                                          <div class="dropdown-divider w-100"></div>

                                          <div class="form-group row m-0 py-3 w-100">
                                             <div class="col-12 px-0">
                                                <div class="d-flex align-items-start w-100">
                                                   <div class="form-label position-relative">金</div>
                                                   <div class="row m-0 flex-1 content-box" style="border-left:2px solid #f8f9fa">

                                                      <div class="d-flex justify-content-end align-items-center pr-1">
                                                         <div class="form-label d-flex flex-column w-auto mr-0 pr-0">
                                                            <div class="position-relative">記事項</div>
                                                            <div class="position-relative">その手当付</div>
                                                         </div>
                                                         <div class="position-relative" style="writing-mode: initial;">(d)</div>
                                                      </div>

                                                      <div class="form-group row content-box m-0 py-4 w-100">

                                                         <div class="col-12">
                                                            <textarea name="articles_with_allowance" id="articles_with_allowance" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->articles_with_allowance }}@endif</textarea>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                       </div>
                                    </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column form-label">
                                       <div class="position-relative pr-0">賃金</div>
                                       <div class="position-relative pr-0">形態等</div>
                                    </div>
                                    <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                       <div class="col-12">
                                          <div class="row py-3">
                                             <div class="col-4">月給</div>
                                             <div class="col-8">なし</div>
                                          </div>
                                       </div>
                                       <div class="col-12">
                                          <div class="row py-3">
                                             <div class="col-4">その他内容</div>
                                             <div class="col-lg-8">
                                                <textarea name="other_contents" id="other_contents" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->other_contents }}@endif</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column">
                                       <div class="position-relative pr-0">通勤</div>
                                       <div class="position-relative pr-0">手当</div>
                                    </div>
                                    <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                       <div class="col-12">
                                          <div class="row align-items-center py-3">
                                             <div class="col-2">実費支給</div>
                                             <div class="col-4">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="actual_expense_payment" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->actual_expense_payment=="あり") checked @endif> あり
                                                   <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="actual_expense_payment" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->actual_expense_payment=="なし") checked @endif> なし
                                                   <span></span>
                                                   </label>
                                                </div>
                                                <label id="actual_expense_payment-error" class="form-error" for="actual_expense_payment"></label>
                                             </div>

                                             <div class="col-lg-6">
                                                <div class="row align-items-center flex-nowrap">
                                                   <div class="col-auto mr-2">月額</div>

                                                   <div class="col-auto">
                                                      <div class="input-group mb-2">
                                                         <input type="text" @if($employee_intro_service_1 && $employee_intro_service_1->actual_expense_payment=="なし") disabled @endif   class="form-control form-control-sm" id="actual_expense_payment_monthly" name="actual_expense_payment_monthly" placeholder=""
                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->actual_expense_payment_monthly }}" @endif>
                                                         <div class="input-group-append">
                                                            <div class="input-group-text">円</div>
                                                         </div>
                                                      </div>
                                                      <label id="actual_expense_payment_monthly-error" class="form-error" for="actual_expense_payment_monthly"></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column">
                                       <div class="position-relative pr-0">賃金</div>
                                       <div class="position-relative pr-0">締切日</div>
                                    </div>
                                    <div class="form-group row flex-column content-box m-0 w-100 py-3">
                                       <div class="col-12">
                                          <div class="row align-items-center">
                                             <div class="col-6">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="wages" value="固定 (月末以外)" @if($employee_intro_service_1 && $employee_intro_service_1->wages=="固定 (月末以外)") checked @endif> 固定 (月末以外)
                                                   <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="wages" value="その他" @if($employee_intro_service_1 && $employee_intro_service_1->wages=="その他") checked @endif> その他
                                                   <span></span>
                                                   </label>
                                                </div>
                                                <label id="wages-error" class="form-error" for="wages"></label>
                                             </div>

                                             <div class="col-lg-6">
                                                <div class="row align-items-center flex-nowrap">
                                                   <div class="col-auto mr-2">毎月</div>

                                                   <div class="col-auto">
                                                      <div class="input-group mb-2">
                                                         <input @if($employee_intro_service_1 && $employee_intro_service_1->wages=="その他") disabled @endif type="text" class="form-control form-control-sm" id="wages_details" name="wages_details" placeholder=""
                                                                @if($employee_intro_service_1) value="{{ $employee_intro_service_1->wages_details }}" @endif>
                                                         <div class="input-group-append">
                                                            <div class="input-group-text">日</div>
                                                         </div>
                                                      </div>
                                                      <label id="wages_details-error" class="form-error" for="wages_details"></label>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="col-lg-12 py-3">
                                                <textarea id="wages_notes" name="wages_notes" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->wages_notes }}@endif</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column">
                                       <div class="position-relative pr-0">賃金 </div>
                                       <div class="position-relative pr-0">支払い日</div>
                                    </div>
                                    <div class="form-group row flex-column content-box m-0 w-100 py-3">
                                       <div class="col-12">
                                          <div class="row align-items-center">
                                             <div class="col-6">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="payment_date" value="固定 (月末以外)" @if($employee_intro_service_1 && $employee_intro_service_1->payment_date=="固定 (月末以外)") checked @endif> 固定(月末以外)
                                                   <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="payment_date" value="その他" @if($employee_intro_service_1 && $employee_intro_service_1->payment_date=="その他") checked @endif> その他
                                                   <span></span>
                                                   </label>
                                                </div>
                                                <label id="payment_date-error" class="form-error" for="payment_date"></label>
                                             </div>

                                             <div class="col-2">毎月</div>

                                             <div class="col-lg-4">
                                                <div class="input-group mb-2">
                                                   <input type="text" @if($employee_intro_service_1 && $employee_intro_service_1->payment_date=="その他") disabled @endif class="form-control form-control-sm" id="payment_date_details" name="payment_date_details" placeholder=""
                                                          @if($employee_intro_service_1) value="{{ $employee_intro_service_1->payment_date_details }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">日</div>
                                                    </div>
                                                 </div>
                                                 <label id="payment_date_details-error" class="form-error" for="payment_date_details"></label>
                                             </div>

                                             <div class="col-lg-12 py-3">
                                                <textarea id="payment_date_notes" name="payment_date_notes" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->payment_date_notes }}@endif</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">昇給</div>
                                    <div class="form-group row flex-column content-box m-0 w-100">

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <div class="col-sm-8">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="salary_raise" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->salary_raise=="あり") checked @endif> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="salary_raise" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->salary_raise=="なし") checked @endif> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="salary_raise-error" class="form-error" for="salary_raise"></label>
                                          </div>
                                          <div class="col-12 pt-3">
                                             <textarea @if($employee_intro_service_1 && $employee_intro_service_1->salary_raise=="なし") disabled @endif id="salary_raise_details" name="salary_raise_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->salary_raise_details }}@endif</textarea>
                                          </div>
                                       </div>
                                 </div>
                              </div>

                              <div class="dropdown-divider"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-label position-relative">賞与</div>
                                 <div class="form-group row flex-column content-box m-0 w-100">

                                    <div class="form-group row mx-0 mb-0 w-100">
                                       <div class="col-sm-8">
                                          <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="bonus" value="あり" @if($employee_intro_service_1 && $employee_intro_service_1->bonus=="あり") checked @endif> あり
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="bonus" value="なし" @if($employee_intro_service_1 && $employee_intro_service_1->bonus=="なし") checked @endif> なし
                                             <span></span>
                                             </label>
                                          </div>
                                          <label id="bonus-error" class="form-error" for="bonus"></label>
                                       </div>
                                       <div class="col-12 pt-3 pb-3">
                                          <textarea @if($employee_intro_service_1 && $employee_intro_service_1->bonus=="なし") disabled @endif  id="bonus_details" name="bonus_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_1){{ $employee_intro_service_1->bonus_details }}@endif</textarea>
                                       </div>
                                    </div>
                              </div>
                           </div>
                          </div>
                        </div>
                      </div>
                  </div>










               <div class="row" style="margin-top: 10rem;">

                  <div class="col-lg-4">
                     <div class="bd-example form_container">
                              <div class="form-section position-static">
                                 <h5 for="staticEmail" class="col-sm-4 col-form-label">４ 労働時間</h5>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">就業時間</div>
                                    <div class="row flex-column m-0">
                                       <div class="form-group row flex-nowrap content-box m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(1)</label>
                                          <div class="c">
                                             <div class="row m-0 align-items-center">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_1_from" name="working_hours_1_from" placeholder="00:00" maxlength="5"
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_1_from }}" @endif >
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_1_to" name="working_hours_1_to" placeholder="00:00"
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_1_to }}" @endif>
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group row flex-nowrap content-box m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(2)</label>
                                          <div class="c">
                                             <div class="row m-0 align-items-center">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_2_from" name="working_hours_2_from" placeholder="00:00"
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_2_from }}" @endif

                                                          @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_1_from) || !$employee_intro_service_2) disabled="disabled" @endif
                                                           >
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_2_to" name="working_hours_2_to" placeholder="00:00"
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_2_to }}" @endif
                                                         @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_1_to) || !$employee_intro_service_2)
                                                         disabled="disabled"
                                                         @endif
                                                        >
                                             </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group row flex-nowrap content-box m-0 py-3 w-100">
                                          <label class="col col-form-label" for="">(3)</label>
                                          <div class="c">

                                             <div class="row m-0 align-items-center">
                                                <div class="col">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_3_from" name="working_hours_3_from" placeholder="00:00"
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_3_from }}" @endif

                                                          @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_2_from) || !$employee_intro_service_2)
                                                          disabled="disabled"
                                                          @endif
                                                          >
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                             <div class="col">
                                                <input type="text" class="form-control form-control-sm time" id="working_hours_3_to" name="working_hours_3_to" placeholder="00:00"
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_3_to }}" @endif

                                                       @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_2_to) || !$employee_intro_service_2)
                                                       disabled="disabled"
                                                       @endif
                                                       >
                                             </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="form-group row flex-nowrap content-box m-0 py-3 w-100">
                                          <label class="col col-form-label" for="" style="white-space: nowrap;">又は</label>
                                          <div class="col-auto">
                                             <div class="row align-items-center">
                                                <div class="col-2">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_or_from" name="working_hours_or_from" placeholder="00:00"
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_or_from }}" @endif

                                                          @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_3_from) || !$employee_intro_service_2)
                                                          disabled="disabled"
                                                          @endif
                                                          >
                                                </div>
                                                <div class="col-2">
                                                   <input type="text" class="form-control form-control-sm time" id="working_hours_or_to" name="working_hours_or_to" placeholder="00:00"
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_or_to }}" @endif

                                                          @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_3_to) || !$employee_intro_service_2)
                                                          disabled="disabled"
                                                          @endif
                                                          >
                                                </div>
                                                <div class="d-inline-block mx-2">〜</div>
                                                <div class="col-5">
                                                   <div class="d-flex align-items-center">
                                                      <label class="mr-3 col-form-label" style="white-space: nowrap" for="">の間の</label>
                                                      <div class="col">
                                                         <div class="input-group" style="width:120px">
                                                            <input type="text" class="form-control form-control-sm number_field" id="working_hours_between" name="working_hours_between"
                                                                   @if($employee_intro_service_2) value="{{ $employee_intro_service_2->working_hours_between }}" @endif
                                                                   
                                                                   @if(($employee_intro_service_2 && !$employee_intro_service_2->working_hours_or_to) || !$employee_intro_service_2)
                                                          disabled="disabled"
                                                          @endif
                                                                   >
                                                            <div class="input-group-append">
                                                               <div class="input-group-text">時間</div>
                                                             </div>
                                                          </div>
                                                          <label id="working_hours_between-error" class="form-error" for="working_hours_between"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 py-3">
                                          <label class="col-12 col-form-label" for="">就業時間に関する特記事項</label>
                                          <textarea name="working_hours_notes" id="working_hours_notes" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->working_hours_notes }}@endif</textarea>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">時間外労働時間</div>

                                    <div class="row flex-column content-box m-0">
                                       <div class="form-group row m-0 pb-3 align-items-center">
                                          <div class="col-lg-6">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="overtime_work_hours" value="時間外労働あり" @if($employee_intro_service_2 && $employee_intro_service_2->overtime_work_hours=="時間外労働あり") checked @endif> 時間外労働あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="overtime_work_hours" value="時間外労働なし" @if($employee_intro_service_2 && $employee_intro_service_2->overtime_work_hours=="時間外労働なし") checked @endif> 時間外労働なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="overtime_work_hours-error" class="form-error" for="overtime_work_hours"></label>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="row flex-nowrap align-items-center">
                                                <label class="col-auto mr-2 col-form-label" for="">月平均</label>
                                                <input @if($employee_intro_service_2 && $employee_intro_service_2->overtime_work_hours=="時間外労働なし") disabled @endif  type="text" class="col form-control form-control-sm" id="overtime_work_hours_details" name="overtime_work_hours_details"
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->overtime_work_hours_details }}" @endif>
                                                <label class="col-auto ml-2 col-form-label" for="">時間</label>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="dropdown-divider my-0"></div>

                                       <div class="form-group row align-items-center m-0 pb-3">
                                          <label class="col-lg-6 col-form-label" for="">36協定における特別条項</label>
                                          <div class="col-sm-6">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="special_provisions" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->special_provisions=="あり") checked @endif> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="special_provisions" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->special_provisions=="なし") checked @endif> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="special_provisions-error" class="form-error" for="special_provisions"></label>
                                          </div>

                                          <div class="dropdown-divider my-0"></div>

                                       <div class="col-12 py-3">
                                          <label class="col-12 col-form-label" for="">特別な事情・期間等</label>
                                          <textarea @if($employee_intro_service_2 && $employee_intro_service_2->special_provisions=="なし") disabled @endif id="special_provisions_details" name="special_provisions_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->special_provisions_details }}@endif</textarea>
                                       </div>

                                       </div>


                                    </div>

                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="row flex-column content-box py-3 m-0">
                                       <div class="form-group row m-0">
                                          <div class="col-lg-6">
                                             <div class="input-group">
                                                <label class="col-form-label mr-3" for="">時間　休憩</label>
                                                <input type="text" class="form-control form-control-sm number_field" id="time_break" name="time_break" placeholder=""
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->time_break }}" @endif>
                                                <div class="input-group-append">
                                                   <div class="input-group-text">分</div>
                                                 </div>
                                              </div>
                                              <label id="time_break-error" class="form-error" for="time_break"></label>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="input-group">
                                                <label for="" class="col-form-label mr-3">年間休日数</label>
                                                <input type="text" class="form-control form-control-sm number_field" id="annual_holidays" name="annual_holidays" placeholder=""
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->annual_holidays }}" @endif>
                                                <div class="input-group-append">
                                                   <div class="input-group-text">日</div>
                                                 </div>
                                              </div>
                                              <label id="annual_holidays-error" class="form-error" for="annual_holidays"></label>
                                          </div>
                                       </div>

                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">休日等</div>

                                    <div class="row flex-column content-box m-0">
                                       <div class="form-group row m-0">
                                          <div class="col-12 py-3">
                                             <textarea name="holidays_etc" id="holidays_etc" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->holidays_etc }}@endif</textarea>
                                          </div>
                                       </div>

                                       <div class="form-group row m-0">
                                          <div class="col-sm-7">
                                             <label class="col-12 col-form-label" for="">6ヶ月経過後の年次有給休暇日数</label>
                                          </div>
                                          <div class="col-sm-5">
                                             <div class="input-group mb-2">
                                                <input type="text" class="form-control form-control-sm number_field" id="annual_paid_vacation_days_after_6_months" name="annual_paid_vacation_days_after_6_months" placeholder=""
                                                       @if($employee_intro_service_2) value="{{ $employee_intro_service_2->annual_paid_vacation_days_after_6_months }}" @endif>
                                                <div class="input-group-append">
                                                   <div class="input-group-text">日</div>
                                                 </div>
                                              </div>
                                              <label id="annual_paid_vacation_days_after_6_months-error" class="form-error" for="annual_paid_vacation_days_after_6_months"></label>
                                          </div>
                                       </div>

                                    </div>
                                 </div>
                              </div>

                              <div class="form-section position-static mt-5">
                                 <h5 for="staticEmail" class="col-sm-12 col-form-label">５ その他の労働条件等</h5>
                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column form-label">
                                       <div class="position-relative pr-0">加入</div>
                                       <div class="position-relative pr-0">保険</div>
                                    </div>
                                    <div class="form-group row m-0 w-100 content-box py-3">
                                       <div class="col-sm-4">

                                          <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="雇用" @if($employee_intro_service_2){{in_array("雇用",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 雇用
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="労災" @if($employee_intro_service_2){{in_array("労災",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 労災
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="insurance_coverage[]" value="公災" @if($employee_intro_service_2){{in_array("公災",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 公災
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="健康" @if($employee_intro_service_2){{in_array("健康",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 健康
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="厚生" @if($employee_intro_service_2){{in_array("厚生",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 厚生
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" name="insurance_coverage[]" value="財形" @if($employee_intro_service_2){{in_array("財形",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> 財形
                                                <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                <input type="checkbox" id="insurance_coverage_other" name="insurance_coverage[]" value="その他" @if($employee_intro_service_2){{in_array("その他",explode(', ', $employee_intro_service_2->insurance_coverage))?'checked':''}} @endif> その他
                                                <span></span>
                                             </label>
                                         </div>

                                         <label id="insurance_coverage-error" class="form-error" for="insurance_coverage"></label>

                                          <textarea  @if($employee_intro_service_2){{in_array("その他",explode(', ', $employee_intro_service_2->insurance_coverage))?'':'disabled'}} @else disabled @endif id="insurance_coverage_details" name="insurance_coverage_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->insurance_coverage_details }}@endif</textarea>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="row">
                                             <label class="col-form-label col-12" for="">退職金共済</label>
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_benefits" value="加入" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_benefits=="加入") checked @endif> 加入
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_benefits" value="未加入" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_benefits=="未加入") checked @endif> 未加入
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="retirement_benefits-error" class="form-error" for="retirement_benefits"></label>
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="row">
                                             <label class="col-form-label col-12" for="">退職金制度</label>
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_system" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_system=="あり") checked @endif> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="retirement_system" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_system=="なし") checked @endif> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="retirement_system-error" class="form-error" for="retirement_system"></label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label d-flex justify-content-center flex-column form-label">
                                       <div class="position-relative pr-0">企業</div>
                                       <div class="position-relative pr-0">年金</div>
                                    </div>

                                    <div class="form-group row flex-column m-0 w-100 content-box py-3">
                                       <div class="col-sm-12 mb-3">

                                          <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]" value="厚生年金機構" @if($employee_intro_service_2){{in_array("厚生年金機構",explode(', ', $employee_intro_service_2->annuities_corporate))?'checked':''}} @endif> 厚生年金機構
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]" value="確定拠出機構" @if($employee_intro_service_2){{in_array("確定拠出機構",explode(', ', $employee_intro_service_2->annuities_corporate))?'checked':''}} @endif> 確定拠出機構
                                             <span></span>
                                             </label>
                                             <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                             <input type="checkbox" name="annuities_corporate[]" value="確定給付年金" @if($employee_intro_service_2){{in_array("確定給付年金",explode(', ', $employee_intro_service_2->annuities_corporate))?'checked':''}} @endif> 確定給付年金
                                             <span></span>
                                             </label>
                                         </div>

                                         <label id="annuities_corporate[]-error" class="form-error" for="annuities_corporate[]"></label>

                                          <!-- <div class="kt-radio-inline">
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pension_corporations" value="EM system"> 厚生年金機構
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                             <input type="radio" name="pension_corporations" value="Gooco"> 確定拠出機構
                                             <span></span>
                                             </label>
                                             <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="pension_corporations" value="Gooco"> 確定給付年金
                                                <span></span>
                                                </label>
                                          </div> -->

                                       </div>
                                    </div>
                                 </div>

                                 <!-- <div class="dropdown-divider my-0"></div> -->

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-group row m-0 mb-0 w-100 py-3">
                                       <div class="col-lg-8">
                                          <div class="row">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                   <label class="col-form-label col-lg-12" for="">定年制</label>
                                                   <div class="col-sm-12">
                                                      <div class="kt-radio-inline">
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="retirement_age" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_age=="あり") checked @endif> あり
                                                         <span></span>
                                                         </label>
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="retirement_age" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->retirement_age=="なし") checked @endif> なし
                                                         <span></span>
                                                         </label>
                                                      </div>
                                                      <label id="retirement_age-error" class="form-error" for="retirement_age"></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6">
                                                <div class="row">
                                                   <label class="col-form-label col-lg-12" for="">再雇用制度</label>
                                                   <div class="col-sm-12">
                                                      <div class="kt-radio-inline">
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="reemployment_system" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->reemployment_system=="あり") checked @endif> あり
                                                         <span></span>
                                                         </label>
                                                         <label class="kt-radio kt-radio--solid kt-radio--success">
                                                         <input type="radio" name="reemployment_system" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->reemployment_system=="なし") checked @endif> なし
                                                         <span></span>
                                                         </label>
                                                      </div>
                                                      <label id="reemployment_system-error" class="form-error" for="reemployment_system"></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-lg-4">
                                          <div class="row">
                                             <label class="col-form-label col-lg-12" for="">勤務延長</label>
                                             <div class="col-sm-12">
                                                <div class="kt-radio-inline">
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="duty_extension" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->duty_extension=="あり") checked @endif> あり
                                                   <span></span>
                                                   </label>
                                                   <label class="kt-radio kt-radio--solid kt-radio--success">
                                                   <input type="radio" name="duty_extension" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->duty_extension=="なし") checked @endif> なし
                                                   <span></span>
                                                   </label>
                                                </div>
                                                <label id="duty_extension-error" class="form-error" for="duty_extension"></label>
                                             </div>
                                          </div>
                                       </div>

                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row m-0 mb-0 w-100 py-3">
                                    <div class="col-lg-12">
                                       <div class="row align-items-center">
                                          <div class="col-lg-4">
                                             <div class="row m-0">
                                                <label class="col-lg-12 col-form-label" for="">一律</label>
                                                <div class="input-group mb-2">
                                                   <input @if($employee_intro_service_2 && $employee_intro_service_2->retirement_age=="なし") disabled @endif type="text" class="form-control form-control-sm number_field" id="retirement_age_details" name="retirement_age_details" placeholder=""
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->retirement_age_details }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳</div>
                                                      </div>
                                                </div>
                                                <label id="retirement_age_details-error" class="form-error" for="retirement_age_details"></label>
                                             </div>
                                          </div>
                                          <div class="col-lg-4">
                                             <div class="row m-0">
                                                <label class="col-lg-12 col-form-label" for="">上限</label>
                                                <div class="input-group mb-2">
                                                   <input @if($employee_intro_service_2 && $employee_intro_service_2->reemployment_system=="なし") disabled @endif type="text" class="form-control form-control-sm number_field" id="reemployment_system_details" name="reemployment_system_details" placeholder=""
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->reemployment_system_details }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳まで</div>
                                                      </div>
                                                </div>
                                                <label id="reemployment_system_details-error" class="form-error" for="reemployment_system_details"></label>
                                             </div>
                                          </div>
                                          <div class="col-lg-4">
                                             <div class="row m-0">
                                                <label class="col-form-label col-lg-12" for="">上限</label>
                                                <div class="input-group mb-2">
                                                   <input @if($employee_intro_service_2 && $employee_intro_service_2->duty_extension=="なし") disabled @endif type="text" class="form-control form-control-sm number_field" id="duty_extension_details" name="duty_extension_details" placeholder=""
                                                          @if($employee_intro_service_2) value="{{ $employee_intro_service_2->duty_extension_details }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">歳まで</div>
                                                    </div>
                                                </div>
                                                <label id="duty_extension_details-error" class="form-error" for="duty_extension_details"></label>
                                             </div>
                                          </div>
                                          </div>
                                       </div>

                                    </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row align-items-center m-0 mb-0 w-100 py-3">

                                    <label class="col-lg-3 col-form-label" for="">入居可能住宅</label>

                                    <div class="col-lg-9">
                                       <div class="row align-items-center content-box">
                                          <div class="col-lg-6">
                                             <div class="row flex-column">
                                                <label class="col-lg-12 col-form-label" for="">単身用</label>
                                                <div class="col-lg-12">
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_singles" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->housing_for_singles=="あり") checked @endif> あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_singles" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->housing_for_singles=="なし") checked @endif> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="housing_for_singles-error" class="form-error" for="housing_for_singles"></label>
                                                </div>
                                                <div class="dropdown-divider"></div>
                                                <label class="col-lg-12 col-form-label" for="">世帯用</label>
                                                <div class="col-lg-12">
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_households" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->housing_for_households=="あり") checked @endif> あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="housing_for_households" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->housing_for_households=="なし") checked @endif> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="housing_for_households-error" class="form-error" for="housing_for_households"></label>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="col-lg-6">
                                             <textarea name="housing_details" id="housing_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->housing_details }}@endif</textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row align-items-center m-0 mb-0 w-100 content-box py-3">

                                    <label class="col-lg-6 col-form-label" for="">利用可能託児施設</label>

                                    <div class="col-lg-6">
                                       <div class="row">
                                          {{-- <label class="col-lg-4 col-form-label" for="">単身用</label> --}}
                                          <div class="col-lg-12">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="childcare_facilities" value="あり" @if($employee_intro_service_2 && $employee_intro_service_2->childcare_facilities=="あり") checked @endif> あり
                                                <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                <input type="radio" name="childcare_facilities" value="なし" @if($employee_intro_service_2 && $employee_intro_service_2->childcare_facilities=="なし") checked @endif> なし
                                                <span></span>
                                                </label>
                                             </div>
                                             <label id="childcare_facilities-error" class="form-error" for="childcare_facilities"></label>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="col-12">
                                       <textarea @if($employee_intro_service_2 && $employee_intro_service_2->childcare_facilities=="なし") disabled @endif id="childcare_facilities_details" name="childcare_facilities_details" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_2){{ $employee_intro_service_2->childcare_facilities_details }}@endif</textarea>
                                    </div>

                                 </div>
                              </div>
                           </div>
                        </div>
                      </div>




                      <div class="col-lg-4">
                        <div class="bd-example form_container">
                                 <div class="form-section position-static">

                                    <h5 for="staticEmail" class="col-sm-4 col-form-label">６ 会社の情報</h5>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">就業場所</div>

                                       <div class="row flex-column content-box w-100 m-0">
                                          <div class="form-group align-items-center row m-0 w-100 py-3">
                                             <div class="col-sm-3">
                                                <label class="col-auto col-form-label" for="">従業員数</label>
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="number_of_employees" name="number_of_employees" placeholder=""
                                                          @if($employee_intro_service_3) value="{{ $employee_intro_service_3->number_of_employees }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                                <label id="number_of_employees-error" class="form-error" for="number_of_employees"></label>
                                             </div>
                                             <label class="col-auto col-form-label" for="">設立年</label>
                                             <div class="col-sm-5">
                                                <div class="row flex-nowrap align-items-center">
                                                   <div class="form-group mr-2">
                                                      <label for="exampleFormControlSelect1" style="white-space: nowrap;">日本の年号</label>
                                                      <select class="form-control form-control-sm px-0" id="japanese_era" name="japanese_era">
                                                        <option value="大正"  @if($employee_intro_service_3 && $employee_intro_service_3->japanese_era=="大正") selected @endif>大正</option>
                                                        <option value="昭和"  @if($employee_intro_service_3 && $employee_intro_service_3->japanese_era=="昭和") selected @endif>昭和</option>
                                                        <option value="平成"  @if($employee_intro_service_3 && $employee_intro_service_3->japanese_era=="平成") selected @endif>平成</option>
                                                        <option value="令和"  @if($employee_intro_service_3 && $employee_intro_service_3->japanese_era=="令和") selected @endif>令和</option>
                                                      </select>
                                                    </div>
                                                   <div class="col">
                                                      <div class="input-group" style="width:120px">
                                                         <input type="text" class="form-control form-control-sm" id="full_name" name="full_name"
                                                                @if($employee_intro_service_3) value="{{ $employee_intro_service_3->full_name }}" @endif>
                                                         <div class="input-group-append">
                                                            <div class="input-group-text">年</div>
                                                          </div>
                                                       </div>
                                                       <label id="full_name-error" class="form-error" for="full_name"></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="form-group align-items-center row m-0 w-100 py-3">
                                             <label class="col-auto col-form-label" for="">就業場所</label>
                                             <div class="col-sm-3">
                                                <div class="input-group mb-2">
                                                   <input type="text" name="work_place_name" class="form-control form-control-sm number_field" id="work_place_name" placeholder=""
                                                          @if($employee_intro_service_3) value="{{ $employee_intro_service_3->work_place_name }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                                <label id="number_of_employees-error" class="form-error" for="number_of_employees"></label>
                                             </div>
                                             <label class="col-auto col-form-label" for="">資本金</label>
                                             <div class="col-sm-5">
                                                <textarea name="capital" id="capital" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->capital }}@endif</textarea>
                                             </div>
                                          </div>

                                          <div class="form-group align-items-center row m-0 w-100 py-3">
                                             <label class="col-auto col-form-label" for="">うち女性</label>
                                             <div class="col-sm-3">
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="of_which_female" name="of_which_female" placeholder=""
                                                          @if($employee_intro_service_3) value="{{ $employee_intro_service_3->of_which_female }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                                <label id="of_which_female-error" class="form-error" for="of_which_female"></label>
                                             </div>
                                             <label class="col-auto col-form-label" for="">うちパート</label>
                                             <div class="col-sm-3">
                                                <div class="input-group mb-2">
                                                   <input type="text" class="form-control form-control-sm number_field" id="of_which_part" name="of_which_part" placeholder=""
                                                          @if($employee_intro_service_3) value="{{ $employee_intro_service_3->of_which_part }}" @endif>
                                                   <div class="input-group-append">
                                                      <div class="input-group-text">人</div>
                                                    </div>
                                                </div>
                                                <label id="of_which_part-error" class="form-error" for="of_which_part"></label>
                                             </div>
                                          </div>
                                          <div class="dropdown-divider my-0"></div>
                                          <div class="form-group align-items-center row m-0 py-3">
                                             <label class="col-lg-4 col-form-label" for="">労働組合</label>
                                             <div class="col-sm-8">
                                                <textarea name="union" id="union" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->union }}@endif</textarea>
                                             </div>
                                          </div>
                                       </div>

                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">事業内容</div>
                                       <div class="form-group row m-0 content-box w-100 py-3">
                                          <div class="col-sm-12">
                                             <textarea name="business_content" id="business_content" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->business_content }}@endif</textarea>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column form-label">
                                          <div class="position-relative pr-0">会社の</div>
                                          <div class="position-relative pr-0">特徴</div>
                                       </div>
                                       <div class="form-group row m-0 content-box w-100 py-3">
                                          <div class="col-sm-12">
                                             <textarea name="features_of_the_company" id="features_of_the_company" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->features_of_the_company }}@endif</textarea>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>


                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-group row flex-column m-0 py-3 w-100">

                                          <div class="form-group row mx-0 m-0 w-100">
                                             <label class="col-sm-4 col-form-label" for="">役職／ 代表者名</label>
                                             <div class="col-sm-4 mb-3">
                                                <div class="row flex-column">
                                                   <input type="text" class="form-control form-control-sm mb-3" id="position_name" name="position_name" placeholder="姓"
                                                          @if($employee_intro_service_3) value="{{ $employee_intro_service_3->position_name }}" @endif>
                                                    <input type="text" class="form-control form-control-sm" id="corporate_number" name="corporate_number" placeholder="役職"
                                                           @if($employee_intro_service_3) value="{{ $employee_intro_service_3->corporate_number }}" @endif>

                                                </div>
                                             </div>
                                             <div class="col-lg-4">

                                                 <input type="text" class="form-control form-control-sm" id="representative_name" name="representative_name" placeholder="名"
                                                        @if($employee_intro_service_3) value="{{ $employee_intro_service_3->representative_name }}" @endif>
                                             </div>
                                          </div>

                                          <div class="dropdown-divider my-0"></div>

                                          <div class="form-group row mx-0 mb-0 w-100 pt-3">
                                             <label class="col-sm-2 col-form-label" for="">就業規則</label>
                                             <div class="col-sm-5">
                                                <div class="row align-items-center m-0">
                                                   <label class="col-form-label mr-lg-3" for="">フルタイム</label>
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="full_time" value="上限あり" @if($employee_intro_service_3 && $employee_intro_service_3->full_time=="上限あり") checked @endif> 上限あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="full_time" value="なし" @if($employee_intro_service_3 && $employee_intro_service_3->full_time=="なし") checked @endif> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="full_time-error" class="form-error" for="full_time"></label>
                                                </div>
                                             </div>
                                             <div class="col-sm-5">
                                                <div class="row align-items-center m-0">
                                                   <label class="col-form-label mr-lg-3" for="">パートタイム</label>
                                                   <div class="kt-radio-inline">
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="part_time" value="上限あり" @if($employee_intro_service_3 && $employee_intro_service_3->part_time=="上限あり") checked @endif> 上限あり
                                                      <span></span>
                                                      </label>
                                                      <label class="kt-radio kt-radio--solid kt-radio--success">
                                                      <input type="radio" name="part_time" value="なし" @if($employee_intro_service_3 && $employee_intro_service_3->part_time=="なし") checked @endif> なし
                                                      <span></span>
                                                      </label>
                                                   </div>
                                                   <label id="part_time-error" class="form-error" for="part_time"></label>
                                                </div>
                                             </div>
                                       </div>

                                    </div>
                                 </div>

                                 <div class="dropdown-divider my-0"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-group row flex-column m-0 py-3 w-100">

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">育児休業取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="achievements_of_taking_childcare_leave" value="あり" @if($employee_intro_service_3 && $employee_intro_service_3->achievements_of_taking_childcare_leave=="あり") checked @endif> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="achievements_of_taking_childcare_leave" value="なし" @if($employee_intro_service_3 && $employee_intro_service_3->achievements_of_taking_childcare_leave=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="achievements_of_taking_childcare_leave-error" class="form-error" for="achievements_of_taking_childcare_leave"></label>
                                          </div>
                                       </div>

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">介護休業取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_care_leave_acquisition_record" value="あり" @if($employee_intro_service_3 && $employee_intro_service_3->nursing_care_leave_acquisition_record=="あり") checked @endif> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_care_leave_acquisition_record" value="なし" @if($employee_intro_service_3 && $employee_intro_service_3->nursing_care_leave_acquisition_record=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="nursing_care_leave_acquisition_record-error" class="form-error" for="nursing_care_leave_acquisition_record"></label>
                                          </div>
                                       </div>

                                       <div class="form-group row mx-0 mb-0 w-100">
                                          <label class="col-sm-4 col-form-label" for="">看護休暇取得実績</label>
                                          <div class="col-sm-8 mb-3">
                                             <div class="kt-radio-inline">
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_leave_acquisition_record" value="あり" @if($employee_intro_service_3 && $employee_intro_service_3->nursing_leave_acquisition_record=="あり") checked @endif> あり
                                                  <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--solid kt-radio--success">
                                                  <input type="radio" name="nursing_leave_acquisition_record" value="なし" @if($employee_intro_service_3 && $employee_intro_service_3->nursing_leave_acquisition_record=="なし") checked @endif> なし
                                                  <span></span>
                                                </label>
                                              </div>
                                              <label id="nursing_leave_acquisition_record-error" class="form-error" for="nursing_leave_acquisition_record"></label>
                                          </div>
                                       </div>

                                 </div>
                              </div>

                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex align-items-center">
                                 <div class="form-group row m-0 py-3 w-100">
                                    <label class="col-sm-4 col-form-label" for="">外国人雇用実績</label>
                                    <div class="col-sm-8">
                                       <div class="kt-radio-inline">
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                          <input type="radio" name="foreign_employment_record" value="あり" @if($employee_intro_service_3 && $employee_intro_service_3->foreign_employment_record=="あり") checked @endif> あり
                                          <span></span>
                                          </label>
                                          <label class="kt-radio kt-radio--solid kt-radio--success">
                                          <input type="radio" name="foreign_employment_record" value="不問" @if($employee_intro_service_3 && $employee_intro_service_3->foreign_employment_record=="不問") checked @endif> 不問
                                          <span></span>
                                          </label>
                                       </div>
                                       <label id="foreign_employment_record-error" class="form-error" for="foreign_employment_record"></label>
                                    </div>
                                 </div>
                              </div>


                              <div class="dropdown-divider my-0"></div>

                              <div class="form-section-column position-relative d-flex flex-column">
                                 <label class="col-sm-12 col-form-label" for="">求人に関する特記事項</label>
                                 <div class="col-lg-12 pb-3">
                                    <textarea name="special_notes_on_job_vacancies" id="special_notes_on_job_vacancies" class="form-control form-control-sm" rows="6" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->special_notes_on_job_vacancies }}@endif</textarea>
                                 </div>
                              </div>

                                 </div>
                         </div>
                     </div>








                     <div class="col-lg-4">
                        <div class="bd-example form_container">
                                 <div class="form-section position-static">
                                    <h5 for="staticEmail" class="col-sm-4 col-form-label">7 選考等</h5>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative pr-0">採用</div>
                                          <div class="position-relative pr-0">人数</div>
                                       </div>
                                       <div class="form-group row content-box m-0 py-3 w-100">
                                          <div class="col-sm-4">
                                             <div class="input-group mb-2">
                                                <input type="text" class="form-control form-control-sm" id="number_of_people_recruitment" name="number_of_people_recruitment" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->number_of_people_recruitment }}" @endif>
                                                <div class="input-group-append">
                                                   <div class="input-group-text">人</div>
                                                 </div>
                                             </div>
                                             <label id="number_of_people_recruitment-error" class="form-error" for="number_of_people_recruitment"></label>
                                          </div>

                                          <label class="col-lg-3 col-form-label" for="">募集理由</label>

                                          <div class="col-lg-5">
                                             <textarea name="reason_for_recruitment" id="reason_for_recruitment" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->reason_for_recruitment }}@endif</textarea>
                                          </div>

                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">

                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative pr-0">選考</div>
                                          <div class="position-relative pr-0">方法</div>
                                       </div>

                                       <div class="form-group row content-box m-0 py-3 w-100">
                                          <div class="row m-0 w-100">
                                             <div class="col-12">

                                                   <div class="row kt-checkbox-list row d-flex-wrap align-items-center radio-checkbox m-0 w-100">

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="書類選考" @if($employee_intro_service_3){{in_array("書類選考",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'checked':''}} @endif> 書類選考
                                                         <span></span>
                                                      </label>

                                                      <div class="col-auto kt-box mx-3 d-flex align-items-center">
                                                         <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                            <input type="checkbox" id="methodology_elective_exam_input" name="methodology_elective_exam[]" value="面接" @if($employee_intro_service_3){{in_array("面接",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'checked':''}} @endif> 面接
                                                            <span></span>
                                                         </label>

                                                         <label class="mr-3 mb-0" for="">予定</label>
                                                         <div class="col">
                                                            <div class="input-group" style="width:100px">
                                                               <input   @if($employee_intro_service_3){{in_array("面接",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'':'disabled'}} @else disabled @endif type="text" class="form-control form-control-sm" id="schedule" name="schedule" placeholder=""
                                                                        @if($employee_intro_service_3) value="{{ $employee_intro_service_3->schedule }}" @endif>
                                                               <div class="input-group-append">
                                                                  <div class="input-group-text">回</div>
                                                               </div>
                                                            </div>
                                                            <label id="schedule-error" class="form-error" for="schedule"></label>
                                                         </div>
                                                      </div>

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="筆記試験" @if($employee_intro_service_3){{in_array("筆記試験",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'checked':''}} @endif> 筆記試験
                                                         <span></span>
                                                      </label>

                                                      <label class="col-auto kt-box kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                         <input type="checkbox" name="methodology_elective_exam[]" value="その他" @if($employee_intro_service_3){{in_array("その他",explode(', ', $employee_intro_service_3->methodology_elective_exam))?'checked':''}} @endif> その他
                                                         <span></span>
                                                      </label>

                                                  </div>

                                                  <label id="methodology_elective_exam[]-error" class="form-error" for="methodology_elective_exam[]"></label>

                                             </div>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative">通知</div>
                                          <div class="position-relative pr-0">結果</div>
                                       </div>

                                       <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                          <label class="col-form-label col-12" for=""></label>
                                          <div class="col-lg-12">

                                             <div class="kt-checkbox-list d-flex flex d-flex-wrap align-items-center">
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="即決" @if($employee_intro_service_3){{in_array("即決",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> 即決
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="書類選考結果通知" @if($employee_intro_service_3){{in_array("書類選考結果通知",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> 書類選考結果通知
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                <input type="checkbox" name="result_announcement[]" value="面接選考結果通知" @if($employee_intro_service_3){{in_array("面接選考結果通知",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> 面接選考結果通知
                                                <span></span>
                                                </label>

                                                <div class="mr-3 d-flex align-items-center">
                                                   <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                      <input type="checkbox" id="after_the_book_toggle" name="result_announcement[]" value="書類到着後" @if($employee_intro_service_3){{in_array("書類到着後",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> 書類到着後
                                                      <span></span>
                                                   </label>
                                                   <div class="input-group d-inline-flex" style="width:120px">
                                                      <input @if($employee_intro_service_3){{in_array("書類到着後",explode(', ', $employee_intro_service_3->result_announcement))?'':'disabled'}} @else disabled @endif type="text" class="form-control form-control-sm" id="after_the_book" name="after_the_book" placeholder="0"
                                                             @if($employee_intro_service_3) value="{{ $employee_intro_service_3->after_the_book }}" @endif>
                                                      <div class="input-group-append">
                                                         <div class="input-group-text">日以内</div>
                                                       </div>
                                                   </div>
                                                </div>

                                                <div class="mr-3 d-flex align-items-center">
                                                   <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                      <input type="checkbox" id="after_the_face_toggle" name="result_announcement[]" value="面接後" @if($employee_intro_service_3){{in_array("面接後",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> 面接後
                                                      <span></span>
                                                   </label>
                                                   <div class="input-group d-inline-flex" style="width:120px">
                                                      <input  @if($employee_intro_service_3){{in_array("面接後",explode(', ', $employee_intro_service_3->result_announcement))?'':'disabled'}} @else disabled @endif type="text" class="form-control form-control-sm" id="after_the_face" name="after_the_face" placeholder="0"
                                                             @if($employee_intro_service_3) value="{{ $employee_intro_service_3->after_the_face }}" @endif>
                                                      <div class="input-group-append">
                                                         <div class="input-group-text">日以内</div>
                                                       </div>
                                                   </div>
                                                </div>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="result_announcement[]" value="その他" @if($employee_intro_service_3){{in_array("その他",explode(', ', $employee_intro_service_3->result_announcement))?'checked':''}} @endif> その他
                                                   <span></span>
                                                </label>

                                            </div>

                                            <label id="result_announcement[]-error" class="form-error" for="result_announcement[]"></label>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label d-flex justify-content-center flex-column">
                                          <div class="position-relative">通知</div>
                                          <div class="position-relative pr-0">方法</div>
                                       </div>

                                       <div class="form-group row flex-column content-box m-0 py-3 w-100">
                                          <div class="col-lg-12">


                                             <div class="kt-checkbox-list d-flex flex d-flex-wrap align-items-center">
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="求職者マイページに連絡" @if($employee_intro_service_3){{in_array("求職者マイページに連絡",explode(', ', $employee_intro_service_3->method_notification))?'checked':''}} @endif> 求職者マイページに連絡
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="郵送" @if($employee_intro_service_3){{in_array("郵送",explode(', ', $employee_intro_service_3->method_notification))?'checked':''}} @endif> 郵送
                                                   <span></span>
                                                </label>
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                <input type="checkbox" name="method_notification[]" value="電話" @if($employee_intro_service_3){{in_array("電話",explode(', ', $employee_intro_service_3->method_notification))?'checked':''}} @endif> 電話
                                                <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="Eメール" @if($employee_intro_service_3){{in_array("Eメール",explode(', ', $employee_intro_service_3->method_notification))?'checked':''}} @endif> Eメール
                                                   <span></span>
                                                </label>

                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr-3">
                                                   <input type="checkbox" name="method_notification[]" value="その他" @if($employee_intro_service_3){{in_array("その他",explode(', ', $employee_intro_service_3->method_notification))?'checked':''}} @endif> その他
                                                   <span></span>
                                                </label>

                                            </div>

                                            <label id="method_notification[]-error" class="form-error" for="method_notification[]"></label>

                                          </div>
                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">日時</div>
                                       <div class="form-group row content-box m-0 py-3 w-100">
                                          <label class="col-sm-4 col-form-label" for="">随時</label>
                                          <div class="col-lg-8">
                                             <textarea name="as_needed" id="as_needed" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3){{ $employee_intro_service_3->as_needed }}@endif</textarea>
                                          </div>
                                       </div>

                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">選考場所</div>
                                       <div class="form-group row m-0 pt-3 w-100 content-box">
                                          <div class="col-lg-12 pt-3">
                                             <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                   <div class="input-group-text">〒</div>
                                                 </div>
                                                <input type="text" class="form-control form-control-sm number_field" id="selection_place_code" name="selection_place_code" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->selection_place_code }}" @endif>
                                              </div>
                                          </div>

                                          <div class="col-lg-12 pt-3">
                                             <input type="text" class="form-control form-control-sm" id="selection_place" name="selection_place" placeholder=""
                                                    @if($employee_intro_service_3) value="{{ $employee_intro_service_3->selection_place }}" @endif>
                                          </div>

                                          <div class="col-lg-12 pt-3">
                                             <div class="row">
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">線</label>
                                                      <div class="col-lg-12">
                                                          <input type="text" class="form-control form-control-sm" id="string_1" name="string_1" placeholder=""
                                                                                    @if($employee_intro_service_3) value="{{ $employee_intro_service_3->string_1 }}" @endif>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">駅</label>
                                                      <div class="col-lg-12">
                                                          <input type="text" class="form-control form-control-sm" id="string_2" name="string_2" placeholder=""
                                                                                    @if($employee_intro_service_3) value="{{ $employee_intro_service_3->string_2 }}" @endif>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4">
                                                   <div class="row flex-column">
                                                      <label class="col-form-label col-lg-12" for="">徒歩</label>
                                                      <div class="col-lg-12">
                                                         <div class="input-group mb-2">
                                                            <input type="text" class="form-control form-control-sm" id="on_foot" name="on_foot" placeholder=""
                                                                   @if($employee_intro_service_3) value="{{ $employee_intro_service_3->on_foot }}" @endif>
                                                            <div class="input-group-append">
                                                               <div class="input-group-text">分</div>
                                                             </div>
                                                         </div>
                                                         <label id="on_foot-error" class="form-error" for="on_foot"></label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                       </div>
                                    </div>

                                    <div class="dropdown-divider my-0"></div>

                                    <div class="form-section-column position-relative d-flex align-items-center">
                                       <div class="form-label position-relative">応募書類等</div>
                                       <div class="form-group row flex-column content-box m-0 w-100">
                                          <div class="col-lg-12">
                                             <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                   <div class="input-group-text">〒</div>
                                                 </div>
                                                <input type="text" class="form-control form-control-sm" id="documents" name="documents" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->documents }}" @endif>
                                             </div>
                                             <label id="documents-error" class="form-error" for="documents"></label>
                                          </div>
                                       </div>
                                    </div>

                                 <div class="dropdown-divider"></div>

                                 <div class="form-section-column position-relative d-flex align-items-center">
                                    <div class="form-label position-relative">担当者</div>
                                    <div class="form-group row flex-column content-box m-0 w-100">

                                       <!-- <label class="col-form-label col-12" for="">フリガナ</label> -->

                                       <div class="col-sm-12">
                                          <input type="text" class="form-control form-control-sm" id="furigana_name" name="furigana_name" placeholder="フリガナ"
                                                 @if($employee_intro_service_3) value="{{ $employee_intro_service_3->furigana_name }}" @endif>
                                       </div><br>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-sm" id="name_furigana" name="name_furigana" placeholder="担当者フルネーム"
                                                   @if($employee_intro_service_3) value="{{ $employee_intro_service_3->name_furigana }}" @endif>
                                        </div>

                                       <div class="col-12 pt-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">電話番号</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="telephone_number" name="telephone_number" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->telephone_number }}" @endif>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 pt-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">FAX</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="fax" name="fax" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->fax }}" @endif>
                                             </div>
                                          </div>
                                       </div>

                                       <div class="col-12 pt-3 pb-3">
                                          <div class="row align-items-center">
                                             <div class="col-sm-4">
                                                <label for="" class="col-form-label">Eメール</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm" id="email" name="email" placeholder=""
                                                       @if($employee_intro_service_3) value="{{ $employee_intro_service_3->email }}" @endif>
                                             </div>
                                          </div>
                                       </div>

                                       <!-- <div class="col-12 pt-3">
                                          <textarea name="address" id="address" class="form-control form-control-sm" rows="4" placeholder="ここに入力してください">@if($employee_intro_service_3) {{ $employee_intro_service_3->address }} @endif</textarea>
                                       </div> -->

                                 </div>
                              </div>

                                 </div>
                         </div>
                     </div>

                  </div>



               <div class="text-center pt-5">
                  <button type="submit" class="btn btn-primary-default">保存</button>
               </div>

              </div>
            </div>
            <!-- End bordered tabs -->
          </div>

      </form>

      </div>

      <!-- <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <strong>Successfully submitted!</strong> The form is valid.
       </div> -->

</div>

@endsection
@section('script')
<!--end::Global Theme Bundle -->
<!--begin::Calendar -->
<!-- <script>
   $('#furigana_dp_1').datepicker({});
</script> -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{ asset('store/js/custom/settings.js?ver2') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/masked.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('store/js/localization/ja-messages.js') }}"> </script>
<!-- <script src="{{ asset('story/js/dropzonejs.js') }}" type="text/javascript"></script> -->

<script>
    var saveEmployeeIntroService  = '{{ route('save-employee-intro-service') }}';
// $('input[type=radio][name=bedStatus]').change(function() {
//     if (this.value == 'allot') {
//         alert("Allot Thai Gayo Bhai");
//     }
//     else if (this.value == 'transfer') {
//         alert("Transfer Thai Gayo");
//     }
// });

// Restricts input for each element in the set of matched elements to the given inputFilter.
(function($) {

   var now = new Date();

   var day = ("0" + now.getDate()).slice(-2);
   var month = ("0" + (now.getMonth() + 1)).slice(-2);

   var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

   $('#datePicker').val(today);

  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

$('.time').mask('00:00:00').attr('maxlength', 5);

// Install input filters.
$(".number_field").inputFilter(function(value) {
  return /^-?\d*$/.test(value);
});

$(document). ready(function() {

$('input[type=radio]').change(function() {
    if (this.name == 'appointment_of_full_time_employee' && this.value == '実績あり') {
      $('#appointment_of_full_time_employee_description').removeAttr('disabled')
      $('#appointment_of_full_time_employee_description').prop('required',true);
    }
    else if (this.name == 'appointment_of_full_time_employee' && this.value == 'なし') {
      $('#appointment_of_full_time_employee_description').attr('disabled', 'true')
      $('#appointment_of_full_time_employee_description').val('');
      $('#appointment_of_full_time_employee_description').prop('required',false);
      $('#appointment_of_full_time_employee_description').valid()
      // alert()
    } else if (this.name == 'working_style' && this.value == '派遣・請負') {
      $('#worker_dispatch_business_permit_number').removeAttr('disabled')
      $('#worker_dispatch_business_permit_number').prop('required',true);
    } else if (this.name == 'working_style' && this.value == '派遣・請負ではない') {
      $('#worker_dispatch_business_permit_number').attr('disabled', 'true');
      $('#worker_dispatch_business_permit_number').prop('required',false);
      $('#worker_dispatch_business_permit_number').valid()
      $('#worker_dispatch_business_permit_number').val('');
    } else if (this.name == 'employment_period' && this.value == '雇用期間の定めあり') {
      $('#contract_renewal_conditions').removeAttr('disabled')
      $('#contract_renewal_conditions').prop('required',true);
    } else if (this.name == 'employment_period' && this.value == '雇用期間の定めなし') {
      $('#contract_renewal_conditions').attr('disabled', 'true');
      $('#contract_renewal_conditions').prop('required',false);
      $('#contract_renewal_conditions').val('');
      $('#contract_renewal_conditions').valid()
    } else if (this.name == 'age_promotion' && this.value == 'あり') {
      $('#age_options').removeAttr('disabled')
      $('input[type=radio][name=age_limit_reason').prop('required',true);
    } else if (this.name == 'age_promotion' && this.value == 'なし') {
      $('#age_options').attr('disabled', 'true');
      $('input[type=radio][name=age_limit_reason').prop('checked', false);
      $('#age_limit_reason_details').val('');
    } else if (this.name == 'age_limit_reason' && this.value == '上限あり') {
      $('#age_limit_reason_details').prop('required',true);
      $('#age_limit_reason_details').removeAttr('disabled')
    } else if (this.name == 'age_limit_reason' && this.value == 'なし') {
      $('#age_limit_reason_details').prop('required',false);
      $('#age_limit_reason_details').attr('disabled', 'true');$('#age_limit_reason_details').val('')
    } else if (this.name == 'possibility_of_transfer' && this.value == 'あり') {
      $('#possibility_of_transfer_details').removeAttr('disabled')
      $('#possibility_of_transfer_details').prop('required',true);
    } else if (this.name == 'possibility_of_transfer' && this.value == 'なし') {
      $('#possibility_of_transfer_details').attr('disabled', 'true');
      $('#possibility_of_transfer_details').val('');
      $('#possibility_of_transfer_details').prop('required',false);
      $('#possibility_of_transfer_details').valid()
    } else if (this.name == 'academic_background' && this.value == '大学卒以上') {
      $('#academic_background_details').prop('required',false);
    } else if (this.name == 'academic_background' && this.value == 'その他') {
      $('#academic_background_details').prop('required',true);
    } else if (this.name == 'knowledge_skills' && this.value == '条件あり') {
      $('#knowledge_skills_details').removeAttr('disabled')
      $('#knowledge_skills_details').prop('required',true);
    } else if (this.name == 'knowledge_skills' && this.value == '不問') {
      $('#knowledge_skills_details').prop('required',false);
      $('#knowledge_skills_details').attr('disabled', 'true');
      $('#knowledge_skills_details').val('');
    } else if (this.name == 'pc_skills' && this.value == '必要') {
      $('#pc_skills_details').removeAttr('disabled')
      $('#pc_skills_details').prop('required',true);
    } else if (this.name == 'pc_skills' && this.value == '不要') {
      $('#pc_skills_details').attr('disabled', 'true');$('#pc_skills_details').val('');
      $('#pc_skills_details').prop('required',false);
    } else if (this.name == 'trial_period' && this.value == '試用期間あり') {
      $('#trial_period_details').prop('required',true);
      $('#trial_period_details').removeAttr('disabled')
    } else if (this.name == 'trial_period' && this.value == 'なし') {
      $('#trial_period_details').prop('required',false);
      $('#trial_period_details').attr('disabled', 'true');
      $('#trial_period_details').val('');
    } else if (this.name == 'overtime_pay' && this.value == 'あり') {
      $('#overtime_pay_details input').removeAttr('disabled')
      $('#overtime_pay_note').removeAttr('disabled')
      $('#overtime_pay_1').prop('required',true);
      $('#overtime_pay_2').prop('required',true);
    } else if (this.name == 'overtime_pay' && this.value == 'なし') {
      $('#overtime_pay_details input').attr('disabled', 'true');
        $('#overtime_pay_1').val('');
        $('#overtime_pay_2').val('');
        $('#overtime_pay_1').prop('required',false);
        $('#overtime_pay_2').prop('required',false);
        $('#overtime_pay_1').valid()
        $('#overtime_pay_2').valid()
      $('#overtime_pay_note').attr('disabled', 'true');$('#overtime_pay_note').val('');
    } else if (this.name == 'actual_expense_payment' && this.value == 'あり') {
      $('#actual_expense_payment_monthly').removeAttr('disabled')
      $('#actual_expense_payment_monthly').prop('required',true);
    } else if (this.name == 'actual_expense_payment' && this.value == 'なし') {
      $('#actual_expense_payment_monthly').attr('disabled', 'true');
      $('#actual_expense_payment_monthly').val('');
      $('#actual_expense_payment_monthly').prop('required',false);
      $('#actual_expense_payment_monthly').valid()
    } else if (this.name == 'wages' && this.value == '固定 (月末以外)') {
      $('#wages_details').removeAttr('disabled')
      $('#wages_details').prop('required',true);
      $('#wages_notes').prop('required',false);
    } else if (this.name == 'wages' && this.value == 'その他') {
      $('#wages_details').prop('required',false);
      $('#wages_details').attr('disabled', 'true');
      $('#wages_details').val('');
      $('#wages_details').valid()
    } else if (this.name == 'payment_date' && this.value == '固定 (月末以外)') {
      $('#payment_date_details').removeAttr('disabled')
      $('#payment_date_details').prop('required',true);
      $('#payment_date_notes').prop('required',false);
    } else if (this.name == 'payment_date' && this.value == 'その他') {
      $('#payment_date_details').prop('required',false);
      $('#payment_date_details').attr('disabled', 'true');
      $('#payment_date_details').val('');
      $('#payment_date_details').valid()
      $('#payment_date_notes').prop('required',true);
    } else if (this.name == 'salary_raise' && this.value == 'あり') {
      $('#salary_raise_details').removeAttr('disabled')
      $('#salary_raise_details').prop('required',true);
    } else if (this.name == 'salary_raise' && this.value == 'なし') {
      $('#salary_raise_details').attr('disabled', 'true');
      $('#salary_raise_details').val('');
      $('#salary_raise_details').prop('required',false);
      $('#salary_raise_details').valid()
    } else if (this.name == 'bonus' && this.value == 'あり') {
      $('#bonus_details').removeAttr('disabled')
      $('#bonus_details').prop('required',true);
    } else if (this.name == 'bonus' && this.value == 'なし') {
      $('#bonus_details').attr('disabled', 'true');
      $('#bonus_details').val('');
      $('#bonus_details').prop('required',false);
      $('#bonus_details').valid()
    }


    else if (this.name == 'overtime_work_hours' && this.value == '時間外労働あり') {
      $('#overtime_work_hours_details').removeAttr('disabled')
      $('#overtime_work_hours_details').prop('required',true);
    } else if (this.name == 'overtime_work_hours' && this.value == '時間外労働なし') {
      $('#overtime_work_hours_details').attr('disabled', 'true');
      $('#overtime_work_hours_details').val('');
      $('#overtime_work_hours_details').prop('required',false);
      $('#overtime_work_hours_details').valid()
    } else if (this.name == 'special_provisions' && this.value == 'あり') {
      $('#special_provisions_details').removeAttr('disabled')
      $('#special_provisions_details').prop('required',true);
    } else if (this.name == 'special_provisions' && this.value == 'なし') {
      $('#special_provisions_details').attr('disabled', 'true');
      $('#special_provisions_details').val('');
      $('#special_provisions_details').prop('required',false);
      $('#special_provisions_details').valid()
    } else if (this.name == 'childcare_facilities' && this.value == 'あり') {
      $('#childcare_facilities_details').removeAttr('disabled')
      $('#childcare_facilities_details').prop('required',true);
    } else if (this.name == 'childcare_facilities' && this.value == 'なし') {
      $('#childcare_facilities_details').attr('disabled', 'true');
      $('#childcare_facilities_details').val('');
      $('#childcare_facilities_details').prop('required',false);
      $('#childcare_facilities_details').valid()
    }

    else if (this.name == 'retirement_age' && this.value == 'あり') {
      $('#retirement_age_details').removeAttr('disabled')
      $('#retirement_age_details').prop('required',true);
    } else if (this.name == 'retirement_age' && this.value == 'なし') {
      $('#retirement_age_details').attr('disabled', 'true');
      $('#retirement_age_details').val('');
      $('#retirement_age_details').prop('required',false);
      $('#retirement_age_details').valid()
    } else if (this.name == 'reemployment_system' && this.value == 'あり') {
      $('#reemployment_system_details').removeAttr('disabled')
      $('#reemployment_system_details').prop('required',true);
    } else if (this.name == 'reemployment_system' && this.value == 'なし') {
      $('#reemployment_system_details').attr('disabled', 'true');
      $('#reemployment_system_details').val('');
      $('#reemployment_system_details').prop('required',false);
      $('#reemployment_system_details').valid()
    } else if (this.name == 'duty_extension' && this.value == 'あり') {
      $('#duty_extension_details').removeAttr('disabled')
      $('#duty_extension_details').prop('required',true);
    } else if (this.name == 'duty_extension' && this.value == 'なし') {
      $('#duty_extension_details').attr('disabled', 'true');
      $('#duty_extension_details').val('');
      $('#duty_extension_details').prop('required',false);
      $('#duty_extension_details').valid()
    } else if (this.name == 'methodology_elective_exam' && this.value == '書類選考') {
      $('#selected_book_examination_details').removeAttr('disabled')
    } else if (this.name == 'methodology_elective_exam' && this.value == '筆記試験' || 'その他') {
      $('#selected_book_examination_details').attr('disabled', 'true');
    }
});

$('input[type=checkbox]').change(function() {
if ($('#insurance_coverage_other').is(":checked"))
{
   $('#insurance_coverage_details').removeAttr('disabled')
   $('#insurance_coverage_details').prop('required',true);
} else {
   $('#insurance_coverage_details').attr('disabled', 'true');
   $('#insurance_coverage_details').val('')
   $('#insurance_coverage_details').prop('required',false);
   $('#insurance_coverage_details').valid()
}
});

$('input[type=checkbox]').change(function() {
if ($('#after_the_book_toggle').is(":checked"))
{
   $('#after_the_book').removeAttr('disabled')
   $('#after_the_book').prop('required',true);
} else {
   $('#after_the_book').attr('disabled', 'true');
   $('#after_the_book').val('')
   $('#after_the_book').prop('required',false);
   $('#after_the_book').valid()
}

if ($('#after_the_face_toggle').is(":checked"))
{
   $('#after_the_face').removeAttr('disabled')
   $('#after_the_face').prop('required',true);
} else {
   $('#after_the_face').attr('disabled', 'true');
   $('#after_the_face').val('')
   $('#after_the_face').prop('required',false);
   $('#after_the_face').valid()
}

});

$('input[type=checkbox]').change(function() {
if ($('#methodology_elective_exam_input').is(":checked"))
{
   $('#schedule').removeAttr('disabled')
   $('#schedule').prop('required',true);
} else {
   $('#schedule').attr('disabled', 'true');
   $('#schedule').val('')
   $('#schedule').prop('required',false);
   $('#schedule').valid()
}
});

$('input[type=radio]').change(function() {
   if (this.name == 'my_car_commute' && this.value == '可') {
   // if(this.val == )
       $('input[type=radio][name=parking]').removeAttr('disabled');
      $('input[type=radio][name=parking]').val() == ''
   } else if (this.name == 'my_car_commute' && this.value == '不可') {
      $('input[type=radio][name=parking').attr('disabled', 'true');
      $('input[type=radio][name=parking').prop('checked', false);
   }
});


var $form = $("form"),
  $successMsg = $(".alert");
   $.validator.addMethod("letters", function(value, element) {
  return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
});
$form.validate({
  errorClass: 'form-error',
  lang: 'ja',
  ignore: [],
  rules: {
      office_name_furigana: {
      required: true,
          maxlength:30,
    },
    furigana: {
      required: true,
        maxlength:30,
    },
    office_location_code: {
      required: true,
        maxlength:7,
    },
    office_location: {
          required: true,
        maxlength:100,
      },
      occupation: {
          required: true,
      },
      job_description: {
          required: true,
          maxlength:300,
      },
      work_place_code: {
          required: true,
          maxlength:7,
      },
      work_place: {
          required: true,
          maxlength:100,
      },
      employment_status: {
          required: true,
      },
      appointment_of_full_time_employee: {
          required: true,
      },
      appointment_of_full_time_employee_description:{
          maxlength:200,
      },
      worker_dispatch_business_permit_number:{
          maxlength:15,
      },
      contract_renewal_conditions:{
          maxlength:300,
      },
      possibility_of_transfer_details:{
          maxlength:100,
      },
      age_limit_reason_details:{
          maxlength:200,
      },
      academic_background_details:{
          maxlength:200,
      },
      knowledge_skills_details:{
          maxlength:200,
      },
      pc_skills_details:{
          maxlength:200,
      },

      trial_period_details:{
          maxlength:300,
      },

      working_style: {
          required: true,
      },
      employment_period: {
          required: true,
      },
      age_promotion: {
          required: true,
      },
      knowledge_skills: {
          required: true
      },
      pc_skills: {
          required: true
      },
    pharmacist_license: {
       required:true,
        maxlength:100,
    },
    ordinary_car_drivers_license: {
       required: true
    },
    trial_period: {
       required: true
    },
    monthly_amount_a: {
       required: true,
        maxlength:7,
    },
    monthly_amount_b: {
       required: true,
        maxlength:7,
    },
    average_working_days_per_month: {
       required: true,
        maxlength:2,
    },
    basic_pay_a: {
       required: true,
        maxlength:7,
    },
    basic_pay_b: {
       required: true,
        maxlength:7,
    },
    fixed_amount_1: {
        maxlength: 7
    },
      fixed_amount_1_field_1: {
          maxlength: 7
      },
      fixed_amount_1_field_2: {
          maxlength: 7
      },
    fixed_amount_2: {
        maxlength: 7
    },
      fixed_amount_2_field_1: {
          maxlength: 7
      },
      fixed_amount_2_field_2: {
          maxlength: 7
      },
    fixed_amount_3: {
        maxlength: 7
    },
      fixed_amount_3_field_1: {
          maxlength: 7
      },
      fixed_amount_3_field_2: {
          maxlength: 7
      },
    fixed_amount_4: {
        maxlength: 7
    },
      fixed_amount_4_field_1: {
          maxlength: 7
      },
      fixed_amount_4_field_2: {
          maxlength: 7
      },
    fixed_amount_5: {
        maxlength: 7
    },
      fixed_amount_5_field_1: {
          maxlength: 7
      },
      fixed_amount_5_field_2: {
          maxlength: 7
      },
    overtime_pay: {
       required: true
    },
      overtime_pay_1: {
          maxlength: 7
      },
      overtime_pay_2: {
          maxlength: 7
      },

    overtime_pay_note: {
       required: true,
        maxlength: 200
    },
    articles_with_allowance: {
       required: true,
        maxlength: 300
    },
    other_contents: {
       required: true,
        maxlength: 200
    },
    actual_expense_payment: {
       required: true
    },
      actual_expense_payment_monthly: {
          maxlength: 7
      },
      wages_details: {
          maxlength: 3
      },

    wages: {
       required: true
    },
    wages_notes: {
       required: true,
        maxlength: 100
    },
    payment_date: {
       required: true
    },
      payment_date_details: {
          maxlength: 2
      },
    payment_date_notes: {
       required: true,
        maxlength: 100
    },
    salary_raise: {
       required: true
    },
      salary_raise_details: {
          maxlength: 100
      },
      bonus_details: {
          maxlength: 100
      },

    bonus: {
       required: true
    },

    working_hours_1_from: {
       required: true
    },
    working_hours_1_to: {
       required: true
    },
    /* working_hours_2_from: {
       required: true
    },
    working_hours_2_to: {
       required: true
    }, */
    /* working_hours_3_from: {
       required: true
    },
    working_hours_3_to: {
       required: true
    }, */
    // working_hours_or_from: {
    //    required: true
    // },
    // working_hours_or_to: {
    //    required: true
    // },
    working_hours_between: {
       // required: true,
        maxlength: 5
    },
    working_hours_notes: {
       required: true,
        maxlength: 200
    },

    overtime_work_hours: {
       required: true
    },
      overtime_work_hours_details: {
          maxlength: 3
      },

    special_provisions: {
       required: true
    },
      special_provisions_details: {
          maxlength: 200
      },

    time_break: {
       required: true,
        maxlength: 3
    },
    annual_holidays: {
       required: true,
        maxlength: 3
    },
    holidays_etc: {
       required: true,
         maxlength: 200
    },
    annual_paid_vacation_days_after_6_months: {
       required: true,
        maxlength: 3
    },

    "insurance_coverage[]": {
       required: true
    },

      insurance_coverage_details: {
          maxlength: 7
    },
      retirement_benefits: {
          required: true
      },
    retirement_system: {
       required: true
    },

    "annuities_corporate[]": {
       required: true
    },
    retirement_age: {
       required: true
    },
    reemployment_system: {
       required: true
    },
    duty_extension: {
       required: true
    },
      retirement_age_details: {
          maxlength: 3
      },
      reemployment_system_details: {
          maxlength: 3
      },
      duty_extension_details: {
          maxlength: 3
      },


    housing_for_singles: {
       required: true
    },
    housing_for_households: {
       required: true
    },
    housing_details: {
       required: true,
        maxlength: 200
    },

    childcare_facilities: {
       required: true
    },
      childcare_facilities_details: {
          maxlength: 200
      },

    number_of_employees: {
       required: true,
         maxlength: 3
    },
    japanese_era: {
       required: true
    },
    full_name: {
       required: true,
        maxlength: 2
    },
   work_place_name: {
       required: true,
       maxlength: 3
    },
   capital: {
       required: true,
       maxlength: 20
      },
   union: {
      required: true,
       maxlength: 20
   },
    of_which_female: {
       required: true,
        maxlength: 3
    },
    of_which_part: {
       required: true,
        maxlength: 3
    },
    of_which_or: {
       required: true
    },

    business_content: {
       required: true,
        maxlength: 300
    },
    features_of_the_company: {
       required: true,
        maxlength: 300
    },
    position_name: {
       required: true,
        maxlength: 10
    },
    corporate_number: {
       required: true,
        maxlength: 10
    },
    representative_name: {
       required: true,
        maxlength: 15
    },
    full_time: {
       required: true
    },

    part_time: {
       required: true
    },
    achievements_of_taking_childcare_leave: {
       required: true
    },
    nursing_care_leave_acquisition_record: {
       required: true
    },
    nursing_leave_acquisition_record: {
       required: true
    },
    foreign_employment_record: {
       required: true
    },
    special_notes_on_job_vacancies: {
       required: true,
        maxlength: 500
    },
    number_of_people_recruitment: {
       required: true,
        maxlength: 3
    },

    reason_for_recruitment: {
       required: true,
        maxlength: 100
    },
    "methodology_elective_exam[]": {
       required: true
    },
      schedule: {
          maxlength: 1
      },
    "result_announcement[]": {
       required: true
    },
      after_the_book: {
          maxlength: 2
      },
      after_the_face: {
          maxlength: 2
      },
    "method_notification[]": {
       required: true
    },
    as_needed: {
       required: true,
        maxlength: 100
    },

    selection_place_code: {
       required: true,
        maxlength: 7
    },
    selection_place: {
       required: true,
        maxlength: 30
    },

    string_1: {
       required: true,
        maxlength: 7
    },
    string_2: {
       required: true,
        maxlength: 7,
    },
    on_foot: {
       required: true,
        maxlength: 3
    },
    documents: {
       required: true,
        maxlength: 7
    },
    name_furigana: {
       required: true,
        maxlength: 30
    },
      furigana_name: {
          required: true,
          maxlength: 7
      },
    telephone_number: {
       required: true,
        maxlength: 11
    },
    fax: {
       required: true,
        maxlength: 11
    },
    email: {
       required: true,
        maxlength: 30
    },
    // address: {
    //    required: true
    // },
    possibility_of_transfer: {
       required: true
    },
    my_car_commute: {
       required: true
    },
    parking: {
       required: true
    },
  },
  messages: {
      office_name_furigana: {
         required: "このフィールドは必須であるか、コンテンツの最大制限を超えています。"
      },
    furigana: {
      required: "この欄は入力必須事項です。"
    },
    office_location_code: {
      required: "この欄は入力必須事項です。"
    },
    office_location: {
      required: "この欄は入力必須事項です。"
      },
      occupation: {
         required: "この欄は入力必須事項です。"
      },
      job_description: {
         required: "この欄は入力必須事項です。"
      },
      appointment_of_full_time_employee_description: {
          required: "この欄は入力必須事項です。"
      },
      worker_dispatch_business_permit_number: {
          required: "この欄は入力必須事項です。"
      },
      contract_renewal_conditions: {
          required: "この欄は入力必須事項です。"
      },
      possibility_of_transfer_details: {
          required: "この欄は入力必須事項です。"
      },
      age_limit_reason_details:{
          required: "この欄は入力必須事項です。"
      },
      academic_background_details:{
          required: "この欄は入力必須事項です。"
      },
      knowledge_skills_details:{
          required: "この欄は入力必須事項です。"
      },
      pc_skills_details:{
          required: "この欄は入力必須事項です。"
      },

      trial_period_details:{
          required: "この欄は入力必須事項です。"
      },
      work_place_code: {
         required: "この欄は入力必須事項です。"
      },
      work_place: {
         required: "この欄は入力必須事項です。"
      },
      employment_status: {
         required: "この欄は入力必須事項です。"
      },
      appointment_of_full_time_employee: {
         required: "この欄は入力必須事項です。"
      },
      working_style: {
         required: "この欄は入力必須事項です。"
      },
      employment_period: {
         required: "この欄は入力必須事項です。"
      },
      age_promotion: {
         required: "この欄は入力必須事項です。"
      },
      knowledge_skills: {
         required: "この欄は入力必須事項です。"
      },
      pc_skills: {
         required: "この欄は入力必須事項です。"
      },
    pharmacist_license: {
      required: "この欄は入力必須事項です。"
    },
    ordinary_car_drivers_license: {
      required: "この欄は入力必須事項です。"
    },
    trial_period: {
      required: "この欄は入力必須事項です。"
    },
    monthly_amount_a: {
      required: "この欄は入力必須事項です。"
    },
    monthly_amount_b: {
      required: "この欄は入力必須事項です。"
    },
    average_working_days_per_month: {
      required: "この欄は入力必須事項です。"
    },
    basic_pay_a: {
      required: "この欄は入力必須事項です。"
    },
    basic_pay_b: {
      required: "この欄は入力必須事項です。"
    },
      fixed_amount_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_1_field_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_1_field_2: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_2: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_2_field_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_2_field_2: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_3: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_3_field_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_3_field_2: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_4: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_4_field_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_4_field_2: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_5: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_5_field_1: {
          required: "この欄は入力必須事項です。"
      },
      fixed_amount_5_field_2: {
          required: "この欄は入力必須事項です。"
      },
    overtime_pay: {
      required: "この欄は入力必須事項です。"
    },
      overtime_pay_1: {
          required: "この欄は入力必須事項です。"
      },
      overtime_pay_2: {
          required: "この欄は入力必須事項です。"
      },
    overtime_pay_note: {
      required: "この欄は入力必須事項です。"
    },
    articles_with_allowance: {
      required: "この欄は入力必須事項です。"
    },
    other_contents: {
      required: "この欄は入力必須事項です。"
    },
    actual_expense_payment: {
      required: "この欄は入力必須事項です。"
    },
      actual_expense_payment_monthly: {
          required: "この欄は入力必須事項です。"
      },
      wages_details: {
          required: "この欄は入力必須事項です。"
      },
    wages: {
      required: "この欄は入力必須事項です。"
    },
    wages_notes: {
      required: "この欄は入力必須事項です。"
    },
    payment_date: {
      required: "この欄は入力必須事項です。"
    },
      payment_date_details: {
          required: "この欄は入力必須事項です。"
      },
    payment_date_notes: {
      required: "この欄は入力必須事項です。"
    },
    salary_raise: {
      required: "この欄は入力必須事項です。"
    },
      salary_raise_details: {
          required: "この欄は入力必須事項です。"
      },
      bonus_details: {
          required: "この欄は入力必須事項です。"
      },
    bonus: {
      required: "この欄は入力必須事項です。"
    },

    working_hours_1_from: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_1_to: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_2_from: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_2_to: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_3_from: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_3_to: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_or_from: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_or_to: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_between: {
      required: "この欄は入力必須事項です。"
    },
    working_hours_notes: {
      required: "この欄は入力必須事項です。"
    },
    overtime_work_hours: {
      required: "この欄は入力必須事項です。"
    },
      overtime_work_hours_details: {
          required: "この欄は入力必須事項です。"
      },
    special_provisions: {
      required: "この欄は入力必須事項です。"
    },
      special_provisions_details: {
          required: "この欄は入力必須事項です。"
      },

      time_break: {
      required: "この欄は入力必須事項です。"
    },
    annual_holidays: {
      required: "この欄は入力必須事項です。"
    },
    holidays_etc: {
      required: "この欄は入力必須事項です。"
    },
    annual_paid_vacation_days_after_6_months: {
      required: "この欄は入力必須事項です。"
    },

    "insurance_coverage[]": {
      required: "この欄は入力必須事項です。"
    },
      insurance_coverage_details: {
          required: "この欄は入力必須事項です。"
      },
    retirement_benefits: {
      required: "この欄は入力必須事項です。"
    },

    retirement_system: {
      required: "この欄は入力必須事項です。"
    },

    "annuities_corporate[]": {
      required: "この欄は入力必須事項です。"
    },
    retirement_age: {
      required: "この欄は入力必須事項です。"
    },
    reemployment_system: {
      required: "この欄は入力必須事項です。"
    },
    duty_extension: {
      required: "この欄は入力必須事項です。"
    },
      retirement_age_details: {
          required: "この欄は入力必須事項です。"
      },
      reemployment_system_details: {
          required: "この欄は入力必須事項です。"
      },
      duty_extension_details: {
          required: "この欄は入力必須事項です。"
      },
    housing_for_singles: {
      required: "この欄は入力必須事項です。"
    },
    housing_for_households: {
       required: "この欄は入力必須事項です。"
    },
    housing_details: {
      required: "この欄は入力必須事項です。"
    },

    childcare_facilities: {
      required: "この欄は入力必須事項です。"
    },
      childcare_facilities_details: {
          required: "この欄は入力必須事項です。"
      },
    number_of_employees: {
      required: "この欄は入力必須事項です。"
    },
    japanese_era: {
      required: "この欄は入力必須事項です。"
    },
    full_name: {
      required: "この欄は入力必須事項です。"
    },
   work_place_name: {
      required: "この欄は入力必須事項です。"
    },
   capital: {
      required: "この欄は入力必須事項です。"
      },
   union: {
      required: "この欄は入力必須事項です。"
   },
    of_which_female: {
      required: "この欄は入力必須事項です。"
    },
    of_which_part: {
      required: "この欄は入力必須事項です。"
    },
    of_which_or: {
      required: "この欄は入力必須事項です。"
    },

    business_content: {
      required: "この欄は入力必須事項です。"
    },
    features_of_the_company: {
      required: "この欄は入力必須事項です。"
    },
    position_name: {
      required: "この欄は入力必須事項です。"
    },
    corporate_number: {
      required: "この欄は入力必須事項です。"
    },
    representative_name: {
      required: "この欄は入力必須事項です。"
    },
    full_time: {
      required: "この欄は入力必須事項です。"
    },

    part_time: {
      required: "この欄は入力必須事項です。"
    },
    achievements_of_taking_childcare_leave: {
      required: "この欄は入力必須事項です。"
    },
    nursing_care_leave_acquisition_record: {
      required: "この欄は入力必須事項です。"
    },
    nursing_leave_acquisition_record: {
      required: "この欄は入力必須事項です。"
    },
    foreign_employment_record: {
      required: "この欄は入力必須事項です。"
    },
    special_notes_on_job_vacancies: {
      required: "この欄は入力必須事項です。"
    },
    number_of_people_recruitment: {
      required: "この欄は入力必須事項です。"
    },

    reason_for_recruitment: {
      required: "この欄は入力必須事項です。"
    },
    "methodology_elective_exam[]": {
      required: "この欄は入力必須事項です。"
    },
      schedule: {
          required: "この欄は入力必須事項です。"
      },

    "result_announcement[]": {
      required: "この欄は入力必須事項です。"
    },
      after_the_book: {
          required: "この欄は入力必須事項です。"
      },
      after_the_face: {
          required: "この欄は入力必須事項です。"
      },
    "method_notification[]": {
      required: "この欄は入力必須事項です。"
    },
    as_needed: {
      required: "この欄は入力必須事項です。"
    },

    selection_place_code: {
      required: "この欄は入力必須事項です。"
    },
    selection_place: {
      required: "この欄は入力必須事項です。"
    },

    string_1: {
      required: "この欄は入力必須事項です。"
    },
    string_2: {
      required: "この欄は入力必須事項です。"
    },
    on_foot: {
      required: "この欄は入力必須事項です。"
    },
    documents: {
      required: "この欄は入力必須事項です。"
    },
    name_furigana: {
      required: "この欄は入力必須事項です。"
    },
      furigana_name: {
          required: "この欄は入力必須事項です。"
      },
    telephone_number: {
      required: "この欄は入力必須事項です。"
    },
    fax: {
      required: "この欄は入力必須事項です。"
    },
    email: {
      required: "この欄は入力必須事項です。"
    },
    // address: {
    //   required: "この欄は入力必須事項です。"
    // },
    possibility_of_transfer: {
      required: "この欄は入力必須事項です。"
    },
    my_car_commute: {
      required: "この欄は入力必須事項です。"
    },
    parking: {
      required: "この欄は入力必須事項です。"
    },

  },

    submitHandler: function (form) {
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            dataType : 'json'
        })
            .done(function (response) {
                if (response.status == true) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    setTimeout("window.close()",3000)
                } else {
                    //alert('fail');
                }
            });
        return false; // required to block normal submit since you used ajax
    }
});

let fixed_amt_1 = $('#fixed_amount_1')
let fixed_amt_2 = $('#fixed_amount_2')
let fixed_amt_3 = $('#fixed_amount_3')
let fixed_amt_4 = $('#fixed_amount_4')
let fixed_amt_5 = $('#fixed_amount_5')

$(fixed_amt_1).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#fixed_amount_1_field_1').removeAttr('disabled')
   $('#fixed_amount_1_field_2').removeAttr('disabled')
   }
});

$(fixed_amt_1).keyup(function(e) {
	 if(e.target.value.length === 0) {
   $('#fixed_amount_1_field_1').attr('disabled','true')
   $('#fixed_amount_1_field_2').attr('disabled','true')
   }
});

//////////

$(fixed_amt_2).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#fixed_amount_2_field_1').removeAttr('disabled')
   $('#fixed_amount_2_field_2').removeAttr('disabled')
   }
});

$(fixed_amt_2).keyup(function(e){
	 if(e.target.value.length === 0) {
   $('#fixed_amount_2_field_1').attr('disabled','true')
   $('#fixed_amount_2_field_2').attr('disabled','true')
   }
});

//////////

$(fixed_amt_3).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#fixed_amount_3_field_1').removeAttr('disabled')
   $('#fixed_amount_3_field_2').removeAttr('disabled')
   }
});

$(fixed_amt_3).keyup(function(e){
	 if(e.target.value.length === 0) {
   $('#fixed_amount_3_field_1').attr('disabled','true')
   $('#fixed_amount_3_field_2').attr('disabled','true')
   }
});

//////////

$(fixed_amt_4).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#fixed_amount_4_field_1').removeAttr('disabled')
   $('#fixed_amount_4_field_2').removeAttr('disabled')
   }
});

$(fixed_amt_4).keyup(function(e){
	 if(e.target.value.length === 0) {
   $('#fixed_amount_4_field_1').attr('disabled','true')
   $('#fixed_amount_4_field_2').attr('disabled','true')
   }
});

///////////

$(fixed_amt_5).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#fixed_amount_5_field_1').removeAttr('disabled')
   $('#fixed_amount_5_field_2').removeAttr('disabled')
   }
});

$(fixed_amt_5).keyup(function(e){
	 if(e.target.value.length === 0) {
   $('#fixed_amount_5_field_1').attr('disabled','true')
   $('#fixed_amount_5_field_2').attr('disabled','true')
   }
});

function count() {
   // var value = $(this).val();
    if ( fixed_amt_1.length > 0 ) {
      alert()
    }
}

});  // document.ready

</script>

<script>

   /* $(document).ready(function() {
      if($(working_hours_1_from).val().length != 0) {
         $('#working_hours_2_from').removeAttr('disabled')
         $('#working_hours_2_to').removeAttr('disabled')
      }
      if ($(working_hours_2_from).val().length != 0) {
         $('#working_hours_3_from').removeAttr('disabled')
         $('#working_hours_3_to').removeAttr('disabled')
      }
      if ($(working_hours_3_from).val().length != 0) {
         $('#working_hours_or_from').removeAttr('disabled')
         $('#working_hours_or_to').removeAttr('disabled')
      }
   }) */

   $(working_hours_1_from).keydown(function(e){
	 if(e.target.value.length != 0){
   $('#working_hours_2_from').removeAttr('disabled')
   $('#working_hours_2_to').removeAttr('disabled')
   }
   });

   $(working_hours_1_from).keyup(function(e) {
      if(e.target.value.length === 0) {
      $('#working_hours_2_from').attr('disabled','true')
      $('#working_hours_2_to').attr('disabled','true')
      }
   });

   /* //////////// */

   $(working_hours_2_from).keydown(function(e) {
	 if(e.target.value.length != 0){
   $('#working_hours_3_from').removeAttr('disabled')
   $('#working_hours_3_to').removeAttr('disabled')
   }
   });

   $(working_hours_2_from).keyup(function(e) {
      if(e.target.value.length === 0) {
      $('#working_hours_3_from').attr('disabled','true')
      $('#working_hours_3_to').attr('disabled','true')
      }
   });

      /* //////////// */

   $(working_hours_3_from).keydown(function(e) {
	 if(e.target.value.length != 0){
   $('#working_hours_or_from').removeAttr('disabled')
   $('#working_hours_or_to').removeAttr('disabled')
   }
   });

   $(working_hours_3_from).keyup(function(e) {
      if(e.target.value.length === 0) {
      $('#working_hours_or_from').attr('disabled','true')
      $('#working_hours_or_to').attr('disabled','true')
      }
   });

   /////////

   $(working_hours_or_to).keydown(function(e) {
	 if(e.target.value.length != 0){
   $('#working_hours_between').removeAttr('disabled')
   }
   });

   $(working_hours_or_to).keyup(function(e) {
      if(e.target.value.length === 0) {
      $('#working_hours_between').attr('disabled','true')
      }
   });

</script>
@endsection
