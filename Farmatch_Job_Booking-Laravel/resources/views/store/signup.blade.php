<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin | Drug Store Registration </title>
    <link rel="stylesheet" href="{{ asset('store/css/fullcalendar.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('store/css/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('store/css/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('store/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('store/css/style.css') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('store/images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('store/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('store/images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('store/images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('store/images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">
    <style>
        .error {
            color: red !important;
        }
    </style>
</head>
<body>
    <!-- <div class="media-screen"></div> -->
    <!-- <div class="media-screen-down"></div> -->
    <div class="container-fluid no-padding body-fluid">
    <div class="banner-large">
        <div class="logo">
        <img src="{{ asset('store/images/logo-white.png') }}" alt="" class="white-logo">
        </div>
    </div>
    <div class="container">
        <div class="account_container">
            <div class="inner-card">
                <h4 class="text-center"> 新規登録 </h4>
                <form id="signup-form" method="" action="" class="text-center form-top-pad">
                    <div class="form-group text-center">
                        <input class="form-control" type="text" name="name" placeholder="店舗名/病院名">
                    </div>
                    <div class="form-group text-center">
                        <input class="form-control" type="text" name="in_charge" placeholder="担当者名">
                    </div>
                    <div class="form-group text-center">
                        <input class="form-control" type="text" name="phone" id="phone" placeholder="携帯電話番号">
                    </div>
                    <div class="form-group text-center">
                        <input class="form-control" type="email" name="email" placeholder="メールアドレス">
                    </div>
                    <div class="form-group text-center">
                        <label for="staticEmail" class=""> Google Map </label>
                        <div style="display: none">
                            <input id="pac-input" name="pac_input" class="controls" type="text" placeholder="位置を入力する">
                        </div>
                        <div id="map" ></div>
                        <div id="infowindow-content">
                        <span id="place-name"  class="title"></span><br>
                        <span id="place-id"></span><br>
                        <span id="place-address"></span>
                        </div>
                        <div class="row mt-2 map-l-l-row">
                          <input id="latitude" name="latitude" class="form-control col-md-6 text-left" type="text" name="" placeholder="Latitude" readonly style="display: none;">
                          <input id="longitude" name="longitude" class="form-control col-md-6 text-left" type="text" name="" placeholder="Longitude" readonly style="display: none;">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <input class="form-control" id="password" type="password" name="password" placeholder="パスワード">
                    </div>
                    <div class="form-group text-center">
                        <input class="form-control" id="confirm_password" type="password" name="confirm_password" placeholder="パスワードを再入力">
                    </div>
                    <div class="text-left mb15">
                        <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                          <input type="checkbox" name="accept" >
                          <a href="#" id="" class="jp-letter"> <a target="_blank" href="https://yakuken.work/terms/"> 「利用規約」</a>・
                          <a target="_blank" href="https://yakuken.work/privacy/"> 「プライバシーポリシー」 </a> <a href="#" style="color: black;
                          pointer-events: inherit;">に同意する</a>
                          <span></span>
                        </label>
                    </div>
                    <button class="btn btn-primary-default btn-block register-click" type="submit"> 新規登録 </button>
                    <p class="b-text-decs mt30"> すでに登録済みの方はこちら <a href="{{ route('drug-store-login') }}" class="link-text-secondary jp-letter"> ログイン </a> </p>
                </form>
            </div>
        </div>
  </div>
</div>
<div class="container-fluid footer-fluid">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 ">
        <p class="text-center footer-copy"> 2020 © Farmatch </p>
      </div>
    </div>
  </div>
</div>
<script>
    var route       = '{{ route('store-sign-up-action') }}'
</script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('store/js/bootstrap.min.js') }}"></script>
<!-- Smooth Scrolling -->
<script src="{{ asset('store/js/Smoothscrolling.js') }}"></script>
<!-- Smooth Scrolling end -->
<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{ asset('store/js/plugins.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('store/js/dropzonejs.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{ asset('store/js/custom/signup.js') }}" type="text/javascript"></script>
<script>

    function initMap() {
      var map = new google.maps.Map(
          document.getElementById('map'),
          {center: {lat: -33.8688, lng: 151.2195}, zoom: 13});

      var input = document.getElementById('pac-input');
      var autocomplete = new google.maps.places.Autocomplete(input);

      autocomplete.bindTo('bounds', map);

      // Specify just the place data fields that you need.
      autocomplete.setFields(['place_id', 'geometry', 'name', 'formatted_address']);

      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      var infowindow = new google.maps.InfoWindow();
      var infowindowContent = document.getElementById('infowindow-content');
      infowindow.setContent(infowindowContent);

      var geocoder = new google.maps.Geocoder;

      var marker = new google.maps.Marker({map: map});
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        var place = autocomplete.getPlace();

        if (!place.place_id) {
          return;
        }
        geocoder.geocode({'placeId': place.place_id}, function(results, status) {
          if (status !== 'OK') {
            window.alert('Geocoder failed due to: ' + status);
            return;
          }

          map.setZoom(11);
          map.setCenter(results[0].geometry.location);

          // Set the position of the marker using the place ID and location.
          marker.setPlace(
              {placeId: place.place_id, location: results[0].geometry.location});

          marker.setVisible(true);

          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-id'].textContent = place.place_id;
          infowindowContent.children['place-address'].textContent = results[0].formatted_address;
          $("#latitude").val(place.geometry.location.lat());
          $("#longitude").val(place.geometry.location.lng());
          infowindow.open(map, marker);

        });
      });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwWz0oCfPS01cm74-5p2wYx2T7-sUKDT0&libraries=places&callback=initMap" async defer></script>
<!--end::Page Scripts -->
<!-- SVG Sprite -->
        <svg width="0" height="0" class="hidden">
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.35 18.355" id="settings" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M16.835 9.175a2.361 2.361 0 0 1 1.515-2.2 9.36 9.36 0 0 0-1.133-2.729 2.393 2.393 0 0 1-.961.205A2.356 2.356 0 0 1 14.1 1.133 9.331 9.331 0 0 0 11.378 0a2.359 2.359 0 0 1-4.406 0 9.36 9.36 0 0 0-2.729 1.133 2.356 2.356 0 0 1-2.155 3.316 2.315 2.315 0 0 1-.961-.205A9.567 9.567 0 0 0 0 6.977a2.361 2.361 0 0 1 0 4.406 9.36 9.36 0 0 0 1.133 2.729 2.357 2.357 0 0 1 3.111 3.111 9.414 9.414 0 0 0 2.729 1.133 2.355 2.355 0 0 1 4.4 0 9.36 9.36 0 0 0 2.727-1.134 2.359 2.359 0 0 1 3.111-3.111 9.414 9.414 0 0 0 1.133-2.729 2.373 2.373 0 0 1-1.509-2.207zm-7.617 3.818a3.823 3.823 0 1 1 3.823-3.823 3.822 3.822 0 0 1-3.823 3.823z"
            ></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.3 18.308" id="chat" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <g  >
            <path
                d="M13.75 12.424a8.254 8.254 0 0 0 3.335-2.425 5.179 5.179 0 0 0 0-6.683A8.257 8.257 0 0 0 13.75.891 11.711 11.711 0 0 0 9.15 0a11.712 11.712 0 0 0-4.6.891 8.257 8.257 0 0 0-3.335 2.425A5.25 5.25 0 0 0 0 6.657 5.109 5.109 0 0 0 .923 9.57a7.6 7.6 0 0 0 2.535 2.3q-.13.312-.267.572a3.3 3.3 0 0 1-.325.5q-.189.241-.293.377t-.338.384q-.234.247-.3.325c0-.009-.017.011-.052.059s-.054.069-.059.065-.022.017-.052.065l-.046.072-.033.065a.272.272 0 0 0-.026.078.509.509 0 0 0-.007.084.264.264 0 0 0 .013.084.432.432 0 0 0 .15.273.406.406 0 0 0 .267.1h.039a10.9 10.9 0 0 0 1.118-.208 10.937 10.937 0 0 0 3.615-1.664 13.058 13.058 0 0 0 2.288.208 11.7 11.7 0 0 0 4.6-.885zm-7.275-1.086l-.572.4q-.364.247-.806.507l.455-1.092-1.261-.728a5.978 5.978 0 0 1-1.937-1.716 3.542 3.542 0 0 1-.689-2.054 3.7 3.7 0 0 1 1.021-2.484 6.968 6.968 0 0 1 2.75-1.833 10.123 10.123 0 0 1 3.719-.676 10.123 10.123 0 0 1 3.719.676 6.972 6.972 0 0 1 2.75 1.833 3.532 3.532 0 0 1 0 4.967 6.967 6.967 0 0 1-2.75 1.833 10.121 10.121 0 0 1-3.719.676 11.26 11.26 0 0 1-1.989-.182z">
            </path>
            <path
                d="M22.376 12.907a5.075 5.075 0 0 0-.052-5.91 7.707 7.707 0 0 0-2.653-2.314 6.691 6.691 0 0 1-.572 5.279 8.7 8.7 0 0 1-2.5 2.756 11.513 11.513 0 0 1-3.433 1.677 13.737 13.737 0 0 1-4.018.585q-.39 0-1.144-.052a10.926 10.926 0 0 0 6.139 1.714 13.064 13.064 0 0 0 2.288-.208 10.383 10.383 0 0 0 4.733 1.872.392.392 0 0 0 .286-.091.481.481 0 0 0 .169-.286c0-.052 0-.081.013-.085s.011-.032-.007-.084l-.026-.078-.032-.065a.616.616 0 0 0-.045-.071.571.571 0 0 0-.052-.065c-.013-.013-.032-.035-.058-.065s-.043-.05-.052-.058q-.065-.078-.3-.325t-.338-.383q-.1-.137-.293-.377a3.286 3.286 0 0 1-.325-.5q-.136-.26-.267-.572a7.625 7.625 0 0 0 2.539-2.294z">
            </path>
            </g>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.723 15.723" id="check" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M15.723 7.861A7.862 7.862 0 1 1 7.861 0a7.862 7.862 0 0 1 7.862 7.861zm-8.771 4.163l5.833-5.833a.507.507 0 0 0 0-.717l-.717-.717a.507.507 0 0 0-.717 0L6.593 9.513 4.372 7.292a.507.507 0 0 0-.717 0l-.718.718a.507.507 0 0 0 0 .717l3.3 3.3a.507.507 0 0 0 .717 0z"
            ></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.75 18.75" id="clock" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M9.366 0a9.375 9.375 0 1 0 9.384 9.375A9.372 9.372 0 0 0 9.366 0zm.009 16.875a7.5 7.5 0 1 1 7.5-7.5 7.5 7.5 0 0 1-7.5 7.5z"
            ></path>
            <path d="M9.844 4.688H8.438v5.625l4.922 2.952.7-1.154-4.219-2.5z"></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.612 13.612" id="check-cross" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M6.806 0a6.806 6.806 0 1 0 6.806 6.806A6.8 6.8 0 0 0 6.806 0zm3.4 9.25l-.96.96-2.44-2.444-2.443 2.443-.96-.96 2.443-2.443L3.4 4.363l.96-.96 2.446 2.443L9.25 3.4l.96.96-2.444 2.446z"
            ></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.2 18" id="calander" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M5.4 8.1H3.6v1.8h1.8zm3.6 0H7.2v1.8H9zm3.6 0h-1.8v1.8h1.8zm1.8-6.3h-.9V0h-1.8v1.8H4.5V0H2.7v1.8h-.9A1.792 1.792 0 0 0 .009 3.6L0 16.2A1.8 1.8 0 0 0 1.8 18h12.6a1.805 1.805 0 0 0 1.8-1.8V3.6a1.805 1.805 0 0 0-1.8-1.8zm0 14.4H1.8V6.3h12.6z"
            ></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9.352 11.355" id="download" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path d="M9.352 4.008H6.68V0H2.672v4.008H0l4.676 4.676zM0 10.02v1.336h9.352V10.02z" ></path>
        </symbol>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.667 20.846" id="notification" fill="currentColor" stroke="none" stroke-width="0"
        stroke-linecap="round" stroke-linejoin="round" >
            <path
            d="M8.326 20.847c2.187 0 3.382-1.547 3.382-3.727H4.933c0 2.18 1.2 3.727 3.393 3.727zm8.13-6.593c-.8-.977-2.381-1.549-2.381-5.922 0-4.488-2.147-6.292-4.147-6.725-.188-.043-.323-.1-.323-.284v-.14a1.275 1.275 0 0 0-2.543 0v.14c0 .178-.135.241-.323.284-2.006.438-4.147 2.237-4.147 6.725 0 4.373-1.579 4.94-2.381 5.922a.952.952 0 0 0 .828 1.53h14.594a.952.952 0 0 0 .823-1.53z"
            ></path>
        </symbol>
        </svg>
    </body>
</html>
