@extends('layouts.store')
@section('page-content')
         <div class="container container-wide">
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
               <!-- begin:: Content -->
               <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid p0">
                  <!--Begin::App-->
                  <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
                     <!--Begin:: App Aside Mobile Toggle-->
                     <button class="kt-app__aside-close" id="kt_chat_aside_close">
                     <i class="la la-close"></i>
                     </button>
                     <!--End:: App Aside Mobile Toggle-->
                     <!--Begin:: App Content-->
                     <div class="kt-grid__item kt-grid__item--fluid kt-app__content p20 bg-white" id="kt_chat_content">
                        <div class="kt-chat">
                           <div class="kt-portlet kt-portlet--head-lg kt-portlet--last boxshadow-none">
                              <div class="kt-portlet__head">
                                 <div class="kt-chat__head ">
                                    <div class="kt-chat__left">
                                    </div>
                                    <div class="kt-chat__center">
                                       <div class="kt-chat__label">
                                          <a href="#" class="kt-chat__title"> ふぁーまっち運営本部 </a>
                                          <span class="kt-chat__status">
                                          <span class="kt-badge kt-badge--dot kt-badge--success"></span> アクティブ
                                          </span>
                                       </div>
                                    </div>
                                    <div class="kt-chat__right">
                                    </div>
                                 </div>
                              </div>
                              <div class="kt-portlet__body">
                                 <div class="kt-scroll kt-scroll--pull ps ps--active-y" data-mobile-height="300" style="height: 504px; overflow: hidden;" id="kt-scroll">
                                    <div class="kt-chat__messages">

                                    </div>
                                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                       <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                    </div>
                                    <div class="ps__rail-y" style="top: 0px; height: 504px; right: -2px;">
                                       <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 259px;"></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="kt-portlet__foot">
                                 <div class="kt-chat__input">
                                    <div class="kt-chat__editor">
                                       <textarea name="chat" id="chat" style="height: 30px" placeholder="メッセージを入力する …."></textarea>
                                    </div>
                                    <div class="kt-chat__toolbar">
                                       <div class="kt_chat__tools">
                                       </div>
                                       <div class="kt_chat__actions">
                                          <button onclick="chatSubmit();" type="button" class="btn btn-brand btn-md btn-upper btn-bold submitChat">返信</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--End:: App Content-->
                  </div>
                  <!--End::App-->
               </div>
               <!-- end:: Content -->
            </div>
         </div>
      </div>
      @include('layouts.fcm')
      @endsection
      @section('script')
      <script>
         $(document).ready(function(){
            $.ajax({
               url: '{{route("mark-read-admin")}}',
               type: 'post',
               data: {},
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function(data) {
                  console.log("read")
               }
            });
         });

         var image = '{{auth()->user()->main_image}}'
         if(image){
            var profile_image = base_url+"/images/drug_store/"+image;
         }else{
            var profile_image = placeholder;
         }



         const messageRef = firebase.database().ref('admin/drugstore/{{auth()->user()->id}}');

         //ondatachange
         messageRef.on('child_added', function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();
            var right = ''
            var temp_image = profile_image;
            var user = '管理者';
            if(!childData.admin){
               right = 'kt-chat__message--right';
               user = 'あなた';
            }else{
               temp_image = admin_image;
            }
            moment.locale('ja');
            var timestamp = moment.unix(childData.timestamp).fromNow();
            // console.log({childData});
            var html = `
               <div class="kt-chat__message ${right}">
                  <div class="kt-chat__user">
                     <span class="kt-media kt-media--circle kt-media--sm">
                     <img src="${temp_image}" alt="image">
                     </span>
                     <a href="#" class="kt-chat__username">${user}</a>
                     <span class="kt-chat__datetime" data-datetime="${childData.timestamp}">${timestamp}</span>
                  </div>
                  <div class="kt-chat__text kt-bg-light-success">
                     ${childData.message}
                  </div>
               </div>
            `;
            $('.kt-chat__messages').append(html);
            var d = $('#kt-scroll');
            d.scrollTop(d.prop("scrollHeight"));
         });

         setInterval(() => {
            $('.kt-widget__date').each(function(index, value) {
               var datetime = $(this).data('datetime');
               var timestamp = moment.unix(datetime).fromNow();
               $(this).html(timestamp);
            });
            $('.kt-chat__datetime').each(function(index, value) {
               var datetime = $(this).data('datetime');
               var timestamp = moment.unix(datetime).fromNow();
               $(this).html(timestamp);
            });
         }, 60000);

         function chatSubmit(){
            var chat = $('textarea[name="chat"]').val();
            if(chat.trim()){
               messageRef.push({
                  admin:0,
                  message:chat,
                  timestamp: Math.round((new Date()).getTime() / 1000)
               });
               $.ajax({
                  url:'{{route("save-chat")}}',
                  type:'post',
                  data: {message:chat},
                  headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data){
                     console.log(data);
                  }
               })
               $('textarea[name="chat"]').val('');
            }
         }

         var chat = document.getElementById("chat");
         chat.addEventListener("keydown", function (e) {
            if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
               chatSubmit();
            }
         });
       </script>
      @endsection
