<!doctype html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Invoice</title>
      <style type="text/css">
        .table td, .table th {
          vertical-align: center;
          border: 1px solid #000;
        }
        @font-face{
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            /* src:url('{{ storage_path('fonts/ipag.ttf') }}'); */
        }
        body {
            font-family: ipag;
        }
     </style>
     <style>
         .page-break {
            page-break-after: always;
         }
      </style>
   </head>
   <body>
      <table width="530">
         <tr align="right">
            <td>
               {{date('Y/m/d')}}
            </td>
         </tr>
         <tr align="right">
            <td>
                請求書番号: {{$invoice->id}}
            </td>
         </tr>
      </table>
      <table width="530">
        <tr align="center">
           <td colspan="2" >
            <font color="#000" size="6">請求書</font>
           </td>
        </tr>
     </table>
      <table width="530" cellpadding="15">
         <thead>
            <tr align="center">
               <td colspan="2" height="100">
                  <font color="#000" size="6"></font>
               </td>
            </tr>
         </thead>
         <tbody>
            <table width="530">
               <tr>
                  <td>
                     <table>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              {{$drugStoreName}} 様</font>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              件名：ふぁーまっち利用料として </font>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <font color="#000" size="2">
                              下記のとおりご請求申し上げます。 </font>
                           </td>
                        </tr>
                        <tr>
                           <td style="text-decoration: underline;">

                                @if($invoice->reduce_amount > 0 || $invoice->input_quantity > 0)
                                {{-- <tr>
                                    <td  colspan = "2" align="right"></td>
                                    <td  align="right">合計</td>
                                    <td align="right">{{round($totalAfterExtra, 2)}}</td>
                                </tr> --}}
                                <font color="#000" size="2">
                                    ご請求金額： ¥{{ round($totalAfterExtra,2) }}- </font><!-- total price -->
                               @else
                               <font color="#000" size="2">
                              ご請求金額： ¥{{ round($grandtotal,2) }}- </font><!-- total price -->
                              @endif
                           </td>
                        </tr>
                        <tr>
                           <td height="15"></td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              お支払い期限：{{ $last_date }}</font> <!-- dead line date -->
                           </td>
                        </tr>
                     </table>
                  </td>
                  <td>
                     <table align="right">
                        <tr>
                           <td><font color="#000" size="2">
                              株式会社薬けん </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              代表薬剤師　野中牧  </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              〒874-0909 </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              大分県別府市田の湯町3番7号  </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              アライアンスタワーZ4階 </font>
                           </td>
                        </tr>
                        <tr>
                           <td><font color="#000" size="2">
                              Tel:090-8400-8989 info@yakuken.work  </font>
                           </td>
                        </tr>
                     </table>
                  </td>
                  <td>
                     <table align="right">
                        <tr>
                           <td >
                              <img src="images/img_01.png" width="80">
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <img src="images/img_02.jpeg" width="80">
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </tbody>
         <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
            <thead>
               <tr>
                  <td bgcolor="#ccc">品番・品名</th>
                  <td bgcolor="#ccc">数量</th>
                  <td bgcolor="#ccc">単価</th>
                  <td bgcolor="#ccc">金額(税込)</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td align="left">ふぁーまっち月間掲載料</th>
                  <td align="right">1式</td>
                  <td align="right">2,200</td>
                  <td align="right">2,200</td>
               </tr>
               <tr align="left">
                  <td>マッチング手数料</th>
                  <td align="right">{{round($totalWorkHour,2)}} 時間</td>
                  <td align="right">110</td>
                  <td align="right">{{round($totalWage,2)}}</td>
               </tr>
               <tr align="left">
                <td>直前キャンセル費</th>
                <td align="right">{{ count($cancelledArray) }} 人</td>
                <td align="right">-</td>
                <td align="right">{{round($totalCancelWage,2)}}</td>
               </tr>
               <tr>
                  <td  colspan = "2" align="right"></td>
                  <td  align="right">合計</td>
                  <td align="right">{{ ($invoice->reduce_amount > 0 || $invoice->input_quantity > 0) ? round($grandtotal,2) : round($grandtotal) }}</td>
               </tr>
               @if($invoice->reduce_amount > 0)
               <tr align="left">
                <td>ふぁーまっち月間掲載料割引</th>
                <td align="right">1式</td>
                <td align="right">-{{ @$invoice->reduce_amount }}</td>
                <td align="right">-{{ @$invoice->reduce_amount }}</td>
               </tr>
               @endif
               @if($invoice->input_quantity > 0)
               <tr align="left">
                <td>{{ $invoice->input_text }}</th>
                <td align="right">{{ $invoice->input_quantity }} 式</td>
                <td align="right">{{ round($invoice->input_unit_price, 2) }} </td>
                <td align="right">{{ round($invoice->total_input, 2) }}</td>
               </tr>
               @endif
               @if($invoice->reduce_amount > 0 || $invoice->input_quantity > 0)
                <tr>
                    <td  colspan = "2" align="right"></td>
                    <td  align="right">合計</td>
                    <td align="right"><b>{{round($totalAfterExtra)}}</b></td>
                </tr>
               @endif
            </tbody>
         </table>
      </table>
      <br><br>
      @if(count($completedJobArray) > 0)
      <font color="#000" size="4">
        マッチング詳細  </font>
      <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
         <thead>
            <tr>
               <td align="center" bgcolor="#fbab60">薬剤師勤務日・薬剤師名</th>
               <td align="center" bgcolor="#fbab60">勤務時間</th>
            </tr>
         </thead>
         <tbody>
            @foreach($completedJobArray as $out)
            <tr>
               <td align="left">{{$out['date']}} {{$out['chemist']}}</td>
               <td align="right">{{round($out['totalWorkHour'],2)}}時間</td>
            </tr>
            @endforeach
            <tr>
               <td  colspan = "1" align="right">合計</td>
               <td  colspan = "1" align="right">{{round($totalJobHour,2)}}時間</td>
            </tr>
         </tbody>
      </table>
      <br><br>
      <div class="page-break"></div>
      @endif
      @if(count($cancelledArray) > 0)
      <font color="#000" size="4">
        直前キャンセル詳細  </font>
      <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
         <thead>
            <tr>
               <td align="center" bgcolor="#84a249">勤務日・薬剤師名</th>
               <td align="center" bgcolor="#84a249">金額(税込)</th>
            </tr>
         </thead>
         <tbody>
            @foreach($cancelledArray as $out)
            <tr>
               <td align="left">{{$out['date']}} {{$out['chemist']}}</td>
               {{-- <td align="center">{{round($out['totalWorkHour'],2)}}時間</td>
               <td align="center">{{ @$out['hourly_wage'] }}円</td> --}}
               <td align="center">{{round($out['total_cancel_wage'],2)}}円</td>
            </tr>
            @endforeach
            <tr>
               {{-- <td  colspan = "1" align="right">合計</td>
               <td  colspan = "1" align="center">{{ @$totalCancelHour }}時間</td> --}}
               <td  colspan = "1" align="right">合計</td>
               <td  colspan = "1" align="center">{{ round($totalCancelWage,2) }}円</td>
            </tr>
         </tbody>
      </table>
      <br><br>
      @endif
      <font color="#000" size="4">
        備考  </font>
      <table style="border: 1px solid #000; border-spacing: 0; font-size: 12px;" class="table" width="530" cellpadding="15">
         <tbody>
            <tr>
               <td style="height: 50px;">
                    {{ @$invoice->remarks }}
               </td>
            </tr>
         </tbody>
      </table>
      <br>
      <br>
      <br>
      <br>
      <br>
      <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <font color="#000"  size="2">お振込先：     </font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @if($invoice->bank_details == 'Default')
                                <font color="#000" size="2">大分銀行　別府支店　総合7570563 株式会社薬けん
                                </font>
                            @else
                                <font color="#000" size="2">大分みらい信用金庫 本店 普通 ９４５８９１２ 株式会社 薬けん
                                </font>
                            @endIf
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
   </body>
</html>
