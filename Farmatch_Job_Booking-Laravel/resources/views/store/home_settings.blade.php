@extends('layouts.store')
@section('page-content')

<style>
  @media(min-width: 992px) {
    .Required-Services {
      max-width: 800px;
    }
  }
</style>
         <div class="container container-wide">
            <div class="row">
               <div class="col-12 col-md-12 col-lg-3">
                  <div class="nav flex-column nav-pills s-left-nav" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                     <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
                        aria-controls="v-pills-profile" aria-selected="true"> プロフィール </a>
                     <a class="nav-link" id="v-pills-help-tab" data-toggle="pill" href="#v-pills-help" role="tab"
                        aria-controls="v-pills-help" aria-selected="false"> ヘルプ </a>
                     <a class="nav-link en" id="v-pills-faq-tab" data-toggle="pill" href="#v-pills-faq" role="tab"
                        aria-controls="v-pills-faq" aria-selected="false"> よくある質問 </a>
                     <a class="nav-link en" id="v-pills-qr-tab" data-toggle="pill" href="#v-pills-qr" role="tab"
                        aria-controls="v-pills-qr" aria-selected="false"> QRコード </a>
                  </div>
                </div>
                <div class="col-12 col-md-12 col-lg-9">
                  <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                      <div class="tab__container">
                        <ul class="nav nav-pills mb-3 inner-tab-section" id="pills-tab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="genaral-info-tab" data-toggle="pill" href="#genaral-info" role="tab"
                              aria-controls="genaral-info" aria-selected="true"> 一般情報</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="other-info-tab" data-toggle="pill" href="#other-info" role="tab"
                              aria-controls="other-info" aria-selected="false"> その他情報 </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="drug-store-info-tab" data-toggle="pill" href="#drug-store-info" role="tab"
                              aria-controls="drug-store-info" aria-selected="false" onclick="callLoader()"> 薬局/病院等 </a>
                          </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                          <div class="tab-pane fade show active " id="genaral-info" role="tabpanel"
                            aria-labelledby="genaral-info-tab">
                             <form id="update-profile-phase-1">
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 薬局/病院等 <span style="color: red">*</span> </label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" id="store_id" name="store_id" value="{{ $drugStore->id }}" style="display: none;">
                                  <input type="text" class="form-control" name="name" value="{{ $drugStore->name }}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 代表者名 <span style="color: red">*</span> </label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="manager_company" value="{{ $drugStore->manager_company }}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 担当者 <span style="color: red">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="in_charge" value="{{ $drugStore->in_charge }}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 担当者電話番号 <span style="color: red">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="phone" value="{{ $drugStore->phone }}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 施設電話番号 <span style="color: red">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="alter_phone" value="{{ $drugStore->alter_phone }}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> 所在地 <span style="color: red">*</span></label>
                                <div class="col-sm-9">
                                  <textarea name="address" id="address" class="form-control">{{ $drugStore->address }}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label"> メールアドレス <span style="color: red">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" name="email" value="{{ $drugStore->email }}">
                                </div>
                              </div>

                              <div class="Required-Services pt-1">
                                <h5 class="mt-4"> サービス利用の選択 </h5>
                                  <p class="mt25">各サービス利用希望の有無 <span style="color: red">*</span></p>

                              <p class="mt25 text-muted"> <svg width="18" height="18" class="mr-1" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M12 7L12 13" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                              <path d="M12 17.01L12.01 16.9989" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                              <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                              </svg>
                                  ふぁーまっち利用の開始は保存された当月から開始され、ふぁーまっち月額利用料が発生します。 利用の停止は翌月１日からのみ反映されます。
                              </p>

                                <div class="form-group row justify-content-lg-end mt-3">
                                <div class="col-sm-7">
                                <div class="kt-checkbox-list d-flex flex d-flex-wrap justify-content-lg-start">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                    <input type="checkbox" name="normal_farmatch" id="normal_farmatch" value="1"  @if($drugStore->normal_farmatch==1) checked @endif> ふぁーまっち利用
                                    <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                    <input type="checkbox" name="employee_intro_service" id="employee_intro_service" value="1"  @if($drugStore->employee_intro_service==1) checked @endif> 常勤やパートの紹介希望
                                    <span></span>
                                    </label>
                                </div>
                                </div>
                            </div>

                                  <div id="intro_content"  @if($drugStore->employee_intro_service !=1) style="display: none" @endif>
                                      <p class="mt25">常勤やパートの紹介希望の自動更新 <span style="color: red">*</span></p>

                                      <p class="mt25 text-muted"> <svg width="18" height="18" class="mr-1" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                              <path d="M12 7L12 13" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                                              <path d="M12 17.01L12.01 16.9989" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                                              <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>
                                          </svg> 希望するを選択すると毎年4月1日に自動更新されます。希望しない場合は3月31日に期限が切れます。</p>
                                      <div class="form-group row justify-content-lg-end mt-3">
                                          <div class="col-sm-7">
                                              <div class="kt-checkbox-list d-flex flex d-flex-wrap justify-content-lg-start">
                                                  <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                      <input type="radio" name="automatic_renewal" id="automatic_renewal_1" value="1"  @if($drugStore->employee_intro_automatic==1) checked @endif> 希望する
                                                      <span></span>
                                                  </label>
                                                  <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                                      <input type="radio" name="automatic_renewal" id="automatic_renewal_0" value="0"  @if($drugStore->employee_intro_automatic==0) checked @endif> 希望しない
                                                      <span></span>
                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <p class="mt-4 pb-5">常勤やパートの紹介希望の自動更新 <span style="color: red">*</span> <a class="ml-5" data-toggle="modal" data-target="#postModal" href="javascript:;" style="color: #1CB0E6">求人票を登録する <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg></a></p>

                                  </div>
                              </div>

                              <div class="dropdown-divider"></div>
                                 <div  id="inspection_content" @if($drugStore->normal_farmatch !=1) style="display: none" @endif>
                              <h5 class="mb25"> 見学適時の選択 </h5>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label">  見学可能曜日 </label>
                                        <div class="col-sm-9">
                                        <div class="kt-checkbox-list d-flex flex d-flex-wrap">
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Monday" {{ (in_array('Monday', $days)) ? "checked" : "" }}> 月曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Tuesday" {{ (in_array('Tuesday', $days)) ? "checked" : "" }}> 火曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Wednesday" {{ (in_array('Wednesday', $days)) ? "checked" : "" }}> 水曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Thursday" {{ (in_array('Thursday', $days)) ? "checked" : "" }}> 木曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Friday" {{ (in_array('Friday', $days)) ? "checked" : "" }}> 金曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Saturday" {{ (in_array('Saturday', $days)) ? "checked" : "" }}> 土曜日
                                            <span></span>
                                            </label>
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success mr30 mb20">
                                            <input type="checkbox" name="days[]" value="Sunday" {{ (in_array('Sunday', $days)) ? "checked" : "" }}> 日曜日
                                            <span></span>
                                            </label>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label"> 開始時間 </label>
                                        <div class="col-sm-5">
                                        <input class="form-control" id="kt_timepicker_1" name="time_from" readonly placeholder="Select time" type="text" value=" {{ ($fromTime) ? date('H:i', strtotime($fromTime)) : "08:30"}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label"> 終了時間</label>
                                        <div class="col-sm-5">
                                        <input class="form-control" id="kt_timepicker_1_modal" name="time_to" readonly placeholder="Select time" type="text" value=" {{ ($toTime) ? date('H:i', strtotime($toTime)) : "19:00"}}"/>
                                        </div>
                                    </div>
                                <hr class="mb50 mt50">
                                <div class="table-responsive interview-timings">
                                </div>
                                 </div>
                              <div class="form-group row ">
                                <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                  <button class="btn btn-primary-default" type="submit"> 保存 </button>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="other-info" role="tabpanel" aria-labelledby="other-info-tab">
                            <form id="update-profile-phase-2">
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 外観写真 <span style="color: red">*</span></label>
                                <div class="col-sm-8">
                                  <div class="kt-avatar kt-avatar--outline kt-avatar--danger" id="kt_user_avatar">
                                    <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->main_image) }})"></div>
                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type="file" name="main_image" id="main_image">
                                        <input type="text" name="main_image_validator" id="main_image_validator" style="display: none"  value="{{ $drugStore->main_image }}">
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">設定可能なファイルタイプ：2MB以下のpng、jpg、jpeg</span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 補足写真1（調剤室など） </label>
                                <div class="col-sm-8">
                                  <div class="kt-avatar kt-avatar--outline kt-avatar--danger" id="kt_user_avatar1">
                                    <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->sub_image1) }})"></div>
                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type="file" name="sub_image1" id="sub_image1">
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">設定可能なファイルタイプ：2MB以下のpng、jpg、jpeg</span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 補足写真2（スタッフなど） </label>
                                <div class="col-sm-8">
                                  <div class="kt-avatar kt-avatar--outline kt-avatar--danger" id="kt_user_avatar2">
                                    <div class="kt-avatar__holder" style="background-image: url({{ asset('images/drug_store/'.$drugStore->sub_image2) }})"></div>
                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type="file" name="sub_image2" id="sub_image2">
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">設定可能なファイルタイプ：2MB以下のpng、jpg、jpeg</span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 薬歴の機種 <span style="color: red">*</span></label>
                                <div class="col-sm-8">
                                  <div class="kt-radio-inline">
                                    <label class="kt-radio kt-radio--solid kt-radio--success">
                                      <input type="radio" name="history_model" value="EM system" {{ ($drugStore->history_model == 'EM system') ? "checked": ""  }}> EM system
                                      <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid kt-radio--success">
                                      <input type="radio" name="history_model" value="Gooco" {{ ($drugStore->history_model == 'Gooco') ? "checked": ""  }}> Gooco
                                      <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid kt-radio--success">
                                      <input type="radio" name="history_model" value="Melphin" {{ ($drugStore->history_model == 'Melphin') ? "checked": ""  }}> Melphin
                                      <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid kt-radio--success">
                                      <input type="radio" name="history_model" value="Others" {{ ($drugStore->history_model == 'その他') ? "checked": ""  }}> その他
                                      <span></span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 店舗情報 </label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" name="store_id" id="drug_store_id" value="{{ $drugStore->id }}" style="display: none;">
                                  <textarea id="store_information" class="form-control" name="store_information"  rows="5">{{ $drugStore->store_information }}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 従業員数 <span style="color: red">*</span></label>
                                <div class="col-sm-2">
                                  <input type="text" class="form-control" id="clerks" name="clerks" value="{{ $drugStore->clerks }}">
                                </div>
                                <label for="staticEmail" class="col-sm-2 col-form-label"> 事務員数 </label>
                                <div class="col-sm-2">
                                  <input type="text" class="form-control" id="chemists" name="chemists" value="{{ $drugStore->chemists }}">
                                </div>
                                <label for="staticEmail" class="col-sm-2 col-form-label"> 薬剤師数 </label>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> 返答目安時間 <span style="color: red">*</span></label>
                                <div class="col-sm-8">
                                  <select name="response_speed" id="response_speed" class="form-control">
                                      <option value="1日以内" {{ ($drugStore->respond_speed == '1日以内') ? "selected" : "" }}>1日以内	</option>
                                      <option value="２日以内" {{ ($drugStore->respond_speed == '２日以内') ? "selected" : "" }}>２日以内</option>
                                      <option value="３日以内" {{ ($drugStore->respond_speed == '３日以内') ? "selected" : "" }}>３日以内</option>
                                      <option value="３日以上" {{ ($drugStore->respond_speed == '３日以上') ? "selected" : "" }}>３日以上	</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label"> その他情報</label>
                                <div class="col-sm-8">
                                  <textarea class="form-control" name="other_info" id="other_info" rows="5">{{ $drugStore->other_information }}</textarea>
                                </div>
                              </div>
                              <div class="form-group text-center">
                                  <label for="staticEmail" class=""> 店舗位置情報を変更する </label>
                                  <div style="display: none">
                                      <input id="pac-input" style="height: 39px;width: 360px;z-index: 0;position: absolute;left: 188px;border: 0px;padding: 16px;box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;border-radius: 2px;top: 11px; !important" name="pac_input" class="controls" type="text" placeholder="位置を入力する">
                                  </div>
                                  <div id="map" style="height: 300px;"></div>
                                  <div id="infowindow-content">
                                  <span id="place-name"  class="title"></span><br>
                                  <span id="place-id"></span><br>
                                  <span id="place-address"></span>
                                  </div>
                                  <div class="row mt-2 map-l-l-row">
                                    <input id="latitude" name="latitude" class="form-control col-md-6 text-left" type="text" value="{{ $drugStore->latitude }}" placeholder="Latitude" readonly style="display: none">
                                    <input id="longitude" name="longitude" class="form-control col-md-6 text-left" type="text" value="{{ $drugStore->longitude }}" placeholder="Longitude" readonly style="display: none">
                                  </div>
                              </div>
                              <div class="form-group row ">
                                <label for="staticEmail" class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                  <button class="btn btn-primary-default " type="submit"> 保存 </button>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="drug-store-info" role="tabpanel" aria-labelledby="drug-store-info-tab">
                            <ul class="store-info">
                              <li>
                                <h1 class="counting" data-count="{{ @$experience }}"> 0 </h1>
                                <h6> ふぁーまっち利用年数</h6>
                              </li>
                              <li>
                                <h1 class="counting" data-count="{{ @$completedWorks }}"> 0 </h1>
                                <h6> 勤務回数 </h6>
                              </li>
                              <li>
                                <h1 class="counting" data-count="{{ @$chemistWorked }}"> 0 </h1>
                                <h6> 累計勤務人数 </h6>
                              </li>
                              <li>
                                <h1 class="counting" data-count="{{ @$repeatWorked }}"> 0 </h1>
                                <h6> 薬剤師リピート人数 </h6>
                              </li>
                              <li>
                                <h1 class="counting" data-count="{{ @$suddenCancellation }}"> 0 </h1>
                                <h6> 直前キャンセル件数 </h6>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="v-pills-help" role="tabpanel" aria-labelledby="v-pills-help-tab">
                        <div class="tab__container">
                           <h4 class="mb25"> ヘルプ </h4>
                           <div class="row justify-content-center">
                              <div class="col">
                                 <div class="text-center help__logo__sex">
                                    <img class="img-fluid" src="{{ asset('store/images/logo-color.png') }}" alt="" style="max-width: 25%;">
                                    <p>
                                       〒874-0909 大分県 別府市 田の湯町3-7, <br>
                                       アライアンスタワー 4F
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div class="row address__sec">
                              <div class="col-3 col-md-6 col-lg-3 text-center">
                                 <div class="icon__round">
                                    <svg class="icon">
                                       <use xlink:href="#mail"> </use>
                                    </svg>
                                 </div>
                                 <p> info@yakuken.work </p>
                              </div>
                              <div class="col-3 col-md-6 col-lg-3 text-center">
                                 <div class="icon__round">
                                    <svg class="icon">
                                       <use xlink:href="#phone"> </use>
                                    </svg>
                                 </div>
                                 <p> 090-8400-8989 </p>
                              </div>
                              <div class="col-3 col-md-6 col-lg-3 text-center">
                                 <div class="icon__round last">
                                    <svg class="icon">
                                       <use xlink:href="#globe"> </use>
                                    </svg>
                                 </div>
                                 <p> www.yakuken.work </p>
                              </div>
                              <div class="col-3 col-md-6 col-lg-3 text-center">
                                 <div class="icon__round last">
                                    <svg class="icon">
                                       <use xlink:href="#time"> </use>
                                    </svg>
                                 </div>
                                 <p> 10:00 - 17:00 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="v-pills-faq" role="tabpanel" aria-labelledby="v-pills-faq-tab">
                        <div class="tab__container">
                           <h4 class="mb25 en">  よくある質問  </h4>
                           <div class="accordion accordion-light  accordion-svg-icon" id="accordionExample7">
                              @foreach($faqs as $row)
                              <div class="card faq-card">
                                 <div class="card-header" id="headingOne{{ $loop->index }}">
                                 <div class="card-title " data-toggle="collapse" data-target="#collapseOne{{ $loop->index }}" aria-expanded="true"
                                       aria-controls="collapseOne{{ $loop->index }}">
                                       <svg class="icon faq-plus-icon">
                                          <use xlink:href="#plus"> </use>
                                       </svg>
                                       {{ $row->question }}
                                    </div>
                                 </div>
                                 <div id="collapseOne{{ $loop->index }}" class="collapse show" aria-labelledby="headingOne{{ $loop->index }}"
                                    data-parent="#accordionExample7">
                                    <div class="card-body">
                                       <p> {{ $row->answer }}</p>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="v-pills-qr" role="tabpanel" aria-labelledby="v-pills-qr-tab">
                        <div class="tab__container">
                           <div class="accordion accordion-light  accordion-svg-icon" id="accordionExample7">
                                <div class="row" style="page-break-after: auto;">
                                    <div class="col-md-12">
                                        <h4 class="para text-center mb-5 mt-5">{{ @$drugStore->name }}</h4>
                                        <div class="text-center">
                                            {!! QrCode::size(300)->generate($drugStore->id); !!}
                                        </div>
                                        <p class="text-center mt-5" style="margin-bottom: .5rem;">勤務完了後に薬剤師がこのQRコードをスキャンすると</p>
                                        <p class="text-center">薬剤師⽤アプリで勤務内容確認ができます。</p>
                                        <div class="text-center">
                                            <a href="{{ route('store-qr-code', $drugStore->id) }}">
                                                <img src="{{ asset('images/print-icon.png') }}" class="img-fluid" style="width: 60px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>










      <!-- Modal -->
      <div class="modal fade bd-example-modal-lg" id="postModal" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" data-dismiss="modal" class="close-header">
                        <svg class="icon">
                            <use xlink:href="#check-cross"> </use>
                        </svg>
                    </button>
                    <h3 class="modal-title" id="addeventLabel">求人票登録ページに移動します。<br>すべての必須項目を記入して保存してください。</h3>
                    <form id="post-form" class="mt-4">

                        <div class="form-group row ">
                            <div class="col-sm-6">
                                <a class="btn btn-primary-default btn-block mr10"  onclick="redirectToIntro()" > 登録に進む </a>
                            </div>
                            <div class="col-sm-6">
                              <button class="btn btn-secondary-default btn-block" data-dismiss="modal"> キャンセル </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Checked Modal -->
    <div class="modal fade bd-example-modal-lg" id="checkModal" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
      <div class="modal-dialog modal-md modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-body">
                  <button type="button" data-dismiss="modal" class="close-header">
                      <svg class="icon">
                          <use xlink:href="#check-cross"> </use>
                      </svg>
                  </button>
                  <h3 class="modal-title" id="addeventLabel"> ふぁーまっち利用の停止は翌月１日からのみ反映されます。利用を停止してもよろしいですか？  </h3>
                  <form id="post-form" class="mt-4">

                      <div class="form-group row ">
                          <div class="col-sm-6">
                              <a class="btn btn-primary-default btn-block mr10 confirmModalBtn" type="submit" data-action="confirm"> 停止する </a>
                          </div>
                          <div class="col-sm-6">
                            <button class="btn btn-secondary-default btn-block confirmModalBtn" data-dismiss="modal"  data-action="cancel"> キャンセル </button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>


<div class="modal fade bd-example-modal-lg" id="checkModalIntro" tabindex="-1" role="dialog" aria-labelledby="addeventLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="close-header">
                    <svg class="icon">
                        <use xlink:href="#check-cross"> </use>
                    </svg>
                </button>
                <h3 class="modal-title" id="addeventLabel"> 常勤やパートの紹介の利用を停止してもよろしいですか？  </h3>
                <form id="post-form" class="mt-4">

                    <div class="form-group row ">
                        <div class="col-sm-6">
                            <a class="btn btn-primary-default btn-block mr10 confirmModalBtnIntro" type="submit" data-action="confirm"> 停止する </a>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-secondary-default btn-block confirmModalBtnIntro" data-dismiss="modal"  data-action="cancel"> キャンセル </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



      @endsection
@section('script')
<!--end::Global Theme Bundle -->
<!--begin::Calendar -->
<script src="{{ asset('store/js/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script>
   var route              = '{{ route('update-store-profile') }}';
   var route2             = '{{ route('update-store-profile2') }}';
   var routeTiming        = '{{ route('add-timing') }}';
   var routeTimingDelete  = '{{ route('delete-timing') }}';
   var getTimings         = '{{ route('get-timings') }}';
   var routeEmployeeIntroService         = '{{ route('withdraw-employee-intro-service') }}';
   var routeNormalFarmatch         = '{{ route('withdraw-normal-farmatch') }}';
   var routeEmployeeIntroServiceForm         = '{{ route('employee-intro-service-form') }}';
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{ asset('store/js/custom/settings.js?ver2') }}" type="text/javascript"></script>
<script src="{{ asset('store/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('story/js/dropzonejs.js') }}" type="text/javascript"></script>

<script>
// number count for stats, using jQuery animate
function callLoader(){
   $('.counting').each(function() {
   var $this = $(this),
      countTo = $this.attr('data-count');
   $({ countNum: $this.text()}).animate({
      countNum: countTo
   },{
      duration: 1500,
      easing:'linear',
      step: function() {
         $this.text(Math.floor(this.countNum));
      },
      complete: function() {
         $this.text(this.countNum);
         //alert('finished');
      }
   });
   });
}
$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
$(document).ready(function(){
    setTimeout(function() {
        // alert();
        $("#pac-input").css("top", "10px !important");;
    }, 1000);

})
</script>
<script>

  function initMap() {
    var myLatlng = new google.maps.LatLng({{ $drugStore->latitude }}, {{ $drugStore->longitude }});
    var map = new google.maps.Map(
        document.getElementById('map'),
        {center: {lat: 36.204824, lng: 138.252924}, zoom: 5});

    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    // Specify just the place data fields that you need.
    autocomplete.setFields(['place_id', 'geometry', 'name', 'formatted_address']);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    var geocoder = new google.maps.Geocoder;

    var marker = new google.maps.Marker({map: map});
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      var place = autocomplete.getPlace();

      if (!place.place_id) {
        return;
      }
      geocoder.geocode({'placeId': place.place_id}, function(results, status) {
        if (status !== 'OK') {
          window.alert('Geocoder failed due to: ' + status);
          return;
        }

        map.setZoom(11);
        map.setCenter(results[0].geometry.location);

        // Set the position of the marker using the place ID and location.
        marker.setPlace(
            {placeId: place.place_id, location: results[0].geometry.location});

        marker.setVisible(true);

        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-id'].textContent = place.place_id;
        infowindowContent.children['place-address'].textContent = results[0].formatted_address;
        $("#latitude").val(place.geometry.location.lat());
        $("#longitude").val(place.geometry.location.lng());
        infowindow.open(map, marker);

      });
    });
    var marker = new google.maps.Marker({
    position: myLatlng,
    title:""
});

// To add the marker to the map, call setMap();
marker.setMap(map);
  }

  function redirectToIntro() {


      if($('#normal_farmatch').is(":checked"))
      {
          var normal_farmatch =1
      }
      else
      {
          var normal_farmatch =0
      }

      if($('#employee_intro_service').is(":checked"))
      {
          var employee_intro_service =1
      }
      else
      {
          var employee_intro_service =0
      }


      if($('#automatic_renewal_1').is(":checked"))
      {
          var automatic_renewal = 1
      }
      else if($('#automatic_renewal_0').is(":checked"))
      {
          var automatic_renewal = 0
      }
      else{}

      $.ajax({
          type: "POST",
          url: routeEmployeeIntroServiceForm,
          data:{ normal_farmatch:normal_farmatch, employee_intro_service:employee_intro_service,  automatic_renewal:automatic_renewal},
          dataType: "json",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data) {
              if(data.status==true)
              {
                  $('#postModal').modal('hide');
                  $('body').removeClass('modal-open');
                  $('.modal-backdrop').remove();
                window.open('/drug-store/employee-intro-service', '_blank');
                 // alert(data.message)
              }
          },
          error: function() {}
      });

     // window.open('/drug-store/employee-intro-service', '_blank');
  }

  function valueChanged()
  {
      if($('#employee_intro_service').is(":checked"))
          $("#intro_content").show();
      else
          $("#intro_content").hide();
  }

  // function valueChanged2() {
  //   $('#checkModal').modal('show');
  //     // if($('#normal_farmatch').is(":checked"))
  //     //     $("#inspection_content").show();
  //     // else
  //     //     $("#inspection_content").hide();
  // }


 </script>
 <script>

  $(document).on('click','.confirmModalBtn',function(e){ //modal
    e.preventDefault();
    var action = $(this).data('action');
    // $('#normal_farmatch').removeAttr('checked');
    if(action == 'cancel') {
      if($('#normal_farmatch').is(":checked")) {
        $("#normal_farmatch").prop("checked", true);
      }else {
        $("#normal_farmatch").prop("checked", false);
      }
    }else if(action == 'confirm') {
      if($('#normal_farmatch').is(":checked")) {

          $.ajax({
              type: "POST",
              url: routeNormalFarmatch,
              data:{ normal_farmatch:0},
              dataType: "json",
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data) {
                  if(data.status==true)
                  {
                      $("#inspection_content").hide();
                      $('#normal_farmatch').prop("checked", false);
                      // alert(data.message)
                  }
              },
              error: function() {}
          });

      }else {
        $("#inspection_content").show();
        $('#normal_farmatch').prop("checked", true);
      }
    }
    $('#checkModal').modal('hide');
  })



  $(document).on('click','.confirmModalBtnIntro',function(e){ //modal
      e.preventDefault();
      var action = $(this).data('action');
      // $('#normal_farmatch').removeAttr('checked');
      if(action == 'cancel') {
          if($('#employee_intro_service').is(":checked")) {
              $("#employee_intro_service").prop("checked", true);
          }else {
              $("#employee_intro_service").prop("checked", false);
          }
      }else if(action == 'confirm') {
          if($('#employee_intro_service').is(":checked")) {
              $.ajax({
                  type: "POST",
                  url: routeEmployeeIntroService,
                  data:{ employee_intro_service:0},
                  dataType: "json",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data) {
                     if(data.status==true)
                     {
                         $("#intro_content").hide();
                         $('#employee_intro_service').prop("checked", false);
                        // alert(data.message)
                     }
                  },
                  error: function() {}
              });

          }else {
              $("#intro_content").show();
              $('#employee_intro_service').prop("checked", true);
          }
      }
      $('#checkModalIntro').modal('hide');
  })



 </script>
<script>
   $(document).on('change','#normal_farmatch',function(e){
      e.preventDefault();
      if($(this).is(':checked')){
        $("#inspection_content").show();

      }else{
        $(this).prop("checked", true);
        $('#checkModal').modal('show');

      }
  });


   $(document).on('change','#employee_intro_service',function(e){
       e.preventDefault();
       if($(this).is(':checked')){
           $("#intro_content").show();

       }else{
           $(this).prop("checked", true);
           $('#checkModalIntro').modal('show');

       }
   });


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwWz0oCfPS01cm74-5p2wYx2T7-sUKDT0&libraries=places&callback=initMap" async defer></script>
@endsection
