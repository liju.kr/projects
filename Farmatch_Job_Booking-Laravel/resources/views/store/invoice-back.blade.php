<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>PDF</title>
<style>
   @page { 
      margin-left:18px;
      margin-right: 8px;
   }
   @font-face{
      font-family: ipag;
      font-style: normal;
      font-weight: normal;
      /* src:url('{{ storage_path('fonts/ipag.ttf') }}'); */
   }
   body {
      font-family: ipag;
   }
</style>
</head>
<body lang=EN-IN style='tab-interval:36.0pt'>
    <div class="WordContainer" style="margin:0 auto; display:table">
    <div class=WordSection1 style='width:100%'>
       <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="100%"
          style='width:100.0%;border-collapse:collapse;border:none;mso-yfti-tbllook:
          1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:
          none'>
          <tr>
             <td>
             <p style='text-align:right;margin-right:15px;'>{{ date('Y-m-d') }}</p>
             </td>
          </tr>
          <tr>
             <td>
                <p style='text-align:center;'>
                   <span style='font-size:22.0pt'>
                      　請求内容詳細
                   </span>
                </p>
             </td>
          </tr>
       </table>
       <p class=MsoNormal>
       </p>
       <div align=center>
          <table>
             <tr>
                <td width=349 style='width:261.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                      normal'><span lang=EN-US>{{ $drugStoreName }}</span></p>
                   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                      normal'>{{ $invoice->from_date }}<span lang=EN-US>&#12316;</span>{{ $invoice->to_date }}</p>
                </td>
                <td width=349 style='width:261.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                   <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
                      text-align:right;line-height:normal'>
                      <span style='mso-fareast-language:EN-IN;
                         mso-no-proof:yes'>
                         <img width=97 height=79
                            src="http://farmatch.work/store/images/logo-color.png" v:shapes="Picture_x0020_1"><![endif]>
                      </span>
                   </p>
                </td>
             </tr>
          </table>
       </div>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
          <o:p>&nbsp;</o:p>
       </p>
       <table class=MsoTableGrid  width="50%"
          style='width:30.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
          mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
          <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
             <td width=110 valign=top style='width:100.7pt;border:solid windowtext 1.0pt;
                mso-border-alt:solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:
                background2;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                200%'><span lang=EN-US>Name</span></p>
             </td>
             <td width=150 valign=top style='width:120.7pt;border:solid windowtext 1.0pt;
                border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>Hour</span></p>
             </td>
             <td width=150 valign=top style='width:120.7pt;border:solid windowtext 1.0pt;
                border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>Hourly Wage</span></p>
             </td>
             <td width=150 valign=top style='width:120.7pt;border:solid windowtext 1.0pt;
                border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
                solid windowtext .5pt;background:#AEAAAA;mso-background-themecolor:background2;
                mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>Total</span></p>
             </td>
          </tr>
          @foreach($output as $row)
          <tr style='mso-yfti-irow:1'>
             <td width=174 valign=top style='width:130.7pt;border:solid windowtext 1.0pt;
                border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>{{ $row['date'] }} - {{ $row['chemist'] }}</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>{{ $row['totalWorkHour'] }}</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>110</p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>{{ $row['total'] }}</p>
             </td>
          </tr>
          @endforeach
          <tr style='mso-yfti-irow:5'>
             <td width=174 valign=top style='width:130.7pt;border:solid windowtext 1.0pt;
                border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>Subscription Fee</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US></span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'> </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>{{ $invoice->commision }}</p>
             </td>
          </tr>
          <tr style='mso-yfti-irow:6'>
             <td width=174 valign=top style='width:130.7pt;border:none;border-left:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
                mso-border-left-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border:none;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
                mso-border-right-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>&#23567;&#35336;</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>{{ $invoice->sub_total }}</p>
             </td>
          </tr>
          <tr style='mso-yfti-irow:7'>
             <td width=174 valign=top style='width:130.7pt;border:none;border-left:solid windowtext 1.0pt;
                mso-border-left-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border:none;border-right:solid windowtext 1.0pt;
                mso-border-right-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>消費税(10%)</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>{{ $invoice->tax }}</p>
             </td>
          </tr>
          <tr style='mso-yfti-irow:8'>
             <td width=174 valign=top style='width:130.7pt;border:none;border-left:solid windowtext 1.0pt;
                mso-border-left-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border:none;border-right:solid windowtext 1.0pt;
                mso-border-right-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'><span lang=EN-US>&#21512;&#35336;</span></p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>{{ $invoice->total_price }}</p>
             </td>
          </tr>
          <tr style='mso-yfti-irow:9;mso-yfti-lastrow:yes'>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:
                none;mso-border-left-alt:solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
                padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
             <td width=174 valign=top style='width:130.7pt;border-top:none;border-left:
                none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                   200%'>
                   <o:p>&nbsp;</o:p>
                </p>
             </td>
          </tr>
       </table>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
          <o:p>&nbsp;</o:p>
       </p>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
         Address : {{ $drugStoreAddress }}</o:p>
       </p>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>
         Back Account Details : </o:p>
       </p> 
    </div>
 </body>
</html>