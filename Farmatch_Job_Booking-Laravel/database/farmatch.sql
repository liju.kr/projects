/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.4.8-MariaDB : Database - farmatch
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`farmatch` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `farmatch`;

/*Table structure for table `chemist_interview_timings` */

DROP TABLE IF EXISTS `chemist_interview_timings`;

CREATE TABLE `chemist_interview_timings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chemist_id` bigint(20) unsigned NOT NULL,
  `job_post_id` bigint(20) unsigned NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chemist_interview_timings_chemist_id_foreign` (`chemist_id`),
  KEY `chemist_interview_timings_job_post_id_foreign` (`job_post_id`),
  CONSTRAINT `chemist_interview_timings_chemist_id_foreign` FOREIGN KEY (`chemist_id`) REFERENCES `chemists` (`id`),
  CONSTRAINT `chemist_interview_timings_job_post_id_foreign` FOREIGN KEY (`job_post_id`) REFERENCES `job_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `chemist_interview_timings` */

insert  into `chemist_interview_timings`(`id`,`chemist_id`,`job_post_id`,`day`,`time_from`,`time_to`,`created_at`,`updated_at`,`deleted_at`) values 
(2,1,5,'Monday','02:00:00','04:00:00','2020-01-15 19:31:36','2020-01-15 19:31:36',NULL),
(3,28,5,'Monday','02:00:00','04:00:00','2020-01-20 15:15:34','2020-01-20 15:15:34',NULL);

/*Table structure for table `chemist_job_applications` */

DROP TABLE IF EXISTS `chemist_job_applications`;

CREATE TABLE `chemist_job_applications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chemist_id` bigint(20) unsigned NOT NULL,
  `job_post_id` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `rest_time` int(11) NOT NULL,
  `transportation_cost` int(11) NOT NULL,
  `status` enum('Accepted','Rejected','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chemist_job_applications_chemist_id_foreign` (`chemist_id`),
  KEY `chemist_job_applications_job_post_id_foreign` (`job_post_id`),
  CONSTRAINT `chemist_job_applications_chemist_id_foreign` FOREIGN KEY (`chemist_id`) REFERENCES `chemists` (`id`),
  CONSTRAINT `chemist_job_applications_job_post_id_foreign` FOREIGN KEY (`job_post_id`) REFERENCES `job_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `chemist_job_applications` */

insert  into `chemist_job_applications`(`id`,`chemist_id`,`job_post_id`,`date`,`day`,`time_from`,`time_to`,`rest_time`,`transportation_cost`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,33,5,'2020-01-15','Monday','02:00:00','04:00:00',1,1000,'Rejected','2020-01-15 19:57:50','2020-01-25 19:36:50',NULL),
(3,33,5,'2020-01-17','Friday','02:00:00','04:00:00',1,1000,'Pending',NULL,NULL,NULL),
(4,33,5,'2020-01-17','Friday','02:00:00','04:00:00',1,500,'Pending',NULL,NULL,NULL);

/*Table structure for table `chemist_reviews` */

DROP TABLE IF EXISTS `chemist_reviews`;

CREATE TABLE `chemist_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attitude` int(11) NOT NULL,
  `adapting` int(11) NOT NULL,
  `work_speed` int(11) NOT NULL,
  `value_for_money` int(11) NOT NULL,
  `others` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `chemist_reviews` */

insert  into `chemist_reviews`(`id`,`attitude`,`adapting`,`work_speed`,`value_for_money`,`others`,`created_at`,`updated_at`,`deleted_at`) values 
(1,4,4,4,4,'dfgdfghdfhdfhgfhgfhfghgfhgfhgf ghgfhgfhgfh',NULL,NULL,NULL),
(2,4,3,4,3,'dfghdfhgfhfghgfhfgh',NULL,NULL,NULL),
(3,4,3,4,3,'dfghgfhgfhgf',NULL,NULL,NULL);

/*Table structure for table `chemists` */

DROP TABLE IF EXISTS `chemists`;

CREATE TABLE `chemists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licence_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_year` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_month` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '1234',
  `status` enum('Approved','Pending','Suspended') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `chemists` */

insert  into `chemists`(`id`,`first_name`,`last_name`,`gender`,`post_code`,`phone`,`email`,`licence_number`,`registration_year`,`registration_month`,`image`,`password`,`otp`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Akhil','P P','Male','680616','8891939751','akhil.pp@iroidtechnologies.com','1234567895','2018','March','1823122670.jpeg','$2y$10$zMUvmFLjO5iHspMAhdVIRevEy/jl4U0BxwGJZzpC1UhQH4yLmbP0u','1234','Pending','2019-12-31 12:33:20','2020-01-07 13:51:09',NULL),
(2,'Ajith','P J','Male','680616','8891939758','ajith@iroidtechnologies.com','1234567855','2018','April','9527097.jpg',NULL,'1234','Approved','2019-12-31 12:33:23','2020-01-02 21:46:52',NULL),
(3,'Issac','Peter','Male','680616','8891256325','issac@iroidtechnologies.com','123456856','2018','June','892841380.jpg',NULL,'1234','Suspended','2019-12-31 12:33:25','2020-01-02 21:48:42',NULL),
(4,'Anis','M V','Male','680616','8891356985','anis@iroidtechnologies.com','1252365256','2018','December','631949454.JPG',NULL,'1234','Suspended','2019-12-31 12:33:27','2020-01-02 21:49:39',NULL),
(5,'jayesh','M','Male','680616','8856985623','jayesh@iroidtechnologies.com','1235685566','2018','October','1993296122.jpg',NULL,'1234','Suspended','2019-12-31 12:33:30','2020-01-02 21:50:32',NULL),
(15,'test','cs','Male','683105','9633310024','nazeer28@gmail.com','7787','2020','January',NULL,'$2y$10$pqBQZK0htjKZWxSlcoJ6.uFLoMH2rbyfWhJ9SQ65yzixS4zPxQSQ.','1234','Pending','2020-01-06 16:56:19','2020-01-06 16:56:19',NULL),
(16,'NAZEER','C S','Male','683105','9633310026','nazeercsinfo@gmail.com','6789','2019','December',NULL,'$2y$10$afFWlJNdZ2iv7bfmH3.dvePTVwJOwRM0uVD5Rm9NMPpsCinWgdMSO','1234','Pending','2020-01-06 16:57:58','2020-01-06 16:57:58',NULL),
(17,'test','cs','Male','683105','9633310022','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$GRfl1VoQUIIz4liiXh2U2.EkokFxQjJA2yJl2W6RltM0WwR1eUmpu','1234','Pending','2020-01-06 17:45:38','2020-01-06 17:45:38',NULL),
(18,'test','cs','Male','683105','9633310023','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$tPUEUWUf/sLnvff5uQdezeGA8TDBmjOoH8GZx/Yp.tuvqcdPdhd1W','1234','Pending','2020-01-06 17:52:01','2020-01-06 17:52:01',NULL),
(19,'test','cs','Male','683105','9633310013','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$bleHh3Z5vFDQaRaka3J3D../SsQywvoqubkTrwpO5b.2zfcFG/iUu','1234','Pending','2020-01-06 17:56:17','2020-01-06 17:56:17',NULL),
(20,'test','cs','Male','683105','9633310014','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$CPGhNgJf3GnJ62q8h0MffO3HlJdk55FMra6gKpGWNmvSSQeA/NQNW','1234','Pending','2020-01-06 17:57:32','2020-01-06 17:57:32',NULL),
(21,'test','cs','Male','683105','9633310015','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$/0hbLI1bG.qJbPR4jcbBwOmj/WYzPZ9GoW.fiiMAdFNMW6PJhwDOq','1234','Pending','2020-01-06 17:58:31','2020-01-06 17:58:31',NULL),
(22,'test','cs','Male','683105','9633310017','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$eMba.UEacAQ.BWNpF1qF4.igcI/sHOug0FARfqH0hqOFt9O3EglDC','1234','Pending','2020-01-06 18:02:28','2020-01-06 18:02:28',NULL),
(23,'test','cs','Male','683105','9633310011','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$grAknGjYSkfSn7vsfMKRbOLi9zyptd0dsNB2YgC4IMHBYuv9m38mK','1234','Pending','2020-01-06 18:06:29','2020-01-06 18:06:29',NULL),
(24,'test','cs','Male','683105','9633310010','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$W3nsvsY84xHM0IbHYLlc2.6/5HQZkldhVoPijope2vMn6qVoISDl.','1234','Pending','2020-01-06 18:09:06','2020-01-06 18:09:06',NULL),
(25,'test','cs','Male','683105','9633310009','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$BEtHAR1VY7v.XDNfVGLukeKLKlkG8zeQ2TN42wRVayRAhDdMQdylq','1234','Pending','2020-01-06 18:16:50','2020-01-06 18:16:50',NULL),
(26,'test','cs','Male','683105','9633310008','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$d0D3nZMfwdVw6IeIo.fhCOHAmpsmC9a5WRwnZDNPE85uM9mnBbEh.','1234','Pending','2020-01-06 18:17:32','2020-01-06 18:17:32',NULL),
(27,'test','cs','Male','683105','9633310007','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$INFmqkHsupXX5x0gIxjMLuD4N9csiqras/ZqXXZBPQTA8VCplhXbS','1234','Pending','2020-01-06 18:18:49','2020-01-06 18:18:49',NULL),
(28,'test','cs','Male','683105','9633310100','nazeer29@gmail.com','7787','2020','January',NULL,'$2y$10$PzugweBpN.gjzVpAIuAzRuxJvniYRuLxch7qddT7OzdHAj2cTaCSu','1234','Pending','2020-01-07 12:44:53','2020-01-21 14:15:14',NULL),
(29,'C S','Nazeer','Male','683105','9995351401','nazeersrk@gmail.com','678','2019','12',NULL,'$2y$10$ohW2zlWTYmwCpxm68LS2GexktRy9p6sxbKzNTvBdRdtGCo0EET/cS','1234','Pending','2020-01-15 11:50:51','2020-01-15 11:50:51',NULL),
(30,'Eer','Naz','Male','683107','9995351402','father123@gmail.com','876','2020','01',NULL,'$2y$10$WykK.8nXQhh9C/.cJXHJzuf6nAhU4hmzbPF7/WYhGdynedZfkf0Q.','1234','Pending','2020-01-15 11:57:03','2020-01-15 11:57:03',NULL),
(31,'Usee','Test','Male','683102','9995351403','testuser1@gmail.com','789','2020','12',NULL,'$2y$10$/yIltMmE.Ae3dZbkti2V/.k4xh4fSd8Hp0ekZ3f3sMs/2HCha8ZZO','1234','Pending','2020-01-15 12:02:06','2020-01-15 12:02:06',NULL),
(32,'Gh','Said','Male','683105','9995351404','nazeercsinfo23@gmail.com','567','June','2019',NULL,'$2y$10$hRPMYkqj0QoDo7CUx7turul9374fB7qGEDKVrI9.RDodnt3SS5pmG','1234','Pending','2020-01-21 14:18:34','2020-01-21 14:18:34',NULL),
(33,'Male','Nazeer','','683106','9995351405','father@gmail.com','4567','01','Jan',NULL,'$2y$10$N1nK/eplR6n7AiQ.tTU92.okXx0hIXUu4cX0981BVGuvqud9XOYKe','1234','Pending','2020-01-28 12:02:04','2020-01-28 12:02:04',NULL);

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `prefecture_id` bigint(20) unsigned NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_prefecture_id_foreign` (`prefecture_id`),
  CONSTRAINT `cities_prefecture_id_foreign` FOREIGN KEY (`prefecture_id`) REFERENCES `prefectures` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cities` */

insert  into `cities`(`id`,`prefecture_id`,`city`,`latitude`,`longitude`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,'Aomori','9.9312328','76.26730409999999',NULL,NULL,NULL),
(2,1,'Hachinohe','9.9312328','76.26730409999999',NULL,NULL,NULL),
(3,1,'Hirosaki','9.9312328','76.26730409999999',NULL,NULL,NULL),
(4,1,'Towada','9.9312328','76.26730409999999',NULL,NULL,NULL),
(5,1,'Mutsu','9.9312328','76.26730409999999',NULL,NULL,NULL),
(6,1,'Goshogawara','9.9312328','76.26730409999999',NULL,NULL,NULL),
(7,1,'Misawa','9.9312328','76.26730409999999',NULL,NULL,NULL),
(8,1,'Kuroishi','9.9312328','76.26730409999999',NULL,NULL,NULL),
(9,1,'Tsugaru','9.9312328','76.26730409999999',NULL,NULL,NULL),
(10,1,'Hirakawa','9.9312328','76.26730409999999',NULL,'2020-01-13 21:34:57',NULL),
(11,1,'palarivattom','9.9312328','76.26730409999999','2020-01-13 17:00:15','2020-01-14 05:01:07','2020-01-14 05:01:07'),
(12,1,'kochi','9.9312328','76.26730409999999','2020-01-13 22:06:54','2020-01-13 22:07:36','2020-01-13 22:07:36'),
(13,1,'kochi','9.9312328','76.26730409999999','2020-01-13 22:07:51','2020-01-13 22:13:45','2020-01-13 22:13:45'),
(14,1,'kochi','9.9312328','76.26730409999999','2020-01-13 22:10:10','2020-01-13 22:10:19','2020-01-13 22:10:19'),
(15,1,'kochi','9.9312328','76.26730409999999','2020-01-13 22:13:58','2020-01-13 22:14:06','2020-01-13 22:14:06'),
(22,1,'kochi','9.9312328','76.26730409999999','2020-01-13 23:10:16','2020-01-13 23:10:24','2020-01-13 23:10:24');

/*Table structure for table `drug_store_timings` */

DROP TABLE IF EXISTS `drug_store_timings`;

CREATE TABLE `drug_store_timings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `drug_store_id` bigint(20) unsigned NOT NULL,
  `day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `day_japan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drug_store_timings_drug_store_id_foreign` (`drug_store_id`),
  CONSTRAINT `drug_store_timings_drug_store_id_foreign` FOREIGN KEY (`drug_store_id`) REFERENCES `drug_stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `drug_store_timings` */

insert  into `drug_store_timings`(`id`,`drug_store_id`,`day`,`day_japan`,`time_from`,`time_to`,`created_at`,`updated_at`,`deleted_at`) values 
(24,18,'Monday','','12:00:00','12:30:00',NULL,'2020-01-17 11:57:25','2020-01-17 11:57:25'),
(25,18,'Tuesday','','12:00:00','12:30:00',NULL,'2020-01-17 11:57:25','2020-01-17 11:57:25'),
(26,18,'Wednesday','','12:00:00','12:30:00',NULL,'2020-01-17 11:57:25','2020-01-17 11:57:25'),
(27,18,'Monday','','12:00:00','12:15:00',NULL,'2020-01-17 11:58:23','2020-01-17 11:58:23'),
(28,18,'Wednesday','','12:00:00','12:15:00',NULL,'2020-01-17 11:58:23','2020-01-17 11:58:23'),
(29,18,'Monday','','12:00:00','13:00:00',NULL,'2020-01-17 12:00:34','2020-01-17 12:00:34'),
(30,18,'Tuesday','','12:00:00','13:00:00',NULL,'2020-01-17 12:00:34','2020-01-17 12:00:34'),
(31,18,'Monday','','12:00:00','13:00:00',NULL,'2020-01-17 12:01:09','2020-01-17 12:01:09'),
(32,18,'Wednesday','','12:00:00','13:00:00',NULL,'2020-01-17 12:01:09','2020-01-17 12:01:09'),
(33,18,'Saturday','','12:00:00','13:00:00',NULL,'2020-01-17 12:01:09','2020-01-17 12:01:09'),
(34,18,'Monday','','12:00:00','13:00:00',NULL,'2020-01-18 13:21:50','2020-01-18 13:21:50'),
(35,18,'Wednesday','','12:00:00','13:00:00',NULL,'2020-01-18 13:21:50','2020-01-18 13:21:50'),
(36,18,'Sunday','','12:00:00','13:00:00',NULL,'2020-01-18 13:21:50','2020-01-18 13:21:50'),
(37,18,'Monday','','13:30:00','14:30:00',NULL,'2020-01-18 20:23:32','2020-01-18 20:23:32'),
(38,18,'Tuesday','','13:30:00','14:30:00',NULL,'2020-01-18 20:23:32','2020-01-18 20:23:32'),
(39,18,'Monday','','20:30:00','21:30:00',NULL,'2020-01-20 21:58:38','2020-01-20 21:58:38'),
(40,18,'Tuesday','','20:30:00','21:30:00',NULL,'2020-01-20 21:58:38','2020-01-20 21:58:38'),
(41,18,'Wednesday','','20:30:00','21:30:00',NULL,'2020-01-20 21:58:38','2020-01-20 21:58:38'),
(42,18,'Monday','','10:00:00','23:00:00',NULL,'2020-01-20 22:00:26','2020-01-20 22:00:26'),
(43,18,'Tuesday','','10:00:00','23:00:00',NULL,'2020-01-20 22:00:26','2020-01-20 22:00:26'),
(44,18,'Monday','','22:00:00','23:00:00',NULL,'2020-01-20 22:01:08','2020-01-20 22:01:08'),
(45,18,'Tuesday','','22:00:00','23:00:00',NULL,'2020-01-20 22:01:08','2020-01-20 22:01:08'),
(46,18,'Monday','月曜日','22:00:00','23:00:00',NULL,'2020-01-20 22:12:30','2020-01-20 22:12:30'),
(47,18,'Tuesday','火曜日','22:00:00','23:00:00',NULL,'2020-01-20 22:12:30','2020-01-20 22:12:30'),
(48,18,'Monday','月曜日','22:15:00','23:15:00',NULL,'2020-01-20 22:15:25','2020-01-20 22:15:25'),
(49,18,'Tuesday','火曜日','22:15:00','23:15:00',NULL,'2020-01-20 22:15:25','2020-01-20 22:15:25'),
(50,18,'Wednesday','水曜日','22:15:00','23:15:00',NULL,'2020-01-20 22:15:25','2020-01-20 22:15:25'),
(51,18,'Monday','月曜日','22:15:00','23:15:00',NULL,'2020-01-22 20:13:57','2020-01-22 20:13:57'),
(52,18,'Tuesday','火曜日','22:15:00','23:15:00',NULL,'2020-01-22 20:13:57','2020-01-22 20:13:57'),
(53,18,'Wednesday','水曜日','22:15:00','23:15:00',NULL,'2020-01-22 20:13:57','2020-01-22 20:13:57'),
(54,18,'Thursday','木曜日','22:15:00','23:15:00',NULL,'2020-01-22 20:13:57','2020-01-22 20:13:57'),
(55,18,'Monday','月曜日','20:15:00','21:15:00',NULL,'2020-01-22 20:48:32','2020-01-22 20:48:32'),
(56,18,'Tuesday','火曜日','20:15:00','21:15:00',NULL,'2020-01-22 20:48:32','2020-01-22 20:48:32'),
(57,18,'Tuesday','火曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:50:41','2020-01-22 20:50:41'),
(58,18,'Monday','月曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:37','2020-01-22 20:51:37'),
(59,18,'Tuesday','火曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:37','2020-01-22 20:51:37'),
(60,18,'Wednesday','水曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:37','2020-01-22 20:51:37'),
(61,18,'Monday','月曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:52','2020-01-22 20:51:52'),
(62,18,'Tuesday','火曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:52','2020-01-22 20:51:52'),
(63,18,'Wednesday','水曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:52','2020-01-22 20:51:52'),
(64,18,'Sunday','日曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:51:52','2020-01-22 20:51:52'),
(65,18,'Monday','月曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:52:42','2020-01-22 20:52:42'),
(66,18,'Monday','月曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:53:23','2020-01-22 20:53:23'),
(67,18,'Tuesday','火曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:53:23','2020-01-22 20:53:23'),
(68,18,'Sunday','日曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:55:02','2020-01-22 20:55:02'),
(69,18,'Sunday','日曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:56:01','2020-01-22 20:56:01'),
(70,18,'Tuesday','火曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:57:14','2020-01-22 20:57:14'),
(71,18,'Wednesday','水曜日','21:00:00','22:00:00',NULL,'2020-01-22 20:57:14','2020-01-22 20:57:14'),
(72,18,'Monday','月曜日','21:00:00','22:00:00',NULL,'2020-01-22 21:05:38','2020-01-22 21:05:38'),
(73,18,'Monday','月曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:15:02','2020-01-22 21:15:02'),
(74,18,'Tuesday','火曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:15:02','2020-01-22 21:15:02'),
(75,18,'Wednesday','水曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:15:02','2020-01-22 21:15:02'),
(76,18,'Monday','月曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:15:07','2020-01-22 21:15:07'),
(77,18,'Monday','月曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:15:37','2020-01-22 21:15:37'),
(78,18,'Monday','月曜日','21:15:00','22:15:00',NULL,'2020-01-22 21:17:29','2020-01-22 21:17:29'),
(79,18,'Monday','月曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:19:05','2020-01-22 21:19:05'),
(80,18,'Tuesday','火曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:19:05','2020-01-22 21:19:05'),
(81,18,'Monday','月曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:26:57','2020-01-22 21:26:57'),
(82,18,'Monday','月曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:29:07','2020-01-22 21:29:07'),
(83,18,'Tuesday','火曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:29:07','2020-01-22 21:29:07'),
(84,18,'Monday','月曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:30:19','2020-01-22 21:30:19'),
(85,18,'Tuesday','火曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:30:19','2020-01-22 21:30:19'),
(86,18,'Monday','月曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:32:03','2020-01-22 21:32:03'),
(87,18,'Tuesday','火曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:32:03','2020-01-22 21:32:03'),
(88,18,'Sunday','日曜日','21:30:00','22:30:00',NULL,'2020-01-22 21:32:03','2020-01-22 21:32:03'),
(89,18,'Monday','月曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:32:57','2020-01-22 21:32:57'),
(90,18,'Tuesday','火曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:32:57','2020-01-22 21:32:57'),
(91,18,'Wednesday','水曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:32:57','2020-01-22 21:32:57'),
(92,18,'Thursday','木曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:32:57','2020-01-22 21:32:57'),
(93,18,'Monday','月曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:34:01','2020-01-22 21:34:01'),
(94,18,'Tuesday','火曜日','21:45:00','22:45:00',NULL,'2020-01-22 21:34:01','2020-01-22 21:34:01'),
(95,18,'Monday','月曜日','21:45:00','23:45:00',NULL,'2020-01-22 21:34:35','2020-01-22 21:34:35'),
(96,18,'Tuesday','火曜日','21:45:00','23:45:00',NULL,'2020-01-22 21:34:35','2020-01-22 21:34:35'),
(97,18,'Wednesday','水曜日','21:45:00','23:45:00',NULL,'2020-01-22 21:34:35','2020-01-22 21:34:35'),
(98,18,'Wednesday','水曜日','22:00:00','23:45:00',NULL,'2020-01-23 16:37:49','2020-01-23 16:37:49'),
(99,18,'Monday','月曜日','16:45:00','17:45:00',NULL,'2020-01-23 16:38:56','2020-01-23 16:38:56'),
(100,18,'Tuesday','火曜日','16:45:00','17:45:00',NULL,'2020-01-23 16:38:56','2020-01-23 16:38:56'),
(101,18,'Monday','月曜日','16:45:00','17:45:00',NULL,'2020-01-23 16:39:15','2020-01-23 16:39:15'),
(102,18,'Tuesday','火曜日','16:45:00','17:45:00',NULL,'2020-01-23 16:39:15','2020-01-23 16:39:15'),
(103,18,'Monday','月曜日','16:45:00','18:00:00',NULL,NULL,NULL),
(104,18,'Tuesday','火曜日','16:45:00','18:00:00',NULL,NULL,NULL),
(105,18,'Wednesday','水曜日','16:45:00','18:00:00',NULL,NULL,NULL);

/*Table structure for table `drug_stores` */

DROP TABLE IF EXISTS `drug_stores`;

CREATE TABLE `drug_stores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_model` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_charge` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_need_chemist` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clerks` int(11) DEFAULT NULL,
  `chemists` int(11) DEFAULT NULL,
  `respond_speed` float(9,2) DEFAULT NULL,
  `licence_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_image1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_image2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_expire` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_otp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_otp_expire` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Approved','Pending','Suspended','Not Verified') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Not Verified',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `drug_stores` */

insert  into `drug_stores`(`id`,`name`,`history_model`,`email`,`phone`,`in_charge`,`introduction`,`reason_need_chemist`,`store_information`,`other_information`,`clerks`,`chemists`,`respond_speed`,`licence_number`,`main_image`,`sub_image1`,`sub_image2`,`password`,`latitude`,`longitude`,`otp`,`otp_expire`,`phone_otp`,`phone_otp_expire`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Carepoint','Gooco','lithin@gmail.com','917788992211','Lithin','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',NULL,NULL,NULL,'','1832373254.jpg','1719717558.jpg','1569060490.jpg','','9.9973','76.3007',NULL,NULL,NULL,NULL,'Pending','2019-10-09 11:38:35','2020-01-02 21:55:16',NULL),
(2,'Apotheco','Gooco','anis.m@gmail.com','918555497286','Anis','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',NULL,NULL,NULL,NULL,NULL,'','298444797.jpg','833734058.jpg','1051191742.jpg','','9.9658','76.2421',NULL,NULL,NULL,NULL,'Suspended','2019-11-07 11:39:58','2020-01-02 21:57:22',NULL),
(3,'Assured Rx','Gooco','issac@gmail.com','917555842332','issac Peter','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',NULL,NULL,NULL,NULL,NULL,'','1769434075.jpg','1801901528.jpg','712329222.jpg','','10.0008283','76.3013739',NULL,NULL,NULL,NULL,'Approved','2019-12-04 11:42:07','2020-01-02 21:56:04',NULL),
(8,'Banks Apothecary','Gooco','midhun@gmail.com','918555818697','Mithun Manuel','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',NULL,NULL,NULL,NULL,NULL,'','2066048110.jpg','298291614.jpg','457450232.jpg','','10.0008283','76.3013739',NULL,NULL,NULL,NULL,'Approved','2020-01-01 11:42:12','2020-01-02 21:56:31',NULL),
(9,'Bioplus Specialty','Gooco','samual@gmail.com','919555590326','Samual','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',NULL,NULL,NULL,NULL,NULL,'','1736413101.jpg','313664752.jpg','1643052379.jpg','','10.0008283','76.3013739',NULL,NULL,NULL,NULL,'Suspended','2020-01-01 11:42:55','2020-01-02 21:58:38',NULL),
(18,'Drug Store','EM system','akhil.pp@iroidtechnologies.com','8891939756','Akhil P P','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',10,11,3.00,'12345625','1331777423.jpg','637685251.jpg','1677261394.jpg','$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','10.0008283','76.3013739','epo0G','2020-01-23 17:12:45',NULL,NULL,'Pending','2020-01-07 19:01:29','2020-01-23 16:40:20',NULL),
(19,'Drug Store','Gooco','akhil.pp1@iroidtechnologies.com','8891939752','Arun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123456',NULL,NULL,NULL,'$2y$10$J3gtZgaquKtsVD1Disotl.X/8NsJ/BHH/LwDXVsmQNsWQr4LhTX8W','10.0008283','76.3013739',NULL,NULL,NULL,NULL,'Pending','2020-01-10 15:09:19','2020-01-10 15:09:19',NULL),
(20,'Shameem','Gooco','shameem@iroidtechnologies.com','8891939752','Shameem',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123456',NULL,NULL,NULL,'$2y$10$iDy2Ko.Md5e8eg.BrvgO2u5JcMjg5KGQnfd2Uz3fePAFbbqOrL0pG','9.9312328','76.26730409999999',NULL,NULL,NULL,NULL,'Pending','2020-01-16 21:03:19','2020-01-16 21:03:19',NULL),
(21,'shameer','Gooco','shameerali@iroidtechnologies.com','8129779445','ali',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123456',NULL,NULL,NULL,'$2y$10$HAv1USCFoaz/.DvBOnCxg.o/X.LNRjDnChlO4Q3fMFWWQO48Ko24a','10.0013655','76.310081',NULL,NULL,NULL,NULL,'Pending','2020-01-16 21:11:38','2020-01-16 21:14:15',NULL),
(28,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','01234567891','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dfgdf',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','10.1517834','76.392958','epo0G','2020-01-23 17:12:45','','','Not Verified','2020-01-18 19:47:53','2020-01-23 16:12:45',NULL),
(29,'Akhil P P',NULL,'akhil.pp3@iroidtechnologies.com','01234567891','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fdgdf',NULL,NULL,NULL,'$2y$10$dCQLnh5LOf5IvOXVp1I.geR6ZEZEiZ3IUMoELjrYyAOf1Omej9hSa','9.9312328','76.26730409999999',NULL,NULL,'','','Pending','2020-01-18 20:07:57','2020-01-18 20:10:11',NULL),
(32,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'12345',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','7218','2020-01-20 14:04:51','Not Verified','2020-01-18 22:05:17','2020-01-23 16:12:45',NULL),
(33,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','7218','2020-01-20 14:04:51','Not Verified','2020-01-18 22:06:32','2020-01-23 16:12:45',NULL),
(34,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','7218','2020-01-20 14:04:51','Not Verified','2020-01-18 22:13:51','2020-01-23 16:12:45',NULL),
(35,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','7218','2020-01-20 14:04:51','Not Verified','2020-01-18 22:15:26','2020-01-23 16:12:45',NULL),
(36,'Akhil P P',NULL,'akhil.pp31@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234',NULL,NULL,NULL,'$2y$10$x4AHdkO0uMW7OxKB2p/gg.s23yeUXT.XdP//ECuf6lsaqDjdFjSeW','9.9312328','76.26730409999999','4893',NULL,'1671','2020-01-20 14:15:10','Not Verified','2020-01-20 22:17:04','2020-01-20 13:15:10',NULL),
(38,'Anis Muhammed',NULL,'anis.m@iroidtechnologies.com','9020076288','Ashaan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'654',NULL,NULL,NULL,'$2y$10$FF1jCIQ1TvlaJfA8kFVcw.RTzNEnvoYLQW2RVM4dQCXj22DoD/Q7O','-33.74228600000001','151.045084',NULL,NULL,'','','Pending','2020-01-20 13:44:04','2020-01-20 13:44:27',NULL),
(39,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'34534',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','','','Not Verified','2020-01-20 23:50:06','2020-01-23 16:12:45',NULL),
(40,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'7656',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','6359','2020-01-21 00:52:22','Not Verified','2020-01-20 23:52:22','2020-01-23 16:12:45',NULL),
(41,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5645',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','5411','2020-01-21 00:54:39','Not Verified','2020-01-20 23:54:39','2020-01-23 16:12:45',NULL),
(42,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'456456',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','9650','2020-01-21 00:55:56','Not Verified','2020-01-20 23:55:56','2020-01-23 16:12:45',NULL),
(43,'Akhil P P',NULL,'akhil.pp@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'456456',NULL,NULL,NULL,'$2y$10$2Dcg/8kM9biGL5PC8Vgi.eJ34wJokTTm4n0PMtbEB6TPZaNC6DRYG','9.9312328','76.26730409999999','epo0G','2020-01-23 17:12:45','3454','2020-01-21 00:56:46','Not Verified','2020-01-20 23:56:46','2020-01-23 16:12:45',NULL),
(44,'Akhil P P',NULL,'akhil.pp4@iroidtechnologies.com','8891939751','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'456456',NULL,NULL,NULL,'$2y$10$eVVd56SCln7FyqvPPE7GEuD61d.zf/9b6E54Ovea/JstWTVxS6lxO','9.9312328','76.26730409999999',NULL,NULL,'','','Pending','2020-01-20 23:59:10','2020-01-21 05:01:25',NULL),
(45,'Akhil P P',NULL,'akhilpremkishore@gmail.com','012345678152','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123456',NULL,NULL,NULL,'$2y$10$djYN41O6R4W6uYfFcmw/VuRrYX3AW8AJuPiNf/b.sZcMujJJuhQvG','9.9312328','76.26730409999999',NULL,NULL,'6851','2020-01-21 15:59:43','Not Verified','2020-01-21 14:59:43','2020-01-21 14:59:43',NULL),
(46,'Akhil P P',NULL,'amaitha.g@iroidtechnologies.com','0123456789154','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'34534',NULL,NULL,NULL,'$2y$10$8rruZltGTD//uYdwwzmbIeIneAlXtUzqdEErJ/lmoCcCS7ussT.LG','9.9312328','76.26730409999999',NULL,NULL,'7266','2020-01-21 16:15:38','Not Verified','2020-01-21 15:15:38','2020-01-21 15:15:38',NULL),
(47,'Akhil P P',NULL,'amitha.g@iroidtechnologies.com','0123456789154','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'34534',NULL,NULL,NULL,'$2y$10$cEUJ4T4fsYQubAb0V0R7ue6/iXtPvgSaD5C7IS7mw9xm/VKoIoxbe','9.9312328','76.26730409999999',NULL,NULL,'6724','2020-01-21 16:17:22','Not Verified','2020-01-21 15:17:22','2020-01-21 15:17:22',NULL),
(48,'Test',NULL,'amitha.g@iroidtechnologies.com','012345678144','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'$2y$10$vjU/ngJYKfysvy.PDrk31.sS1fhqmUho7eU7fJWfFroi3WPrgSd6K','9.9312328','76.26730409999999',NULL,NULL,'5181','2020-01-22 14:02:57','Not Verified','2020-01-22 13:02:57','2020-01-22 13:02:57',NULL),
(49,'Akhil P P',NULL,'akhilpremkishore@gmail.com','01234567125','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'$2y$10$VP4kKuX9S/WZTYcJW1YzMOYKpDcOwWGI7az4KpxYwPX1MxyUox1y.','9.9312328','76.26730409999999',NULL,NULL,'3600','2020-01-22 15:08:41','Not Verified','2020-01-22 14:08:41','2020-01-22 14:08:41',NULL),
(50,'Akhil P P',NULL,'akhilpremkishore@gmail.com','01234567142','Akhil P P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'$2y$10$gCyFHyJOmPLa6o9IHHUqBusNt5aCFupeBBFWdLFFWRRI6uNF4xLeu','9.9312328','76.26730409999999',NULL,NULL,'6588','2020-01-22 15:09:51','Not Verified','2020-01-22 14:09:51','2020-01-22 14:09:51',NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `faqs` */

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `faqs` */

insert  into `faqs`(`id`,`question`,`answer`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Test','test','2020-01-29 13:05:07','2020-01-29 13:20:43','2020-01-29 13:20:43'),
(2,'Test1','Testing','2020-01-29 13:06:45','2020-01-29 13:06:45',NULL),
(3,'Test3','Tesinggfhgfhgfh','2020-01-29 13:07:13','2020-01-29 13:33:22',NULL);

/*Table structure for table `favourites` */

DROP TABLE IF EXISTS `favourites`;

CREATE TABLE `favourites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `drug_store_id` bigint(20) unsigned NOT NULL,
  `chemist_id` bigint(20) unsigned NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favourites_drug_store_id_foreign` (`drug_store_id`),
  KEY `favourites_chemist_id_foreign` (`chemist_id`),
  CONSTRAINT `favourites_chemist_id_foreign` FOREIGN KEY (`chemist_id`) REFERENCES `chemists` (`id`),
  CONSTRAINT `favourites_drug_store_id_foreign` FOREIGN KEY (`drug_store_id`) REFERENCES `drug_stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `favourites` */

insert  into `favourites`(`id`,`drug_store_id`,`chemist_id`,`status`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,1,'0','2020-01-15 17:26:28','2020-01-16 16:12:24',NULL);

/*Table structure for table `job_posts` */

DROP TABLE IF EXISTS `job_posts`;

CREATE TABLE `job_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `drug_store_id` bigint(20) unsigned NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_from` time NOT NULL,
  `work_to` time NOT NULL,
  `no_of_vaccancies` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precautions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` enum('Busy Time','Absence of Chemist','Others','Lack of Workers') COLLATE utf8mb4_unicode_ci NOT NULL,
  `others` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` enum('Reviewed','Review','Pending') COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drug_store_id` (`drug_store_id`),
  CONSTRAINT `job_posts_ibfk_1` FOREIGN KEY (`drug_store_id`) REFERENCES `drug_stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `job_posts` */

insert  into `job_posts`(`id`,`drug_store_id`,`job_name`,`date`,`job_title`,`work_from`,`work_to`,`no_of_vaccancies`,`content`,`precautions`,`reason`,`others`,`review_status`,`created_at`,`updated_at`,`deleted_at`) values 
(5,18,'Chemist Required','2020-01-15','3 Year Experienced Chemist Required','13:59:00','14:00:00',4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','test','Absence of Chemist',NULL,'Review','2020-01-09 13:01:45','2020-01-09 13:01:45',NULL),
(6,18,'Chemist Required','2020-01-17','3 Year Experienced Chemist Required','13:00:00','14:00:00',4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','test','Absence of Chemist',NULL,'Review','2020-01-09 13:02:46','2020-01-09 13:02:46',NULL),
(7,18,'Chemist Required','2020-01-18','3 Year Experienced Chemist Required','13:00:00','14:00:00',4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','Lorem Ipsum is simply dummy text of the printing and typesetting industry.','Absence of Chemist',NULL,'Pending',NULL,'2020-01-09 20:44:41',NULL),
(8,18,'Chemist Required','2020-01-17','3 Year Experienced Chemist Required','12:00:00','13:00:00',4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry.','test','Absence of Chemist',NULL,'Review','2020-01-09 21:09:15','2020-01-09 21:09:15',NULL),
(9,18,'test','2020-01-18','test','18:45:00','18:30:00',1,'dfgd','dfgdf','Absence of Chemist',NULL,'Pending','2020-01-18 18:43:26','2020-01-18 18:43:26',NULL),
(10,18,'test','2020-01-22','test','18:45:00','18:30:00',1,'dfgd','dfgdf','Lack of Workers',NULL,'Pending','2020-01-18 18:43:26','2020-01-18 18:43:26',NULL),
(11,18,'Chemist Required','2020-01-24','test','18:45:00','19:45:00',2,'fdg','dfhfg','Others','fghfghgfhfg','Pending','2020-01-18 18:45:13','2020-01-18 18:45:13',NULL),
(12,18,'Chemist Required','2020-01-30','test','18:45:00','19:45:00',2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum','Others','fghfghgfhfg','Pending','2020-01-18 18:45:13','2020-01-18 18:50:48',NULL),
(13,18,'Chemist Required','2020-01-21','test','12:00:00','12:45:00',2,'test','test','Others',NULL,'Pending','2020-01-20 11:59:57','2020-01-20 11:59:57',NULL),
(14,18,'test','2020-01-22','test','17:00:00','18:00:00',56,'test','test','Lack of Workers',NULL,'Pending','2020-01-21 16:54:05','2020-01-21 16:54:05',NULL),
(15,18,'test','2020-01-23','test','17:00:00','18:00:00',56,'test','test','Lack of Workers',NULL,'Pending','2020-01-21 16:54:05','2020-01-21 16:54:05',NULL),
(16,18,'test','2020-01-30','test','17:00:00','18:00:00',56,'test','test','Lack of Workers',NULL,'Pending','2020-01-21 16:54:05','2020-01-21 16:54:05',NULL),
(17,18,'test','2020-01-29','test','17:00:00','18:00:00',56,'test','test','Lack of Workers',NULL,'Pending','2020-01-21 16:54:06','2020-01-21 16:54:06',NULL),
(18,18,'Chemist Required','2020-01-23','3 year experienced chemist required','17:00:00','18:00:00',5,'test','test','Lack of Workers',NULL,'Pending','2020-01-21 16:55:25','2020-01-21 16:55:43',NULL),
(19,18,'test','2020-01-24','','13:30:00','14:30:00',5,'dgdf','ghjgh','Lack of Workers',NULL,'Pending','2020-01-22 13:27:57','2020-01-22 13:27:57',NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2019_12_27_070949_create_roles_table',2),
(5,'2014_10_12_000000_create_users_table',3),
(6,'2019_12_27_072558_create_drug_stores_table',4),
(7,'2019_12_27_073153_create_chemists_table',5),
(8,'2016_06_01_000001_create_oauth_auth_codes_table',6),
(9,'2016_06_01_000002_create_oauth_access_tokens_table',6),
(10,'2016_06_01_000003_create_oauth_refresh_tokens_table',6),
(11,'2016_06_01_000004_create_oauth_clients_table',6),
(12,'2016_06_01_000005_create_oauth_personal_access_clients_table',6),
(15,'2020_01_08_120424_add_prefectures_table',7),
(16,'2020_01_08_124401_create_cities_table',8),
(17,'2020_01_09_123642_create_job_posts_table',9),
(18,'2020_01_13_174928_create_drug_store_timings_table',10),
(20,'2020_01_15_161923_create_favourites_table',11),
(23,'2020_01_15_191125_create_chemist_interview_timings_table',12),
(24,'2020_01_15_195106_create_chemist_job_applications_table',13),
(25,'2020_01_27_141824_create_work_place_reviews_table',14),
(26,'2020_01_29_123455_create_faqs_table',15),
(27,'2020_01_29_221555_create_chemist_reviews_table',16);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) values 
('00409737740b9f222918d3ebc7f058d8df0d75c5d050375ed6a032110e2d19e4bb39b1b9c5f74c41',28,1,'Personal Access Token','[]',0,'2020-01-14 11:48:57','2020-01-14 11:48:57','2021-01-14 11:48:57'),
('021a9a153b9b89d9bb4691c2579569bd6473ad95abda6cfcd50a5db28edc48c057464663f8eba61f',31,1,'Personal Access Token','[]',0,'2020-01-15 16:19:48','2020-01-15 16:19:48','2021-01-15 16:19:48'),
('04d956ed6dd1292ad84ed144d1f55e220d55d8ceaff8a9460a0b85a180dd339c09c51f4d8b73dea2',31,1,'Personal Access Token','[]',0,'2020-01-15 16:04:05','2020-01-15 16:04:05','2021-01-15 16:04:05'),
('066762da4d5d793913b1b523a27b19f44352243bfa3cb548d745210c241c1b011eceec8c6206a84e',28,1,'Personal Access Token','[]',0,'2020-01-13 14:56:35','2020-01-13 14:56:35','2021-01-13 14:56:35'),
('068967a2c2de920568ba8c1458556d14b0970f48680473d6e86be0f9cff3f3509bbc80638b4deb0f',33,3,'Personal Access Token','[]',0,'2020-01-31 15:09:11','2020-01-31 15:09:11','2021-01-31 15:09:11'),
('079bd857f44c8c2ad7c5a669703a1660e7cab1be11255b2cb6f497f12ce08c947cc0d6d7d48566df',28,1,'Personal Access Token','[]',0,'2020-01-13 12:31:09','2020-01-13 12:31:09','2021-01-13 12:31:09'),
('096fa5a13eefd1df7fc9dfb1e12d3420b325e79050417339c7bf9d6c32ff3fbef0c6f4caa9fc1cc0',33,3,'Personal Access Token','[]',0,'2020-01-28 12:02:39','2020-01-28 12:02:39','2021-01-28 12:02:39'),
('097f68261806054164cdad0cb7150f02c7508fa4fd71aeb9c9170eaa2f349d0d7bb93cba1fe2f7ad',33,3,'Personal Access Token','[]',0,'2020-01-28 14:23:36','2020-01-28 14:23:36','2021-01-28 14:23:36'),
('09db68eb9dd97ae4e4d161ec188ed15adac04524fec22f505e220a5e25a492fd2c6ae3640f7da750',28,1,'Personal Access Token','[]',0,'2020-01-13 15:25:23','2020-01-13 15:25:23','2021-01-13 15:25:23'),
('09e2a770f29a03b06ecf4f3000adbc5a188a99c4eee92da2e7210f62538e62bfc575272c9113d7ca',28,1,'Personal Access Token','[]',0,'2020-01-14 15:40:12','2020-01-14 15:40:12','2021-01-14 15:40:12'),
('0a2adefbe3a46965ee4af465aae373d61a6df8679fe7df772178b6e54440fb48e3a2ec4133702f06',28,1,'Personal Access Token','[]',0,'2020-01-13 12:27:19','2020-01-13 12:27:19','2021-01-13 12:27:19'),
('0bdaf3250132130c4d083a1ca632d2abd6840566cbcc49980540af958b2605b4eb2bebe881ad1d60',28,1,'Personal Access Token','[]',0,'2020-01-13 16:35:39','2020-01-13 16:35:39','2021-01-13 16:35:39'),
('0cf44d54287eb402ff2641a6b65e8a10235419e153603bb7e631049e00ebfa5d1867bf5a6b925ca7',1,1,'Personal Access Token','[]',0,'2020-01-08 14:45:01','2020-01-08 14:45:01','2021-01-08 14:45:01'),
('0d9785c7ad78121d75cff2f398c649b75356b236398577824729291120b5ae4cf301459698a16d67',0,1,'Personal Access Token','[]',0,'2020-01-06 20:14:17','2020-01-06 20:14:17','2021-01-06 20:14:17'),
('0f08183ab3a93918197a58bd8f6416134a4bc146ab11f1b0977757bbb88c31f7f43fef27b9ca22f3',31,1,'Personal Access Token','[]',0,'2020-01-15 15:38:19','2020-01-15 15:38:19','2021-01-15 15:38:19'),
('1002b4f24a77f74abdf4f48739b5c4d8c2e2c2393c110d6b8cf402faa489ba5b9d61f3543d22511e',28,1,'Personal Access Token','[]',0,'2020-01-13 14:49:43','2020-01-13 14:49:43','2021-01-13 14:49:43'),
('10b9231c96272646075f9be6c0119a575a43bcfbe710d45b2dccdb03f0923886a1f5c826186f1578',33,3,'Personal Access Token','[]',0,'2020-02-03 16:57:59','2020-02-03 16:57:59','2021-02-03 16:57:59'),
('117240022bae73406eaee4231ff1ea7a01c9af8b05048031afff27c90b0e03967ec82c09fbdbb694',33,3,'Personal Access Token','[]',0,'2020-02-04 14:34:46','2020-02-04 14:34:46','2021-02-04 14:34:46'),
('1206d362659881ba8f98cafabae26f6acdc7e418fbd097fc1770f084ad50936b33d842d84ba1cdf7',28,1,'Personal Access Token','[]',0,'2020-01-07 14:35:50','2020-01-07 14:35:50','2021-01-07 14:35:50'),
('13ce13d16898f6915defacc52fea8b51163a59fe6fb715c0a9c3e4c192cd23b7cd255ca470a081d3',28,1,'Personal Access Token','[]',0,'2020-01-13 15:41:14','2020-01-13 15:41:14','2021-01-13 15:41:14'),
('15df6e803e87ee5de801f32e02126dd5c7a7e4c89bef0a11086ba28911273e68c33a9e77bf7430df',33,3,'Personal Access Token','[]',0,'2020-01-31 15:16:12','2020-01-31 15:16:12','2021-01-31 15:16:12'),
('162a0bf7eb03eac875c746d4a5695930337c481d6ba25a19a6be21d21a756a5fb04c5a6b290f9f88',33,3,'Personal Access Token','[]',0,'2020-01-28 12:04:04','2020-01-28 12:04:04','2021-01-28 12:04:04'),
('16cfe8d2adce90d40c62b3c686b9c05e7cd8dfbdac3a2e071ac231d63a9fe73eeee05c246205e4ef',28,1,'Personal Access Token','[]',0,'2020-01-14 14:50:08','2020-01-14 14:50:08','2021-01-14 14:50:08'),
('17a4e78cadf6d5c6f77253fe0277605ac10ff56bf5e1dd12f0d9fdd5d5ab6a78c62a29452d014ce7',28,1,'Personal Access Token','[]',0,'2020-01-14 14:14:57','2020-01-14 14:14:57','2021-01-14 14:14:57'),
('18dea80c6fcb1c846041c1a55420ae3a0a5f058fe052f216d6e8c775122a68f17efcd57e891d72fb',28,1,'Personal Access Token','[]',0,'2020-01-13 12:22:37','2020-01-13 12:22:37','2021-01-13 12:22:37'),
('19386246e4e06f1a3aad686237a1728896ee93fe8958287b6b6dfecda8904ab5470b6be2785c97da',28,1,'Personal Access Token','[]',0,'2020-01-14 15:33:55','2020-01-14 15:33:55','2021-01-14 15:33:55'),
('19df3e07293eac1f9429918c93b81d010a5b7fd71196916f45545de64a01a8e689cf6cc3e946a09b',28,1,'Personal Access Token','[]',0,'2020-01-13 14:15:41','2020-01-13 14:15:41','2021-01-13 14:15:41'),
('1aaac0a0fa8e9363bbcce58c3e4f646bb5a5dd0e7a2f9d3b507cefae821cb325a30063a47ffeab7c',28,1,'Personal Access Token','[]',0,'2020-01-13 17:36:02','2020-01-13 17:36:02','2021-01-13 17:36:02'),
('1b5f74011971089366d41b90281059bb1ac46cf820239d3b98fb4486c0112fd3be44272f2682b762',28,1,'Personal Access Token','[]',0,'2020-01-13 16:03:49','2020-01-13 16:03:49','2021-01-13 16:03:49'),
('1b9ed566c8ebc649c338a9ff03f9aa1fe769093a774acb1da0579066d983c466d07b2a825458b7eb',33,3,'Personal Access Token','[]',0,'2020-02-04 14:23:41','2020-02-04 14:23:41','2021-02-04 14:23:41'),
('1bd777477abf692ebf5a9f59f4144b4e370665350ce040ebf29af032a0b4b9d2fcb070482f076849',31,1,'Personal Access Token','[]',0,'2020-01-15 16:38:46','2020-01-15 16:38:46','2021-01-15 16:38:46'),
('1c2a3a26e1422ca958355ae9bf49b63ba54bec65ef7982b263b67de9dcae782ced9dae1b36cc7d74',33,3,'Personal Access Token','[]',0,'2020-01-28 12:49:49','2020-01-28 12:49:49','2021-01-28 12:49:49'),
('1cdeb9f706501f5ae8cd7565b7db91f80f3682fce7d4be26a0fafbd4872b21f2acd350bf93a96b15',33,3,'Personal Access Token','[]',0,'2020-02-04 14:15:56','2020-02-04 14:15:56','2021-02-04 14:15:56'),
('1d5b62862e73b8c4c956c12b476dca3f14d2269be456f87c2d6e149df6b2dfe55533f01f479cc52e',28,1,'Personal Access Token','[]',0,'2020-01-13 16:00:44','2020-01-13 16:00:44','2021-01-13 16:00:44'),
('1ee76f2d08a415096342e608be6cf3f5e120042657b599b164834e42afa2c2e7cf19f2154b537031',31,1,'Personal Access Token','[]',0,'2020-01-15 14:50:25','2020-01-15 14:50:25','2021-01-15 14:50:25'),
('1ff46fb9d028831988ebd77f7a268b802e1983a9c363b5043ff9c73119c878fd92af5494002e58c4',31,1,'Personal Access Token','[]',0,'2020-01-15 14:41:58','2020-01-15 14:41:58','2021-01-15 14:41:58'),
('20a71272ff2aaf03fc1e22d9ce73a605e93b6de97affb2cc8f838116a6d1a83bb8bb674ef38b82fc',31,1,'Personal Access Token','[]',0,'2020-01-15 17:00:17','2020-01-15 17:00:17','2021-01-15 17:00:17'),
('20d5d93270642b0cf321725af7594a402eb80b7ffbeb924ed30823c0a84a81330a734a74aee2591e',33,3,'Personal Access Token','[]',0,'2020-01-30 15:43:13','2020-01-30 15:43:13','2021-01-30 15:43:13'),
('21d94415f7520873d24a09e7ca81a91fcc21f0b51653d2f828eb90760ec4490df65f6aa697d157f5',33,3,'Personal Access Token','[]',0,'2020-01-30 12:49:34','2020-01-30 12:49:34','2021-01-30 12:49:34'),
('2259e0e36d909f3d1825afe37fc02e59be0e08f9a1763a44f48808d8be38d6cec2d6b20b9b55fdda',28,1,'Personal Access Token','[]',0,'2020-01-13 15:16:03','2020-01-13 15:16:03','2021-01-13 15:16:03'),
('23308bfa52ea92c18a5b9c1394a471d648037fe575dfafc478151cd46d37022383611f2383a10832',31,1,'Personal Access Token','[]',0,'2020-01-15 16:10:15','2020-01-15 16:10:15','2021-01-15 16:10:15'),
('242aa2b945166711c8258ce9e9ec78f80786e0d402244619d3d2b8986064eb31f42c3128ff3b01e7',33,3,'Personal Access Token','[]',0,'2020-02-04 14:06:41','2020-02-04 14:06:41','2021-02-04 14:06:41'),
('246cd1a961457e3ebaa7c01e509c1fc395c121a286c3c08ebf003aefa306feac7c8942d19c9ce410',33,3,'Personal Access Token','[]',0,'2020-02-04 14:24:59','2020-02-04 14:24:59','2021-02-04 14:24:59'),
('257746eb52e052a8b33f9f84a4024d8395fdedae80ab5ae54dc3276d34ddb0c38a931d17554a871d',33,3,'Personal Access Token','[]',0,'2020-02-04 15:03:36','2020-02-04 15:03:36','2021-02-04 15:03:36'),
('25e31714415192b2db6c56937387cdadb5d8a7cd7c3c2b60704d722b313018b1f8dc7e2f09cfac12',11,1,'Personal Access Token','[]',0,'2020-01-03 21:52:46','2020-01-03 21:52:46','2021-01-03 21:52:46'),
('2608d332331ccf58e95c949d9e4dcd1938518dfb6453a2bbea10596d7c547bb2543b6ba1c940271d',28,1,'Personal Access Token','[]',0,'2020-01-07 12:45:19','2020-01-07 12:45:19','2021-01-07 12:45:19'),
('2618b2e8aa9ebd787d232d7b783a430fca371915e7fa2f0d26ce0f5bd7a35965b6ab3f4aae80eb69',33,3,'Personal Access Token','[]',0,'2020-02-03 10:52:10','2020-02-03 10:52:10','2021-02-03 10:52:10'),
('26e6dc890afe0f642532585d175b7aeb19d4cf687c076bde028f9bd5d2d518939ff3a46ebc28a81f',31,1,'Personal Access Token','[]',0,'2020-01-15 16:08:29','2020-01-15 16:08:29','2021-01-15 16:08:29'),
('2798db0f46fe377fab4b0b03a953b054d5bb2e6088e3ee006d46e08901f9a34774ff61e9fbc4c22c',28,1,'Personal Access Token','[]',0,'2020-01-13 12:31:16','2020-01-13 12:31:16','2021-01-13 12:31:16'),
('27c23ed8b6c8fea60907b5226e3719fbd7bc53a4f915c60e3d241fe8dcaef1d9dc3c6fe167066c5d',28,1,'Personal Access Token','[]',0,'2020-01-14 11:30:20','2020-01-14 11:30:20','2021-01-14 11:30:20'),
('28891010b39dac17ff124a47985fb0abbed879a537a167766aab2b901b10a9a0d3acd37cec4ab5c6',33,3,'Personal Access Token','[]',0,'2020-01-28 12:17:03','2020-01-28 12:17:03','2021-01-28 12:17:03'),
('28c289ee250a0a65ecd95c749e3ad9b0e5a1e291c02981d2eefa7fcb7fdb7680199f1cf3cb8f9944',28,1,'Personal Access Token','[]',0,'2020-01-13 13:55:11','2020-01-13 13:55:11','2021-01-13 13:55:11'),
('294731b1b69074998b0d5e56dec329f3882e5c3ead3078e8fc1971146f878589366f4ca103569d76',33,3,'Personal Access Token','[]',0,'2020-02-04 11:24:53','2020-02-04 11:24:53','2021-02-04 11:24:53'),
('2e666d3112b80fc28fbd0d5bb6277b6f33dfaf9dbb143b1ebf92cf37f8d8bed7e43ce0f848755309',28,1,'Personal Access Token','[]',0,'2020-01-13 16:23:19','2020-01-13 16:23:19','2021-01-13 16:23:19'),
('2ef232b0e1993ed89bfbfea27beefa827eee2b559fad745b50cf617e773449a68436e3c9cdc3382e',28,1,'Personal Access Token','[]',0,'2020-01-14 14:32:34','2020-01-14 14:32:34','2021-01-14 14:32:34'),
('2f52e1ef709c1496a935e69a4bdd46118ae865c32dd115652266ca592762b9e2dc0416a349c71763',33,3,'Personal Access Token','[]',0,'2020-01-28 12:08:41','2020-01-28 12:08:41','2021-01-28 12:08:41'),
('2fef41a96b315bd3073fdd5c710ed8ffd745749794f2727bd6cfd6fedb68f8cf0a37247cf6f9d89f',1,3,'Personal Access Token','[]',0,'2020-01-27 12:47:40','2020-01-27 12:47:40','2021-01-27 12:47:40'),
('2ff12942f933dcbbf5086bbeddcd07e7839145f1dd929cb1df836dfd9eb5c0671eda228363a4671e',33,3,'Personal Access Token','[]',0,'2020-01-28 12:13:49','2020-01-28 12:13:49','2021-01-28 12:13:49'),
('30246830e3bd63769c2249f0fedc5f65e966c9ea916cf05354d35e3729ec707636c334cf6150bba8',28,1,'Personal Access Token','[]',0,'2020-01-13 13:42:11','2020-01-13 13:42:11','2021-01-13 13:42:11'),
('303dd84e0c4a299f29c5dd26c51d46cf93ce1e0a2d52363e4015080b3b62748dd50c1b437101f88f',31,1,'Personal Access Token','[]',0,'2020-01-15 16:49:13','2020-01-15 16:49:13','2021-01-15 16:49:13'),
('31ca34820512f50582c0464f489fc8169200e2ff060c1bc511b4dc39b77d249337f61635613a3723',33,3,'Personal Access Token','[]',0,'2020-01-28 14:22:51','2020-01-28 14:22:51','2021-01-28 14:22:51'),
('31cef13ba13120670b201f43fb525b62506d21f594a2ec8367578e1a9580ab75a1c4fffb79eada11',1,1,'Personal Access Token','[]',0,'2020-01-08 12:20:23','2020-01-08 12:20:23','2021-01-08 12:20:23'),
('324fa7f0ffc52181bc8910064569900fcc63a836a9583b4b64351290f4fc6585287147c3b09b0caa',33,3,'Personal Access Token','[]',0,'2020-02-03 17:15:55','2020-02-03 17:15:55','2021-02-03 17:15:55'),
('33bd574c540ef323266d565c618302e6c77533fbb07b90de5a8c1dbc80129c18e9e7d379f75cba16',33,3,'Personal Access Token','[]',0,'2020-02-06 10:16:08','2020-02-06 10:16:08','2021-02-06 10:16:08'),
('33cbf1d70a6beeb2609608bf5e87c35505507cd518d5f2a39e883fb37328e86c6072fd7aef6bcb28',28,1,'Personal Access Token','[]',0,'2020-01-13 12:10:00','2020-01-13 12:10:00','2021-01-13 12:10:00'),
('341144b46688d2baac09adb1c2790f91c6bb9e11fc2072bf46eafebf3ebf110b758c10e3ffa239b5',33,3,'Personal Access Token','[]',0,'2020-01-28 14:20:37','2020-01-28 14:20:37','2021-01-28 14:20:37'),
('342184d7660bf9dd91fa0af338f1142689472e11fa7f9a050e641a4e63427b43d15581e9fa82cfa8',28,1,'Personal Access Token','[]',0,'2020-01-14 13:59:00','2020-01-14 13:59:00','2021-01-14 13:59:00'),
('343a6eb9519e165b5464fe1942c0c5e43d75036e0484b323b47625873aef38402d58d611c481cea5',28,1,'Personal Access Token','[]',0,'2020-01-13 14:06:23','2020-01-13 14:06:23','2021-01-13 14:06:23'),
('3457e388aba537a115bd07bec77a2f36746d7da1eca92400808a50332cda63759be159fbc59a2659',33,3,'Personal Access Token','[]',0,'2020-02-04 11:33:26','2020-02-04 11:33:26','2021-02-04 11:33:26'),
('34e36156dcdff97ec79f275222044c37fe96b225e8ad14eff80cf03f16a8a6d03454346792fd75be',31,1,'Personal Access Token','[]',0,'2020-01-15 16:53:51','2020-01-15 16:53:51','2021-01-15 16:53:51'),
('365bd54cf6b664122c7e816d1899d9108f3348ac89c14f50267e5964efb0cf3502f76989c6c2d634',33,3,'Personal Access Token','[]',0,'2020-01-28 14:45:45','2020-01-28 14:45:45','2021-01-28 14:45:45'),
('36e3c01fddd65ec49cabe6d8e82656949ce81320684fb026db2f965490dac13bdb19c05a60117a1a',28,1,'Personal Access Token','[]',0,'2020-01-13 14:13:22','2020-01-13 14:13:22','2021-01-13 14:13:22'),
('36feb953a40578fa690d1d936fd9fe3ede7fee6addf4986aa764ded674ae9bef53b0d60932568457',28,1,'Personal Access Token','[]',0,'2020-01-13 16:09:12','2020-01-13 16:09:12','2021-01-13 16:09:12'),
('37ce4487bdeba9433f11fe88980474d79d754e027fdd71c4f23139429af4069b9b301c6d9a41af44',28,1,'Personal Access Token','[]',0,'2020-01-13 15:34:17','2020-01-13 15:34:17','2021-01-13 15:34:17'),
('39d483440db8f9a1bd10dba32008eb65b49148219f86c1aec9b08fb75b793fb802f7bd0055a2ccf3',28,1,'Personal Access Token','[]',0,'2020-01-13 12:21:59','2020-01-13 12:21:59','2021-01-13 12:21:59'),
('3a114e4f28457c82ddeda7795c9a7cfd87fbf4562bb69a20f4de2f11a2b8655c0bb59d4169e32909',33,3,'Personal Access Token','[]',0,'2020-02-03 10:47:48','2020-02-03 10:47:48','2021-02-03 10:47:48'),
('3a2c93a608c3dad856f6925d8d9cf5be153e8a8d949d96f678557e0790ff591df0041b006444f33c',31,1,'Personal Access Token','[]',0,'2020-01-15 16:29:20','2020-01-15 16:29:20','2021-01-15 16:29:20'),
('3a49dd63f583425f2d31aa126473d1928271dac92c4f2ff8f84c28310d61574b947c503f9d8334dc',31,1,'Personal Access Token','[]',0,'2020-01-15 16:07:38','2020-01-15 16:07:38','2021-01-15 16:07:38'),
('3a9e57e3cbe3b81d2da87ec69d3be761fca6cb95b9134a2a66cf11cb0e7cdb2e8733801450e4f89b',28,1,'Personal Access Token','[]',0,'2020-01-13 13:50:03','2020-01-13 13:50:03','2021-01-13 13:50:03'),
('3b5a97de01c90dcc7989246a5969c5a6f5a33a78305ab3a5b3b6cfce75c183e4ef62b9dcdc5bfb3b',28,1,'Personal Access Token','[]',0,'2020-01-13 13:57:57','2020-01-13 13:57:57','2021-01-13 13:57:57'),
('3ba3041cf22572d716b8b823c868bdc24321e8bf3e68db3d79af70545fb3628b367d5f6eac9180af',31,1,'Personal Access Token','[]',0,'2020-01-15 17:35:13','2020-01-15 17:35:13','2021-01-15 17:35:13'),
('3cbd42ee1d22d51c05ca55c3f89ea3fde0816eae9904b42624384ad54269ff6fd9766c38093ed4c1',28,1,'Personal Access Token','[]',0,'2020-01-14 11:36:49','2020-01-14 11:36:49','2021-01-14 11:36:49'),
('3d8465539148e674b87f14217621976682745c049d57da35763877172eb801ce63cdfea4b7783205',28,1,'Personal Access Token','[]',0,'2020-01-13 15:26:58','2020-01-13 15:26:58','2021-01-13 15:26:58'),
('3db05bbd3a9dcec6f81164553229af5b964b3cdde05e4b5e789dc98b5ad840e3c9fae3358528a60e',31,1,'Personal Access Token','[]',0,'2020-01-15 16:23:07','2020-01-15 16:23:07','2021-01-15 16:23:07'),
('3e8847eebc772cd4136753867b53edb41ea259aeb32273e1d7797c77c45b4cb920b7c8a47e7e4fda',28,1,'Personal Access Token','[]',0,'2020-01-14 14:25:13','2020-01-14 14:25:13','2021-01-14 14:25:13'),
('3feaa7ebec876f10165bc9f33cc481aed2f449dd92508c0aac3a85717d3eafd2ec2f759c7cee50c8',31,1,'Personal Access Token','[]',0,'2020-01-15 15:59:43','2020-01-15 15:59:43','2021-01-15 15:59:43'),
('3ff9dc08138ec2aa71435c529fee088c1ab353b90eea43c58cc9a3d281c8a389c5e83704f778135f',31,1,'Personal Access Token','[]',0,'2020-01-15 14:58:28','2020-01-15 14:58:28','2021-01-15 14:58:28'),
('4120f5bdc0bd094fce8ebbeb982f2cdc9248175eef467d1d2a23de56a52ed20136983dbad74b6810',28,1,'Personal Access Token','[]',0,'2020-01-14 11:31:34','2020-01-14 11:31:34','2021-01-14 11:31:34'),
('4169a02dec4299b371067a57fb67d576a351f4bc290a7befcf8c46c663415d0e8cbfebe524e8e141',33,3,'Personal Access Token','[]',0,'2020-01-28 14:19:23','2020-01-28 14:19:23','2021-01-28 14:19:23'),
('4193b0fd573594f241a7677d85051225af0641d2d6b304d7012e3f5871a154a136731aeb0649a8df',33,3,'Personal Access Token','[]',0,'2020-02-04 14:12:58','2020-02-04 14:12:58','2021-02-04 14:12:58'),
('42405061a64a975cdebcaaa77ea4b01954a54f2bff98b1fdf51c0ee2f19f1965e6dec661ee71e41a',31,1,'Personal Access Token','[]',0,'2020-01-15 14:56:46','2020-01-15 14:56:46','2021-01-15 14:56:46'),
('43bb7e82518ac9d52414c9f24dfd9e30f2ce84303118bd7acfa5c57ba98186398ccfff61af2ef349',28,1,'Personal Access Token','[]',0,'2020-01-07 12:51:51','2020-01-07 12:51:51','2021-01-07 12:51:51'),
('45c08ebe0cd1dcf2bfbc6f8b6b2f1ff599130248a102093ea35c83b5ac5a33b4147ece98e068e2ef',28,1,'Personal Access Token','[]',0,'2020-01-13 17:02:09','2020-01-13 17:02:09','2021-01-13 17:02:09'),
('464d2ab9b00c7c641e6622b1eda56fd3f6a077bc016c2f0e27718bc39200c2244f70e0250e220d1a',28,1,'Personal Access Token','[]',0,'2020-01-14 14:18:45','2020-01-14 14:18:45','2021-01-14 14:18:45'),
('46d2b163dc15e3c168f62ddbd68e931286c2355c0efbd7a78d638699f3331fa46f6a2ca6d3e3ea0d',28,1,'Personal Access Token','[]',0,'2020-01-13 16:21:54','2020-01-13 16:21:54','2021-01-13 16:21:54'),
('46dacc3f582914ac142ba15fe4c161aecc8b855c41662322d8c3ea863c2972872f9b637fb66cdab6',31,1,'Personal Access Token','[]',0,'2020-01-15 16:05:05','2020-01-15 16:05:05','2021-01-15 16:05:05'),
('47aef4424de1b2ee84763eeb7c7780daac95eca2508fd2f54fae7a537447dc22c4ba9cd4c3e208f5',28,1,'Personal Access Token','[]',0,'2020-01-13 15:52:21','2020-01-13 15:52:21','2021-01-13 15:52:21'),
('488d2aceb1004cde6d64865b74085a9be3343c37417752c2ebb3509d04b79fc672cb582c6193b141',28,1,'Personal Access Token','[]',0,'2020-01-14 15:53:39','2020-01-14 15:53:39','2021-01-14 15:53:39'),
('4928023f5467061cc02adf67fc5819f78c0364b5306c68f7c2a0ce9501581a17681cc1f8e2b9b6e3',33,3,'Personal Access Token','[]',0,'2020-01-31 17:24:00','2020-01-31 17:24:00','2021-01-31 17:24:00'),
('49b81c073925b26265fd3e26f53e5227a4748a211a7a01886533d337b36416438582eead6368a127',33,3,'Personal Access Token','[]',0,'2020-02-03 10:49:17','2020-02-03 10:49:17','2021-02-03 10:49:17'),
('49dd56bf00c70ec9fc51674272810458e71cad917019c4f6a4e1895dc7bb408fb0694a56be686337',31,1,'Personal Access Token','[]',0,'2020-01-15 16:40:02','2020-01-15 16:40:02','2021-01-15 16:40:02'),
('49f99d407d895b69edbcb797ad18d01049a7c0ab7078375c2337af0412cff6766dab552e154dc2fe',28,1,'Personal Access Token','[]',0,'2020-01-13 15:26:54','2020-01-13 15:26:54','2021-01-13 15:26:54'),
('4c7fe0cc45a96a9e1762efb89fde95a892b9614057db2c6bb47db2bf8fcb2268555a8fc0fb8d3af4',33,3,'Personal Access Token','[]',0,'2020-01-28 14:54:36','2020-01-28 14:54:36','2021-01-28 14:54:36'),
('4d811dd53f337972f98d0d3375f53e8a3eecab4e065bd999ce5ad2718f55c1b7ba95e29653dd0648',33,3,'Personal Access Token','[]',0,'2020-02-04 11:20:32','2020-02-04 11:20:32','2021-02-04 11:20:32'),
('4deb13912ddee49e70efd1ec7365c62d5b4ef0959e862abd3da729e4e5db3efa1b17fcb769b44da8',28,1,'Personal Access Token','[]',0,'2020-01-13 15:14:31','2020-01-13 15:14:31','2021-01-13 15:14:31'),
('4f11d6e5fea5b2853ae59119e8e01034f4565d08e9953b171bceb9634a16108de8a933d12a535c7c',28,1,'Personal Access Token','[]',0,'2020-01-13 12:34:12','2020-01-13 12:34:12','2021-01-13 12:34:12'),
('4f306e51f6eec796168ec997141e0f765f64ab248a3b3e84ecaed7f4640df73e79da053487aa517f',28,1,'Personal Access Token','[]',0,'2020-01-13 14:20:44','2020-01-13 14:20:44','2021-01-13 14:20:44'),
('4f485fe57879be97e061803d596be69df5147300e11fa88f7cc51859e8573e5e1a90502390bd182e',31,1,'Personal Access Token','[]',0,'2020-01-15 14:49:17','2020-01-15 14:49:17','2021-01-15 14:49:17'),
('5210d2544394302c20ccd91c4f109d7c7e91a64907b4b2308e183600c45caecfa0e13907f51cc2da',33,3,'Personal Access Token','[]',0,'2020-01-28 12:06:39','2020-01-28 12:06:39','2021-01-28 12:06:39'),
('52b73a7ba32008dee372c27e83501252fb1033e5f4a465b314dad5784298c66cfd9ed4fd2dbe6028',33,3,'Personal Access Token','[]',0,'2020-02-04 12:28:03','2020-02-04 12:28:03','2021-02-04 12:28:03'),
('533d5dccdfc0049b078a00090112a388fae1b946d51833ef4cc9465de04639d22c45dd686ee63ebb',33,3,'Personal Access Token','[]',0,'2020-01-28 14:50:48','2020-01-28 14:50:48','2021-01-28 14:50:48'),
('53627e844359fd4e82635ddf862089993d1f27486921a100fedcd986e6204235d117fdb813e29fd9',31,1,'Personal Access Token','[]',0,'2020-01-15 16:32:37','2020-01-15 16:32:37','2021-01-15 16:32:37'),
('53b971a40d6cfb855e8455a73f1aaf373e96f7f33f3abc055b98d6cf9373769181257e6c00bbd9af',31,1,'Personal Access Token','[]',0,'2020-01-15 16:37:06','2020-01-15 16:37:06','2021-01-15 16:37:06'),
('54799dec278dfad85f90b6a22c3c9e7fa5ca68a7ccb9425a26b5ff3d97e1e22b71ea6d4c21ae9866',28,1,'Personal Access Token','[]',0,'2020-01-13 14:14:48','2020-01-13 14:14:48','2021-01-13 14:14:48'),
('555308610e9c7192aa0a6c5148490c0c2b0fd7966ae52ce09fe200280dcb71cdd1b2ed94cbae2757',33,3,'Personal Access Token','[]',0,'2020-01-28 12:18:31','2020-01-28 12:18:31','2021-01-28 12:18:31'),
('55af0678a694e4b8df4cc39d915a0b6617cdd542e1708571881ed25261dd8936915de6b99538fb8a',31,1,'Personal Access Token','[]',0,'2020-01-15 16:50:09','2020-01-15 16:50:09','2021-01-15 16:50:09'),
('55c940778d32c932d1abf86197227d988e1448ffaec5eca8f4296e06ff734ecf4aa2a4e5c2d47ed7',28,1,'Personal Access Token','[]',0,'2020-01-14 15:48:25','2020-01-14 15:48:25','2021-01-14 15:48:25'),
('55f2edab78fdd5c20b9177a4d607f94f10ac95c41b54dc01db836eb1a4feeec8f482ef8ac79d63ef',33,3,'Personal Access Token','[]',0,'2020-02-04 15:05:21','2020-02-04 15:05:21','2021-02-04 15:05:21'),
('568b27538e8313224a2290368400f6c950d51bbe46e5b674a4a32238742b1b84b4e23bc3f15717fc',28,1,'Personal Access Token','[]',0,'2020-01-14 11:29:35','2020-01-14 11:29:35','2021-01-14 11:29:35'),
('5743d6b78ae391c7ef725407421901723f6e32f7986d8236ba787b06c81bce292da0d0d057802f68',31,1,'Personal Access Token','[]',0,'2020-01-15 12:03:02','2020-01-15 12:03:02','2021-01-15 12:03:02'),
('58ac9894d8f8988c4bdfb331c3bbfba1f1401eef398a8c11d090453d1f8cf2bdee3fbcd6c799f9cc',33,3,'Personal Access Token','[]',0,'2020-02-04 10:55:11','2020-02-04 10:55:11','2021-02-04 10:55:11'),
('5b8c8f76ea204b03736b326cc9b112d110c250f5ef1e7a671a89cc970836c4a0a2416ef902612f23',28,1,'Personal Access Token','[]',0,'2020-01-13 14:01:03','2020-01-13 14:01:03','2021-01-13 14:01:03'),
('5bf124b609da6c16a621653cd2f3f18af1911320cb5b39f031f89ab2315f58c14497509c61f4bd9c',33,3,'Personal Access Token','[]',0,'2020-02-04 15:47:51','2020-02-04 15:47:51','2021-02-04 15:47:51'),
('5d67954bb92898cbf7981deca2ca8bd2ee62202e2fe9307da0d921ad81f057bebbdb57d78c4f81c7',31,1,'Personal Access Token','[]',0,'2020-01-15 14:57:37','2020-01-15 14:57:37','2021-01-15 14:57:37'),
('5e42d64cdaa595d0efdd490a6a795c94f58f88f5d96d6c03cd65956978e016fa2095925b80d819f2',28,1,'Personal Access Token','[]',0,'2020-01-13 14:50:34','2020-01-13 14:50:34','2021-01-13 14:50:34'),
('5eb4b3fc14871ef8c4cc73fc01ac233d87278f0acbe31b80551a4a7016fdc01c13bb7397e27ff7a9',28,1,'Personal Access Token','[]',0,'2020-01-13 15:11:35','2020-01-13 15:11:35','2021-01-13 15:11:35'),
('5fa8a3f5a7f6491fccca03f89ab0bac0757f09c4ca9005c3a2886ebbb464bdb0fe8f1ac270a59143',28,1,'Personal Access Token','[]',0,'2020-01-14 14:11:33','2020-01-14 14:11:33','2021-01-14 14:11:33'),
('5fbeb1813895df98571aa05263b6cb042ed01749c3f545934bf5fb5bfc1402cb5381cfba473a80fe',33,3,'Personal Access Token','[]',0,'2020-01-31 16:42:54','2020-01-31 16:42:54','2021-01-31 16:42:54'),
('5ff5fb866846d6c241c0b37743845e8b1afa01a84ec527205891628cf5755115488b6d55607e8ee6',28,1,'Personal Access Token','[]',0,'2020-01-13 16:21:25','2020-01-13 16:21:25','2021-01-13 16:21:25'),
('609c9b391ada372825084e3b6ed63d34cbec28548acb3948f1fedc66dd2a8abc2dc978c66b40f174',28,1,'Personal Access Token','[]',0,'2020-01-07 14:31:22','2020-01-07 14:31:22','2021-01-07 14:31:22'),
('60f69dad2162eefa179dd552df8586d8d000c842407d30ef153fb7f6b74488d5b8fa9a71fa55c30b',28,1,'Personal Access Token','[]',0,'2020-01-13 15:56:19','2020-01-13 15:56:19','2021-01-13 15:56:19'),
('610ebed7e0049c6e9aa27dd60323e26db70e406a9ee3d69577557242159ad63461d5bad05e8022bd',33,3,'Personal Access Token','[]',0,'2020-01-30 12:49:58','2020-01-30 12:49:58','2021-01-30 12:49:58'),
('6128c2dd67186764eb749b9952a6d81e98fc4c44fa45f302ace2912357af59d7e0478ac979d9af69',28,1,'Personal Access Token','[]',0,'2020-01-14 14:23:35','2020-01-14 14:23:35','2021-01-14 14:23:35'),
('61750d89ed193a718c83ed0be1fe06e572f4d55424386149782849f7a965014b7e32d198b54acd15',28,1,'Personal Access Token','[]',0,'2020-01-13 16:15:58','2020-01-13 16:15:58','2021-01-13 16:15:58'),
('6179151eb93dd9612c2133bdb8ba39d8d1c90f6a5a7259bb9df087cda5311f9bcd53c6b342c90fe0',28,1,'Personal Access Token','[]',0,'2020-01-14 14:32:57','2020-01-14 14:32:57','2021-01-14 14:32:57'),
('61bf842fb46716d21ee7961289082b3ca20ad1b49067da402366c6729d3b331db6c431187255016d',33,3,'Personal Access Token','[]',0,'2020-02-03 16:14:16','2020-02-03 16:14:16','2021-02-03 16:14:16'),
('63bb5317106206600bd1a2a0e789d363ff100140f0c857b2e09b1bb1f57ee95c6c9325db57edb975',31,1,'Personal Access Token','[]',0,'2020-01-15 17:25:45','2020-01-15 17:25:45','2021-01-15 17:25:45'),
('64ac2bbde5ae234d42d2d62051b80c2faf3b47470ed71e488c2ddfdf8b3a737eac09ed64bd3bba67',33,3,'Personal Access Token','[]',0,'2020-02-04 15:59:13','2020-02-04 15:59:13','2021-02-04 15:59:13'),
('64d88c46ec3582387547c1c190b6cc0e428b7182c7ef24c67f795875d24bf5fc90d0bd583cf4e61a',28,1,'Personal Access Token','[]',0,'2020-01-14 15:00:23','2020-01-14 15:00:23','2021-01-14 15:00:23'),
('64f74f7b459066608084706c7835452f48ec31cea9e8226d5ec7802cc89c10f050c980ec1043c5ef',28,1,'Personal Access Token','[]',0,'2020-01-15 11:58:19','2020-01-15 11:58:19','2021-01-15 11:58:19'),
('64fd2d86b3cbec873391d6b4368a016cc9c5d4b56e5d61faf771ff6fec9b71b98ca73402ddca5548',28,1,'Personal Access Token','[]',0,'2020-01-14 14:57:46','2020-01-14 14:57:46','2021-01-14 14:57:46'),
('68844f295992ab25d332abbb3a67181a33a62d0641c3ac96aebeb2d77c05c82cd4b53187dfff1cdc',31,1,'Personal Access Token','[]',0,'2020-01-15 15:50:57','2020-01-15 15:50:57','2021-01-15 15:50:57'),
('688881b7a28b5a57088f7a38416496ec7e0b9986fcefc1ede479d448d918ac08b35f25dd518ddc64',33,3,'Personal Access Token','[]',0,'2020-02-04 14:12:01','2020-02-04 14:12:01','2021-02-04 14:12:01'),
('688afb59bc68ee18efe02b139c4961334b296577f3ae2a9c4d1e284df6b5844d7899640514863811',28,1,'Personal Access Token','[]',0,'2020-01-14 14:13:19','2020-01-14 14:13:19','2021-01-14 14:13:19'),
('68e04e6393728422fe7b6c4b2b3f1b0d5f4ac28c258c0b4c0c875632a899ad45409df70c7a3ecfc7',28,1,'Personal Access Token','[]',0,'2020-01-13 12:15:08','2020-01-13 12:15:08','2021-01-13 12:15:08'),
('6a67c6c51a53846d4440357c21625535284edc3b363b242a32e6f78d74de089b651c8d1df4571e68',28,1,'Personal Access Token','[]',0,'2020-01-13 15:52:56','2020-01-13 15:52:56','2021-01-13 15:52:56'),
('6a9954e01c7f39304e47596f60d96f5cf0fb60279e5d528a50503f3fd562e80d5e49343e55d8a725',28,1,'Personal Access Token','[]',0,'2020-01-14 11:47:52','2020-01-14 11:47:52','2021-01-14 11:47:52'),
('6b61f0ca2965e436f3147e6b24d3e70a724f1af4a718a9c7efc16038484a553d0a789ca8f7123ea6',33,3,'Personal Access Token','[]',0,'2020-01-28 14:44:14','2020-01-28 14:44:14','2021-01-28 14:44:14'),
('6b965a805696eb78ca4517f2b3d6a5448c8bf4d2a82fe47658df083eda4f4f03fac9fb2bd2c4affd',28,1,'Personal Access Token','[]',0,'2020-01-13 13:00:00','2020-01-13 13:00:00','2021-01-13 13:00:00'),
('6baf643daf3e42f9d625735d68738990c2dc9f35aec8333d31ec5bd5ba8149fc03fca3c7f4b2e784',28,1,'Personal Access Token','[]',0,'2020-01-13 16:19:05','2020-01-13 16:19:05','2021-01-13 16:19:05'),
('6bb35da0467ffa735085ed230d313d09a7ddeba2c339824b11a12ffb3c447a5d898b24e54e8eb9af',33,3,'Personal Access Token','[]',0,'2020-01-30 14:51:12','2020-01-30 14:51:12','2021-01-30 14:51:12'),
('6be036bbc6320387fbf5d8fd94d5ab333ac8c6b75bb6f7adffc997a17fa5ec63c91e9e021ec07c9f',1,1,'Personal Access Token','[]',0,'2020-01-08 14:49:34','2020-01-08 14:49:34','2021-01-08 14:49:34'),
('6cecc72bb571c2e22ff895b9a59f0b928e82f2a8aea8e0de21abf98e03547aa9205fabb849cfdc55',33,3,'Personal Access Token','[]',0,'2020-01-31 14:18:57','2020-01-31 14:18:57','2021-01-31 14:18:57'),
('6de285e9937e2def82bf0eb67ae14c64f7757500d1f688eacf06a8cd204ca4914139538bdd7c6f59',28,1,'Personal Access Token','[]',0,'2020-01-14 11:29:06','2020-01-14 11:29:06','2021-01-14 11:29:06'),
('6debb6d7762ff00581cfea2a4f36f0d7550d6c8f634d03dd4303ea7b2f718510c66589550aa86113',33,3,'Personal Access Token','[]',0,'2020-02-04 11:06:53','2020-02-04 11:06:53','2021-02-04 11:06:53'),
('6e36121bb45db9bda4db5e05cbfe70e74f16e5b6e3b3ab68e68a0b9f91ca46f6e64203c1947cee3c',33,3,'Personal Access Token','[]',0,'2020-01-31 15:18:03','2020-01-31 15:18:03','2021-01-31 15:18:03'),
('6e83f2d755c4e4e4b7eabf27be60417893ad36348da3e32b4553df2d6e7f73693cee017eec86ff1e',31,1,'Personal Access Token','[]',0,'2020-01-15 12:17:02','2020-01-15 12:17:02','2021-01-15 12:17:02'),
('6ee6141bd1b54cf986808d462e31369227dd025798559a7907f53fc0bacf39cad59e94fa7f9752b1',33,3,'Personal Access Token','[]',0,'2020-01-31 15:02:13','2020-01-31 15:02:13','2021-01-31 15:02:13'),
('6f4bc6591b73ef5009e054360eeefd5633f4e3535300d52ef81cd8a9aeb589c3b31edcbcf6b3c125',28,1,'Personal Access Token','[]',0,'2020-01-14 11:41:25','2020-01-14 11:41:25','2021-01-14 11:41:25'),
('6f56f56ac4a1d734cfc60240343334ae5cbadf0064365d078d5ca5849844212c08f08897b64af10d',33,3,'Personal Access Token','[]',0,'2020-01-31 17:30:39','2020-01-31 17:30:39','2021-01-31 17:30:39'),
('6f70bb65837384ebdc54d550bc9c62df5af7ae8abb0c9aca71f24f51fe4e6dda21d07e8546891323',28,1,'Personal Access Token','[]',0,'2020-01-15 11:37:10','2020-01-15 11:37:10','2021-01-15 11:37:10'),
('7153cb275fdf110beea8c2bf374c9e2e0f44b345ff06d19241dbbd00979a2ea2e4f544787ff5a3fa',33,3,'Personal Access Token','[]',0,'2020-02-04 11:35:15','2020-02-04 11:35:15','2021-02-04 11:35:15'),
('73c44c529e827f1fd0e92a3c4dcd42a8bdcb5f612d1b170718a29a669fbd0fdbbf061d80040d08b8',33,3,'Personal Access Token','[]',0,'2020-01-31 15:17:13','2020-01-31 15:17:13','2021-01-31 15:17:13'),
('747a93ab768d93d82bd659ed9c84d3514af7647088ef943a31ebb2bda8cac6a9cac598faf8a85580',33,3,'Personal Access Token','[]',0,'2020-02-04 11:53:29','2020-02-04 11:53:29','2021-02-04 11:53:29'),
('74e4403fe72dfd5731500a2160feaa00093a234ba6e193b8da80db9428797c2409e7e7dc0316d0cb',28,1,'Personal Access Token','[]',0,'2020-01-14 15:36:58','2020-01-14 15:36:58','2021-01-14 15:36:58'),
('75219b78c4e45f74493e2ad57f1b752bb6725aa0a805ad8dc9de4b7e1c53d03e1a60a57d44b93959',33,3,'Personal Access Token','[]',0,'2020-02-06 09:49:00','2020-02-06 09:49:00','2021-02-06 09:49:00'),
('75686afa7f340496042203d6bb1b96cd04da0dc536f893be23d9f55f18e9d656bcb4d390b14adc63',28,1,'Personal Access Token','[]',0,'2020-01-13 15:21:36','2020-01-13 15:21:36','2021-01-13 15:21:36'),
('757d4194f7a4e122e76a7b779731b4ad2d223786e82fd5ceb7646ea50fb79f4b35729bbd58dacd11',31,1,'Personal Access Token','[]',0,'2020-01-15 14:53:26','2020-01-15 14:53:26','2021-01-15 14:53:26'),
('76cffd7a509071c7ca82150593c0dfef228b354c4058cba0123bf4a0171b10d3b49d95043ae3de4a',33,3,'Personal Access Token','[]',0,'2020-02-04 11:31:42','2020-02-04 11:31:42','2021-02-04 11:31:42'),
('7748cc419180c947303a196786c9c32266f5c1711717f9f7e5b1eb133e7dcf5791aa9ca20183859e',33,3,'Personal Access Token','[]',0,'2020-02-04 11:55:01','2020-02-04 11:55:01','2021-02-04 11:55:01'),
('77a5d2107eb23f1f1cbb8ee9dcec6c7550c5791e8f8ea190f9db1bb837b62419420192274bb7cce3',28,1,'Personal Access Token','[]',0,'2020-01-13 15:16:26','2020-01-13 15:16:26','2021-01-13 15:16:26'),
('77ac9115bdeec1499606b6c4c8d7740136c7bf63e89aebcb898e7ea61e8cba9b44533880650c4392',31,1,'Personal Access Token','[]',0,'2020-01-15 15:04:01','2020-01-15 15:04:01','2021-01-15 15:04:01'),
('7910d01f8bc025021c2553e2a5b636d9f5b4f133b3fb7d19831db0d433ddc713421cc92b7022f5db',28,1,'Personal Access Token','[]',0,'2020-01-14 11:49:58','2020-01-14 11:49:58','2021-01-14 11:49:58'),
('799d3a87bcf157d6bc1009ff69290b3c9e6ba7cbee49539460e7cd6c882e0ee6bf4089c844ed2d61',33,3,'Personal Access Token','[]',0,'2020-02-04 15:01:17','2020-02-04 15:01:17','2021-02-04 15:01:17'),
('7a6cddfd2cd12ce6f843e9469c05743757d479c5f89108a007d2090d4999e39c7b4b7a7f5209b1cb',31,1,'Personal Access Token','[]',0,'2020-01-15 15:32:45','2020-01-15 15:32:45','2021-01-15 15:32:45'),
('7a85b5b5993890a91f912cc92a8ed1186e2b91b50b15a3138ad623f850885f1bfa7e4f0f132801ed',28,1,'Personal Access Token','[]',0,'2020-01-13 16:33:56','2020-01-13 16:33:56','2021-01-13 16:33:56'),
('7b1ff23791a62b323641582e0c4b5ab49eedadcd1396094de7edc4f7f44fe49b90ed51bc663021d2',31,1,'Personal Access Token','[]',0,'2020-01-15 14:51:50','2020-01-15 14:51:50','2021-01-15 14:51:50'),
('7b9638595d065984911619d0dc71d53b4841fbd556fa047bbadd91fcfa77e0546c991baed7b0c45a',28,1,'Personal Access Token','[]',0,'2020-01-14 14:21:53','2020-01-14 14:21:53','2021-01-14 14:21:53'),
('7bcdb8c1e7789e33aba80348df755bf57f7926a047b9e8b3728501cde764e7b849744e77100b55a7',28,1,'Personal Access Token','[]',0,'2020-01-07 14:34:27','2020-01-07 14:34:27','2021-01-07 14:34:27'),
('7c3744b4bc388dbb8607d4807c1faf180051559c684bf598102a3cb83f0f46490f9eac1d5260bc32',28,1,'Personal Access Token','[]',0,'2020-01-14 14:08:52','2020-01-14 14:08:52','2021-01-14 14:08:52'),
('7c394fa91689dff6ff2e7a9b0e1dd4eeaeb8ed21c2930c85c1202c06973d73df32386af08bd30cec',28,1,'Personal Access Token','[]',0,'2020-01-07 14:43:00','2020-01-07 14:43:00','2021-01-07 14:43:00'),
('7c7e1624998110423fcd09f77f008ddceb913d1533bc3bd23d5324adfdfd6c3f7f07256b18c252fa',1,1,'Personal Access Token','[]',0,'2020-01-08 14:44:20','2020-01-08 14:44:20','2021-01-08 14:44:20'),
('7cd09724310d4c829887ef972f440d6f604f70f447fa854ceafc968c4a7f127914e532c018966fd1',33,3,'Personal Access Token','[]',0,'2020-01-28 12:30:11','2020-01-28 12:30:11','2021-01-28 12:30:11'),
('7d365d92e0c75dc0f52a800ac1e51a36de0fc856bb93c06c7f4c258b909b225ce3bf9621619a851b',28,1,'Personal Access Token','[]',0,'2020-01-13 16:35:38','2020-01-13 16:35:38','2021-01-13 16:35:38'),
('7d7497aeb57707365cc4241420b47508fc053e051c24f853e1d434cd0e73571a3ea4f3ff91aed6b3',28,1,'Personal Access Token','[]',0,'2020-01-14 11:40:40','2020-01-14 11:40:40','2021-01-14 11:40:40'),
('80011602c11a4e517c0eb0df72d4afa275b8faf98652f13f5c773338a4059ccea12e45f3c7b8a3cc',31,1,'Personal Access Token','[]',0,'2020-01-15 14:48:04','2020-01-15 14:48:04','2021-01-15 14:48:04'),
('8060ab971e30db1309d42b8c31e62a8e348c7195bf56a97f601fa43294cac87ee0ab6bc6dc22f4cc',28,1,'Personal Access Token','[]',0,'2020-01-13 16:00:03','2020-01-13 16:00:03','2021-01-13 16:00:03'),
('80e6a8bd231363d3e4cefbdab49b64898c67e29017bf561c5b62836e03c2f0249ad6d6fa413145fe',33,3,'Personal Access Token','[]',0,'2020-01-31 15:28:06','2020-01-31 15:28:06','2021-01-31 15:28:06'),
('82a49a02e475d2f30a46f56756383f91fd96247c2f125a217c37dcf0cd7655d86319ea244e045d9c',11,1,'Personal Access Token','[]',0,'2020-01-06 12:42:44','2020-01-06 12:42:44','2021-01-06 12:42:44'),
('836e8a7669b75fb3c778e606956466beaa187554605131db9bc37fa525d54f4fe13949c1e0a9ad5d',28,1,'Personal Access Token','[]',0,'2020-01-14 14:35:21','2020-01-14 14:35:21','2021-01-14 14:35:21'),
('84b6fa29d2e742f361fabfad280847225ba7a9065c867cbeece6ef1fe172d4823cf57ffdbf469690',31,1,'Personal Access Token','[]',0,'2020-01-15 14:56:45','2020-01-15 14:56:45','2021-01-15 14:56:45'),
('8500dbe69d1f8d027cfb4a6776c23ca16f81eec4c92cf084899887b8d349632cf3fc546ae163c4b5',33,3,'Personal Access Token','[]',0,'2020-02-03 10:48:45','2020-02-03 10:48:45','2021-02-03 10:48:45'),
('85a4bddfb5965778c225ad998ba013777e6671c47ac54ad4645b51b2865a9015a3664658a74dc298',33,3,'Personal Access Token','[]',0,'2020-01-28 12:50:35','2020-01-28 12:50:35','2021-01-28 12:50:35'),
('873d30b669f65a3cde35b7d483f7eefa32838beaffea46e7babd30b28c458a8feb7093ff066415d9',28,1,'Personal Access Token','[]',0,'2020-01-14 14:31:52','2020-01-14 14:31:52','2021-01-14 14:31:52'),
('8762c1b903648da47c562bd058cb3b42ca6a9fce6a8a26fc7e59e9b7b9443b965607090bc3f48f55',28,1,'Personal Access Token','[]',0,'2020-01-13 12:24:34','2020-01-13 12:24:34','2021-01-13 12:24:34'),
('87ea5d82c17547d8541fde5747fc2ef642331362c1de8d5ad5c4d13b2b835ed51d3a875ceb3654f7',31,1,'Personal Access Token','[]',0,'2020-01-15 16:09:31','2020-01-15 16:09:31','2021-01-15 16:09:31'),
('8862eb60b9e9e0b25585ff5b200b882cc6adf334c13c79a8e9f9b45d9f426f0301193ed039f1489e',28,1,'Personal Access Token','[]',0,'2020-01-14 15:41:54','2020-01-14 15:41:54','2021-01-14 15:41:54'),
('88e6875b413d72b3e750314949ac07565f6ac8492a09b92b76eb1a3f516d225bd074d1acf3c1a89c',33,3,'Personal Access Token','[]',0,'2020-02-04 11:57:49','2020-02-04 11:57:49','2021-02-04 11:57:49'),
('894ec5c6b0dd5edea8705b246bbb6d869f318123329d8fbc2704514c2a775a114718fb535c738d3c',28,1,'Personal Access Token','[]',0,'2020-01-13 15:55:27','2020-01-13 15:55:27','2021-01-13 15:55:27'),
('8980977ef86ecda40ae704299b77051a7db4db6c3782c895d47398bf9f28465b3388d489102cb67b',33,3,'Personal Access Token','[]',0,'2020-02-04 11:01:27','2020-02-04 11:01:27','2021-02-04 11:01:27'),
('89a27da17b659d1ede277f095535f475e5ef303847fbd733792f2873abb93969a8a6b607fc8f976a',33,3,'Personal Access Token','[]',0,'2020-02-04 14:36:20','2020-02-04 14:36:20','2021-02-04 14:36:20'),
('89bd96b472a8d4225d105cb464b3eea1e389346a95fc635383d2731ad61a2086869befb1298a8756',28,1,'Personal Access Token','[]',0,'2020-01-13 13:48:46','2020-01-13 13:48:46','2021-01-13 13:48:46'),
('8acdd928c36fb7664168685828f76d8be4499d99811b39980089dd4ee8b7a1f995290b44f6b89d20',28,1,'Personal Access Token','[]',0,'2020-01-13 14:23:53','2020-01-13 14:23:53','2021-01-13 14:23:53'),
('8c13e3b2cee83c9f96e247f1bd4abaeefc0d074f2157d34d6a1732ea42372d39fc09a6eb99fd6d43',28,1,'Personal Access Token','[]',0,'2020-01-07 12:57:27','2020-01-07 12:57:27','2021-01-07 12:57:27'),
('8d91f8d32e0b71e17cfdac5ff3bd7b5acfbd802c9027bfd9f138a7508984a082e6ebdb8adafe3039',33,3,'Personal Access Token','[]',0,'2020-02-03 17:01:12','2020-02-03 17:01:12','2021-02-03 17:01:12'),
('8df33edb5876fe4c838823c7bf75131ce08dfc29a2d10bf8a49fb258bc7043ce0194a52100ac44af',28,1,'Personal Access Token','[]',0,'2020-01-13 16:04:33','2020-01-13 16:04:33','2021-01-13 16:04:33'),
('8e5b757e85b3d8a6a1b5997ee51c4eeab3d0ec5627fd66142d2f7a49d0af39e5cb29addf6181e35e',28,1,'Personal Access Token','[]',0,'2020-01-13 14:03:50','2020-01-13 14:03:50','2021-01-13 14:03:50'),
('8e7392408fc6467f9d66966afbf1f4a1d1b589b65900d57205d432f4b8ed3f84af5d37ccdf826c59',28,1,'Personal Access Token','[]',0,'2020-01-14 14:10:47','2020-01-14 14:10:47','2021-01-14 14:10:47'),
('8eb9c34b1412ca0784207b12dbe1b6f86a9f4c23a9e93aff269381e81299d107c1dfb92aef1a002a',33,3,'Personal Access Token','[]',0,'2020-02-03 17:21:48','2020-02-03 17:21:48','2021-02-03 17:21:48'),
('8ec6672dc236a584482978c67abda275f6e047b210a1e1ad9cdd99f99781e2e6033c5fca080ffd6e',31,1,'Personal Access Token','[]',0,'2020-01-15 17:18:13','2020-01-15 17:18:13','2021-01-15 17:18:13'),
('8fe57c00cf88d223249c64e94c46f7d55564cd5f24c010cfc18148ef3a883583a61b00027164b885',28,1,'Personal Access Token','[]',0,'2020-01-13 17:00:39','2020-01-13 17:00:39','2021-01-13 17:00:39'),
('909d6e85dcc2d130e671662a4f7b8d35ddc75a44480dfb94845c96b6754b472490e3feaa7fa3d1fd',28,1,'Personal Access Token','[]',0,'2020-01-13 14:55:19','2020-01-13 14:55:19','2021-01-13 14:55:19'),
('90c621100f3131e129f3c6a9b080ac3a8609b11f031f5213d67bd425b7eb81b8b1b38461298e8a36',31,1,'Personal Access Token','[]',0,'2020-01-15 16:26:53','2020-01-15 16:26:53','2021-01-15 16:26:53'),
('90f6717f61edd2224443f638aee8f53b39512c9077f16fb38d29b805a5670e51e3b9c9840b2dd33d',28,1,'Personal Access Token','[]',0,'2020-01-13 12:58:10','2020-01-13 12:58:10','2021-01-13 12:58:10'),
('90f8a643a5f99af4c8a610aa003ea5d8ca20b570ddf694cbb664f854b90de4bf5fe40efe387ea6d8',33,3,'Personal Access Token','[]',0,'2020-01-31 14:23:57','2020-01-31 14:23:57','2021-01-31 14:23:57'),
('9164265a1b295a3b914066a1f41c873b353c8dbf7ee65491faf284fd369f7244d404310a5a4935c0',33,3,'Personal Access Token','[]',0,'2020-02-04 14:33:55','2020-02-04 14:33:55','2021-02-04 14:33:55'),
('91f3e8b734c4a30ee107f57da5866546233ad0dbf0ea5a43db8e850c0de99734769f6dab24836b5a',33,3,'Personal Access Token','[]',0,'2020-01-31 16:31:17','2020-01-31 16:31:17','2021-01-31 16:31:17'),
('933a4e7f779dd90c1f70a39598902ad84662f7ca10b172e8a7cce60eee2460093c7e87d4c6aaa3e4',33,3,'Personal Access Token','[]',0,'2020-02-06 10:10:52','2020-02-06 10:10:52','2021-02-06 10:10:52'),
('93581f34c3b609da823bd75727ca4ab2691ff8273b3dbf7706a22bb4045fb2f4aeb71549a3c47522',31,1,'Personal Access Token','[]',0,'2020-01-15 15:33:33','2020-01-15 15:33:33','2021-01-15 15:33:33'),
('935dd7e2aa3e8918c3c9da355769248c581d63028dc8370d930ac691141e1382208228a558ba2944',31,1,'Personal Access Token','[]',0,'2020-01-15 14:59:33','2020-01-15 14:59:33','2021-01-15 14:59:33'),
('95e6913ae7fa4988d76a9e4cafcca38e3a6bf450a44e5625c5395bc714ac591de8f14236af819c5e',28,1,'Personal Access Token','[]',0,'2020-01-14 11:33:58','2020-01-14 11:33:58','2021-01-14 11:33:58'),
('9781265f8faa8a64faf9ffb855eb4090710b4219a12f38c0053c00b34bdc97e5d66356de19d0a779',33,3,'Personal Access Token','[]',0,'2020-01-31 15:06:19','2020-01-31 15:06:19','2021-01-31 15:06:19'),
('9793cc677b3885565feab0295306fbfd4a5452b8e3973170b130e14a9fa201607799da60e00b2c37',28,1,'Personal Access Token','[]',0,'2020-01-13 12:10:53','2020-01-13 12:10:53','2021-01-13 12:10:53'),
('99507ea5bb296671c25289cc1dfda1c63f68243f84d1aaba2d8fcdb251e73c5d1b81382865cf810f',33,3,'Personal Access Token','[]',0,'2020-01-31 15:29:30','2020-01-31 15:29:30','2021-01-31 15:29:30'),
('99630d868c3f61a129674a48649ec39c0e9e6632b4f1b537251c331d269d9e7d8c2ae130ae26de98',28,1,'Personal Access Token','[]',0,'2020-01-13 14:14:07','2020-01-13 14:14:07','2021-01-13 14:14:07'),
('9a3bac50001fb032ec5dbf3e0537b4e370f6132cea643ecff687bd59faef9235c9e27cb32d3ef8db',28,1,'Personal Access Token','[]',0,'2020-01-13 16:45:37','2020-01-13 16:45:37','2021-01-13 16:45:37'),
('9a59b7ca363a04e06b0430b4809f58f3630b0b7ccaf41da7ac077c0d022703df6af2a521f69b2ab7',31,1,'Personal Access Token','[]',0,'2020-01-15 16:31:24','2020-01-15 16:31:24','2021-01-15 16:31:24'),
('9a985d65f92756f04fc21bf5d9a51a508bc032696b3276862b23cadf856b7dc0600465953ff3ee44',28,1,'Personal Access Token','[]',0,'2020-01-14 15:32:35','2020-01-14 15:32:35','2021-01-14 15:32:35'),
('9b5e3206317a822194c6392a8dca40016754b0717447f0821ef66006481ec894cbc882dbe43d060b',33,3,'Personal Access Token','[]',0,'2020-02-03 12:07:21','2020-02-03 12:07:21','2021-02-03 12:07:21'),
('9b9dd0e5f25ea08134a306fbffa1a33292583951893d7b4666fbae29f95c1420ca68e52efd7750cc',31,1,'Personal Access Token','[]',0,'2020-01-15 15:00:22','2020-01-15 15:00:22','2021-01-15 15:00:22'),
('9c8d297bfb89ba4d48dff7470ba5ce03f4f6981f973b490ffe072f38f2024ea0eaa8c64927064d8c',28,1,'Personal Access Token','[]',0,'2020-01-13 14:08:50','2020-01-13 14:08:50','2021-01-13 14:08:50'),
('9e73a3794fe405ee628b17e1173d8b9e116b82af4deacdf2e5bdae1a83a2d56af485688dab324da3',1,1,'Personal Access Token','[]',0,'2020-01-08 14:44:12','2020-01-08 14:44:12','2021-01-08 14:44:12'),
('9f6ea93794c0b8df4e9bc2aece505bfd86d960cedd46fda0867f05fb60e647ef52d85dc1151f5266',31,1,'Personal Access Token','[]',0,'2020-01-15 17:19:57','2020-01-15 17:19:57','2021-01-15 17:19:57'),
('a0538b43a6970e5bcf4451456eacb351ef3fbb05f9e7f66fa939579f819ed7b50db8c6dc4e74a5a0',33,3,'Personal Access Token','[]',0,'2020-02-04 11:28:16','2020-02-04 11:28:16','2021-02-04 11:28:16'),
('a33aa419b49b1b114a6c9cfb7219ecee02a11d95586437351658d1940eaea181707b82781e4615e6',11,1,'Personal Access Token','[]',0,'2020-01-06 14:06:48','2020-01-06 14:06:48','2021-01-06 14:06:48'),
('a47c3900b0c3e7052726e5f886f32c28c5b68039c06cd222db9754cfc227ec97110c42227b5ff71d',32,1,'Personal Access Token','[]',0,'2020-01-21 14:19:19','2020-01-21 14:19:19','2021-01-21 14:19:19'),
('a48d74e6e82bbcca235c32662c3181fadece41f37337e518672077a01f72ffa3aadcd099f12d5bf3',31,1,'Personal Access Token','[]',0,'2020-01-15 16:21:15','2020-01-15 16:21:15','2021-01-15 16:21:15'),
('a615d609f945d97114e43e7ae1684a39e77d6bd0552e1f1ea9cf8747e835c07dbf502f8951af7566',28,1,'Personal Access Token','[]',0,'2020-01-14 15:02:23','2020-01-14 15:02:23','2021-01-14 15:02:23'),
('a6e60f3281b94f25dc2648168ece289ff24ced4c0250541ae890782115599ae816219ab4bfeeac07',33,3,'Personal Access Token','[]',0,'2020-01-28 14:44:53','2020-01-28 14:44:53','2021-01-28 14:44:53'),
('a774355014e03fa30d4f6083a04f513293b353c7a328d77aa3825a5755090fdd3649a40fb2addd84',33,3,'Personal Access Token','[]',0,'2020-01-31 17:21:08','2020-01-31 17:21:08','2021-01-31 17:21:08'),
('a796222600c0bca936cca9c1dcf6f5d11647593a91a4c3ee1dc3ec48853377dfd88774cb595ff0ec',33,3,'Personal Access Token','[]',0,'2020-02-04 11:56:17','2020-02-04 11:56:17','2021-02-04 11:56:17'),
('a86a4f23803120ac0310ee2a0d5f2863e6ff6f6cfb40140ee11b333eb8227bfc87f7c8670b6fc8c0',31,1,'Personal Access Token','[]',0,'2020-01-15 15:24:10','2020-01-15 15:24:10','2021-01-15 15:24:10'),
('a93d2f2d66ed073da80714f00ab1038f6649e1db633d08fee2a21c74e7ae44a3aea1ce646bb464df',33,3,'Personal Access Token','[]',0,'2020-02-05 12:23:04','2020-02-05 12:23:04','2021-02-05 12:23:04'),
('a94b47655462b8e288082b2a9526398f0c80021ab144a3bb632f37d96f0b4c2d4bf2aa801d2bb0ef',33,3,'Personal Access Token','[]',0,'2020-01-28 14:22:17','2020-01-28 14:22:17','2021-01-28 14:22:17'),
('a951daa4edc6a093d94333ef7afdac8829e12ce5337cb40a4612d3cea88bff429b0d1550be3473dd',31,1,'Personal Access Token','[]',0,'2020-01-15 16:36:15','2020-01-15 16:36:15','2021-01-15 16:36:15'),
('aa2087eddb059f71b371f67dbcd929a878ebe6ef7345a51cbfbf441ff41b1f3f093051dbd8bacbc7',33,3,'Personal Access Token','[]',0,'2020-02-04 11:20:07','2020-02-04 11:20:07','2021-02-04 11:20:07'),
('aa6c43e67821ade78f3b994b84d0bb26df49bb44a1242cd6246410aba76af9ebf72aeb304cc067f9',28,1,'Personal Access Token','[]',0,'2020-01-13 12:56:56','2020-01-13 12:56:56','2021-01-13 12:56:56'),
('aa6cdc74e0da034d79d9ad1b999c9da9505a420f8d4c0c5a48a757442282891626eba6f0b87fa09d',28,1,'Personal Access Token','[]',0,'2020-01-14 11:46:19','2020-01-14 11:46:19','2021-01-14 11:46:19'),
('aad4efb2594055cc74805f9487642dd2d861c3e9b978ebcef4556cd49b484a64fdd6da1ba8ad4b30',28,1,'Personal Access Token','[]',0,'2020-01-13 15:12:43','2020-01-13 15:12:43','2021-01-13 15:12:43'),
('aaeda6eafd98b588faec707746580a21ee201ab6a334c9e33ca30bddc2becd6ba826d9d9ffa7f6b6',33,3,'Personal Access Token','[]',0,'2020-01-28 12:10:59','2020-01-28 12:10:59','2021-01-28 12:10:59'),
('aaffcc7181f255d7c1e9d7a301e7628af70d0fb67fe91b3f81e2d18f050b602d65cec0f5b4205d36',28,1,'Personal Access Token','[]',0,'2020-01-14 14:42:18','2020-01-14 14:42:18','2021-01-14 14:42:18'),
('ae7b6c67fe829d99f172b0aa7f9e37e0362320634b97e36f8c35de7e8f3016a74719be84d1c34b35',28,1,'Personal Access Token','[]',0,'2020-01-14 15:38:15','2020-01-14 15:38:15','2021-01-14 15:38:15'),
('aed0ed1db763956681e72458232ae4f734c94463fa84daaee7d90104ffe6263e98dab46b7236e459',33,3,'Personal Access Token','[]',0,'2020-01-31 12:44:44','2020-01-31 12:44:44','2021-01-31 12:44:44'),
('aefe89f54e48f6749af366a3efed375f9b4ddac8fe03b1bc4db60352e88259673ff30cd48efabc7b',33,3,'Personal Access Token','[]',0,'2020-02-06 10:15:28','2020-02-06 10:15:28','2021-02-06 10:15:28'),
('af5e70b8a5eb44fc9d41217539b70b1320eef82b254508bbec21e1a8b18c542b00e170547f56a39e',28,1,'Personal Access Token','[]',0,'2020-01-13 16:24:42','2020-01-13 16:24:42','2021-01-13 16:24:42'),
('b0264c0b58030007f3a39fd77b8c4caf9bea3e628d3a39308ecc8d301149a60ca50138a8273e1e02',28,1,'Personal Access Token','[]',0,'2020-01-13 17:02:44','2020-01-13 17:02:44','2021-01-13 17:02:44'),
('b0cfa2fc51e70ec6953f1f3d0b3936d37ebe99ef5510beb0b04cbed16787905891cc667dcf7afb57',31,1,'Personal Access Token','[]',0,'2020-01-15 12:21:05','2020-01-15 12:21:05','2021-01-15 12:21:05'),
('b1300642b1d54d73015f3c5d5ff16a737369ace888e29489cdb569689f41eddb5c45d1efb7e70f97',28,1,'Personal Access Token','[]',0,'2020-01-13 13:41:21','2020-01-13 13:41:21','2021-01-13 13:41:21'),
('b2c9598197a7baab589b3207383f062b28aee94ea0a0e58a455314e3fdfc536c1c24b2aebb8b8ac7',33,3,'Personal Access Token','[]',0,'2020-02-04 17:25:59','2020-02-04 17:25:59','2021-02-04 17:25:59'),
('b4298b4b0b8d362b64b1e80b3947edfc9cf45bd14165a692a145b7957d21f912dab75dd2d3f404bf',28,1,'Personal Access Token','[]',0,'2020-01-13 13:52:44','2020-01-13 13:52:44','2021-01-13 13:52:44'),
('b45e54a9fc6ea479453b5f4128d5d095b7e7f1e6bd599b763963c2636b4e5fee9d363bcd05977c68',28,1,'Personal Access Token','[]',0,'2020-01-13 14:16:20','2020-01-13 14:16:20','2021-01-13 14:16:20'),
('b48ab19bfeeeb83e0b4a71d2a52bd2d8293689f325956508fd438f13480c43cc187bf28e4fdd34ac',29,1,'Personal Access Token','[]',0,'2020-01-15 15:58:20','2020-01-15 15:58:20','2021-01-15 15:58:20'),
('b4ef67fbb82e0ad0974beb1cb28c46866acf95db02e22cfa21fe359f5eba99b6442fbce87cc5645d',28,1,'Personal Access Token','[]',0,'2020-01-14 14:03:07','2020-01-14 14:03:07','2021-01-14 14:03:07'),
('b515041b981c90a15ca08618d7c745c3e042c3ada4d19e62f4c4a25cdae17dfc0a1c27b2c6881f53',31,1,'Personal Access Token','[]',0,'2020-01-15 15:36:09','2020-01-15 15:36:09','2021-01-15 15:36:09'),
('b6460d0a96d46b1c03dc5896561a1e662b64294a0ad322bc19fa26a2149b4b08864784cbb642725e',1,1,'Personal Access Token','[]',0,'2020-01-08 14:31:24','2020-01-08 14:31:24','2021-01-08 14:31:24'),
('b728cfb05a6fd02f0d5644adaa9ac5be431d6e6ae74687452d92519c7f348d72c6a2d7981c047db8',28,1,'Personal Access Token','[]',0,'2020-01-13 14:03:09','2020-01-13 14:03:09','2021-01-13 14:03:09'),
('b77ff91b6fe8e42a2a53e09e1491162979fa72f5ac56909b3d106ac76ff08325c323bf2f323be722',28,1,'Personal Access Token','[]',0,'2020-01-13 16:22:21','2020-01-13 16:22:21','2021-01-13 16:22:21'),
('b8102f54acdc0092ee71c3d89703921aa5d890c35ed9008066572435797ec461341977d9d293b026',31,1,'Personal Access Token','[]',0,'2020-01-15 17:29:12','2020-01-15 17:29:12','2021-01-15 17:29:12'),
('b8c41414e4205ea004abcb181bc9152eb2a66707596d2df926928dccb2bcb9c730bb6632924f9ae0',31,1,'Personal Access Token','[]',0,'2020-01-15 12:22:40','2020-01-15 12:22:40','2021-01-15 12:22:40'),
('b8f019f52ed8eadca7093cf2773e15ae097f940b2f63148d949a8800264671bf90917562ea1f13b2',33,3,'Personal Access Token','[]',0,'2020-01-31 16:14:54','2020-01-31 16:14:54','2021-01-31 16:14:54'),
('ba1381c483f49e9fb12afe0f591a75d16e5f96da108191ad841a82493cec2e8487dda55c65f7d371',28,1,'Personal Access Token','[]',0,'2020-01-14 13:47:35','2020-01-14 13:47:35','2021-01-14 13:47:35'),
('ba22af961e4cd0cbbd3667d529fd7e99502c683d66c9dd0159a90fbd2b99d2724f8d8f5cbabb8c6f',33,3,'Personal Access Token','[]',0,'2020-01-28 12:21:46','2020-01-28 12:21:46','2021-01-28 12:21:46'),
('bbf06152fbc21f56ccd57ea721f12a38c388f42d20487c016595382c10d43070a9ba71003e1ca305',28,1,'Personal Access Token','[]',0,'2020-01-14 15:42:35','2020-01-14 15:42:35','2021-01-14 15:42:35'),
('bda99127461585280f65e81fc5c27427c166047a1d7087ee0c900d1c85913df436739b6c8507cb79',31,1,'Personal Access Token','[]',0,'2020-01-15 16:27:34','2020-01-15 16:27:34','2021-01-15 16:27:34'),
('bede7eda724bdb07d1de103bc621c389a4e0d943655f298469ab17539db1d3a4942f9dc9d2eb9261',33,3,'Personal Access Token','[]',0,'2020-01-28 12:12:46','2020-01-28 12:12:46','2021-01-28 12:12:46'),
('bf31e5880589c3655c6c4d500d6f867510febbf02ab82bb9dbba6f624f60d2503dc02ebf0fa59347',31,1,'Personal Access Token','[]',0,'2020-01-15 16:49:14','2020-01-15 16:49:14','2021-01-15 16:49:14'),
('bf5b38133e3d92b3731fa187d90e288de02da3ae8bf66e0461d6204c383b71c15c8c8f3d5c618819',33,3,'Personal Access Token','[]',0,'2020-02-04 14:07:25','2020-02-04 14:07:25','2021-02-04 14:07:25'),
('c08558f4eb3bcd73488efc32d3972881f1c61eefebc30637836295bb03bc91b592f87980ebc5d6f2',31,1,'Personal Access Token','[]',0,'2020-01-15 17:22:08','2020-01-15 17:22:08','2021-01-15 17:22:08'),
('c0ce0637fe11eab5bea0649b95c774e70532246aa94db5590431cc9493db1e73a983a163b1ddd94c',33,3,'Personal Access Token','[]',0,'2020-02-04 14:19:22','2020-02-04 14:19:22','2021-02-04 14:19:22'),
('c36285c78fabf27c8f781229d7e6ecbc961efe1fee434cd9351d0ef3d62beb101212e6e3c7e04766',31,1,'Personal Access Token','[]',0,'2020-01-15 17:27:40','2020-01-15 17:27:40','2021-01-15 17:27:40'),
('c39f92108529d4539967abbb3d31a90d000727ddc590ad5aa894f85a93a4b034fdd3d1d7574a4f47',28,1,'Personal Access Token','[]',0,'2020-01-13 12:19:44','2020-01-13 12:19:44','2021-01-13 12:19:44'),
('c3a345b7b69712dd13921b9a18c67027b201bb3705ab0de6c1ae320ae0f3034757a8eb734f685bc2',28,1,'Personal Access Token','[]',0,'2020-01-14 14:20:23','2020-01-14 14:20:23','2021-01-14 14:20:23'),
('c3fff401d636babbf3f15e1def614a9a99ff9fdee62c6b1ce755ac86b383035838ec09d6ca1761ca',28,1,'Personal Access Token','[]',0,'2020-01-13 16:28:43','2020-01-13 16:28:43','2021-01-13 16:28:43'),
('c42e5644b77b52354bfaac2f06cbece69d78330b366d762f16380380ddb8b0a9b3d7a78ae92172b7',28,1,'Personal Access Token','[]',0,'2020-01-13 17:48:13','2020-01-13 17:48:13','2021-01-13 17:48:13'),
('c48d64be477532e74807fa93123e20753fd46bff6f7cb1cb5a51c1d8327ad6c0beba60bae562a33a',28,1,'Personal Access Token','[]',0,'2020-01-13 13:43:35','2020-01-13 13:43:35','2021-01-13 13:43:35'),
('c672704dfa0e38daa51bca8fc4010dd7ed040f0b3c3f233e47c9d0147f56e17ec0898117d9fcec8c',31,1,'Personal Access Token','[]',0,'2020-01-15 14:29:28','2020-01-15 14:29:28','2021-01-15 14:29:28'),
('c6849287e7be821a68a58dc6c1416e10a9a79da189e4ddeb492ddcee92e6517fad0c0ad188eea6ff',33,3,'Personal Access Token','[]',0,'2020-02-04 14:10:10','2020-02-04 14:10:10','2021-02-04 14:10:10'),
('c773f123289c7d38302ee41e5597bc1515dccd1ff63542d0344f46c42c699dddb85a8bc86df23061',11,1,'Personal Access Token','[]',0,'2020-01-06 13:55:07','2020-01-06 13:55:07','2021-01-06 13:55:07'),
('c7e48a06522960aec5748cf64e52d8005e6425b8230e3f1abf4c52e9fa4d84888761e7be5281928b',31,1,'Personal Access Token','[]',0,'2020-01-15 16:52:55','2020-01-15 16:52:55','2021-01-15 16:52:55'),
('c9ddf9a4166c60e04d5d5e66ad50669c4cc3b00a60728b5761dc15266d179ae3999af7eb59e01041',28,1,'Personal Access Token','[]',0,'2020-01-13 12:05:20','2020-01-13 12:05:20','2021-01-13 12:05:20'),
('ca382698660edd7485f175403a8a9c6a1263113e97bfaf275f5f47526db0529cfe46219db5b5027c',33,3,'Personal Access Token','[]',0,'2020-02-06 09:49:57','2020-02-06 09:49:57','2021-02-06 09:49:57'),
('cd60d8e97c086852f895e1291e357cd585c685d61beaa623925c49b2615d95014bb76d01796d7d0a',28,1,'Personal Access Token','[]',0,'2020-01-14 14:36:31','2020-01-14 14:36:31','2021-01-14 14:36:31'),
('cd933859cf95113852f4a986200645f08f08db00f205ad10dede56e3c3b44d05ecf770711380717b',33,3,'Personal Access Token','[]',0,'2020-02-03 16:56:46','2020-02-03 16:56:46','2021-02-03 16:56:46'),
('cdbcfe550d874b2532e6e7e74940ce63df3ad1d23f226f271d71b215cdf4f62d1add7b2389fb8916',31,1,'Personal Access Token','[]',0,'2020-01-15 16:19:41','2020-01-15 16:19:41','2021-01-15 16:19:41'),
('cdff73b5b02fdee073781066e8d8d1575f9a7e15d66f7f7a10f256caf3647a38680e2f959b612400',28,1,'Personal Access Token','[]',0,'2020-01-14 13:58:53','2020-01-14 13:58:53','2021-01-14 13:58:53'),
('ce896f3d120a750172752cd8df40acbd324690dbfeec7f19a88f9b710f8bcfd34c270248ef72fdc3',28,1,'Personal Access Token','[]',0,'2020-01-13 13:42:49','2020-01-13 13:42:49','2021-01-13 13:42:49'),
('d20966a23dcd556b3e9144837bd7e212336c874cc78ea3ea4f2b3150969d1ff18bec4801b618ced0',31,1,'Personal Access Token','[]',0,'2020-01-15 17:41:04','2020-01-15 17:41:04','2021-01-15 17:41:04'),
('d2c895e8aad21baa77be9edaede5ecc9735627dae8a9c7cebfe86d27df0eb8d3b5dc8845979f1fe5',31,1,'Personal Access Token','[]',0,'2020-01-15 16:34:19','2020-01-15 16:34:19','2021-01-15 16:34:19'),
('d2ee594480b9e06c701d596a81ad262df5ab7180a0b0e6c4294db9372124e227fa8bd422fb6a1a41',31,1,'Personal Access Token','[]',0,'2020-01-15 15:26:14','2020-01-15 15:26:14','2021-01-15 15:26:14'),
('d3241d530eb8b53e8ce9f20521c821ddb375ccbea6ccfc617588cfa0be0e8d3bd50f4534d98a3a89',28,1,'Personal Access Token','[]',0,'2020-01-07 14:34:30','2020-01-07 14:34:30','2021-01-07 14:34:30'),
('d3a687d4231e2e4d54f9e44080656b220e8a29e254904e98eb560f5943eb61dbe3a2255dddac4db2',28,1,'Personal Access Token','[]',0,'2020-01-14 14:59:27','2020-01-14 14:59:27','2021-01-14 14:59:27'),
('d3a6929a73ed66082a756966e94954a4aa4035e1e23aade6afaae9e70310eef8d2ac967df81a47c1',11,1,'Personal Access Token','[]',1,'2020-01-03 21:59:00','2020-01-03 21:59:00','2021-01-03 21:59:00'),
('d41bbf7259987f9c3152ed49df2462897873efad01e37873890126fe8e1c471efebe7b48559ac572',28,1,'Personal Access Token','[]',0,'2020-01-13 15:36:39','2020-01-13 15:36:39','2021-01-13 15:36:39'),
('d4437a08cae00e14fc44a034d2ce385629ed3cc61f0e74b05c595c0e63b21ca59eeb00b0c6ac41a0',33,3,'Personal Access Token','[]',0,'2020-01-28 12:54:02','2020-01-28 12:54:02','2021-01-28 12:54:02'),
('d52b931fc240e38aa8fed4142ff5589c87e10fbc55b6783956dbf11206ff449aaf0ead1bc291831e',28,1,'Personal Access Token','[]',0,'2020-01-14 11:44:17','2020-01-14 11:44:17','2021-01-14 11:44:17'),
('d5694d51bb92e91451c1e612758352c67d8bb5eaf7610012fbd984a0ce70000ba5e6f933eeea5f02',33,3,'Personal Access Token','[]',0,'2020-02-04 11:06:17','2020-02-04 11:06:17','2021-02-04 11:06:17'),
('d5b74fcffabe19ada31e80e267e5f9392c2043f0c5b740bd4e8a6818a931009a473fb511c7456a6e',28,1,'Personal Access Token','[]',0,'2020-01-13 14:48:23','2020-01-13 14:48:23','2021-01-13 14:48:23'),
('d691ce23ba25f4fc3d6146a8e885477cf76d716951f1faaed1ef52836d12a411a6ab15010cb57039',28,1,'Personal Access Token','[]',0,'2020-01-13 16:07:04','2020-01-13 16:07:04','2021-01-13 16:07:04'),
('d756606d6eff9aaeabd0ff0a5f67c67632e3dcee9f0c88ed94a32cf5e1e66b82b74aa14c179596ae',28,1,'Personal Access Token','[]',0,'2020-01-07 14:15:34','2020-01-07 14:15:34','2021-01-07 14:15:34'),
('d7bc67b8795b1f4839e18520a7977a5b2de0247ec2a7a3b4da539bf33cc9a85fac1396b05abb1f71',31,1,'Personal Access Token','[]',0,'2020-01-15 16:43:47','2020-01-15 16:43:47','2021-01-15 16:43:47'),
('d8349a8a78a42d42b6073f648845f78796395514a8972e8f165b19de014406e197160b4252f70cad',28,1,'Personal Access Token','[]',0,'2020-01-13 16:04:32','2020-01-13 16:04:32','2021-01-13 16:04:32'),
('d84abd1242f398f675973529ee515623a67cfa404e4505ef836467ec6f1e1bcad30b232d48158e8c',33,3,'Personal Access Token','[]',0,'2020-01-28 14:55:41','2020-01-28 14:55:41','2021-01-28 14:55:41'),
('d97019a129a400868d1235b1cb8df19cc820ad5ad9927a03cfe016da5700f8d4869a5836906febfa',28,1,'Personal Access Token','[]',0,'2020-01-13 14:49:08','2020-01-13 14:49:08','2021-01-13 14:49:08'),
('dcce4fb6ba3b4a20c92fb6e5fb42fe564e9156c5487390bbf4a734dc45762ed7c7f0e5a7c847e3ca',31,1,'Personal Access Token','[]',0,'2020-01-15 15:06:28','2020-01-15 15:06:28','2021-01-15 15:06:28'),
('dcf0715fdc2a95b062c7abb6be7ce3da78e9d8413636796f6856eea2e3a657a389e2e57d6befc035',33,3,'Personal Access Token','[]',0,'2020-02-04 10:57:43','2020-02-04 10:57:43','2021-02-04 10:57:43'),
('dd24c76606b0ca3b9c47133295225c26d2ba84b921fdc10a210b2c83767cdbc8f0e15e1f253e52ab',33,3,'Personal Access Token','[]',0,'2020-02-03 17:16:53','2020-02-03 17:16:53','2021-02-03 17:16:53'),
('dd2832f60f8b26f3414bfa3a517366892b84e3642d4c70860c2695c20ff938ba5608267fb8d10372',28,1,'Personal Access Token','[]',0,'2020-01-13 16:15:09','2020-01-13 16:15:09','2021-01-13 16:15:09'),
('dd9e165161f257a46fd5dc9c09f4531abe6f2006c05421f638186f47699d9cb27452c2ef9607dafc',28,1,'Personal Access Token','[]',0,'2020-01-14 11:43:20','2020-01-14 11:43:20','2021-01-14 11:43:20'),
('df2e35b20fac6e2babf26a0b272b6e295f3ff658c56ea379c9489786eb36d58dd5542b020d7a4257',31,1,'Personal Access Token','[]',0,'2020-01-15 16:37:56','2020-01-15 16:37:56','2021-01-15 16:37:56'),
('e070ee732162ec4c407a4f9f11b9b3e45b305c548e39047bbdf4d8a76a54fe63319646094734ede0',28,1,'Personal Access Token','[]',0,'2020-01-13 12:06:08','2020-01-13 12:06:08','2021-01-13 12:06:08'),
('e15d26fb29737170d79853d9cbf89ae89f2b4ebdceb752d5c1eefd1359f5ceabd5ddfbc41e921c79',28,1,'Personal Access Token','[]',0,'2020-01-14 15:50:17','2020-01-14 15:50:17','2021-01-14 15:50:17'),
('e19823a07b2efb8403d8d5422adfcac39ef4f182a7688cd10d6f992e7e2a65a13f6162e50f90d925',11,1,'Personal Access Token','[]',0,'2020-01-06 16:50:39','2020-01-06 16:50:39','2021-01-06 16:50:39'),
('e24d1b76b3b2c2b24cb0138fdf82e3b863302f7cefbbed6825daa0bf7960ac943e4510aad2750137',28,1,'Personal Access Token','[]',0,'2020-01-13 16:44:52','2020-01-13 16:44:52','2021-01-13 16:44:52'),
('e263b7aad5f4b847d0a45b0276f4e81bc98b0a4c2375e532ad3e4adf90c4c16d2508a87427a2c169',28,1,'Personal Access Token','[]',0,'2020-01-14 11:45:20','2020-01-14 11:45:20','2021-01-14 11:45:20'),
('e27f72626ed9c9615174b39343dfbdddac2ffa5153b973af8930d64196511e386c1b100f0731c23f',31,1,'Personal Access Token','[]',0,'2020-01-15 16:55:01','2020-01-15 16:55:01','2021-01-15 16:55:01'),
('e30acd98ee9b5de44bf074af7237e96fc01fd2a056c625a588c1c024c3f0a05504b5768f1810ae19',33,3,'Personal Access Token','[]',0,'2020-01-28 12:54:58','2020-01-28 12:54:58','2021-01-28 12:54:58'),
('e32789cea12688a80706dd186a99208f4bf5e194090c9a982f1582364d12ecfdde9f90adf255315e',33,3,'Personal Access Token','[]',0,'2020-02-04 15:14:03','2020-02-04 15:14:03','2021-02-04 15:14:03'),
('e36e0b23f91ce0f294b9d94532bdf9b9957f0a51f7df0145235d39b6cd84bfae52acfe9cc75f63f8',33,3,'Personal Access Token','[]',0,'2020-02-03 10:52:32','2020-02-03 10:52:32','2021-02-03 10:52:32'),
('e3c0355dbc4725d361ae7da5d745200655d92c40aa016cf1c9e7af30b27e4f37687161d0b44a30cf',33,3,'Personal Access Token','[]',0,'2020-02-06 10:12:07','2020-02-06 10:12:07','2021-02-06 10:12:07'),
('e4f6907da0a2bfecdc60a2ee1fb201c1d57be6bf1f2181b9c1f933aa6ad0618302ce007f3d232c07',33,3,'Personal Access Token','[]',0,'2020-02-04 15:26:30','2020-02-04 15:26:30','2021-02-04 15:26:30'),
('e6cdbc7ea582783bca32e031c214b847e56a93384213aa6ac12e3592596fb8a4d0bc66c317fce814',31,1,'Personal Access Token','[]',0,'2020-01-15 16:56:12','2020-01-15 16:56:12','2021-01-15 16:56:12'),
('e7dfc1ee365e89ec680fda3cde7410ec7f954714e4fba43ba0adf16826ef45232c0a4e00b95dffe9',28,1,'Personal Access Token','[]',0,'2020-01-14 15:40:12','2020-01-14 15:40:12','2021-01-14 15:40:12'),
('e7ec65a05b0fc0d74eb4f624ebe02be8125c972d48cfddb4a56a32a86cbdc464df0c7aebd0ca5cef',33,3,'Personal Access Token','[]',0,'2020-02-04 15:14:55','2020-02-04 15:14:55','2021-02-04 15:14:55'),
('e81ca8478a8d5ed14e9515b101f5192f9ef5cf261e4717598c7b661e833f4abcfb20bcacf2e85b54',31,1,'Personal Access Token','[]',0,'2020-01-15 15:41:39','2020-01-15 15:41:39','2021-01-15 15:41:39'),
('e8646334ebcaf48d1f0711afc947004be1d47e0e6222adba27ccee58a14671f9c5f42a1ba679832e',33,3,'Personal Access Token','[]',0,'2020-01-31 14:59:22','2020-01-31 14:59:22','2021-01-31 14:59:22'),
('e8fe4c564a41aef349e569a49ece812bcfe57d608ebd12ee5ed6d951b59c86a0f384a200a384d1ea',31,1,'Personal Access Token','[]',0,'2020-01-15 16:42:38','2020-01-15 16:42:38','2021-01-15 16:42:38'),
('ea94ea89f83e27822b33d91c398e96fb3caf8bc0f1f01eaaf0e24458e3cd0abb455df3d4b0b39c19',33,3,'Personal Access Token','[]',0,'2020-01-31 14:26:13','2020-01-31 14:26:13','2021-01-31 14:26:13'),
('eb7f3155893308d64c739d2e237478d648f8803f6bb64ac2844586f8aac3c98acdc25b6b38ff4400',33,3,'Personal Access Token','[]',0,'2020-01-31 15:20:58','2020-01-31 15:20:58','2021-01-31 15:20:58'),
('ebe2f9eb2298906e1c7b9ea2e1bee9ff1809525470b9c99d729e34010f350c67a0c9f3525c17bafd',31,1,'Personal Access Token','[]',0,'2020-01-15 16:06:28','2020-01-15 16:06:28','2021-01-15 16:06:28'),
('ee0cb751016df3c2e162c9a5993a1ff49874a7fdae03b6a21f7075415a39e381783ec466924c7760',31,1,'Personal Access Token','[]',0,'2020-01-15 16:28:13','2020-01-15 16:28:13','2021-01-15 16:28:13'),
('efe661c7ef803cfab6cfa6b41d13668ab0308a491760a94d21a78f39f54cb1c26f504046269cb281',31,1,'Personal Access Token','[]',0,'2020-01-15 16:24:24','2020-01-15 16:24:24','2021-01-15 16:24:24'),
('f0184286b0343820ab1ff7d5edb4f3ed82a9d023fddbe8b427f0db678256fe2cbe593f39c05df2ca',28,1,'Personal Access Token','[]',0,'2020-01-13 15:23:44','2020-01-13 15:23:44','2021-01-13 15:23:44'),
('f1b542ccd389adab5a46f46baa14d558114fd74b89689bc6747fae88a50b38da8b1bd17ce66ec3ef',33,3,'Personal Access Token','[]',0,'2020-02-03 17:03:09','2020-02-03 17:03:09','2021-02-03 17:03:09'),
('f1be6a32bb237ae19d1398f685723dfe91ee44f7a4d75f5c317805213906276345afa9aefe54764b',33,3,'Personal Access Token','[]',0,'2020-01-28 12:27:01','2020-01-28 12:27:01','2021-01-28 12:27:01'),
('f1fb849ee76dbf8b224532b7d1b256b99c49aa66b5e4280c2a47a3d4cb5c211f0329aebc684b1ffd',28,1,'Personal Access Token','[]',0,'2020-01-13 17:01:41','2020-01-13 17:01:41','2021-01-13 17:01:41'),
('f2e010abdbd5ef92d05c9fbad7b0ce0d2b51c710073a0f2ed31cf52b76670e2cd7229d3af2edb002',28,1,'Personal Access Token','[]',0,'2020-01-13 16:33:59','2020-01-13 16:33:59','2021-01-13 16:33:59'),
('f4464125cf5b38074def2c54c648c9df0ff4418084b971d63a89240ebd1b1dc3cc2c8e170e860642',28,1,'Personal Access Token','[]',0,'2020-01-14 13:58:57','2020-01-14 13:58:57','2021-01-14 13:58:57'),
('f4d69a5d1aa85d783fe3b0319b583d97c8aa27226650715dec26027dc3032d971796527aaf5af5d1',28,1,'Personal Access Token','[]',0,'2020-01-13 15:37:50','2020-01-13 15:37:50','2021-01-13 15:37:50'),
('f6b19f0044fdd7d3930cd1b8c2c872d35d6daa8d7da0e0aa04a0fd72fe1c5c6d874a65fd6fd28acb',33,3,'Personal Access Token','[]',0,'2020-01-31 15:24:02','2020-01-31 15:24:02','2021-01-31 15:24:02'),
('f6fac73a3a598507ba96072e0fd94574bed25081d38e2001d51b2a1ac47fcc8f6b5ad78f8cfe37a1',28,1,'Personal Access Token','[]',0,'2020-01-13 12:14:08','2020-01-13 12:14:08','2021-01-13 12:14:08'),
('f70f839a6e10b21bf7354c653b3b914f0e3bc59a9b90683cb152c4103f5e1984cc46628441590a8d',31,1,'Personal Access Token','[]',0,'2020-01-15 16:35:22','2020-01-15 16:35:22','2021-01-15 16:35:22'),
('f91317b779f99a99be3517e7dc536f5116f8ec753d4ff7a95a2d81549dea0adb334aa020b3461a4d',31,1,'Personal Access Token','[]',0,'2020-01-15 15:23:08','2020-01-15 15:23:08','2021-01-15 15:23:08'),
('fa8a6df42578d855d6dfb427a656071a7dcd0269e749dd96c439510bb7f3ed2accc7a5aeeffbcd95',28,1,'Personal Access Token','[]',0,'2020-01-14 15:34:24','2020-01-14 15:34:24','2021-01-14 15:34:24'),
('fbe5726c22463266278d513a4edff7825d6b629d2466e4ccd4096a819ce131e2a7f08d3f7b7d2a3f',33,3,'Personal Access Token','[]',0,'2020-01-28 12:17:48','2020-01-28 12:17:48','2021-01-28 12:17:48'),
('fd3d46b4bb78c43ff893c3ce0b600457a4647befc9bf3b98af64e5705e5240d9f28f2f2d61b58acd',28,1,'Personal Access Token','[]',0,'2020-01-15 11:45:00','2020-01-15 11:45:00','2021-01-15 11:45:00'),
('fd634235253b32f18dce517b99133d01e6b212b67bace40d70f8816bed53d6c258216f8f4d33ba61',33,3,'Personal Access Token','[]',0,'2020-02-04 14:25:56','2020-02-04 14:25:56','2021-02-04 14:25:56'),
('fd9c591732a3fc8c54f4d8420f25bdd1da3df7e177f28b42c71e30f6902cc111de59a711b4962f50',31,1,'Personal Access Token','[]',0,'2020-01-15 15:55:50','2020-01-15 15:55:50','2021-01-15 15:55:50'),
('feb4795812b1c5e35614745941924041ffff3ad99b50bbb4aaf45804d3003e3564f898920a9b614c',28,1,'Personal Access Token','[]',0,'2020-01-13 15:51:07','2020-01-13 15:51:07','2021-01-13 15:51:07'),
('fef21de5ce7aa6c6c6e063196893b6b6c4f134eb7521da10e2875dca19d136045b7ab66124c1c787',33,3,'Personal Access Token','[]',0,'2020-01-30 12:52:17','2020-01-30 12:52:17','2021-01-30 12:52:17'),
('ff49873507e82c72a4b846924576b289f582cc8195d4fd22593d5799e1e1355c8bcd7200ce039dcc',33,3,'Personal Access Token','[]',0,'2020-02-03 17:18:51','2020-02-03 17:18:51','2021-02-03 17:18:51'),
('ff74a072d863a2e9523cf4f6d707bb0ecde07477d0925da7a3330faca750e366788a6d33310c84a9',33,3,'Personal Access Token','[]',0,'2020-02-04 15:06:48','2020-02-04 15:06:48','2021-02-04 15:06:48');

/*Table structure for table `oauth_auth_codes` */

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`id`,`user_id`,`name`,`secret`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) values 
(1,NULL,'Laravel Personal Access Client','9sUnY5TO66owKdbIV78jiwfpF4BDVHFNYvR83ajR','http://localhost',1,0,0,'2020-01-03 21:40:09','2020-01-03 21:40:09'),
(2,NULL,'Laravel Password Grant Client','hqWnDwyk0IULkUTHvPMDbwpAmgHJuhtfO4RNnecc','http://localhost',0,1,0,'2020-01-03 21:40:10','2020-01-03 21:40:10'),
(3,NULL,'Laravel Personal Access Client','pLxercgL8SJNgFE8mPUJly5NTDuB20xQR3LUVc25','http://localhost',1,0,0,'2020-01-27 12:47:31','2020-01-27 12:47:31'),
(4,NULL,'Laravel Password Grant Client','ZJJQc2qbcVqYv4QfDqHN3BRpmKRcDHTj8SR9cBBO','http://localhost',0,1,0,'2020-01-27 12:47:32','2020-01-27 12:47:32');

/*Table structure for table `oauth_personal_access_clients` */

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

insert  into `oauth_personal_access_clients`(`id`,`client_id`,`created_at`,`updated_at`) values 
(1,1,'2020-01-03 21:40:10','2020-01-03 21:40:10'),
(2,3,'2020-01-27 12:47:32','2020-01-27 12:47:32');

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `prefectures` */

DROP TABLE IF EXISTS `prefectures`;

CREATE TABLE `prefectures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `prefecture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `prefectures` */

insert  into `prefectures`(`id`,`prefecture`,`latitude`,`longitude`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Aomori','9.9312328','76.26730409999999',NULL,NULL,NULL),
(2,'Iwate','9.9312328','76.26730409999999',NULL,NULL,NULL),
(3,'Miyagi','9.9312328','76.26730409999999',NULL,NULL,NULL),
(4,'Akita','9.9312328','76.26730409999999',NULL,NULL,NULL),
(5,'Yamagata','9.9312328','76.26730409999999',NULL,NULL,NULL),
(6,'Fukushima','9.9312328','76.26730409999999',NULL,NULL,NULL),
(7,'Ibaraki','9.9312328','76.26730409999999',NULL,NULL,NULL),
(8,'Tochigi','9.9312328','76.26730409999999',NULL,NULL,NULL),
(9,'Gunma','9.9312328','76.26730409999999',NULL,NULL,NULL),
(10,'Saitama','9.9312328','76.26730409999999',NULL,NULL,NULL),
(12,'kochi','9.9312328','76.26730409999999','2020-01-13 15:52:45','2020-01-13 23:58:34',NULL),
(13,'test','9.9312328','76.26730409999999','2020-01-14 00:01:06','2020-01-14 00:01:15','2020-01-14 00:01:15'),
(14,'test','9.9312328','76.26730409999999','2020-01-14 05:00:17','2020-01-14 05:00:39','2020-01-14 05:00:39');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role` enum('Super Admin','Admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Super Admin','2019-12-27 12:42:17',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_expire` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`role_id`,`email`,`phone`,`location`,`company_name`,`website`,`email_verified_at`,`password`,`image`,`otp`,`otp_expire`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Akhil','P P',1,'info@farmatch.com','8891939751','Cochin.','Iroid Technologies','http://iroidtechnologies.in',NULL,'$2y$12$iS4nzwPaHU9PR/F1Y5t2E.mP6413ph0Y8nxDAc3esiOBY.Vfw0XDu','1077218111.JPG','','2019-12-30 08:39:06',NULL,'2019-12-27 12:52:02','2019-12-31 10:02:09',NULL);

/*Table structure for table `work_place_reviews` */

DROP TABLE IF EXISTS `work_place_reviews`;

CREATE TABLE `work_place_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chemist_id` bigint(10) unsigned NOT NULL,
  `atmosphere` int(11) NOT NULL,
  `clerk_adaption` int(11) NOT NULL,
  `pharmacist_adaption` int(11) NOT NULL,
  `compliance_level` int(11) NOT NULL,
  `others` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chemist_id` (`chemist_id`),
  CONSTRAINT `work_place_reviews_ibfk_1` FOREIGN KEY (`chemist_id`) REFERENCES `chemists` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `work_place_reviews` */

insert  into `work_place_reviews`(`id`,`chemist_id`,`atmosphere`,`clerk_adaption`,`pharmacist_adaption`,`compliance_level`,`others`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,4,4,3,4,'test',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
