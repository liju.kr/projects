<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGroupChatHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_chats', function (Blueprint $table) {
            $table->boolean('admin_read_status')->default(false);
            $table->boolean('chemist_read_status')->default(false);
            $table->boolean('store_read_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_chats', function (Blueprint $table) {
            $table->dropColumn('admin_read_status');
            $table->dropColumn('chemist_read_status');
            $table->dropColumn('store_read_status');
        });
    }
}
