<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminChemistChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_chemist_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_chemist_chat_head_id');
            $table->boolean('admin')->default(0);
            $table->string('message');
            $table->timestamps();
            $table->index('admin_chemist_chat_head_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_chemist_chats');
    }
}
