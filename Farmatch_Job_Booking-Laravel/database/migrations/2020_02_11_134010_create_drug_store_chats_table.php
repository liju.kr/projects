<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugStoreChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_store_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_chat_head_id');
            $table->boolean('admin')->default(0);
            $table->string('message');
            $table->timestamps();
            $table->index('drug_store_chat_head_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_store_chats');
    }
}
