<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmployeeIntroServiceFirstToDrugStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_stores', function (Blueprint $table) {
            $table->integer('employee_intro_service_first')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_stores', function (Blueprint $table) {
            $table->dropColumn('employee_intro_service_first');
        });
    }
}
