<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStoreChemistChatHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_chemist_chats', function (Blueprint $table) {
            $table->boolean('chemist_read_status')->default(false);
            $table->boolean('store_read_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_chemist_chats', function (Blueprint $table) {
            $table->dropColumn('chemist_read_status');
            $table->dropColumn('store_read_status');
        });
    }
}
