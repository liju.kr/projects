<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChemistChatHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemist_chat_heads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->unsignedBigInteger('chemist_id');
            $table->longText('last_message');
            $table->timestamps();
            $table->index('drug_store_id');
            $table->index('chemist_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemist_chat_heads');
    }
}
