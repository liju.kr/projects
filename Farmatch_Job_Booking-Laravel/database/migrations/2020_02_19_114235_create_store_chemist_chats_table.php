<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreChemistChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_chemist_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('store_chemist_chat_head_id');
            $table->boolean('user')->default(0);
            $table->string('message');
            $table->timestamps();
            $table->index('store_chemist_chat_head_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_chemist_chats');
    }
}
