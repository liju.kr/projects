<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotificationCount2ToChemistJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            $table->integer('notification_count_2')->after('notification_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            $table->dropColumn('notification_count_2');
        });
    }
}
