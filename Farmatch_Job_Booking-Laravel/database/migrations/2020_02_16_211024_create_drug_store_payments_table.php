<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugStorePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_store_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('amount');
            $table->enum('status', array('Hold', 'Paid'));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_store_payments');
    }
}
