<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChemistReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemist_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('chemist_id');
            $table->foreign('chemist_id')->references('id')->on('chemists');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');
            $table->integer('attitude');
            $table->integer('adapting');
            $table->integer('work_speed');
            $table->integer('value_for_money');
            $table->text('others');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemist_reviews');
    }
}
