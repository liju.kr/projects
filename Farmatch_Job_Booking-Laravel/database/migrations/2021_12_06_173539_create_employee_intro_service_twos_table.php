<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeIntroServiceTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_intro_service_twos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');

            $table->text('working_hours_1_from')->nullable();
            $table->text('working_hours_1_to')->nullable();
            $table->text('working_hours_2_from')->nullable();
            $table->text('working_hours_2_to')->nullable();
            $table->text('working_hours_3_from')->nullable();
            $table->text('working_hours_3_to')->nullable();
            $table->text('working_hours_or_from')->nullable();
            $table->text('working_hours_or_to')->nullable();
            $table->text('working_hours_between')->nullable();
            $table->text('working_hours_notes')->nullable();
            $table->text('overtime_work_hours')->nullable();
            $table->text('overtime_work_hours_details')->nullable();
            $table->text('special_provisions')->nullable();
            $table->text('special_provisions_details')->nullable();
            $table->integer('time_break')->nullable();
            $table->integer('annual_holidays')->nullable();
            $table->text('holidays_etc')->nullable();
            $table->integer('annual_paid_vacation_days_after_6_months')->nullable();
            $table->text('insurance_coverage')->nullable();
            $table->text('insurance_coverage_details')->nullable();
            $table->text('retirement_benefits')->nullable();
            $table->text('retirement_system')->nullable();
            $table->text('annuities_corporate')->nullable();
            $table->text('retirement_age')->nullable();
            $table->text('reemployment_system')->nullable();
            $table->text('duty_extension')->nullable();
            $table->text('retirement_age_details')->nullable();
            $table->text('reemployment_system_details')->nullable();
            $table->text('duty_extension_details')->nullable();
            $table->text('housing_for_singles')->nullable();
            $table->text('housing_for_households')->nullable();
            $table->text('housing_details')->nullable();
            $table->text('childcare_facilities')->nullable();
            $table->text('childcare_facilities_details')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_intro_service_twos');
    }
}
