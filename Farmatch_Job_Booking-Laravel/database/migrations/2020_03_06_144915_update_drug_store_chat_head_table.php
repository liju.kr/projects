<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDrugStoreChatHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_store_chats', function (Blueprint $table) {
            $table->boolean('admin_read_status')->default(false);
            $table->boolean('store_read_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_store_chats', function (Blueprint $table) {
            $table->dropColumn('admin_read_status');
            $table->dropColumn('store_read_status');
        });
    }
}
