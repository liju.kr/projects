<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChemistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('gender', array('Male', 'Female'));
            $table->string('post_code');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('licence_number')->unique();
            $table->string('registration_year');
            $table->string('registration_month');
            $table->string('image');
            $table->string('password');
            $table->string('device_token')->nullable();
            $table->integer('hourly_wage')->nullable();
            $table->enum('status', array('Approved', 'Pending', 'Suspended'));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemists');
    }
}
