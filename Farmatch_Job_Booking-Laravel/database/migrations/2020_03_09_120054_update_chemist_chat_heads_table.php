<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateChemistChatHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chemist_chats', function (Blueprint $table) {
            $table->boolean('admin_read_status')->default(false);
            $table->boolean('chemist_read_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chemist_chats', function (Blueprint $table) {
            $table->dropColumn('admin_read_status');
            $table->dropColumn('chemist_read_status');
        });
    }
}
