<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBanDetailsToDrugStorePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_store_payments', function (Blueprint $table) {

            $table->enum('bank_details', array('Default', 'Non Default'))->default('Default')->after('reduce_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_store_payments', function (Blueprint $table) {
            //
        });
    }
}
