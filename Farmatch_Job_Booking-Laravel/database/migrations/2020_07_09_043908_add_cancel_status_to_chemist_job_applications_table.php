<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCancelStatusToChemistJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            $table->enum('chemist_cancelled', array('Yes', 'No'))->default('No')->after('is_reviewed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            //
        });
    }
}
