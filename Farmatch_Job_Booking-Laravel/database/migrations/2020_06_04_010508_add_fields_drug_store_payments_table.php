<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsDrugStorePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_store_payments', function (Blueprint $table) {

            $table->integer('input_quantity')->after('remarks');
            $table->double('input_unit_price', 8, 2)->after('input_quantity');
            $table->double('total_input', 8, 2)->after('input_unit_price');
            $table->string('input_text')->after('total_input');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_store_payments', function (Blueprint $table) {
            //

        });
    }
}
