<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('history_model');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('in_charge');
            $table->text('introduction');
            $table->text('reason_need_chemist');
            $table->string('main_image');
            $table->string('sub_image1');
            $table->string('sub_image2');
            $table->string('latitude');
            $table->string('longitude');
            $table->enum('status', array('Approved', 'Pending', 'Suspended'));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_stores');
    }
}
