<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeIntroServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_intro_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');

            $table->date('date')->nullable();
            $table->text('furigana')->nullable();
            $table->text('office_name_furigana')->nullable();
            $table->text('office_location_code')->nullable();
            $table->text('office_location')->nullable();
            $table->text('occupation')->nullable();
            $table->text('job_description')->nullable();
            $table->text('employment_status')->nullable();
            $table->text('appointment_of_full_time_employee')->nullable();
            $table->text('appointment_of_full_time_employee_description')->nullable();
            $table->text('working_style')->nullable();
            $table->text('worker_dispatch_business_permit_number')->nullable();
            $table->text('employment_period')->nullable();
            $table->text('contract_renewal_conditions')->nullable();
            $table->text('work_place_code')->nullable();
            $table->text('work_place')->nullable();
            $table->text('my_car_commute')->nullable();
            $table->text('parking')->nullable();
            $table->text('possibility_of_transfer')->nullable();
            $table->text('possibility_of_transfer_details')->nullable();
            $table->text('age_promotion')->nullable();
            $table->text('age_limit_reason')->nullable();
            $table->text('age_limit_reason_details')->nullable();
            $table->text('academic_background')->nullable();
            $table->text('academic_background_details')->nullable();
            $table->text('knowledge_skills')->nullable();
            $table->text('knowledge_skills_details')->nullable();
            $table->text('pc_skills')->nullable();
            $table->text('pc_skills_details')->nullable();
            $table->text('pharmacist_license')->nullable();
            $table->text('ordinary_car_drivers_license')->nullable();
            $table->text('trial_period')->nullable();
            $table->text('trial_period_details')->nullable();
            $table->integer('monthly_amount_a')->nullable();
            $table->integer('monthly_amount_b')->nullable();
            $table->integer('average_working_days_per_month')->nullable();
            $table->integer('basic_pay_a')->nullable();
            $table->integer('basic_pay_b')->nullable();
            $table->text('fixed_amount_1')->nullable();
            $table->integer('fixed_amount_1_field_1')->nullable();
            $table->integer('fixed_amount_1_field_2')->nullable();
            $table->text('fixed_amount_2')->nullable();
            $table->integer('fixed_amount_2_field_1')->nullable();
            $table->integer('fixed_amount_2_field_2')->nullable();
            $table->text('fixed_amount_3')->nullable();
            $table->integer('fixed_amount_3_field_1')->nullable();
            $table->integer('fixed_amount_3_field_2')->nullable();
            $table->text('fixed_amount_4')->nullable();
            $table->integer('fixed_amount_4_field_1')->nullable();
            $table->integer('fixed_amount_4_field_2')->nullable();
            $table->text('fixed_amount_5')->nullable();
            $table->integer('fixed_amount_5_field_1')->nullable();
            $table->integer('fixed_amount_5_field_2')->nullable();
            $table->text('overtime_pay')->nullable();
            $table->integer('overtime_pay_1')->nullable();
            $table->integer('overtime_pay_2')->nullable();
            $table->text('overtime_pay_note')->nullable();
            $table->text('articles_with_allowance')->nullable();
            $table->text('other_contents')->nullable();
            $table->text('actual_expense_payment')->nullable();
            $table->integer('actual_expense_payment_monthly')->nullable();
            $table->text('wages')->nullable();
            $table->integer('wages_details')->nullable();
            $table->text('wages_notes')->nullable();
            $table->text('payment_date')->nullable();
            $table->text('payment_date_details')->nullable();
            $table->text('payment_date_notes')->nullable();
            $table->text('salary_raise')->nullable();
            $table->text('salary_raise_details')->nullable();
            $table->text('bonus')->nullable();
            $table->text('bonus_details')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_intro_services');
    }
}
