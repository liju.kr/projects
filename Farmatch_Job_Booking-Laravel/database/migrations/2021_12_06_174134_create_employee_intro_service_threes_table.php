<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeIntroServiceThreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_intro_service_threes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');

            $table->integer('number_of_employees')->nullable();
            $table->text('japanese_era')->nullable();
            $table->text('full_name')->nullable();
            $table->text('work_place_name')->nullable();
            $table->text('capital')->nullable();
            $table->text('of_which_female')->nullable();
            $table->text('of_which_part')->nullable();
            $table->text('union')->nullable();
            $table->text('business_content')->nullable();
            $table->text('features_of_the_company')->nullable();
            $table->text('position_name')->nullable();
            $table->text('representative_name')->nullable();
            $table->text('corporate_number')->nullable();
            $table->text('full_time')->nullable();
            $table->text('part_time')->nullable();
            $table->text('achievements_of_taking_childcare_leave')->nullable();
            $table->text('nursing_care_leave_acquisition_record')->nullable();
            $table->text('nursing_leave_acquisition_record')->nullable();
            $table->text('foreign_employment_record')->nullable();
            $table->text('special_notes_on_job_vacancies')->nullable();
            $table->integer('number_of_people_recruitment')->nullable();
            $table->text('reason_for_recruitment')->nullable();
            $table->text('methodology_elective_exam')->nullable();
            $table->text('schedule')->nullable();
            $table->text('result_announcement')->nullable();
            $table->integer('after_the_book')->nullable();
            $table->integer('after_the_face')->nullable();
            $table->text('method_notification')->nullable();
            $table->text('as_needed')->nullable();
            $table->text('selection_place_code')->nullable();
            $table->text('selection_place')->nullable();
            $table->text('string_1')->nullable();
            $table->text('string_2')->nullable();
            $table->text('on_foot')->nullable();
            $table->text('documents')->nullable();
            $table->text('furigana_name')->nullable();
            $table->text('name_furigana')->nullable();
            $table->text('telephone_number')->nullable();
            $table->text('fax')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_intro_service_threes');
    }
}
