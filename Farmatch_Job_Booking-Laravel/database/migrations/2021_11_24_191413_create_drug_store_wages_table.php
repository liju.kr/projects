<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugStoreWagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_store_wages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('chemist_id');
            $table->foreign('chemist_id')->references('id')->on('chemists');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');
            $table->integer('hourly_wage')->nullable();
            $table->integer('transportation_cost')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_store_wages');
    }
}
