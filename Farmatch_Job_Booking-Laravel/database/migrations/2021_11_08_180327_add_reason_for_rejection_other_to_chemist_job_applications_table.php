<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReasonForRejectionOtherToChemistJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            $table->text('reason_for_rejection_other')->after('reason_for_rejection')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chemist_job_applications', function (Blueprint $table) {
            $table->dropColumn('reason_for_rejection_other');
        });
    }
}
