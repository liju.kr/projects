<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToDrugStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_stores', function (Blueprint $table) {
            //
            $table->integer('normal_farmatch')->default(0);
            $table->integer('normal_farmatch_disable')->default(0);
            $table->integer('employee_intro_service')->default(0);
            $table->integer('employee_intro_automatic')->default(0);
            $table->integer('employee_intro_renewed')->default(0);
            $table->date('employee_intro_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_stores', function (Blueprint $table) {
            $table->dropColumn('normal_farmatch');
            $table->dropColumn('employee_intro_service');
            $table->dropColumn('employee_intro_automatic');
            $table->dropColumn('employee_intro_date');
        });
    }
}
