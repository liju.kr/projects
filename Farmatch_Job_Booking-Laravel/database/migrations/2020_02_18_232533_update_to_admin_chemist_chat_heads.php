<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToAdminChemistChatHeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_chemist_chat_heads', function (Blueprint $table) {
            $table->longText('last_message')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_chemist_chat_heads', function (Blueprint $table) {
            $table->longText('last_message')->nullable(false)->change();
        });
    }
}
