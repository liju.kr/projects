<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChemistJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemist_job_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('chemist_id');
            $table->foreign('chemist_id')->references('id')->on('chemists');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');
            $table->unsignedBigInteger('job_post_id');
            $table->foreign('job_post_id')->references('id')->on('job_posts');
            $table->enum('day', array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'));
            $table->time('time_from');
            $table->time('time_to');
            $table->integer('rest_time');
            $table->integer('transportation_cost');
            $table->enum('status', array('Pending', 'Accepted', 'Rejected'));
            $table->text('reason_for_rejection')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemist_job_applications');
    }
}
