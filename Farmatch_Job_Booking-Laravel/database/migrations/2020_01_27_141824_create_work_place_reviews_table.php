<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkPlaceReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_place_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drug_store_id');
            $table->foreign('drug_store_id')->references('id')->on('drug_stores');
            $table->integer('atmosphere');
            $table->integer('clerk_adaption');
            $table->integer('pharmacist_adaption');
            $table->integer('compliance_level');
            $table->text('others');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_place_reviews');
    }
}
