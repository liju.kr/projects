<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class DrugStoreTiming extends Model
{
    //
    use SoftDeletes;
}
