<?php

namespace App;

use App\Http\Middleware\Authenticate;
use Laravel\Passport\HasApiTokens;
use \Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Foundation\Auth\Chemist as Authenticatable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Chemist extends Authenticatable
{
    //
    use HasApiTokens, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'gender', 'post_code', 'phone', 'email', 'password', 'licence_number', 'registration_year',
        'registration_month', 'phone_otp', 'phone_otp_expire', 'insurance_number'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return "{$this->last_name} {$this->first_name}";
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function education()
    {
        return $this->hasMany('App\ChemistEducation')->where('year', '!=', "")->where('content', '!=', "")->where('year', '!=', null)->where('content', '!=', null)->orderBy('year')->where('deleted_at', null)->orderBy('month');
    }

    public function qualification()
    {
        return $this->hasMany('App\ChemistQualification')->where('year', '!=', "")->where('content', '!=', "")->where('year', '!=', null)->where('content', '!=', null)->orderBy('year')->orderBy('month');
    }

    public function prefecture()
    {
        return $this->belongsTo('App\Prefecture', 'prefecture_id', 'id');
    }
}
