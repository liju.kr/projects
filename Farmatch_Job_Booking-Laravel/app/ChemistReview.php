<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ChemistReview extends Model
{
    //
    use SoftDeletes;
}
