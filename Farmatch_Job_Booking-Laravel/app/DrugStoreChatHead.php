<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugStoreChatHead extends Model
{
    public function user()
    {
        return $this->hasOne('App\DrugStore','id','drug_store_id');
    }

    public function chats()
    {
        return $this->hasMany('App\DrugStoreChat', 'drug_store_chat_head_id', 'id');
    }
}
