<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ChemistEducation extends Model
{
    //
    use SoftDeletes;

    protected $table = 'chemist_educations';
}
