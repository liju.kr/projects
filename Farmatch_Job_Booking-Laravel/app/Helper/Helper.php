<?php

namespace App\Helper;

use App\AdminAction;

class Helper
{
    public function addAction($userId, $receiverId, $receiver, $message, $icon){

        $action              = new AdminAction();
        $action->user_id     = $userId;
        $action->receiver    = $receiver;
        $action->receiver_id = $receiverId;
        $action->message     = $message;
        $action->icon        = $icon;
        return $action->save();
    }
    public static function instance() {

         return new Helper();
     }

}
