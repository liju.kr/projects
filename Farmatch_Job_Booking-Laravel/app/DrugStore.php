<?php

namespace App;

use \Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Foundation\Auth\DrugStore as Authenticatable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DrugStore extends Authenticatable
{
    //
    use SoftDeletes;

    protected $fillable = [
        'name', 'in_charge', 'phone', 'email', 'licence_number', 'password', 'latitude', 'longitude', 'phone_otp', 'phone_otp_expire'
    ];
}
