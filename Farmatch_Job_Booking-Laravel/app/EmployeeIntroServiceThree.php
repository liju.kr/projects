<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeIntroServiceThree extends Model
{
    use SoftDeletes;
}
