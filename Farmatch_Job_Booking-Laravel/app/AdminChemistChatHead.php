<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AdminChemistChatHead extends Model
{
    public function chemist()
    {
        return $this->hasOne('App\Chemist', 'id', 'chemist_id');
    }

    public function chats()
    {
        return $this->hasMany('App\AdminChemistChat', 'admin_chemist_chat_head_id', 'id');
    }
}
