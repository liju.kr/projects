<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class DrugStorePayment extends Model
{
    //
    use SoftDeletes;
}
