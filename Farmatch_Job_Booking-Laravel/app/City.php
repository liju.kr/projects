<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'prefecture_id', 'city', 'latitude', 'longitude'
    ];
}
