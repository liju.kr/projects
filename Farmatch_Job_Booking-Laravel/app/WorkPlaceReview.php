<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class WorkPlaceReview extends Model
{
    //
    use SoftDeletes;
}
