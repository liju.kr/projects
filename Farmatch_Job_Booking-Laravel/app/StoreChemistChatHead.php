<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StoreChemistChatHead extends Model
{
    public function chemist()
    {
        return $this->hasOne('App\Chemist', 'id', 'chemist_id');
    }

    public function store()
    {
        return $this->hasOne('App\DrugStore', 'id', 'drug_store_id');
    }

    public function chats()
    {
        return $this->hasMany('App\StoreChemistChat', 'store_chemist_chat_head_id', 'id');
    }

    
}
