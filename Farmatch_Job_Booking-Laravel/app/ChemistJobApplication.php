<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class ChemistJobApplication extends Model
{
    //
    use SoftDeletes;
    public function chemistDetails() {
        return $this->hasOne('App\Chemist', 'id', 'chemist_id');
    }
}
