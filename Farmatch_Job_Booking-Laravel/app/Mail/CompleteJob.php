<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CompleteJob extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.CompleteJob')
            ->from('support@farmatch.work', 'ふぁーまっち | 勤務内容確認のお願い')
            ->subject('【早急】 ' . $this->data['working_day'] . ' の勤務内容の確認を完了してください')
            ->with($this->data);
    }
}
