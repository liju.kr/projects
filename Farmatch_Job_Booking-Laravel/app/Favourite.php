<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Favourite extends Model
{
    //
    use SoftDeletes;
    public function drug_store() {
        return $this->hasOne(DrugStore::class, 'id', 'drug_store_id');
    }
}
