<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminAction extends Model
{
    //

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function drugStore(){

        return $this->belongsTo('App\DrugStore', 'receiver_id', 'id');
    }

    public function chemist(){

        return $this->belongsTo('App\Chemist', 'receiver_id', 'id');
    }
}
