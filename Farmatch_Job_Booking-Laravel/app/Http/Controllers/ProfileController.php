<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Faq;
use App\User;
use App\City;
use App\Prefecture;

class ProfileController extends Controller
{
    public function index(){

        $profile = User::find(Auth::id());
        return view('profile')->with('profile', $profile)
                              ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                              ->with('profileActive', 'kt-menu__item--active')
                              ->with('page_title', 'マイプロフィール');
    }

    public function changePassword(){

        $profile = User::find(Auth::id());
        return view('change_password')->with('profile', $profile)
                                      ->with('page_title', 'Change Password');
    }

    public function resetPassword(Request $request){

        if(Hash::check($request->currentPassword, Auth::user()->password)){
            User::where('id', Auth::user()->id)
                ->update(['password' =>  Hash::make($request->password),'updated_at' => Carbon::now()]);
            return response()->json(['message' => 'パスワードを再設定しました', 'status' => TRUE]);
        }else{
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function updateProfile(Request $request){

        $profile    =   User::find(Auth::id());
        $validator  =   Validator::make($request->all(),[
            'first_name'        => 'required',
            'last_name'         => 'required',
            'company_name'      => 'required',
            'location'          => 'required',
            'phone'             => 'required|unique:users,phone,'.$profile->id.',id,deleted_at,NULL',
            'email'             => 'required|unique:users,email,'.$profile->id.',id,deleted_at,NULL',
        ], [
            'phone.unique'       => 'この番号はすでに使用されています',
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                if($request->file('image')){
                    $validator  = Validator::make($request->all(),[
                                        'image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('image');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/profile/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$profile->image);
                    }
                    $profile->image   = $newImage;
                }
                $profile->first_name   = request('first_name');
                $profile->last_name    = request('last_name');
                $profile->company_name = request('company_name');
                $profile->location     = request('location');
                $profile->phone        = request('phone');
                $profile->email        = request('email');
                $profile->website      = request('website');
                $profile->save();
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function getPrefectures(){

        $prefectures  = Prefecture::select('id', 'prefecture')->orderBy('id', 'DESC')->get();
        return view('prefectures') ->with('prefectures', $prefectures)
                                   ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                                   ->with('prefectureActive', 'kt-menu__item--active')
                                   ->with('page_title', '都道府県の選択肢を管理する');
    }

    public function addPrefecture(Request $request){

        $validator  =   Validator::make($request->all(),[
            'prefecture'    => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $prefecture             = new Prefecture();
                $prefecture->prefecture = $request->prefecture;
                $prefecture->latitude   = $request->latitude;
                $prefecture->longitude  = $request->longitude;
                $prefecture->save();
                return response()->json(['message' => '追加しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function getCities(){

        $prefectures = Prefecture::select('id', 'prefecture')->get();
        $cities      = City::select('id', 'prefecture_id', 'city')->orderBy('id', 'DESC')->get();
        foreach($cities as $row){
            $row->prefecture = Prefecture::where('id', $row->prefecture_id)->first()->prefecture;
        }
        return view('cities')->with('prefectures', $prefectures)
                             ->with('cities', $cities)
                             ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                             ->with('cityActive', 'kt-menu__item--active')
                             ->with('page_title', '都市範囲を管理する');
    }

    public function addCity(Request $request){

        $validator  =   Validator::make($request->all(),[
            'prefecture'    => 'required',
            'city'          => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $prefecture                 = new City();
                $prefecture->prefecture_id  = $request->prefecture;
                $prefecture->city           = $request->city;
                $prefecture->latitude       = $request->latitude;
                $prefecture->longitude      = $request->longitude;
                $prefecture->save();
                return response()->json(['message' => '追加しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function deleteCity(Request $request){

        $validator  =   Validator::make($request->all(),[
            'cityId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $deletedRows = City::where('id', $request->cityId)->delete();
                if($deletedRows){
                    return response()->json(['message' => '削除しました', 'status' => TRUE]);
                }else{
                    return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function editCity(Request $request){

        try{
            $city  = City::select('id', 'prefecture_id', 'city', 'latitude', 'longitude')->where('id', $request->cityId)->first();
            return response()->json(['message' => $city, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function editCityAction(Request $request){

        $validator  =   Validator::make($request->all(),[
            'city_id'       => 'required',
            'prefecture'    => 'required',
            'city'          => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                City::where('id', $request->city_id)
                     ->update(['prefecture_id' =>  $request->prefecture,
                               'city'          => $request->city,
                               'latitude'      => $request->latitude,
                               'longitude'     => $request->longitude]);
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function deletePrefecture(Request $request){

        $validator  =   Validator::make($request->all(),[
            'prefectureId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $deletedRows = Prefecture::where('id', $request->prefectureId)->delete();
                City::where('prefecture_id', $request->prefectureId)->delete();
                if($deletedRows){
                    return response()->json(['message' => '削除しました', 'status' => TRUE]);
                }else{
                    return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function editPrefecture(Request $request){

        try{
            $prefecture  = Prefecture::select('id', 'prefecture', 'latitude', 'longitude')->where('id', $request->prefectureId)->first();
            return response()->json(['message' => $prefecture, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function editPrefectureAction(Request $request){

        $validator  =   Validator::make($request->all(),[
            'prefecture_id' => 'required',
            'prefecture'    => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                Prefecture::where('id', $request->prefecture_id)
                     ->update(['prefecture'    => $request->prefecture,
                               'latitude'      => $request->latitude,
                               'longitude'     => $request->longitude]);
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function faq(){

        $faqs = Faq::select('id', 'question', 'answer', 'type')->orderBy('id', 'DESC')->get();
        return view('faq')  ->with('faqs', $faqs)
                            ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                            ->with('faqActive', 'kt-menu__item--active')
                            ->with('page_title', 'よくある質問を管理する');

    }

    public function addFaq(Request $request){

        $validator  =   Validator::make($request->all(),[
            'question'    => 'required',
            'answer'      => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $faq             = new Faq();
                $faq->type       = $request->type;
                $faq->question   = $request->question;
                $faq->answer     = $request->answer;
                $faq->save();
                return response()->json(['message' => '追加しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function deleteFaq(Request $request){

        $validator  =   Validator::make($request->all(),[
            'faqId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $deletedRows = Faq::where('id', $request->faqId)->delete();
                if($deletedRows){
                    return response()->json(['message' => '削除しました', 'status' => TRUE]);
                }else{
                    return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }

    }

    public function editFaq(Request $request){

        try{
            $faq  = Faq::select('id', 'question', 'answer', 'type')->where('id', $request->faqId)->first();
            return response()->json(['message' => $faq, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function editFaqAction(Request $request){

        $validator  =   Validator::make($request->all(),[
            'faq_id'    => 'required',
            'question'  => 'required',
            'answer'    => 'required',
            'type'      => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                Faq::where('id', $request->faq_id)
                     ->update(['type'        => $request->type,
                               'question'    => $request->question,
                               'answer'      => $request->answer]);
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }
}
