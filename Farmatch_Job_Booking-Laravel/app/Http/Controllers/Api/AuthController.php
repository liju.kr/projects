<?php
namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use App\Mail\SendChemistOtp;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;

use App\City;
use App\Chemist;
Use App\Prefecture;
use App\ChemistEducation;
use App\ChemistQualification;

class AuthController extends Controller
{



    public function appVersion(Request $request) {

            return response()->json([ 'android' => '2.2.4', 'ios' => '2.7.0', 'force_update' => true]);
    }

    public function checkValid(Request $request) {

        if(Auth::guard('api')->check())
        {
            return response()->json([ 'valid' => true, 'message' => 'Authenticated']);
        }
        else
        {
            return response()->json([ 'valid' => false, 'message' => 'Not Authenticated']);
        }

    }


    public function signup(Request $request) {

        $validator  =   Validator::make($request->all(),[
                'first_name'         => 'required',
                'last_name'          => 'required',
                'gender'             => 'required',
                'post_code'          => 'required',
                'phone'              => 'required|unique:chemists,phone,Not Verified,status,deleted_at,NULL',
                'email'              => 'required|email|unique:chemists,email,Not Verified,status,deleted_at,NULL',
                'password'           => 'required',
                'licence_number'     => 'required',
                'registration_year'  => 'required',
                'registration_month' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => 'すべての必須項目に入力が必要です' ]);
        }else{
            try{
                $attributes                     = $request->all();
                $attributes['password']         = Hash::make(request('password'));
                $digits                         = 4;
                $attributes['phone_otp']        = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $attributes['phone_otp_expire'] = Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
                $insertData                     = Chemist::create($attributes);
                $data   = [ 'otp' => $attributes['phone_otp'] ];
                $client = new Client();
                $res    = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number='.$request->phone.'&otp='.$attributes['phone_otp'] , [
                ]);
                $res->getStatusCode();
                $res->getHeader('content-type');
                $res->getBody();
                Mail::to($request->email)->send(new SendChemistOtp($data));
                return response()->json([ 'status' => 'success', 'chemist_id' => $insertData->id, 'message' => '確認コードが送信されました' ]);
            } catch(\Exception $e){
                // print_r($e->getMessage());exit;
                return response()->json([ 'status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }

    public function verifyOtp(Request $request){

        $validator  =   Validator::make($request->all(),[
            'chemist_id'   => 'required',
            'otp'          => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => 'Please fill all the fields' ]);
        }else{
            try{
                $chemist  = Chemist::where('phone_otp', $request->otp)->first();
                if ($chemist) {
                    if (Carbon::parse($chemist->phone_otp_expire)->greaterThan(Carbon::now())) {
                        try {
                            $chemistUserUpdate = Chemist::where('phone_otp', $request->otp)
                                                        ->update(['phone_otp'        => '',
                                                                  'phone_otp_expire' => '',
                                                                  'status'           => 'Pending',
                                                                  'is_verified'      => 1,
                                                                  'updated_at'       => Carbon::now(),
                                                                ]);
                            return response()->json(['message' => "正常に検証されました", 'status' => 'success']);
                        } catch (\Exception $e) {
                            return response()->json(['message' => "入力が正しくありません", 'status' => 'error']);
                        }
                    } else {
                        return response()->json(['message' => "確認コードが期限切れです", 'status' => 'error']);
                    }
                } else {
                    return response()->json(['message' => "確認コードが一致しません", 'status' => 'error']);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function login(Request $request){

        $validator  =   Validator::make($request->all(),[
            'phone'         => 'required',
            'password'      => 'required|string',
            //'device_token'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            if (!Auth::guard('chemists')->attempt(['phone' => $request->phone, 'password' => $request->password, 'is_verified' => 1 ])) {
                return response()->json([ 'status' => 'error', 'message' => '無効なユーザー名、またはパスワードです' ]);
            }
            $user           = Auth::guard('chemists')->user();
            $tokenResult    = $user->createToken('Personal Access Token');
            $token          = $tokenResult->token;
            $token->save();
            $approveStatus  = Chemist::select('status')->where('id', Auth::guard('chemists')->user()->id)->first()->status;
            Chemist::where('id', Auth::guard('chemists')->user()->id)->update([ 'device_token' => $request->device_token, 'online_status' => 'Online' ]);
            return response()->json([ 'status'         => 'success',
                                      'access_token'   => $tokenResult->accessToken,
                                      'token_type'     => 'Bearer',
                                      'user_id'        => strval (Auth::guard('chemists')->user()->id),
                                      'approve_status' => $approveStatus
                                    ]);
        }
    }

    public function logout(Request $request){

        Chemist::where('id', $request->user()->id)->update([ 'online_status' => 'Offline', 'device_token' => '' ]);
        $request->user()->token()->revoke();
        return response()->json([ 'status' => 'success', 'message' => 'ログアウトしました' ]);
    }

    public function passwordReset(Request $request){

        $validator  =   Validator::make($request->all(),[
            'phone'    => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if (Chemist::where('phone', '=', $request->phone)->where('is_verified', '=', 1)->exists()) {
                    $digits     = 4;
                    $otp        = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $data       = ['otp' => $otp];
                    $client     = new Client();
                    $res        = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number='.$request->phone.'&otp='.$otp , [
                    ]);
                    $res->getStatusCode();
                    $res->getHeader('content-type');
                    $res->getBody();
                    $updateChemist  =   Chemist::where('phone', $request->phone)
                                                ->where('is_verified', 1)
                                                ->update(['otp' => $otp]);
                    if($updateChemist){
                        return response()->json([ 'status' => 'success', 'message' => '入力された電話番号に確認コードが送られました' ]);
                    }else{
                        return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
                    }
                }else{
                    return response()->json([ 'status' => 'error', 'message' => "入力された電話番号が存在しません" ]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function verifyResetOtp(Request $request){

        $validator  =   Validator::make($request->all(),[
            'phone' => 'required',
            'otp'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $chemist  = Chemist::where('otp', $request->otp)->where('phone', $request->phone)->first();
                if ($chemist) {
                    try {
                        $chemistUserUpdate = Chemist::where('otp', $request->otp)
                                                    ->where('phone', $request->phone)
                                                    ->update([  'otp'        => '',
                                                                'updated_at' => Carbon::now(),
                                                            ]);
                        return response()->json(['message' => "正常に検証されました", 'status' => 'success']);
                    } catch (\Exception $e) {
                        return response()->json(['message' => "入力が正しくありません", 'status' => 'error']);
                    }
                } else {
                    return response()->json(['message' => "確認コードが一致しません", 'status' => 'error']);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }

    }

    public function resetPassword(Request $request){

        $validator  =   Validator::make($request->all(),[
            'phone'     => 'required',
            'password'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $chemist  = Chemist::where('phone', $request->phone)->where('is_verified', 1)->first();
                if($chemist){
                    Chemist::where('phone', $request->phone)
                            ->where('is_verified', 1)
                            ->update([ 'password' => Hash::make(request('password')) ]);
                    return response()->json([ 'status' => 'success', 'message' => 'パスワードを変更しました' ]);
                }else{
                    return response()->json([ 'status' => 'error', 'message' => '確認コードが期限切れです' ]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }

    }

    public function updateProfile(Request $request){

        $validator  =   Validator::make($request->all(),[
            'first_name'        => 'required',
            'last_name'         => 'required',
            'gender'            => 'required',
            'email'             => 'required|unique:chemists,email,'.$request->user()->id.',id,is_verified,1',
            'pincode'           => 'required',
            'licence_number'    => 'required',
            'registration_year' => 'required',
            'registration_month'=> 'required'
        ], [
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $chemist                    =  Chemist::where('id', $request->user()->id)->first();
                $chemist->first_name        =  $request->first_name;
                $chemist->first_furigana    =  $request->first_furigana;
                $chemist->last_name         =  $request->last_name;
                $chemist->last_furigana     =  $request->last_furigana;
                $chemist->gender            =  $request->gender;
                $chemist->dob               =  $request->dob;
                $chemist->email             =  $request->email;
                $chemist->have_child        =  $request->have_child;
                $chemist->response_time     =  $request->response_time;
                $chemist->self_introduction =  $request->self_introduction;
                $chemist->post_code         =  $request->pincode;
                $chemist->prefecture        =  $request->prefecture;
                $chemist->city              =  $request->city;
                $chemist->town              =  $request->town;
                $chemist->furikana          =  $request->furikana;
                $chemist->registration_year =  $request->registration_year;
                $chemist->registration_month=  $request->registration_month;
                $chemist->licence_number    =  $request->licence_number;
                $chemist->skills            =  $request->skills;
                $chemist->no_of_dependents  =  $request->no_of_dependents;
                $chemist->spouse            =  $request->spouse;
                $chemist->spousal_support   =  $request->spousal_support;
                $chemist->desired_entry     =  $request->desired_entry;
                $chemist->save();
                $i = 0; $data = [];
                foreach($request->education_year as $row){
                    $data[$i]['chemist_id'] = $chemist->id;
                    $data[$i]['year']       = $request->education_year[$i];
                    $data[$i]['month']      = $request->education_month[$i];
                    $data[$i]['content']    = $request->education_content[$i];
                    $i++;
                }
                ChemistEducation::where('chemist_id', $chemist->id)->delete();
                ChemistEducation::insert($data);
                $i = 0; $data = [];
                foreach($request->qualification_year as $row){
                    $data[$i]['chemist_id'] = $chemist->id;
                    $data[$i]['year']       = $request->qualification_year[$i];
                    $data[$i]['month']      = $request->qualification_month[$i];
                    $data[$i]['content']    = $request->qualification_content[$i];
                    $i++;
                }
                ChemistQualification::where('chemist_id', $chemist->id)->delete();
                ChemistQualification::insert($data);
                return response()->json([ 'status' => 'success', 'message' => 'プロフィールを更新しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getProfileDetails(Request $request){

        try{
            $chemist    =  Chemist::where('id', $request->user()->id)->first();
            $output['first_name']         =  $chemist->first_name;
            $output['first_furigana']     =  ($chemist->first_furigana) ? $chemist->first_furigana : "";
            $output['last_name']          =  $chemist->last_name;
            $output['last_furigana']      =  ($chemist->last_furigana) ? $chemist->last_furigana : "";
            $output['gender']             =  $chemist->gender;
            $output['dob']                =  ($chemist->dob) ? $chemist->dob : "";
            $output['email']              =  $chemist->email;
            $output['have_child']         =  ($chemist->have_child) ? $chemist->have_child : "";
            $output['response_time']      =  ($chemist->response_time) ? $chemist->response_time : "";
            $output['self_introduction']  =  ($chemist->self_introduction) ? $chemist->self_introduction : "";
            $output['post_code']          =  $chemist->post_code;
            $output['prefecture']         =  ($chemist->prefecture) ? $chemist->prefecture : "";
            $output['city']               =  ($chemist->city) ? $chemist->city : "";
            $output['town']               =  ($chemist->town) ? $chemist->town : "";
            $output['furikana']           =  ($chemist->furikana) ? $chemist->furikana : "";
            $output['registration_year']  =  $chemist->registration_year;
            $output['registration_month'] =  $chemist->registration_month;
            $output['licence_number']     =  $chemist->licence_number;
            $output['skills']             =  ($chemist->skills) ? $chemist->skills : "";
            $output['no_of_dependents']   =  ($chemist->no_of_dependents) ? $chemist->no_of_dependents : "";
            $output['spouse']             =  ($chemist->spouse) ? $chemist->spouse : "";
            $output['spousal_support']    =  ($chemist->spousal_support) ? $chemist->spousal_support : "";
            $output['desired_entry']      =  ($chemist->desired_entry) ? $chemist->desired_entry : "";
            $educations = ChemistEducation::where('chemist_id', $chemist->id)->get();
            $i=0; $output['educations'] = [];
            foreach($educations as $row){
                if($row->content){
                    $output['educations'][$i]['year']    = $row->year;
                    $output['educations'][$i]['month']   = $row->month;
                    $output['educations'][$i]['content'] = $row->content;
                    $i++;
                }
            }
            $qualifications = ChemistQualification::where('chemist_id', $chemist->id)->get();
            $i=0; $output['qualifications'] = [];
            foreach($qualifications as $row){
                if($row->content){
                    $output['qualifications'][$i]['year']    = $row->year;
                    $output['qualifications'][$i]['month']   = $row->month;
                    $output['qualifications'][$i]['content'] = $row->content;
                    $i++;
                }
            }
            return response()->json([ 'status' => 'success', 'profile_details' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function getProfilePicture(Request $request){

        try{
            $chemist  =  Chemist::where('id', $request->user()->id)->first();
            if($chemist->image)
                $image  =  asset("/images/chemist/".$chemist->image);
            else
                $image  =  "";
            return response()->json([ 'status' => 'success', 'image' => $image, 'first_name' => $chemist->first_name, "last_name" => $chemist->last_name ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function setProfilePicture(Request $request){

        $validator  = Validator::make($request->all(),[
                            'image'  => 'required'
                        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else {
            try{
                $chemistimage  =  Chemist::where('id', $request->user()->id)->first();
                if($chemistimage->image)
                    File::delete(public_path().'/images/chemist/'.$chemistimage->image);

                $safeName            = rand().'.'.'png';
                $data                = $request['image'];
                list($type, $data)   = explode(';', $data);
                list(, $data)        = explode(',', $data);
                $data                = base64_decode($data);
                file_put_contents(public_path().'/images/chemist/'.$safeName, $data);
                $chemistimage->image = $safeName;
                $chemistimage->save();
                return response()->json([ 'status' => 'success', 'message' => 'イメージ写真を更新しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function resendOtp(Request $request){

        $validator  =   Validator::make($request->all(),[
            'chemist_id'  => 'required',
            'phone'       => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $digits               = 4;
                $phone_otp            = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $phone_otp_expire     = Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
                $email                = Chemist::where('id', $request->chemist_id)->first()->email;
                $updateData           = Chemist::where('id', $request->chemist_id)->update([ 'phone_otp'        => $phone_otp,
                                                                                             'phone_otp_expire' => $phone_otp_expire
                                                                                           ]);
                $data   = [ 'otp' => $phone_otp ];
                $client = new Client();
                $res    = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number='.$request->phone.'&otp='.$phone_otp);
                $res->getStatusCode();
                $res->getHeader('content-type');
                $res->getBody();
                Mail::to($email)->send(new SendChemistOtp($data));
                return response()->json([ 'status' => 'success', 'message' => '確認コードを再送信しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }

    }

    public function checkPhoneNumberExist(Request $request){

        $validator  = Validator::make($request->all(),[
            'phone'  => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else {
            try{
                if (Chemist::where('phone', '=', $request->phone)->whereIn('status', ['Approved', 'Pending', 'Suspended'])->exists()) {
                    return response()->json([ 'status' => 'error', 'message' => '入力した電話番号は既に登録済みです']);
                }else{
                    return response()->json([ 'status' => 'success', 'message' => 'Not Exist']);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function checkEmailExist(Request $request){

        $validator  = Validator::make($request->all(),[
            'email'  => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else {
            try{
                if (Chemist::where('email', '=', $request->email)->whereIn('status', ['Approved', 'Pending', 'Suspended'])->exists()) {
                    return response()->json([ 'status' => 'error', 'message' => '入力したEメールは既に登録済みです']);
                }else{
                    return response()->json([ 'status' => 'success', 'message' => 'Not Exist']);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function updateDeviceToken(Request $request){

        $validator  = Validator::make($request->all(),[
            'phone'         => 'required',
            'device_token'  => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else {
            try{
                if (Chemist::where('phone', '=', $request->phone)->whereIn('status', ['Approved', 'Pending', 'Suspended'])->exists()) {

                    Chemist::where('phone', $request->phone)
                            ->where('is_verified', 1)
                            ->update([ 'device_token' => $request->device_token ]);
                    return response()->json([ 'status' => 'success']);
                }else{
                    return response()->json([ 'status' => 'error']);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function sendMessage(){

        $data = array(
                "title" => 'test',
                "body" => 'test',
                "sound" => "default",
            );
        $type = 1;
        if ($type == '2') {
        $fcmFields  =   array( 'to' => 'fWtORnmpoUrWmLtFilihRs:APA91bHUfIij7NpbxvOKzSL46sQJ1Z9vsqV8Jte7FnxldZ-5oRgl-rfqHk84RsXHq8mfYi192W93hAGcI9f5KsP0rA-YYf0jhlbpwss5q6NMCPmiKjKVES_uthGtejEtvVPVE2qKTbNP',
                            'priority' => 'high',
                            'notification' => $data,
                            'data' => $data
                            );
        } else {
        $fcmFields  =   array( 'to' => 'fWtORnmpoUrWmLtFilihRs:APA91bHUfIij7NpbxvOKzSL46sQJ1Z9vsqV8Jte7FnxldZ-5oRgl-rfqHk84RsXHq8mfYi192W93hAGcI9f5KsP0rA-YYf0jhlbpwss5q6NMCPmiKjKVES_uthGtejEtvVPVE2qKTbNP',
                            'priority' => 'high',
                            'notification' => $data,
                            'data' => $data
                            );
        }
        // $fcmFields = array(
        //         "to" => 'fZUhSfNHCNQ:APA91bGj34DJkm8FskWi_BGYa6CzleO84PvaamrdJQrWC-yFjHyUtMSLDKBqlimLlIElYY9wDE4wAxBl8acMx01J8s8bCPyvSVtxotquTE8D8JxCTL8GeAYCbV46x-3ZBGW8ieV_QtJX',
        //         "notification" => array(
        //             "title" => 'test',
        //             "body" => 'test',
        //             "sound" => "default",
        //             "priority" => "high",
        //             "show_in_foreground" => true,
        //             "targetScreen" => 'detail'
        //         ),
        //         "priority" => 10
        // );
        $headers = array(
        'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
        'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        print_r($result);
        curl_close($ch);
    }
}
