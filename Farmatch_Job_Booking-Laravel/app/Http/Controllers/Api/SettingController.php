<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Faq;
use App\City;
use App\JobPost;
use App\Chemist;
use App\DrugStore;
use App\Prefecture;
use App\Notification;
use App\ChemistPayment;
use App\ChemistJobApplication;
use App\ChemistInterviewTiming;

class SettingController extends Controller
{
    public function getPrefecture(){

        try{
            $prefectures = Prefecture::select('id', 'prefecture', 'latitude', 'longitude')->get();
            $output = [];$i = 0;
            foreach($prefectures as $row){
                $output[$i]['id']         = $row->id;
                $output[$i]['prefecture'] = $row->prefecture;
                $output[$i]['latitude']   = $row->latitude;
                $output[$i]['longitude']  = $row->longitude;
                $i++;
            }
            return response()->json([ 'status' => 'success', 'prefectures' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
        }
    }

    public function getCities(Request $request){

        $validator  =   Validator::make($request->all(),[
            'prefecture_id'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $cities = City::select('id', 'prefecture_id', 'city', 'latitude', 'longitude')->where('prefecture_id', $request->prefecture_id)->get();
                $prefecture = Prefecture::select('id', 'prefecture', 'latitude', 'longitude')->where('id',$request->prefecture_id)->first();
                $output = [];$i = 0;
                $output[0]['id']         = 0;
                $output[0]['prefecture_id'] = (int)$request->prefecture_id;
                $output[0]['prefecture_name'] = $prefecture->prefecture;
                $output[$i]['city']          = "全て";
                $output[0]['latitude']   = $prefecture->latitude;
                $output[0]['longitude']  = $prefecture->longitude;
                $i = 1;
                foreach($cities as $row){
                    $prefecture = Prefecture::select('id', 'prefecture')->where('id',$row->prefecture_id)->first();
                    $output[$i]['id']            = $row->id;
                    $output[$i]['prefecture_id'] = $row->prefecture_id;
                    $output[$i]['prefecture_name'] = $prefecture->prefecture;
                    $output[$i]['city']          = $row->city;
                    $output[$i]['latitude']      = $row->latitude;
                    $output[$i]['longitude']     = $row->longitude;
                    $i++;
                }
            return response()->json([ 'status' => 'success', 'cities' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getChemistHourlyWage(Request $request){

        try{
            $hourlyWage = Chemist::select('hourly_wage')->where('id', $request->user()->id)->first();
            return response()->json([ 'status' => 'success', 'hourly_wage' => $hourlyWage->hourly_wage ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }

    }

    public function hourlyWageSettings(Request $request){

        $validator  =   Validator::make($request->all(),[
            'hourly_wage'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                Chemist::where('id', $request->user()->id)
                               ->update([ 'hourly_wage' => $request->hourly_wage ]);
                return response()->json([ 'status' => 'success', 'message' => '更新しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getNotificationMessages(Request $request){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    if($row->job_post_id != ''){
                        $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                        $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                        if($row->job_application_id != ''){
                            $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                            $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                            $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                            $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                            $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                            $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                        }else{
                            $diffHours                    = 0;
                            $output[$i]['job_title']      = "";
                            $output[$i]['post_date']      = "";
                            $output[$i]['time_from']      = "";
                            $output[$i]['time_to']        = "";
                            $output[$i]['rest_time']      = "";
                        }
                    } else{
                        $diffHours                    = 0;
                        $output[$i]['drug_store_id']  = "";
                        $output[$i]['drug_store_name']= "";
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                    if($output[$i]['type'] == 'Job Application'){
                        $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                        $applicationStatus  = ($application) ? $application->status : "";
                    }else{
                        $applicationStatus  = "";
                    }
                    if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                        $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                    }else{
                        $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['application_status'] = $applicationStatus;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if($applicationStatus == 'Accepted'){
                        if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                            unset($output[$i]);
                            Notification::where('id',$row->id)->delete();
                            $isIncrement = 1;
                        }
                    }
                    if($applicationStatus == 'Job Completed'){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }

                    if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                            ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        }
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
            }
            return response()->json([ 'status' => 'success', 'notifications' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function getPendingIntervieTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'drug_store_id'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $timing     =   ChemistInterviewTiming::where('chemist_id', $request->user()->id)
                                                        ->where('drug_store_id', $request->drug_store_id)
                                                        ->where('status', 'Pending')
                                                        ->first();
                if($timing)
                    return response()->json([ 'status' => 'success', 'day' => $timing->day, 'time_from' => $timing->time_from, 'time_to' => $timing->time_to ]);
                else
                   return response()->json([ 'status' => 'error', 'message' => 'Nothing to show' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getChemistPayment(Request $request){

        try{
            $currentDay = Carbon::now()->format('d');
            if($currentDay > 20){
                $startDate      = Carbon::now()->format('Y-m')."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->addMonths(1)->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $request->user()->id)
                                                        ->whereIn('status', ['Job completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $row){

                    if($row->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $row->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $row->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $row->rest_time;
                        $wageInMinutes      = $row->hourly_wage/60;
                        $totalWage         += (($totalWorkTime * $wageInMinutes) * $row->cancellation_percentage) /100;
                    }else{
                        $to                 =  Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                        $from               =  Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                        $diff_in_minutes    =  $to->diffInMinutes($from);
                        $workHourinMinutes  =  $diff_in_minutes;
                       // $totalWorkTime      =  $workHourinMinutes - $row->actual_rest_time;
                        $totalWorkTime      =  $workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work;
                        $wageInMinutes      =  $row->hourly_wage/60;
                        $totalWage          += $totalWorkTime * $wageInMinutes;
                    }
                    $transportationCost+= $row->transportation_cost;
                }
            }else{
                $startDate      = Carbon::now()->subMonth()->format('Y-m')."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $request->user()->id)
                                                        ->whereIn('status', ['Job completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $row){

                    if($row->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $row->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $row->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $row->rest_time;
                        $wageInMinutes      = $row->hourly_wage/60;
                        $totalWage         += (($totalWorkTime * $wageInMinutes) * $row->cancellation_percentage) /100;
                        $transportationCost+= $row->transportation_cost;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        //$totalWorkTime      = $workHourinMinutes - $row->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work;
                        $wageInMinutes      = $row->hourly_wage/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                        $transportationCost+= $row->actual_transportation_cost;
                    }

                }
            }
            $output = [];
            $output['amount']              = round($totalWage);
            $output['transportation_cost'] = round($transportationCost);
            $output['consumption_tax']     = round((($totalWage + $transportationCost) * 10) /100, 2);
            $output['total_amount']        = $output['amount'] + $output['transportation_cost'] + $output['consumption_tax'];
            $output['total_amount']        = round($output['amount'] + $output['transportation_cost']);

            return response()->json([ 'status' => 'success', 'data' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function getChemistPaymentHistory(Request $request){

        $validator  =   Validator::make($request->all(),[
            'date'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $startDate      = Carbon::parse($request->date)->format('Y-m');
                $startDate      = $startDate."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::parse($request->date)->format('m');
                if($endDate+1 > 12){
                    $endYear    = Carbon::parse($request->date)->format('Y');
                    $endYear    = $endYear+1;
                    $endDate    = $endYear."-01-20";
                }else{
                    $endYear    = Carbon::parse($request->date)->format('Y');
                    $endDate    = $endYear."-".sprintf("%02d", $endDate+1)."-20";
                }

                $startDate      = Carbon::parse($startDate)->format('Y-m-d');
                $endDate        = Carbon::parse($endDate)->format('Y-m-d');
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $request->user()->id)
                                                        ->whereIn('status', ['Job completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $row){
                    if($row->status == 'Job Cancelled'){
                        $to                 =  Carbon::createFromFormat('H:i:s', $row->time_from);
                        $from               =  Carbon::createFromFormat('H:i:s', $row->time_to);
                        $diff_in_minutes    =  $to->diffInMinutes($from);
                        $workHourinMinutes  =  $diff_in_minutes;
                        $totalWorkTime      =  $workHourinMinutes - $row->rest_time;
                        $wageInMinutes      =  $row->hourly_wage/60;
                        $totalWage          += (($totalWorkTime * $wageInMinutes) * $row->cancellation_percentage) /100;
                        $transportationCost+= $row->transportation_cost;
                    }else{
                        $to                 =  Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                        $from               =  Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                        $diff_in_minutes    =  $to->diffInMinutes($from);
                        $workHourinMinutes  =  $diff_in_minutes;
                        $totalWorkTime      =  $workHourinMinutes - $row->actual_rest_time;
                        $wageInMinutes      =  $row->hourly_wage/60;
                        $totalWage          += $totalWorkTime * $wageInMinutes;
                        $transportationCost += $row->actual_transportation_cost;
                    }
                }
                $output = [];
                $output['amount']              = round($totalWage);
                $output['transportation_cost'] = round($transportationCost);
                $output['consumption_tax']     = round((($totalWage + $transportationCost) * 10) /100, 2);
                $output['total_amount']        = $output['amount'] + $output['transportation_cost'] + $output['consumption_tax'];
                $output['total_amount']        = round($output['amount'] + $output['transportation_cost']);
                return response()->json([ 'status' => 'success', 'data' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function pendingJobs(Request $request){

        try{
            $pendingjobs = ChemistJobApplication::where('chemist_id', $request->user()->id)
                                                ->whereDate('date', '>=', Carbon::now())
                                                ->where('status', 'Pending')
                                                ->get();
            $output = []; $i = 0;
            foreach($pendingjobs as $row){
                $drugStore = DrugStore::where('id', $row->drug_store_id)->first();
                $jobPost   = JobPost::where('id', $row->job_post_id)->first();
                $output[$i]['job_post_id'] = $row->job_post_id;
                $output[$i]['drug_store']  = $drugStore->name;
                $output[$i]['date']        = $row->date;
                $output[$i]['time_from']   = date('H:i', strtotime($row->time_from));
                $output[$i]['time_to']     = date('H:i', strtotime($row->time_to));
                $output[$i]['content']     = $jobPost->content;
                $output[$i]['title']       = $jobPost->job_name;
                $output[$i]['image']       = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                $i++;
            }
            return response()->json([ 'status' => 'success', 'pending_jobs' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function downloadResume(Request $request){

        //$chemist_id = $request->chemist;
        $chemist    = Chemist::find($request->user()->id);
        if(!$chemist){
            return response()->json(['status'=>false, 'message' => 'Chemist not exist']);
        }
        $pdf   = PDF::loadView('store.testresume3',compact('chemist'));
        $mask  = public_path()."/resumes/".Auth::user()->id.'_*.*';
        array_map('unlink', glob($mask));
        $random = rand();
        $pdf->setPaper('a4', 'portrait')->save(public_path()."/resumes/".Auth::user()->id."_".$random.".pdf");
        return response()->json([ 'status' => 'success', 'path' => URL::to('/')."/resumes/".Auth::user()->id."_".$random.".pdf"]);
    }

    public function chemistFaq(Request $request){

        try{
            $faqs     =   Faq::where('type', 'Chemist')->get();
            $output = [] ; $i = 0;
            foreach($faqs as $row){
                $output[$i]['question'] = $row->question;
                $output[$i]['answer']   = $row->answer;
                $i++;
            }
            return response()->json([ 'status' => 'success', 'faqs' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function deleteNotification(Request $request){

        $validator  =   Validator::make($request->all(),[
            'notification_id'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $notification = Notification::find($request->notification_id)->delete();
                return response()->json([ 'status' => 'success', 'message' => '削除しました' ]);

            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function sendNotification(){

        $fcmFields = array(
                "to" => 'fZUhSfNHCNQ:APA91bGj34DJkm8FskWi_BGYa6CzleO84PvaamrdJQrWC-yFjHyUtMSLDKBqlimLlIElYY9wDE4wAxBl8acMx01J8s8bCPyvSVtxotquTE8D8JxCTL8GeAYCbV46x-3ZBGW8ieV_QtJX',
                "notification" => array(
                    "title" => 'test',
                    "body" => 'test',
                    "sound" => "default",
                    "priority" => "high",
                    "show_in_foreground" => true,
                    "targetScreen" => 'detail'
                ),
                "priority" => 10
        );
        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        curl_close($ch);
    }


    public function getChemistLocation(){

            try{
                $output = Chemist::select('id', 'prefecture_id', 'city_id')->where('id', Auth::user()->id)->first();

                return response()->json([ 'status' => 'success', 'chemist_location' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }

    }

    public function saveChemistLocation(Request $request)
    {

        $validator  =   Validator::make($request->all(),[
            'prefecture_id'   => 'numeric',
            'city_id'   => 'numeric'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
        try {
            $chemist = Chemist::where('id', Auth::user()->id)->first();
            if($request->prefecture_id) $chemist->prefecture_id = $request->prefecture_id;
            if($request->city_id) $chemist->city_id = $request->city_id;
            $chemist->save();

            return response()->json(['status' => 'success', 'message' => '場所を更新しました']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    }

    public function listyears(Request $request)
    {
        for($year=1951;$year<=date('Y');$year++){
            $data[]=(string)$year;
        }
        return $data;
    }

}
