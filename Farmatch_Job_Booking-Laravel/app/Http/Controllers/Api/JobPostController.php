<?php

namespace App\Http\Controllers\Api;

use App\AdminChemistChatHead;
use App\DrugStoreWage;
use App\GroupChatHead;
use App\StoreChemistChatHead;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Chemist;
use App\JobPost;
use App\Favourite;
use App\DrugStore;
use App\Notification;
use App\ReadStatus;
use App\DrugStoreTiming;
use App\WorkPlaceReview;
use App\ChemistJobApplication;
use App\ChemistInterviewTiming;
use App\ChemistInspectionTiming;

use phpDocumentor\Reflection\Types\Null_;

class JobPostController extends Controller
{

    public function jobPostListing(Request $request){

        $validator  =   Validator::make($request->all(),[
            'latitude'   => 'required',
            'longitude'  => 'required',
            'date'       => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if($request->latitude != '' && $request->longitude != ''){

                    $posts  =  DB::select("SELECT job_posts.id, `job_posts`.drug_store_id, `job_posts`.status, `drug_stores`.name, job_name, job_posts.date, work_from, work_to, job_title, main_image, latitude, longitude,  SQRT(
                                        POW(69.1 * (latitude - $request->latitude), 2) +
                                        POW(69.1 * ($request->longitude - longitude) * COS(latitude / 57.3), 2)) AS distance
                                        FROM `drug_stores` INNER JOIN `job_posts` ON
                                        job_posts.drug_store_id = `drug_stores`.id WHERE `date` = '$request->date' AND job_posts.deleted_at IS NULL AND job_posts.status = 'Active' HAVING distance < 100  ORDER BY distance");
                    $output = [];$i = 0;$drugStoreIds = [];
                    $current = Carbon::now();
                    $current_date = $current->toDateTimeString();
                    $current_date_time = date('Y-m-d H:i:s', strtotime($current_date));

                    foreach($posts as $row){
                        if($row->status == 'Active'){
                        if($this->checkIsVaccancy($row->id)){
                            $end_time = $row->date." ".$row->work_to;
                            $end_date_time = date('Y-m-d H:i:s', strtotime($end_time));
                            if(strtotime($end_date_time) > strtotime($current_date_time))
                            {
                            $output[$i]['id']            = $row->id;
                            $output[$i]['drug_store_id'] = $row->drug_store_id;
                            $output[$i]['store_name']    = $row->name;
                            $output[$i]['job_name']      = $row->job_name;
                            $output[$i]['work_from']     = Carbon::createFromFormat('H:i:s', $row->work_from)->format('H:i');
                            $output[$i]['work_to']       = Carbon::createFromFormat('H:i:s', $row->work_to)->format('H:i');
                            $output[$i]['latitude']      = (float)$row->latitude;
                            $output[$i]['longitude']     = (float)$row->longitude;
                            $output[$i]['image']         = ($row->main_image) ? asset("/images/drug_store/".$row->main_image) : "";
                            $i++;
                            }
                        }
                        }
                    }
                }
                $result = $output;
                $i = 0;$drugStoreIds = [];$output1 = [];
                if($request->favourite != ''){

                    $favouriteJobPosts      = Favourite::select('drug_store_id')
                                                        ->where('chemist_id', Auth::user()->id)
                                                        ->where('status', 1)
                                                        ->where('drug_store_id', $request->favourite)
                                                        ->get();
                    $favouriteDrugStores    = [];
                    foreach($favouriteJobPosts as $row){
                        $favouriteDrugStores[] = $row->drug_store_id;
                    }
                    foreach($output as $row){
                        if(in_array($row['drug_store_id'], $favouriteDrugStores)){
                            if($this->checkIsVaccancy($row['id'])){
                                $output1[$i]['id']           = $row['id'];
                                $drugStoreIds[]              = $row['drug_store_id'];
                                $output1[$i]['store_name']   = DrugStore::where('id', $row['drug_store_id'])->first()->name;
                                $output1[$i]['job_name']     = $row['job_name'];
                                $output1[$i]['work_from']    = Carbon::createFromFormat('H:i', $row['work_from'])->format('H:i');
                                $output1[$i]['work_to']      = Carbon::createFromFormat('H:i', $row['work_to'])->format('H:i');
                                $output1[$i]['latitude']     = (float)DrugStore::where('id', $row['drug_store_id'])->first()->latitude;
                                $output1[$i]['longitude']    = (float)DrugStore::where('id', $row['drug_store_id'])->first()->longitude;
                                $mainImage = DrugStore::where('id', $row['drug_store_id'])->first()->main_image;
                                $output1[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
                                $i++;
                            }
                        }
                    }
                    $result = $output1;
                }

                if($request->interviewed == 1){

                    $interviewedJobPosts      = ChemistInterviewTiming::select('drug_store_id')
                                                                    ->where('chemist_id', Auth::user()->id)
                                                                    ->where('status', 'Approved')
                                                                    ->get();
                    $interviewedDrugStores    = [];
                    foreach($interviewedJobPosts as $row){
                        $interviewedDrugStores[] = $row->drug_store_id;
                    }
                    foreach($output as $row){
                        if(in_array($row['drug_store_id'], $interviewedDrugStores) && !in_array($row['drug_store_id'], $drugStoreIds)){
                            if($this->checkIsVaccancy($row['id'])){
                                $output1[$i]['id']           = $row['id'];
                                $output1[$i]['store_name']   = DrugStore::where('id', $row['drug_store_id'])->first()->name;
                                $output1[$i]['job_name']     = $row['job_name'];
                                $output1[$i]['work_from']    = Carbon::createFromFormat('H:i', $row['work_from'])->format('H:i');
                                $output1[$i]['work_to']      = Carbon::createFromFormat('H:i', $row['work_to'])->format('H:i');
                                $output1[$i]['latitude']     = (float)DrugStore::where('id', $row['drug_store_id'])->first()->latitude;
                                $output1[$i]['longitude']    = (float)DrugStore::where('id', $row['drug_store_id'])->first()->longitude;
                                $mainImage = DrugStore::where('id', $row['drug_store_id'])->first()->main_image;
                                $output1[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
                                $i++;
                            }
                        }
                    }
                    $result = $output1;
                }
                return response()->json([ 'status' => 'success', 'drug_stores' => $result ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }




    public function checkIsVaccancy($jobPostId){

        $countJobAccepted = ChemistJobApplication::whereIn('status', ['Chemist Accept', 'Job Completed'])
                                                    ->where('job_post_id', $jobPostId)
                                                    ->count();
        $totalVaccancy    = JobPost::where('id', $jobPostId)->first()->no_of_vaccancies;
        if($countJobAccepted < $totalVaccancy){
           return true;
        }else{
           return false;
        }
    }

    public function jobPostDetails(Request $request){

        $validator  =   Validator::make($request->all(),[
            'job_post_id'  => 'required'
        ]);
        $daysJapan = ['Sunday' => '日', 'Monday' => '月', 'Tuesday' => '火', 'Wednesday' => '水', 'Thursday' => '木', 'Friday' => '金', 'Saturday' => '土' ];
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $postDetails            = JobPost::where('id', $request->job_post_id)->first();
                $drugStoreDetails       = DrugStore::where('id', $postDetails->drug_store_id)->first();
                $application            = ChemistJobApplication::where('chemist_id', Auth::user()->id)->where('job_post_id', $request->job_post_id)->latest('id')->first();
                $registeredDate         = Carbon::parse($drugStoreDetails->created_at);
                $diffYears              = Carbon::now()->diffInYears($registeredDate);
                $completedWorks         = JobPost::whereDate('date', '<=', Carbon::yesterday()->toDateString())
                                                    ->where('status', 'Active')
                                                    ->where('drug_store_id', $postDetails->drug_store_id)
                                                    ->count();
                $completedJobs = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                    ->where('status', 'Job Completed')
                    ->where('drug_store_id', $postDetails->drug_store_id)
                    ->groupBy('chemist_id')
                    ->selectRaw('chemist_id, count(chemist_id) as count')
                    ->get();

                $completed = 0;
                foreach($completedJobs as $row){
                    if($row->count > 0)
                    $completed++;
                }
                $chemistWorked          = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                                ->where('drug_store_id', $postDetails->drug_store_id)
                                                                ->where('status', 'Job Completed')
                                                                ->count();
                $chemistRepeatWorked    = ChemistJobApplication::whereDate('date', '<=',Carbon::now()->toDateString())
                                                                ->where('drug_store_id', $postDetails->drug_store_id)
                                                                ->groupBy('chemist_id')
                                                                ->where('status', 'Job Completed')
                                                                ->selectRaw('chemist_id, count(chemist_id) as count')
                                                                ->get();
                $suddenCancellation     = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                                ->where('drug_store_id', $postDetails->drug_store_id)
                                                                ->where('status', 'Job Cancelled')
                                                                ->count();
                $repeated = 0;
                foreach($chemistRepeatWorked as $row){
                    if($row->count > 1)
                    $repeated++;
                }

                $inspectionStatus       = ChemistInspectionTiming::where('chemist_id', Auth::user()->id)->where('drug_store_id', $postDetails->drug_store_id)->first();
                $interviewStatus        = ChemistInterviewTiming::where('drug_Store_id', $postDetails->drug_store_id)->where('chemist_id', Auth::user()->id)->first();
                $favouriteStatus        = Favourite::where('drug_Store_id', $postDetails->drug_store_id)->where('chemist_id', Auth::user()->id)->first();
                $output['id']           = $postDetails->id;
                $output['drug_store_id']= $postDetails->drug_store_id;
                $output['drug_model']   = ($drugStoreDetails->history_model) ? $drugStoreDetails->history_model : "" ;
                $output['address']      = ($drugStoreDetails->address) ? $drugStoreDetails->address : "";
                $output['store_name']   = $drugStoreDetails->name;
                $output['job_name']     = $postDetails->job_name;
                $output['work_from']    = Carbon::createFromFormat('H:i:s', $postDetails->work_from)->format('H:i');
                $output['work_to']      = Carbon::createFromFormat('H:i:s', $postDetails->work_to)->format('H:i');
                $output['job_title']    = $postDetails->job_title;
                $output['work_content'] = $postDetails->content;
                $output['content_others'] = $postDetails->content_others;
                $output['notes']        = $postDetails->precautions;
                $output['year']         = Carbon::createFromFormat('Y-m-d', $postDetails->date)->format('Y');
                $output['month']        = Carbon::createFromFormat('Y-m-d', $postDetails->date)->format('m');
                $output['day']          = Carbon::createFromFormat('Y-m-d', $postDetails->date)->format('d');
                // $dayEng                 = Carbon::createFromFormat('Y-m-d', $postDetails->date)->format('l');
                foreach ($daysJapan as $dayeng => $day) {
                    if($dayeng == Carbon::createFromFormat('Y-m-d', $postDetails->date)->format('l')) {
                        $output['week_name'] = $day;
                    }
                }
                if($drugStoreDetails->main_image != '')
                    $mainImage = asset("/images/drug_store/".$drugStoreDetails->main_image);
                else
                    $mainImage = "";
                $output['main_image']   = $mainImage;
                if($drugStoreDetails->sub_image1 != '')
                    $subImage1 = asset("/images/drug_store/".$drugStoreDetails->sub_image1);
                else
                    $subImage1 = "";
                $output['sub_image1']   = $subImage1;
                if($drugStoreDetails->sub_image2 != '')
                    $subImage2 = asset("/images/drug_store/".$drugStoreDetails->sub_image2);
                else
                    $subImage2 = "";
                $output['sub_image2']          = $subImage2;
                $output['latitude']            = $drugStoreDetails->latitude;
                $output['longitude']           = $drugStoreDetails->longitude;
                $output['inteview_status']     = ($interviewStatus) ? $interviewStatus->status : "Not Applied";
                $output['time_editable']       = $postDetails->is_editable;
                $output['favourite_status']    = ($favouriteStatus) ? (($favouriteStatus->status == 1) ? true : false ) : false;
                $output['application_status']  = ($application) ? $application->status : "Not Applied";
                $output['application_id']      = ($application) ? $application->id : "";
                $output['inspection_status']   = ($inspectionStatus) ? true : false;
                $output['experience']          = $diffYears;
                // $output['given_jobs']          = $completedWorks;
                $output['given_jobs']          = $chemistWorked;
                $output['chemist_worked']      = $completed;
                $output['repeat_worked']       = $repeated;
                $output['sudden_cancellation'] = $suddenCancellation;
               // $output['hourly_wage']         = Auth::user()->hourly_wage;
               // $output['transportation_cost'] = Auth::user()->transportation_cost;
                $chemist_wage = DrugStoreWage::where('chemist_id', Auth::user()->id)->where('drug_store_id', $postDetails->drug_store_id)->first();
                $output['hourly_wage']         = ($chemist_wage) ? $chemist_wage->hourly_wage : "";
                $output['transportation_cost'] = ($chemist_wage) ? $chemist_wage->transportation_cost : "";
                return response()->json([ 'status' => 'success', 'drug_stores' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function addRemoveFavourite(Request $request){

        $validator  =   Validator::make($request->all(),[
            'store_id'  => 'required',
            'status'    => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if (Favourite::where('drug_store_id', '=', $request->store_id)->where('chemist_id', '=', Auth::user()->id)->exists()) {
                    Favourite::where('drug_store_id', $request->store_id)
                               ->where('chemist_id', Auth::user()->id)
                               ->update([ 'status' => $request->status ]);
                    return response()->json([ 'status' => 'success', 'message' => '更新しました' ]);
                }else{
                    $favourite                = new Favourite;
                    $favourite->drug_store_id = $request->store_id;
                    $favourite->chemist_id    = Auth::user()->id;
                    $favourite->status        = 1;
                    $favourite->save();
                    return response()->json([ 'status' => 'success', 'message' => 'お気に入りに追加しました' ]);
                }

            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }


    public function getInterviewTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'store_id'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{

                $timing = DrugStoreTiming::where('drug_store_id', $request->store_id)->get();
                $output = []; $i = 0; $timeFrom = ""; $timeTo = "";

                $daysJapan = ['Sunday' => '日', 'Monday' => '月', 'Tuesday' => '火', 'Wednesday' => '水', 'Thursday' => '木', 'Friday' => '金', 'Saturday' => '土' ];
                if($timing->count() > 0){
                    foreach($timing as $row){
                        $output[$i] = $daysJapan[$row->day];
                        $timeFrom   = $row->time_from;
                        $timeTo     = $row->time_to;
                        $i++;
                    }
                    $timeFrom = date('H:i',strtotime($timeFrom));
                    $timeTo   = date('H:i',strtotime($timeTo));
                }
                return response()->json([ 'status' => 'success', 'interview_days' => $output, 'time_from' => $timeFrom, 'time_to' => $timeTo ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function applyTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'day'           => 'required',
            'time_from'     => 'required',
            'post_id'       => 'required',
            'drug_store_id' => 'required',
            'time_to'       => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if (ChemistInterviewTiming::where('chemist_id', Auth::user()->id)->where('drug_store_id', $request->drug_store_id)->where('status', '!=', 'Rejected')->exists()) {
                    return response()->json([ 'status' => 'success', 'message' => 'Already Applied' ]);
                }else{
                    $timing                =  new ChemistInterviewTiming;
                    $timing->chemist_id    =  Auth::user()->id;
                    $timing->drug_store_id =  $request->drug_store_id;
                    $timing->job_post_id   =  $request->post_id;
                    $timing->time_from     =  $request->time_from;
                    $timing->time_to       =  $request->time_to;
                    $timing->day           =  $request->day;
                    $timing->save();
                    return response()->json([ 'status' => 'success', 'message' => '申請しました' ]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function applyJob(Request $request){

        $validator  =   Validator::make($request->all(),[
            'post_id'             => 'required',
            'drug_store_id'       => 'required',
            'from_time'           => 'required',
            'to_time'             => 'required',
            'rest_time'           => 'required',
            // 'transportaion_cost'  => 'required'
        ], [
            // 'transportaion_cost.required'  => '交通費は入力が必須です'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{ //commented the below if statement because we are now using multiple insertions in job applications instead of updating the status. This could be later taken as a log for evaluation which is not implemented
                // if (ChemistJobApplication::where('job_post_id', '=', $request->post_id)->where('chemist_id', Auth::user()->id)->whereIn('status', ['Accepted', 'Chemist Accept', 'Job Completed', 'Pending', 'Not Applied'])->exists()) {
                //     return response()->json([ 'status' => 'success', 'message' => 'Already applied for this job' ]);
                // }else{
                    $countJobAccepted = ChemistJobApplication::whereIn('status', ['Chemist Accept', 'Job Completed'])
                                                               ->where('job_post_id', $request->post_id)
                                                               ->count();
                    $totalVaccancy    = JobPost::where('id', $request->post_id)->first()->no_of_vaccancies;
                    if($countJobAccepted >= $totalVaccancy){
//                        return response()->json([ 'status' => 'error', 'message' => 'No vacancy for this job' ]);
                        return response()->json([ 'status' => 'error', 'message' => 'この仕事に欠員はありません' ]);
                    }

                $countJobApplied = ChemistJobApplication::whereIn('status', ['Accepted', 'Pending', 'Chemist Accept', 'Job Completed', 'Job Cancelled'])
                    ->where('job_post_id', $request->post_id)
                    ->where('chemist_id', Auth::user()->id)
                    ->count();

                if($countJobApplied){
                    return response()->json([ 'status' => 'error', 'message' => 'すでに適用されています' ]);
                }


                    $postDate   =   JobPost::where('id', $request->post_id)->first()->date;
                    $getChemistAccepetedJobs = ChemistJobApplication::select('time_from', 'time_to')
                                                                      ->where('chemist_id', Auth::user()->id)
                                                                      ->where('date', $postDate)
                                                                      ->where('status', 'Chemist Accept')
                                                                      ->get();
                    foreach($getChemistAccepetedJobs as $row){

                        $postDate           = Carbon::createFromFormat('Y-m-d', $postDate)->format('Y-m-d');

                        $fromInputTime      = Carbon::createFromFormat('H:i', $request->from_time)->format('H:i');
                        $fromInputDateTime  = $postDate." ".$fromInputTime;
                        $fromInputDateTime  = Carbon::parse($fromInputDateTime);

                        $toInputTime        = Carbon::createFromFormat('H:i', $request->to_time)->format('H:i');
                        $toInputDateTime    = $postDate." ".$toInputTime;
                        $toInputDateTime    = Carbon::parse($toInputDateTime);

                        $fromTime           = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i:s');
                        $fromDateTime       = $postDate." ".$fromTime;
                        $fromDateTime       = Carbon::parse($fromDateTime);

                        $toTime             = Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i:s');
                        $toDateTime         = $postDate." ".$toTime;
                        $toDateTime         = Carbon::parse($toDateTime);

                        if (($fromInputDateTime->gte($fromDateTime) && $fromInputDateTime->lte($toDateTime)) || ($toInputDateTime->gte($fromDateTime) && $toInputDateTime->lte($toDateTime)) || ($fromInputDateTime->lt($fromDateTime) && $toInputDateTime->gt($toDateTime))) {
                            return response()->json([ 'status' => 'error', 'message' => "すでにこの時間の間に他の勤務案件を確定しています"]);
                        }
                    }
                    $chemist_wage = DrugStoreWage::where('chemist_id', Auth::user()->id)->where('drug_store_id', $request->drug_store_id)->first();
                   // $hourlyWage =  Auth::user()->hourly_wage;
                    $hourlyWage =  ($chemist_wage) ? $chemist_wage->hourly_wage : 0;
                    if($hourlyWage == 0 || $hourlyWage == ''){
                        return response()->json([ 'status' => 'error', 'message' => "マイページから時給を設定をしてください"]);
                    }else{

                        $applications                       =  new ChemistJobApplication();
                        $applications->chemist_id           =  Auth::user()->id;
                        $applications->drug_store_id        =  $request->drug_store_id;
                        $applications->date                 =  $postDate;
                       // $applications->hourly_wage          =  $hourlyWage;
                        $applications->hourly_wage          =  ($chemist_wage) ? $chemist_wage->hourly_wage : 0;
                        $applications->job_post_id          =  $request->post_id;
                        $applications->time_from            =  $request->from_time;
                        $applications->time_to              =  $request->to_time;
                        $applications->rest_time            =  $request->rest_time;
                        //$applications->transportation_cost  =  Auth::user()->transportation_cost;
                        $applications->transportation_cost  =  ($chemist_wage) ? $chemist_wage->transportation_cost : 0;
                        $applications->save();
                        $notification                       = new Notification();
                        $notification->type                 = 'Drug Store';
                        $notification->message_from         = 'Not Admin';
                        $notification->drug_store_id        = $request->drug_store_id;
                        $notification->title                = "募集案件に申請があります";
                        $notification->message              = Auth::user()->last_name." ".Auth::user()->first_name."が".$postDate."の".JobPost::where('id', $request->post_id)->first()->job_name."の募集案件に申請しました";
                        $notification->save();
                        return response()->json([ 'status' => 'success', 'application_id' => $applications->id, 'message' => '申請しました' ]);
                    }
                // }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }


    public function rejectJob(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'             => 'required',
            // 'transportaion_cost'  => 'required'
        ], [
            // 'transportaion_cost.required'  => '交通費は入力が必須です'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{

                        $application                        =  ChemistJobApplication::where('id', '=', $request->application_id)->latest('id')->first();
                        $application->status                =  "Chemist Reject";
                        $application->save();

                        $postDate   =   JobPost::where('id', $application->job_post_id)->first()->date;
                        $notification                       = new Notification();
                        $notification->type                 = 'Drug Store';
                        $notification->message_from         = 'Not Admin';
                        $notification->drug_store_id        = $application->drug_store_id;
//                      $notification->title                = "化学者の求人応募が拒否されました";
                        $notification->title                = "薬剤師が承認した申請を拒否しました";
                        $notification->message              = Auth::user()->last_name." ".Auth::user()->first_name."が".$postDate."の".JobPost::where('id', $application->job_post_id)->first()->job_name."採用プロジェクトを拒否しました";
                        $notification->save();
                        return response()->json([ 'status' => 'success', 'message' => '拒否しました' ]);


            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function shiftOfThisMonth(Request $request){

        try{

            $after6months = Carbon::now()->addMonths(6);
            $today=Carbon::now();
            $getThisMonthJobs = ChemistJobApplication::select('date')
                                            // ->whereYear('date', Carbon::now()->year)
                                            // ->whereMonth('date', Carbon::now()->month)
                                            ->whereBetween('date', [$today, $after6months])
                                            ->where('chemist_id', Auth::user()->id)
                                            ->whereIn('status', ['Chemist Accept'])
                                            ->orderBy('date', 'ASC')
                                            ->get();
            $dateArray = $output = [];$i = 0;
            foreach($getThisMonthJobs as $row){
                if(!in_array($row->date,$dateArray)){
                    $dateArray[]   = $row->date;
                    $output[$i]    = $row->date;
                    $i++;
                }
            }
            return response()->json([ 'status' => 'success', 'dates' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function shiftDetails(Request $request){

        $validator  =   Validator::make($request->all(),[
            'date'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $jobDetails =   ChemistJobApplication::where('date', $request->date)
                                                        ->where('chemist_id', Auth::user()->id)
                                                        ->whereIn('status', ['Chemist Accept'])
                                                        ->orderBy('date', 'ASC')
                                                        ->get();
                $output = [];$i = 0;
                foreach($jobDetails as $row){
                    $output[$i]['work_hour']      = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i')." ~ ".Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i');
                    $output[$i]['rest_time']      = $row->rest_time;
                    $drugStore = DrugStore::where('id', $row['drug_store_id'])->first();
                    $output[$i]['image']          = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                    $output[$i]['name']           = ($drugStore->name) ? $drugStore->name : "";
                }
                return response()->json([ 'status' => 'success', 'details' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function reviewWorkPlace(Request $request){

        $validator  =   Validator::make($request->all(),[
            'job_id'                => 'required',
            'job_post_id'           => 'required',
            'drug_store_id'         => 'required',
            'atmosphere'            => 'required',
            'clerk_adaption'        => 'required',
            'pharmacist_adaption'   => 'required',
            'compliance_level'      => 'required',
        ], [
            'atmosphere.required'           => '薬局の雰囲気は入力が必須です',
            'clerk_adaption.required'       => '事務員の対応は入力が必須です',
            'pharmacist_adaption.required'  => '薬剤師の対応は入力が必須です',
            'compliance_level.required'     => 'コンプライアンス遵守の程度は入力が必須です'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if (WorkPlaceReview::where('chemist_id', Auth::user()->id)->where('job_post_id', $request->job_post_id)->exists()) {
                    return response()->json([ 'status' => 'error', 'message' => 'すでに提出が完了しています' ]);
                }else{
                    $attributes                 = $request->all();
                    unset($attributes['job_id']);
                    $attributes['chemist_id']   = $request->user()->id;
                    $attributes['created_at']   = Carbon::now();
                    WorkPlaceReview::insert($attributes);
                    ChemistJobApplication::where('id', $request->job_id)->update([ 'is_reviewed' => 1 ]);
                    return response()->json([ 'status' => 'success', 'details' => "提出しました" ]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function getAllJobPosts(Request $request){

        $validator  =   Validator::make($request->all(),[
            'date'         => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $posts  =  JobPost::join('drug_stores', 'drug_stores.id', '=', 'job_posts.drug_store_id')
                                    ->select('job_posts.id', 'drug_stores.name', 'job_posts.job_name', 'job_posts.work_from',
                                             'job_posts.work_to', 'job_posts.job_title', 'drug_stores.latitude',
                                             'drug_stores.longitude', 'drug_stores.main_image')
                                    ->where('date', $request->date)
                                    ->where('job_posts.status', 'Active')
                                    ->get();
                $output = [];$i = 0;
                foreach($posts as $row){
                    if($row->main_image){
                        $output[$i]['id']           = $row->id;
                        $output[$i]['store_name']   = $row->name;
                        $output[$i]['job_name']     = $row->job_name;
                        $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->work_from)->format('h:i A');
                        $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->work_to)->format('h:i A');
                        $output[$i]['job_title']    = $row->job_title;
                        $output[$i]['latitude']     = floatval($row->latitude);
                        $output[$i]['longitude']    = floatval($row->longitude);
                        $output[$i]['image']        = asset("/images/drug_store/".$row->main_image);
                    }else{
                        $output[$i]['id']           = $row->id;
                        $output[$i]['store_name']   = $row->name;
                        $output[$i]['job_name']     = $row->job_name;
                        $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->work_from)->format('h:i A');
                        $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->work_to)->format('h:i A');
                        $output[$i]['job_title']    = $row->job_title;
                        $output[$i]['latitude']     = floatval($row->latitude);
                        $output[$i]['longitude']    = floatval($row->longitude);
                        $output[$i]['image']        = "" ;
                    }
                    $i++;
                }
                return response()->json([ 'status' => 'success', 'drug_stores' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function chemistAcceptRejectApplication(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'  => 'required',
            'status'          => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{

                if($request->status == 'Accept')
                    $status = 'Chemist Accept';
                else
                    $status = 'Not Applied';

                $applicationRow = ChemistJobApplication::where('id', $request->application_id)->first();
                if(Carbon::now()->diffInHours($applicationRow->updated_at) >= 48 || Carbon::now()->startOfDay()->gt( Carbon::parse($applicationRow->date)->startOfDay())){
                    return response()->json([ 'status' => 'error', 'message' => 'この申請の有効期限が切れています']);
                }
                if($applicationRow->status != 'Accepted'){
                    return response()->json([ 'status' => 'error', 'message' => 'この申請の有効期限が切れています']);
                }

                $chemistId    = $applicationRow->chemist_id;
                $drugstoreID    = $applicationRow->drug_store_id;
                $postDate     = $applicationRow->date;
                if($status == 'Chemist Accept'){

                    $jobPostId          = ChemistJobApplication::where('id', $request->application_id)->first()->job_post_id;
                    $noOfVaccancies     = JobPost::where('id', $jobPostId)->first()->no_of_vaccancies;
                    $totalAccepted      = ChemistJobApplication::where('job_post_id', $jobPostId)
                                                                ->where('status', 'Chemist Accept')
                                                                ->count();
                    if($totalAccepted < $noOfVaccancies){
                        ChemistJobApplication::where('id', $request->application_id)->update(['status' => $status]);
                    }else{
                        return response()->json([ 'status' => 'error', 'message' => 'この案件は定員人数に達しました']);
                    }

                    $notification                       = new Notification();
                    $notification->type                 = 'Drug Store';
                    $notification->message_from         = 'Not Admin';
                    $notification->hyperlink            = 'Yes';
                    $notification->accept_or_cancel     = 'Accept';
                    $notification->job_application_id   = $request->application_id;
                    $notification->drug_store_id        = $applicationRow->drug_store_id;
                    $notification->job_post_id          = $jobPostId;
                    $notification->title                = "薬剤師から確定された勤務があります";
                    $notification->message              = $applicationRow->date."の".JobPost::where('id', $applicationRow->job_post_id)->first()->job_name."の勤務を".
                                                            Chemist::where('id', $applicationRow->chemist_id)->first()->last_name." ".
                                                            Chemist::where('id', $applicationRow->chemist_id)->first()->first_name.
                                                            "が確定しました";
                    $notification->save();


                    if($totalAccepted+1 >= $noOfVaccancies ){

                        $applications   =   ChemistJobApplication::where('job_post_id', $jobPostId)
                                                                   ->whereIn('status', ['Accepted', 'Pending'])
                                                                   ->get();
                        foreach($applications as $row){
                            Notification::where('job_application_id',  $row->id)->where('accept_or_cancel','!=','Cancel')->delete();
                            // $notification                = new Notification();
                            // $notification->type          = 'Chemist';
                            // $notification->message_from  = 'Not Admin';
                            // $notification->chemist_id    = $row->chemist_id;
                            // $notification->title         = "確定済みの案件のキャンセル";
                            // $notification->message       = $postDate." の勤務案件がキャンセルされました";
                            // $notification->save();

                            //commented as per jiitak team instruction

//                            $notification                = new Notification();
//                            $notification->type          = 'Drug Store';
//                            $notification->notification_type  = 'Job Application';
//                            $notification->message_from  = 'Not Admin';
//                            $notification->hyperlink            = 'Yes';
//                            $notification->accept_or_cancel     = 'Cancel';
//                            $notification->chemist_id    = $row->chemist_id;
//                            $notification->job_application_id   = $row->id;
//                            $notification->drug_store_id        = $drugstoreID;
//                            $notification->title         = "確定済みの案件のキャンセル";
//                            $notification->message       = $postDate." の勤務案件がキャンセルされました";
//                            $notification->save();

                            //commented as per jiiitak team instruction
                        }



//                        ChemistJobApplication::where('job_post_id', $jobPostId)
//                                              ->whereIn('status', ['Accepted', 'Pending'])
//                                              ->update(['status' => 'Rejected','reason_for_rejection' => '他の薬剤師が選択されました']);


                      $applications_for_rejection =  ChemistJobApplication::where('job_post_id', $jobPostId)->whereIn('status', ['Accepted', 'Pending'])->get();

                        foreach ($applications_for_rejection as $application_rejected)
                        {
                            $application_rejected->status = "Rejected";
                            $application_rejected->reason_for_rejection = "他の薬剤師が選択されました";
                            $application_rejected->save();

                            $notification                       = new Notification();
                            $notification->type                 = 'Chemist';
                            $notification->notification_type    = 'Job Application';
                            $notification->chemist_id           = $application_rejected->chemist_id;
                            $notification->job_post_id          = ChemistJobApplication::where('id', $application_rejected->id)->first()->job_post_id;
                            $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $application_rejected->id)->first()->drug_store_id)->first();
                            $notification->image                = $drugStore->main_image;
                            $notification->drug_store_address   = $drugStore->address;
                            $notification->drug_store_model     = $drugStore->history_model;
                            $notification->job_application_id   = $application_rejected->id;
                            //$notification->title                = '勤務申請が拒否されました';
                            $notification->title                = '勤務申請が成立しませんでした';
                            //if($application->reason_for_rejection == "Others") $reason = $application->reason_for_rejection_other; else $reason = $application->reason_for_rejection;
                            $reason ="";
                            $notification->message              = $application_rejected->date . 'の勤務申請が薬局/病院に受付けられませんでした '.$reason;
                            $notification->save();
                            $deviceToken        =   Chemist::where('id', $notification->chemist_id)->first()->device_token;
                            $this->sendNotification('勤務申請が成立しませんでした', ChemistJobApplication::where('id', $application_rejected->id)->first()->date . 'の勤務申請が薬局/病院に受付けられませんでした '.$reason, $deviceToken, NULL, $notification->chemist_id);


                        }


                    }

                    $getChemistAccepetedJobs = ChemistJobApplication::select('id', 'drug_store_id', 'time_from', 'time_to', 'job_post_id')
                                                                        ->where('chemist_id', $chemistId)
                                                                        ->where('date', $postDate)
                                                                        ->whereIn('status', ['Accepted', 'Pending'])
                                                                        ->get();
                    foreach($getChemistAccepetedJobs as $row){

                        $postDate           = Carbon::createFromFormat('Y-m-d', $postDate)->format('Y-m-d');

                        $fromInputTime      = Carbon::createFromFormat('H:i:s', $applicationRow ->time_from)->format('H:i:s');
                        $fromInputDateTime  = $postDate." ".$fromInputTime;
                        $fromInputDateTime  = Carbon::parse($fromInputDateTime);

                        $toInputTime        = Carbon::createFromFormat('H:i:s', $applicationRow ->time_to)->format('H:i:s');
                        $toInputDateTime    = $postDate." ".$toInputTime;
                        $toInputDateTime    = Carbon::parse($toInputDateTime);

                        $fromTime           = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i:s');
                        $fromDateTime       = $postDate." ".$fromTime;
                        $fromDateTime       = Carbon::parse($fromDateTime);

                        $toTime             = Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i:s');
                        $toDateTime         = $postDate." ".$toTime;
                        $toDateTime         = Carbon::parse($toDateTime);

                        if (($fromInputDateTime->gte($fromDateTime) && $fromInputDateTime->lte($toDateTime)) || ($toInputDateTime->gte($fromDateTime) && $toInputDateTime->lte($toDateTime)) || ($fromInputDateTime->lt($fromDateTime) && $toInputDateTime->gt($toDateTime))) {
                            Notification::where('job_application_id', $row->id)->delete();
                            ChemistJobApplication::where('id', $row->id)->update(['status' => 'Rejected']);
                            $notification                = new Notification();
                            $notification->type          = 'Drug Store';
                            $notification->message_from  = 'Not Admin';
                            $notification->drug_store_id = $row->drug_store_id;
                            //$notification->title         = "薬剤師が募集案件を受け付けませんでした";
                            $notification->title         = "薬剤師が承認した申請を拒否しました";
                            $notification->message       = $postDate." の ".JobPost::where('id', $row->job_post_id)->first()->job_name." の募集案件は ".Chemist::where('id', $chemistId)->first()->last_name." ".Chemist::where('id', $chemistId)->first()->first_name." が他の案件を確定させたため自動拒否されました";
                            $notification->save();
                        }
                    }
                }else{
                    ChemistJobApplication::where('id', $request->application_id)->update(['status' => $status, 'reason_for_rejection' => $request->reason]);
                    $notification                = new Notification();
                    $notification->type          = 'Drug Store';
                    $notification->message_from  = 'Not Admin';
                    $notification->drug_store_id = $applicationRow->drug_store_id;
                   // $notification->title         = "薬剤師が募集案件を受け付けませんでした";
                    $notification->title         = "薬剤師が承認した申請を拒否しました";
                    $notification->message       = Chemist::where('id', $applicationRow->chemist_id)->first()->last_name." ".
                                                   Chemist::where('id', $applicationRow->chemist_id)->first()->first_name."が".
                                                   $applicationRow->date."の募集案件".
                                                   JobPost::where('id', $applicationRow->job_post_id)->first()->job_name."を受け付けませんでした";
                    $notification->job_application_id   = $request->application_id;
                    $notification->save();
                }
                return response()->json([ 'status' => 'success', 'message' => '更新しました' ]);

            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function workContentConfirmation(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'        => 'required',
            'work_from'             => 'required',
            'work_to'               => 'required',
            'rest_time'             => 'required',
            'transportation_cost'   => 'required',
            'late_arrived'          => 'required',
            'extra_work'            => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $applicationRow = ChemistJobApplication::where('id', $request->application_id)->first();
                if($applicationRow->status != 'Chemist Accept'){
                    Notification::where('job_application_id', $request->applicationId)->delete();
                    return response()->json([ 'status' => 'error', 'message' => 'Notification expired']);
                }
                ChemistJobApplication::where('id', $request->application_id)
                                        ->update(['actual_time_from'           => $request->work_from,
                                                  'actual_time_to'             => $request->work_to,
                                                  'actual_rest_time'           => $request->rest_time,
                                                  'actual_transportation_cost' => $request->transportation_cost,
                                                  'late_arrived'               => $request->late_arrived,
                                                  'extra_work'                 => $request->extra_work,
                                                  'status'                     => 'Job Completed'
                                                ]);
                return response()->json([ 'status' => 'success', 'message' => '更新しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => $e->getMessage() ]);
            }
        }
    }

    public function pendingReviews(Request $request){

//        try{
//            $reviewPending = ChemistJobApplication::select('id', 'job_post_id', 'drug_store_id', 'date', 'actual_time_from', 'actual_time_to')
//                                                    ->where('status', 'Job Completed')
//                                                    ->where('is_reviewed', 0)
//                                                    ->where('chemist_id', $request->user()->id)
//                                                    ->get();
//            $output = []; $i = 0;
//            foreach($reviewPending as $row){
//                $output[$i]['drug_store_id']    = $row->drug_store_id;
//                $drugStoreName = DrugStore::where('id', $row->drug_store_id)->first()->name;
//                $output[$i]['drug_store_name']  = $drugStoreName;
//                $output[$i]['job_id']           = $row->id;
//                $output[$i]['job_post_id']      = $row->job_post_id;
//                $output[$i]['time_from']        = date('H:i', strtotime($row->actual_time_from));
//                $output[$i]['time_to']          = date('H:i', strtotime($row->actual_time_to));
//                $output[$i]['date']             = $row->date;
//                $i++;
//            }
//            return response()->json([ 'status' => 'success', 'pending_drug_stores' => $output ]);
//        } catch(\Exception $e){
//            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
//        }


        $validator  =   Validator::make($request->all(),[
            'application_id'        => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else {
            try {
                $reviewPending = ChemistJobApplication::select('id', 'job_post_id', 'drug_store_id', 'date', 'actual_time_from', 'actual_time_to')
                    ->where('status', 'Job Completed')
                    ->where('is_reviewed', 0)
                    ->where('chemist_id', $request->user()->id)
                    ->where('id', $request->application_id)
                    ->get();
                $output = [];
                $i = 0;
                foreach ($reviewPending as $row) {
                    $output[$i]['drug_store_id'] = $row->drug_store_id;
                    $drugStoreName = DrugStore::where('id', $row->drug_store_id)->first()->name;
                    $output[$i]['drug_store_name'] = $drugStoreName;
                    $output[$i]['job_id'] = $row->id;
                    $output[$i]['job_post_id'] = $row->job_post_id;
                    $output[$i]['time_from'] = date('H:i', strtotime($row->actual_time_from));
                    $output[$i]['time_to'] = date('H:i', strtotime($row->actual_time_to));
                    $output[$i]['date'] = $row->date;
                    $i++;
                }
                return response()->json(['status' => 'success', 'pending_drug_stores' => $output]);
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getJobContent(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'  => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $application    =   ChemistJobApplication::where('id', $request->application_id)->first();
                $output = [];
                if($application){
                    if($application->actual_time_from){
                        $output['time_from']            = date('H:i',strtotime($application->actual_time_from));
                        $output['time_to']              = date('H:i',strtotime($application->actual_time_to));
                        $output['rest_time']            = $application->actual_rest_time;
                        $output['transportation_cost']  = $application->transportation_cost;
                        $output['post_date']            = $application->date;
                    }else{
                        $output['time_from']            = date('H:i',strtotime($application->time_from));
                        $output['time_to']              = date('H:i',strtotime($application->time_to));
                        $output['rest_time']            = $application->rest_time;
                        $output['transportation_cost']  = $application->transportation_cost;
                        $output['post_date']            = $application->date;
                    }
                }
                return response()->json([ 'status' => 'success', 'data' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function cancelJobApplication(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'  => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $application    =   ChemistJobApplication::where('id', $request->application_id)
                                                           ->update(['status'            => 'Chemist Reject',
                                                                     'chemist_cancelled' => 'Yes'
                                                                   ]);
                $drugStore      =   DrugStore::where('id', ChemistJobApplication::where('id', $request->application_id)->first()->drug_store_id)->first();
                $chemist        =   Chemist::where('id', ChemistJobApplication::where('id', $request->application_id)->first()->chemist_id)->first();
                $jobPost        =   JobPost::where('id', ChemistJobApplication::where('id', $request->application_id)->first()->job_post_id)->first();
                $notification                           =   new Notification();
                $notification->type                     =   'Drug Store';
                $notification->drug_Store_id            =   $drugStore->id;
                $notification->message_from             =   'Not Admin';
                $notification->hyperlink                =   'Yes';
                $notification->accept_or_cancel         =   'Cancel';
                $notification->job_application_id       =   $request->application_id;
                $notification->title                    =   '確定済みの案件がキャンセルされました';
                $notification->message                  =   $chemist->last_name." ".$chemist->first_name."が".ChemistJobApplication::where('id', $request->application_id)->first()->date.
                                                            "の確定済みの案件".$jobPost->job_name."をキャンセルしました";
                $notification->save();
                return response()->json([ 'status' => 'success', 'message' => "確定した募集申請をキャンセルしました" ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function getJobHistory(Request $request){

        try{
            $historyDates   =   DB::table('chemist_job_applications')
                                    ->select('date')
                                    ->whereDate('date', '<', Carbon::now())
                                    ->where('chemist_id', $request->user()->id)
                                    ->where('status', 'Job Completed')
                                    ->where('deleted_at', Null)
                                    ->groupBy('date')
                                    ->get();
            $i = 0;$output = [];
            foreach($historyDates as $row){
                $output[$i]    =   $row->date;
                $i++;
            }
            return response()->json([ 'status' => 'success', 'dates' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
        }
    }

    public function getJobsOfPerticularDate(Request $request){

        $validator  =   Validator::make($request->all(),[
            'date'  => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $applications   =   ChemistJobApplication::where('chemist_id', $request->user()->id)
                                                        ->where('date', $request->date)
                                                        ->where('status', 'Job Completed')
                                                        ->get();
                $i = 0;$output = [];
                foreach($applications as $row){
                    $jobPost = JobPost::where('id', $row->job_post_id)->first();
                    $output[$i]['id']           = $row->job_post_id;
                    $output[$i]['store_name']   = DrugStore::where('id', $row->drug_store_id)->first()->name;
                    $output[$i]['job_name']     = $jobPost->job_name;
                    $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->actual_time_from)->format('H:i');
                    $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->actual_time_to)->format('H:i');
                    $output[$i]['job_title']    = $jobPost->job_title;
                    $output[$i]['latitude']     = DrugStore::where('id', $row->drug_store_id)->first()->latitude;
                    $output[$i]['longitude']    = DrugStore::where('id', $row->drug_store_id)->first()->longitude;
                    $mainImage  =   DrugStore::where('id', $row->drug_store_id)->first()->main_image;
                    $output[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
                    $i++;
                }
                return response()->json([ 'status' => 'success', 'dates' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function shiftOfThisMonthJobHistory(Request $request){

        try{
            $after6months = date('Y-m-d', strtotime(Carbon::now()->addMonths(6)));
            $today = date('Y-m-d', strtotime(Carbon::now()));

            $getThisMonthJobs = ChemistJobApplication::select('date')
                // ->whereYear('date', Carbon::now()->year)
                // ->whereMonth('date', Carbon::now()->month)
               ->whereBetween('date', [$today, $after6months])
                ->where('chemist_id', Auth::user()->id)
                ->whereIn('status', ['Chemist Accept'])
                ->orderBy('date', 'ASC')
                ->get();
            $dateArray = $output1 = [];$i = 0;
            foreach($getThisMonthJobs as $row){
                if(!in_array($row->date,$dateArray)){
                    $dateArray[]   = $row->date;
                    $output1[$i]    = $row->date;
                    $i++;
                }
            }


            $historyDates   =   DB::table('chemist_job_applications')
                ->select('date')
                ->whereDate('date', '<=', $today)
                ->where('chemist_id', Auth::user()->id)
                ->where('status', 'Job Completed')
                ->where('deleted_at', Null)
                ->groupBy('date')
                ->get();
            $j = 0;$output2 = [];
            foreach($historyDates as $row){
                $output2[$j]    =   $row->date;
                $j++;
            }


            $output = array_merge($output1,$output2);
            $output = array_unique($output);

            return response()->json([ 'status' => 'success', 'dates' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }

    }

    public function shiftHistoryDetails(Request $request){

        $validator  =   Validator::make($request->all(),[
            'date'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{

                $date = date("Y-m-d", strtotime($request->date));
                $current_date = date("Y-m-d", strtotime(Carbon::now()));

                $jobDetails =   ChemistJobApplication::where('date', $request->date)
                    ->where('chemist_id', Auth::user()->id)
                    ->whereIn('status', ['Chemist Accept', 'Job Completed'])
                    ->orderBy('date', 'ASC')
                    ->get();
                $output = [];$i = 0;
                foreach($jobDetails as $row)
                {
                    if($row->status=="Chemist Accept")
                    {
                        $status_flag = "future";
                        $output[$i]['work_hour']      = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i')." ~ ".Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i');
                        $output[$i]['rest_time']      = $row->rest_time;
                        $drugStore = DrugStore::where('id', $row['drug_store_id'])->first();
                        $output[$i]['image']          = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                        $output[$i]['name']           = ($drugStore->name) ? $drugStore->name : "";
                        $output[$i]['status'] = $status_flag;
                    }
                    else
                    {
                        $status_flag = "history";
                        $jobPost = JobPost::where('id', $row->job_post_id)->first();
                        $output[$i]['id']           = $row->job_post_id;
                        $output[$i]['store_name']   = DrugStore::where('id', $row->drug_store_id)->first()->name;
                        $output[$i]['job_name']     = $jobPost->job_name;
                        $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->actual_time_from)->format('H:i');
                        $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->actual_time_to)->format('H:i');
                        $output[$i]['job_title']    = $jobPost->job_title;
                        $output[$i]['latitude']     = DrugStore::where('id', $row->drug_store_id)->first()->latitude;
                        $output[$i]['longitude']    = DrugStore::where('id', $row->drug_store_id)->first()->longitude;
                        $mainImage  =   DrugStore::where('id', $row->drug_store_id)->first()->main_image;
                        $output[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
                        $output[$i]['status'] = $status_flag;
                    }

                    $i++;
                }


//                if(strtotime($date) >= strtotime($current_date))
//                {
//                    $jobDetails =   ChemistJobApplication::where('date', $request->date)
//                        ->where('chemist_id', Auth::user()->id)
//                        ->whereIn('status', ['Chemist Accept'])
//                        ->orderBy('date', 'ASC')
//                        ->get();
//                    $status_flag = "future";
//
//                    $output = [];$i = 0;
//                    foreach($jobDetails as $row){
//                        $output[$i]['work_hour']      = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i')." ~ ".Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i');
//                        $output[$i]['rest_time']      = $row->rest_time;
//                        $drugStore = DrugStore::where('id', $row['drug_store_id'])->first();
//                        $output[$i]['image']          = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
//                        $output[$i]['name']           = ($drugStore->name) ? $drugStore->name : "";
//                        $output[$i]['status'] = $status_flag;
//                        $i++;
//                    }
//
//                }
//                else
//                {
//                    $applications   =   ChemistJobApplication::where('chemist_id', Auth::user()->id)
//                        ->where('date', $request->date)
//                        ->where('status', 'Job Completed')
//                        ->get();
//                    $status_flag = "history";
//                    $i = 0;$output = [];
//                    foreach($applications as $row){
//                        $jobPost = JobPost::where('id', $row->job_post_id)->first();
//                        $output[$i]['id']           = $row->job_post_id;
//                        $output[$i]['store_name']   = DrugStore::where('id', $row->drug_store_id)->first()->name;
//                        $output[$i]['job_name']     = $jobPost->job_name;
//                        $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->actual_time_from)->format('H:i');
//                        $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->actual_time_to)->format('H:i');
//                        $output[$i]['job_title']    = $jobPost->job_title;
//                        $output[$i]['latitude']     = DrugStore::where('id', $row->drug_store_id)->first()->latitude;
//                        $output[$i]['longitude']    = DrugStore::where('id', $row->drug_store_id)->first()->longitude;
//                        $mainImage  =   DrugStore::where('id', $row->drug_store_id)->first()->main_image;
//                        $output[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
//                        $output[$i]['status'] = $status_flag;
//                        $i++;
//                    }
//                }

                return response()->json([ 'status' => 'success', 'details' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません', 'error' => $e->getMessage()]);
            }
        }
    }



    public function updateActualTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'application_id'    => 'required',
            'actual_from_time'  => 'required',
            'actual_to_time'    => 'required',
            'actual_rest_time'  => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                ChemistJobApplication::where('id', $request->application_id)
                                       ->update([ 'actual_time_from' => $request->actual_from_time,
                                                  'actual_time_to'   => $request->actual_to_time,
                                                  'actual_rest_time' => $request->actual_rest_time
                                               ]);
                return response()->json([ 'status' => 'success', 'dates' => '更新しました' ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function applyinspectionTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'day'           => 'required',
            'time_from'     => 'required',
            'post_id'       => 'required',
            'drug_store_id' => 'required',
            'time_to'       => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                if (ChemistInspectionTiming::where('chemist_id', Auth::user()->id)->where('drug_store_id', $request->drug_store_id)->exists()) {
                    return response()->json([ 'status' => 'success', 'message' => 'Already Applied' ]);
                }else{
                    $timing                =  new ChemistInspectionTiming;
                    $timing->chemist_id    =  Auth::user()->id;
                    $timing->drug_store_id =  $request->drug_store_id;
                    $timing->job_post_id   =  $request->post_id;
                    $timing->time_from     =  $request->time_from;
                    $timing->time_to       =  $request->time_to;
                    $timing->day           =  $request->day;
                    $timing->save();
                    return response()->json([ 'status' => 'success', 'message' => '申請しました' ]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
            }
        }
    }

    public function getJobDaysByFilter(Request $request){

        $validator  =   Validator::make($request->all(),[
            'latitude'   => 'required',
            'longitude'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{

            try{
                $today  =  Carbon::now()->toDateString();
                if($request->latitude != '' && $request->longitude != ''){
                    $posts  =  DB::select("SELECT job_posts.id, job_posts.drug_store_id, `date`,  SQRT(
                                        POW(69.1 * (latitude - $request->latitude), 2) +
                                        POW(69.1 * ($request->longitude - longitude) * COS(latitude / 57.3), 2)) AS distance
                                        FROM `drug_stores` INNER JOIN `job_posts` ON
                                        job_posts.drug_store_id = `drug_stores`.id WHERE `date` >= '$today' AND job_posts.status = 'Active' HAVING distance < 100  ORDER BY distance");
                    $output = [];$i = 0;$drugStoreIds = []; $dateArray = [];
                    foreach($posts as $row){
                        if($this->checkIsVaccancy($row->id)){
                            if(!in_array($row->date, $dateArray)){
                                $dateArray[] = $row->date;
                            }
                        }
                    }
                }
                $result = $dateArray;
                $i = 0;$drugStoreIds = [];$output1 = [];$favouriteDateArray = [];
                if($request->favourite != ''){
                    $favouriteJobPosts      =  DB::select("SELECT job_posts.id, job_posts.drug_store_id, `date`, SQRT(
                                                            POW(69.1 * (latitude - $request->latitude), 2) +
                                                            POW(69.1 * ($request->longitude - longitude) * COS(latitude / 57.3), 2)) AS distance
                                                            FROM `drug_stores` INNER JOIN `job_posts` ON
                                                            job_posts.drug_store_id = `drug_stores`.id
                                                            INNER JOIN `favourites` ON
                                                            job_posts.drug_store_id = `favourites`.drug_store_id
                                                            WHERE `date` >= '$today' AND job_posts.status = 'Active' AND job_posts.deleted_at IS NULL AND favourites.chemist_id =". Auth::user()->id." AND
                                                            favourites.status = '1' AND favourites.chemist_id =". Auth::user()->id." AND favourites.drug_store_id = ".$request->favourite." HAVING distance < 100  ORDER BY distance");
                    foreach($favouriteJobPosts as $row){
                        if($this->checkIsVaccancy($row->id)){
                            if(!in_array($row->date, $favouriteDateArray)){
                                $favouriteDateArray[] = $row->date;
                            }
                        }
                    }
                    $result = $favouriteDateArray;
                }

                if($request->interviewed == 1){

                    $interviewedJobPosts    =   DB::select("SELECT job_posts.id, job_posts.drug_store_id, `date`, SQRT(
                                                    POW(69.1 * (latitude - $request->latitude), 2) +
                                                    POW(69.1 * ($request->longitude - longitude) * COS(latitude / 57.3), 2)) AS distance
                                                    FROM `drug_stores` INNER JOIN `job_posts` ON
                                                    job_posts.drug_store_id = `drug_stores`.id
                                                    INNER JOIN `chemist_interview_timings` ON
                                                    job_posts.drug_store_id = `chemist_interview_timings`.drug_store_id
                                                    WHERE `date` >= '$today' AND job_posts.status = 'Active'  AND chemist_interview_timings.chemist_id =". Auth::user()->id." AND
                                                    chemist_interview_timings.status = 'Approved' HAVING distance < 100  ORDER BY distance");
                    $interviewedDateArray  = [];
                    foreach($interviewedJobPosts as $row){
                        if($this->checkIsVaccancy($row->id)){
                            if(!in_array($row->date, $interviewedDateArray)){
                                $interviewedDateArray[] = $row->date;
                            }
                        }
                    }
                    $result = array_values(array_unique(array_merge($favouriteDateArray, $interviewedDateArray)));
                }
                return response()->json([ 'status' => 'success', 'dates' => $result ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }

    public function getJobContentByQrCode(Request $request){

        $validator  =   Validator::make($request->all(),[
            'drug_store_id'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $application    =   ChemistJobApplication::where('date', Carbon::now()->toDateString())
                                                           ->where('drug_Store_id', $request->drug_store_id)
                                                           ->where('chemist_id', Auth::user()->id)
                                                           ->where('status', 'Chemist Accept')
                                                           ->orderBy('time_from', 'ASC')
                                                           ->first();
                $output = [];
                if($application){
                    $drugStore      = DrugStore::find($request->drug_store_id);
                    $notification   = Notification::select('id')
                                                    ->where('chemist_id', Auth::user()->id)
                                                    ->where('job_application_id', $application->id)
                                                    ->first();
                    if($application->actual_time_from){
                        $output['time_from']            = date('H:i',strtotime($application->actual_time_from));
                        $output['time_to']              = date('H:i',strtotime($application->actual_time_to));
                        $output['rest_time']            = $application->actual_rest_time;
                        $output['transportation_cost']  = $application->transportation_cost;
                        $output['drugstore_name']       = $drugStore->name;
                        $output['post_date']            = $application->date;
                        $output['notification_id']      = $notification->id;
                        $output['application_id']       = $application->id;
                    }else{
                        $output['time_from']            = date('H:i',strtotime($application->time_from));
                        $output['time_to']              = date('H:i',strtotime($application->time_to));
                        $output['rest_time']            = $application->rest_time;
                        $output['transportation_cost']  = $application->transportation_cost;
                        $output['drugstore_name']       = $drugStore->name;
                        $output['post_date']            = $application->date;
                        $output['notification_id']      = $notification->id;
                        $output['application_id']       = $application->id;
                    }
                    return response()->json([ 'status' => 'success', 'data' => $output ]);
                }else{
                    return response()->json([ 'status' => 'error', 'message' => 'No pending jobs are here confirm' ]);
                }

            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function getFavouriteList(Request $request){

        try{
            $favourites =   Favourite::select('drug_store_id')
                                       ->where('chemist_id', Auth::user()->id)
                                       ->where('status', 1)
                                       ->get();
            $output = [];
            $i = 0;
            foreach($favourites as $row){
                $drugStore = DrugStore::find($row->drug_store_id);
                $output[$i]['drugstore_id']    = $row->drug_store_id;
                $output[$i]['drugstore_name']  = $drugStore->name;
                $output[$i]['address']         = $drugStore->address;
                $output[$i]['image']           = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                $i++;
            }
            return response()->json([ 'status' => 'success', 'data' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
        }
    }


    public function getFavouriteListFilter(Request $request){

        try{
            $favourites =   Favourite::select('drug_store_id')
                                       ->where('chemist_id', Auth::user()->id)
                                       ->where('status', 1)
                                       ->get();
            $output = [];
            $i = 0;
                $output[$i]['drugstore_id']    = 0;
                $output[$i]['drugstore_name']  = "指定しない";
                $output[$i]['address']         = "";
                $output[$i]['image']           = "";
            $i = 1;
            foreach($favourites as $row){
                $drugStore = DrugStore::find($row->drug_store_id);
                $output[$i]['drugstore_id']    = $row->drug_store_id;
                $output[$i]['drugstore_name']  = $drugStore->name;
                $output[$i]['address']         = $drugStore->address;
                $output[$i]['image']           = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                $i++;
            }
            return response()->json([ 'status' => 'success', 'data' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
        }
    }


    // public function getDrugstorePostCount(Request $req) {
    //     try{
    //         $urrent_date = Carbon::now()->toDateString();
    //         $jobPosts = JobPost::where('drug_store_id', $req->drug_store_id)
    //             ->whereDate('date', '>=', Carbon::now()->toDateString())
    //             ->selectRaw('drug_store_id, DATE_FORMAT(created_at, "%Y-%m-%d") as formatted_date, count(*) as count')
    //             // ->when($current_date == 'likes', function ($q) {
    //             //     return $q->where('likes', '>', request('likes_amount', 0));
    //             // });
    //             ->groupBy('formatted_date')
    //             ->orderBy('formatted_date', 'DESC')
    //             ->get();
    //         $output = [];
    //         $i = 0;
    //         // Auth::user()->id
    //         foreach ($jobPosts as $key => $value) {
    //             $read_status = ReadStatus::
    //                 where('chemist_id', 252)
    //                 // where('chemist_id', Auth::user()->id)
    //                 ->where('drug_store_id', $value->drug_store_id)
    //                 ->whereDate('date', $value->formatted_date)
    //                 ->where('read_status', false)
    //                 ->count();
    //             $output[$i]['drug_store_id'] = $value->drug_store_id;
    //             $output[$i]['date'] = $value->formatted_date;
    //             $output[$i]['count'] = $value->count;
    //             $output[$i]['is_new'] = ($read_status > 0) ? true : false;
    //             $i++;
    //         }
    //         return response()->json(['status' => 'success', 'data' => $output]);
    //     } catch(\Exception $e){
    //         return response()->json([ 'status' => 'error', 'message' => $e->getMessage() ]);
    //     }
    // }


    public function getDrugstorePostCount(Request $req) {
        try{
            $current_date = Carbon::now()->toDateTimeString();
            $jobPosts = JobPost::where('drug_store_id', $req->drug_store_id)
                ->whereDate('date', '>=', Carbon::now()->toDateString())
                ->where(DB::raw("CONCAT(date,' ',work_to)"), '>', $current_date)
                ->selectRaw('drug_store_id, date, work_to, DATE_FORMAT(created_at, "%Y-%m-%d") as formatted_date, count(*) as count')
                ->groupBy('formatted_date')
                ->orderBy('formatted_date', 'DESC')
                ->get();
            $output = [];
            $i = 0;
            // Auth::user()->id
            foreach ($jobPosts as $key => $value) {
                $read_status = ReadStatus::
                    where('chemist_id', 252)
                    // where('chemist_id', Auth::user()->id)
                    ->where('drug_store_id', $value->drug_store_id)
                    ->whereDate('date', $value->formatted_date)
                    ->where('read_status', false)
                    ->count();
                $output[$i]['drug_store_id'] = $value->drug_store_id;
                $output[$i]['date'] = $value->formatted_date;
                $output[$i]['count'] = $value->count;
                $output[$i]['is_new'] = ($read_status > 0) ? true : false;
                $i++;
            }
            return response()->json(['status' => 'success', 'data' => $output]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => $e->getMessage() ]);
        }
    }


    public function getDrugStorePosts(Request $req) {
        try {
            $current_date = Carbon::now()->toDateTimeString();
            ReadStatus::
                where('chemist_id', 252)
                // where('chemist_id', Auth::user()->id)
                ->where('drug_store_id', $req->id)
                ->whereDate('date', $req->date)
                ->update([
                    'read_status' => true
                ]);

            $daysJapan = ['Sunday' => '日', 'Monday' => '月', 'Tuesday' => '火', 'Wednesday' => '水', 'Thursday' => '木', 'Friday' => '金', 'Saturday' => '土' ];
            $jobPosts = JobPost::
                where('drug_store_id', $req->id)
                ->whereDate('date', '>=', Carbon::now()->toDateString())
                ->where(DB::raw("CONCAT(date,' ',work_to)"), '>', $current_date)
                ->whereBetween('created_at', [$req->date.' 00:00:00',$req->date.' 23:59:59'])
                ->select('id as job_id', 'drug_store_id', 'job_name', 'work_from as from', 'work_to as to', 'date', 'created_at')
                ->get();
            $output = [];
            $i = 0;
            foreach ($jobPosts as $key => $value) {
                $from = explode(":", $value->from);
                $to = explode(":", $value->to);
                $output[$i]["job_id"] = $value->job_id;
                $output[$i]["drug_store_id"] = $value->drug_store_id;
                $output[$i]["job_name"] = $value->job_name;
                $output[$i]["from"] = $from[0] . ":" . $from[1];
                $output[$i]["to"] = $to[0] . ":" . $to[1];
                $output[$i]["date"] = $value->date;
                foreach ($daysJapan as $dayeng => $day) {
                    if($dayeng == Carbon::createFromFormat('Y-m-d', $value->date)->format('l')) {
                        $output[$i]['day'] = $day;
                    }
                }
                $i++;
            }
            // print_r($output);exit;
            return response()->json(['status' => 'success', 'data' => $output]);
        } catch(\Exception $e) {
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません' ]);
        }
    }


    public function testPush(Request $request)
    {
        $deviceToken = Chemist::where('id', $request->id)->first()->device_token;
        $phone = Chemist::where('id', $request->id)->first()->device_token;
        $this->sendNotification('Test Push Notification', 'Test Push Notification to '.$phone, $deviceToken, NULL, $request->id);

        return "Success";
    }


    public function sendNotification($title, $message, $deviceToken, $data = null,$chemistId){

        $badgeCount = $this->getNotificationCount($chemistId);
        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                'badge' => $badgeCount,
                "show_in_foreground" => true,
                "targetScreen" => 'detail',
                "type" => 'FAV',
                "drug_store_id" => Auth::id()
            ),
            "data"=> $data,
            "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }

    public function getNotificationCount($chemistId){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                ->where('chemist_id', $chemistId)
                ->orderBy('id', 'DESC')
                ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                $output[$i]['notification_id']    = $row->id;
                $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                if($row->job_post_id != ''){
                    $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                    $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                    if($row->job_application_id != ''){
                        $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                        $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                        $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                        $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                        $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                        $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                    }else{
                        $diffHours                    = 0;
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                } else{
                    $diffHours                    = 0;
                    $output[$i]['drug_store_id']  = "";
                    $output[$i]['drug_store_name']= "";
                    $output[$i]['job_title']      = "";
                    $output[$i]['post_date']      = "";
                    $output[$i]['time_from']      = "";
                    $output[$i]['time_to']        = "";
                    $output[$i]['rest_time']      = "";
                }
                if($output[$i]['type'] == 'Job Application'){
                    $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                    $applicationStatus  = ($application) ? $application->status : "";
                }else{
                    $applicationStatus  = "";
                }
                if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                    $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                }else{
                    $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                }
                $output[$i]['image']              = $image;
                $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                $output[$i]['application_status'] = $applicationStatus;
                $output[$i]['title']              = $row->title;
                $output[$i]['message']            = $row->message;
                $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                $isIncrement = 0;
                if($applicationStatus == 'Accepted'){
                    if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }
                }
                if($applicationStatus == 'Job Completed'){
                    unset($output[$i]);
                    Notification::where('id',$row->id)->delete();
                    $isIncrement = 1;
                }

                if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                    unset($output[$i]);
                    Notification::where('id',$row->id)->delete();
                    if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                        ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                    }
                    $isIncrement = 1;
                }
                if($isIncrement == 0){
                    $i++;
                }
            }
            $adminChatCount     =  AdminChemistChatHead::
            join('admin_chemist_chats', 'admin_chemist_chats.admin_chemist_chat_head_id', '=', 'admin_chemist_chat_heads.id')
                ->select('admin_chemist_chats.admin_chemist_chat_head_id')
                ->where('admin_chemist_chats.chemist_read_status', 0)
                ->where('chemist_id', $chemistId)
                ->groupBy('admin_chemist_chats.admin_chemist_chat_head_id')
                ->get()
                ->count();
            $storeChatCount     =  StoreChemistChatHead::
            join('store_chemist_chats', 'store_chemist_chats.store_chemist_chat_head_id', '=', 'store_chemist_chat_heads.id')
                ->select('store_chemist_chats.store_chemist_chat_head_id')
                ->where('store_chemist_chats.chemist_read_status', 0)
                ->where('chemist_id', $chemistId)
                ->groupBy('store_chemist_chats.store_chemist_chat_head_id')
                ->get()
                ->count();
            $groupChatCount     =  GroupChatHead::
            join('group_chats', 'group_chats.group_chat_head_id', '=', 'group_chat_heads.id')
                ->select('group_chats.group_chat_head_id')
                ->where('group_chats.chemist_read_status', 0)
                ->where('chemist_id', $chemistId)
                ->groupBy('group_chats.group_chat_head_id')
                ->get()
                ->count();
            return count($output) + $storeChatCount + $adminChatCount + $groupChatCount;
        } catch(\Exception $e){
            return 0;
        }
    }


}
