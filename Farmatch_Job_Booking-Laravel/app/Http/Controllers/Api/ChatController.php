<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\GroupChat;
use App\GroupChatHead;
use App\StoreChemistChat;
use App\AdminChemistChat;
use Illuminate\Http\Request;
use App\AdminChemistChatHead;
use App\StoreChemistChatHead;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;


class ChatController extends Controller
{
    public function groupChatHeads(Request $request)
    {

        $chatheads = GroupChatHead::where('chemist_id', Auth::user()->id)->orderBy('updated_at', 'desc')->get();
        foreach ($chatheads as $head) {
            $head->chat_count = GroupChat::where('group_chat_head_id', $head->id)->where('chemist_read_status', 0)->count();
            $head->store = $head->store;
        }

        $groupchats = \App\GroupChatHead::where('chemist_id', Auth::user()->id)
            ->whereHas('chats', function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('chemist_read_status', 0);
            })->count();

        $admin_image = User::first()->image;
        if ($admin_image) {
            $admin_image = url('/') . "/images/profile/" . $admin_image;
        }

        return response()->json([
            'status' => true,
            'admin_image' => $admin_image,
            'data' => $chatheads,
            'count' => $groupchats
        ]);
    }

    public function storeChatHeads(Request $request)
    {
        $search = $request->search;
        $chatheads = StoreChemistChatHead::where('chemist_id', Auth::user()->id)->orderBy('updated_at', 'desc')->get();

        foreach ($chatheads as $head) {
            $head->chat_count = StoreChemistChat::where('store_chemist_chat_head_id', $head->id)->where('chemist_read_status', 0)->count();
            $head->store = $head->store;
        }

        $storechats = \App\StoreChemistChatHead::where('chemist_id', Auth::user()->id)
            ->whereHas('chats', function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('chemist_read_status', 0);
            })->count();

        $admin_image = User::first()->image;
        if ($admin_image) {
            $admin_image = url('/') . "/images/profile/" . $admin_image;
        }

        $data = [
            'chatheads' => $chatheads,
            'search' => $search,
            'count' => $storechats,
            'admin_image' => $admin_image
        ];
        return response()->json($data);
    }

    public function adminChatHead(Request $request)
    {
        $chathead = AdminChemistChatHead::where('chemist_id', Auth::user()->id)->orderBy('updated_at', 'desc')->first();
        $count = 0;
        $new_chat = 1;
        if ($chathead) {
            $new_chat = 0;
            $count = $chathead->chat_count = AdminChemistChat::where('admin_chemist_chat_head_id', $chathead->id)->where('chemist_read_status', 0)->count();
        }

        $admin_image = User::first()->image;
        if ($admin_image) {
            $admin_image = url('/') . "/images/profile/" . $admin_image;
        }

        $data = [
            'chathead' => $chathead,
            'count' => $count,
            'new_chat' => $new_chat,
            'admin_image' => $admin_image
        ];
        return response()->json($data);
    }

    public function chatHeadCount(Request $request)
    {
        $storechats = \App\StoreChemistChatHead::where('chemist_id', Auth::user()->id)
            ->whereHas('chats', function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('chemist_read_status', 0);
            })->count();

        $groupchats = \App\GroupChatHead::where('chemist_id', Auth::user()->id)
            ->whereHas('chats', function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('chemist_read_status', 0);
            })->count();

        $adminchats = AdminChemistChat::where('admin_chemist_chat_head_id', Auth::user()->id)
            ->where('chemist_read_status', 0)->count();


        $data = [
            'status' => true,
            'storechats' => $storechats,
            'groupchats' => $groupchats,
            'admin_chats' => $adminchats,
            'total' => $storechats + $groupchats + $adminchats
        ];

        return response()->json($data);
    }

    public function saveGroupChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'store' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = Auth::user()->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new GroupChat();
        $chat->group_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->chemist_read_status = 1;
        $chat->user = 0;
        $chat->save();

        return response()->json(['status' => true]);
    }

    public function startGroupChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = Auth::user()->id;
            $chathead->last_message = "";
        }
        $chathead->updated_at   = now();
        $chathead->save();

        return response()->json(['status' => true]);
    }

    public function startNewStoreChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = StoreChemistChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new StoreChemistChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = Auth::user()->id;
            $chathead->last_message = "";
        }
        $chathead->save();
        return response()->json(['status' => true]);
    }

    public function saveStorechat(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'store' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = StoreChemistChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new StoreChemistChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = Auth::user()->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new StoreChemistChat();
        $chat->store_chemist_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->chemist_read_status = 1;
        $chat->user = 0;
        $chat->save();

        return response()->json(['status' => true]);
    }

    public function saveAdminChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = AdminChemistChatHead::where('chemist_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new AdminChemistChatHead();
            $chathead->chemist_id = Auth::user()->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new AdminChemistChat();
        $chat->admin_chemist_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->chemist_read_status = 1;
        $chat->admin = 0;
        $chat->save();

        return response()->json(['status' => true]);
    }

    public function chemistMarkReadStoreChemist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => "Store ID is required", 'status' => false]);
        }

        $head = StoreChemistChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->first();
        $head_id = null;
        if ($head) {
            $head_id = $head->id;
            StoreChemistChat::where('store_chemist_chat_head_id', $head->id)->update(['chemist_read_status' => 1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head' => $head_id]);
    }

    public function chemistMarkReadAdmin(Request $request)
    {

        $head    = AdminChemistChatHead::where('chemist_id', Auth::user()->id)->first();
        $head_id = null;
        if ($head) {
            $head_id = $head->id;
            AdminChemistChat::where('admin_chemist_chat_head_id', $head->id)->update(['chemist_read_status' => 1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head' => $head_id]);
    }

    public function chemistMarkReadGroup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $head = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', Auth::user()->id)->first();
        $head_id = null;
        if ($head) {
            $head_id = $head->id;
            GroupChat::where('group_chat_head_id', $head->id)->update(['chemist_read_status' => 1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head' => $head_id]);
    }

    public function deleteGroupChat(Request $request)
    {
        $chathead = GroupChatHead::find($request->id);
        if ($chathead) {
            $chathead->delete();
            return response()->json(['status' => true, 'message' => "削除しました"]);
        } else {
            return response()->json(['status' => false, 'message' => "削除できませんでした"]);
        }
    }

    public function deleteStoreChat(Request $request)
    {
        $chathead = StoreChemistChatHead::find($request->id);
        if ($chathead) {
            $chathead->delete();
            return response()->json(['status' => true, 'message' => "削除しました"]);
        } else {
            return response()->json(['status' => false, 'message' => "削除できませんでした"]);
        }
    }

    public function deleteAdminChat(Request $request)
    {
        $chathead = AdminChemistChatHead::find($request->id);
        if ($chathead) {
            $chathead->delete();
            return response()->json(['status' => true, 'message' => "削除しました"]);
        } else {
            return response()->json(['status' => false, 'message' => "削除できませんでした"]);
        }
    }
}
