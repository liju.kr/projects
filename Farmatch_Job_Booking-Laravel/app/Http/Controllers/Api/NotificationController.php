<?php

namespace App\Http\Controllers\Api;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Chemist;
use App\JobPost;
use App\Favourite;
use App\DrugStore;
use App\Notification;
use App\DrugStoreTiming;
use App\WorkPlaceReview;
use App\ChemistJobApplication;
use App\ChemistInterviewTiming;
use phpDocumentor\Reflection\Types\Null_;

class NotificationController extends Controller
{
    public function approvedByDS(Request $request){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where('notification_type', 'Job Application')
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Accepted'){
                   // $jobPost                          = JobPost::find($row->job_post_id);

                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    $postId = $chemist_job_application->job_post_id;
                    $jobPost=JobPost::where('id', $postId)->withTrashed()->first();

                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
//                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_post_id']        = ($jobPost) ? $jobPost->id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;

                    $diffHours                        = Carbon::now()->diffInHours($row->created_at);
                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
//                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['image'] = ($jobPost->drug_store_id) ? asset("/images/drug_store/" . DrugStore::find($output[$i]['drug_store_id'])->main_image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }
                    if($diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
                }
            }
            return response()->json([ 'status' => 'success', 'notifications' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function rejectedCancelledByDS(Request $request){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_id', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where('notification_type', 'Job Application')
                                             //->where('job_post_id', '!=', null)
                                            // ->whereIn('accept_or_cancel', ['Cancel'])
                                            ->orderBy('id', 'DESC')
                                            ->get();

            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && (ChemistJobApplication::find($row->job_application_id)->status == 'Rejected' || ChemistJobApplication::find($row->job_application_id)->status == 'Job Cancelled')){



//                    $postId = $row->job_post_id;
//                    // $jobPost             = JobPost::find($postId);
//                    $jobPost=JobPost::where('id', $postId)->withTrashed()->first();


                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    $postId = $chemist_job_application->job_post_id;
                    $jobPost=JobPost::where('id', $postId)->withTrashed()->first();


                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['reason_for_rejection']  = $jobApplication->reason_for_rejection;
                    $output[$i]['reason_for_rejection_other'] = $jobApplication->reason_for_rejection_other;
                    //$output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['image'] = ($jobPost->drug_store_id) ? asset("/images/drug_store/" . DrugStore::find($output[$i]['drug_store_id'])->main_image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
                }
            }
            return response()->json([ 'status' => 'success', 'posts' => $postId, 'notifications' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function jobConfirmNotification(Request $request){


        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where('notification_type', 'Job Application')
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Chemist Accept'){
                    $jobPost                          = JobPost::find($row->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;

                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $dateTime = $output[$i]['post_date']." ".$output[$i]['time_from'];
                    $dateTime = new DateTime($dateTime);
                    $output[$i]['job_complete_time']  = $dateTime->format('Y-m-d H:i:s');
                    $i++;
                }
            }
            usort($output, array($this, "dateCompare"));
            return response()->json([ 'status' => 'success', 'notifications' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function dateCompare($a, $b){

        $t1 = strtotime($a['job_complete_time']);
        $t2 = strtotime($b['job_complete_time']);
        return $t1 - $t2;
    }

    public function otherNotifications(Request $request){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where(function($q) {
                                                $q->where('notification_type', '<>', 'Job Application')
                                                  ->orWhereNull('notification_type');
                                            })
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    if($row->notification_type == 'Interview'){
                        $image          = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                        $drugStoreId    = JobPost::find($row->job_post_id)->drug_store_id;
                        $address        = DrugStore::find($drugStoreId)->address;
                        $dsName         = DrugStore::find($drugStoreId)->name;
                    }else{
                        $image      = ($row->image) ? asset("/images/profile/".$row->image) : "";
                        $address    = "";
                        $dsName     = "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['address']            = $address;
                    $output[$i]['ds_name']            = $dsName;
                    $output[$i]['notification_type']  = $row->notification_type;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
            }
            return response()->json([ 'status' => 'success', 'notifications' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

    public function notificationCounts(Request $request){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where('notification_type', 'Job Application')
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Accepted'){
                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $diffHours                        = Carbon::now()->diffInHours($row->created_at);
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }
                    if($diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
                }
            }
            $approvedByDSCount = count($output);

            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && (ChemistJobApplication::find($row->job_application_id)->status == 'Rejected' || ChemistJobApplication::find($row->job_application_id)->status == 'Job Cancelled')){
                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
//                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
//                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
//                    $output[$i]['job_title']          = $jobPost->job_name;
//                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
                }
            }
            $rejectedCancelledByDS = count($output);

            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Chemist Accept'){
                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
//                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
//                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
//                    $output[$i]['job_title']          = $jobPost->job_name;
//                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
                }
            }
            $confirmNotificationCount = count($output);

            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $request->user()->id)
                                            ->where(function($q) {
                                                $q->where('notification_type', '<>', 'Job Application')
                                                ->orWhereNull('notification_type');
                                            })
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                $output[$i]['notification_id']    = $row->id;
                if($row->type == 'Interview'){
                    $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                }else{
                    $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                }
                $output[$i]['image']              = $image;
                $output[$i]['title']              = $row->title;
                $output[$i]['message']            = $row->message;
                $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                $i++;
            }
            $otherNotifications = count($output);

            return response()->json([ 'status'                     => 'success',
                                      'approved_by_ds_count'       => $approvedByDSCount,
                                      'rejected_cancelled_by_dS'   => $rejectedCancelledByDS,
                                      'confirm_notification_count' => $confirmNotificationCount,
                                      'other_notifications'        => $otherNotifications
                                   ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }
    }

}
