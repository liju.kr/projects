<?php

namespace App\Http\Controllers;

use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Chemist;
use App\JobPost;
use App\DrugStore;
use App\Notification;
use App\GroupChatHead;
use App\AdminChemistChatHead;
use App\StoreChemistChatHead;
use App\ChemistJobApplication;

class NotificationController extends Controller
{
    public function index(){

        $chemists   = Chemist::select('first_name', 'last_name', 'phone', 'email', 'created_at')->where('is_verified', '1')->orderBy('id', 'DESC')->get();
        $drugStores = DrugStore::select('name', 'phone', 'email', 'created_at')->where('is_verified', '1')->orderBy('id', 'DESC')->get();
        $notificationArray = [];$i = 0;
        foreach($chemists as $row){
            $notificationArray[$i]['name']         =  $row->last_name." ".$row->first_name;
            $notificationArray[$i]['type']         = 'Chemist';
            $notificationArray[$i]['phone']        = $row->phone;
            $notificationArray[$i]['email']        = $row->email;
            $notificationArray[$i]['created_at']   = Carbon::parse($row->created_at)->format('Y-m-d h:i:s');;
            $i++;
        }
        foreach($drugStores as $row){
            $notificationArray[$i]['name']         = $row->name;
            $notificationArray[$i]['type']         = 'Drug Store';
            $notificationArray[$i]['phone']        = $row->phone;
            $notificationArray[$i]['email']        = $row->email;
            $notificationArray[$i]['created_at']   = Carbon::parse($row->created_at)->format('Y-m-d h:i:s');;
            $i++;
        }
        usort($notificationArray, function($a, $b) {
            return new DateTime($a['created_at']) <=> new DateTime($b['created_at']);
        });
        $notificationArray = array_reverse($notificationArray, true);
        return view('notification') ->with('notification', 'kt-menu__item--active')
                                    ->with('page_title', 'Notification')
                                    ->with('notificationArray', $notificationArray);
    }

    public function sendDrugStoreNotification(){

        $drugStores     =   DrugStore::select('id', 'name')->where('status', 'Approved')->get();
        $notifications  =   Notification::where('type', 'Drug Store')->where('notification_type', 'Admin Message')
                                            ->orderBy('id', 'DESC')
                                            ->get();
        foreach($notifications as $row){
            $drugStore = DrugStore::where('id', $row->drug_store_id)->first();
            if($drugStore){
                $row->drug_store = $drugStore->name;
            }else{
                $row->drug_store = "";
            }

        }
        return view('drug_store_notification')  ->with('notification', 'kt-menu__item--active')
                                                ->with('page_title', '薬局/病院等　お知らせ')
                                                ->with('drugStores', $drugStores)
                                                ->with('notifications', $notifications);
    }

    public function adddDrugStoreNotification(Request $request){

        $validator = Validator::make($request->all(), [
            'drug_store' => 'required',
            'title'      => 'required',
            'message'    => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                if($request->drug_store == 'All'){
                    $drugStores = DrugStore::select('id')->where('status', 'Approved')->get();
                    foreach($drugStores as $row){
                        $notification = new Notification();
                        $notification->type           = 'Drug Store';
                        $notification->notification_type    = 'Admin Message';
                        $notification->drug_store_id  = $row->id;
                        $notification->title          = request('title');
                        $notification->message        = request('message');
                        $notification->save();
                        \Helper::instance()->addAction(Auth::user()->id, $row->id, 'DrugStore', '薬局/病院等にお知らせを送りました', 'flaticon2-bell-4 kt-font-success');
                    }
                }else{
                    $notification = new Notification();
                    $notification->type           = 'Drug Store';
                    $notification->notification_type    = 'Admin Message';
                    $notification->drug_store_id  = request('drug_store');
                    $notification->title          = request('title');
                    $notification->message        = request('message');
                    $notification->save();
                    \Helper::instance()->addAction(Auth::user()->id, request('drug_store'), 'DrugStore', '薬局/病院等にお知らせを送りました', 'flaticon2-bell-4 kt-font-success');
                }
                return response()->json(['message' => '送信しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function sendChemistNotification(){

        $chemists       =   Chemist::select('id', 'first_name', 'last_name')->where('status', 'Approved')->get();
        $notifications  =   Notification::where('type', 'Chemist')->where('notification_type', 'Admin Message')
                                            ->orderBy('id', 'DESC')
                                            ->get();
        foreach($notifications as $row){
            if(Chemist::where('id', $row->chemist_id)->exists()){
                $row->chemist = Chemist::where('id', $row->chemist_id)->first()->first_name;
            }else{
                $row->chemist = "";
            }
        }
        return view('chemist_notification')  ->with('notification', 'kt-menu__item--active')
                                                ->with('page_title', '薬剤師　お知らせ')
                                                ->with('chemists', $chemists)
                                                ->with('notifications', $notifications);
    }

    public function adddChemistNotification(Request $request){

        $validator = Validator::make($request->all(), [
            'chemist'    => 'required',
            'title'      => 'required',
            'message'    => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $adminImage = User::select('image')->first()->image;
                if($request->chemist == 'All'){
                    $chemists = Chemist::select('id')->where('status', 'Approved')->get();
                    foreach($chemists as $row){
                        $notification = new Notification();
                        $notification->type                 = 'Chemist';
                        $notification->notification_type    = 'Admin Message';
                        $notification->chemist_id           = $row->id;
                        $notification->image                = ($adminImage) ? $adminImage : "";
                        $notification->title                = request('title');
                        $notification->message              = request('message');
                        $notification->save();
                        $deviceToken = Chemist::where('id', $row->id)->first()->device_token;
                        $this->sendNotification(request('title'), request('message'), $deviceToken,$row->id);
                        \Helper::instance()->addAction(Auth::user()->id, $row->id, 'Chemist', '薬剤師にお知らせを送りました', 'flaticon2-bell-4 kt-font-success');
                    }
                }else{
                    $notification = new Notification();
                    $notification->type                 = 'Chemist';
                    $notification->notification_type    = 'Admin Message';
                    $notification->chemist_id           = request('chemist');
                    $notification->image                = ($adminImage) ? $adminImage : "";
                    $notification->title                = request('title');
                    $notification->message              = request('message');
                    $notification->save();
                    $deviceToken = Chemist::where('id', request('chemist'))->first()->device_token;
                    $this->sendNotification(request('title'), request('message'), $deviceToken,request('chemist'));
                    \Helper::instance()->addAction(Auth::user()->id, request('chemist'), 'Chemist', '薬剤師にお知らせを送りました', 'flaticon2-bell-4 kt-font-success');
                }
                return response()->json(['message' => '送信しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function sendNotification($title, $message, $deviceToken, $chemistId){

        $badgeCount = $this->getNotificationCount($chemistId);
        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                'badge' => $badgeCount,
                "show_in_foreground" => true,
                "targetScreen" => 'detail'
            ),
                "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }

    public function getNotificationCount($chemistId){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $chemistId)
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    if($row->job_post_id != ''){
                        $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                        $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                        if($row->job_application_id != ''){
                            $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                            $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                            $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                            $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                            $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                            $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                        }else{
                            $diffHours                    = 0;
                            $output[$i]['job_title']      = "";
                            $output[$i]['post_date']      = "";
                            $output[$i]['time_from']      = "";
                            $output[$i]['time_to']        = "";
                            $output[$i]['rest_time']      = "";
                        }
                    } else{
                        $diffHours                    = 0;
                        $output[$i]['drug_store_id']  = "";
                        $output[$i]['drug_store_name']= "";
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                    if($output[$i]['type'] == 'Job Application'){
                        $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                        $applicationStatus  = ($application) ? $application->status : "";
                    }else{
                        $applicationStatus  = "";
                    }
                    if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                        $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                    }else{
                        $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['application_status'] = $applicationStatus;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if($applicationStatus == 'Accepted'){
                        if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                            unset($output[$i]);
                            Notification::where('id',$row->id)->delete();
                            $isIncrement = 1;
                        }
                    }
                    if($applicationStatus == 'Job Completed'){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }

                    if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                            ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        }
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
            }
            $adminChatCount     =  AdminChemistChatHead::
                                                        join('admin_chemist_chats', 'admin_chemist_chats.admin_chemist_chat_head_id', '=', 'admin_chemist_chat_heads.id')
                                                        ->select('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->where('admin_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $storeChatCount     =  StoreChemistChatHead::
                                                        join('store_chemist_chats', 'store_chemist_chats.store_chemist_chat_head_id', '=', 'store_chemist_chat_heads.id')
                                                        ->select('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->where('store_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $groupChatCount     =  GroupChatHead::
                                                join('group_chats', 'group_chats.group_chat_head_id', '=', 'group_chat_heads.id')
                                                ->select('group_chats.group_chat_head_id')
                                                ->where('group_chats.chemist_read_status', 0)
                                                ->where('chemist_id', $chemistId)
                                                ->groupBy('group_chats.group_chat_head_id')
                                                ->get()
                                                ->count();
            return count($output) + $storeChatCount + $adminChatCount + $groupChatCount;
        } catch(\Exception $e){
            return 0;
        }
    }

}
