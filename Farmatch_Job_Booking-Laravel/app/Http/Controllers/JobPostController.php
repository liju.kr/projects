<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Chemist;
use App\JobPost;
use App\DrugStore;
use App\Notification;
use App\ChemistJobApplication;
use App\Mail\CompleteJob;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Stmt\Foreach_;

class JobPostController extends Controller
{
    public function jobPosts(){

        $jobPostsCalender = JobPost::select('id' , 'drug_store_id', 'job_name', 'job_title', 'content', 'date')
                                    ->orderBy('id', 'DESC')
                                    ->get();
        $calenderArray = []; $i = 0;
        foreach($jobPostsCalender as $row){
            $checkConfirmedJobs = $this->checkConfirmedJobs($row->id);
            if($checkConfirmedJobs > 0){
                $class  = 'fc-event-solid-success fc-event-light';
            }else{
                $class  = 'fc-event-solid-orange fc-event-light';
            }
            $calenderArray[$i]['title']     = DrugStore::select('name')->where('id', $row->drug_store_id)->first()->name." [". $row->job_name."]";
            $calenderArray[$i]['start']     = $row->date;
            $calenderArray[$i]['url']       = url('job-post-applications/'.$row->id);
            $calenderArray[$i]['className'] = $class;
            $i++;
        }
        return view('job_posts')    ->with('calenderArray', json_encode($calenderArray))
                                    ->with('jobPost', 'kt-menu__item--active kt-menu__item--open')
                                    ->with('jobPostActive', 'kt-menu__item--active')
                                    ->with('page_title', '募集案件');
    }

    public function checkConfirmedJobs($postId){

        $confirmedJobs = ChemistJobApplication::where('job_post_id', $postId)->where('status', 'Chemist Accept')->get()->count();
        return $confirmedJobs;
    }

    public function approveApplication(Request $request){

        try{
            ChemistJobApplication::where('id', $request->applicationId)
                                ->update([ 'status' =>  'Accepted' ]);
            return response()->json(['message' => '承認しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function rejectApplication(Request $request){

        try{
            ChemistJobApplication::where('id', $request->applicationId)
                                ->update([ 'status' =>  'Rejected' ]);
            return response()->json(['message' => '拒否しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function approvedApplications(){

        $applications   =   DB::table('chemist_job_applications')
                                    ->join('chemists', 'chemist_job_applications.chemist_id', '=', 'chemists.id')
                                    ->join('job_posts', 'chemist_job_applications.job_post_id', '=', 'job_posts.id')
                                    ->join('drug_stores', 'job_posts.drug_store_id', '=', 'drug_stores.id')
                                    ->select('chemist_job_applications.*', 'chemists.first_name as first_name', 'chemists.last_name as last_name', 'drug_stores.name as drug_store_name')
                                    ->whereNull('chemist_job_applications.deleted_at')
                                    ->where('chemist_job_applications.status', 'Accepted')
                                    ->get();
        return view('approved_applications')  ->with('applicationActive', 'kt-menu__item--active kt-menu__item--open')
                                              ->with('approvedApplicationActive', 'kt-menu__item--active kt-menu__item--open')
                                              ->with('page_title', 'Approved Applications')
                                              ->with('applications', $applications);
    }

    public function rejectedApplication(){

        $applications   =   DB::table('chemist_job_applications')
                                    ->join('chemists', 'chemist_job_applications.chemist_id', '=', 'chemists.id')
                                    ->join('job_posts', 'chemist_job_applications.job_post_id', '=', 'job_posts.id')
                                    ->join('drug_stores', 'job_posts.drug_store_id', '=', 'drug_stores.id')
                                    ->select('chemist_job_applications.*', 'chemists.first_name as first_name', 'chemists.last_name as last_name', 'drug_stores.name as drug_store_name')
                                    ->whereNull('chemist_job_applications.deleted_at')
                                    ->where('chemist_job_applications.status', 'Rejected')
                                    ->get();
        return view('rejected_applications') ->with('applicationActive', 'kt-menu__item--active kt-menu__item--open')
                                             ->with('rejectedApplicationActive', 'kt-menu__item--active kt-menu__item--open')
                                             ->with('page_title', 'Rejected Applications')
                                             ->with('applications', $applications);
    }

    public function postDetails($jobId){


        $applications   = ChemistJobApplication::where('job_post_id', $jobId)->get();
        $jobPost        = JobPost::where('id', $jobId)->first();
        $reason         = ["Absence of Chemist" => '職員の休暇', "Busy Time" => '繁忙期', "Lack of Workers" => '人手不足', "Others" => 'その他' ];
        $status         = [ "Accepted" => '承認', "Chemist Accept" => '薬剤師承認済み', "Chemist Reject" => '薬剤師拒否済み', "Job Cancelled" => '勤務案件キャンセル', "Job Completed" => '業務完了', "Pending" => '申請中', "Rejected" => '拒否'];
        $jobPost->reason= $reason[$jobPost->reason];

        foreach($applications as $row){
            $row->chemist = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
            $row->email   = Chemist::where('id', $row->chemist_id)->first()->email;
            $row->phone   = Chemist::where('id', $row->chemist_id)->first()->phone;
            $row->image   = Chemist::where('id', $row->chemist_id)->first()->image;
            $row->status_text  = $status[$row->status];
        }
        return view('job_applications') ->with('allApplications', $applications)
                                        ->with('jobPost', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('jobPostActive', 'kt-menu__item--active')
                                        ->with('jobPost', $jobPost)
                                        ->with('page_title', '募集案件');
    }

    public function sendNotifications(Request $req) {
        // $data = [
        //     'username' => 'ANIS',
        //     'working_day' => '02-01-2021',
        //     'working_time' => '09:00 - 10:00',
        //     "hours" => '21'
        // ];
        // Mail::to('anis.m@iroidtechnologies.com')->send(new CompleteJob($data));
        // echo "here";
        // exit;

        // $this->sendNotification('【早急】 02-03-2021 の勤務内容の確認を完了してください', '21時間後に自動で勤務内容の確認を完了します。', 'fwnS4TFfzEGJpZIwAsxbLi:APA91bE3eL5CZRTGExFhEtpops8TQITZe20B2yxC0i1A2APgAfFJrtEG9iNrHlnIgoQ41jLu-0s31kpTKwr31q_l0NaWXb1LgR7IdPDXZDgb5HZoFBzQmDDNRv51Bw0zOIuilzmlrZb2', 71, 22);
        // echo "heteeeeee";exit;
        $mytime = Carbon::now();
        $curr_date_time = $mytime->toDateTimeString();
        // print_r(strtotime($curr_date_time));exit;
        $confirmedJobs = ChemistJobApplication::
            where('status', 'Chemist Accept')
            ->where('date', '>=', Carbon::now()->toDateString())
            ->with('chemistDetails')
            ->get();
        // print_r($confirmedJobs);exit;
        $curr_time = explode(':', date('H:i:s'));
        $curr_time_in_minutes = $curr_time[0] * 60 + $curr_time[1];
            foreach ($confirmedJobs as $key => $value) {
                $to = Carbon::createFromFormat('Y-m-d H:i:s', $value->date . " " . $value->time_to);
                $from = Carbon::createFromFormat('Y-m-d H:i:s', $curr_date_time);
                $diff_in_minutes = $to->diffInMinutes($from);
                $from_time_stamp = strtotime($curr_date_time);
                $to_time_stamp = strtotime($value->date . " " . $value->time_to);
                $time_stamp_diff = $from_time_stamp - $to_time_stamp;
                // print_r($diff_in_minutes);exit;
                if($time_stamp_diff > 0) {

                    // $to_time = explode(":", $value->time_to);
                    // $to_time_in_minutes = $to_time[0] * 60 + $to_time[1];
                    // $diff_in_minutes = $curr_time_in_minutes - $to_time_in_minutes;
                    $chemist = Chemist::where('id', $value->chemist_id)->first();
                    $notification = Notification::select('id')
                        ->where('chemist_id', $value->chemistDetails->id)
                        ->where('job_application_id', $value->id)
                        ->first();
                    $notification_id = $notification->id;
                    $data = [
                        'username' => $value->chemistDetails->last_name . " " . $value->chemistDetails->first_name,
                        'working_day' => $value->date,
                        'working_time' => "( " . $value->actual_time_from . " - " . $value->actual_time_to . " )"
                    ];
                    if($diff_in_minutes >= 180 && $value->notification_count == 0) {
                        $value->notification_count = 1;
                        $value->save();
                        $data["hours"] = "21";
                       $this->sendNotification('【早急】 ' .$value->date. ' の勤務内容の確認を完了してください', '21時間後に自動で勤務内容の確認を完了します。', $chemist->device_token, $value->id, $notification_id);
                        Mail::to($chemist->email)->send(new CompleteJob($data));

                    }
                    if($diff_in_minutes >= 1080 && $value->notification_count == 1) {
                        $value->notification_count = 2;
                        $value->save();
                        $data["hours"] = "6";
                        $this->sendNotification('【至急】 ' .$value->date. ' の勤務内容の確認を完了してください', '6時間後に自動で勤務内容の確認を完了します。', $chemist->device_token, $value->id, $notification_id);
                        Mail::to($chemist->email)->send(new CompleteJob($data));

                    }

                }
            }
    }

    public function markJobCompleted() {
        $confirmedJobs = ChemistJobApplication::where('status', 'Chemist Accept')
            // ->where('date', Carbon::now()->toDateString())
            ->get();
        $mytime = Carbon::now();
        $curr_date_time = $mytime->toDateTimeString();
        foreach ($confirmedJobs as $key => $job) {

            $to = Carbon::createFromFormat('Y-m-d H:s:i', $job->date . " " . $job->time_to);
            $next = Carbon::createFromFormat('Y-m-d H:s:i', $curr_date_time)->addDays();
            $yesterday = Carbon::yesterday();
            $from = Carbon::createFromFormat('Y-m-d H:s:i', $curr_date_time);
            $diff_in_minutes = $to->diffInMinutes($from);

            // print_r("next=".$next.",from=".$from.",to=".$to.",diff=".$diff_in_minutes);

            // $diff = $from->diffInDays($to);
            // print_r($diff);
            // exit;
            if($to < $from){

            if($diff_in_minutes > 1440) {
                $job->actual_time_from = $job->time_from;
                $job->actual_time_to = $job->time_to;
                $job->actual_rest_time = $job->rest_time;
                $job->actual_transportation_cost = $job->transportation_cost;
                $job->status = 'Job Completed';
                $job->save();
            }

        }

        }
    }




    public function sendNotification($title, $message, $deviceToken, $applicationId, $notification_id){
        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                "show_in_foreground" => true,
            ),
            "data" => array(
                "type" => 'COMPLETE_JOB',
                "application_id" => $applicationId,
                "notification_id" => $notification_id
            ),
            "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }
}
