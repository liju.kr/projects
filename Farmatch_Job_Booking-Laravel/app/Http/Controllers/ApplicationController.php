<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ChemistJobApplication;

class ApplicationController extends Controller
{
    public function approveRejectApplication(){
 
        $applications   =   DB::table('chemist_job_applications')
                                    ->join('chemists', 'chemist_job_applications.chemist_id', '=', 'chemists.id')
                                    ->join('job_posts', 'chemist_job_applications.job_post_id', '=', 'job_posts.id')
                                    ->join('drug_stores', 'job_posts.drug_store_id', '=', 'drug_stores.id')
                                    ->select('chemist_job_applications.*', 'chemists.first_name as first_name', 'chemists.last_name as last_name', 'drug_stores.name as drug_store_name')
                                    ->whereNull('chemist_job_applications.deleted_at')
                                    ->where('chemist_job_applications.status', 'Pending')
                                    ->get();
        return view('approve_reject_application')   ->with('applicationActive', 'kt-menu__item--active kt-menu__item--open')
                                                    ->with('pendingApplicationActive', 'kt-menu__item--active kt-menu__item--open')  
                                                    ->with('page_title', 'Pending Applications')  
                                                    ->with('applications', $applications);
    }


    public function approveApplication(Request $request){

        try{
            ChemistJobApplication::where('id', $request->applicationId)
                                ->update([ 'status' =>  'Accepted' ]);
            return response()->json(['message' => '承認しました', 'status' => TRUE]);        
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }   
    }

    public function rejectApplication(Request $request){

        try{
            ChemistJobApplication::where('id', $request->applicationId)
                                ->update([ 'status' =>  'Rejected' ]);
            return response()->json(['message' => '拒否しました', 'status' => TRUE]);        
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }   
    }

    public function approvedApplications(){

        $applications   =   DB::table('chemist_job_applications')
                                    ->join('chemists', 'chemist_job_applications.chemist_id', '=', 'chemists.id')
                                    ->join('job_posts', 'chemist_job_applications.job_post_id', '=', 'job_posts.id')
                                    ->join('drug_stores', 'job_posts.drug_store_id', '=', 'drug_stores.id')
                                    ->select('chemist_job_applications.*', 'chemists.first_name as first_name', 'chemists.last_name as last_name', 'drug_stores.name as drug_store_name')
                                    ->whereNull('chemist_job_applications.deleted_at')
                                    ->where('chemist_job_applications.status', 'Accepted')
                                    ->get();
        return view('approved_applications')  ->with('applicationActive', 'kt-menu__item--active kt-menu__item--open')
                                              ->with('approvedApplicationActive', 'kt-menu__item--active kt-menu__item--open') 
                                              ->with('page_title', 'Approved Applications')    
                                              ->with('applications', $applications);
    }

    public function rejectedApplication(){

        $applications   =   DB::table('chemist_job_applications')
                                    ->join('chemists', 'chemist_job_applications.chemist_id', '=', 'chemists.id')
                                    ->join('job_posts', 'chemist_job_applications.job_post_id', '=', 'job_posts.id')
                                    ->join('drug_stores', 'job_posts.drug_store_id', '=', 'drug_stores.id')
                                    ->select('chemist_job_applications.*', 'chemists.first_name as first_name', 'chemists.last_name as last_name', 'drug_stores.name as drug_store_name')
                                    ->whereNull('chemist_job_applications.deleted_at')
                                    ->where('chemist_job_applications.status', 'Rejected')
                                    ->get();
        return view('rejected_application') ->with('applicationActive', 'kt-menu__item--active kt-menu__item--open')
                                             ->with('rejectedApplicationActive', 'kt-menu__item--active kt-menu__item--open') 
                                             ->with('page_title', 'Rejected Applications')  
                                             ->with('applications', $applications);
    }
}
