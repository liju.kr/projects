<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Chemist;
use App\Notification;

class ChemistController extends Controller
{
    public function pending(){

        $chemists = Chemist::select('id', 'first_name', 'last_name', 'phone', 'email', 'online_status', 'created_at')
                            ->where('status', 'pending')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('pending_chemist_approval') ->with('chemistActive', 'kt-menu__item--active kt-menu__item--open')
                                                ->with('pendingChemistActive', 'kt-menu__item--active')
                                                ->with('chemists', $chemists)
                                                ->with('page_title', 'Pending Approval');
    }
    public function approved(){

        $chemists = Chemist::select('id', 'first_name', 'last_name', 'phone', 'email', 'online_status', 'created_at')
                                ->where('status', 'Approved')
                                ->orderBy('id', 'DESC')
                                ->get();
        return view('approved_chemists')->with('chemistActive', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('approvedChemistActive', 'kt-menu__item--active')
                                        ->with('chemists', $chemists)
                                        ->with('page_title', 'Approved');
    }
    public function suspended(){

        $chemists = Chemist::select('id', 'first_name', 'last_name', 'phone', 'email', 'online_status', 'created_at')
                            ->where('status', 'Suspended')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('suspended_chemist')->with('chemistActive', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('suspendChemistActive', 'kt-menu__item--active')
                                        ->with('chemists', $chemists)
                                        ->with('page_title', 'Suspended');
    }
    public function approveChemist($chemistId){

        $chemist = Chemist::where('id', $chemistId)->first();
        return view('approve_chemist')  ->with('page_title', 'Approve Chemist')
                                        ->with('chemistActive', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('chemist', $chemist);
    }

    public function updateChemist(Request $request){

        $chemist = Chemist::where('id', $request->id)->first();
        $validator = Validator::make($request->all(), [
            'first_name'         => 'required',
            'last_name'          => 'required',
            'gender'             => 'required',
            'post_code'          => 'required',
            'phone'              => 'required|unique:chemists,phone,' . $request->id . ',id,is_verified,1',
            'email'              => 'required|unique:chemists,email,' . $request->id . ',id,is_verified,1',
            'licence'            => 'required',
            'registration_year'  => 'required',
            'registration_month' => 'required',
        ], [
            'phone.unique'       => 'この番号はすでに使用されています',
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                if ($request->file('main_image')) {
                    $validator = Validator::make($request->all(), [
                        'main_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                    ], [
                        'main_image.required' => 'Please select an image',
                        'main_image.mimes' => 'Main Picture , Allowed image types are pngmjpg,jpeg',
                        'main_image.max' => 'Main picture , Maximum file size allowed is 2 MB',
                    ]);
                    if ($validator->fails()) {
                        return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
                    } else {
                        $image = $request->file('main_image');
                        $newImage = rand() . '.' . $image->getClientOriginalExtension();
                        $image->move(public_path('images/chemist/'), $newImage);
                        File::delete(public_path() . '/images/chemist/' . $chemist->image);
                    }
                    $chemist->image = $newImage;
                }
                $chemist->first_name        = request('first_name');
                $chemist->last_name         = request('last_name');
                $chemist->gender            = request('gender');
                $chemist->post_code         = request('post_code');
                $chemist->phone             = request('phone');
                $chemist->prefecture             = request('prefecture');
                $chemist->city             = request('city');
                $chemist->town             = request('town');
                $chemist->furikana             = request('furikana');
                $chemist->email             = request('email');
                $chemist->licence_number    = request('licence');
                $chemist->insurance_number = request('insurance');
                $chemist->no_of_dependents = request('no_of_dependents');
                $chemist->spouse = request('spouse');
                $chemist->spousal_support = request('spousal_support');
                $chemist->response_time = request('response_time');
                $chemist->self_introduction = request('self_introduction');
                $chemist->skills = request('skills');
                $chemist->desired_entry = request('desired_entry');
                $chemist->registration_year = request('registration_year');
                $chemist->registration_month= request('registration_month');
                $chemist->save();
                return response()->json(['message' => '更新が完了しました。', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage(), 'status' => false]);
            }
        }
    }

    public function chemistApproveRequest(Request $request){

        try {
            Chemist::where('id', $request->chemistId)
                    ->update(['status' => 'Approved']);
            $deviceToken                        = Chemist::where('id', $request->chemistId)->first()->device_token;
            $notification                       = new Notification();
            $notification->type                 = 'Chemist';
            $notification->image                = User::first()->image;
            $notification->notification_type    = 'Admin Approval';
            $notification->chemist_id           = $request->chemistId;
            $notification->title                = "運営本部が承認しました";
            $notification->message              = "アプリ内の全ての機能が使用可能になりました。下のボタンから再ログインして下さい。";
            $notification->save();
            $this->sendNotification('運営本部が承認しました', 'アプリ内の全ての機能が使用可能になりました。下のボタンから再ログインして下さい。', $deviceToken);
            \Helper::instance()->addAction(Auth::user()->id, $request->chemistId, 'Chemist', '新しい薬剤師を承認しました', 'flaticon-avatar kt-font-success');
            return response()->json(['message' => '承認しました', 'status' => true]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false]);
        }
    }

    public function suspendChemist($chemistId){

        $chemist = Chemist::where('id', $chemistId)->first();
        return view('suspend_chemist')  ->with('page_title', 'Suspend Chemist')
                                        ->with('chemistActive', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('chemist', $chemist);
    }

    public function chemistSuspendRequest(Request $request){

        try {
            Chemist::where('id', $request->chemistId)
                    ->update(['status' => 'Suspended']);
            \Helper::instance()->addAction(Auth::user()->id, $request->chemistId, 'Chemist', '新しい薬剤師を拒否しました', 'flaticon-avatar kt-font-danger');
            return response()->json(['message' => '停職にしました', 'status' => true]);
        } catch (\Exception $e) {
            return response()->json(['message' => '入力が正しくありません', 'status' => false]);
        }
    }

    public function searchChemist(Request $request){

        try {
            $fromDate   = Carbon::createFromFormat('m/d/Y', $request->fromDate)->format('Y-m-d');
            $toDate     = Carbon::createFromFormat('m/d/Y', $request->toDate)->format('Y-m-d');
            $chemists   = Chemist::whereBetween('created_at', [$fromDate . " 00:00:00", $toDate . " 23:59:59"])->where('status', $request->status)->get();
            if (count($chemists)) {
                $table = '<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>phone</th>
                                        <th>E-mail</th>
                                        <th>Registered Date</th>
                                        <th>Online Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';
                $i = 1;
                foreach ($chemists as $row) {
                    $table .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $row->first_name . '</td>
                                    <td>' . $row->last_name . '</td>
                                    <td>' . $row->phone . '</td>
                                    <td>' . $row->email . '</td>
                                    <td>' . date('d/m/Y', strtotime($row->created_at)) . '</td>
                                    <td>Online</td>
                                    <td nowrap>
                                        <a href="chat" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                            <i class="la la-wechat" style="font-size: 2.3rem;"></i>
                                        </a>
                                        <a href="approve-drug-store/' . $row->id . '" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="View Chemist">
                                            <i class="la la-eye" style="font-size: 2.3rem;"></i>
                                        </a>
                                    </td>
                                </tr>';
                    $i++;
                }
                $table .= '</tbody>
                            </table>';
                return response()->json(['message' => $table, 'status' => true]);
            } else {
                return response()->json(['message' => '<span class="text-center kt-font-danger">Nothing to show.</span>', 'status' => true]);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => '入力が正しくありません', 'status' => false]);
        }
    }
    public function pickAChemistAdmin(Request $request){

        $query_string = $request->q;
        $chemists = Chemist::where(function ($q) use ($query_string) {
                                $q->orWhere('first_name', 'like', "%{$query_string}%");
                                $q->orWhere('last_name', 'like', "%{$query_string}%");
                                $q->orWhere('email', 'like', "%{$query_string}%");
                                $q->orWhere('phone', 'like', "%{$query_string}%");
                                $q->orWhere('licence_number', 'like', "%{$query_string}%");
                            })  ->where('status', 'Approved')
                                ->limit(20)->get();

        $response = [];
        if ($chemists->count() > 0) {
            $i = 0;
            foreach ($chemists as $chemist) {
                $response[$i]['id'] = $chemist->id;
                $response[$i]['text'] = $chemist->full_name."(".$chemist->email.")";
                $i++;
            }
        }
        return response()->json(['results' => $response]);
    }

    public function sendNotification($title, $message, $deviceToken){

        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                "show_in_foreground" => true,
                "targetScreen" => 'detail'
            ),
                "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }
}
