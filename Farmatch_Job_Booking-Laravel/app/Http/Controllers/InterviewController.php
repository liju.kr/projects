<?php

namespace App\Http\Controllers;

use App\ChemistJobApplication;
use App\DrugStoreWage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Chemist;
use App\DrugStore;
use App\Notification;
use App\GroupChatHead;
use App\AdminChemistChatHead;
use App\StoreChemistChatHead;
use App\ChemistInterviewTiming;
use App\ChemistInspectionTiming;

class InterviewController extends Controller
{
    public function pendingInterviews(){

        $pendingInterviews  =  ChemistInterviewTiming::select('id', 'drug_store_id', 'chemist_id', 'day', 'time_from', 'time_to')
                                                        ->where('status', 'Pending')
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
        foreach($pendingInterviews as $row){
            $row->drug_store  = DrugStore::select('name', 'manager_company')->where('id', $row->drug_store_id)->first();
            $row->chemist     = Chemist::select('first_name', 'last_name', 'phone', 'email') ->where('id', $row->chemist_id)->first();
        }
        return view('pending_interviews')   ->with('interviewActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('pendingInterviewActive', 'kt-menu__item--active')
                                            ->with('page_title', '承認待ちの見学')
                                            ->with('pendingInterviews', $pendingInterviews);
    }

    public function changeInterviewStatus(Request $request){

        $validator  =   Validator::make($request->all(),[
            'interviewId'           => 'required',
            'status'                => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                ChemistInterviewTiming::where('id', $request->interviewId)
                                        ->update([ 'status'    => $request->status ]);
                $chemistId      =   ChemistInterviewTiming::where('id', $request->interviewId)->first();
                $deviceToken    =   Chemist::where('id', $chemistId->chemist_id)->first()->device_token;
                $notification                      =   new Notification();
                $notification->type                =   'Chemist';
                $notification->notification_type   =   'Interview';
                $notification->job_post_id         =   $chemistId->job_post_id;
                $notification->chemist_id          =   $chemistId->chemist_id;
                $drugStore  =   DrugStore::where('id', ChemistInterviewTiming::where('id', $request->interviewId)->first()->drug_store_id)->first();
                $notification->image               =   $drugStore->main_image;
                $notification->drug_store_address  =   $drugStore->address;
                $notification->drug_store_model    =   $drugStore->history_model;
                if($request->status == 'Approved'){


//                    Chemist::where('id', $chemistId->chemist_id)
//                                        ->update([ 'hourly_wage'         => $request->hourly_wage,
//                                                   'transportation_cost' => $request->transportation_cost
//                                                ]);



                    $chemist_wage = DrugStoreWage::where('chemist_id', $chemistId->chemist_id)->where('drug_store_id', $chemistId->drug_store_id)->first();

                    if($chemist_wage)
                    {
                        $chemist_wage_update = DrugStoreWage::where('chemist_id', $chemistId->chemist_id)->where('drug_store_id', $chemistId->drug_store_id)->first();
                        $chemist_wage_update->hourly_wage = $request->hourly_wage;
                        $chemist_wage_update->transportation_cost = $request->transportation_cost;
                        $chemist_wage_update->save();
                    }
                    else
                    {
                        $chemist_wage_new = new DrugStoreWage();
                        $chemist_wage_new->chemist_id = $chemistId->chemist_id;
                        $chemist_wage_new->drug_store_id = $chemistId->drug_store_id;
                        $chemist_wage_new->hourly_wage = $request->hourly_wage;
                        $chemist_wage_new->transportation_cost = $request->transportation_cost;
                        $chemist_wage_new->save();
                    }





                    $notification->title     =   '面談の結果が確認されました';
                    $notification->message   =   '面談をした勤務先の募集案件に申し込めるようになりました';
                    \Helper::instance()->addAction(Auth::user()->id, $chemistId->chemist_id, 'Chemist', '薬剤師の見学を承認＆時給/交通費を設定しました', 'flaticon-notepad kt-font-success');
                }else{
                    $notification->title     =   '面談の結果が受け付けられませんでした';
                    $notification->message   =   '面談の結果、勤務先に受付けられませんでした';
                    \Helper::instance()->addAction(Auth::user()->id, $chemistId->chemist_id, 'Chemist', '薬剤師の見学を拒否しました', 'flaticon-notepad kt-font-danger');
                }
                $notification->save();
                $this->sendNotification($notification->title, $notification->message, $deviceToken, $chemistId);
                return response()->json(['message' => '更新しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function approvedInterviews(Request $request){

        $approvedInterviews  =  ChemistInterviewTiming::select('id', 'drug_store_id', 'chemist_id', 'day', 'time_from', 'time_to', 'job_post_id')
                                                        ->where('status', 'Approved')
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
        foreach($approvedInterviews as $row){
            $row->drug_store  = DrugStore::select('name', 'manager_company', 'id')->where('id', $row->drug_store_id)->first();
            $row->chemist     = Chemist::select('first_name', 'last_name', 'phone', 'email') ->where('id', $row->chemist_id)->first();
        }

        return view('approved_interviews')  ->with('interviewActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('approvedInterviewActive', 'kt-menu__item--active')
                                            ->with('page_title', '承認済みの見学')
                                            ->with('approvedInterviews', $approvedInterviews);

    }

    public function rejectedInterviews(Request $request){

        $rejectedInterviews  =  ChemistInterviewTiming::select('id', 'drug_store_id', 'chemist_id', 'day', 'time_from', 'time_to')
                                                        ->where('status', 'Rejected')
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
        foreach($rejectedInterviews as $row){
            $row->drug_store  = DrugStore::select('name', 'manager_company')->where('id', $row->drug_store_id)->first();
            $row->chemist     = Chemist::select('first_name', 'last_name', 'phone', 'email') ->where('id', $row->chemist_id)->first();
        }
        return view('rejected_interviews')  ->with('interviewActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('rejectedInterviewActive', 'kt-menu__item--active')
                                            ->with('page_title', '拒否された見学')
                                            ->with('rejectedInterviews', $rejectedInterviews);
    }

    public function pendingInspections(Request $request){


        $pendingInspections =   ChemistInspectionTiming::select('id', 'drug_store_id', 'chemist_id', 'day', 'time_from', 'time_to')
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
        foreach($pendingInspections as $row){
            $row->drug_store  = DrugStore::select('name')->where('id', $row->drug_store_id)->first();
            $row->chemist     = Chemist::select('first_name', 'last_name', 'phone', 'email') ->where('id', $row->chemist_id)->first();
        }
        return view('pending_inspections')   ->with('inspectionActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('pendingInspectionActive', 'kt-menu__item--active')
                                            ->with('page_title', '申請中の⾒学')
                                            ->with('pendingInspections', $pendingInspections);
    }

    public function getHourlyWage(Request $request){

        $drugStoreId = $request->drugStoreId;
        $chemistId      =   ChemistInterviewTiming::select('chemist_id', 'job_post_id', 'drug_store_id')->where('id', $request->interviewId)->first();

         // $chemist        =   Chemist::select('hourly_wage', 'transportation_cost')->where('id', $chemistId->chemist_id)->first();   // old code

        $chemist = DrugStoreWage::select('hourly_wage', 'transportation_cost')->where('chemist_id', $chemistId->chemist_id)->where('drug_store_id', $chemistId->drug_store_id)->first();

        //return $chemistId->chemist_id."-".$chemistId->job_post_id;

        return response()->json(['message' => $chemist, 'status' => TRUE]);
    }

    public function updateHourlyWage(Request $request){

        $validator  =   Validator::make($request->all(),[
            'interviewId'           => 'required',
            'hourly_wage'           => 'required',
            'transportation_cost'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                 $chemistId      =   ChemistInterviewTiming::where('id', $request->interviewId)->first();
//                Chemist::where('id', $chemistId->chemist_id)
//                                        ->update([ 'hourly_wage'         => $request->hourly_wage,
//                                                   'transportation_cost' => $request->transportation_cost
//                                                ]);


                 $chemist_wage = DrugStoreWage::where('chemist_id', $chemistId->chemist_id)->where('drug_store_id', $chemistId->drug_store_id)->first();

                if($chemist_wage)
                {
                    $chemist_wage_update = DrugStoreWage::where('chemist_id', $chemistId->chemist_id)->where('drug_store_id', $chemistId->drug_store_id)->first();
                    $chemist_wage_update->hourly_wage = $request->hourly_wage;
                    $chemist_wage_update->transportation_cost = $request->transportation_cost;
                    $chemist_wage_update->save();
                }
                else
                {
                    $chemist_wage_new = new DrugStoreWage();
                    $chemist_wage_new->chemist_id = $chemistId->chemist_id;
                    $chemist_wage_new->drug_store_id = $chemistId->drug_store_id;
                    $chemist_wage_new->hourly_wage = $request->hourly_wage;
                    $chemist_wage_new->transportation_cost = $request->transportation_cost;
                    $chemist_wage_new->save();
                }


                \Helper::instance()->addAction(Auth::user()->id, $chemistId->chemist_id, 'Chemist', '薬剤師の時給/交通費を再設定しました', 'flaticon2-layers-2 kt-font-success');
                return response()->json(['message' => '更新しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function sendNotification($title, $message, $deviceToken, $chemistId){

        $badgeCount = $this->getNotificationCount($chemistId);
        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                'badge' => $badgeCount,
                "show_in_foreground" => true,
                "targetScreen" => 'detail'
            ),
                "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }

    public function getNotificationCount($chemistId){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $chemistId)
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    if($row->job_post_id != ''){
                        $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                        $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                        if($row->job_application_id != ''){
                            $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                            $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                            $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                            $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                            $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                            $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                        }else{
                            $diffHours                    = 0;
                            $output[$i]['job_title']      = "";
                            $output[$i]['post_date']      = "";
                            $output[$i]['time_from']      = "";
                            $output[$i]['time_to']        = "";
                            $output[$i]['rest_time']      = "";
                        }
                    } else{
                        $diffHours                    = 0;
                        $output[$i]['drug_store_id']  = "";
                        $output[$i]['drug_store_name']= "";
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                    if($output[$i]['type'] == 'Job Application'){
                        $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                        $applicationStatus  = ($application) ? $application->status : "";
                    }else{
                        $applicationStatus  = "";
                    }
                    if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                        $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                    }else{
                        $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['application_status'] = $applicationStatus;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if($applicationStatus == 'Accepted'){
                        if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                            unset($output[$i]);
                            Notification::where('id',$row->id)->delete();
                            $isIncrement = 1;
                        }
                    }
                    if($applicationStatus == 'Job Completed'){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }

                    if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                            ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        }
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
            }
            $adminChatCount     =  AdminChemistChatHead::
                                                        join('admin_chemist_chats', 'admin_chemist_chats.admin_chemist_chat_head_id', '=', 'admin_chemist_chat_heads.id')
                                                        ->select('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->where('admin_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $storeChatCount     =  StoreChemistChatHead::
                                                        join('store_chemist_chats', 'store_chemist_chats.store_chemist_chat_head_id', '=', 'store_chemist_chat_heads.id')
                                                        ->select('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->where('store_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $groupChatCount     =  GroupChatHead::
                                                join('group_chats', 'group_chats.group_chat_head_id', '=', 'group_chat_heads.id')
                                                ->select('group_chats.group_chat_head_id')
                                                ->where('group_chats.chemist_read_status', 0)
                                                ->where('chemist_id', $chemistId)
                                                ->groupBy('group_chats.group_chat_head_id')
                                                ->get()
                                                ->count();
            return count($output) + $storeChatCount + $adminChatCount + $groupChatCount;
        } catch(\Exception $e){
            return 0;
        }
    }

}
