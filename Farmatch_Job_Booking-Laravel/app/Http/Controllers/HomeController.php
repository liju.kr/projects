<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeIntroService;
use App\EmployeeIntroServiceThree;
use App\EmployeeIntroServiceTwo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use PDF;
use App\Chemist;
use App\DrugStore;
use App\ChemistJobApplication;

class HomeController extends Controller
{
    public function index(){

        $distinctYears          = DB::select("SELECT DISTINCT YEAR(DATE) AS years FROM `chemist_job_applications` ORDER BY years ASC");
        $distinctYearsChemist   = DB::select("SELECT DISTINCT YEAR(created_at) AS years FROM `chemists` ORDER BY years ASC");
        $distinctYearsDrugStore = DB::select("SELECT DISTINCT YEAR(created_at) AS years FROM `drug_stores` ORDER BY years ASC");
        $drugStores             = DB::select("SELECT MONTH(created_at) as month , COUNT(created_at) as count FROM `drug_stores`WHERE created_at >= NOW() - INTERVAL 1 YEAR AND status != 'Not Verified' GROUP BY MONTH(created_at) ORDER BY created_at");
        $chemists               = DB::select("SELECT MONTH(created_at) as month , COUNT(created_at) as count FROM `chemists`WHERE created_at >= NOW() - INTERVAL 1 YEAR AND status != 'Not Verified' GROUP BY MONTH(created_at) ORDER BY created_at");

        $drugStoreArray = $chemistArray = [];
        $yearArray      = ["1" => "1月", "2" => "2月", "3" => '3月', "4" => "4月", "5" => "5月", "6" => "6月", "7" => "7月", "8" => "8月", "9" => "9月", "10" => "10月", "11" => "11月v", "12" => "12月"];

        foreach($distinctYears as $row){
            $row->totalApplication       = ChemistJobApplication::whereYear('date', $row->years)->count();
            $row->acceptedApplication    = ChemistJobApplication::whereYear('date', $row->years)->where('status', 'Job Completed')->count();
        }
        foreach($distinctYearsChemist as $row){
            $row->totalChemists          = Chemist::whereYear('created_at', $row->years)->where('status', '!=' ,'Not Verified')->count();
            $row->activeChemists         = Chemist::whereYear('created_at', $row->years)->where('status', 'Approved')->count();
        }
        foreach($distinctYearsDrugStore as $row){
            $row->totalDrugStores        = DrugStore::whereYear('created_at', $row->years)->where('status', '!=' ,'Not Verified')->count();
            $row->activeDrugStores       = DrugStore::whereYear('created_at', $row->years)->where('status', 'Approved')->count();
        }
        foreach($drugStores as $row){
            $drugStoreArray[$row->month] = $row->count;
        }
        foreach($chemists as $row){
            $chemistArray[$row->month]   = $row->count;
        }
        $finalArray = [];$i = 0;
        foreach($drugStoreArray as $k => $v){
            $finalArray[$i]['country']  = $yearArray[$k];
            $finalArray[$i]['income']   = (array_key_exists($k, $chemistArray)) ? $chemistArray[$k] :  0 ;
            $finalArray[$i]['expenses'] = $v;
            $i++;
        }
        $finalJobArray = [];$i = 0;
        foreach($distinctYears as $row){
            $finalJobArray[$i]['country']   = $row->years;
            $finalJobArray[$i]['year2004']  = $row->totalApplication;
            $finalJobArray[$i]['year2005']  = $row->acceptedApplication;
            $i++;
        }
        $finalChemistArray = [];$i = 0;
        foreach($distinctYearsChemist as $row){
            $finalChemistArray[$i]['country']   = $row->years;
            $finalChemistArray[$i]['year2004']  = $row->totalChemists;
            $finalChemistArray[$i]['year2005']  = $row->activeChemists;
            $i++;
        }
        $finalStoreArray = [];$i = 0;
        foreach($distinctYearsDrugStore as $row){
            $finalStoreArray[$i]['country']   = $row->years;
            $finalStoreArray[$i]['year2004']  = $row->totalDrugStores;
            $finalStoreArray[$i]['year2005']  = $row->activeDrugStores;
            $i++;
        }
        $registrationResult     = json_encode($finalArray);
        $jobAcceptanceReport    = json_encode($finalJobArray);
        $chemistReport          = json_encode($finalChemistArray);
        $storeReport            = json_encode($finalStoreArray);

        $latestChemists         = Chemist::select('id', 'first_name', 'last_name', 'phone', 'email', 'image', 'status')->orderBy('id', 'DESC')->where('status', 'Approved')->offset(0)->limit(5)->get();
        $latestDrugStores       = DrugStore::select('id', 'name', 'phone', 'email', 'main_image', 'created_at', 'status')->orderBy('id', 'DESC')->where('status', 'Approved')->offset(0)->limit(4)->get();

        $avgChemistReviews      = DB::select("SELECT chemist_id,CAST(AVG(attitude) AS DECIMAL(10,2)) AS attitude, CAST(AVG(adapting) AS DECIMAL(10,2)) AS adapting, CAST(AVG(work_speed) AS DECIMAL(10,2)) AS work_speed,
                                        CAST(AVG(value_for_money) AS DECIMAL(10,2)) AS value_for_money FROM `chemist_reviews` GROUP BY chemist_id");
        $chemistReviews = []; $i = 0;
        foreach($avgChemistReviews as $row){

            $chemistReviews[$i]['country']    =  Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
            $chemistReviews[$i]['visits']     =  ($row->attitude + $row->adapting + $row->work_speed + $row->value_for_money) / 4;
            $i++;
        }
        usort($chemistReviews, function($a, $b) {
            return $a['visits'] <=> $b['visits'];
        });
        $chemistReviewReport    =   json_encode($chemistReviews);

        $avgDrugStoreReviews    =   DB::select("SELECT drug_store_id,CAST(AVG(atmosphere) AS DECIMAL(10,2)) AS atmosphere, CAST(AVG(clerk_adaption) AS DECIMAL(10,2)) AS clerk_adaption, CAST(AVG(pharmacist_adaption) AS DECIMAL(10,2)) AS pharmacist_adaption,
                                        CAST(AVG(compliance_level) AS DECIMAL(10,2)) AS compliance_level FROM `work_place_reviews` GROUP BY drug_store_id");
        $drugStoreReviews = []; $i = 0;
        foreach($avgDrugStoreReviews as $row){

            $drugStoreReviews[$i]['country']    =  DrugStore::where('id', $row->drug_store_id)->first()->name;
            $drugStoreReviews[$i]['visits']     =  ($row->atmosphere + $row->clerk_adaption + $row->pharmacist_adaption + $row->compliance_level) / 4;
            $i++;
        }
        usort($drugStoreReviews, function($a, $b) {
            return $a['visits'] <=> $b['visits'];
        });

        $chemistReviewReport    = json_encode($chemistReviews);
        $drugStoreReviewReport  = json_encode($drugStoreReviews);

        return view('home') ->with('chemists', $latestChemists)
                            ->with('drugStores', $latestDrugStores)
                            ->with('registrations', $registrationResult)
                            ->with('jobReport', $jobAcceptanceReport)
                            ->with('chemistReport', $chemistReport)
                            ->with('storeReport', $storeReport)
                            ->with('chemistReviewReport', $chemistReviewReport)
                            ->with('drugStoreReviewReport', $drugStoreReviewReport)
                            ->with('dashboardActive', 'kt-menu__item--active')
                            ->with('page_title', 'Dashboard');
    }


    public function introServices(){

        $drugStores     =   DrugStore::select('id', 'name', 'address', 'employee_intro_date', 'employee_intro_service')
            ->orderBy('id', 'DESC')
            ->get();
        return view('intro_service')
            ->with('introActive', 'kt-menu__item--active')
            ->with('drugStores', $drugStores)
            ->with('page_title', '求人票');
    }

    public function deleteIntro(Request $request){

        $validator  =   Validator::make($request->all(),[
            'storeId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $drugStore= DrugStore::where('id', $request->storeId)->first();
                $employee_intro_date = date('Y-m-d', strtotime(Carbon::now()));
                DrugStore::where('id', $drugStore->id)->update(['employee_intro_service'=> 0, 'employee_intro_date'=> $employee_intro_date]);
                $intro_1 = EmployeeIntroService::where('drug_store_id', $drugStore->id)->delete();
                $intro_2 = EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->delete();
                $intro_3 = EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->delete();
                if($intro_1){
                    return response()->json(['message' => '削除しました', 'status' => TRUE]);
                }else{
                    return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function introServicesPdf($id){

        $drugStore     =   DrugStore::where('id', $id)->first();
        // $employee_intro_service_1 = EmployeeIntroService::where('drug_store_id', $drugStore->id)->first();
        // $employee_intro_service_2 = EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first();
        // $employee_intro_service_3 = EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first();
        // $pdf = PDF::loadView('pdfview',[$employee_intro_service_1,$employee_intro_service_2,$employee_intro_service_3])
        //             ->setPaper('a4')
        //             ->setOrientation('landscape');
        //     return $pdf->download('pdfdownload.pdf');


        // return view('pdfview')
        //     ->with('employee_intro_service_1', $employee_intro_service_1)
        //     ->with('employee_intro_service_2', $employee_intro_service_2)
        //     ->with('employee_intro_service_3', $employee_intro_service_3);
        $data=[
            'employee_intro_service_1' => EmployeeIntroService::where('drug_store_id', $drugStore->id)->first(),
            'employee_intro_service_2' => EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first(),
            'employee_intro_service_3' => EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first()
        ];

        return view('pdfview',$data);

        // $pdf = PDF::loadView('pdfview',$data)
        // ->setPaper('letter')
        // ->setOrientation('landscape');
        // return $pdf->download('pdfdownload.pdf');
    }

    public function introServicePdfDownload($id){
        $drugStore     =   DrugStore::where('id', $id)->first();
        $data=[
            'employee_intro_service_1' => EmployeeIntroService::where('drug_store_id', $drugStore->id)->first(),
            'employee_intro_service_2' => EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first(),
            'employee_intro_service_3' => EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first()
        ];


        //return view('pdfview',$data);

        $pdf = PDF::loadView('pdfview',$data)
        ->setPaper('A4')
        ->setOrientation('landscape')
        ->setOption('margin-top', 0)
        ->setOption('margin-left', 0)
        ->setOption('margin-right', 0)
        ->setOption('margin-bottom', 0);
        return $pdf->download('求人票.pdf');
    }


    public function introupdate($id){

//        EmployeeIntroService::where('drug_store_id', $id)
//                    ->update(['date' => date('Y-m-d')]);
//        DrugStore::where('id', $id)->update(['employee_intro_renewed' => 1]);
//        return redirect('/drug-store/store-home-dashboard');

        return redirect('/drug-store/store-settings');
    }

    public function introinactive($id){
        DrugStore::where('id', $id)->update(['employee_intro_service'=> 0]);//updating inactive state for intro service
        //EmployeeIntroService::where('drug_store_id', $id)->update(['date' => date('Y-m-d')]); //updating last edit date
        return redirect('/drug-store/store-home-dashboard');
    }

}
