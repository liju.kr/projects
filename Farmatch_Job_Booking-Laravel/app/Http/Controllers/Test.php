<?php

namespace App\Http\Controllers;

use App\Chemist;
use App\ChemistInterviewTiming;
use App\ChemistJobApplication;
use App\DrugStore;
use App\DrugStoreWage;
use App\JobPost;
use App\Notification;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Test extends Controller
{
    public function rejectedCancelledByDS(Request $request)
    {

//        try{
        $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_id', 'drug_store_address', 'drug_store_model', 'created_at')
            ->where('chemist_id', 256)
            ->where('notification_type', 'Job Application')
           // ->where('job_post_id', '!=', null)
            // ->whereIn('accept_or_cancel', ['Cancel'])
            ->orderBy('id', 'DESC')
            ->get();

        $output = [];
        $i = 0;
        foreach ($notifications as $row) {
            if (ChemistJobApplication::where('id', $row->job_application_id)->exists() && (ChemistJobApplication::find($row->job_application_id)->status == 'Rejected' || ChemistJobApplication::find($row->job_application_id)->status == 'Job Cancelled')) {


                //                    $postId = $row->job_post_id;
//                    // $jobPost             = JobPost::find($postId);
//                    $jobPost=JobPost::where('id', $postId)->withTrashed()->first();


                $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                $postId = $chemist_job_application->job_post_id;
                $jobPost=JobPost::where('id', $postId)->withTrashed()->first();


                $jobApplication = ChemistJobApplication::find($row->job_application_id);
                $output[$i]['notification_id'] = $row->id;
                $output[$i]['job_post_id'] = ($row->job_post_id) ? $row->job_post_id : "";
                $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                $output[$i]['drug_store_id'] = $jobPost->drug_store_id;
                $output[$i]['drug_store_name'] = DrugStore::find($output[$i]['drug_store_id'])->name;
                $output[$i]['job_title'] = $jobPost->job_name;
                $output[$i]['post_date'] = $jobPost->date;
                $output[$i]['time_from'] = date('H:i', strtotime($jobApplication->time_from));
                $output[$i]['time_to'] = date('H:i', strtotime($jobApplication->time_to));
                $output[$i]['rest_time'] = $jobApplication->rest_time;
                $output[$i]['reason_for_rejection'] = $jobApplication->reason_for_rejection;
                $output[$i]['reason_for_rejection_other'] = $jobApplication->reason_for_rejection_other;
               // $output[$i]['image'] = ($row->image) ? asset("/images/drug_store/" . $row->image) : "";
                $output[$i]['image'] = ($jobPost->drug_store_id) ? asset("/images/drug_store/" . DrugStore::find($output[$i]['drug_store_id'])->main_image) : "";
                $output[$i]['address'] = ($row->drug_store_address) ? $row->drug_store_address : "";
                $output[$i]['model'] = ($row->drug_store_model) ? $row->drug_store_model : "";
                $output[$i]['title'] = $row->title;
                $output[$i]['message'] = $row->message;
                $output[$i]['notification_time'] = date('Y-m-d H:i', strtotime($row->created_at));
                $i++;
            }
        }
        return response()->json(['status' => 'success', 'posts' => $postId, 'notifications' => $output]);
//        } catch(\Exception $e){
//            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
//        }
    }


    public function downloadResume(Request $request)
    {

        //$chemist_id = $request->chemist;
        $chemist = Chemist::find(256);
        if (!$chemist) {
            return response()->json(['status' => false, 'message' => 'Chemist not exist']);
        }
        $pdf = PDF::loadView('store.testresume3', compact('chemist'));
        $mask = public_path() . "/resumes/256" . '_*.*';
        array_map('unlink', glob($mask));
        $random = rand();
        $pdf->setPaper('a4', 'portrait')->save(public_path() . "/resumes/256" . "_" . $random . ".pdf");
        return response()->json(['status' => 'success', 'path' => URL::to('/') . "/resumes/256" . "_" . $random . ".pdf"]);
    }

    public function notificationCounts(Request $request){

//        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                ->where('chemist_id', 256)
                ->where('notification_type', 'Job Application')
                ->orderBy('id', 'DESC')
                ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Accepted'){
                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $diffHours                        = Carbon::now()->diffInHours($row->created_at);
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }
                    if($diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
                }
            }
            $approvedByDSCount = count($output);

            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && (ChemistJobApplication::find($row->job_application_id)->status == 'Rejected' || ChemistJobApplication::find($row->job_application_id)->status == 'Job Cancelled')){

                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                   // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
//                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
//                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
//
//                    $output[$i]['job_title']          = $jobPost->job_name;
//                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
                }
            }
            $rejectedCancelledByDS = count($output);

            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Chemist Accept'){
                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    // $jobPost                          = JobPost::find($row->job_post_id);
                    $jobPost                          = JobPost::find($chemist_job_application->job_post_id);
                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
//                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
//                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;
//
//                    $output[$i]['job_title']          = $jobPost->job_name;
//                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $i++;
                }
            }
            $confirmNotificationCount = count($output);

            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                ->where('chemist_id', 256)
                ->where(function($q) {
                    $q->where('notification_type', '<>', 'Job Application')
                        ->orWhereNull('notification_type');
                })
                ->orderBy('id', 'DESC')
                ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                $output[$i]['notification_id']    = $row->id;
                if($row->type == 'Interview'){
                    $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                }else{
                    $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                }
                $output[$i]['image']              = $image;
                $output[$i]['title']              = $row->title;
                $output[$i]['message']            = $row->message;
                $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                $i++;
            }
            $otherNotifications = count($output);

            return response()->json([ 'status'                     => 'success',
                'approved_by_ds_count'       => $approvedByDSCount,
                'rejected_cancelled_by_dS'   => $rejectedCancelledByDS,
                'confirm_notification_count' => $confirmNotificationCount,
                'other_notifications'        => $otherNotifications
            ]);
//        } catch(\Exception $e){
//            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
//        }
    }


    public function shiftOfThisMonthJobHistory(Request $request){

        try{

            $after6months = Carbon::now()->addMonths(6);
            $today=Carbon::now();

            $after6months = date('Y-m-d', strtotime($after6months));
            $today = date('Y-m-d', strtotime($today));

            $getThisMonthJobs = ChemistJobApplication::select('date')
                // ->whereYear('date', Carbon::now()->year)
                // ->whereMonth('date', Carbon::now()->month)
                 ->whereBetween('date', [$today, $after6months])
//                ->where('date', '>=', $today)
//                ->where('date', '<=', $after6months)
                ->where('chemist_id', 256)
                ->whereIn('status', ['Chemist Accept'])
                ->orderBy('date', 'ASC')
                ->get();
            $dateArray = $output1 = [];$i = 0;
            foreach($getThisMonthJobs as $row){
                if(!in_array($row->date,$dateArray)){
                    $dateArray[]   = $row->date;
                    $output1[$i]    = $row->date;
                    $i++;
                }
            }


            $historyDates   =   DB::table('chemist_job_applications')
                ->select('date')
                ->whereDate('date', '<=', $today)
                ->where('chemist_id', 256)
                ->where('status', 'Job Completed')
                ->where('deleted_at', Null)
                ->groupBy('date')
                ->get();
            $j = 0;$output2 = [];
            foreach($historyDates as $row){
                $output2[$j]    =   $row->date;
                $j++;
            }
            $output = array_merge($output1,$output2);
            $output = array_unique($output);

            return response()->json([ 'status' => 'success', 'dates' => $output ]);
        } catch(\Exception $e){
            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
        }

    }


    public function shiftHistoryDetails(Request $request){

            try{

                $date = date("Y-m-d", strtotime($request->date));
                $current_date = date("Y-m-d", strtotime(Carbon::now()));

                  $jobDetails =   ChemistJobApplication::where('date', $request->date)
                        ->where('chemist_id', 256)
                        ->whereIn('status', ['Chemist Accept', 'Job Completed'])
                        ->orderBy('date', 'ASC')
                        ->get();
                    $output = [];$i = 0;
                    foreach($jobDetails as $row)
                    {
                        if($row->status=="Chemist Accept")
                        {
                            $status_flag = "future";
                            $output[$i]['work_hour']      = Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i')." ~ ".Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i');
                            $output[$i]['rest_time']      = $row->rest_time;
                            $drugStore = DrugStore::where('id', $row['drug_store_id'])->first();
                            $output[$i]['image']          = ($drugStore->main_image) ? asset("/images/drug_store/".$drugStore->main_image) : "";
                            $output[$i]['name']           = ($drugStore->name) ? $drugStore->name : "";
                            $output[$i]['status'] = $status_flag;
                        }
                        else
                        {
                            $status_flag = "history";
                            $jobPost = JobPost::where('id', $row->job_post_id)->first();
                            $output[$i]['id']           = $row->job_post_id;
                            $output[$i]['store_name']   = DrugStore::where('id', $row->drug_store_id)->first()->name;
                            $output[$i]['job_name']     = $jobPost->job_name;
                            $output[$i]['work_from']    = Carbon::createFromFormat('H:i:s', $row->actual_time_from)->format('H:i');
                            $output[$i]['work_to']      = Carbon::createFromFormat('H:i:s', $row->actual_time_to)->format('H:i');
                            $output[$i]['job_title']    = $jobPost->job_title;
                            $output[$i]['latitude']     = DrugStore::where('id', $row->drug_store_id)->first()->latitude;
                            $output[$i]['longitude']    = DrugStore::where('id', $row->drug_store_id)->first()->longitude;
                            $mainImage  =   DrugStore::where('id', $row->drug_store_id)->first()->main_image;
                            $output[$i]['image']        = ($mainImage) ? asset("/images/drug_store/".$mainImage) : "";
                            $output[$i]['status'] = $status_flag;
                        }

                        $i++;
                    }

                return response()->json([ 'status' => 'success', 'details' => $output ]);
            } catch(\Exception $e){
                return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません', 'error' => $e->getMessage()]);
            }

    }


    public function approvedByDS(Request $request){

//        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                ->where('chemist_id', 256)
                ->where('notification_type', 'Job Application')
                ->orderBy('id', 'DESC')
                ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                if(ChemistJobApplication::where('id', $row->job_application_id)->exists() && ChemistJobApplication::find($row->job_application_id)->status == 'Accepted'){

//                    $jobPost                          = JobPost::find($row->job_post_id);

                    $chemist_job_application = ChemistJobApplication::where('id', $row->job_application_id)->first();
                    $postId = $chemist_job_application->job_post_id;
                    $jobPost=JobPost::where('id', $postId)->withTrashed()->first();

                    $jobApplication                   = ChemistJobApplication::find($row->job_application_id);
                    $output[$i]['notification_id']    = $row->id;
//                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_post_id']        = ($jobPost) ? $jobPost->id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    $output[$i]['drug_store_id']      = $jobPost->drug_store_id;
                    $output[$i]['drug_store_name']    = DrugStore::find($output[$i]['drug_store_id'])->name;

                    $diffHours                        = Carbon::now()->diffInHours($row->created_at);
                    $output[$i]['job_title']          = $jobPost->job_name;
                    $output[$i]['post_date']          = $jobPost->date;
                    $output[$i]['time_from']          = date('H:i',strtotime($jobApplication->time_from));
                    $output[$i]['time_to']            = date('H:i',strtotime($jobApplication->time_to));
                    $output[$i]['rest_time']          = $jobApplication->rest_time;
//                    $output[$i]['image']              = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    $output[$i]['image'] = ($jobPost->drug_store_id) ? asset("/images/drug_store/" . DrugStore::find($output[$i]['drug_store_id'])->main_image) : "";
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }
                    if($diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
                }
            }
            return response()->json([ 'status' => 'success', 'notifications' => $output ]);
//        } catch(\Exception $e){
//            return response()->json([ 'status' => 'error', 'message' => '入力が正しくありません']);
//        }
    }

    public function wagesImport(Request $request)
    {

      $interviews =  ChemistInterviewTiming::select('id', 'drug_store_id', 'chemist_id', 'day', 'time_from', 'time_to', 'job_post_id')
        ->orderBy('id', 'DESC')
        ->get();

     foreach ($interviews as $interview)
     {
         $chemist = Chemist::select('hourly_wage', 'transportation_cost')->where('id', $interview->chemist_id)->first();

         $drugstore_wage = DrugStoreWage::select('hourly_wage', 'transportation_cost')->where('chemist_id', $interview->chemist_id)->where('drug_store_id', $interview->drug_store_id)->first();

         if(!$drugstore_wage)
         {
             $wage = new DrugStoreWage();
             $wage->drug_store_id = $interview->drug_store_id;
             $wage->chemist_id = $interview->chemist_id;
             $wage->hourly_wage = $chemist->hourly_wage;
             $wage->transportation_cost = $chemist->transportation_cost;
             $wage->save();
         }

     }

      return  $drugstore_wages = DrugStoreWage::select('drug_store_id', 'chemist_id', 'hourly_wage', 'transportation_cost')->get();


    }


}
