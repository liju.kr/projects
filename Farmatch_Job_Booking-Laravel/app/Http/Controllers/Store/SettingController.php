<?php

namespace App\Http\Controllers\Store;

use App\EmployeeIntroService;
use App\EmployeeIntroServiceThree;
use App\EmployeeIntroServiceTwo;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Faq;
use App\JobPost;
use App\Chemist;
use App\DrugStore;
use App\DrugStoreTiming;
use App\DrugStorePayment;
use App\ChemistJobApplication;
use App\ChemistInspectionTiming;

use Session;

class SettingController extends Controller
{
    public function index(){

        $faqs              = Faq::where('type', 'Drug Store')->get();
        $drugStore         = DrugStore::where('id', Auth::id())->first();
        $drugStoreTiming   = DrugStoreTiming::select('day', 'day_japan', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->get();
        $drugStoreFromTo   = DrugStoreTiming::select('day', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->first();
        $registeredDate    = Carbon::parse($drugStore->created_at);
        $diffYears         = Carbon::now()->diffInYears($registeredDate);
        $completedWorks    = JobPost::whereDate('date', '<=', Carbon::yesterday()->toDateString())
                                            ->where('status', 'Active')
                                            ->where('drug_store_id', Auth::id())
                                            ->count();
        $completedJobs = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
            ->where('status', 'Job Completed')
            ->where('drug_store_id', Auth::id())
            ->groupBy('chemist_id')
            ->selectRaw('chemist_id, count(chemist_id) as count')
            ->get();
        $completed = 0;
        foreach($completedJobs as $row){
            if($row->count > 0)
              $completed++;
        }
        $chemistWorked          = ChemistJobApplication::whereDate('date', '<=',Carbon::now()->toDateString())
                                                        ->where('drug_store_id', Auth::id())
                                                        ->where('status', 'Job Completed')
                                                        ->count();
        $chemistRepeatWorked    = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', Auth::id())
                                                        ->groupBy('chemist_id')
                                                        ->where('status', 'Job Completed')
                                                        ->selectRaw('chemist_id, count(chemist_id) as count')
                                                        ->get();
        $suddenCancellation     = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', Auth::id())
                                                        ->where('status', 'Job Cancelled')
                                                        ->count();
        $repeated = 0;
        foreach($chemistRepeatWorked as $row){
            if($row->count > 1)
              $repeated++;
        }
        $drugStoretiming    =  DrugStoreTiming::select('day', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->get();
        $days   = []; $fromTime = $toTime = "";
        foreach($drugStoreTiming as $row){
            $days[]    = $row->day;
            $fromTime  = $row->time_from;
            $toTime    = $row->time_to;
        }
        return view('store.home_settings') ->with('drugStore', $drugStore)
                                           ->with('faqs', $faqs)
                                           ->with('days', $days)
                                           ->with('fromTime', $fromTime)
                                           ->with('toTime', $toTime)
                                           ->with('drugStoreTiming', $drugStoreTiming)
                                           ->with('drugStoreFromTo', $drugStoreFromTo)
                                           ->with('experience', $diffYears)
                                        //    ->with('completedWorks', $completedWorks)
                                           ->with('completedWorks', $chemistWorked)
                                           ->with('chemistWorked', $completed)
                                           ->with('repeatWorked', $repeated)
                                           ->with('suddenCancellation', $suddenCancellation)
                                           ->with('page_title', '設定');
    }

    public function employeeIntroService(){

        $drugStore         = DrugStore::where('id', Auth::id())->first();

        $employee_intro_service_checking_1 = EmployeeIntroService::where('drug_store_id', $drugStore->id)->first();
        $employee_intro_service_checking_2 = EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first();
        $employee_intro_service_checking_3 = EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first();

        if($employee_intro_service_checking_1)
        {
            $employee_intro_service_1 = EmployeeIntroService::findOrFail($employee_intro_service_checking_1->id);
            //$employee_intro_service_1 = [];
        }
        else
        {
            $employee_intro_service_1 = [];
        }
        if($employee_intro_service_checking_2)
        {
            $employee_intro_service_2 = EmployeeIntroServiceTwo::findOrFail($employee_intro_service_checking_2->id);
        }
        else
        {
            $employee_intro_service_2 = [];
        }
        if($employee_intro_service_checking_3)
        {
            $employee_intro_service_3 = EmployeeIntroServiceThree::findOrFail($employee_intro_service_checking_3->id);
        }
        else
        {
            $employee_intro_service_3 = [];
        }
        return view('store.employee_intro_service')->with('employee_intro_service_1', $employee_intro_service_1)->with('employee_intro_service_2', $employee_intro_service_2)->with('employee_intro_service_3', $employee_intro_service_3)->with('page_title', '常勤やパートの紹介希望');
    }

    public function saveEmployeeIntroService(Request $request){

        try {
        //return $request;
        $drugStore         = DrugStore::where('id', Auth::id())->first();

        $employee_intro_service_checking_1 = EmployeeIntroService::where('drug_store_id', $drugStore->id)->first();

        if($employee_intro_service_checking_1)
        {
            $employee_intro_service_1 = EmployeeIntroService::findOrFail($employee_intro_service_checking_1->id);
        }
        else
        {
            $employee_intro_service_1 = new EmployeeIntroService();
        }

        $employee_intro_service_1->drug_store_id = $drugStore->id;
        $employee_intro_service_1->date = $request->date;
        $employee_intro_service_1->furigana = $request->furigana;
        $employee_intro_service_1->office_name_furigana = $request->office_name_furigana;
        $employee_intro_service_1->office_location_code = $request->office_location_code;
        $employee_intro_service_1->office_location = $request->office_location;
        $employee_intro_service_1->occupation = $request->occupation;
        $employee_intro_service_1->job_description = $request->job_description;
        $employee_intro_service_1->employment_status = $request->employment_status;
        $employee_intro_service_1->appointment_of_full_time_employee = $request->appointment_of_full_time_employee;
        $employee_intro_service_1->appointment_of_full_time_employee_description = $request->appointment_of_full_time_employee_description;
        $employee_intro_service_1->working_style = $request->working_style;
        $employee_intro_service_1->worker_dispatch_business_permit_number = $request->worker_dispatch_business_permit_number;
        $employee_intro_service_1->employment_period = $request->employment_period;
        $employee_intro_service_1->contract_renewal_conditions = $request->contract_renewal_conditions;
        $employee_intro_service_1->work_place_code = $request->work_place_code;
        $employee_intro_service_1->work_place = $request->work_place;
        $employee_intro_service_1->my_car_commute = $request->my_car_commute;
        $employee_intro_service_1->parking = $request->parking;
        $employee_intro_service_1->possibility_of_transfer = $request->possibility_of_transfer;
        $employee_intro_service_1->possibility_of_transfer_details = $request->possibility_of_transfer_details;
        $employee_intro_service_1->age_promotion = $request->age_promotion;
        $employee_intro_service_1->age_limit_reason = $request->age_limit_reason;
        $employee_intro_service_1->age_limit_reason_details = $request->age_limit_reason_details;
        $employee_intro_service_1->academic_background = $request->academic_background;
        $employee_intro_service_1->academic_background_details = $request->academic_background_details;
        $employee_intro_service_1->knowledge_skills = $request->knowledge_skills;
        $employee_intro_service_1->knowledge_skills_details = $request->knowledge_skills_details;
        $employee_intro_service_1->pc_skills = $request->pc_skills;
        $employee_intro_service_1->pc_skills_details = $request->pc_skills_details;
        $employee_intro_service_1->pharmacist_license = $request->pharmacist_license;
        $employee_intro_service_1->ordinary_car_drivers_license = $request->ordinary_car_drivers_license;
        $employee_intro_service_1->trial_period = $request->trial_period;
        $employee_intro_service_1->trial_period_details = $request->trial_period_details;
        $employee_intro_service_1->monthly_amount_a = $request->monthly_amount_a;
        $employee_intro_service_1->monthly_amount_b = $request->monthly_amount_b;
        $employee_intro_service_1->average_working_days_per_month = $request->average_working_days_per_month;
        $employee_intro_service_1->basic_pay_a = $request->basic_pay_a;
        $employee_intro_service_1->basic_pay_b = $request->basic_pay_b;
        $employee_intro_service_1->fixed_amount_1 = $request->fixed_amount_1;
        $employee_intro_service_1->fixed_amount_1_field_1 = $request->fixed_amount_1_field_1;
        $employee_intro_service_1->fixed_amount_1_field_2 = $request->fixed_amount_1_field_2;
        $employee_intro_service_1->fixed_amount_2 = $request->fixed_amount_2;
        $employee_intro_service_1->fixed_amount_2_field_1 = $request->fixed_amount_2_field_1;
        $employee_intro_service_1->fixed_amount_2_field_2 = $request->fixed_amount_2_field_2;
        $employee_intro_service_1->fixed_amount_3 = $request->fixed_amount_3;
        $employee_intro_service_1->fixed_amount_3_field_1 = $request->fixed_amount_3_field_1;
        $employee_intro_service_1->fixed_amount_3_field_2 = $request->fixed_amount_3_field_2;
        $employee_intro_service_1->fixed_amount_4 = $request->fixed_amount_4;
        $employee_intro_service_1->fixed_amount_4_field_1 = $request->fixed_amount_4_field_1;
        $employee_intro_service_1->fixed_amount_4_field_2 = $request->fixed_amount_4_field_2;
        $employee_intro_service_1->fixed_amount_5 = $request->fixed_amount_5;
        $employee_intro_service_1->fixed_amount_5_field_1 = $request->fixed_amount_5_field_1;
        $employee_intro_service_1->fixed_amount_5_field_2 = $request->fixed_amount_5_field_2;
        $employee_intro_service_1->overtime_pay = $request->overtime_pay;
        $employee_intro_service_1->overtime_pay_1 = $request->overtime_pay_1;
        $employee_intro_service_1->overtime_pay_2 = $request->overtime_pay_2;
        $employee_intro_service_1->overtime_pay_note = $request->overtime_pay_note;
        $employee_intro_service_1->articles_with_allowance = $request->articles_with_allowance;
        $employee_intro_service_1->other_contents = $request->other_contents;
        $employee_intro_service_1->actual_expense_payment = $request->actual_expense_payment;
        $employee_intro_service_1->actual_expense_payment_monthly = $request->actual_expense_payment_monthly;
        $employee_intro_service_1->wages = $request->wages;
        $employee_intro_service_1->wages_details = $request->wages_details;
        $employee_intro_service_1->wages_notes = $request->wages_notes;
        $employee_intro_service_1->payment_date = $request->payment_date;
        $employee_intro_service_1->payment_date_details = $request->payment_date_details;
        $employee_intro_service_1->payment_date_notes = $request->payment_date_notes;
        $employee_intro_service_1->salary_raise = $request->salary_raise;
        $employee_intro_service_1->salary_raise_details = $request->salary_raise_details;
        $employee_intro_service_1->bonus = $request->bonus;
        $employee_intro_service_1->bonus_details = $request->bonus_details;
        $employee_intro_service_1->save();





        if(!empty($request->insurance_coverage))
        {
            $insurance_coverage="";
            foreach ($request->insurance_coverage as $insurance)
            {
                $insurance_coverage = $insurance_coverage.$insurance.", ";
            }
            $insurance_coverage = rtrim($insurance_coverage, ", ");
        }
        else
        {
            $insurance_coverage=null;
        }

        if(!empty($request->annuities_corporate))
        {
            $annuities_corporate="";
            foreach ($request->annuities_corporate as $annuities)
            {
                $annuities_corporate = $annuities_corporate.$annuities.", ";
            }
            $annuities_corporate = rtrim($annuities_corporate, ", ");
        }
        else
        {
            $annuities_corporate=null;
        }





         $employee_intro_service_checking_2 = EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first();

            if($employee_intro_service_checking_2)
            {
                $employee_intro_service_2 = EmployeeIntroServiceTwo::findOrFail($employee_intro_service_checking_2->id);
            }
            else
            {
                $employee_intro_service_2 = new EmployeeIntroServiceTwo();
            }

            $employee_intro_service_2->drug_store_id = $drugStore->id;
            $employee_intro_service_2->working_hours_1_from = $request->working_hours_1_from;
            $employee_intro_service_2->working_hours_1_to = $request->working_hours_1_to;
            $employee_intro_service_2->working_hours_2_from = $request->working_hours_2_from;
            $employee_intro_service_2->working_hours_2_to = $request->working_hours_2_to;
            $employee_intro_service_2->working_hours_3_from = $request->working_hours_3_from;
            $employee_intro_service_2->working_hours_3_to = $request->working_hours_3_to;
            $employee_intro_service_2->working_hours_or_from = $request->working_hours_or_from;
            $employee_intro_service_2->working_hours_or_to = $request->working_hours_or_to;
            $employee_intro_service_2->working_hours_between = $request->working_hours_between;
            $employee_intro_service_2->working_hours_notes = $request->working_hours_notes;
            $employee_intro_service_2->overtime_work_hours = $request->overtime_work_hours;
            $employee_intro_service_2->overtime_work_hours_details = $request->overtime_work_hours_details;
            $employee_intro_service_2->special_provisions = $request->special_provisions;
            $employee_intro_service_2->special_provisions_details = $request->special_provisions_details;
            $employee_intro_service_2->time_break = $request->time_break;
            $employee_intro_service_2->annual_holidays = $request->annual_holidays;
            $employee_intro_service_2->holidays_etc = $request->holidays_etc;
            $employee_intro_service_2->annual_paid_vacation_days_after_6_months = $request->annual_paid_vacation_days_after_6_months;
            $employee_intro_service_2->insurance_coverage = $insurance_coverage;
            $employee_intro_service_2->insurance_coverage_details = $request->insurance_coverage_details;
            $employee_intro_service_2->retirement_benefits = $request->retirement_benefits;
            $employee_intro_service_2->retirement_system = $request->retirement_system;
            $employee_intro_service_2->annuities_corporate = $annuities_corporate;
            $employee_intro_service_2->retirement_age = $request->retirement_age;
            $employee_intro_service_2->reemployment_system = $request->reemployment_system;
            $employee_intro_service_2->duty_extension = $request->duty_extension;
            $employee_intro_service_2->retirement_age_details = $request->retirement_age_details;
            $employee_intro_service_2->reemployment_system_details = $request->reemployment_system_details;
            $employee_intro_service_2->duty_extension_details = $request->duty_extension_details;
            $employee_intro_service_2->housing_for_singles = $request->housing_for_singles;
            $employee_intro_service_2->housing_for_households = $request->housing_for_households;
            $employee_intro_service_2->housing_details = $request->housing_details;
            $employee_intro_service_2->childcare_facilities = $request->childcare_facilities;
            $employee_intro_service_2->childcare_facilities_details = $request->childcare_facilities_details;
            $employee_intro_service_2->save();


            if(!empty($request->methodology_elective_exam))
            {
                $methodology_elective_exam="";
                foreach ($request->methodology_elective_exam as $methodology)
                {
                    $methodology_elective_exam = $methodology_elective_exam.$methodology.", ";
                }
                $methodology_elective_exam = rtrim($methodology_elective_exam, ", ");
            }
            else
            {
                $methodology_elective_exam=null;
            }


            if(!empty($request->result_announcement))
            {
                $result_announcement="";
                foreach ($request->result_announcement as $result)
                {
                    $result_announcement = $result_announcement.$result.", ";
                }
                $result_announcement = rtrim($result_announcement, ", ");
            }
            else
            {
                $result_announcement=null;
            }

            if(!empty($request->method_notification))
            {
                $method_notification="";
                foreach ($request->method_notification as $method)
                {
                    $method_notification = $method_notification.$method.", ";
                }
                $method_notification = rtrim($method_notification, ", ");
            }
            else
            {
                $method_notification=null;
            }





            $employee_intro_service_checking_3 = EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first();

            if($employee_intro_service_checking_3)
            {
                $employee_intro_service_3 = EmployeeIntroServiceThree::findOrFail($employee_intro_service_checking_3->id);
            }
            else
            {
                $employee_intro_service_3 = new EmployeeIntroServiceThree();
            }

            $employee_intro_service_3->drug_store_id = $drugStore->id;
            $employee_intro_service_3->number_of_employees = $request->number_of_employees;
            $employee_intro_service_3->japanese_era = $request->japanese_era;
            $employee_intro_service_3->full_name = $request->full_name;
            $employee_intro_service_3->work_place_name = $request->work_place_name;
            $employee_intro_service_3->capital = $request->capital;
            $employee_intro_service_3->of_which_female = $request->of_which_female;
            $employee_intro_service_3->of_which_part = $request->of_which_part;
            $employee_intro_service_3->union = $request->union;
            $employee_intro_service_3->business_content = $request->business_content;
            $employee_intro_service_3->features_of_the_company = $request->features_of_the_company;
            $employee_intro_service_3->position_name = $request->position_name;
            $employee_intro_service_3->representative_name = $request->representative_name;
            $employee_intro_service_3->corporate_number = $request->corporate_number;
            $employee_intro_service_3->full_time = $request->full_time;
            $employee_intro_service_3->part_time = $request->part_time;
            $employee_intro_service_3->achievements_of_taking_childcare_leave = $request->achievements_of_taking_childcare_leave;
            $employee_intro_service_3->nursing_care_leave_acquisition_record = $request->nursing_care_leave_acquisition_record;
            $employee_intro_service_3->nursing_leave_acquisition_record = $request->nursing_leave_acquisition_record;
            $employee_intro_service_3->foreign_employment_record = $request->foreign_employment_record;
            $employee_intro_service_3->special_notes_on_job_vacancies = $request->special_notes_on_job_vacancies;
            $employee_intro_service_3->number_of_people_recruitment = $request->number_of_people_recruitment;
            $employee_intro_service_3->reason_for_recruitment = $request->reason_for_recruitment;
            $employee_intro_service_3->methodology_elective_exam = $methodology_elective_exam;
            $employee_intro_service_3->schedule = $request->schedule;
            $employee_intro_service_3->result_announcement = $result_announcement;
            $employee_intro_service_3->after_the_book = $request->after_the_book;
            $employee_intro_service_3->after_the_face = $request->after_the_face;
            $employee_intro_service_3->method_notification = $method_notification;
            $employee_intro_service_3->as_needed = $request->as_needed;
            $employee_intro_service_3->selection_place_code = $request->selection_place_code;
            $employee_intro_service_3->selection_place = $request->selection_place;
            $employee_intro_service_3->string_1 = $request->string_1;
            $employee_intro_service_3->string_2 = $request->string_2;
            $employee_intro_service_3->on_foot = $request->on_foot;
            $employee_intro_service_3->documents = $request->documents;
            $employee_intro_service_3->name_furigana = $request->name_furigana;
            $employee_intro_service_3->furigana_name = $request->furigana_name;
            $employee_intro_service_3->telephone_number = $request->telephone_number;
            $employee_intro_service_3->fax = $request->fax;
            $employee_intro_service_3->email = $request->email;
            $employee_intro_service_3->address = $request->address;
            $employee_intro_service_3->save();


            if(Session::has('employee_intro_service'))
            {
                $employee_intro_date = date('Y-m-d', strtotime(Carbon::now()));
                DrugStore::where('id', Auth::id())->update(['employee_intro_service'=> Session::get('employee_intro_service'), 'employee_intro_renewed'=> 1, 'employee_intro_service_first'=> 1, 'employee_intro_automatic'=> Session::get('automatic_renewal'), 'employee_intro_date'=> $employee_intro_date]);
                Session::forget('normal_farmatch');
                Session::forget('employee_intro_service');
                Session::forget('automatic_renewal');
            }


            return response()->json(['status' => true, "message"=>"正常に更新されました"]);

    } catch(\Exception $e){
            return response()->json(['status' => false]);
    }

    }



    public function employeeIntroServicePdf(){
//        return view('store.employee_intro_service_pdf');

        $output="liju";
        $pdf = PDF::loadView('store.employee_intro_service_pdf', compact('output'));
        $pdf->setPaper('a4', 'landscape');
        return $pdf->stream('employee.pdf');
    }


    public function testmypdf(){
                $output="liju";
                $pdf = PDF::loadView('store.employee_intro_service_pdf1', compact('output'));
                $pdf->setPaper('a4', 'landscape');
                return $pdf->stream('employee.pdf');
            }

    public function updateStoreProfile(Request $request){

        $validator  =   Validator::make($request->all(),[
            'name'            => 'required',
            'manager_company' => "required",
            'in_charge'       => 'required',
            'phone'           => 'required',
            'alter_phone'     => 'required',
            'email'           => 'required|unique:drug_stores,email,'.$request->user()->id.',id,is_verified,1',
            'address'         => 'required',
            'days'            => 'required',
            'time_from'       => 'required',
            'time_to'         => 'required'
        ], [
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{

                if($request->normal_farmatch==1)
                {
                    DrugStore::where('id', $request->store_id)->update(['normal_farmatch'=> 1, 'normal_farmatch_disable'=> 0]);
                }
                else
                {
                    DrugStore::where('id', $request->store_id)->update(['normal_farmatch_disable'=> 1]);
                }

                if($request->employee_intro_service==1)
                {
                    $drugStore         = DrugStore::where('id', Auth::id())->first();
                    $employee_intro_service_checking_1 = EmployeeIntroService::where('drug_store_id', $drugStore->id)->first();
                    $employee_intro_service_checking_2 = EmployeeIntroServiceTwo::where('drug_store_id', $drugStore->id)->first();
                    $employee_intro_service_checking_3 = EmployeeIntroServiceThree::where('drug_store_id', $drugStore->id)->first();

                    if(!$employee_intro_service_checking_1 || !$employee_intro_service_checking_2 || !$employee_intro_service_checking_3)
                    {
                        return response()->json(['message' => "従業員紹介サービスを更新してください", 'status' => FALSE]);
                    }

                    $employee_intro_date = date('Y-m-d', strtotime(Carbon::now()));

                    DrugStore::where('id', $request->store_id)->update(['employee_intro_service'=> 1, 'employee_intro_renewed'=> 1, 'employee_intro_service_first'=> 1, 'employee_intro_automatic'=> $request->automatic_renewal, 'employee_intro_date'=> $employee_intro_date]);


                }
                else
                {
                    DrugStore::where('id', $request->store_id)->update(['employee_intro_service'=> 0, 'employee_intro_renewed'=> 0]);
                }

               // return response()->json(['message' => "test", 'status' => FALSE]);


                $fromTime = Carbon::createFromFormat('H:i', $request->time_from)->format('H:i:s');
                $toTime   = Carbon::createFromFormat('H:i', $request->time_to)->format('H:i:s');
                if(Carbon::parse($fromTime)->floatDiffInHours($toTime, false) <= 0){
                    return response()->json(['message' => '「開始時間」は「終了時間」よりも早い時間である必要があります。', 'status' => FALSE]);
                }
                if(count($request->days) > 3){
                    return response()->json(['message' => '面談可能曜日は3つ以下で選択してください', 'status' => FALSE]);
                }
                DrugStore::where('id', $request->store_id)->update(['name'           => request('name'),
                                                                    'manager_company'=> request('manager_company'),
                                                                    'in_charge'      => request('in_charge'),
                                                                    'phone'          => request('phone'),
                                                                    'alter_phone'    => request('alter_phone'),
                                                                    'email'          => request('email'),
                                                                    'address'        => request('address')
                                                                 ]);
                $drugStoreTiming = DrugStoreTiming::where('drug_store_id', Auth::id())->first();
                $japanDays       = [ 'Sunday' => '日曜日', 'Monday' => '月曜日', 'Tuesday' => '火曜日', 'Wednesday' => '水曜日', 'Thursday' => '木曜日', 'Friday' => '金曜日', 'Saturday' => '土曜日' ];
                if(!$drugStoreTiming){
                    $timings = [];$i = 0;
                    foreach($request->days as $day){
                        $timings[$i]['drug_store_id'] = Auth::id();
                        $timings[$i]['day']           = $day;
                        $timings[$i]['day_japan']     = $japanDays[$day];
                        $timings[$i]['time_from']     = $fromTime;
                        $timings[$i]['time_to']       = $toTime;
                        $i++;
                    }
                    DrugStoreTiming::insert($timings);
                }else{
                    DrugStoreTiming::where('drug_store_id', Auth::id())->delete();
                    $timings = [];$i = 0;
                    foreach($request->days as $day){
                        $timings[$i]['drug_store_id'] = Auth::id();
                        $timings[$i]['day']           = $day;
                        $timings[$i]['day_japan']     = $japanDays[$day];
                        $timings[$i]['time_from']     = $fromTime;
                        $timings[$i]['time_to']       = $toTime;
                        $i++;
                    }
                    DrugStoreTiming::insert($timings);
                }
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '何かがうまくいかなかった', 'status' => FALSE]);
            }
        }
    }

    public function updateStoreProfile2(Request $request){

        $drugStore  = DrugStore::where('id', $request->store_id)->first();
        $validator  =   Validator::make($request->all(),[
            'history_model'     => 'required',
            'clerks'            => 'required',
            'chemists'          => 'required',
            'response_speed'    => 'required',
            'latitude'          => 'required',
            'longitude'         => 'required',
        ], [
            'latitude.required'  => '有効な場所を入力してください',
            'longitude.required' => '有効な場所を入力してください'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                if($request->file('main_image') == '' && $drugStore->main_image == ''){
                    return response()->json(['message' => '画像を選択してください', 'status' => FALSE]);
                }
                if($request->file('main_image')){
                    $validator  = Validator::make($request->all(),[
                                        'main_image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'main_image.required' => '画像を選択してください',
                                        'main_image.mimes'    => 'サブ画像2、許可される画像タイプはpngmjpg、jpegです',
                                        'main_image.max'      => 'サブ画像2、許可される最大ファイルサイズは2 MBです'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('main_image');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->image);
                    }
                    $drugStore->main_image   = $newImage;
                }
                if($request->file('sub_image1')){
                    $validator  = Validator::make($request->all(),[
                                        'sub_image1'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'sub_image1.required' => '画像を選択してください',
                                        'sub_image1.mimes'    => 'サブ画像2、許可される画像タイプはpngmjpg、jpegです',
                                        'sub_image1.max'      => 'サブ画像2、許可される最大ファイルサイズは2 MBです'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('sub_image1');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->sub_image1);
                    }
                    $drugStore->sub_image1   = $newImage;
                }
                if($request->file('sub_image2')){
                    $validator  = Validator::make($request->all(),[
                                        'sub_image2'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'sub_image2.required' => '画像を選択してください',
                                        'sub_image2.mimes'    => 'サブ画像2、許可される画像タイプはpngmjpg、jpegです',
                                        'sub_image2.max'      => 'サブ画像2、許可される最大ファイルサイズは2 MBです'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('sub_image2');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->sub_image2);
                    }
                    $drugStore->sub_image2   = $newImage;
                }
                $drugStore->history_model        = (request('history_model') == 'Others') ? "その他" : request('history_model');
                $drugStore->store_information    = request('store_information');
                $drugStore->clerks               = request('clerks');
                $drugStore->chemists             = request('chemists');
                $drugStore->respond_speed        = request('response_speed');
                $drugStore->other_information    = request('other_info');
                $drugStore->latitude             = request('latitude');
                $drugStore->longitude            = request('longitude');
                $drugStore->save();
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
            }
        }
    }

    public function addTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'days'          => 'required',
            'time_from'     => 'required',
            'time_to'       => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $fromTime = Carbon::createFromFormat('H:i', $request->time_from)->format('H:i:s');
                $toTime   = Carbon::createFromFormat('H:i', $request->time_to)->format('H:i:s');
                if(Carbon::parse($fromTime)->floatDiffInHours($toTime, false) <= 0){
                    return response()->json(['message' => '「開始時間」は「終了時間」よりも早い時間である必要があります。', 'status' => FALSE]);
                }
                if(count($request->days) > 3){
                    return response()->json(['message' => '面談可能曜日は3つ以下で選択してください', 'status' => FALSE]);
                }
                $drugStoreTiming = DrugStoreTiming::where('drug_store_id', Auth::id())->first();
                $japanDays       = [ 'Sunday' => '日曜日', 'Monday' => '月曜日', 'Tuesday' => '火曜日', 'Wednesday' => '水曜日', 'Thursday' => '木曜日', 'Friday' => '金曜日', 'Saturday' => '土曜日' ];
                if(!$drugStoreTiming){
                    $timings = [];$i = 0;
                    foreach($request->days as $day){
                        $timings[$i]['drug_store_id'] = Auth::id();
                        $timings[$i]['day']           = $day;
                        $timings[$i]['day_japan']     = $japanDays[$day];
                        $timings[$i]['time_from']     = $fromTime;
                        $timings[$i]['time_to']       = $toTime;
                        $i++;
                    }
                    DrugStoreTiming::insert($timings);
                    return response()->json(['message' => '正常に保存', 'status' => TRUE]);
                }else{
                    DrugStoreTiming::where('drug_store_id', Auth::id())->delete();
                    $timings = [];$i = 0;
                    foreach($request->days as $day){
                        $timings[$i]['drug_store_id'] = Auth::id();
                        $timings[$i]['day']           = $day;
                        $timings[$i]['day_japan']     = $japanDays[$day];
                        $timings[$i]['time_from']     = $fromTime;
                        $timings[$i]['time_to']       = $toTime;
                        $i++;
                    }
                    DrugStoreTiming::insert($timings);
                    return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function deleteTiming(Request $request){

        $validator  =   Validator::make($request->all(),[
            'storeId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $deletedRows = DrugStoreTiming::where('drug_store_id', $request->storeId)->delete();
                if($deletedRows){
                    return response()->json(['message' => '削除しました', 'status' => TRUE]);
                }else{
                    return response()->json(['message' => 'Already Added', 'status' => FALSE]);
                }
            } catch(\Exception $e){
                return response()->json(['message' => '何かがうまくいかなかった', 'status' => FALSE]);
            }
        }
    }

    public function getTimings(){

        $drugStoreTiming   = DrugStoreTiming::select('day', 'day_japan', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->get();
        $drugStoreFromTo   = DrugStoreTiming::select('day', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->first();

        $table = '';
        if(count($drugStoreTiming)) {
                $table  .=   '<table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="400"> インタビュー可能曜日 </th>
                                        <th>開始時間 </th>
                                        <th>終了時間</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>';
            foreach ($drugStoreTiming as $row){
                $table  .=   '<div class="tag">'.$row->day_japan.'</div>';
            }
                $table  .=               '</td>
                                        <td>'.date("H:i",strtotime($drugStoreFromTo->time_from)) .'</td>
                                        <td>'.date("H:i",strtotime($drugStoreFromTo->time_to)).'</td>
                                    </tr>
                                </tbody>
                            </table>';
        }
        return response()->json(['message' => $table, 'status' => TRUE]);
    }

    public function cancelAllApplications(Request $request){

        try{
            ChemistJobApplication::where('drug_store_id',  Auth::id())->where('chemist_cancelled', 'Yes')->delete();
            return response()->json(['message' => 'すべて削除しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '何かがうまくいかなかった', 'status' => FALSE]);
        }
    }

    public function downloadInvoice($invoiceId){

        $invoice           = DrugStorePayment::where('id', $invoiceId)->first();
        $drugStoreName     = DrugStore::where('id', $invoice->drug_store_id)->first()->name;
        $drugStoreAddress  = DrugStore::where('id', $invoice->drug_store_id)->first()->address;
        $startDate         = Carbon::createFromFormat('Y-m-d', $invoice->from_date)->toDateString();
        $endDate           = Carbon::createFromFormat('Y-m-d', $invoice->to_date)->toDateString();

        $last_date         = Carbon::createFromFormat('Y-m-d', $invoice->to_date)->addMonths(1)->format('Y-m')."-10";

        $fetchRows         = ChemistJobApplication::whereBetween('date', [$startDate, $endDate])
                                                    ->where('drug_store_id', $invoice->drug_store_id)
                                                    ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                    ->get();
        $totalWage = 0; $totalWorkHour = 0; $transportationCost = 0; $cusumptionTax = 0; $output = []; $i = 0; $cancelledArray = [];
        $totalCancelWage = 0; $totalCancelHour = 0; $completedJobArray = []; $totalJobHour = 0; $otalAfterExtra = 0;
        foreach($fetchRows as $row){
            if($row->status == 'Job Cancelled'){
                $to                                 = Carbon::createFromFormat('H:i:s', $row->time_from);
                $from                               = Carbon::createFromFormat('H:i:s', $row->time_to);
                $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                $diff_in_minutes                    = $to->diffInMinutes($from);
                $workHourinMinutes                  = $diff_in_minutes;
                $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->rest_time;
                $output[$i]['hourlyWage']           = $row->hourly_wage;
                $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->rest_time) / 60;
                $output[$i]['wageInMinutes']        = 110/60;
                $output[$i]['cancelWageInMinutes']  = $output[$i]['hourlyWage']/60;
                $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                $output[$i]['totalCancelWage']      = $output[$i]['totalWorkTime'] * $output[$i]['cancelWageInMinutes'];
                $output[$i]['total']                = $output[$i]['totalWage'];
                $output[$i]['date']                 = $row->date;
                $totalCancelHour    += $output[$i]['totalWorkHour'];
                $totalCancelWage    += $output[$i]['totalCancelWage'];
                $cancelledArray[] = ['chemist'           => $output[$i]['chemist'],
                                     'totalWorkHour'     => $output[$i]['totalWorkHour'],
                                     'date'              => $row->date,
                                     'hourly_wage'       => $output[$i]['hourlyWage'],
                                     'total_cancel_wage' => $output[$i]['totalCancelWage']
                                    ];
            }else{
                $to                                 = Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                $from                               = Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                $diff_in_minutes                    = $to->diffInMinutes($from);
                $workHourinMinutes                  = $diff_in_minutes;
               // $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time;
                $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work;
                //$output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time) / 60;
                $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work) / 60;
                $output[$i]['wageInMinutes']        = 110/60;
                $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                $output[$i]['total']                = $output[$i]['totalWage'];
                $output[$i]['date']                 = $row->date;
                $totalJobHour    += $output[$i]['totalWorkHour'];
                $completedJobArray[] = ['chemist'        => $output[$i]['chemist'],
                                        'totalWorkHour'  => $output[$i]['totalWorkHour'],
                                        'date'           => $row->date
                                       ];
            }
            $totalWage      += $output[$i]['total'];
            $totalWorkHour  += $output[$i]['totalWorkHour'];
            $i++;
        }
        $grandtotal      = $totalWage + 2200 + $totalCancelWage;
        $totalAfterExtra = ($invoice->input_quantity) ? $invoice->total_input + $grandtotal : 0;
        if($invoice->reduce_amount > 0){
            if($totalAfterExtra == 0){
                $totalAfterExtra = $grandtotal - $invoice->reduce_amount;
            }else{
                $totalAfterExtra = $totalAfterExtra - $invoice->reduce_amount;
            }
        }
        $pdf = PDF::loadView('store.invoice', compact('output', 'invoice', 'drugStoreName', 'drugStoreAddress',
                             'totalWage','totalWorkHour','grandtotal','last_date', 'cancelledArray', 'totalCancelHour', 'totalCancelWage',
                             'completedJobArray', 'totalJobHour', 'totalAfterExtra'));
        $pdf->setPaper('a4', 'portrait');
        return $pdf->stream('invoice.pdf');
    }

    public function downloadResume(Request $request){
        $chemist_id = $request->chemist;
        $chemist    = Chemist::find($chemist_id);
        // echo $chemist->city; exit;
        if(!$chemist){
            return response()->json(['status'=>false]);
        }
        $pdf = PDF::loadView('store.testresume',compact('chemist'));
        $pdf->setPaper('a4', 'portrait');
        return $pdf->stream('hello.pdf');
        // return $pdf->download('invoice.pdf');
    }

    public function cancelInspection(Request $request){

        try{
            ChemistInspectionTiming::where('id', $request->inspectionId)->delete();
            return response()->json(['message' => 'すべて削除しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '何かがうまくいかなかった', 'status' => FALSE]);
        }
    }

    public function drugStoreQrCode($drugStoreId){

        $drugStoreName = DrugStore::select('name')->where('id', $drugStoreId)->first()->name;
        return view('qr_code')->with('drugstoreId', $drugStoreId)
                              ->with('name', $drugStoreName);
    }



    public function withdrawEmployeeIntro(Request $request)
    {
        DrugStore::where('id', Auth::id())->update(['employee_intro_service'=> $request->employee_intro_service]);
        return response()->json(['message' => "true", 'status' => TRUE]);
    }


    public function withdrawNormalFarmatch(Request $request)
    {
        if($request->normal_farmatch==0)
        {
            DrugStore::where('id', Auth::id())->update(['normal_farmatch_disable'=> 1]);
        }
        else
        {
            return response()->json(['message' => "false", 'status' => FALSE]);
        }
        return response()->json(['message' => "true", 'status' => TRUE]);
    }

    public function employeeIntroForm(Request $request)
    {

        Session::put('employee_intro_service', $request->employee_intro_service);
        Session::put('automatic_renewal', $request->automatic_renewal);
        Session::put('normal_farmatch', $request->normal_farmatch);

        return response()->json(['message' => 'true', 'status' => TRUE]);
    }






}
