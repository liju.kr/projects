<?php

namespace App\Http\Controllers\Store;

use App\Mail\EmployeeIntroServiceExpiry;
use App\Mail\JobConfirmationMail;
use DateTime;
use GuzzleHttp\Client;
use App\Mail\SendStoreOtp;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendStoreForgotPassword;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use PDF;
use App\City;
use App\User;
use App\Chemist;
use App\JobPost;
use App\ReadStatus;
use App\DrugStore;
use App\GroupChat;
use App\Favourite;
use App\Prefecture;
use App\Notification;
use App\ChemistReview;
use App\DrugStoreChat;
use App\GroupChatHead;
use App\DrugStoreTiming;
use App\DrugStorePayment;
use App\StoreChemistChat;
use App\DrugStoreChatHead;
use App\AdminChemistChatHead;
use App\StoreChemistChatHead;
use App\ChemistQualification;
use App\ChemistJobApplication;
use App\ChemistInterviewTiming;
use App\ChemistInspectionTiming;



class HomeController extends Controller
{
    public function index(){

        return view('store.login');
    }

    public function signUp(){

        return view('store.signup');
    }

    public function forgotPassword(){

        return view('store.forgot_password');
    }

    public function resetPasswordLinkSend(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => '有効なメールアドレスを入力してください', 'status' => false]);
        } else {
            $email = DrugStore::select('*')->where('email', $request->email)->first();
            if ($email) {
                $email          = $email->email;
                $otp            = Str::random(5);
                $otp_expire     = Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
                $storeUpdate    = DrugStore::where('email', '=', $request->email)
                    ->update(['otp' => $otp, 'otp_expire' => $otp_expire]);
                if ($storeUpdate) {
                    $parameter = ['email' => $email, 'otp' => $otp];
                    $parameter = Crypt::encrypt($parameter);
                    $resetLink = URL::to('/') . "/drug-store/password-reset/" . $parameter;
                    try {
                        Mail::to($request->email)->send(new SendStoreForgotPassword($resetLink));
                        return response()->json(['message' => "パスワードリセットのためのリンクが登録済みのEメールアドレスに送信されました。メールボックスを確認してください。", 'status' => true]);
                    } catch (\Exception $e) {
                        return response()->json(['message' => '入力が正しくありません', 'status' => false]);
                    }
                } else {
                    return response()->json(['message' => "何かが間違っていました。もう一度やり直してください", 'status' => false]);
                }
            } else {
                return response()->json(['message' => 'このEメールアドレスを持つアカウントが見つかりません', 'status' => false]);
            }
        }
    }

    public function passwordReset($params){

        return view('store/password_reset')->with('params', $params)
            ->with('page_name', 'Password Reset');
    }

    public function passwordResetAction(Request $request){

        $validator = Validator::make($request->all(), [
            'params' => 'required',
            'password' => 'required',
            'confirm_password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'すべての項目に入力が必要です', 'status' => '0']);
        } else {
            $params = Crypt::decrypt($request->params);
            $email = $params['email'];
            $otp = $params['otp'];
            $storeUser = DrugStore::select('*')->where('email', $email)
                ->where('otp', $otp)
                ->first();
            if ($storeUser) {
                if (Carbon::parse($storeUser->otp_expire)->greaterThan(Carbon::now())) {
                    try {
                        $storeUserUpdate = DrugStore::where('otp', $otp)
                            ->where('email', $email)
                            ->update([
                                'password'   => Hash::make($request->password),
                                'otp'        => '',
                                'updated_at' => Carbon::now(),
                            ]);
                        return response()->json(['message' => "パスワードが更新されました。新しいパスワードでログインしてください。", 'status' => '1']);
                    } catch (\Exception $e) {
                        return response()->json(['message' => "何かが間違っていました。もう一度やり直してください", 'status' => false]);
                    }
                } else {
                    return response()->json(['message' => "OTPの有効期限が切れました", 'status' => false]);
                }
            } else {
                return response()->json(['message' => "リンクの期限が切れました", 'status' => false]);
            }
        }
    }

    public function otpVerification($number){

        return view('store/otp_verification')   ->with('number', $number)
                                                ->with('page_name', 'OTP Verification');
    }

    public function signUpAction(Request $request){

        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'in_charge' => 'required',
            'phone'     => 'required|unique:drug_stores,phone,Not Verified,status,deleted_at,NULL',
            'email'     => 'required|email|unique:drug_stores,email,Not Verified,status,deleted_at,NULL',
            'password'  => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
        ], [
            'latitude.required'  => '有効な場所を入力してください',
            'longitude.required' => '有効な場所を入力してください',
            'phone.unique'       => 'この番号はすでに使用されています',
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $request['password'] = Hash::make($request->password);
                $digits = 4;
                $request['phone_otp'] = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $request['phone_otp_expire'] = Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
                $request['is_verified']      = 0;
                unset($request['accept']);
                DrugStore::create($request->all());
                $data = ['otp' => $request['phone_otp']];
                $client = new Client();
                $res = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number=' . $request->phone . '&otp=' . $request['phone_otp'], []);
                $res->getStatusCode();
                $res->getHeader('content-type');
                $res->getBody();
                 Mail::to($request->email)->send(new SendStoreOtp($data));
                return response()->json(['message' => '登録された電話番号宛に確認コードを送信しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function otpResend($number){

        $storeUser = DrugStore::select('*')->where('phone', $number)->orderBy('created_at', 'desc')->first();
        if ($storeUser) {
            if (Carbon::parse($storeUser->phone_otp_expire)->greaterThan(Carbon::now())) {
                try {
                    $digits = 4;
                    $otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $storeUserUpdate = DrugStore::where('phone', $number)
                        ->orderBy('created_at', 'desc')->first()
                        ->update([
                            'phone_otp' => $otp,
                            'updated_at' => Carbon::now(),
                            'phone_otp_expire' => Carbon::now()->addHours(1)->format('Y-m-d H:i:s')
                        ]);
                    $data = ['otp' => $otp];
                    $client = new Client();
                    $res = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number=' . $storeUser->phone . '&otp=' . $otp, []);
                    $res->getStatusCode();
                    $res->getHeader('content-type');
                    $res->getBody();
                     Mail::to($storeUser->email)->send(new SendStoreOtp($data));
                    Session::flash('message', '登録された電話番号にワンタイムパスワードを送信しました。');
                    Session::flash('alert-class', 'alert-success');
                } catch (\Exception $e) {
                    Session::flash('message', '入力されたワンタイムパスワードが正しくありません');
                    Session::flash('alert-class', 'alert-danger');
                }
            } else {
                Session::flash('message', '入力されたワンタイムパスワードが正しくありません。');
                Session::flash('alert-class', 'alert-danger');
            }
        } else {
            Session::flash('message', '入力されたワンタイムパスワードが正しくありません。');
            Session::flash('alert-class', 'alert-danger');
        }
        return Redirect::back();
    }

    public function otpVerificationAction(Request $request){

        $validator = Validator::make($request->all(), [
            'inputs1' => 'required',
            'inputs2' => 'required',
            'inputs3' => 'required',
            'inputs4' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'すべての項目に入力が必要です', 'status' => '0']);
        } else {
            $otp = $request->inputs1 . $request->inputs2 . $request->inputs3 . $request->inputs4;
            $storeUser = DrugStore::select('*')->where('phone_otp', $otp)->first();
            if ($storeUser) {
                if (Carbon::parse($storeUser->phone_otp_expire)->greaterThan(Carbon::now())) {
                    try {
                        $storeUserUpdate = DrugStore::where('phone_otp', $otp)
                            ->update([
                                'phone_otp'        => '',
                                'phone_otp_expire' => '',
                                'status'           => 'Pending',
                                'is_verified'      => 1,
                                'updated_at'       => Carbon::now(),
                            ]);
                        return response()->json(['message' => "入力されたワンタイムパスワードが正常に確認されました。", 'status' => '1']);
                    } catch (\Exception $e) {
                        return response()->json(['message' => "入力されたワンタイムパスワードが正しくありません。", 'status' => false]);
                    }
                } else {
                    return response()->json(['message' => "入力されたワンタイムパスワードが正しくありません。", 'status' => false]);
                }
            } else {
                return response()->json(['message' => "入力されたワンタイムパスワードが正しくありません。", 'status' => false]);
            }
        }
    }

    public function loginAction(Request $request){

        $drugStore = DrugStore::select('*') ->where('email', request('email'))
                                            ->whereIn('status', ['Approved', 'Pending'])
                                            ->first();
        if ($drugStore) {
            if (Auth::guard('stores')->attempt(['email' => $drugStore->email, 'password' => request('password'), 'is_verified' => 1])) {
                session::put('name', $drugStore->name);
                session::put('userId', $drugStore->id);
                session::put('loginTime', Carbon::now());
                if ($drugStore->status == 'Approved' &&  $drugStore->is_approved_login == 0){
                    $verifyStatus = 1;
                    DrugStore::where('id', $drugStore->id)
                                ->update(['is_approved_login' =>  1]);
                }elseif($drugStore->status == 'Approved' &&  $drugStore->is_approved_login == 1){
                    $verifyStatus = 2;
                }else{
                    $verifyStatus = 3;
                }

                if(!$drugStore->first_login){
                    $this->sendFirstLoginMessage($drugStore->id);
                    $drugStore->first_login = 1;
                    $drugStore->save();
                }

                DrugStore::where('id', $drugStore->id)
                           ->update(['online_status'     => 'Online']);
                return response()->json(['message' => 'ログインが完了しました。', 'status' => true, 'verify_status' => $verifyStatus]);
            } else {
                return response()->json(['message' => 'パスワードに誤りがあります。', 'status' => false]);
            }
        } else {
            return response()->json(['message' => 'このEメールアドレスを持つアカウントが見つかりません。', 'status' => false]);
        }
    }

    public function sendFirstLoginMessage($id){

        $chathead = DrugStoreChatHead::where('drug_store_id', $id)->get()->first();
        if (!$chathead) {
            $chathead = new DrugStoreChatHead();
            $chathead->drug_store_id = $id;
        }

        $chathead->last_message = "ご登録ありがとうございます。<br>
        担当者が詳細についてご説明に伺います。　<br>
        希望日と時間を第三希望日まで<br>
        お知らせください。<br>
        第一希望　○月○日　△時〜△時<br>
        第二希望　○月○日　△時〜△時<br>
        第三希望　○月○日　△時〜△時<br>
        説明の所要時間は1時間程度です。";
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new DrugStoreChat();
        $chat->drug_store_chat_head_id = $chathead->id;
        $chat->message = "ご登録ありがとうございます。<br>
        担当者が詳細についてご説明に伺います。　<br>
        希望日と時間を第三希望日まで<br>
        お知らせください。<br>
        第一希望　○月○日　△時〜△時<br>
        第二希望　○月○日　△時〜△時<br>
        第三希望　○月○日　△時〜△時<br>
        説明の所要時間は1時間程度です。";
        $chat->admin_read_status = 1;
        $chat->admin = 1;
        $chat->save();

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/farmatch-ce7f6-eff1b65cc8a6.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://farmatch-ce7f6.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $newPost = $database
        ->getReference('admin/drugstore/'.$id)
        ->push([
            "admin" => 1,
            "message"=> "ご登録ありがとうございます。<br>
            担当者が詳細についてご説明に伺います。　<br>
            希望日と時間を第三希望日まで<br>
            お知らせください。<br>
            第一希望　○月○日　△時〜△時<br>
            第二希望　○月○日　△時〜△時<br>
            第三希望　○月○日　△時〜△時<br>
            説明の所要時間は1時間程度です。",
            "timestamp"=> time()
        ]);
    }

    public function logout(){

        DrugStore::where('id', Auth::id())
                    ->update(['online_status' => 'Offline']);
        Auth::logout();
        Session::flush();
        return redirect()->route('drug-store-login');
    }

    public function dashboard(){

        $jobPosts = JobPost::select('id', 'job_name', 'job_title', 'content', 'date')
                            ->where('date', Carbon::now()->format('Y-m-d'))
                            ->where('drug_store_id', Auth::id())
                            ->where('status', 'Active')
                            ->orderBy('id', 'DESC')
                            ->get();
        $jobPostsCalender = JobPost::select('id', 'job_name', 'job_title', 'content', 'date')
                                    ->where('drug_store_id', Auth::id())
                                    ->where('status', 'Active')
                                    ->orderBy('id', 'DESC')
                                    ->get();
        $calenderArray = [];
        $i = 0;
        foreach ($jobPostsCalender as $row) {
            $calenderArray[$i]['title']     = $row->job_name;
            $calenderArray[$i]['start']     = $row->date;
            $calenderArray[$i]['url']       = url('drug-store/job-post-details/' . $row->id);
            $calenderArray[$i]['className'] = 'fc-event-solid-success fc-event-light';
            $i++;
        }
        return view('store.dashboard')->with('jobPosts', $jobPosts)
            ->with('calenderArray', json_encode($calenderArray))
            ->with('page_title', '投稿管理');
    }

    public function homeDashboard(Request $request){

        $drugStoreTiming        =   DrugStoreTiming::select('day', 'day_japan', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->get();
        $drugStoreFromTo        =   DrugStoreTiming::select('day', 'time_from', 'time_to')->where('drug_store_id', Auth::id())->first();
        $jobPostsCalender       =   JobPost::select('id', 'job_name', 'job_title', 'content', 'date')
                                            ->where('drug_store_id', Auth::id())
                                            ->where('status', '!=', 'Rejected')
                                            ->get();
        $cancelledApplications  =   ChemistJobApplication::
                                            join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                            ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                            ->select('job_name', 'chemist_id', 'first_name', 'last_name', 'chemist_job_applications.id', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                            ->where('chemist_job_applications.chemist_cancelled', 'Yes')
                                            ->where('job_posts.drug_store_id', Auth::id())
                                            ->where('job_posts.date', '>=' ,date('Y-m-d'))
                                            ->get();
        $acceptedApplications   =   ChemistJobApplication::
                                            join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                            ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                            ->select('chemist_job_applications.status', 'job_name', 'chemist_id', 'first_name', 'last_name', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                            ->whereIn('chemist_job_applications.status', [ 'Accepted', 'Chemist Accept' ])
                                            ->where('job_posts.drug_store_id', Auth::id())
                                            ->where('job_posts.date', '>=' ,date('Y-m-d'))
                                            ->get();
        $paymentInformations    =   DrugStorePayment::where('drug_store_id', Auth::id())
                                                        ->where('active_status', 'Active')
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
        $pendingInterviews      =   ChemistInterviewTiming::select('chemist_id', 'day', 'time_from', 'time_to')
                                                            ->where('drug_store_id', Auth::id())
                                                            ->where('status', 'Pending')
                                                            ->get();
        $pendingInspections     =   ChemistInspectionTiming::select('id', 'chemist_id', 'day', 'time_from', 'time_to')
                                                            ->where('drug_store_id', Auth::id())
                                                            ->get();
        //$status                 =   [ "Accepted" => '承認', "Chemist Accept" => '薬剤師承認済み', "Chemist Reject" => '薬剤師拒否済み', "Job Cancelled" => '勤務案件キャンセル', "Job Completed" => '業務完了', "Pending" => '申請中', "Rejected" => '拒否'];
        $status                 =   [ "Accepted" => '薬剤師の承認待ち', "Chemist Accept" => '薬剤師承認済み', "Chemist Reject" => '薬剤師拒否済み', "Job Cancelled" => '勤務案件キャンセル', "Job Completed" => '業務完了', "Pending" => '申請中', "Rejected" => '拒否'];
        foreach ($pendingInterviews as $row) {
            $row->chemist = Chemist::select('first_name', 'last_name', 'gender', 'image')->where('id', $row->chemist_id)->first();
        }
        foreach ($pendingInspections as $row) {
            $row->chemist = Chemist::select('first_name', 'last_name', 'gender', 'image')->where('id', $row->chemist_id)->first();
        }
        foreach($acceptedApplications as $row){
            $row->actual_status = $row->status;
            $row->status  = $status[$row->status];
        }
        $calenderArray = $dayArray = $pendingArray = [];
        $i = $k =  0;
        foreach ($jobPostsCalender as $row) {

            if(Carbon::parse($row->date)->startOfDay()->gte(Carbon::now()->startOfDay())){

                $checkIsPending = $this->checkIsPending($row->id);
                if($checkIsPending > 0){
                    $class                         = 'fc-event-primary fc-event-light';
                    $url                           = url('drug-store/pending-application-for-each-post/' . $row->id);
                    $title                         = $row->job_name;
                    $pendingArray[$k]['title']     = $title;
                    $pendingArray[$k]['start']     = $row->date;
                    $pendingArray[$k]['url']       = $url;
                    $pendingArray[$k]['className'] = $class;
                    $k++;
                }
                // else{
                //     $class = 'fc-event-solid-primary fc-event-light';
                //     $url   = "#";
                // }

            }
            $reviewStatus = $this->checkReviewStatus($row->date);
            if ($reviewStatus == 'Not Review') {
                $checkConfirmedJobs = $this->checkConfirmedJobs($row->id);
                if($checkConfirmedJobs > 0){
                    $getConfirmedJobs = $this->getconfirmedJobs($row->id);
                    $class       = 'fc-event-primary fc-event-light';
                    $title       = $row->job_name;
                    $description = $getConfirmedJobs;
                    $url         = url('drug-store/drug-store-review-all/' . $row->id);
                }else{
                    $class       = 'fc-event-solid-success fc-event-light';
                    $title       = $row->job_name;
                    $description = "";
                    $url   = url('drug-store/drug-store-review-all/' . $row->id);
                }
            } else {
                $checkAllReviewed = $this->checkAllReviewed($row->id);
                if($checkAllReviewed == 1){
                    $class       = 'fc-event-solid-gray fc-event-light';
                    $title       = $row->job_name;
                    $description = "";
                    $url         = url('drug-store/drug-store-review/' . $row->id);
                }else{
                    $isReviewJob = $this->checkIsReviewJob($row->id);
                    if($isReviewJob == 0){
                        $class       = 'fc-event-solid-success fc-event-light';
                        $title       = $row->job_name;
                        $description = "";
                        $url         = url('drug-store/drug-store-review/' . $row->id);
                    }else{
                        $class       = 'fc-event-success fc-event-light';
                        $title       = $row->job_name;
                        $description = "";
                        $url         = url('drug-store/drug-store-review/' . $row->id);
                    }
                }
            }
            $calenderArray[$i]['title']        = $title;
            $calenderArray[$i]['description']  = $description;
            $calenderArray[$i]['start']        = $row->date;
            $calenderArray[$i]['url']          = $url;
            $calenderArray[$i]['className']    = $class;
            $i++;
        }



        return view('store.settings')->with('drugStoreTiming', $drugStoreTiming)
                                    ->with('drugStoreFromTo', $drugStoreFromTo)
                                    ->with('calenderArray', json_encode($calenderArray))
                                    ->with('cancelledApplications', $cancelledApplications)
                                    ->with('pendingApplications', json_encode($pendingArray))
                                    ->with('paymentInformations', $paymentInformations)
                                    ->with('acceptedApplications', $acceptedApplications)
                                    ->with('pendingInterviews', $pendingInterviews)
                                    ->with('pendingInspections', $pendingInspections)
                                    ->with('page_title', 'ダッシュボード');
    }

    public function checkIsReviewJob($postId){

        $completedjobsCount  = ChemistJobApplication::where('job_post_id', $postId)->where('status', 'Job Completed')->get()->count();
        if($completedjobsCount > 0)
            return 1;
        else
            return 0;
    }

    public function getconfirmedJobs($postId){

        $confirmedJobs  =   ChemistJobApplication::
                                                join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                ->select('first_name', 'last_name', 'time_from', 'time_to')
                                                ->whereIn('chemist_job_applications.status', [ 'Chemist Accept' ])
                                                ->where('job_posts.id', $postId)
                                                ->get();
        $cancelledCount =   ChemistJobApplication::
                                                join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                ->select('first_name', 'last_name', 'time_from', 'time_to')
                                                ->where('chemist_job_applications.chemist_cancelled', 'Yes')
                                                ->where('job_posts.id', $postId)
                                                ->get();
        $jobs = "<p><span class='pull-left'>確定済み : <b>".$confirmedJobs->count()."</b>&nbsp;&nbsp;"."<span class='pull-right'>キャンセル : <b>".$cancelledCount->count()."</b></span></p><br>";
        foreach($confirmedJobs as $row){
            $jobs  .= "<h6>".$row->last_name." ".$row->first_name." : ".Carbon::createFromFormat('H:i:s', $row->time_from)->format('H:i')." ~ ".Carbon::createFromFormat('H:i:s', $row->time_to)->format('H:i')."</h6>";
        }
        return $jobs;
    }

    public function checkAllReviewed($postId){

        $completedjobsCount     = ChemistJobApplication::where('job_post_id', $postId)->where('status', 'Job Completed')->get()->count();
        $completedReviewCount   = ChemistReview::where('job_post_id', $postId)->get()->count();
        if($completedjobsCount > 0){
            if($completedjobsCount == $completedReviewCount)
                return 1;
            else
                return 0;
        }else{
            return 0;
        }
    }

    public function checkConfirmedJobs($postId){

        $confirmedJobs = ChemistJobApplication::where('job_post_id', $postId)->where('status', 'Chemist Accept')->get()->count();
        return $confirmedJobs;
    }

    public function checkIsPending($jobId){

        $pendingApplication = ChemistJobApplication::where('job_post_id', $jobId)->where('status', 'Pending')->get()->count();
        return $pendingApplication;
    }

    public function drugStoreReview($postId){

        $applications   =   ChemistJobApplication::join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                ->select('chemist_id', 'first_name', 'last_name', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                                ->where('chemist_job_applications.status', 'Job Completed')
                                                ->where('job_posts.id', $postId)
                                                ->get();
        $jobPost        =   JobPost::where('id', $postId)->first();

        if (Carbon::parse($jobPost->date)->gt(Carbon::now()) && ($jobPost->status != 'Rejected'))
            $isCancel = true;
        else
            $isCancel = false;


        foreach ($applications as $row) {
            if ($isExist = ChemistReview::where('job_post_id', $postId)->where('chemist_id', $row->chemist_id)->exists())
                $row->is_reviewed   = 1;
            else
                $row->is_reviewed   = 0;
        }
        return view('store.review') ->with('applications', $applications)
                                    ->with('postId', $postId)
                                    ->with('jobName', $jobPost->job_name)
                                    ->with('vaccancies', $jobPost->no_of_vaccancies)
                                    ->with('page_title', '案件詳細')
                                    ->with('isCancel', $isCancel);
    }

    public function drugStoreReviewAll($postId){

        $applications   =   ChemistJobApplication::join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                ->select('chemist_job_applications.id', 'chemist_id', 'first_name', 'last_name', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                                ->where('chemist_job_applications.status', 'Chemist Accept')
                                                ->where('job_posts.id', $postId)
                                                ->get();
        $jobPost        =   JobPost::where('id', $postId)->first();

        if (Carbon::parse($jobPost->date)->gt(Carbon::now()) && ($jobPost->status != 'Rejected'))
            $isCancel = true;
        else
            $isCancel = false;


        foreach ($applications as $row) {
            if ($isExist = ChemistReview::where('job_post_id', $postId)->where('chemist_id', $row->chemist_id)->exists())
                $row->is_reviewed   = 1;
            else
                $row->is_reviewed   = 0;
        }


        return view('store.review_all') ->with('applications', $applications)
                                        ->with('postId', $postId)
                                        ->with('jobName', $jobPost->job_name)
                                        ->with('vaccancies', $jobPost->no_of_vaccancies)
                                        ->with('page_title', '案件詳細')
                                         ->with('isCancel', $isCancel);
    }

    public function drugStoreAcceptedApplications($postId, $applicationId, $notificationId){

        Notification::where('id', $notificationId)
                    ->update(['hyperlink_read_status' => 'Read']);
        $applications   =   ChemistJobApplication::join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                ->select('chemist_job_applications.id', 'chemist_id', 'first_name', 'last_name', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                                ->where('chemist_job_applications.status', 'Chemist Accept')
                                                ->where('job_posts.id', $postId)
                                                ->get();
        $jobPost        =   JobPost::where('id', $postId)->first();

        if (Carbon::parse($jobPost->date)->gt(Carbon::now()) && ($jobPost->status != 'Rejected'))
            $isCancel = true;
        else
            $isCancel = false;

        foreach ($applications as $row) {
            if ($isExist = ChemistReview::where('job_post_id', $postId)->where('chemist_id', $row->chemist_id)->exists())
                $row->is_reviewed   = 1;
            else
                $row->is_reviewed   = 0;
        }
        return view('store.review_all') ->with('applications', $applications)
                                        ->with('postId', $postId)
                                        ->with('jobName', $jobPost->job_name)
                                        ->with('vaccancies', $jobPost->no_of_vaccancies)
                                        ->with('page_title', '案件詳細')
                                        ->with('isCancel', $isCancel);
    }

    public function markAsReadCancelled(Request $request){

        Notification::where('id', $request->notificationId)
                    ->update(['hyperlink_read_status' => 'Read']);
        return response()->json([ 'status' => 'success']);

    }

    public function reviewed(Request $request){

        return view('store.reviewed')->with('page_title', '案件詳細');
    }

    public function review(Request $request){

        return view('store.review')->with('page_title', '案件詳細');
    }

    public function pendingReview(Request $request){

        return view('store.prending_review')->with('page_title', '案件詳細');
    }

    public function notification(){

        $notifications = Notification::where('drug_store_id', Auth::id())
                                        ->OrderBy('id', 'DESC')
                                        ->get();
        Notification::where('drug_store_id', Auth::id())
                    ->update(['read_status' => 1]);
        foreach($notifications as $row){
            if($row->job_application_id!=null)
            {
                $jobApplication = ChemistJobApplication::where('id', $row->job_application_id)->withTrashed()->first();
                if($jobApplication)
                {
                    $row->reason = $jobApplication->reason_for_rejection;
                    $row->reason_other = $jobApplication->reason_for_rejection_other;
                }
                else
                {
                    $row->reason = $jobApplication->reason_for_rejection;
                    $row->reason_other = $jobApplication->reason_for_rejection_other;
                }

            }
            else
            {
                $row->reason = "";
                $row->reason_other = "";
            }


        }



        return view('store.notification') ->with('notifications', $notifications)
                                          ->with('page_title', 'お知らせ');
    }

    public function chat(){

        return view('store.chat')->with('page_title', 'チャット');
    }

    public function groupChat(Request $request){

        $store = "";
        $chemist = "";
        if ($request->chemist) {
            $chemist_details = Chemist::find($request->chemist);

            if ($chemist_details) {
                $chathead = GroupChatHead::where('drug_store_id', Auth::user()->id)->where('chemist_id', $request->chemist)->get()->first();
                if (!$chathead) {
                    $chathead = new GroupChatHead();
                    $chathead->drug_store_id = Auth::user()->id;
                    $chathead->chemist_id = $request->chemist;
                    $chathead->last_message = "";
                }

                $chathead->updated_at = now();
                $chathead->save();

                $store = $store_details->id;
                $chemist = $chemist_details->id;
            }
        }elseif(\Session::has('chemist')){
            $chemist =  session('chemist');
        }

        $search = $request->search;
        $chatheads = GroupChatHead::select('group_chat_heads.*')
            ->where(function ($q) use ($search) {
                $q->orWhere('chemists.first_name', 'like', "%{$search}%");
                $q->orWhere('chemists.last_name', 'like', "%{$search}%");
                $q->orWhere('chemists.email', 'like', "%{$search}%");
                $q->orWhere('chemists.phone', 'like', "%{$search}%");
                $q->orWhere('drug_stores.name', 'like', "%{$search}%");
                $q->orWhere('drug_stores.email', 'like', "%{$search}%");
                $q->orWhere('drug_stores.phone', 'like', "%{$search}%");
            })
            ->where('group_chat_heads.drug_store_id', Auth::user()->id)
            ->join('chemists', 'chemists.id', '=', 'chemist_id')
            ->join('drug_stores', 'drug_stores.id', '=', 'drug_store_id')
            ->orderBy('group_chat_heads.updated_at', 'desc')
            ->get();

        $data = [
            'chatheads' =>  $chatheads, //GroupChatHead::where('drug_store_id',Auth::user()->id)->orderBy('updated_at','desc')->get(),
            'search'    => $search,
            'chemist'   => $chemist
        ];
        return view('store.group_chat', $data)->with('page_title', 'グループチャット');
    }

    public function getChemistDetails(Request $request){

        $chemist_details = Chemist::find($request->id);
        return response()->json(['chemist' => $chemist_details, 'status' => true,]);
    }

    public function saveGroupChat(Request $request){

        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', Auth::user()->id)->where('chemist_id', $request->id)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = Auth::user()->id;
            $chathead->chemist_id = $request->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new GroupChat();
        $chat->group_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->store_read_status = 1;
        $chat->user = 2;
        $chat->save();

        if($chathead->chemist && $chathead->chemist->device_token){
            $data = [
                'type' => "group_chat",
                'store_id'=> $chathead->store->id,
                'store_image'=> $chathead->store->main_image,
                'store_name'=> $chathead->store->name,
                'admin_image'=> User::first()->image,
                'user_id'=>$chathead->chemist->id
            ];
            $this->sendNotification($chathead->store->name.' から新着メッセージがあります', $request->message, $chathead->chemist->device_token,$data, $request->id);
        }

        return response()->json(['status' => true]);
    }

    public function pickAChemist(Request $request){

        $query_string = $request->q;
        $chemists = Chemist::where(function ($q) use ($query_string) {
            $q->orWhere('first_name', 'like', "%{$query_string}%");
            $q->orWhere('last_name', 'like', "%{$query_string}%");
            $q->orWhere('email', 'like', "%{$query_string}%");
            $q->orWhere('phone', 'like', "%{$query_string}%");
            $q->orWhere('licence_number', 'like', "%{$query_string}%");
        })->where('status', 'Approved')
            ->limit(20)->get();

        $response = [];
        if ($chemists->count() > 0) {
            $i = 0;
            foreach ($chemists as $chemist) {
                $response[$i]['id'] = $chemist->id;
                $response[$i]['text'] = $chemist->full_name . "(" . $chemist->email . ")";
                $i++;
            }
        }
        return response()->json(['results' => $response]);
    }

    public function startNewGroupChatAdmin(Request $request){

        $validator = Validator::make($request->all(), [
            'store' => 'required',
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = $request->chemist;
            $chathead->last_message = "";
        }

        $chathead->updated_at = now();
        $chathead->save();

        \Session::flash('chemist', $chathead->chemist_id);
        return response()->json(['status' => true]);
    }


    public function addJobPost(Request $request){

        $validator = Validator::make($request->all(), [
            'job_name'         => 'required',
            'dates'            => 'required',
            'work_from'        => 'required',
            'work_to'          => 'required',
            'no_of_vaccancies' => 'required',
            'content'          => 'required',
            'content_others'   => 'sometimes',
            'reason'           => 'required',
            'is_editable'      => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $DrugStoreTiming  = DrugStoreTiming::where('drug_store_id', Auth::id())->count();
                $address          = DrugStore::where('id', Auth::id())->first()->address;
                $mainImage        = DrugStore::where('id', Auth::id())->first()->main_image;
                $historyModel     = DrugStore::where('id', Auth::id())->first()->history_model;
                if($DrugStoreTiming == 0 || $address == '' || $mainImage == '' || $historyModel == '' ){
                    return response()->json(['message' => '新規投稿をする前にプロフィール設定の入力を完了してください', 'status' => false]);
                }
                if ($request->reason == 'Others') {
                    if ($request->others == '') {
                        return response()->json(['message' => 'すべての項目に入力が必要です', 'status' => false]);
                    }
                }
                $fromTime = Carbon::createFromFormat('H:i', $request->work_from)->format('H:i:s');
                $toTime = Carbon::createFromFormat('H:i', $request->work_to)->format('H:i:s');
                if (Carbon::parse($fromTime)->floatDiffInHours($toTime, false) <= 0) {
                    return response()->json(['message' => '「開始時間」は「終了時間」よりも早い時間である必要があります。', 'status' => false]);
                }
                $dates = explode(",", $request->dates);
                foreach ($dates as $row) {
                    // print_r(Carbon::now()->toDateTimeString());exit;
                    // print_r(Carbon::createFromFormat('Y-m-d H:i:s', $row . ' ' .$toTime)->toDateTimeString());exit;
                    $current_date = Carbon::now();
                    $from_date = Carbon::createFromFormat('Y-m-d H:i:s', $row . ' ' .$fromTime);
                    $result = $current_date->gte($from_date);
                    if($result) {
                        return response()->json([
                            'status' => false,
                            'message' => '選択した日時はすでに過ぎています。修正して、もう一度お試しください'
                        ]);
                    }
                    $post = new JobPost();
                    $post->drug_store_id    = Auth::id();
                    $post->job_name         = $request->job_name;
                    $post->date             = $row;
                    $post->work_from        = $fromTime;
                    $post->work_to          = $toTime;
                    $post->no_of_vaccancies = $request->no_of_vaccancies;
                    $post->content          = implode(',', $request->content);
                   if(isset($request->content_others))  $post->content_others          = $request->content_others;
                    $post->precautions      = $request->precautions;
                    $post->reason           = $request->reason;
                    $post->others           = $request->others;
                    $post->is_editable      = $request->is_editable;
                    $post->save();

                    // select fav chenists
                    $fav_chem = Favourite::where('drug_store_id', Auth::id())
                        ->where('status', 1)
                        ->get();

                    // save read status as false for each date
                    foreach($fav_chem as $chem) {
                        $read_status = new ReadStatus();
                        $read_status->job_post_id = $post->id;
                        $read_status->chemist_id = $chem->chemist_id;
                        $read_status->drug_store_id = Auth::id();
                        $read_status->date = Carbon::now()->toDateString();
                        $read_status->read_status = false;
                        $read_status->save();
                    }
                }
                // $favouriteChemists = Favourite::select('chemist_id')->where('drug_store_id', Auth::id())
                //                               ->where('status', 1)
                //                               ->get();
                $favouriteChemists = Favourite::where('drug_store_id', Auth::id())
                                              ->where('status', 1)
                                              ->get();
                $drugStoreName = DrugStore::find(Auth::id())->name;
                foreach($favouriteChemists as $row){
                    $data = [
                        'type' => 'favourite',
                        'drug_store_id' => $row->drug_store_id,
                        'id' => $post->id
                    ];
                    $this->sendNotification($drugStoreName.'が勤務案件を投稿しました', '定員になる前にチェックしましょう！', Chemist::find($row->chemist_id)->device_token, $data, $row->chemist_id);
                }
                return response()->json(['message' => '正常に追加されました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage(), 'status' => false]);
            }
        }
    }

    public function postDetails($postId){

        $postDetails = JobPost::where('id', $postId)->first();
        if (Carbon::parse($postDetails->date)->gt(Carbon::now()) && ($postDetails->status != 'Rejected'))
            $isCancel = true;
        else
            $isCancel = false;
        return view('store.job_post_details')->with('postDetails', $postDetails)
            ->with('isCancel', $isCancel)
            ->with('page_title', '募集案件詳細');
    }

    public function updatePostDetails(Request $request){

        $validator = Validator::make($request->all(), [
            'job_name'         => 'required',
            'no_of_vaccancies' => 'required',
            'reason'           => 'required',
            'content'          => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $vaccancies = JobPost::where('id', $request->post_id)->first()->no_of_vaccancies;
                if($request->no_of_vaccancies < $vaccancies){
                    return response()->json(['message' => '募集人数を初期設定の人数以下に変更することはできません', 'status' => false]);
                }
                JobPost::where('id', $request->post_id)->update([
                    'job_name'         => request('job_name'),
                    'no_of_vaccancies' => request('no_of_vaccancies'),
                    'reason'           => request('reason'),
                    'others'           => request('others'),
                    'content'          => request('content'),
                    'precautions'      => request('precautions')
                ]);
                return response()->json(['message' => '更新しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function storeTerms(){

        return view('store.terms')->with('page_title', 'チャット');
    }

    public function adminChat(){

        return view('store.admin_chat')->with('page_title', 'ふぁーまっち運営本部　チャット', 'admin_image');
    }

    public function saveChat(Request $request){

        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = DrugStoreChatHead::where('drug_store_id', Auth::user()->id)->get()->first();
        if (!$chathead) {
            $chathead = new DrugStoreChatHead();
            $chathead->drug_store_id = Auth::user()->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new DrugStoreChat();
        $chat->drug_store_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->store_read_status = 1;
        $chat->save();

        return response()->json(['status' => true]);
    }

    public function addReview(Request $request){

        $validator = Validator::make($request->all(), [
            'attitude'          => 'required',
            'adapting'          => 'required',
            'work_speed'        => 'required',
            'value_for_money'   => 'required',
            'others'            => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $attributes                  = $request->all();
                unset($attributes['post_id']);
                $attributes['chemist_id']    = $request->chemist_id;
                $attributes['job_post_id']   = $request->post_id;
                $attributes['drug_store_id'] = Auth::user()->id;
                ChemistReview::insert($attributes);
                return response()->json(['message' => '正常に追加されました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function rejectChemistApplication(Request $request){

        $validator = Validator::make($request->all(), [
            'applicationId' => 'required',
            'rejectReason'  => 'required',
            'rejectReasonOther'  => 'sometimes|max:20'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
//                ChemistJobApplication::where('id', $request->applicationId)->update([
//                    'status'               => 'Rejected',
//                    'reason_for_rejection' => request('rejectReason'),
//                ]);

                $chemistJobApplication = ChemistJobApplication::findOrFail($request->applicationId);
                $chemistJobApplication->status = "Rejected";
                $chemistJobApplication->reason_for_rejection = request('rejectReason');
                if(request('rejectReasonOther')) $chemistJobApplication->reason_for_rejection_other = request('rejectReasonOther');
                $chemistJobApplication->save();

                $application = ChemistJobApplication::where('id', $request->applicationId)->first();
                $notification                       = new Notification();
                $notification->type                 = 'Chemist';
                $notification->notification_type    = 'Job Application';
                $notification->chemist_id           = $application->chemist_id;
                $notification->job_post_id          = ChemistJobApplication::where('id', $request->applicationId)->first()->job_post_id;
                $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $request->applicationId)->first()->drug_store_id)->first();
                $notification->image                = $drugStore->main_image;
                $notification->drug_store_address   = $drugStore->address;
                $notification->drug_store_model     = $drugStore->history_model;
                $notification->job_application_id   = $request->applicationId;
                //$notification->title                = '勤務申請が拒否されました';
                $notification->title                = '勤務申請が成立しませんでした';
                //if($application->reason_for_rejection == "Others") $reason = $application->reason_for_rejection_other; else $reason = $application->reason_for_rejection;
                $reason ="";
                $notification->message              = $application->date . 'の勤務申請が薬局/病院に受付けられませんでした '.$reason;
                $notification->save();
                $deviceToken        =   Chemist::where('id', $notification->chemist_id)->first()->device_token;
                $this->sendNotification('勤務申請が成立しませんでした', ChemistJobApplication::where('id', $request->applicationId)->first()->date . 'の勤務申請が薬局/病院に受付けられませんでした '.$reason, $deviceToken, NULL, $notification->chemist_id);
                return response()->json(['message' => '更新しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function cancelJobPost(Request $request){

        $validator = Validator::make($request->all(), [
            'postId' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        try {
            $jobDate        =   JobPost::where('id', $request->postId)->first()->date;
            $date           =   Carbon::parse($jobDate);
            $now            =   Carbon::now();
            $diffDays       =   $date->diffInDays($now);
            if ($diffDays > 7) {
                $percentage = 0;
            } elseif ($diffDays >= 4 && $diffDays <= 7) {
                $percentage = 50;
            } else {
                $percentage = 100;
            }
            JobPost::where('id', $request->postId)->update(['status' => 'Rejected', 'deleted_at' => $now]);
            $applicants     =   ChemistJobApplication::select('id', 'chemist_id')
                                                       ->whereIn('status', ['Accepted', 'Chemist Accept', 'Pending'])
                                                       ->where('job_post_id', $request->postId)
                                                       ->get();

            ChemistJobApplication::where('job_post_id', $request->postId)
                                    ->where('status', '!=', 'Chemist Accept')->update([ 'status'  => 'Rejected']);
            ChemistJobApplication::where('job_post_id', $request->postId)
                                    ->where('status', 'Chemist Accept')->update([ 'status'                  => 'job Cancelled',
                                                                                  'cancellation_percentage' => $percentage
                                                                               ]);
            Notification::where('job_post_id', $request->postId)->delete();
            $i = 0;
            if ($applicants->count()) {
                foreach ($applicants as $row) {

                    $notification[$i]['type']                 = 'Chemist';
                    $notification[$i]['notification_type']    = 'Job Application';
                    $notification[$i]['chemist_id']           = $row->chemist_id;
                    $notification[$i]['job_post_id']          = ChemistJobApplication::where('id', $row->id)->first()->job_post_id;
                    $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $row->id)->first()->drug_store_id)->first();
                    $notification[$i]['image']                = $drugStore->main_image;
                    $notification[$i]['drug_store_address']   = $drugStore->address;
                    $notification[$i]['drug_store_model']     = $drugStore->history_model;
                    $notification[$i]['job_application_id']   = $row->id;
                    // $notification[$i]['title']                = '勤務申請が受付けられました';
                    $notification[$i]['title']                = '勤務がキャンセルされました';
                    $notification[$i]['message']              = $jobDate . ' の勤務案件がキャンセルされました';
                    $deviceToken            =   Chemist::where('id', $row->chemist_id)->first()->device_token;
                    $this->sendNotification(' 確定済みの案件のキャンセル', $jobDate . ' の勤務案件がキャンセルされました', $deviceToken, NULL, $row->chemist_id);
                    $i++;
                }
                Notification::insert($notification);
            }
            return response()->json(['message' => '募集申請をキャンセルしました', 'status' => true]);
        } catch (\Exception $e) {
            return response()->json(['message' => '入力が正しくありません', 'status' => false]);
        }
    }

    public function getChemistInformation(Request $request) {

        $validator = Validator::make($request->all(), [
            'chemistId' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                $Chemist                        = Chemist::where('id', $request->chemistId)->first();
                $output['name']                 = $Chemist->last_name." ".$Chemist->first_name;
                $output['gender']               = ($Chemist->gender == 'Male') ? "男" : "女" ;
                $age = $this->calculateAge($Chemist->dob);
                $output['age']                  = $age;
                $output['address']              = trim($Chemist->prefecture.",".$Chemist->city, ",");
                $output['image']                = ($Chemist->image) ? asset("/images/chemist/" . $Chemist->image) : asset("/store/images/profile.jpg");
                $output['introduction']         = ($Chemist->self_introduction) ? $Chemist->self_introduction : "";
                $output['child']                = ($Chemist->have_child) ? (($Chemist->have_child == 'Yes') ? "有": "無") : "";
                $output['skills']               = ($Chemist->desired_entry) ? $Chemist->desired_entry : "";
                $output['response_time']        = ($Chemist->response_time) ? $Chemist->response_time : "";
                $qualifications = ChemistQualification::where('chemist_id', $request->chemistId)->get();
                $qualification  = '';
                foreach($qualifications as $row){
                    if($row->content)
                        $qualification .= $row->content.",";
                }
                $output['qualifications']       = rtrim($qualification,",");
                $from       =   Carbon::parse($Chemist->created_at);
                $to         =   Carbon::now();
                $difference =   $to->diffInYears($from) . " 年" . $to->diffInMonths($from) . " ヶ月";

                $completedWorks = ChemistJobApplication::where('chemist_id', $request->chemistId)->where('status', 'job Completed')->count();
                $noOfDrugStores = ChemistJobApplication::where('chemist_id', $request->chemistId)->groupBy('drug_store_id')->count();
                $chemistRepeatWorked    = ChemistJobApplication::where('chemist_id', $request->chemistId)
                                                                ->groupBy('chemist_id')
                                                                ->where('status', 'Job Completed')
                                                                ->selectRaw('chemist_id, count(chemist_id) as count')
                                                                ->get();
                $suddenCancellation     = ChemistJobApplication::where('chemist_id', $request->chemistId)
                                                                ->where('status', 'Chemist Reject')
                                                                ->count();
                $repeated = 0;
                foreach ($chemistRepeatWorked as $row) {
                    if ($row->count > 1)
                        $repeated++;
                }
                $output['farmatch_experience']  =  $difference;
                $output['completed_works']      =  $completedWorks;
                $output['no_of_drug_stores']    =  $noOfDrugStores;
                $output['no_of_repeated']       =  $repeated;
                $output['sudden_cancellation']  =  $suddenCancellation;

                return response()->json(['chemist' => $output, 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' =>  $e->getMessage(), 'status' => false]);
            }
        }
    }

    public function acceptJobApplication(Request $request){

        $validator = Validator::make($request->all(), [
            'applicationId' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        } else {
            try {
                // $application     =  ChemistJobApplication::where('id', $request->applicationId)->first();
                // $acceptedCount   =  ChemistJobApplication::where('job_post_id', $application->job_post_id)->whereIn('status', ['Accepted', 'Chemist Accept'])->count();
                // $vaccancyCount   =  JobPost::where('id', $application->job_post_id)->first()->no_of_vaccancies;
                $applicationRow = ChemistJobApplication::where('id', $request->applicationId)->first();
                if($applicationRow->status != 'Pending'){
                    return response()->json([ 'status' => 'error', 'message' => 'この申請の有効期限が切れています']);
                }
                ChemistJobApplication::where('id', $request->applicationId)->update(['status' => 'Accepted']);
                $notification                       = new Notification();
                $notification->type                 = 'Chemist';
                $notification->notification_type    = 'Job Application';
                $notification->chemist_id           = ChemistJobApplication::where('id', $request->applicationId)->first()->chemist_id;
                $notification->job_post_id          = ChemistJobApplication::where('id', $request->applicationId)->first()->job_post_id;
                $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $request->applicationId)->first()->drug_store_id)->first();
                $notification->image                = $drugStore->main_image;
                $notification->drug_store_address   = $drugStore->address;
                $notification->drug_store_model     = $drugStore->history_model;
                $notification->job_application_id   = $request->applicationId;
                $notification->title                = '勤務申請が受付けられました';
                $notification->message              = ChemistJobApplication::where('id', $request->applicationId)->first()->date . ' の勤務申請が薬局/病院に受付けられました';
                $notification->save();
                $deviceToken        =   Chemist::where('id', $notification->chemist_id)->first()->device_token;
                $this->sendNotification('勤務申請が受付けられました', ChemistJobApplication::where('id', $request->applicationId)->first()->date . ' の勤務申請が薬局/病院に受付けられました', $deviceToken, NULL, $notification->chemist_id);
                return response()->json(['message' => '承認しました', 'status' => true]);
            } catch (\Exception $e) {
                return response()->json(['message' => '入力が正しくありません', 'status' => false]);
            }
        }
    }

    public function checkReviewStatus($date){

        if (Carbon::now()->startOfDay()->gt(Carbon::parse($date)->startOfDay())) {
            return "Review";
        } else {
            return "Not Review";
        }
    }
    //chemist chat store
    public function chemistChat(Request $request){

        $search     = $request->search;
        $chemist    = $request->chemist;

        if ($chemist) {
            $chemist_details = Chemist::find($chemist);
            if (!$chemist_details) {
                return redirect()->route('store-dashboard');
            }
        }elseif(\Session::has('chemist')){
            $chemist =  session('chemist');
        }

        $chatheads  = StoreChemistChatHead::select('store_chemist_chat_heads.*')
                                            ->where(function ($q) use ($search) {
                                                $q->orWhere('chemists.first_name', 'like', "%{$search}%");
                                                $q->orWhere('chemists.last_name', 'like', "%{$search}%");
                                                $q->orWhere('chemists.email', 'like', "%{$search}%");
                                                $q->orWhere('chemists.phone', 'like', "%{$search}%");
                                            })
                                            ->where('store_chemist_chat_heads.drug_store_id', Auth::user()->id)
                                            ->join('chemists', 'chemists.id', '=', 'chemist_id')
                                            ->orderBy('store_chemist_chat_heads.updated_at', 'desc')
                                            ->get();
        $data = ['chatheads' => $chatheads, 'search' => $search, 'chemist' => $chemist, 'page_title' => '薬剤師　チャット'];
        return view('store.chemist_chat', $data);
    }

    public function startNewChemistChat(Request $request){

        $validator = Validator::make($request->all(), [
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = StoreChemistChatHead::where('drug_store_id', Auth::user()->id)->where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new StoreChemistChatHead();
            $chathead->chemist_id = $request->chemist;
            $chathead->drug_store_id = Auth::user()->id;
            $chathead->last_message = "";
        }
        $chathead->save();
        \Session::flash('chemist', $chathead->chemist_id);
        return response()->json(['status' => true]);
    }

    public function saveChemistchat(Request $request){

        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = StoreChemistChatHead::where('drug_store_id', Auth::user()->id)->where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new StoreChemistChatHead();
            $chathead->chemist_id = $request->chemist;
            $chathead->drug_store_id = Auth::user()->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new StoreChemistChat();
        $chat->store_chemist_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->store_read_status = 1;
        $chat->user = 1;
        $chat->save();

        if($chathead->chemist && $chathead->chemist->device_token){
            $data = [
                'type' => "store_chat",
                'store_id'=> $chathead->store->id,
                'store_image'=> $chathead->store->main_image,
                'store_name'=> $chathead->store->name,
                'user_id'=>$chathead->chemist->id
            ];
            $this->sendNotification($chathead->store->name.' から新着メッセージがあります', $request->message, $chathead->chemist->device_token, $data, $request->chemist);
        }
        return response()->json(['status' => true]);
    }

    public function cancelJobApplication(Request $request){

        try {
            $applicationStatus  = ChemistJobApplication::where('id', $request->applicationId)->first();
            if($applicationStatus->status == 'Chemist Accept'){
                $jobDate        =   $applicationStatus->date;
                $date           =   Carbon::parse($jobDate);
                $now            =   Carbon::now();
                $diffDays       =   $date->diffInDays($now);
                if ($diffDays > 7) {
                    $percentage = 0;
                } elseif ($diffDays >= 4 && $diffDays <= 7) {
                    $percentage = 50;
                } else {
                    $percentage = 100;
                }
                ChemistJobApplication::where('id', $request->applicationId)
                                       ->where('status', 'Chemist Accept')
                                       ->update([ 'status'                  => 'job Cancelled',
                                                  'cancellation_percentage' => $percentage
                                                ]);
                Notification::where('job_application_id', $request->applicationId)->delete();
                $notification                       =   new Notification();

                $notification->type                 = 'Chemist';
                $notification->notification_type    = 'Job Application';
                $notification->chemist_id           = ChemistJobApplication::where('id', $request->applicationId)->first()->chemist_id;
                $notification->job_post_id          = ChemistJobApplication::where('id', $request->applicationId)->first()->job_post_id;
                $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $request->applicationId)->first()->drug_store_id)->first();
                $notification->image                = $drugStore->main_image;
                $notification->drug_store_address   = $drugStore->address;
                $notification->drug_store_model     = $drugStore->history_model;
                $notification->job_application_id   = $request->applicationId;
                $notification->title                = '確定済みの案件がキャンセルされました';
                $notification->message              = $applicationStatus->date . ' の勤務案件がキャンセルされました';

                $deviceToken                =   Chemist::where('id', $applicationStatus->chemist_id)->first()->device_token;
                $this->sendNotification('確定済みの案件がキャンセルされました', $applicationStatus->date . ' の勤務案件がキャンセルされました', $deviceToken, NULL, $applicationStatus->chemist_id);
                $notification->save();
            }else{
                ChemistJobApplication::where('id', $request->applicationId)->update(['status' => 'Rejected']);
                $notification                       =   new Notification();

                $notification->type                 = 'Chemist';
                $notification->notification_type    = 'Job Application';
                $notification->chemist_id           = ChemistJobApplication::where('id', $request->applicationId)->first()->chemist_id;
                $notification->job_post_id          = ChemistJobApplication::where('id', $request->applicationId)->first()->job_post_id;
                $drugStore = DrugStore::where('id', ChemistJobApplication::where('id', $request->applicationId)->first()->drug_store_id)->first();
                $notification->image                = $drugStore->main_image;
                $notification->drug_store_address   = $drugStore->address;
                $notification->drug_store_model     = $drugStore->history_model;
                $notification->job_application_id   = $request->applicationId;
                $notification->title                = '確定済みの案件のキャンセル';
                $notification->message              = $applicationStatus->date . ' の勤務案件がキャンセルされました';

                $deviceToken        =   Chemist::where('id', $applicationStatus->chemist_id)->first()->device_token;
                $this->sendNotification('確定済みの案件のキャンセル', $applicationStatus->date . ' の勤務案件がキャンセルされました', $deviceToken, NULL, $applicationStatus->chemist_id);
                // $this->sendNotification('確定済みの案件がキャンセルされました', $applicationStatus->date . ' の勤務案件がキャンセルされました', $deviceToken, NULL, $applicationStatus->chemist_id);
                $notification->save();
            }
            return response()->json(['message' => '更新しました', 'status' => true]);
        } catch (\Exception $e) {
            return response()->json(['message' => '入力が正しくあり���せん', 'status' => false]);
        }
    }

    public function calculateAge($dob){

        $from = new DateTime($dob);
        $to   = new DateTime('today');
        $age  = $from->diff($to)->y;
        if ($age >= 10 && $age < 20) {
            return "10代";
        } else if ($age >= 20 && $age < 30) {
            return "20代";
        } else if ($age >= 30 && $age < 40) {
            return "30代";
        } else if ($age >= 40 && $age < 50) {
            return "40代";
        } else if ($age >= 50 && $age < 60) {
            return "50代";
        } else if ($age >= 60 && $age < 70) {
            return "60代";
        } else if ($age >= 70 && $age < 80) {
            return "70代";
        } else if ($age >= 80 && $age < 90) {
            return "80代";
        } else if ($age >= 90 && $age < 100) {
            return "90代";
        }
    }

    public function sendNotification($title, $message, $deviceToken, $data = null,$chemistId){

        $badgeCount = $this->getNotificationCount($chemistId);
        $fcmFields = array(
            "to" => $deviceToken,
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "sound" => "default",
                "priority" => "high",
                'badge' => $badgeCount,
                "show_in_foreground" => true,
                "targetScreen" => 'detail',
                "type" => 'FAV',
                "drug_store_id" => Auth::id()
            ),
            "data"=> $data,
            "priority" => 10
        );

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }

    public function getNotificationCount($chemistId){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $chemistId)
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    if($row->job_post_id != ''){
                        $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                        $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                        if($row->job_application_id != ''){
                            $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                            $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                            $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                            $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                            $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                            $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                        }else{
                            $diffHours                    = 0;
                            $output[$i]['job_title']      = "";
                            $output[$i]['post_date']      = "";
                            $output[$i]['time_from']      = "";
                            $output[$i]['time_to']        = "";
                            $output[$i]['rest_time']      = "";
                        }
                    } else{
                        $diffHours                    = 0;
                        $output[$i]['drug_store_id']  = "";
                        $output[$i]['drug_store_name']= "";
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                    if($output[$i]['type'] == 'Job Application'){
                        $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                        $applicationStatus  = ($application) ? $application->status : "";
                    }else{
                        $applicationStatus  = "";
                    }
                    if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                        $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                    }else{
                        $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['application_status'] = $applicationStatus;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if($applicationStatus == 'Accepted'){
                        if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                            unset($output[$i]);
                            Notification::where('id',$row->id)->delete();
                            $isIncrement = 1;
                        }
                    }
                    if($applicationStatus == 'Job Completed'){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }

                    if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                            ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        }
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
            }
            $adminChatCount     =  AdminChemistChatHead::
                                                        join('admin_chemist_chats', 'admin_chemist_chats.admin_chemist_chat_head_id', '=', 'admin_chemist_chat_heads.id')
                                                        ->select('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->where('admin_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $storeChatCount     =  StoreChemistChatHead::
                                                        join('store_chemist_chats', 'store_chemist_chats.store_chemist_chat_head_id', '=', 'store_chemist_chat_heads.id')
                                                        ->select('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->where('store_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $groupChatCount     =  GroupChatHead::
                                                join('group_chats', 'group_chats.group_chat_head_id', '=', 'group_chat_heads.id')
                                                ->select('group_chats.group_chat_head_id')
                                                ->where('group_chats.chemist_read_status', 0)
                                                ->where('chemist_id', $chemistId)
                                                ->groupBy('group_chats.group_chat_head_id')
                                                ->get()
                                                ->count();
            return count($output) + $storeChatCount + $adminChatCount + $groupChatCount;
        } catch(\Exception $e){
            return 0;
        }
    }

    public function markReadStoreChemist(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => "Chemist ID is required", 'status' => false]);
        }

        $head = StoreChemistChatHead::where('drug_store_id',Auth::user()->id)->where('chemist_id',$request->id)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            StoreChemistChat::where('store_chemist_chat_head_id',$head->id)->update(['store_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }

    public function markReadAdmin(Request $request){

        $head = DrugStoreChatHead::where('drug_store_id',Auth::user()->id)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            DrugStoreChat::where('drug_store_chat_head_id',$head->id)->update(['store_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }

    public function markReadGroup(Request $request){

        $validator = Validator::make($request->all(), [
            'chemist' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $head = GroupChatHead::where('drug_store_id',Auth::user()->id)->where('chemist_id', $request->chemist)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            GroupChat::where('group_chat_head_id',$head->id)->update(['store_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }

    public function pendingApplicationForEachPost(Request $request){

        $pendingApplications    =   ChemistJobApplication::
                                                            join('job_posts', 'job_posts.id', '=', 'chemist_job_applications.job_post_id')
                                                            ->join('chemists', 'chemists.id', '=', 'chemist_job_applications.chemist_id')
                                                            ->select('chemist_job_applications.status', 'job_name', 'chemist_id', 'first_name', 'last_name', 'chemist_job_applications.date', 'time_from', 'time_to', 'rest_time', 'chemist_job_applications.transportation_cost', 'chemist_job_applications.id', 'gender', 'chemist_job_applications.hourly_wage', 'image')
                                                            ->where('chemist_job_applications.status', 'Pending')
                                                            ->where('job_posts.drug_store_id', Auth::id())
                                                            ->where('chemist_job_applications.job_post_id', $request->postId)
                                                            ->get();
        $jobPost   =   JobPost::find($request->postId);

        if (Carbon::parse($jobPost->date)->gt(Carbon::now()) && ($jobPost->status != 'Rejected'))
            $isCancel = true;
        else
            $isCancel = false;

        return view('store.pending_applications')->with('jobName', $jobPost->job_name)
                                                 ->with('postId', $request->postId)
                                                 ->with('vaccancies', $jobPost->no_of_vaccancies)
                                                 ->with('pendingApplications', $pendingApplications)
                                                 ->with('page_title', $jobPost->job_name)
                                                 ->with('isCancel', $isCancel);
    }







    //phase 5

    public function sendPushNotificationsToChemist(){

               $current = Carbon::now();
                 $current_date_time = $current->toDateTimeString();
                 $current_date = date('Y-m-d', strtotime($current_date_time));
                  $accepted_applications = ChemistJobApplication::where('status', 'Accepted')->where('date', '>=', $current_date)->get();

                foreach ($accepted_applications as $accepted_application)
                {

                     $accepted_time = date('Y-m-d H:i:s', strtotime($accepted_application->updated_at));

                    $time1 = strtotime($current_date_time);
                    $time2 = strtotime($accepted_time);
                    $time_difference_in_hour = round(abs($time2 - $time1) / 3600,2);

                   if($accepted_application->notification_count_2==0 && $time_difference_in_hour >=24) // 24 hours
                    {
                        $accepted_application->notification_count_2=1;
                        $accepted_application->save();
//                        $notification                       = new Notification();
//                        $notification->type                 = 'Chemist';
//                        $notification->notification_type    = 'Job Application';
//                        $notification->chemist_id           = $accepted_application->chemist_id;
//                        $notification->job_post_id          = $accepted_application->job_post_id;
//                        $drugStore = DrugStore::where('id', $accepted_application->drug_store_id)->first();
//                        $notification->image                = $drugStore->main_image;
//                        $notification->drug_store_address   = $drugStore->address;
//                        $notification->drug_store_model     = $drugStore->history_model;
//                        $notification->job_application_id   = $accepted_application->id;
//                        $notification->title                = $accepted_application->date.'の勤務が確定待ちの状態です';
//                        $notification->message              = '承認ボタンを押して勤務を確定してください。';
//                        $notification->save();
                        $deviceToken        =   Chemist::where('id', $accepted_application->chemist_id)->first()->device_token;
                        $chemist        =   Chemist::where('id', $accepted_application->chemist_id)->first();
                        $this->sendNotification($accepted_application->date.'の勤務が確定待ちの状態です', '承認ボタンを押して勤務を確定してください。', $deviceToken, NULL, $accepted_application->chemist_id);

                        $data = [
                            'username' => $accepted_application->chemistDetails->last_name . " " . $accepted_application->chemistDetails->first_name,
                            'subject' => '【早急】'.$accepted_application->date.'の勤務がまだ確定待ちの状態です',
                            'title' => $accepted_application->date.'の勤務がまだ確定待ちの状態です。',
                            'message' => "勤務の確定は、アプリ内の【通知】→ 【承認待ち】より承認ボタンを押して勤務を確定できます。募集人数が定員になると勤務申請が自動で不成立となりますのでお早めに勤務を確定してください。"
                        ];

                        Mail::to($chemist->email)->send(new JobConfirmationMail($data));

                    }
                  elseif($accepted_application->notification_count_2==1 && $time_difference_in_hour >=48) //48 Hours
                    {
                        $accepted_application->notification_count_2=2;
                        $accepted_application->save();
//                        $notification                       = new Notification();
//                        $notification->type                 = 'Chemist';
//                        $notification->notification_type    = 'Job Application';
//                        $notification->chemist_id           = $accepted_application->chemist_id;
//                        $notification->job_post_id          = $accepted_application->job_post_id;
//                        $drugStore = DrugStore::where('id', $accepted_application->drug_store_id)->first();
//                        $notification->image                = $drugStore->main_image;
//                        $notification->drug_store_address   = $drugStore->address;
//                        $notification->drug_store_model     = $drugStore->history_model;
//                        $notification->job_application_id   = $accepted_application->id;
//                        $notification->title                = $accepted_application->date.'の勤務が確定待ちの状態です';
//                        $notification->message              = '承認ボタンを押して勤務を確定してください。';
//                        $notification->save();
                        $deviceToken        =   Chemist::where('id', $accepted_application->chemist_id)->first()->device_token;
                        $chemist        =   Chemist::where('id', $accepted_application->chemist_id)->first();
                        $this->sendNotification($accepted_application->date.'の勤務が確定待ちの状態です', '承認ボタンを押して勤務を確定してください。', $deviceToken, NULL, $accepted_application->chemist_id);

                        $data = [
                            'username' => $accepted_application->chemistDetails->last_name . " " . $accepted_application->chemistDetails->first_name,
                            'subject' => '【至急】'.$accepted_application->date.'の勤務がまだ確定待ちの状態です',
                            'title' => $accepted_application->date.'の勤務がまだ確定待ちの状態です。',
                            'message' => "勤務の確定は、アプリ内の【通知】→ 【承認待ち】より承認ボタンを押して勤務を確定できます。募集人数が定員になると勤務申請が自動で不成立となりますのでお早めに勤務を確定してください。"
                        ];
                        Mail::to($chemist->email)->send(new JobConfirmationMail($data));

                    }
                    else
                    {

                    }


                }


    }


    public function updateEmployeeIntroService(){

        if(date('d')==01)
        {
            $drugStores     =   DrugStore::select('id', 'name', 'email', 'in_charge', 'employee_intro_automatic', 'employee_intro_service', 'employee_intro_date', 'normal_farmatch_disable')
                ->where('normal_farmatch_disable', 1)
                ->orderBy('id', 'DESC')
                ->get();

            foreach ($drugStores as $drugStore)
            {
                DrugStore::where('id', $drugStore->id)->update(['normal_farmatch'=> 0]);
            }

        }

        if(date('m')==04 && date('d')==01)
        {
            $drugStores     =   DrugStore::select('id', 'name', 'email', 'in_charge', 'employee_intro_automatic', 'employee_intro_service', 'employee_intro_date')
                ->where('employee_intro_service', 1)
                ->orderBy('id', 'DESC')
                ->get();
            foreach ($drugStores as $drugStore)
            {
                if($drugStore->employee_intro_service==1)
                {

                   if($drugStore->employee_intro_automatic==1)
                   // if(0)
                    {
                        $employee_intro_date = date('Y-m-d', strtotime(Carbon::now()));
                        DrugStore::where('id', $drugStore->id)->update(['employee_intro_service'=> 1, 'employee_intro_renewed'=> 1, 'employee_intro_date'=> $employee_intro_date]);

                    }
                    else
                    {
                        $employee_intro_date = date('Y-m-d', strtotime(Carbon::now()));
                        DrugStore::where('id', $drugStore->id)->update(['employee_intro_service'=> 0, 'employee_intro_renewed'=> 0, 'employee_intro_date'=> $employee_intro_date]);

                        $data = [
                            'username' => $drugStore->in_charge,
                            'subject' => '【お知らせ】常勤やパートの紹介希望のご利用期限が終了しました',
                            'title' => '4月1日で「常勤やパートの紹介希望」のご利用期限が切れました。',
                            'message' => "再びサービスのご利用を希望される場合は、ふぁーまっちWEBアプリの【設定ページ】、サービス利用の選択から更新を行ってください。"
                        ];
                      Mail::to($drugStore->email)->send(new EmployeeIntroServiceExpiry($data));
                       // Mail::to("liju@jiitak.com")->send(new EmployeeIntroServiceExpiry($data));

                    }
                }
            }


        }


    }


    public function generatepdf(){
            $pdf = PDF::loadView('pdfview')
                        ->setPaper('a4')
                        ->setOrientation('landscape');
            return $pdf->download('pdfdownload.pdf');
    }

    public function generatepdfview(){
        return view('pdfview');
    }

    public function testMail(){
        $data = [
            'username' => "Liju",
            'subject' => 'Test Mail',
            'title' => 'Title Of Test Mail',
            'message' => "Message Of Test Mail"
        ];

        Mail::to("liju@jiitak.com")->send(new JobConfirmationMail($data));

        if (Mail::failures())
        {
            return "error";
        }
        else
        {
            return "no error";
        }

        return "Success";
    }






}
