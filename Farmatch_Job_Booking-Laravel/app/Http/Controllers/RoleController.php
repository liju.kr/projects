<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Chemist;
use App\DrugStore;
use App\AdminAction;
use App\AccessRestriction;

class RoleController extends Controller
{
    public function adminAccounts(){

        $users  = User::select('id', 'first_name', 'last_name', 'role_id', 'email', 'phone', 'location', 'company_name')
                        ->where('role_id', '<>', 1)
                        ->get();
        return view('admin_accounts')->with('users', $users)
                                     ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                                     ->with('accountActive', 'kt-menu__item--active')
                                     ->with('page_title', '管理者アカウント');
    }

    public function viewAdmin($userId){

        $profile = User::find($userId);
        return view('view_user')->with('profile', $profile)
                                ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                                ->with('accountActive', 'kt-menu__item--active')
                                ->with('page_title', 'マイプロフィール');
    }

    public function deleteUserAccount(Request $request){

        try{
            User::where('id', $request->userId)->delete();
            return response()->json(['message' => '削除しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function updateUserAccount(Request $request){

        $profile    =   User::find($request->id);
        $validator  =   Validator::make($request->all(),[
            'first_name'      => 'required',
            'last_name'       => 'required',
            'company_name'    => 'required',
            'location'        => 'required',
            'phone'           => 'required|unique:users,phone,'.$profile->id.',id,deleted_at,NULL',
            'email'           => 'required|unique:users,email,'.$profile->id.',id,deleted_at,NULL',
        ], [
            'phone.unique'    => 'この番号はすでに使用されています',
            'email.unique'    => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                if($request->file('image')){
                    $validator  = Validator::make($request->all(),[
                                        'image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ], [
                                        'image.image' => '許可されるファイルタイプ: png、jpg、jpeg、gif'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('image');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/profile/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$profile->image);
                    }
                    $profile->image   = $newImage;
                }
                $profile->first_name   = request('first_name');
                $profile->last_name    = request('last_name');
                $profile->company_name = request('company_name');
                $profile->location     = request('location');
                $profile->phone        = request('phone');
                $profile->email        = request('email');
                $profile->save();
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function accountChangePassword($userId){

        $profile = User::find($userId);
        return view('account_change_password')->with('profile', $profile)
                                              ->with('page_title', 'Change Password');
    }

    public function accountResetPassword(Request $request){

        try{
            User::where('id', $request->id)
                ->update(['password' =>  Hash::make($request->password),'updated_at' => Carbon::now()]);
            return response()->json(['message' => 'パスワードを再設定しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function accountAccessRestrictions($userId){

        $profile       = User::find($userId);
        $restriction   = AccessRestriction::where('user_id', $userId)-> first();
        return view('account_restrictions')->with('profile', $profile)
                                           ->with('restriction', $restriction)
                                           ->with('page_title', 'Access Restrictions');
    }

    public function addNewAdmin(){

        return view('add_new_user') ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                                    ->with('accountActive', 'kt-menu__item--active')
                                    ->with('page_title', 'Add New Restrictions');
    }

    public function addUserAccount(Request $request){

        $validator  =   Validator::make($request->all(),[
            'first_name'        => 'required',
            'last_name'         => 'required',
            'company_name'      => 'required',
            'location'          => 'required',
            'phone'             => 'required|unique:users,phone,NULL,id,deleted_at,NULL',
            'email'             => 'required|unique:users,email,NULL,id,deleted_at,NULL',
            'password'          => 'required',
        ], [
            'phone.unique'       => 'この番号はすでに使用されています',
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                $profile         =  new User();
                $profile->image  =  '';
                if($request->file('image')){
                    $validator  = Validator::make($request->all(),[
                                        'image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ], [
                                        'image.image' => '許可されるファイルタイプ: png、jpg、jpeg、gif'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('image');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/profile/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$profile->image);
                    }
                    $profile->image   = $newImage;
                }
                $profile->first_name   = request('first_name');
                $profile->last_name    = request('last_name');
                $profile->role_id      = 2;
                $profile->company_name = request('company_name');
                $profile->location     = request('location');
                $profile->phone        = request('phone');
                $profile->email        = request('email');
                $profile->password     = Hash::make($request->password);
                $profile->save();

                $restriction           = new AccessRestriction();
                $restriction->user_id  = $profile->id;
                $restriction->save();

                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function updateUserRestriction(Request $request){

        try{
            $restriction              = AccessRestriction::where('user_id', $request->id)->first();
            $restriction->dashboard   = ($request->has('dashboard')) ? 'Active': 'Disable';
            $restriction->drug_store  = ($request->has('drug_store')) ? 'Active': 'Disable';
            $restriction->chemist     = ($request->has('chemist')) ? 'Active': 'Disable';
            $restriction->interview   = ($request->has('interview')) ? 'Active': 'Disable';
            $restriction->payment     = ($request->has('invoice')) ? 'Active': 'Disable';
            $restriction->chat        = ($request->has('chat')) ? 'Active': 'Disable';
            $restriction->notification= ($request->has('notification')) ? 'Active': 'Disable';
            $restriction->settings    = ($request->has('settings')) ? 'Active': 'Disable';
            $restriction->save();
            return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function viewAdminActions(Request $request){

        $year    = $request->year;
        $month   = $request->month;
        $keyword = $request->keyword;
        if($year){
            $actions = AdminAction::orderBy('id', 'DESC')
                                    ->whereYear('created_at', '=', $year)
                                    ->whereMonth('created_at', '=', sprintf("%02d", $month))
                                    ->get();
        }else{
            $actions = AdminAction::orderBy('id', 'DESC')->get();
        }
        if($keyword){

            $chemists   =   Chemist::select('id')
                                        ->where('first_name', 'like', '%' . $keyword . '%')
                                        ->orWhere('last_name', 'like', '%' . $keyword . '%')
                                        ->orWhere('phone', 'like', '%' . $keyword . '%')
                                        ->orWhere('email', 'like', '%' . $keyword . '%')
                                        ->get();
            $drugStores =   DrugStore:: select('id')
                                        ->where('name', 'like', '%' . $keyword . '%')
                                        ->orWhere('phone', 'like', '%' . $keyword . '%')
                                        ->orWhere('email', 'like', '%' . $keyword . '%')
                                        ->get();
            $users      =   User:: select('id')
                                        ->where('first_name', 'like', '%' . $keyword . '%')
                                        ->orWhere('last_name', 'like', '%' . $keyword . '%')
                                        ->get();
            $actions = [];
            if(count($chemists) || count($drugStores)){

                $finalArray  = [];
                $i = 0;
                foreach($chemists as $row){

                    $finalArray[$i]['type']        = 'Chemist';
                    $finalArray[$i]['receiver_id'] = $row->id;
                    $i++;
                }

                foreach($drugStores as $row){

                    $finalArray[$i]['type']        = 'DrugStore';
                    $finalArray[$i]['receiver_id'] = $row->id;
                    $i++;
                }
                $result  = AdminAction::orderBy('id', 'DESC')->get();
                $actions = [];
                foreach($result as $row){

                    foreach($finalArray as $inRow){

                        if($row->receiver_id == $inRow['receiver_id'] && $row->receiver == $inRow['type']){
                            $actions[] = $row;
                        }
                    }
                }
            }
            if(count($users)){

                $userArray  = [];
                $i = 0;
                foreach($users as $row){

                    $userArray[$i]['user_id'] = $row->id;
                    $i++;
                }
                $result  = AdminAction::orderBy('id', 'DESC')->get();
                $actions = [];
                foreach($result as $row){

                    foreach($userArray as $inRow){

                        if($row->user_id == $inRow['user_id']){
                            $actions[] = $row;
                        }
                    }
                }
            }

        }
        return view('admin_actions')->with('actions', $actions)
                                    ->with('settings', 'kt-menu__item--active kt-menu__item--open')
                                    ->with('accountActive', 'kt-menu__item--active')
                                    ->with('page_title', 'アクション履歴');
    }
}
