<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Chemist;
use App\JobPost;
use App\GroupChat;
use App\DrugStore;
use App\ChemistChat;
use App\Notification;
use App\DrugStoreChat;
use App\GroupChatHead;
use App\ChemistChatHead;
use App\AdminChemistChat;
use App\DrugStoreChatHead;
use App\AdminChemistChatHead;
use App\StoreChemistChatHead;
use App\ChemistJobApplication;

class ChatController extends Controller
{
    public function index()
    {
        return view('chat');
    }

    public function home(Request $request)
    {
        $store = "";

        if ($request->store) {
            $store_details = DrugStore::find($request->store);
            if ($store_details) {
                $chathead = DrugStoreChatHead::where('drug_store_id', $request->store)->get()->first();
                if (!$chathead) {
                    $chathead = new DrugStoreChatHead();
                    $chathead->drug_store_id = $request->store;
                    $chathead->last_message = "";
                }
                $chathead->updated_at = now();
                $chathead->save();
                $store = $store_details->id;
            }
        }elseif(\Session::has('store')){
            $store =  session('store');
        }

        $data = [
            'chatheads' => DrugStoreChatHead::orderBy('updated_at', 'desc')->get(),
            'store' => $store
        ];

        return view('drugstore_chat', $data)->with('chats', 'kt-menu__item--active kt-menu__item--open')
            ->with('drugStoreChatActive', 'kt-menu__item--active');
    }


    public function startChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = DrugStoreChatHead::where('drug_store_id', $request->store)->get()->first();
        if (!$chathead) {
            $chathead = new DrugStoreChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->last_message = "";
        }

        $chathead->updated_at = now();
        $chathead->save();

        \Session::flash('store', $chathead->drug_store_id);

        return response()->json(['status' => true]);
    }

    public function saveChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = DrugStoreChatHead::where('drug_store_id', $request->id)->get()->first();
        if (!$chathead) {
            $chathead = new DrugStoreChatHead();
            $chathead->drug_store_id = $request->id;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new DrugStoreChat();
        $chat->drug_store_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->admin_read_status = 1;
        $chat->admin = 1;
        $chat->save();

        return response()->json(['status' => true]);
    }

    public function getDrugStoreDetails(Request $request)
    {
        $store_details = DrugStore::find($request->id);
        $chat = DrugStoreChatHead::where('drug_store_id', $request->id)->first();
        $chatcount = 0;

        if ($chat) {
            $chatcount = $chat->chats->count();
        }

        return response()->json([
            'store' => $store_details,
            'status' => true,
            'chatcount' => $chatcount
        ]);
    }

    public function groupChatAdmin(Request $request)
    {
        $search = $request->search;

        $store = "";
        $chemist = "";

        if ($request->store && $request->chemist) {
            $store_details = DrugStore::find($request->store);
            $chemist_details = Chemist::find($request->chemist);

            if ($chemist_details && $store_details) {
                $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', $request->chemist)->get()->first();
                if (!$chathead) {
                    $chathead = new GroupChatHead();
                    $chathead->drug_store_id = $request->store;
                    $chathead->chemist_id = $request->chemist;
                    $chathead->last_message = "";
                }

                $chathead->updated_at = now();
                $chathead->save();

                $store = $store_details->id;
                $chemist = $chemist_details->id;
            }
        }elseif(\Session::has('store') && \Session::has('chemist')){
            $store =  session('store');
            $chemist =  session('chemist');
        }

        $chatheads = GroupChatHead::select('group_chat_heads.*')
            ->where(function ($q) use ($search) {
            $q->orWhere('chemists.first_name', 'like', "%{$search}%");
            $q->orWhere('chemists.last_name', 'like', "%{$search}%");
            $q->orWhere('chemists.email', 'like', "%{$search}%");
            $q->orWhere('chemists.phone', 'like', "%{$search}%");
            $q->orWhere('drug_stores.name', 'like', "%{$search}%");
            $q->orWhere('drug_stores.email', 'like', "%{$search}%");
            $q->orWhere('drug_stores.phone', 'like', "%{$search}%");
        })
            ->join('chemists', 'chemists.id', '=', 'chemist_id')
            ->join('drug_stores', 'drug_stores.id', '=', 'drug_store_id')
            ->orderBy('group_chat_heads.updated_at', 'desc')
            ->get();

        $data = [
            'chatheads' => $chatheads,
            'search' => $search,
            'chemist' => $chemist,
            'store' => $store
        ];
        return view('group_chat', $data)->with('chats', 'kt-menu__item--active kt-menu__item--open')
            ->with('groupChatActive', 'kt-menu__item--active');;
    }

    public function getGroupChatDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store' => 'required',
            'chemist' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $data = [
            'store' => DrugStore::find($request->store),
            'chemist' => Chemist::find($request->chemist),
            'status' => true,
        ];

        return response()->json($data);
    }

    public function saveGroupChatAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'store' => 'required',
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = $request->chemist;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new GroupChat();
        $chat->group_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->admin_read_status = 1;
        $chat->user = 1;
        $chat->save();

        if($chathead->chemist && $chathead->chemist->device_token){
            $data = [
                'type' => "group_chat",
                'store_id'=> $chathead->store->id,
                'store_image'=> $chathead->store->main_image,
                'store_name'=> $chathead->store->name,
                'admin_image'=> User::first()->image,
                'user_id'=>$chathead->chemist->id
            ];
            $this->sendNotification('運営本部から新着メッセージがあります', $request->message, $chathead->chemist->device_token, $data, $chathead->chemist->id);
        }

        return response()->json(['status' => true]);
    }

    public function startNewGroupChatAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'store' => 'required',
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = GroupChatHead::where('drug_store_id', $request->store)->where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new GroupChatHead();
            $chathead->drug_store_id = $request->store;
            $chathead->chemist_id = $request->chemist;
            $chathead->last_message = "";
        }
        $chathead->updated_at = now();
        $chathead->save();

        \Session::flash('store', $chathead->drug_store_id);
        \Session::flash('chemist', $chathead->chemist_id);

        return response()->json(['status' => true]);
    }

    public function chemistChatAdmin(Request $request)
    {
        $search = $request->search;
        $chemist = "";

        if ($request->chemist) {
            $chemist_details = Chemist::find($request->chemist);

            if ($chemist_details) {
                $chathead = AdminChemistChatHead::where('chemist_id', $request->chemist)->get()->first();
                if (!$chathead) {
                    $chathead = new AdminChemistChatHead();
                    $chathead->chemist_id = $request->chemist;
                    $chathead->last_message = "";
                }

                $chathead->updated_at = now();
                $chathead->save();

                $chemist = $chemist_details->id;
            }
        }elseif(\Session::has('chemist')){
            $chemist =  session('chemist');
        }

        $chatheads = AdminChemistChatHead::select('admin_chemist_chat_heads.*')
            ->where(function ($q) use ($search) {
                $q->orWhere('chemists.first_name', 'like', "%{$search}%");
                $q->orWhere('chemists.last_name', 'like', "%{$search}%");
                $q->orWhere('chemists.email', 'like', "%{$search}%");
                $q->orWhere('chemists.phone', 'like', "%{$search}%");
            })
            ->join('chemists', 'chemists.id', '=', 'chemist_id')
            ->orderBy('admin_chemist_chat_heads.updated_at', 'desc')
            ->get();
        $data = [
            'chatheads' => $chatheads,
            'search' => $search,
            'chemist' => $chemist
        ];
        return view('chemist_chat', $data)->with('chats', 'kt-menu__item--active kt-menu__item--open')
            ->with('chemistChatActive', 'kt-menu__item--active');
    }

    public function startNewChemistChatAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = AdminChemistChatHead::where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new AdminChemistChatHead();
            $chathead->chemist_id = $request->chemist;
            $chathead->last_message = "";
        }
        $chathead->updated_at = now();
        $chathead->save();
        \Session::flash('chemist', $chathead->chemist_id);
        return response()->json(['status' => true]);
    }

    public function saveChemistchatAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'chemist' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $chathead = AdminChemistChatHead::where('chemist_id', $request->chemist)->get()->first();
        if (!$chathead) {
            $chathead = new AdminChemistChatHead();
            $chathead->chemist_id = $request->chemist;
        }

        $chathead->last_message = $request->message;
        $chathead->updated_at = now();
        $chathead->save();

        $chat = new AdminChemistChat();
        $chat->admin_chemist_chat_head_id = $chathead->id;
        $chat->message = $request->message;
        $chat->admin_read_status = 1;
        $chat->admin = 1;
        $chat->save();

        if($chathead->chemist && $chathead->chemist->device_token){
            $data = [
                'type' => "admin_chat",
                'admin_image'=> User::first()->image,
                'user_id'=>$chathead->chemist->id
            ];
            $this->sendNotification('運営本部から新着メッセージがあります', $request->message, $chathead->chemist->device_token,$data, $chathead->chemist->id);
        }

        return response()->json(['status' => true]);
    }

    public function getChemistDetailsAdmin(Request $request)
    {
        $chemist_details = Chemist::find($request->chemist);
        return response()->json([
            'chemist' => $chemist_details,
            'status' => true,
        ]);
    }

    public function sendNotification($title, $message, $deviceToken, $data = null, $chemistId = null){

        if($chemistId != null){
            $badgeCount = $this->getNotificationCount($chemistId);
            $fcmFields = array(
                "to" => $deviceToken,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "sound" => "default",
                    "priority" => "high",
                    'badge' => $badgeCount,
                    "show_in_foreground" => true,
                    "targetScreen" => 'detail'
                ),
                    "priority" => 10
            );
        }else{
            $fcmFields = array(
                "to" => $deviceToken,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "sound" => "default",
                    "priority" => "high",
                    "show_in_foreground" => true,
                    "targetScreen" => 'detail'
                ),
                "data"=>$data,
                "priority" => 10
            );
        }

        $headers = array(
            'Authorization: key=' . 'AAAAAcvdjiU:APA91bG5nUxkTHr3Ty_Vna_4nS7EeUuMpXUwQz8H2fde-BgrVOo5sAjq0Rddl8sl3bs5BVuFkJJi1pjEqedzyXlABZOA_LTsRV1cwnwhPRHhLq2LCu-r3iNMyJHhr0RlkKnXM8kB03ne',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        // print_r($result);die;
        curl_close($ch);
        //return $result;
    }

    public function adminMarkReadChemist(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => "Chemist ID is required", 'status' => false]);
        }

        $head = AdminChemistChatHead::where('chemist_id',$request->id)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            AdminChemistChat::where('admin_chemist_chat_head_id',$head->id)->update(['admin_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }

    public function adminMarkReadStore(Request $request){

        $validator = Validator::make($request->all(), [
            'store' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => "Chemist ID is required", 'status' => false]);
        }

        $head = DrugStoreChatHead::where('drug_store_id',$request->store)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            DrugStoreChat::where('drug_store_chat_head_id',$head->id)->update(['admin_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }

    public function adminMarkReadGroup(Request $request){

        $validator = Validator::make($request->all(), [
            'chemist' => 'required',
            'store'=> 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => false]);
        }

        $head = GroupChatHead::where('drug_store_id',$request->store)->where('chemist_id', $request->chemist)->first();
        $head_id = null;
        if($head){
            $head_id = $head->id;
            GroupChat::where('group_chat_head_id',$head->id)->update(['admin_read_status'=>1]);
        }
        return response()->json(['message' => "Success", 'status' => true, 'head'=>$head_id]);
    }
    public function getNotificationCount($chemistId){

        try{
            $notifications = Notification::select('id', 'title', 'message', 'notification_type', 'job_post_id', 'job_application_id', 'image', 'drug_store_address', 'drug_store_model', 'created_at')
                                            ->where('chemist_id', $chemistId)
                                            ->orderBy('id', 'DESC')
                                            ->get();
            $output = []; $i = 0;
            foreach($notifications as $row){
                    $output[$i]['notification_id']    = $row->id;
                    $output[$i]['type']               = ($row->notification_type) ? $row->notification_type : "";
                    $output[$i]['job_post_id']        = ($row->job_post_id) ? $row->job_post_id : "";
                    $output[$i]['job_application_id'] = ($row->job_application_id) ? $row->job_application_id : "";
                    if($row->job_post_id != ''){
                        $output[$i]['drug_store_id']  = JobPost::where('id', $row->job_post_id)->first()->drug_store_id;
                        $output[$i]['drug_store_name']= DrugStore::where('id', $output[$i]['drug_store_id'])->first()->name;
                        if($row->job_application_id != ''){
                            $diffHours                    = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ((ChemistJobApplication::where('id', $row->job_application_id)->first()->status == 'Accepted') ? Carbon::now()->diffInHours($row->created_at) : 0 ): 0;
                            $output[$i]['job_title']      = JobPost::where('id', $row->job_post_id)->first()->job_name;
                            $output[$i]['post_date']      = JobPost::where('id', $row->job_post_id)->first()->date;
                            $output[$i]['time_from']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_from)) : "";
                            $output[$i]['time_to']        = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? date('H:i',strtotime(ChemistJobApplication::where('id', $row->job_application_id)->first()->time_to)) : "";
                            $output[$i]['rest_time']      = (ChemistJobApplication::where('id', $row->job_application_id)->exists()) ? ChemistJobApplication::where('id', $row->job_application_id)->first()->rest_time : "";
                        }else{
                            $diffHours                    = 0;
                            $output[$i]['job_title']      = "";
                            $output[$i]['post_date']      = "";
                            $output[$i]['time_from']      = "";
                            $output[$i]['time_to']        = "";
                            $output[$i]['rest_time']      = "";
                        }
                    } else{
                        $diffHours                    = 0;
                        $output[$i]['drug_store_id']  = "";
                        $output[$i]['drug_store_name']= "";
                        $output[$i]['job_title']      = "";
                        $output[$i]['post_date']      = "";
                        $output[$i]['time_from']      = "";
                        $output[$i]['time_to']        = "";
                        $output[$i]['rest_time']      = "";
                    }
                    if($output[$i]['type'] == 'Job Application'){
                        $application        = ChemistJobApplication::where('id', $output[$i]['job_application_id'])->first();
                        $applicationStatus  = ($application) ? $application->status : "";
                    }else{
                        $applicationStatus  = "";
                    }
                    if($output[$i]['type'] == 'Admin Approval' || $output[$i]['type'] == 'Admin Message'){
                        $image = ($row->image) ? asset("/images/profile/".$row->image) : "";
                    }else{
                        $image = ($row->image) ? asset("/images/drug_store/".$row->image) : "";
                    }
                    $output[$i]['image']              = $image;
                    $output[$i]['address']            = ($row->drug_store_address) ? $row->drug_store_address : "";
                    $output[$i]['model']              = ($row->drug_store_model) ? $row->drug_store_model : "";
                    $output[$i]['application_status'] = $applicationStatus;
                    $output[$i]['title']              = $row->title;
                    $output[$i]['message']            = $row->message;
                    $output[$i]['notification_time']  = date('Y-m-d H:i',strtotime($row->created_at));
                    $isIncrement = 0;
                    if($applicationStatus == 'Accepted'){
                        if(Carbon::now()->startOfDay()->gt( Carbon::parse($output[$i]['post_date'])->startOfDay())){
                            unset($output[$i]);
                            Notification::where('id',$row->id)->delete();
                            $isIncrement = 1;
                        }
                    }
                    if($applicationStatus == 'Job Completed'){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        $isIncrement = 1;
                    }

                    if((!ChemistJobApplication::where('id', $row->job_application_id)->exists() && $row->job_application_id != '') || $diffHours >= 48){
                        unset($output[$i]);
                        Notification::where('id',$row->id)->delete();
                        if(ChemistJobApplication::where('id', $row->job_application_id)->exists()){
                            ChemistJobApplication::where('id', $row->job_application_id)->update([ 'status' => 'Chemist Reject' ]);
                        }
                        $isIncrement = 1;
                    }
                    if($isIncrement == 0){
                        $i++;
                    }
            }
            $adminChatCount     =  AdminChemistChatHead::
                                                        join('admin_chemist_chats', 'admin_chemist_chats.admin_chemist_chat_head_id', '=', 'admin_chemist_chat_heads.id')
                                                        ->select('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->where('admin_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('admin_chemist_chats.admin_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $storeChatCount     =  StoreChemistChatHead::
                                                        join('store_chemist_chats', 'store_chemist_chats.store_chemist_chat_head_id', '=', 'store_chemist_chat_heads.id')
                                                        ->select('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->where('store_chemist_chats.chemist_read_status', 0)
                                                        ->where('chemist_id', $chemistId)
                                                        ->groupBy('store_chemist_chats.store_chemist_chat_head_id')
                                                        ->get()
                                                        ->count();
            $groupChatCount     =  GroupChatHead::
                                                join('group_chats', 'group_chats.group_chat_head_id', '=', 'group_chat_heads.id')
                                                ->select('group_chats.group_chat_head_id')
                                                ->where('group_chats.chemist_read_status', 0)
                                                ->where('chemist_id', $chemistId)
                                                ->groupBy('group_chats.group_chat_head_id')
                                                ->get()
                                                ->count();
            return count($output) + $storeChatCount + $adminChatCount + $groupChatCount;
        } catch(\Exception $e){
            return 0;
        }
    }
}
