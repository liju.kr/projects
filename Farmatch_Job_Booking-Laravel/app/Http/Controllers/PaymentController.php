<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Chemist;
use App\JobPost;
use App\DrugStore;
use App\ChemistReview;
use App\ChemistPayment;
use App\WorkPlaceReview;
use App\DrugStorePayment;
use App\ChemistJobApplication;

class PaymentController extends Controller
{
    public function drugStorePayment(){
        File::cleanDirectory(public_path('invoices/'));
        $getCurrentDay      =   Carbon::now()->format('d');
        $getCurrentMonth    =   Carbon::now()->format('m');
        $getCurrentYear     =   Carbon::now()->format('Y');
        $distinctStores     =   DrugStore::whereIn('status', ['Approved', 'Suspended'])->get();
        $output = []; $i = 0;
        foreach ($distinctStores as $row){
            if($getCurrentDay > 20){
                $startDate      = $getCurrentYear."-".$getCurrentMonth."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->addMonths(1)->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('drug_store_id', $row->id)
                                                        ->whereIn('status', ['Job completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0; $totalHours = 0; $cancelCount = 0; $cancelTotalWage = 0;
                foreach($fetchRows as $inRow){
                    if($inRow->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $inRow->rest_time;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += ($totalWorkTime * $wageInMinutes);
                        $hourlyWage         = $inRow->hourly_wage;
                        $cancelWageInMinutes  = $hourlyWage/60;
                        $cancelTotalWage     += $cancelWageInMinutes * $totalWorkTime;
                        $cancelCount++;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        //$totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                    }
                }
                $output[$i]['drug_store']       =  $row->name;
                $output[$i]['manager_company']  =  $row->manager_company;
                $output[$i]['from_date']        =  $startDate;
                $output[$i]['end_date']         =  $endDate;
                $output[$i]['total_wage']       =  number_format($totalWage, 2, '.', '');
                $output[$i]['total']            =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_hours']      =  $totalHours;
                $output[$i]['cancel_count']     =  $cancelCount;
                $output[$i]['commission']       =  2200;
                $output[$i]['sub_total']        =  number_format($output[$i]['total'] + $output[$i]['commission'] + $cancelTotalWage, 2, '.', '');
                // $output[$i]['tax']              =  number_format(($output[$i]['sub_total'] * 10 ) / 100, 2, '.', '');
                // $output[$i]['grand_total']      =  number_format($output[$i]['sub_total'] + $output[$i]['tax'], 2, '.', '');
                $i++;
            }else{
                $startDate      = Carbon::now()->subMonth()->format('Y-m')."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('drug_store_id', $row->id)
                                                        ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0; $totalHours = 0; $cancelCount = 0; $cancelTotalWage = 0;
                foreach($fetchRows as $inRow){
                    if($inRow->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $inRow->rest_time;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += ($totalWorkTime * $wageInMinutes);
                        $hourlyWage         = $inRow->hourly_wage;
                        $cancelWageInMinutes  = $hourlyWage/60;
                        $cancelTotalWage     += $cancelWageInMinutes * $totalWorkTime;
                        $cancelCount++;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                       // $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                    }
                }
                $output[$i]['drug_store']                =  $row->name;
                $output[$i]['manager_company']           =  $row->manager_company;
                $output[$i]['from_date']                 =  $startDate;
                $output[$i]['end_date']                  =  $endDate;
                $output[$i]['total_wage']                =  number_format($totalWage, 2, '.', '');
                $output[$i]['total']                     =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_hours']               =  $totalHours;
                $output[$i]['cancel_count']              =  $cancelCount;
                $output[$i]['commission']                =  2200;
                $output[$i]['sub_total']                 =  number_format($output[$i]['total'] + $output[$i]['commission'] + $cancelTotalWage, 2, '.', '');
                // $output[$i]['tax']                       =  number_format(($output[$i]['sub_total'] * 10 ) / 100, 2, '.', '');
                // $output[$i]['grand_total']               =  number_format($output[$i]['sub_total'] + $output[$i]['tax'], 2, '.', '');
                $i++;
            }
        }
        return view('drug_store_payments') ->with('payments', 'kt-menu__item--active kt-menu__item--open')
                                           ->with('drugStorePaymentActive', 'kt-menu__item--active')
                                           ->with('page_title', '薬局/病院等　お支払い')
                                           ->with('totalPayments', $output);
    }

    public function chemistPayment(){

        $getCurrentDay      =   Carbon::now()->format('d');
        $getCurrentMonth    =   Carbon::now()->format('m');
        $getCurrentYear     =   Carbon::now()->format('Y');
        $distinctChemsits   =   Chemist::whereIn('status', ['Approved', 'Suspended'])->get();
        $output = []; $i = 0;
        foreach ($distinctChemsits as $row){
            if($getCurrentDay > 20){
                $startDate      = $getCurrentYear."-".$getCurrentMonth."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->addMonths(1)->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $row->id)
                                                        ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $inRow){
                    if($inRow->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $inRow->rest_time;
                        $wageInMinutes      = $inRow->hourly_wage/60;
                        $totalWage         += (($totalWorkTime * $wageInMinutes) * $inRow->cancellation_percentage) /100;
                        $transportationCost+= $inRow->transportation_cost;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                       // $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $wageInMinutes      = $inRow->hourly_wage/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                        $transportationCost+= $inRow->actual_transportation_cost;
                    }
                }
                $output[$i]['chemist']                      =  $row->last_name." ".$row->first_name;
                $output[$i]['from_date']                    =  $startDate;
                $output[$i]['end_date']                     =  $endDate;
                $output[$i]['total_wage']                   =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_transportation_cost']    =  number_format($transportationCost, 2, '.', '');
                $output[$i]['total']                        =  number_format($totalWage+$transportationCost, 2, '.', '');
                $output[$i]['tax']                          =  number_format(($output[$i]['total'] * 10 ) / 100, 2, '.', '');
                $output[$i]['grand_total']                  =  number_format($output[$i]['total'] + $output[$i]['tax'], 2, '.', '');
                $i++;
            }else{
                $startDate      = Carbon::now()->subMonth()->format('Y-m')."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::now()->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $row->id)
                                                        ->whereIn('status', ['Job completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $inRow){

                    if($inRow->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                      //$totalWorkTime      = $workHourinMinutes - $inRow->rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $wageInMinutes      = $inRow->hourly_wage/60;
                        $totalWage         += (($totalWorkTime * $wageInMinutes) * $inRow->cancellation_percentage) /100;
                        $transportationCost+= $inRow->transportation_cost;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        //$totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $wageInMinutes      = $inRow->hourly_wage/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                        $transportationCost+= $inRow->actual_transportation_cost;
                    }

                }
                $output[$i]['chemist']                      =  $row->last_name." ".$row->first_name;
                $output[$i]['from_date']                    =  $startDate;
                $output[$i]['end_date']                     =  $endDate;
                $output[$i]['total_wage']                   =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_transportation_cost']    =  $transportationCost;
                $output[$i]['total']                        =  number_format($totalWage+$transportationCost, 2, '.', '');
                $output[$i]['tax']                          =  number_format(($output[$i]['total'] * 10 ) / 100, 2, '.', '');
                $output[$i]['grand_total']                  =  number_format($output[$i]['total'] + $output[$i]['tax'], 2, '.', '');
                $i++;
            }
        }
        return view('chemist_payments') ->with('payments', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('chemistpaymentActive', 'kt-menu__item--active')
                                        ->with('page_title', '薬剤師　お支払い')
                                        ->with('chemistPayments', $output);
    }

    public function changeDrugStorePaidStatus(Request $request){

        $validator  =   Validator::make($request->all(),[
            'paymentId'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                DrugStorePayment::where('id', $request->paymentId)
                                ->update([ 'status'    => 'Paid' ]);
                return response()->json(['message' => '更新しました', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function searchDrugStorePayment(Request $request){

        try{
            $getCurrentDay      =   Carbon::now()->format('d');
            $getCurrentMonth    =   sprintf("%02d", $request->month);
            $getCurrentYear     =   $request->year;
            // $distinctStores     =   DB::table('chemist_job_applications')  ->select('drug_store_id')
            //                                                                ->groupBy('drug_store_id')
            //                                                                ->get();
            $distinctStores     =   DrugStore::whereIn('status', ['Approved', 'Suspended'])->get();
            $startDate          =   $getCurrentYear."-".$getCurrentMonth."-21";
            $startDate          =   Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
            $endDate            =   Carbon::parse($startDate) ->addMonths(1)->format('Y-m')."-20";
            $output = []; $i = 0;
            foreach ($distinctStores as $row){
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('drug_store_id', $row->id)
                                                        ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0; $totalHours = 0; $cancelCount = 0; $cancelTotalWage = 0;
                foreach($fetchRows as $inRow){
                    if($inRow->status == 'Job Cancelled'){
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        $totalWorkTime      = $workHourinMinutes - $inRow->rest_time;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += ($totalWorkTime * $wageInMinutes);
                        $hourlyWage         = $inRow->hourly_wage;
                        $cancelWageInMinutes  = $hourlyWage/60;
                        $cancelTotalWage     += $cancelWageInMinutes * $totalWorkTime;
                        $cancelCount++;
                    }else{
                        $to                 = Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               = Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    = $to->diffInMinutes($from);
                        $workHourinMinutes  = $diff_in_minutes;
                        //$totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $totalHours        += $totalWorkTime / 60;
                        $wageInMinutes      = 110/60;
                        $totalWage         += $totalWorkTime * $wageInMinutes;
                    }
                }
                $output[$i]['drug_store']                =  $row->name;
                $output[$i]['manager_company']           =  $row->manager_company;
                $output[$i]['drug_store_id']             =  $row->id;
                $output[$i]['from_date']                 =  $startDate;
                $output[$i]['end_date']                  =  $endDate;
                $output[$i]['total_wage']                =  number_format($totalWage, 2, '.', '');
                $output[$i]['total']                     =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_hours']               =  $totalHours;
                $output[$i]['cancel_count']              =  $cancelCount;
                $output[$i]['commission']                =  2200;
                $output[$i]['sub_total']                 =  number_format($output[$i]['total'] + $output[$i]['commission'] + $cancelTotalWage, 2, '.', '');
                $output[$i]['tax']                       =  number_format(($output[$i]['sub_total'] * 10 ) / 100, 2, '.', '');
                $output[$i]['grand_total']               =  number_format($output[$i]['total'] + $output[$i]['commission'] + $cancelTotalWage, 2, '.', '');
                $i++;
            }
            $table  = '<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                    <th>選択<input type="checkbox" name="select-all" id="select-all" onclick="checkUncheck(this)" class="form-control" style="width: 18px;"></th>
                                    <th>#</th>
                                    <th>薬局/病院等</th>
                                    <th>代表者名</th>
                                    <th>開始日</th>
                                    <th>締め日</th>
                                    <th>勤務時間合計</th>
                                    <th>マッチング手数料</th>
                                    <th>直前キャンセル</th>
                                    <th>総計</th>
                                    <th>アクション</th>
                                </tr>
                            </thead>
                            <tbody>';
            $i = 1;
            foreach($output as $row){
                $invoiceGenerated   =  (DrugStorePayment::where('drug_store_id', '=', $row['drug_store_id'])->where('from_date', $startDate)->where('active_status', 'Active')->exists()) ? "disabled" : "";
                $invoicePaidStatus  =  (DrugStorePayment::where('drug_store_id', '=', $row['drug_store_id'])->where('from_date', $startDate)->where('status', 'Paid')->exists()) ? "disabled" : "";
                // $invoiceId          =   DrugStorePayment::where('drug_store_id', '=', $row['drug_store_id'])->where('from_date', $startDate)->first()->id;
                if($invoiceGenerated ){
                    $invoiceButton  =   '<br><button type="button" class="btn btn-success btn-elevate mt-3 btn-sm" style="width: 96px;" '. $invoicePaidStatus .' onclick="chengeDrugStorePaidStatus('.$row['drug_store_id'].",'".$row['from_date'].
                                        "','".$row['end_date']."',".$row['total_wage'].')">
                                            支払済
                                        </button>';
                }else{
                    $invoiceButton = "";
                }
                $table  .= '<tr>
                                <td><input type="checkbox" value="'.$row['drug_store_id'].",".$row['from_date'].
                                ",".$row['end_date'].",".$row['total_wage'].",".$row['commission'].",".$row['sub_total'].",".$row['tax'].",".$row['grand_total'].'" name="send-invoice" class="send-invoice form-control" style="width: 18px;"></td>
                                <td>'.$i.'</td>
                                <td>'.$row['drug_store'].'</td>
                                <td>'.$row['manager_company'].'</td>
                                <td>'.$row['from_date'].'</td>
                                <td>'.$row['end_date'].'</td>
                                <td>'.$row['total_hours'].'</td>
                                <td>110</td>
                                <td>'.$row['cancel_count'].'</td>
                                <td>'.round($row['sub_total']).'</td>
                                <td>
                                    <button type="button" class="btn btn-success btn-elevate btn-sm" style="width: 96px;" data-toggle="modal" onclick="modalPopup('.$row['drug_store_id'].",'".$row['from_date'].
                                    "','".$row['end_date']."',".$row['total_wage'].",".$row['commission'].",".$row['sub_total'].",".$row['tax'].",".$row['grand_total'].');">
                                        詳細
                                    </button><br>
                                    <button type="button" class="btn btn-primary btn-elevate  mt-3 btn-sm"'. $invoiceGenerated .' onclick="generateInvoice('.$row['drug_store_id'].",'".$row['from_date'].
                                    "','".$row['end_date']."',".$row['total_wage'].",".$row['commission'].",".$row['sub_total'].",".$row['tax'].",".$row['grand_total'].')">
                                        請求書を送る
                                    </button><br>
                                    <a href= "'.route('admin-download-invoice', $row['drug_store_id'].",".$row['from_date'].
                                    ",".$row['end_date'].",".$row['total_wage'].",".$row['commission'].",".$row['sub_total'].",".$row['tax'].",".$row['grand_total']).'"
                                    target="_blank"><button type="button" class="btn btn-sm btn-success btn-elevate mt-3">
                                                ダウンロード
                                            </button>
                                    </a>
                                    '.$invoiceButton.'
                                </td>
                            </tr>';
                $i++;
            }
            $table .=       '<tr>
                               <td>
                                <button style="width: 65px;" type="button" class="btn btn-success btn-elevate mr-3 mt-3" onclick="downloadMultiple()">
                                印刷
                                </button>
                                <td>
                             <td>
                             <button style="width: 120px;" type="button" class="btn btn-success btn-elevate ml-1 mt-3" onclick="sendInvoice()">
                                請求書を送る
                             </button>
                             </td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             </tr>
                             </tbody>
                        </table>
                        ';
            return response()->json(['data' => $table, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
        }
    }

    public function searchChemistPayment(Request $request){

        try{
            $getCurrentMonth    =   sprintf("%02d", $request->month);
            $getCurrentYear     =   $request->year;
            // $distinctChemsits=   DB::table('chemist_job_applications')   ->select('chemist_id')
            //                                                                 ->groupBy('chemist_id')
            //                                                                 ->get();
            $distinctChemsits   =   Chemist::whereIn('status', ['Approved', 'Suspended'])->get();
            $output = []; $i = 0;
            foreach ($distinctChemsits as $row){
                $startDate      = $getCurrentYear."-".$getCurrentMonth."-21";
                $startDate      = Carbon::createFromFormat('Y-m-d', $startDate)->toDateString();
                $endDate        = Carbon::parse($startDate) ->addMonths(1)->format('Y-m')."-20";
                $fetchRows      = ChemistJobApplication::whereBetween('date', [$startDate." 00:00:00", $endDate." 23:59:59"])
                                                        ->where('chemist_id', $row->id)
                                                        ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                        ->get();
                $totalWage = 0; $transportationCost = 0; $cusumptionTax = 0;
                foreach($fetchRows as $inRow){
                    if($inRow->status == 'Job Cancelled'){

                        $to                 =  Carbon::createFromFormat('H:i:s', $inRow->time_from);
                        $from               =  Carbon::createFromFormat('H:i:s', $inRow->time_to);
                        $diff_in_minutes    =  $to->diffInMinutes($from);
                        $workHourinMinutes  =  $diff_in_minutes;
                        $totalWorkTime      =  $workHourinMinutes - $inRow->rest_time;
                        $wageInMinutes      =  $inRow->hourly_wage/60;
                        $totalWage          += (($totalWorkTime * $wageInMinutes) * $inRow->cancellation_percentage) /100;
                        $transportationCost += $inRow->transportation_cost;
                    }else{
                        $to                 =  Carbon::createFromFormat('H:i:s', $inRow->actual_time_from);
                        $from               =  Carbon::createFromFormat('H:i:s', $inRow->actual_time_to);
                        $diff_in_minutes    =  $to->diffInMinutes($from);
                        $workHourinMinutes  =  $diff_in_minutes;
                        //$totalWorkTime      =  $workHourinMinutes - $inRow->actual_rest_time;
                        $totalWorkTime      = $workHourinMinutes - $inRow->actual_rest_time - $inRow->late_arrived + $inRow->extra_work;
                        $wageInMinutes      =  $inRow->hourly_wage/60;
                        $totalWage         +=  $totalWorkTime * $wageInMinutes;
                        $transportationCost+=  $inRow->actual_transportation_cost;
                    }
                }
                $output[$i]['chemist']                      =  $row->last_name." ".$row->first_name;
                $output[$i]['chemist_id']                   =  $row->id;
                $output[$i]['from_date']                    =  $startDate;
                $output[$i]['end_date']                     =  $endDate;
                $output[$i]['total_wage']                   =  number_format($totalWage, 2, '.', '');
                $output[$i]['total_transportation_cost']    =  number_format($transportationCost, 2, '.', '');
                $output[$i]['total']                        =  number_format($totalWage+$transportationCost, 2, '.', '');
                $output[$i]['tax']                          =  number_format(($output[$i]['total'] * 10 ) / 100, 2, '.', '');
                $output[$i]['grand_total']                  =  number_format($output[$i]['total'] + $output[$i]['tax'], 2, '.', '');
                $i++;
            }
            $table  = '<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>薬剤師</th>
                                <th>開始日</th>
                                <th>締め日</th>
                                <th>給与額</th>
                                <th>交通費</th>
                                <th>合計</th>
                                <th>アクション</th>
                                </tr>
                            </thead>
                            <tbody>';
            $i = 1;
            foreach($output as $row){
            $isChemistPaid  = (ChemistPayment::where('chemist_id', '=', $row['chemist_id'])->where('from_date', $startDate)->exists()) ? "disabled" : "";
            $table  .= '<tr>
                        <td>'.$i.'</td>
                        <td>'.$row['chemist'].'</td>
                        <td>'.$row['from_date'].'</td>
                        <td>'.$row['end_date'].'</td>
                        <td>'.$row['total_wage'].'</td>
                        <td>'.$row['total_transportation_cost'].'</td>
                        <td>'.$row['total'].'</td>
                        <td>
                        <div class="row">
                            <button type="button" class="btn btn-success btn-elevate" '.$isChemistPaid.' onclick="chengeChemistPaidStatus('.$row['chemist_id'].",'".$row['from_date'].
                            "','".$row['end_date']."',".$row['total_wage'].",".$row['total_transportation_cost'].",".$row['tax'].",".$row['grand_total'].')">
                            支払済
                            </button>
                        </div>
                    </td>
                    </tr>';
            $i++;
            }
            $table .=       '</tbody>
                    </table>';
            return response()->json(['data' => $table, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function generateDrugstoreInvoice(Request $request){

        try{
            if (DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->where('active_status', 'Active')->exists()) {
                return response()->json(['message' => "Invoice already generated", 'status' => FALSE]);
             }else{
                if (Carbon::now()->startOfDay()->lte(Carbon::parse($request->end_date)->startOfDay())) {
                    return response()->json(['message' => '請求書は締め日後にのみ送ることが可能です', 'status' => FALSE]);
                }
                if (DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->exists()) {
                    $invoiceId = DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->first()->id;
                    DrugStorePayment::where('id', $invoiceId)->update([ "active_status" => 'Active' ]);
                }else{
                    $drugStore                      = new drugStorePayment();
                    $drugStore->drug_Store_id       = $request->drug_store_id;
                    $drugStore->from_date           = $request->from_date;
                    $drugStore->to_date             = $request->end_date;
                    $drugStore->hourly_wage         = $request->total_wage;
                    $drugStore->commision           = $request->commision;
                    $drugStore->sub_total           = $request->sub_total;
                    $drugStore->tax                 = $request->tax;
                    $drugStore->total_price         = $request->grand_total;
                    $drugStore->active_status       = 'Active';
                    $drugStore->save();
                }
                \Helper::instance()->addAction(Auth::user()->id, $request->drug_store_id, 'DrugStore', '薬局/病院等へ請求書を送りました', 'flaticon-coins kt-font-success');
                return response()->json(['message' => "請求書を送りました", 'status' => TRUE]);
             }
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function generateAllDrugstoreInvoice(Request $request){

        $invoices =  $request->invoices;
        try{
            foreach($invoices as $row){
                $splitRows = explode(",", $row);
                if (!DrugStorePayment::where('drug_store_id', '=', $splitRows[0])->where('from_date', $splitRows[1])->where('status', 'Active')->exists()) {
                    if (Carbon::now()->startOfDay()->lte(Carbon::parse($splitRows[2])->startOfDay())) {
                        return response()->json(['message' => '請求書は締め日後にのみ送ることが可能です', 'status' => FALSE]);
                    }
                    if (DrugStorePayment::where('drug_store_id', '=', $splitRows[0])->where('from_date', $splitRows[1])->exists()) {
                        $invoiceId = DrugStorePayment::where('drug_store_id', '=', $splitRows[0])->where('from_date', $splitRows[1])->first()->id;
                        DrugStorePayment::where('id', $invoiceId)->update([ "active_status" => 'Active' ]);
                    }else{
                        $drugStore                  = new drugStorePayment();
                        $drugStore->drug_Store_id   = $splitRows[0];
                        $drugStore->from_date       = $splitRows[1];
                        $drugStore->to_date         = $splitRows[2];
                        $drugStore->hourly_wage     = $splitRows[3];
                        $drugStore->commision       = $splitRows[4];
                        $drugStore->sub_total       = $splitRows[5];
                        $drugStore->tax             = $splitRows[6];
                        $drugStore->total_price     = $splitRows[7];
                        $drugStore->active_status   = 'Active';
                        $drugStore->save();
                        \Helper::instance()->addAction(Auth::user()->id, $splitRows[0], 'DrugStore', '薬局/病院等へ請求書を送りました', 'flaticon-coins kt-font-success');
                    }
                }
            }
            return response()->json(['message' => "請求書を送りました", 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
        }
    }

    public function changeDrugstoreInvoiceStatus(Request $request){

        try{
            if (DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->where('status', 'Paid')->exists()) {
                return response()->json(['message' => "Status already paid", 'status' => FALSE]);
             }else{
                DrugStorePayment::where('drug_store_id', $request->drug_store_id)
                                  ->where('from_date', $request->from_date)
                                  ->update(['status' => 'Paid']);
                \Helper::instance()->addAction(Auth::user()->id, $request->drug_store_id, 'DrugStore', '薬局/病院等の請求書を支払済にしました', 'flaticon2-check-mark kt-font-success');
                return response()->json(['message' => "ステータスを支払済に変更しました", 'status' => TRUE]);
             }
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function generateChemistInvoice(Request $request){

        try{
            if (ChemistPayment::where('chemist_id', '=', $request->chemist_id)->where('from_date', $request->from_date)->exists()) {
                return response()->json(['message' => "Invoice already generated", 'status' => FALSE]);
             }else{
                $chemist                      = new ChemistPayment();
                $chemist->chemist_id          = $request->chemist_id;
                $chemist->from_date           = $request->from_date;
                $chemist->to_date             = $request->end_date;
                $chemist->amount              = $request->total_wage;
                $chemist->transportation_cost = $request->transportation_cost;
                $chemist->consumption_tax     = $request->consumption_tax;
                $chemist->total_amount        = $request->total;
                $chemist->consumption_tax     = $request->consumption_tax;
                $chemist->save();
                \Helper::instance()->addAction(Auth::user()->id, $request->chemist_id, 'Chemist', '薬局/病院等の請求書を支払済にしました', 'flaticon2-check-mark kt-font-success');
                return response()->json(['message' => "支払いステータスを変更しました", 'status' => TRUE]);
             }
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function downloadInvoice(Request $request){

        $paymentArray = explode(",", $request->payment_information);
        $fromDate     = $paymentArray[1];
        $drugStoreId  = $paymentArray[0];
        if (DrugStorePayment::where('drug_store_id', '=', $drugStoreId)->where('from_date', $fromDate)->exists()) {
            $invoiceId = DrugStorePayment::where('drug_store_id', '=', $drugStoreId)->where('from_date', $fromDate)->first()->id;
            \Helper::instance()->addAction(Auth::user()->id, $drugStoreId, 'DrugStore', '薬局/病院等の請求書をダウンロードしました', 'flaticon-download-1 kt-font-success');
            return redirect()->route('admin-download-invoice-action', $invoiceId);
         }else{
            $drugStore                      = new drugStorePayment();
            $drugStore->drug_Store_id       = $drugStoreId;
            $drugStore->from_date           = $fromDate;
            $drugStore->to_date             = $paymentArray[2];
            $drugStore->hourly_wage         = $paymentArray[3];
            $drugStore->commision           = $paymentArray[4];
            $drugStore->sub_total           = $paymentArray[5];
            $drugStore->tax                 = $paymentArray[6];
            $drugStore->total_price         = $paymentArray[7];
            $drugStore->save();
            \Helper::instance()->addAction(Auth::user()->id, $drugStoreId, 'DrugStore', '薬局/病院等の請求書をダウンロードしました', 'flaticon-download-1 kt-font-success');
            return redirect()->route('admin-download-invoice-action', $drugStore->id);
         }

    }

    public function generateInvoicePopup(Request $request){

        try{
            $drugStore   = DrugStore::find($request->drug_store_id);
            $invoiceId   = "";
            if (!DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->exists()) {
                $drugStore                 = new drugStorePayment();
                $drugStore->drug_Store_id  = $request->drug_store_id;
                $drugStore->from_date      = $request->from_date;
                $drugStore->to_date        = $request->end_date;
                $drugStore->hourly_wage    = $request->total_wage;
                $drugStore->commision      = $request->commision;
                $drugStore->sub_total      = $request->sub_total;
                $drugStore->tax            = $request->tax;
                $drugStore->total_price    = $request->grand_total;
                $drugStore->save();
                $invoiceId = $drugStore->id;
            }else{
                $invoiceId = DrugStorePayment::where('drug_store_id', '=', $request->drug_store_id)->where('from_date', $request->from_date)->first()->id;
            }
            $registeredDate  = Carbon::createFromFormat('Y-m-d H:i:s', $drugStore->created_at)->toDateString();
            $registeredDate  = Carbon::createFromFormat('Y-m-d H:i:s', $drugStore->created_at)->toDateString();
            if(JobPost::where('drug_store_id', '=', $drugStore->id)->where('status', '=', 'Active')->exists()){
                $firstJobPost = JobPost::where('drug_store_id', '=', $drugStore->id)
                                         ->orderBy('date','asc')
                                         ->where('status', 'active')
                                         ->first()
                                         ->date;
            }else{
                $firstJobPost = "";
            }

            $fetchRows         = ChemistJobApplication::whereBetween('date', [$request->from_date, $request->end_date])
                                                    ->where('drug_store_id', $drugStore->id)
                                                    ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                    ->get();

            $totalWage = 0; $totalWorkHour = 0; $transportationCost = 0; $cusumptionTax = 0; $output = []; $i = 0; $cancelledArray = [];
            $totalCancelWage = 0; $totalCompleteWage = 0; $totalCancelHour = 0; $completedJobArray = []; $totalJobHour = 0; $otalAfterExtra = 0;$cancelPosts = '';$k = 1; $l = 1; $completeposts = '';
            foreach($fetchRows as $row){
                // print_r($row);exit;
                if($row->status == 'Job Cancelled'){
                    $to                                 = Carbon::createFromFormat('H:i:s', $row->time_from);
                    $from                               = Carbon::createFromFormat('H:i:s', $row->time_to);
                    $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                    $diff_in_minutes                    = $to->diffInMinutes($from);
                    $workHourinMinutes                  = $diff_in_minutes;
                    $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->rest_time;
                    $output[$i]['hourlyWage']           = $row->hourly_wage;
                    $output[$i]['hrs'] = floor(($workHourinMinutes - $row->rest_time) / 60);
                    $output[$i]['mins'] = floor(($workHourinMinutes - $row->rest_time) % 60);

                    $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->rest_time) / 60;
                    // $output[$i]['totalWorkHour']        = floor(($workHourinMinutes - $row->rest_time) / 60) . "." . floor(($workHourinMinutes - $row->rest_time) % 60);
                    $output[$i]['wageInMinutes']        = 110/60;
                    $output[$i]['cancelWageInMinutes']  = $output[$i]['hourlyWage']/60;
                    $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                    $output[$i]['totalCancelWage']      = $output[$i]['totalWorkTime'] * $output[$i]['cancelWageInMinutes'];
                    $output[$i]['total']                = $output[$i]['totalWage'];
                    $output[$i]['date']                 = $row->date;
                    $totalCancelWage    += $output[$i]['totalCancelWage'];
                    $cancelPosts    .= '<tr>
                                            <td scope="row">'.$k.'</td>
                                            <td>'.$output[$i]['chemist'].'</td>
                                            <td>'.$row->date.'</td>
                                            <td>'.$output[$i]['totalWorkHour'].' 時間</td>
                                            <td>'.$output[$i]['hourlyWage'].' 円</td>
                                        </tr>
                                        <tr>
                                            <td scope="row" colspan="4">給与合計</td>
                                            <td align="right">'.$output[$i]['totalCancelWage'].'</td>
                                        </tr>';
                    $k++;
                }else{
                    // print_r($row);exit;
                    // print_r($row);exit;

                    // $select_hr_html = '';
                    // $select_hr_html .= '<select onchange="alert(`'.$row->id.'`)">';
                    // for($m = 0; $m < 12; $m++) {
                    //     $m == $output[$i]['hrs'] ? $selected = 'selected' : $selected = '';
                    //     $select_hr_html .= '<option value="'.$m.'" '.$selected.'>'.$m.'</option>';
                    // }
                    // $select_hr_html .= '</select>';

                    // $select_min_html = '';
                    // $select_min_html .= '<select>';
                    // for($m = 0; $m < 60; $m++) {
                    //     $m == $output[$i]['mins'] ? $min_selected = 'selected' : $min_selected = '';
                    //     $select_min_html .= '<option value="'.$m.'" '.$min_selected.'>'.$m.'</option>';
                    // }
                    // $select_min_html .= '</select>';

                    $to                                 = Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                    $from                               = Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                    $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                    $diff_in_minutes                    = $to->diffInMinutes($from);
                    $workHourinMinutes                  = $diff_in_minutes;
                   // $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time;
                    $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work;
                    //$output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time) / 60;
                    $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work) / 60;

                    $output[$i]['hourlyWage']           = $row->hourly_wage;
                    $output[$i]['wageInMinutes']        = 110/60;
                    $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                    $output[$i]['total']                = $output[$i]['totalWage'];
                    $output[$i]['date']                 = $row->date;
                    $output[$i]['completewageInMinutes']  = $output[$i]['hourlyWage']/60;
                    $output[$i]['totalCompleteWage']      = ($output[$i]['totalWorkTime'] * 110) / 60;
                    // $output[$i]['totalCompleteWage']      = $output[$i]['totalWorkTime'] * $output[$i]['completewageInMinutes'];
                    $output[$i]['hrs'] = floor(($workHourinMinutes - $row->rest_time) / 60);
                    $output[$i]['mins'] = floor(($workHourinMinutes - $row->rest_time) % 60);
                    $totalCompleteWage                  += $output[$i]['totalCompleteWage'];


                    $select_hr_html = '';
                    $select_hr_html .= '<select id="whr'.$l.'">';
                    for($m = 0; $m < 24; $m++) {
                        $m == $output[$i]['hrs'] ? $selected = 'selected' : $selected = '';
                        $select_hr_html .= '<option value="'.$m.'" '.$selected.'>'.$m.'</option>';
                    }
                    $select_hr_html .= '</select>';

                    $select_min_html = '';
                    $select_min_html .= '<select id="wmin'.$l.'">';
                    for($m = 0; $m < 60; $m++) {
                        $m == $output[$i]['mins'] ? $min_selected = 'selected' : $min_selected = '';
                        $select_min_html .= '<option value="'.$m.'" '.$min_selected.'>'.$m.'</option>';
                    }
                    $select_min_html .= '</select>';
                    $completeposts    .= '<tr>
                                            <td scope="row">'.$l.'</td>
                                            <td>'.$output[$i]['chemist'].'</td>
                                            <td>'.$row->date.'</td>
                                            <td>'.$select_hr_html. " " . $select_min_html .' <span style="word-break: keep-all;"> 時間 </span></td>
                                            <td>'.$output[$i]['hourlyWage'].' 円</td>
                                            <td><a href="#" data-toggle="tooltip" title="Update Working time" class="btn btn-sn btn-green" onclick="gethrValues('.$l.', '.$row->id.')">更新</a></td>

                                        </tr>
                                        <tr>
                                            <td scope="row" colspan="4">マッチング手数料</td>
                                            <td align="right">'.$output[$i]['totalCompleteWage'].'</td>
                                        </tr>';
                    $l++;

                }
                $totalWage      += $output[$i]['total'];
                $totalWorkHour  += $output[$i]['totalWorkHour'];
                $i++;
            }

            $grandtotal   = $totalWage + 2200 + $totalCancelWage;
            $message  = [ "registered_date" => $registeredDate,
                          "first_job_date"  => $firstJobPost,
                          "drug_store_name" => $drugStore->name,
                          "total_wage"      => round($totalWage),
                          "grand_total"     => round($grandtotal),
                          'cancel_posts'    => $cancelPosts,
                          'complete_posts'  => $completeposts,
                          'invoice_id'      => $invoiceId
            ];
            // print_r($message);exit;
            return response()->json(['message' => $message, 'status' => TRUE]);
        } catch(\Exception $e){
            // print_r($e->getMessage());exit;
            return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
        }
    }

    public function saveInvoiceAdditionalDetails(Request $request){

        try{
            $drugstore = DrugStorePayment::find($request->invoice_id)->drug_store_id;
            DrugStorePayment::where('id', $request->invoice_id)->update([ "reduce_amount"   => $request->discount,
                                                                          "remarks"         => $request->remarks,
                                                                          "input_text"      => $request->input_text,
                                                                          "input_quantity"  => $request->quantity,
                                                                          "input_unit_price"=> $request->amount,
                                                                          "total_input"     => $request->quantity * $request->amount,
                                                                          "bank_details"    => $request->ban_details
                                                                        ]);
            \Helper::instance()->addAction(Auth::user()->id, $drugstore, 'DrugStore', '薬局/病院等の請求書の編集を反映しました', 'flaticon2-edit kt-font-success');
            return response()->json(['message' => "請求書に反映されました", 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function downloadAllDrugstoreInvoice(Request $request){

        $invoices =  $request->invoices;
        $html = '';$invoiceArray = [];
        try{
            foreach($invoices as $row){
                $splitRows = explode(",", $row);
                $invoiceId = "";
                if (DrugStorePayment::where('drug_store_id', '=', $splitRows[0])->where('from_date', $splitRows[1])->exists()) {
                    $invoiceId = DrugStorePayment::where('drug_store_id', '=', $splitRows[0])->where('from_date', $splitRows[1])->first()->id;
                    $view = $this->downloadPdf($invoiceId);
                    $html = $view->render();
                    $pdf = PDF::loadHtml($html);
                    $random = rand();
                    $pdf->setPaper('a4', 'portrait')->save(public_path()."/invoices/".$random.".pdf");
                    $invoiceArray[]  =  URL::to('/')."/invoices/".$random.".pdf";
                }else{
                    $drugStore                  = new drugStorePayment();
                    $drugStore->drug_Store_id   = $splitRows[0];
                    $drugStore->from_date       = $splitRows[1];
                    $drugStore->to_date         = $splitRows[2];
                    $drugStore->hourly_wage     = $splitRows[3];
                    $drugStore->commision       = $splitRows[4];
                    $drugStore->sub_total       = $splitRows[5];
                    $drugStore->tax             = $splitRows[6];
                    $drugStore->total_price     = $splitRows[7];
                    $drugStore->save();
                    $invoiceId = $drugStore->id;
                    $view = $this->downloadPdf($invoiceId);
                    $html = $view->render();
                    $pdf = PDF::loadHtml($html);
                    $random = rand();
                    $pdf->setPaper('a4', 'portrait')->save(public_path()."/invoices/".$random.".pdf");
                    $invoiceArray[]  = URL::to('/')."/invoices/".$random.".pdf";
                }
            }
            return response()->json(['paths' => $invoiceArray, 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
        }
    }

    public function downloadPdf($invoiceId){

        $invoice           = DrugStorePayment::where('id', $invoiceId)->first();
        $drugStoreName     = DrugStore::where('id', $invoice->drug_store_id)->first()->name;
        $drugStoreAddress  = DrugStore::where('id', $invoice->drug_store_id)->first()->address;
        $startDate         = Carbon::createFromFormat('Y-m-d', $invoice->from_date)->toDateString();
        $endDate           = Carbon::createFromFormat('Y-m-d', $invoice->to_date)->toDateString();

        $last_date         = Carbon::createFromFormat('Y-m-d', $invoice->to_date)->addMonths(1)->format('Y-m')."-10";

        $fetchRows         = ChemistJobApplication::whereBetween('date', [$startDate, $endDate])
                                                    ->where('drug_store_id', $invoice->drug_store_id)
                                                    ->whereIn('status', ['Job Completed', 'Job Cancelled'])
                                                    ->get();
        $totalWage = 0; $totalWorkHour = 0; $transportationCost = 0; $cusumptionTax = 0; $output = []; $i = 0; $cancelledArray = [];
        $totalCancelWage = 0; $totalCancelHour = 0; $completedJobArray = []; $totalJobHour = 0; $otalAfterExtra = 0;

        foreach($fetchRows as $row){
            if($row->status == 'Job Cancelled'){
                $to                                 = Carbon::createFromFormat('H:i:s', $row->time_from);
                $from                               = Carbon::createFromFormat('H:i:s', $row->time_to);
                $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                $diff_in_minutes                    = $to->diffInMinutes($from);
                $workHourinMinutes                  = $diff_in_minutes;
                $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->rest_time;
                $output[$i]['hourlyWage']           = $row->hourly_wage;
                $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->rest_time) / 60;
                $output[$i]['wageInMinutes']        = 110/60;
                $output[$i]['cancelWageInMinutes']  = $output[$i]['hourlyWage']/60;
                $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                $output[$i]['totalCancelWage']      = $output[$i]['totalWorkTime'] * $output[$i]['cancelWageInMinutes'];
                $output[$i]['total']                = $output[$i]['totalWage'];
                $output[$i]['date']                 = $row->date;
                $totalCancelHour    += $output[$i]['totalWorkHour'];
                $totalCancelWage    += $output[$i]['totalCancelWage'];
                $cancelledArray[] = ['chemist'           => $output[$i]['chemist'],
                                     'totalWorkHour'     => $output[$i]['totalWorkHour'],
                                     'date'              => $row->date,
                                     'hourly_wage'       => $output[$i]['hourlyWage'],
                                     'total_cancel_wage' => $output[$i]['totalCancelWage']
                                    ];
            }else{
                $to                                 = Carbon::createFromFormat('H:i:s', $row->actual_time_from);
                $from                               = Carbon::createFromFormat('H:i:s', $row->actual_time_to);
                $output[$i]['chemist']              = Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
                $diff_in_minutes                    = $to->diffInMinutes($from);
                $workHourinMinutes                  = $diff_in_minutes;
                //$output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time;
                $output[$i]['totalWorkTime']        = $workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work;
               // $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time) / 60;
                $output[$i]['totalWorkHour']        = ($workHourinMinutes - $row->actual_rest_time - $row->late_arrived + $row->extra_work) / 60;

                $output[$i]['wageInMinutes']        = 110/60;
                $output[$i]['totalWage']            = $output[$i]['totalWorkTime'] * $output[$i]['wageInMinutes'];
                $output[$i]['total']                = $output[$i]['totalWage'];
                $output[$i]['date']                 = $row->date;
                $totalJobHour    += $output[$i]['totalWorkHour'];
                $completedJobArray[] = ['chemist'        => $output[$i]['chemist'],
                                        'totalWorkHour'  => $output[$i]['totalWorkHour'],
                                        'date'           => $row->date
                                       ];
            }
            $totalWage      += $output[$i]['total'];
            $totalWorkHour  += $output[$i]['totalWorkHour'];
            $i++;
        }
        $grandtotal      = $totalWage + 2200 + $totalCancelWage;
        $totalAfterExtra = ($invoice->input_quantity) ? $invoice->total_input + $grandtotal : 0;
        if($invoice->reduce_amount > 0){
            if($totalAfterExtra == 0){
                $totalAfterExtra = $grandtotal - $invoice->reduce_amount;
            }else{
                $totalAfterExtra = $totalAfterExtra - $invoice->reduce_amount;
            }
        }

        return view('store.invoice', compact('output', 'invoice', 'drugStoreName', 'drugStoreAddress',
                             'totalWage','totalWorkHour','grandtotal','last_date', 'cancelledArray', 'totalCancelHour', 'totalCancelWage',
                             'completedJobArray', 'totalJobHour', 'totalAfterExtra'));
        // $pdf->setPaper('a4', 'portrait');
        // return $pdf->stream('invoice.pdf');
    }

    public function updateCompletedWorking(Request $req) {
        try {
            $job_application = ChemistJobApplication::where('id', $req->id)->first();
            // print_r($job_application->actual_time_from);exit;
            $from = Carbon::createFromFormat('H:i:s', $job_application->actual_time_from);
            $to = Carbon::createFromFormat('H:i:s', $job_application->actual_time_to);
            $diff_in_minutes = $from->diffInMinutes($to);
            $start = explode(":", $job_application->actual_time_from);
            $start_in_minutes = ($start[0] * 60) + $start[1];
            $end = explode(":", $job_application->actual_time_to);
            $end_in_minutes = ($end[0] * 60) + $end[1];
            // $work_input = $req->whr . ':' . $req->wmin . ':00';
            $work_input_in_minutes = ($req->whr * 60) + $req->wmin;
            $final_work_hours_in_minutes = $work_input_in_minutes + $start_in_minutes + $job_application->actual_rest_time;
            $final_work_hour = floor($final_work_hours_in_minutes / 60);
            $final_work_minutes = floor($final_work_hours_in_minutes % 60);
            $last_hours = $final_work_hour . ":" . $final_work_minutes . ":00";
            $job_application->actual_time_to = $last_hours;
            $job_application->save();
            return response()->json(['message' => '更新されました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }
}
