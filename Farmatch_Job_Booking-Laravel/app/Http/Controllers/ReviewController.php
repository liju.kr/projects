<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Chemist;
use App\JobPost;
use App\DrugStore;
use App\ChemistReview;
use App\WorkPlaceReview;

class ReviewController extends Controller
{
    public function chemistReviews(){

        $reviews = ChemistReview::orderBy('id', 'DESC')->get();
        foreach($reviews as $row){
            $row->chemist    =  Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
            $row->drugStore  =  DrugStore::where('id', $row->drug_store_id)->first()->name;
            $row->postDate   =  JobPost::where('id', $row->job_post_id)->first()->date;
            $row->email      =  Chemist::where('id', $row->chemist_id)->first()->email;
            $row->phone      =  Chemist::where('id', $row->chemist_id)->first()->phone;
        }
        return view('chemist_review')->with('allReviews', $reviews)
                                     ->with('reviews', 'kt-menu__item--active kt-menu__item--open')
                                     ->with('chemistReviewActive', 'kt-menu__item--active')
                                     ->with('page_title', '薬剤師評価');
    }

    public function drugStoreReviews(){

        $reviews = WorkPlaceReview::orderBy('id', 'DESC')->get();
        foreach($reviews as $row){
            $row->chemist    =  Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
            $row->drugStore  =  DrugStore::where('id', $row->drug_store_id)->first()->name;
            $row->postDate   =  JobPost::where('id', $row->job_post_id)->first()->date;
            $row->email      =  Chemist::where('id', $row->chemist_id)->first()->email;
            $row->phone      =  Chemist::where('id', $row->chemist_id)->first()->phone;
        }
        return view('drug_store_review')->with('allReviews', $reviews)
                                        ->with('reviews', 'kt-menu__item--active kt-menu__item--open')
                                        ->with('drugStoreReviewActive', 'kt-menu__item--active')
                                        ->with('page_title', '勤務報告');
    }

    public function chemistReviewDetails(Request $request){

        $review     =   ChemistReview::where('id', $request->reviewId)->first();
        $chemist    =   Chemist::where('id', $review->chemist_id)->first()->first_name;
        $drugStore  =   DrugStore::where('id', $review->drug_store_id)->first()->name;
        $email      =   Chemist::where('id', $review->chemist_id)->first()->email;
        $phone      =   Chemist::where('id', $review->chemist_id)->first()->phone;

        $reviewDetails =    '<table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="width: 37%;">薬局/病院等</td>
                                    <td>'.$drugStore.'</td>
                                </tr>
                                <tr>
                                    <td style="width: 37%;">薬剤師</td>
                                    <td>'.$chemist.'</td>
                                </tr>
                                <tr>
                                    <td>Eメール</td>
                                    <td>'.$email.'</td>
                                </tr>
                                <tr>
                                    <td>勤務日</td>
                                    <td>'.$phone.'</td>
                                </tr>
                                <tr>
                                    <td>患者対応</td>
                                    <td>'.$review->attitude.'</td>
                                </tr>
                                <tr>
                                    <td>職員との相性</td>
                                    <td>'.$review->adapting.'</td>
                                </tr>
                                <tr>
                                    <td>作業スピード</td>
                                    <td>'.$review->work_speed.'</td>
                                </tr>
                                <tr>
                                    <td>時給に対する働き</td>
                                    <td>'.$review->value_for_money.'</td>
                                </tr>
                                <tr>
                                    <td>その他</td>
                                    <td>'.$review->others.'</td>
                                </tr>
                                <tr>
                                    <td>合計</td>
                                    <td>'.($review->attitude + $review->adapting + $review->work_speed + $review->value_for_money) / 4  .'</td>
                                </tr>
                                </tbody>
                            </table>';
        return response()->json(['data' => $reviewDetails, 'status' => true]);
    }

    public function drugStoreReviewDetails(Request $request){

        $review     =   WorkPlaceReview::where('id', $request->reviewId)->first();
        $chemist    =   Chemist::where('id', $review->chemist_id)->first()->first_name;
        $drugStore  =   DrugStore::where('id', $review->drug_store_id)->first()->name;
        $email      =   Chemist::where('id', $review->chemist_id)->first()->email;
        $phone      =   Chemist::where('id', $review->chemist_id)->first()->phone;

        $reviewDetails  =   '<table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="width: 37%;">薬剤師</td>
                                    <td>'.$chemist.'</td>
                                </tr>
                                <tr>
                                    <td style="width: 37%;">薬局/病院等</td>
                                    <td>'.$drugStore.'</td>
                                </tr>
                                <tr>
                                    <td>Eメール</td>
                                    <td>'.$email.'</td>
                                </tr>
                                <tr>
                                    <td>勤務日</td>
                                    <td>'.$phone.'</td>
                                </tr>
                                <tr>
                                    <td>薬局/病院等の雰囲気</td>
                                    <td>'.$review->atmosphere.'</td>
                                </tr>
                                <tr>
                                    <td>事務員の対応</td>
                                    <td>'.$review->clerk_adaption.'</td>
                                </tr>
                                <tr>
                                    <td>薬剤師の対応</td>
                                    <td>'.$review->pharmacist_adaption.'</td>
                                </tr>
                                <tr>
                                    <td>コンプライアンス遵守の程度</td>
                                    <td>'.$review->compliance_level.'</td>
                                </tr>
                                <tr>
                                    <td>その他</td>
                                    <td>'.$review->others.'</td>
                                </tr>
                                <tr>
                                    <td>合計</td>
                                    <td>'.(($review->atmosphere + $review->clerk_adaption + $review->pharmacist_adaption + $review->compliance_level)/4).'</td>
                                </tr>
                                </tbody>
                            </table>';
        return response()->json(['data' => $reviewDetails, 'status' => true]);
    }

    public function chemistAverageReviews(){

        $avgReviews     =   DB::select("SELECT chemist_id,CAST(AVG(attitude) AS DECIMAL(10,2)) AS attitude, CAST(AVG(adapting) AS DECIMAL(10,2)) AS adapting, CAST(AVG(work_speed) AS DECIMAL(10,2)) AS work_speed,
                                        CAST(AVG(value_for_money) AS DECIMAL(10,2)) AS value_for_money FROM `chemist_reviews` GROUP BY chemist_id");
        foreach($avgReviews as $row){
            $row->chemist    =  Chemist::where('id', $row->chemist_id)->first()->last_name." ".Chemist::where('id', $row->chemist_id)->first()->first_name;
            $row->email      =  Chemist::where('id', $row->chemist_id)->first()->email;
            $row->phone      =  Chemist::where('id', $row->chemist_id)->first()->phone;
        }
        return view('chemist_average_review')->with('avgReviews', $avgReviews)
                                             ->with('reviews', 'kt-menu__item--active kt-menu__item--open')
                                             ->with('chemistAverageReviewActive', 'kt-menu__item--active')
                                             ->with('page_title', '薬剤師平均評価');
    }

    public function drugStoreAverageReviews(){

        $avgReviews     =   DB::select("SELECT drug_store_id,CAST(AVG(atmosphere) AS DECIMAL(10,2)) AS atmosphere, CAST(AVG(clerk_adaption) AS DECIMAL(10,2)) AS clerk_adaption, CAST(AVG(pharmacist_adaption) AS DECIMAL(10,2)) AS pharmacist_adaption,
                                        CAST(AVG(compliance_level) AS DECIMAL(10,2)) AS compliance_level FROM `work_place_reviews` GROUP BY drug_store_id");
        foreach($avgReviews as $row){
            $row->drug_store =  DrugStore::where('id', $row->drug_store_id)->first()->name;
            $row->email      =  DrugStore::where('id', $row->drug_store_id)->first()->email;
            $row->phone      =  DrugStore::where('id', $row->drug_store_id)->first()->phone;
        }
        return view('drug_store_average_review')->with('avgReviews', $avgReviews)
                                                ->with('reviews', 'kt-menu__item--active kt-menu__item--open')
                                                ->with('drugStoreAverageReviewActive', 'kt-menu__item--active')
                                                ->with('page_title', '薬局/病院等平均評価');
    }
}
