<?php

namespace App\Http\Controllers;

use mysqli;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\SendForgotPassword;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use App\User;

class LoginController extends Controller
{
    public function index(){

        $user = User::select('*')->where('phone', request('phone'))->first();
        if ($user) {
            if(Auth::attempt([ 'phone' => $user->phone, 'password' => request('password') ])) {
                session::put('name', $user->name);
                session::put('userId', $user->id);
                session::put('loginTime', Carbon::now());
                $digits               = 4;
                $phone_otp            = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $phone_otp_expire     = Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
                $updateData           = User::where('id', $user->id)->update([ 'phone_otp'        => $phone_otp,
                                                                               'phone_otp_expire' => $phone_otp_expire
                                                                            ]);
                $client = new Client();
                // $res    = $client->request('GET', 'http://ongoingprojects.info/aws-mesasge/message.php?number='.request('phone').'&otp='.$phone_otp);
                $res    = $client->request('GET', 'https://farmatch.work/aws-mesasge/message.php?number='.request('phone').'&otp='.$phone_otp);
                $res->getStatusCode();
                $res->getHeader('content-type');
                $res->getBody();
                return response()->json(['message' => '確認コードが送信されました', 'status' => TRUE]);
            } else {
                return response()->json(['message' => 'パスワードが間違っています', 'status' => FALSE]);
            }
        } else {
            return response()->json(['message' => '入力した電話番号が存在しません', 'status' => FALSE]);
        }
    }

    public function login(){

        return view('login');
    }

    public function forgotPassword(){

        return view('forgot_password');
    }

    public function forgotCheck(Request $request){

        $validator  = Validator::make($request->all(),[
            'forgotEmail'  => 'required|email'
          ]);
        if($validator->fails()) {
            return response()->json(['message' => '正しいメールアドレスを入力してください', 'status' => '0']);
        } else {
            $email = User::select('*')->where('email', $request->forgotEmail)->first();
            if ($email) {
            $email             =   $email->email;
            $otp               =   Str::random(5);
            $otp_expire        =   Carbon::now()->addHours(1)->format('Y-m-d H:i:s');
            $adminUserUpdate   =   User::where('email' ,'=', $request->forgotEmail)
                                            ->update(['otp' =>  $otp,'otp_expire' => $otp_expire]);
                if($adminUserUpdate){
                    $parameter    =   [ 'email' => $email, 'otp' => $otp ];
                    $parameter    =   Crypt::encrypt($parameter);
                    $resetLink    =   URL::to('/')."/password-reset/".$parameter;
                    try{
                        Mail::to($request->forgotEmail)->send(new SendForgotPassword($resetLink));
                        return response()->json(['message' => "Pパスワードリセットのためのリンクが登録済みのEメールアドレスに送信されました。メールボックスを確認してください",'status' => TRUE]);
                    }catch(\Exception $e){
                        return response()->json(['message' =>  '入力が正しくありません','status' => FALSE]);
                    }
                }else{
                    return response()->json(['message' => "入力が正しくありません",'status' => FALSE]);
                }
            }else{
                return response()->json(['message' => 'このEメールアドレスを持つアカウントが見つかりません', 'status' => FALSE]);
            }
        }
    }

    public function passwordReset($params){

        return view('password_reset')->with('params',$params);
    }

    public function passwordResetAction(Request $request){

        $validator  = Validator::make($request->all(),[
                        'params'            => 'required',
                        'password'          => 'required',
                        'confirmPassword'   => 'required'
                      ]);
        if($validator->fails()) {
            return response()->json(['message' => 'Please fill all the fields.', 'status' => '0']);
        } else {
            $params     =   Crypt::decrypt($request->params);
            $email      =   $params['email'];
            $otp        =   $params['otp'];
            $adminUser  =   User::select('*')->where('email', $email)
                                                  ->where('otp', $otp)
                                                  ->first();
            if($adminUser){
                if (Carbon::parse($adminUser->otp_expire)->greaterThan(Carbon::now())){
                    try{
                        $adminUserUpdate    =   User::where('otp' , $otp)
                                                           ->where('email' , $email)
                                                           ->update(['password'   => Hash::make($request->password),
                                                                     'otp'        => '',
                                                                     'updated_at' => Carbon::now()
                                                                   ]);
                        return response()->json(['message' => "パスワードが更新されました。新しいパスワードでログインしてください。", 'status' => '1']);
                    }catch (\Exception $e){
                        return response()->json(['message' => "入力が正しくありません", 'status' => FALSE]);
                    }
                }else{
                    return response()->json(['message' => "OTPの有効期限が切れました", 'status' => FALSE]);
                }
            }else{
                return response()->json(['message' => "リンクの期限が切れました", 'status' => FALSE]);
            }
        }
    }

    public function adminOtpCheck(Request $request){

        $validator  =   Validator::make($request->all(),[
            'otp'   => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $user  = User::where('phone_otp', $request->otp)->first();
                if ($user) {
                    if (Carbon::parse($user->phone_otp_expire)->greaterThan(Carbon::now())) {
                        try {
                            $userUpdate = User::where('phone_otp', $request->otp)
                                                ->update(['phone_otp'        => '',
                                                          'phone_otp_expire' => '',
                                                          'updated_at'       => Carbon::now(),
                                                        ]);
                            User::where('id', $user->id)->update([ 'online_status' => 'Online']);
                            return response()->json(['message' => "ログイン", 'status' => true, 'role' => $user->role_id]);
                        } catch (\Exception $e) {
                            return response()->json(['message' => "入力が正しくありません", 'status' => false]);
                        }
                    } else {
                        return response()->json(['message' => "確認コードが期限切れです", 'status' => false]);
                    }
                } else {
                    return response()->json(['message' => "確認コードが一致しません", 'status' => false]);
                }
            } catch(\Exception $e){
                return response()->json([ 'status' => false, 'message' => '入力が正しくありません' ]);
            }
        }
    }

    public function adminOtpVerification(Request $request){

        return view('verify_otp');
    }

    public function databaseBackup(Request $request){

       // $this->EXPORT_DATABASE("localhost", "phpmyadmin", "farmatch567", "demo" );
        $this->EXPORT_DATABASE(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE') );
    }

    function EXPORT_DATABASE($host,$user,$pass,$name,$tables=false, $backup_name=false) {

        set_time_limit(3000); $mysqli = new mysqli($host,$user,$pass,$name); $mysqli->select_db($name); $mysqli->query("SET NAMES 'utf8'");
        $queryTables = $mysqli->query('SHOW TABLES'); while($row = $queryTables->fetch_row()) { $target_tables[] = $row[0]; }	if($tables !== false) { $target_tables = array_intersect( $target_tables, $tables); }
        $content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8 */;\r\n--\r\n-- Database: `".$name."`\r\n--\r\n\r\n\r\n";
        foreach($target_tables as $table){
            if (empty($table)){ continue; }
            $result	= $mysqli->query('SELECT * FROM `'.$table.'`');  	$fields_amount=$result->field_count;  $rows_num=$mysqli->affected_rows; 	$res = $mysqli->query('SHOW CREATE TABLE '.$table);	$TableMLine=$res->fetch_row();
            $content .= "\n\n".$TableMLine[1].";\n\n";   $TableMLine[1]=str_ireplace('CREATE TABLE `','CREATE TABLE IF NOT EXISTS `',$TableMLine[1]);
            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
                while($row = $result->fetch_row())	{ //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )	{$content .= "\nINSERT INTO ".$table." VALUES";}
                        $content .= "\n(";    for($j=0; $j<$fields_amount; $j++){ $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); if (isset($row[$j])){$content .= '"'.$row[$j].'"' ;}  else{$content .= '""';}	   if ($j<($fields_amount-1)){$content.= ',';}   }        $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {$content .= ";";} else {$content .= ",";}	$st_counter=$st_counter+1;
                }
            } $content .="\n\n\n";
        }
        $content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
        $backup_name = $backup_name ? $backup_name : $name.'___('.date('H-i-s').'_'.date('d-m-Y').').sql';
        ob_get_clean(); header('Content-Type: application/octet-stream');  header("Content-Transfer-Encoding: Binary");  header('Content-Length: '. (function_exists('mb_strlen') ? mb_strlen($content, '8bit'): strlen($content)) );    header("Content-disposition: attachment; filename=\"".$backup_name."\"");
        echo $content; exit;
    }

    public function backup(Request $request){

        return view('backup_database')->with('page_title', 'バックアップ');
    }

    public function logout() {

        User::where('id', Auth::user()->id)->update(['online_status' => 'Offline']);
        Auth::logout();
        Session::flush();
        return redirect()->route('login');
    }
}
