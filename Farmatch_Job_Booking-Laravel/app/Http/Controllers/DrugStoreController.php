<?php

namespace App\Http\Controllers;

use Endroid\QrCode\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\City;
use App\JobPost;
use App\DrugStore;
use App\ChemistJobApplication;

class DrugStoreController extends Controller
{

    public function pending(){

        $drugStores     =   DrugStore::select('id', 'name', 'phone', 'email', 'in_charge', 'online_status', 'created_at')
                                        ->where('status', 'Pending')
                                        ->orderBy('id', 'DESC')
                                        ->get();
        $cities         =   City::select('id', 'city')->get();
        return view('pending_drug_store_approval')  ->with('DrugStoreActive', 'kt-menu__item--active kt-menu__item--open')
                                                    ->with('pendingDrugStoreActive', 'kt-menu__item--active')
                                                    ->with('drugStores', $drugStores)
                                                    ->with('cities', $cities)
                                                    ->with('page_title', '承認待ち');
    }
    public function approved(){

        $drugStores     =   DrugStore::select('id', 'name', 'phone', 'email', 'in_charge', 'online_status', 'created_at')
                                        ->where('status', 'Approved')
                                        ->orderBy('id', 'DESC')
                                        ->get();
        $cities         =   City::select('id', 'city')->get();
        return view('approved_drug_stores') ->with('DrugStoreActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('approvedDrugStoreActive', 'kt-menu__item--active')
                                            ->with('drugStores', $drugStores)
                                            ->with('cities', $cities)
                                            ->with('page_title', '承認済み');
    }
    public function suspended(){

        $drugStores     =   DrugStore::select('id', 'name', 'phone', 'email', 'in_charge', 'online_status', 'created_at')
                                        ->where('status', 'Suspended')
                                        ->orderBy('id', 'DESC')
                                        ->get();
        $cities         =   City::select('id', 'city')->get();
        return view('suspended_drug_stores')    ->with('DrugStoreActive', 'kt-menu__item--active kt-menu__item--open')
                                                ->with('suspendDrugStoreActive', 'kt-menu__item--active')
                                                ->with('drugStores', $drugStores)
                                                ->with('cities', $cities)
                                                ->with('page_title', '停職済み');
    }

    public function approveStore($drugStoreId){

        $drugStore         = DrugStore::where('id', $drugStoreId)->first();
        $registeredDate    = Carbon::parse($drugStore->created_at);
        $diffYears         = Carbon::now()->diffInYears($registeredDate);
        $completedWorks         = JobPost::whereDate('date', '<=', Carbon::yesterday()->toDateString())
                                            ->where('status', 'Active')
                                            ->where('drug_store_id', $drugStoreId)
                                            ->count();
        $chemistWorked          = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->where('status', 'Job Completed')
                                                        ->count();
        $chemistRepeatWorked    = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->groupBy('chemist_id')
                                                        ->where('status', 'Job Completed')
                                                        ->selectRaw('chemist_id, count(chemist_id) as count')
                                                        ->get();
        $repeated = 0;
        foreach($chemistRepeatWorked as $row){
            if($row->count > 1)
                $repeated++;
        }
        $suddenCancellation     =  ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->where('status', 'Job Cancelled')
                                                        ->count();
        return view('approve_drug_store')   ->with('page_title', 'Approve DrugStore')
                                            ->with('drugStoreActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('drugStore', $drugStore)
                                            ->with('drugStoreExperience', $diffYears)
                                            ->with('completedWorks', $completedWorks)
                                            ->with('chemistWorked', $chemistWorked)
                                            ->with('chemistRepeated', $repeated)
                                            ->with('suddenCancellation', $suddenCancellation);
    }

    public function updateDrugStore(Request $request){

        $drugStore  = DrugStore::where('id', $request->id)->first();
        $validator  = Validator::make($request->all(),[
            'name'                  => 'required',
            'manager_company'       => 'required',
            'history_model'         => 'required',
            'mobile'                => 'required|unique:drug_stores,phone,'.$request->id.',id,is_verified,1',
            'alter_mobile'          => 'required',
            'email'                 => 'required|unique:drug_stores,email,'.$request->id.',id,is_verified,1',
            'in_charge'             => 'required',
            'clerks'                => 'required',
            'chemists'              => 'required',
            'response_speed'        => 'required'
        ], [
            'mobile.unique'      => 'この番号はすでに使用されています',
            'email.unique'       => 'このEメールアドレスは既に使用されています。',
        ]);
        if($validator->fails()) {
        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
        }else{
            try{
                if($request->file('main_image')){
                    $validator  = Validator::make($request->all(),[
                                        'main_image'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'main_image.required' => 'Please select an image',
                                        'main_image.mimes'    => 'Main Picture , Allowed image types are pngmjpg,jpeg',
                                        'main_image.max'      => 'Main picture , Maximum file size allowed is 2 MB'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('main_image');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->image);
                    }
                    $drugStore->main_image   = $newImage;
                }
                if($request->file('sub_image1')){
                    $validator  = Validator::make($request->all(),[
                                        'sub_image1'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'sub_image1.required' => 'Please select an image',
                                        'sub_image1.mimes'    => 'Sub picture 1 , Allowed image types are pngmjpg,jpeg',
                                        'sub_image1.max'      => 'Sub picture 1 , Maximum file size allowed is 2 MB'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('sub_image1');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->sub_image1);
                    }
                    $drugStore->sub_image1   = $newImage;
                }
                if($request->file('sub_image2')){
                    $validator  = Validator::make($request->all(),[
                                        'sub_image2'  => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
                                    ],[
                                        'sub_image2.required' => 'Please select an image',
                                        'sub_image2.mimes'    => 'Sub picture 2 , Allowed image types are pngmjpg,jpeg',
                                        'sub_image2.max'      => 'Sub picture 2 , Maximum file size allowed is 2 MB'
                                    ]);
                    if($validator->fails()){
                        return response()->json(['message' => $validator->errors()->first(), 'status' => FALSE]);
                    }else {
                        $image        = $request->file('sub_image2');
                        $newImage     = rand().'.'.$image->getClientOriginalExtension();
                        $image->move(public_path('images/drug_store/'),$newImage);
                        File::delete(public_path().'/images/profile/'.$drugStore->sub_image2);
                    }
                    $drugStore->sub_image2   = $newImage;
                }
                $drugStore->name                = request('name');
                $drugStore->manager_company     = request('manager_company');
                $drugStore->history_model       = request('history_model');
                $drugStore->phone               = request('mobile');
                $drugStore->alter_phone         = request('alter_mobile');
                $drugStore->email               = request('email');
                $drugStore->in_charge           = request('in_charge');
                $drugStore->introduction        = request('introduction');
                $drugStore->clerks              = request('clerks');
                $drugStore->chemists            = request('chemists');
                $drugStore->respond_speed       = request('response_speed');
                $drugStore->other_information   = request('other_informations');
                $drugStore->save();
                return response()->json(['message' => '更新が完了しました。', 'status' => TRUE]);
            } catch(\Exception $e){
                return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
            }
        }
    }

    public function DrugStoreApproveRequest(Request $request){
        try{
            DrugStore::where('id', $request->chemistId)
                    ->update([ 'status' =>  'Approved' ]);
            \Helper::instance()->addAction(Auth::user()->id, $request->chemistId, 'DrugStore', '新しい薬局/病院等を承認しました', 'flaticon2-hospital kt-font-success');
            return response()->json(['message' => '承認しました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function suspendDrugStore($drugStoreId){

        $drugStore         = DrugStore::where('id', $drugStoreId)->first();
        $registeredDate    = Carbon::parse($drugStore->created_at);
        $diffYears         = Carbon::now()->diffInYears($registeredDate);
        $completedWorks         = JobPost::whereDate('date', '<=', Carbon::yesterday()->toDateString())
                                            ->where('status', 'Active')
                                            ->where('drug_store_id', $drugStoreId)
                                            ->count();
        $chemistWorked          = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->where('status', 'Job Completed')
                                                        ->count();
        $chemistRepeatWorked    = ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->groupBy('chemist_id')
                                                        ->where('status', 'Job Completed')
                                                        ->selectRaw('chemist_id, count(chemist_id) as count')
                                                        ->get();
        $repeated = 0;
        foreach($chemistRepeatWorked as $row){
            if($row->count > 1)
                $repeated++;
        }
        $suddenCancellation     =  ChemistJobApplication::whereDate('date', '<=', Carbon::now()->toDateString())
                                                        ->where('drug_store_id', $drugStoreId)
                                                        ->where('status', 'Job Cancelled')
                                                        ->count();
        return view('suspend_drug_store')   ->with('page_title', 'Approve DrugStore')
                                            ->with('drugStoreActive', 'kt-menu__item--active kt-menu__item--open')
                                            ->with('drugStore', $drugStore)
                                            ->with('drugStoreExperience', $diffYears)
                                            ->with('completedWorks', $completedWorks)
                                            ->with('chemistWorked', $chemistWorked)
                                            ->with('chemistRepeated', $repeated)
                                            ->with('suddenCancellation', $suddenCancellation);
    }

    public function drugStoreSuspendRequest(Request $request){

        try{
            DrugStore::where('id', $request->chemistId)
                       ->update([ 'status' =>  'Suspended' ]);
            \Helper::instance()->addAction(Auth::user()->id, $request->chemistId, 'DrugStore', '新しい薬局/病院等を拒否しました', 'flaticon2-hospital kt-font-danger');
            return response()->json(['message' => '停職にしました', 'status' => TRUE]);
        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function searchDrugStore(Request $request){

        try{
            $fromDate = Carbon::createFromFormat('m/d/Y', $request->fromDate)->format('Y-m-d');
            $toDate   = Carbon::createFromFormat('m/d/Y', $request->toDate)->format('Y-m-d');
            $stores   = DrugStore::whereBetween('created_at', [$fromDate." 00:00:00", $toDate." 23:59:59"])->where('status', $request->status)->get();
            if(count($stores)){
                $table    = '<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Store Name</th>
                                        <th>Representative Name</th>
                                        <th>Mobile</th>
                                        <th>E-mail</th>
                                        <th>Registered Date</th>
                                        <th>Online Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';
                $i = 1;
                foreach($stores as $row){
                    $table .=   '<tr>
                                    <td>'.$i.'</td>
                                    <td>'.$row->name.'</td>
                                    <td>'.$row->in_charge.'</td>
                                    <td>'.$row->phone.'</td>
                                    <td>'.$row->email.'</td>
                                    <td>'.date('d/m/Y', strtotime($row->created_at)).'</td>
                                    <td>Online</td>
                                    <td nowrap>
                                        <a href="chat" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                            <i class="la la-wechat" style="font-size: 2.3rem;"></i>
                                        </a>
                                        <a href="approve-drug-store/'.$row->id .'" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="View Chemist">
                                            <i class="la la-eye" style="font-size: 2.3rem;"></i>
                                        </a>
                                    </td>
                                </tr>';
                    $i++;
                }
                $table     .=   '</tbody>
                            </table>';
                return response()->json(['message' => $table, 'status' => TRUE]);
            }else{
                return response()->json(['message' => '<span class="text-center kt-font-danger">Nothing to show.</span>', 'status' => TRUE]);
            }

        } catch(\Exception $e){
            return response()->json(['message' => '入力が正しくありません', 'status' => FALSE]);
        }
    }

    public function locationSearchDrugStore(Request $request){

        try{
            $getlatLong     =   City::select('latitude', 'longitude')->where('id', $request->cityId)->first();
            $drugStores     =   DB::select("SELECT *, ( 3959 * ACOS( COS( RADIANS(9.9658) ) * COS( RADIANS( $getlatLong->latitude ) ) * COS( RADIANS( $getlatLong->longitude ) - RADIANS(76.2421) )
                                       + SIN( RADIANS(9.9658) ) * SIN(RADIANS( $getlatLong->latitude )) ) ) AS distance FROM `drug_stores` where `status` = '$request->status'  ORDER BY distance");
            if(count($drugStores)){
                $table    = '<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Store Name</th>
                                        <th>Representative Name</th>
                                        <th>Mobile</th>
                                        <th>E-mail</th>
                                        <th>Registered Date</th>
                                        <th>Online Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';
                $i = 1;
                foreach($drugStores as $row){
                    $table .=   '<tr>
                                    <td>'.$i.'</td>
                                    <td>'.$row->name.'</td>
                                    <td>'.$row->in_charge.'</td>
                                    <td>'.$row->phone.'</td>
                                    <td>'.$row->email.'</td>
                                    <td>'.date('d/m/Y', strtotime($row->created_at)).'</td>
                                    <td>Online</td>
                                    <td nowrap>
                                        <a href="chat" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="Chat">
                                            <i class="la la-wechat" style="font-size: 2.3rem;"></i>
                                        </a>
                                        <a href="approve-drug-store/'.$row->id .'" class="btn btn-lg btn-clean btn-icon btn-icon-md" title="View Chemist">
                                            <i class="la la-eye" style="font-size: 2.3rem;"></i>
                                        </a>
                                    </td>
                                </tr>';
                    $i++;
                }
                $table     .=   '</tbody>
                            </table>';
                return response()->json(['message' => $table, 'status' => TRUE]);
            }else{
                return response()->json(['message' => '<span class="text-center kt-font-danger">表示するデータが現在ありません.</span>', 'status' => TRUE]);
            }
        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'status' => FALSE]);
        }
    }

    public function pickAStoreAdmin(Request $request){

        $query_string = $request->q;
        $stores = DrugStore::where(function ($q) use ($query_string) {
                                $q->orWhere('name', 'like', "%{$query_string}%");
                                $q->orWhere('email', 'like', "%{$query_string}%");
                                $q->orWhere('phone', 'like', "%{$query_string}%");
                                $q->orWhere('licence_number', 'like', "%{$query_string}%");
                            })  ->where('status', 'Approved')
                                ->limit(20)->orderBy('name','asc')->get();

        $response = [];
        if($stores->count() > 0){
            $i = 0;
            foreach($stores as $store){
                $response[$i]['id'] = $store->id;
                $response[$i]['text'] = $store->name."(".$store->email.")";
                $i++;
            }
        }
        return response()->json(['results' => $response]);
    }

    public function drugStoreQrCode($drugStoreId){

        $drugStoreName = DrugStore::select('name')->where('id', $drugStoreId)->first()->name;
        return view('qr_code')->with('drugstoreId', $drugStoreId)
                              ->with('name', $drugStoreName);
    }
}
