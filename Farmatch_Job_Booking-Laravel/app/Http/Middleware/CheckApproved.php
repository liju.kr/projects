<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Closure;
use App\DrugStore;

class CheckApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(DrugStore::where('id', $request->user()->id)->first()->status != 'Approved'){
            $adminApproveStatus = "Not Approved";
        }else{
            $adminApproveStatus = "Approved";
        }
        app()->instance('adminApproveStatus', $adminApproveStatus);
        return $next($request);
    }
}
