<?php

namespace App\Http\Middleware;

use Illuminate\Support\Carbon;

use Closure;
use DateTime;
use App\Chemist;
use App\DrugStore;


class CheckNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $chemists   = Chemist::select('id', 'first_name', 'phone', 'email', 'created_at')->where('status', 'Pending')->orderBy('id', 'DESC')->get();
        $drugStores = DrugStore::select('id', 'name', 'phone', 'email', 'created_at')->where('status', 'Pending')->orderBy('id', 'DESC')->get();
        $notificationArray = [];$i = 0;
        foreach($chemists as $row){
            $notificationArray[$i]['id']           = $row->id;
            $notificationArray[$i]['name']         = $row->first_name;
            $notificationArray[$i]['type']         = 'Chemist';
            $notificationArray[$i]['phone']        = $row->phone;
            $notificationArray[$i]['email']        = $row->email;
            $notificationArray[$i]['created_at']   = Carbon::parse($row->created_at)->format('Y-m-d H:i:s');;
            $i++;
        }
        foreach($drugStores as $row){
            $notificationArray[$i]['id']           = $row->id;
            $notificationArray[$i]['name']         = $row->name;
            $notificationArray[$i]['type']         = 'Drug Store';
            $notificationArray[$i]['phone']        = $row->phone;
            $notificationArray[$i]['email']        = $row->email;
            $notificationArray[$i]['created_at']   = Carbon::parse($row->created_at)->format('Y-m-d H:i:s');;
            $i++;
        }
        usort($notificationArray, function($a, $b) {
            return new DateTime($a['created_at']) <=> new DateTime($b['created_at']);
        });
        $notificationArray = array_reverse($notificationArray, true);
        array_splice($notificationArray, 5);
        app()->instance('myObj', $notificationArray);
        return $next($request);
    }
}
