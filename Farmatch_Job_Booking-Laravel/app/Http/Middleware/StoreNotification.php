<?php

namespace App\Http\Middleware;

use Closure;

use App\Notification;

class StoreNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        if(Notification::where('drug_store_id', $request->user()->id)->where('read_status', 0)->count()){
            app()->instance('storeNotificationStatus', TRUE);
        }else{
            app()->instance('storeNotificationStatus', FALSE);
        }
        return $next($request);
    }
}
