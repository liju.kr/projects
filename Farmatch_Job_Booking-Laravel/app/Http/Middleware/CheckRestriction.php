<?php

namespace App\Http\Middleware;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

use Closure;
use DateTime;
use App\Chemist;
use App\DrugStore;
use App\AccessRestriction;


class CheckRestriction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $restrictions = AccessRestriction::where('user_id', Auth::user()->id)->first();
        app()->instance('restrictions', $restrictions);
        return $next($request);
    }
}
