<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Api routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Api" middleware group. Enjoy building your Api!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('signup', 'Api\AuthController@signup');
    Route::post('verify-otp', 'Api\AuthController@verifyOtp');
    Route::post('login', 'Api\AuthController@login');
    Route::get('password-reset', 'Api\AuthController@passwordReset');
    Route::get('verify-reset-otp', 'Api\AuthController@verifyResetOtp');
    Route::post('reset-password', 'Api\AuthController@resetPassword');
    Route::get('resend-otp', 'Api\AuthController@resendOtp');
    Route::get('check-phone-number-exist', 'Api\AuthController@checkPhoneNumberExist');
    Route::get('check-email-exist', 'Api\AuthController@checkEmailExist');
    Route::get('list-years', 'Api\SettingController@listyears')->name('listyears');
    Route::get('get-cities-test', 'Api\SettingController@getCities');

    Route::get('valid', 'Api\AuthController@checkValid');
    Route::get('version', 'Api\AuthController@appVersion');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('get-prefecture', 'Api\SettingController@getPrefecture');
        Route::get('get-cities', 'Api\SettingController@getCities');
        Route::get('job-post-listing', 'Api\JobPostController@jobPostListing');
        Route::get('job-post-details', 'Api\JobPostController@jobPostDetails');
        Route::post('add-remove-favourite', 'Api\JobPostController@addRemoveFavourite');
        Route::get('get-interview-timing', 'Api\JobPostController@getInterviewTiming');
        Route::post('apply-timing', 'Api\JobPostController@applyTiming');
        Route::post('apply-job', 'Api\JobPostController@applyJob');
        Route::post('reject-job', 'Api\JobPostController@rejectJob');
        Route::post('send-message', 'Api\AuthController@sendMessage');
        Route::get('shift-of-this-month', 'Api\JobPostController@shiftOfThisMonth');
        Route::get('shift-details', 'Api\JobPostController@shiftDetails');
        Route::post('review-work-place', 'Api\JobPostController@reviewWorkPlace');
        Route::post('hourly-wage-settings', 'Api\SettingController@hourlyWageSettings');
        Route::get('get-chemist-hourly-wage', 'Api\SettingController@getChemistHourlyWage');
        Route::get('get-notification-messages', 'Api\SettingController@getNotificationMessages');
        Route::get('get-pending-interview-timing', 'Api\SettingController@getPendingIntervieTiming');
        Route::get('get-all-job-posts', 'Api\JobPostController@getAllJobPosts');
        Route::post('send-notification', 'Api\SettingController@sendNotification');
        Route::get('download-resume', 'Api\SettingController@downloadResume');
        Route::post('update-profile', 'Api\AuthController@updateProfile');
        Route::get('get-profile-details', 'Api\AuthController@getProfileDetails');
        Route::get('get-profile-picture', 'Api\AuthController@getProfilePicture');
        Route::post('set-profile-picture', 'Api\AuthController@setProfilePicture');
        Route::get('get-chemist-payment', 'Api\SettingController@getChemistPayment');
        Route::get('pending-jobs', 'Api\SettingController@pendingJobs');
        Route::post('cancel-job-application', 'Api\JobPostController@cancelJobApplication');
        Route::get('get-job-history', 'Api\JobPostController@getJobHistory');
        Route::get('get-jobs-of-perticular-date', 'Api\JobPostController@getJobsOfPerticularDate');
        Route::post('update-actual-timing', 'Api\JobPostController@updateActualTiming');

        //chats
        Route::get('group-chat-heads', 'Api\ChatController@groupChatHeads');
        Route::post('chemist-mark-read-group', 'Api\ChatController@chemistMarkReadGroup');
        Route::get('store-chat-heads', 'Api\ChatController@storeChatHeads');
        Route::post('chemist-mark-read-store-chemist', 'Api\ChatController@chemistMarkReadStoreChemist');
        Route::get('admin-chat-head', 'Api\ChatController@adminChatHead');
        Route::post('chemist-mark-read-admin', 'Api\ChatController@chemistMarkReadAdmin');



        Route::post('start-group-chat', 'Api\ChatController@startGroupChat');
        Route::post('save-group-chat', 'Api\ChatController@saveGroupChat');

        Route::post('save-store-chat', 'Api\ChatController@saveStorechat');
        Route::post('start-store-chat', 'Api\ChatController@startNewStoreChat');


        Route::post('save-admin-chat', 'Api\ChatController@saveAdminChat');

        Route::post('chemist-accept-reject-application', 'Api\JobPostController@chemistAcceptRejectApplication');
        Route::post('work-content-confirmation', 'Api\JobPostController@workContentConfirmation');

        Route::get('pending-reviews', 'Api\JobPostController@pendingReviews');

        Route::get('get-chemist-payment-history', 'Api\SettingController@getChemistPaymentHistory');

        Route::get('get-job-content', 'Api\JobPostController@getJobContent');

        Route::get('chemist-faq', 'Api\SettingController@chemistFaq');
        Route::get('delete-notification', 'Api\SettingController@deleteNotification');

        Route::post('delete-group-chat', 'Api\ChatController@deleteGroupChat');
        Route::post('delete-store-chat', 'Api\ChatController@deleteStoreChat');
        Route::post('delete-admin-chat', 'Api\ChatController@deleteAdminChat');

        Route::get('chatheadcount', 'Api\ChatController@chatHeadCount');


        /**************************************** Second Phase ***************************************************/

        Route::get('approved-by-drug-store', 'Api\NotificationController@approvedByDS');
        Route::get('rejected-cancelled-by-drug-store', 'Api\NotificationController@rejectedCancelledByDS');
        Route::get('job-confirm-notification', 'Api\NotificationController@jobConfirmNotification');
        Route::get('other-notifications', 'Api\NotificationController@otherNotifications');
        Route::get('notification-counts', 'Api\NotificationController@notificationCounts');

        Route::post('apply-inspection-timing', 'Api\JobPostController@applyinspectionTiming');

        Route::get('get-job-days-by-filter', 'Api\JobPostController@getJobDaysByFilter');
        Route::post('update-device-token', 'Api\AuthController@updateDeviceToken');

        Route::get('get-job-content-by-qr-code', 'Api\JobPostController@getJobContentByQrCode');
        Route::get('get-chemist-favuorite-list', 'Api\JobPostController@getFavouriteList');
        Route::get('get-chemist-favuorite-list-filter', 'Api\JobPostController@getFavouriteListFilter');

        /**************************************** Third Phase ***************************************************/

        Route::get('get-drugstore-post-count', 'Api\JobPostController@getDrugstorePostCount');
        Route::get('get-drug-store-posts', 'Api\JobPostController@getDrugStorePosts');


        /**************************************** 5th Phase ***************************************************/
        Route::get('get-chemist-location', 'Api\SettingController@getChemistLocation');
        Route::post('save-chemist-location', 'Api\SettingController@saveChemistLocation');

        Route::get('shift-of-this-month-job-history', 'Api\JobPostController@shiftOfThisMonthJobHistory');
        Route::get('shift-history-details', 'Api\JobPostController@shiftHistoryDetails');



    });
});


Route::get('get-drugstore-post-count', 'Api\JobPostController@getDrugstorePostCount');
Route::get('get-drug-store-posts', 'Api\JobPostController@getDrugStorePosts');
