<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Http\Controllers\Api\JobPostController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('store.login');
});

Route::get('/admin', function () {
    return view('login');
});

Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@login']);
Route::post('/login-check', 'LoginController@index');

Route::get('admin-otp-verification', 'LoginController@adminOtpVerification');
Route::post('admin-otp-check', 'LoginController@adminOtpCheck');

Route::get('firebase', 'FirebaseController@index');

Route::get('/forgot-password', 'LoginController@forgotPassword');
Route::post('/forgot-check', 'LoginController@forgotCheck');

Route::get('password-reset/{parameter}', 'LoginController@passwordReset');
Route::post('password-reset/password-reset-action', 'LoginController@passwordResetAction')->name('password-reset-action');
Route::get('store-pdf-check', 'Store\HomeController@generatepdf')->name('generatepdf');
Route::get('store-pdf-check-view', 'Store\HomeController@generatepdfview')->name('generatepdfview');
Route::group(['middleware' => ['auth', 'revalidate', 'notification', 'restrictions']], function () {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::get('/pending-drug-store', 'DrugStoreController@pending')->name('pending-drug-store');
    Route::get('/approved-drug-store', 'DrugStoreController@approved')->name('approved-drug-store');
    Route::get('/suspended-drug-store', 'DrugStoreController@suspended')->name('suspended-drug-store');
    Route::get('/drug-store-qr-code/{qr_code}', 'DrugStoreController@drugStoreQrCode')->name('drug-store-qr-code');

    Route::get('/pending-chemist', 'ChemistController@pending')->name('pending-chemist');
    Route::get('/approved-chemist', 'ChemistController@approved')->name('approved-chemist');
    Route::get('/suspended-chemist', 'ChemistController@suspended')->name('suspended-chemist');

    Route::get('/notifications', 'NotificationController@index')->name('notifications');
    Route::get('/send-drug-store-notification', 'NotificationController@sendDrugStoreNotification')->name('send-drug-store-notification');
    Route::post('/add-drug-store-notification', 'NotificationController@adddDrugStoreNotification')->name('add-drug-store-notification');
    Route::get('/send-chemist-notification', 'NotificationController@sendChemistNotification')->name('send-chemist-notification');
    Route::post('/add-chemist-notification', 'NotificationController@adddChemistNotification')->name('add-chemist-notification');

    // Route::get('/admin-download-invoice/{invoice_id}', 'Store\SettingController@downloadInvoice')->name('admin-download-invoice');
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/update-profile', 'ProfileController@updateProfile');
    Route::get('/change-password', 'ProfileController@changePassword');
    Route::post('/reset-password', 'ProfileController@resetPassword');
    Route::get('/prefecture', 'ProfileController@getPrefectures')->name('prefecture');
    Route::post('/add-prefecture', 'ProfileController@addPrefecture')->name('add-prefecture');
    Route::get('/city', 'ProfileController@getCities')->name('city');
    Route::post('/add-city', 'ProfileController@addCity')->name('add-city');
    Route::post('/delete-city', 'ProfileController@deleteCity')->name('delete-city');
    Route::post('/edit-city', 'ProfileController@editCity')->name('edit-city');
    Route::post('/edit-city-action', 'ProfileController@editCityAction')->name('edit-city-action');
    Route::post('/delete-prefecture', 'ProfileController@deletePrefecture')->name('delete-prefecture');
    Route::post('/edit-prefecture', 'ProfileController@editPrefecture')->name('edit-prefecture');
    Route::post('/edit-prefecture-action', 'ProfileController@editPrefectureAction')->name('edit-prefecture-action');
    Route::get('/faq', 'ProfileController@faq')->name('faq');
    Route::post('/add-faq', 'ProfileController@addFaq')->name('add-faq');
    Route::post('/delete-faq', 'ProfileController@deleteFaq')->name('delete-faq');
    Route::post('/edit-faq', 'ProfileController@editFaq')->name('edit-faq');
    Route::post('/edit-faq-action', 'ProfileController@editFaqAction')->name('edit-faq-action');

    /******************** New Route *******************/
    Route::get('/admin-download-invoice/{payment_information}', 'PaymentController@downloadInvoice')->name('admin-download-invoice');
    Route::get('/admin-download-invoice-action/{invoice_id}', 'Store\SettingController@downloadInvoice')->name('admin-download-invoice-action');
    Route::post('/generate-invoice-popup', 'PaymentController@generateInvoicePopup')->name('generate-invoice-popup');
    Route::post('/save-invoice-additional-details', 'PaymentController@saveInvoiceAdditionalDetails')->name('save-invoice-additional-details');
    Route::post('/download-all-drugstore-invoice', 'PaymentController@downloadAllDrugstoreInvoice')->name('download-all-drugstore-invoice');
    Route::post('/get-hourly-wage', 'InterviewController@getHourlyWage')->name('get-hourly-wage');
    Route::post('/update-hourly-wage', 'InterviewController@updateHourlyWage')->name('update-hourly-wage');

    /**************************************************** */

    Route::get('/chat/{id}', 'ChatController@index');
    Route::get('/chat', 'ChatController@home')->name('chat');

    Route::post('/admin-mark-read-store', 'ChatController@adminMarkReadStore')->name('admin-mark-read-store');

    Route::post('/admin-chat', 'ChatController@saveChat')->name('save-chat-admin');
    Route::post('/start-admin-chat', 'ChatController@startChat')->name('start-new-store-chat-admin');

    Route::get('/get-drug-store-details', 'ChatController@getDrugStoreDetails')->name('get-drug-store-details');

    Route::get('group-chat', 'ChatController@groupChatAdmin')->name('group-chat-admin');
    Route::post('/admin-mark-read-group', 'ChatController@adminMarkReadGroup')->name('admin-mark-read-group');

    Route::post('/group-chat', 'ChatController@saveGroupChatAdmin')->name('save-groupchat-admin');
    Route::get('/get-group-chat-details', 'ChatController@getGroupChatDetails')->name('get-group-chat-details');

    Route::get('/pick-a-store', 'DrugStoreController@pickAStoreAdmin')->name('pick-a-store-admin');
    Route::get('/pick-a-chemist', 'ChemistController@pickAChemistAdmin')->name('pick-a-chemist-admin');
    Route::get('/start-new-group-chat-admin', 'ChatController@startNewGroupChatAdmin')->name('start-new-group-chat-admin');


    Route::get('/chemist-chat', 'ChatController@chemistChatAdmin')->name('chemist-chat-admin');
    Route::post('/admin-mark-read-chemist', 'ChatController@adminMarkReadChemist')->name('admin-mark-read-chemist');

    Route::get('/start-new-chemist-chat-admin', 'ChatController@startNewChemistChatAdmin')->name('start-new-chemist-chat-admin');
    Route::post('save-chemistchat-admin', 'ChatController@saveChemistchatAdmin')->name('save-chemistchat-admin');
    Route::get('/get-chemist-details-admin', 'ChatController@getChemistDetailsAdmin')->name('get-chemist-details-admin');


    Route::get('/approve-drug-store/{drug_store_id}', 'DrugStoreController@approveStore')->name('approve-drug-store');
    Route::get('/suspend-drug-store/{drug_store_id}', 'DrugStoreController@suspendDrugStore');
    Route::get('/suspend-drug-store', 'DrugStoreController@suspendStore');
    Route::post('/update-drug-store', 'DrugStoreController@updateDrugStore')->name('update-drug-store');
    Route::post('/approve-drug-store-request', 'DrugStoreController@DrugStoreApproveRequest')->name('approve-drug-store-request');
    Route::post('/suspend-drug-store-request', 'DrugStoreController@drugStoreSuspendRequest')->name('suspend-drug-store-request');
    Route::post('/search-drug-store', 'DrugStoreController@searchDrugStore')->name('search-drug-store');
    Route::post('/location-search-drug-store', 'DrugStoreController@locationSearchDrugStore')->name('location-search-drug-store');

    Route::get('/approve-chemist/{chemist_id}', 'ChemistController@approveChemist')->name('approve-chemist');
    Route::get('/suspend-chemist/{chemist_id}', 'ChemistController@suspendChemist');
    Route::post('/search-chemist', 'ChemistController@searchChemist')->name('search-chemist');

    Route::post('/update-chemist', 'ChemistController@updateChemist')->name('update-chemist');
    Route::post('/approve-chemist-request', 'ChemistController@chemistApproveRequest')->name('approve-chemist-request');
    Route::post('/suspend-chemist-request', 'ChemistController@chemistSuspendRequest')->name('suspend-chemist-request');

    Route::get('/approve-reject-application', 'ApplicationController@approveRejectApplication')->name('approve-reject-application');
    Route::post('/approve-application', 'ApplicationController@approveApplication')->name('approve-application');
    Route::post('/reject-application', 'ApplicationController@rejectApplication')->name('reject-application');

    Route::get('/approved-application', 'ApplicationController@approvedApplications')->name('approved-application');

    Route::get('/rejected-application', 'ApplicationController@rejectedApplication')->name('rejected-application');

    Route::get('/job-posts', 'JobPostController@jobPosts')->name('job-posts');

    Route::get('/chemist-reviews', 'ReviewController@chemistReviews')->name('chemist-reviews');
    Route::get('/chemist-average-reviews', 'ReviewController@chemistAverageReviews')->name('chemist-average-reviews');
    Route::get('/chemist-review-details', 'ReviewController@chemistReviewDetails')->name('chemist-review-details');
    Route::get('/drug-store-reviews', 'ReviewController@drugStoreReviews')->name('drug-store-reviews');
    Route::get('/drug-store-average-reviews', 'ReviewController@drugStoreAverageReviews')->name('drug-store-average-reviews');
    Route::get('/drug-store-review-details', 'ReviewController@drugStoreReviewDetails')->name('drug-store-review-details');

    Route::get('/job-post-applications/{job_id}', 'JobPostController@postDetails')->name('job-post-applications');

    Route::get('/drug-store-payment', 'PaymentController@drugStorePayment')->name('drug-store-payment');
    Route::post('/generate-all-drugstore-invoice', 'PaymentController@generateAllDrugstoreInvoice')->name('generate-all-drugstore-invoice');
    Route::get('/chemist-payment', 'PaymentController@chemistPayment')->name('chemist-payment');
    Route::post('/change-drug-store-paid-status', 'PaymentController@changeDrugStorePaidStatus')->name('change-drug-store-paid-status');

    // update working hours
    Route::post('/update-completed-working-hours', 'PaymentController@updateCompletedWorking')->name('update-completed-working-hours');

    Route::get('/pending-interviews', 'InterviewController@pendingInterviews')->name('pending-interviews');
    Route::post('/change-interview-status', 'InterviewController@changeInterviewStatus')->name('change-interview-status');

    Route::get('/approved-interviews', 'InterviewController@approvedInterviews')->name('approved-interviews');
    Route::get('/rejected-interviews', 'InterviewController@rejectedInterviews')->name('rejected-interviews');

    Route::post('/search-drug-store-payment', 'PaymentController@searchDrugStorePayment')->name('search-drug-store-payment');
    Route::post('/search-chemist-payment', 'PaymentController@searchChemistPayment')->name('search-chemist-payment');
    Route::post('/generate-drugstore-invoice', 'PaymentController@generateDrugstoreInvoice')->name('generate-drugstore-invoice');
    Route::post('/change-drugstore-invoice-status', 'PaymentController@changeDrugstoreInvoiceStatus')->name('change-drugstore-invoice-status');
    Route::post('/generate-chemist-invoice', 'PaymentController@generateChemistInvoice')->name('generate-chemist-invoice');

    Route::get('/database-backup', 'LoginController@databaseBackup')->name('database-backup');
    Route::get('/backup', 'LoginController@backup')->name('backup');

    Route::get('/pending-inspections', 'InterviewController@pendingInspections')->name('pending-inspections');

    /****************************** New routes for admin roles  ******************************/

    Route::get('/admin-accounts', 'RoleController@adminAccounts')->name('admin-accounts');
    Route::get('/view-admin-details/{user_id}', 'RoleController@viewAdmin')->name('view-admin-details');
    Route::post('/delete-user-account', 'RoleController@deleteUserAccount')->name('delete-user-account');
    Route::post('/update-user-account', 'RoleController@updateUserAccount')->name('update-user-account');
    Route::get('/account-change-password/{user_id}', 'RoleController@accountChangePassword')->name('account-change-password');
    Route::post('/account-reset-password', 'RoleController@accountResetPassword')->name('account-reset-password');
    Route::get('/account-access-restrictions/{user_id}', 'RoleController@accountAccessRestrictions')->name('account-access-restrictions');
    Route::get('/add-new-admin', 'RoleController@addNewAdmin')->name('add-new-admin');
    Route::post('/add-user-account', 'RoleController@addUserAccount')->name('add-user-account');
    Route::post('/update-user-restriction', 'RoleController@updateUserRestriction')->name('update-user-restriction');
    Route::get('/view-admin-actions', 'RoleController@viewAdminActions')->name('view-admin-actions');

    /****************************************************************************************/
    Route::get('/logout', 'LoginController@logout')->name('logout');


    Route::get('/intro', 'HomeController@introServices')->name('intro');
    Route::get('/intro/{drug_store_id}', 'HomeController@introServicesPdf')->name('intro-pdf');
    Route::get('intro-test/{drug_store_id}', 'HomeController@introServicePdfDownload');


    Route::post('/delete-intro', 'HomeController@deleteIntro')->name('delete-intro');
});

Route::get('/drug-store', function () {
    return view('store.login');
});

Route::group([
    'prefix' => 'drug-store',
], function () {
    Route::get('/drug-store-login', 'Store\HomeController@index')->name('drug-store-login');
    Route::get('/drug-store-signup', 'Store\HomeController@signUp')->name('drug-store-signup');
    Route::get('/drug-store-forgot-password', 'Store\HomeController@forgotPassword')->name('drug-store-forgot-password');
    Route::post('/reset-password-link-send', 'Store\HomeController@resetPasswordLinkSend')->name('reset-password-link-send');
    Route::get('/password-reset/{parameter}', 'Store\HomeController@passwordReset');
    Route::post('/password-reset/password-reset-action', 'Store\HomeController@passwordResetAction')->name('password-reset-action');
    Route::get('/otp-verification/{parameter}', 'Store\HomeController@otpVerification');
    Route::post('/otp-verification-action', 'Store\HomeController@otpVerificationAction')->name('otp-verification-action');
    Route::get('/drug-store-resend-otp/{parameter}', 'Store\HomeController@otpResend')->name('drug-store-resend-otp');
    Route::post('/store-sign-up-action', 'Store\HomeController@signUpAction')->name('store-sign-up-action');
    Route::post('/store-login-action', 'Store\HomeController@loginAction')->name('store-login-action');


    Route::group(['middleware' => ['auth:stores', 'store.revalidate', 'CheckApproved', 'StoreNotification']], function () {

        Route::get('/store-terms', 'Store\HomeController@storeTerms')->name('store-terms');

        Route::get('/admin-chat', 'Store\HomeController@adminChat')->name('admin-chat');
        Route::post('/mark-read-admin', 'Store\HomeController@markReadAdmin')->name('mark-read-admin');

        Route::post('/admin-chat', 'Store\HomeController@saveChat')->name('save-chat');

        Route::get('/store-chat', 'Store\HomeController@chat')->name('store-chat');

        Route::get('/group-chat', 'Store\HomeController@groupChat')->name('group-chat');
        Route::post('/mark-read-group', 'Store\HomeController@markReadGroup')->name('mark-read-group');

        Route::get('/get-chemist-details', 'Store\HomeController@getChemistDetails')->name('get-chemist-details');
        Route::post('/save-group-chat', 'Store\HomeController@saveGroupChat')->name('save-group-chat');
        Route::get('/pick-a-chemist', 'Store\HomeController@pickAChemist')->name('pick-a-chemist');
        Route::get('/start-new-group-chat', 'Store\HomeController@startNewGroupChatAdmin')->name('start-new-group-chat');

        Route::get('/chemist-chat', 'Store\HomeController@chemistChat')->name('chemist-chat');
        Route::post('/mark-read-store-chemist', 'Store\HomeController@markReadStoreChemist')->name('mark-read-store-chemist');
        Route::post('/save-chemist-chat', 'Store\HomeController@saveChemistChat')->name('save-chemist-chat');
        Route::get('/start-new-chemist-chat', 'Store\HomeController@startNewChemistChat')->name('start-new-chemist-chat');

        Route::get('/store-dashboard', 'Store\HomeController@dashboard')->name('store-dashboard');
        Route::get('/store-home-dashboard', 'Store\HomeController@homeDashboard')->name('store-home-dashboard');
        Route::get('/store-home-dashboard#v-pills-job-applications-tab', 'Store\HomeController@homeDashboard')->name('store-home-dashboard-pending');
        Route::get('/store-home-dashboard#v-pills-cancelled-application-tab#{application_id}#{notification_id}', 'Store\HomeController@homeDashboard')->name('store-home-dashboard-cancel');
        Route::post('/mark-as-read-cancelled', 'Store\HomeController@markAsReadCancelled')->name('mark-as-read-cancelled');

        Route::get('/store-notification', 'Store\HomeController@notification')->name('store-notification');
        Route::post('/add-store-job-post', 'Store\HomeController@addJobPost')->name('add-store-job-post');
        Route::get('/job-post-details/{job_id}', 'Store\HomeController@postDetails')->name('job-post-details');
        Route::post('/update-store-job-post', 'Store\HomeController@updatePostDetails')->name('update-store-job-post');
        Route::get('/drug-store-reviewed', 'Store\HomeController@reviewed')->name('drug-store-reviewed');
        Route::get('/drug-store-review', 'Store\HomeController@review')->name('drug-store-review');
        Route::get('/pending-review', 'Store\HomeController@pendingReview')->name('pending-review');
        Route::post('/add-review', 'Store\HomeController@addReview')->name('add-review');

        // Route::get('/store-settings', 'Store\SettingController@index')->name('store-settings');
        // Route::post('/update-store-profile', 'Store\SettingController@updateStoreProfile')->name('update-store-profile');
        // Route::post('/update-store-profile2', 'Store\SettingController@updateStoreProfile2')->name('update-store-profile2');

        Route::post('/add-timing', 'Store\SettingController@addTiming')->name('add-timing');
        Route::post('/delete-timing', 'Store\SettingController@deleteTiming')->name('delete-timing');
        Route::post('/get-timings', 'Store\SettingController@getTimings')->name('get-timings');

        Route::get('/store-logout', 'Store\HomeController@logout')->name('drug-store-logout');

        Route::get('/store-terms', 'Store\HomeController@storeTerms')->name('store-terms');

        Route::get('/admin-chat', 'Store\HomeController@adminChat')->name('admin-chat');
        Route::post('/admin-chat', 'Store\HomeController@saveChat')->name('save-chat');

        Route::get('/store-chat', 'Store\HomeController@chat')->name('store-chat');
        Route::get('/group-chat', 'Store\HomeController@groupChat')->name('group-chat');
        Route::get('/get-chemist-details', 'Store\HomeController@getChemistDetails')->name('get-chemist-details');
        Route::post('/save-group-chat', 'Store\HomeController@saveGroupChat')->name('save-group-chat');
        Route::get('/pick-a-chemist', 'Store\HomeController@pickAChemist')->name('pick-a-chemist');
        Route::get('/start-new-group-chat', 'Store\HomeController@startNewGroupChatAdmin')->name('start-new-group-chat');

        Route::get('/chemist-chat', 'Store\HomeController@chemistChat')->name('chemist-chat');
        Route::post('/save-chemist-chat', 'Store\HomeController@saveChemistChat')->name('save-chemist-chat');
        Route::get('/start-new-chemist-chat', 'Store\HomeController@startNewChemistChat')->name('start-new-chemist-chat');


        Route::post('reject-chemist-application', 'Store\HomeController@rejectChemistApplication')->name('reject-chemist-application');

        Route::post('get-chemist-information', 'Store\HomeController@getChemistInformation')->name('get-chemist-information');

        Route::post('accept-job-application', 'Store\HomeController@acceptJobApplication')->name('accept-job-application');

        Route::post('cancel-job-post', 'Store\HomeController@cancelJobPost')->name('cancel-job-post');
        Route::get('drug-store-review/{postId}', 'Store\HomeController@drugStoreReview')->name('drug-store-review');

        Route::get('drug-store-review-all/{postId}', 'Store\HomeController@drugStoreReviewAll')->name('drug-store-review-all');

        Route::get('drug-store-accepted-application/{postId}/{applicationId}/{notificationId}', 'Store\HomeController@drugStoreAcceptedApplications')->name('drug-store-accepted-application');

        Route::post('cancel-job-application', 'Store\HomeController@cancelJobApplication')->name('cancel-job-application');

        Route::get('/store-settings', 'Store\SettingController@index')->name('store-settings');
        Route::post('/update-store-profile', 'Store\SettingController@updateStoreProfile')->name('update-store-profile');
        Route::post('/update-store-profile2', 'Store\SettingController@updateStoreProfile2')->name('update-store-profile2');

        Route::post('/add-timing', 'Store\SettingController@addTiming')->name('add-timing');
        Route::post('/delete-timing', 'Store\SettingController@deleteTiming')->name('delete-timing');
        Route::post('/get-timings', 'Store\SettingController@getTimings')->name('get-timings');

        Route::post('/cancel-all-applications', 'Store\SettingController@cancelAllApplications')->name('cancel-all-applications');

        Route::get('/download-invoice/{invoice_id}', 'Store\SettingController@downloadInvoice')->name('download-invoice');
        Route::get('/download-resume', 'Store\SettingController@downloadResume')->name('download-resume');

        Route::get('/pending-application-for-each-post/{postId}', 'Store\HomeController@pendingApplicationForEachPost')->name('pending-application-for-each-post');
        Route::post('/cancel-inspection', 'Store\SettingController@cancelInspection')->name('cancel-inspection');

        Route::get('/store-qr-code/{qr_code}', 'Store\SettingController@drugStoreQrCode')->name('store-qr-code');

        Route::get('/store-logout', 'Store\HomeController@logout')->name('drug-store-logout');

        //5th phase
        Route::get('/employee-intro-service', 'Store\SettingController@employeeIntroService')->name('employee-intro-service');
        Route::post('save-employee-intro-service', 'Store\SettingController@saveEmployeeIntroService')->name('save-employee-intro-service');
        Route::get('/employee-intro-service-pdf', 'Store\SettingController@employeeIntroServicePdf')->name('employee-intro-service-pdf');
        Route::get('/intro-update/{drug_store_id}', 'HomeController@introupdate');
        Route::get('/intro-inactive/{drug_store_id}', 'HomeController@introinactive');

        Route::post('/withdraw-employee-intro-service', 'Store\SettingController@withdrawEmployeeIntro')->name('withdraw-employee-intro-service');
        Route::post('/withdraw-normal-farmatch', 'Store\SettingController@withdrawNormalFarmatch')->name('withdraw-normal-farmatch');
        Route::post('/employee-intro-service-form', 'Store\SettingController@employeeIntroForm')->name('employee-intro-service-form');

    });
});


Route::get('/send-notification-incomplete-jobs', 'JobPostController@sendNotifications')->name('send-notification-incomplete-jobs');
Route::get('/mark-job-completed', 'JobPostController@markJobCompleted')->name('mark-job-completed');

Route::get('/send-push-notification-to-chemist', 'Store\HomeController@sendPushNotificationsToChemist')->name('send-push-notification-to-chemist');  // 5min crone
Route::get('/update-employee-intro-service', 'Store\HomeController@updateEmployeeIntroService')->name('update-employee-intro-service');  // daily crone


Route::get('test', 'Test@approvedByDS');
Route::get('testPush', 'Api\JobPostController@testPush');
Route::get('testMail', 'Store\HomeController@testMail');

Route::get('wagesImport', 'Test@wagesImport');
