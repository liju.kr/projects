<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtLockUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_lock_users', function (Blueprint $table) {
            $table->id();
            $table->string('store_name')->nullable();
            $table->string('tt_user_name')->nullable();
            $table->string('tt_password')->nullable();
            $table->string('tt_access_token')->nullable();
            $table->string('tt_refresh_token')->nullable();
            $table->string('tt_scope')->nullable();
            $table->bigInteger('tt_uid')->nullable();
            $table->bigInteger('tt_openid')->nullable();
            $table->bigInteger('expires_in')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_lock_users');
    }
}
