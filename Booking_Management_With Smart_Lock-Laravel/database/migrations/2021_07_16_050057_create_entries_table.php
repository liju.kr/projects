<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('user_id')->references('id')->on('users')->nullable();
            $table->bigInteger('room_id')->references('id')->on('rooms')->nullable();
            $table->bigInteger('service_id')->references('id')->on('services')->nullable();
            $table->string('name')->nullable();
            $table->string('entry_unique_id')->nullable();
            $table->string('lock_device_id')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_verified')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
