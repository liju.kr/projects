<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('store_id')->references('id')->on('stores')->nullable();
            $table->string('name')->nullable();
            $table->integer('min_capacity')->default(0);
            $table->integer('max_capacity')->default(0);
            $table->double('base_price_per_hour')->default(0);
            $table->double('default_slot_range_in_hour')->default(1);
            $table->double('area_in_square_feet')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
