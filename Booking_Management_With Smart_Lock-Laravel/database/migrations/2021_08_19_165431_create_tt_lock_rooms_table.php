<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtLockRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_lock_rooms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tt_lock_user_id')->references('id')->on('tt_lock_users')->nullable();
            $table->string('room_name')->nullable();
            $table->bigInteger('lock_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_lock_rooms');
    }
}
