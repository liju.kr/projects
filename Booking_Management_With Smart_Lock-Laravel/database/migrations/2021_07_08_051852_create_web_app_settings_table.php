<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_app_settings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->references('id')->on('users')->nullable();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('country_id')->references('id')->on('countries')->nullable();
            $table->bigInteger('language_id')->references('id')->on('languages')->nullable();
            $table->bigInteger('currency_id')->references('id')->on('currencies')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_app_settings');
    }
}
