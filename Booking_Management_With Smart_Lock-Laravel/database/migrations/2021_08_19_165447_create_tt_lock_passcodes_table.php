<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtLockPasscodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_lock_passcodes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tt_lock_room_id')->references('id')->on('tt_lock_rooms')->nullable();
            $table->string('passcode_name')->nullable();
            $table->bigInteger('passcode')->nullable();
            $table->bigInteger('passcode_id')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('type')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_lock_passcodes');
    }
}
