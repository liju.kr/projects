<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoginSessionTokenToSkewUserTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('skew_user_tokens', function (Blueprint $table) {
            $table->string('login_session_token')->nullable()->after('skew_client_secret');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skew_user_tokens', function (Blueprint $table) {
            $table->dropColumn('login_session_token');
        });
    }
}
