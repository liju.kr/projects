<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSecretKeyToStoreDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_details', function (Blueprint $table) {
            $table->string('lock_api_key')->after('website_url')->nullable();
            $table->double('buffer_time_before')->after('lock_api_key')->default(0);
            $table->double('buffer_time_after')->after('buffer_time_before')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_details', function (Blueprint $table) {
            $table->dropColumn('lock_api_key', 'buffer_time_before', 'buffer_time_after');
        });
    }
}
