<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('location_id');
            $table->bigInteger('store_category_id')->references('id')->on('store_categories')->nullable()->after('user_id');
            $table->string('name_furigana')->nullable()->after('name');
            $table->string('slug')->nullable()->after('name_furigana');
            $table->bigInteger('country_id')->references('id')->on('countries')->nullable()->after('slug');
            $table->bigInteger('state_id')->references('id')->on('states')->nullable()->after('country_id');
            $table->bigInteger('city_id')->references('id')->on('cities')->nullable()->after('state_id');
            $table->string('address_line_1')->nullable()->after('city_id');
            $table->string('address_line_2')->nullable()->after('address_line_1');
            $table->string('zip_code')->nullable()->after('address_line_2');
            $table->double('longitude')->nullable()->after('zip_code');
            $table->double('latitude')->nullable()->after('longitude');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->bigInteger('location_id')->references('id')->on('locations')->nullable();
            $table->dropColumn('store_category_id');
            $table->dropColumn('name_furigana');
            $table->dropColumn('slug');
            $table->dropColumn('country_id');
            $table->dropColumn('state_id');
            $table->dropColumn('city_id');
            $table->dropColumn('address_line_1');
            $table->dropColumn('address_line_2');
            $table->dropColumn('zip_code');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
    }
}
