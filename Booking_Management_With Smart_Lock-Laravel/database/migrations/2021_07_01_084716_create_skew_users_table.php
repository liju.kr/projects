<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkewUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skew_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->references('id')->on('users')->nullable();
            $table->string('first_name')->nullable();
            $table->string('first_name_furigana')->nullable();
            $table->string('last_name')->nullable();
            $table->string('last_name_furigana')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_number')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('is_store_manager')->default(false);
            $table->boolean('is_staff_member')->default(false);
            $table->boolean('is_skew_member')->default(false);
            $table->boolean('is_any_store_member')->default(false);
            $table->boolean('is_drop_in_member')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('is_verified')->default(false);
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skew_users');
    }
}
