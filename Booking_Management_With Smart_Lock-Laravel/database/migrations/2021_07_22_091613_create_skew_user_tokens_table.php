<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkewUserTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skew_user_tokens', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->string('skew_client_id')->nullable();
            $table->string('skew_client_secret')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skew_user_tokens');
    }
}
