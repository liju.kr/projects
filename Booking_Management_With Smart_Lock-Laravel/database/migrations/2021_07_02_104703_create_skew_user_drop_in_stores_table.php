<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkewUserDropInStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skew_user_drop_in_stores', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('store_id')->references('id')->on('stores')->nullable();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('assigned_by_manager_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('assigned_by_admin_id')->references('id')->on('users')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skew_user_drop_in_stores');
    }
}
