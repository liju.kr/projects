<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStoreDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_details', function (Blueprint $table) {
            $table->longText('description')->nullable()->after('store_id');
            $table->string('primary_contact_number')->nullable()->after('description');
            $table->string('secondary_contact_number')->nullable()->after('primary_contact_number');
            $table->string('primary_email')->nullable()->after('secondary_contact_number');
            $table->string('secondary_email')->nullable()->after('primary_email');
            $table->string('website_url')->nullable()->after('secondary_email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_details', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('primary_contact_number');
            $table->dropColumn('secondary_contact_number');
            $table->dropColumn('primary_email');
            $table->dropColumn('secondary_email');
            $table->dropColumn('website_url');
        });
    }
}
