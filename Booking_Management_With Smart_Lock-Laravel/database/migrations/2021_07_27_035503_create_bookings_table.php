<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('reference_id')->nullable();
            $table->bigInteger('skew_user_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('skew_manager_id')->references('id')->on('skew_users')->nullable();
            $table->bigInteger('user_id')->references('id')->on('users')->nullable();
            $table->bigInteger('room_id')->references('id')->on('rooms')->nullable();
            $table->dateTime('booking_from', $precision = 0)->nullable();
            $table->dateTime('booking_to', $precision = 0)->nullable();
            $table->text('remarks')->nullable();
            $table->text('qr_code')->nullable();
            $table->dateTime('cancelled_at', $precision = 0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
