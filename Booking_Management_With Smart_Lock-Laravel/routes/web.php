<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Import\ImportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('locales', 'App\Http\Controllers\Settings\LocaleController');
    Route::resource('entries', 'App\Http\Controllers\Settings\VerificationController');

    Route::get('/import/manager', [ImportController::class, 'importManager'])->name('home');
    Route::get('/import/member', [ImportController::class, 'importMember'])->name('home');
    Route::get('/import/reservations', [ImportController::class, 'importReservations'])->name('home');

});

