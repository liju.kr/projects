<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\SkewUserLoginController;
use App\Http\Controllers\SkewUserHomeController;
use App\Http\Controllers\StoreApi\SkewManagerLoginController;
use App\Http\Controllers\StoreApi\SkewScannerLoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//prefix : auth

Route::middleware(['throttle'])->group(function () {
    Route::post('/store_manager_login', [SkewManagerLoginController::class, 'login'])->name('auth.store_manager_login');
    Route::post('/store_manager_login_with_token', [SkewManagerLoginController::class, 'loginWithToken'])->name('auth.store_manager_login_with_token');
    Route::post('/scanner_app_login', [SkewScannerLoginController::class, 'login'])->name('auth.scanner_app_login');
});
