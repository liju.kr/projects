<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Settings\SettingController;
use App\Http\Controllers\StoreApi\StoreCategoryController;
use App\Http\Controllers\StoreApi\StoreController;
use App\Http\Controllers\StoreApi\RoomController;
use App\Http\Controllers\StoreApi\AmenityController;
use App\Http\Controllers\StoreApi\EntryController;
use App\Http\Controllers\StoreApi\ServiceController;
use App\Http\Controllers\StoreApi\MemberController;
use App\Http\Controllers\StoreApi\SkewScannerLoginController;
use App\Http\Controllers\StoreApi\DashboardController;
use App\Http\Controllers\StoreApi\BookingController;
use App\Http\Controllers\StoreApi\ManagerController;
use App\Http\Controllers\StoreApi\AdminActionController;
use App\Http\Controllers\StoreApi\ContentController;
use App\Http\Controllers\ExternalApi\StoreDataController;
use App\Http\Controllers\ExternalApi\UpdateDataController;
use App\Http\Controllers\ExternalApi\DeleteDataController;
use App\Http\Controllers\ExternalApi\QrCodeController;
use App\Http\Controllers\ExternalApi\ExternalSkewLoginController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// middleware web
//Route::get('/login', [SkewUserLoginController::class, 'showLoginForm'])->name('skew_user.login');
//Route::post('/login', [SkewUserLoginController::class, 'login'])->name('skew_user.login.post');
//Route::post('/logout', [SkewUserLoginController::class, 'logout'])->name('skew_user.logout');
////Admin Home page after login
//Route::group(['middleware'=>'skew_user'], function() {
//    Route::get('/home', [SkewUserHomeController::class, 'index']);
//});

// Admin Panel for Store Managers Angular Js


Route::middleware('auth:skew_user')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'api'], function () {
    Route::post('/store', [StoreDataController::class, 'externalApiStoreData'])->name('skew_user.api.store');
    Route::post('/update', [UpdateDataController::class, 'externalApiUpdateData'])->name('skew_user.api.update');
    Route::post('/delete', [DeleteDataController::class, 'externalApiDeleteData'])->name('skew_user.api.delete');
    Route::post('/cancel', [DeleteDataController::class, 'externalApiCancelData'])->name('skew_user.api.cancel');
    Route::post('/qr_code', [QrCodeController::class, 'getQrCode'])->name('skew_user.api.qr_code');
    Route::post('/login_to_skew_panel', [ExternalSkewLoginController::class, 'loginToSkewPanel'])->name('skew_user.api.login_to_skew_panel');
});

Route::middleware(['auth:skew_user'])->group(function () {

    //Sanner App
    Route::post('/scan_qr_code', [SkewScannerLoginController::class, 'scanQrCode'])->name('skew_user.scan_qr_code');


    //dashboard
    Route::get('/dashboard_store_member_chart', [DashboardController::class, 'storeMemberChart'])->name('skew_user.dashboard_store_member_chart');
    Route::get('/dashboard_usage_chart', [DashboardController::class, 'usageChart'])->name('skew_user.dashboard_usage_chart');
    Route::get('/dashboard_today_usage', [DashboardController::class, 'todayUsage'])->name('skew_user.dashboard_today_usage');

    Route::get('/member_usage_history', [MemberController::class, 'memberUsageHistory'])->name('skew_user.member_usage_history');
    Route::get('/usage_history', [BookingController::class, 'UsageHistory'])->name('skew_user.usage_history');

    Route::get('/get_manager_profile', [ManagerController::class, 'getManagerProfile'])->name('skew_user.get_manager_profile');
    Route::post('/change_manager_profile_pic', [ManagerController::class, 'changeManagerProfilePic'])->name('skew_user.change_manager_profile_pic');
    Route::post('/delete_manager_profile_pic', [ManagerController::class, 'deleteManagerProfilePic'])->name('skew_user.delete_manager_profile_pic');
    Route::get('/get_manager_detail', [ManagerController::class, 'getManagerDetail'])->name('skew_user.get_manager_detail');

    Route::get('/get_admin_actions', [AdminActionController::class, 'index'])->name('skew_user.get_admin_actions');

    Route::get('/get_status_codes', [ContentController::class, 'getStatusCodes'])->name('skew_user.get_status_codes');

    Route::get('/get_month_year', [DashboardController::class, 'getMonthYear'])->name('skew_user.get_month_year');




    Route::get('/get_countries', [SettingController::class, 'getCountries'])->name('skew_user.get_countries');
    Route::get('/get_states', [SettingController::class, 'getStates'])->name('skew_user.get_states');
    Route::get('/get_cities', [SettingController::class, 'getCities'])->name('skew_user.get_cities');

    Route::get('/get_store_categories', [StoreCategoryController::class, 'getStoreCategories'])->name('skew_user.get_store_categories');
    Route::get('/get_amenities', [AmenityController::class, 'getAmenities'])->name('skew_user.get_amenities');
    Route::get('/get_service_categories', [ServiceController::class, 'getServiceCategories'])->name('skew_user.get_service_categories');
    Route::get('/get_services', [ServiceController::class, 'getServices'])->name('skew_user.get_services');

    Route::get('/get_store_lists', [StoreController::class, 'getStoreLists'])->name('skew_user.get_store_lists');
    Route::get('/stores', [StoreController::class, 'index'])->name('skew_user.stores.index');
    Route::post('/stores/store', [StoreController::class, 'store'])->name('skew_user.stores.store');
    Route::get('/stores/{id}', [StoreController::class, 'show'])->name('skew_user.stores.show');
    Route::get('/stores/{id}/edit', [StoreController::class, 'edit'])->name('skew_user.stores.edit');
    Route::post('/stores/{id}/update', [StoreController::class, 'update'])->name('skew_user.stores.update');
    Route::post('/stores/{id}/update_buffer_time', [StoreController::class, 'updateBufferTime'])->name('skew_user.stores.update_buffer_time');

    Route::get('/stores/{store_id}/get_room_lists', [RoomController::class, 'getRoomLists'])->name('skew_user.stores.rooms.get_room_lists');
    Route::get('/stores/{store_id}/rooms', [RoomController::class, 'index'])->name('skew_user.stores.rooms.index');
    Route::post('/stores/{store_id}/rooms/store', [RoomController::class, 'store'])->name('skew_user.stores.rooms.store');
    Route::get('/stores/{store_id}/rooms/{room_id}', [RoomController::class, 'show'])->name('skew_user.stores.rooms.show');
    Route::get('/stores/{store_id}/rooms/{room_id}/edit', [RoomController::class, 'edit'])->name('skew_user.stores.rooms.edit');
    Route::post('/stores/{store_id}/rooms/{room_id}/update', [RoomController::class, 'update'])->name('skew_user.stores.rooms.update');
    Route::post('/stores/{store_id}/rooms/{room_id}/update_lock', [RoomController::class, 'updateLock'])->name('skew_user.stores.rooms.update_lock');
    Route::post('/stores/{store_id}/rooms/{room_id}/unlock_room', [RoomController::class, 'unLockRoom'])->name('skew_user.stores.rooms.unlock_room');
    Route::post('/stores/{store_id}/rooms/{room_id}/lock_room', [RoomController::class, 'lockRoom'])->name('skew_user.stores.rooms.lock_room');
    Route::post('/stores/{store_id}/rooms/{room_id}/battery_status', [RoomController::class, 'getBatteryStatus'])->name('skew_user.stores.rooms.get_battery_status');

    Route::get('/stores/{store_id}/rooms/{room_id}/entries', [EntryController::class, 'index'])->name('skew_user.stores.rooms.entries.index');
    Route::post('/stores/{store_id}/rooms/{room_id}/entries/store', [EntryController::class, 'store'])->name('skew_user.stores.rooms.entries.store');
    Route::get('/stores/{store_id}/rooms/{room_id}/entries/{entry_id}', [EntryController::class, 'show'])->name('skew_user.stores.rooms.entries.show');
    Route::get('/stores/{store_id}/rooms/{room_id}/entries/{entry_id}/edit', [EntryController::class, 'edit'])->name('skew_user.stores.rooms.entries.edit');
    Route::post('/stores/{store_id}/rooms/{room_id}/entries/{entry_id}/update', [EntryController::class, 'update'])->name('skew_user.stores.rooms.entries.update');

    Route::get('/members', [MemberController::class, 'index'])->name('skew_user.members.index');

    Route::get('/managers', [ManagerController::class, 'index'])->name('skew_user.managers.index');




});