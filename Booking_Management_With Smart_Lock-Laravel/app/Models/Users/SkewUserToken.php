<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkewUserToken extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function skew_user()
    {
        return $this->belongsTo('App\Models\Users\SkewUser', 'skew_user_id');
    }
}
