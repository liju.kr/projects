<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;


class SkewUser extends Authenticatable
{
    use HasFactory, SoftDeletes, Notifiable, HasApiTokens;

    protected $table = 'skew_users';
    protected $dates = ['deleted_at'];
    protected $hidden = ['password',  'remember_token'];
    protected $appends = ['profile_image', 'role', 'created_on', 'updated_on', 'member_store_names', 'manager_store_names'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeVerified($query)
    {
        return $query->where('is_verified',true);
    }
    public function scopeStoreMember($query)
    {
        return $query->where('is_any_store_member',true);
    }
    public function scopeStoreManager($query)
    {
        return $query->where('is_store_manager',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('first_name','ASC');
    }
    public function skew_user_profile()
    {
        return $this->hasOne('App\Models\Users\SkewUserProfile', 'skew_user_id');
    }
    // Store Managers
    public function manager_assigned_by_manager()
    {
        return $this->belongsToMany('App\Models\Users\SkewUser', 'skew_user_store_managers', 'assigned_by_manager_id');
    }

    public function own_stores()
    {
        return $this->hasMany('App\Models\Stores\Store', 'skew_user_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Models\Bookings\Booking', 'skew_user_id');
    }

    public function manager_bookings()
    {
        return $this->hasMany('App\Models\Bookings\Booking', 'skew_manager_id');
    }

    public function assigned_managing_stores()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_store_managers', 'skew_user_id');
    }
    // Store Members
    public function member_assigned_by_manager()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_member_stores', 'assigned_by_manager_id');
    }

    public function assigned_member_stores()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_member_stores', 'skew_user_id');
    }
    // Store Dropins
    public function drop_in_assigned_by_manager()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_drop_in_stores', 'assigned_by_manager_id');
    }

    public function assigned_drop_in_stores()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_drop_in_stores', 'skew_user_id');
    }

    public function web_app_setting()
    {
        return $this->hasOne('App\Models\Settings\WebAppSetting', 'skew_user_id');
    }

    public function mob_app_setting()
    {
        return $this->hasOne('App\Models\Settings\MobAppSetting', 'skew_user_id');
    }

    public function skew_user_token()
    {
        return $this->hasOne('App\Models\Users\SkewUserToken', 'skew_user_id');
    }

    //Append Fields
    public function getProfileImageAttribute()
    {
        if(!$this->skew_user_profile->image)
        {
            return null;
        }
        else
        {
            return env('APP_URL').'/storage/uploads/manager/profile/'.$this->skew_user_profile->image;
        }



    }

    public function getRoleAttribute()
    {

        if($this->is_any_store_member) return "store_member";
        if($this->own_stores()->count())
        {
            return "store_owner";
        }
        else
        {
            return "store_manager";
        }

    }

    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }

    public function getMemberStoreNamesAttribute()
    {
       $store_names = $this->assigned_member_stores()->pluck('name');
       $names="";
        foreach($store_names as $store_name)
        {
            $names = $names.$store_name.", ";
        }
        return rtrim($names, ', ');
    }

    public function getManagerStoreNamesAttribute()
    {
        $store_names = $this->assigned_managing_stores()->pluck('name');
        $names="";
        foreach($store_names as $store_name)
        {
            $names = $names.$store_name.", ";
        }
        return rtrim($names, ', ');
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($skew_user) { // before delete() method call this
            if($skew_user->skew_user_profile)$skew_user->skew_user_profile->delete();
            if($skew_user->web_app_setting)$skew_user->web_app_setting->delete();
            if($skew_user->mob_app_setting) $skew_user->mob_app_setting->delete();
            if($skew_user->skew_user_token)$skew_user->skew_user_token->delete();
            foreach ($skew_user->bookings as $booking) $booking->delete();
            //foreach ($skew_user->manager_bookings as $manager_booking) $manager_booking->delete();
            foreach ($skew_user->own_stores as $own_store) $own_store->delete();
            DB::table('skew_user_store_managers')->where('skew_user_id', $skew_user->id)->orWhere('assigned_by_manager_id', $skew_user->id)->delete();
            DB::table('skew_user_member_stores')->where('skew_user_id', $skew_user->id)->orWhere('assigned_by_manager_id', $skew_user->id)->delete();
            DB::table('skew_user_drop_in_stores')->where('skew_user_id', $skew_user->id)->orWhere('assigned_by_manager_id', $skew_user->id)->delete();
        });
    }

}
