<?php

namespace App\Models\Test;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtLockUser extends Model
{
    use HasFactory;

    public function tt_lock_rooms()
    {
        return $this->hasMany('App\Models\Test\TtLockRoom', 'tt_lock_user_id');
    }
}
