<?php

namespace App\Models\Test;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtLockRoom extends Model
{
    use HasFactory;

    public function tt_lock_user()
    {
        return $this->belongsTo('App\Models\Test\TtLockUser', 'tt_lock_user_id');
    }
    public function tt_lock_passcodes()
    {
        return $this->hasMany('App\Models\Test\TtLockPasscode', 'tt_lock_room_id');
    }
}
