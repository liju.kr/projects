<?php

namespace App\Models\Test;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtLockPasscode extends Model
{
    use HasFactory;

    public function tt_lock_room()
    {
        return $this->belongsTo('App\Models\Test\TtLockRoom', 'tt_lock_room_id');
    }
}
