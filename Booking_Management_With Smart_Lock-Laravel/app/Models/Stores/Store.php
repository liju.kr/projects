<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['created_on', 'updated_on', 'approved_on', 'is_owner', 'buffer_time_before', 'buffer_time_after', 'manager_names', 'store_contact_number'];


    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeVerified($query)
    {
        return $query->where('is_verified',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\Settings\City', 'city_id');
    }
    public function state()
    {
        return $this->belongsTo('App\Models\Settings\State', 'state_id');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Settings\Country', 'country_id');
    }
    public function store_category()
    {
        return $this->belongsTo('App\Models\Stores\StoreCategory', 'store_category_id');
    }
    public function approved_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function store_owner()
    {
        return $this->belongsTo('App\Models\Users\SkewUser', 'skew_user_id');
    }
    public function store_detail()
    {
        return $this->hasOne('App\Models\Stores\StoreDetail', 'store_id');
    }
    public function rooms()
    {
        return $this->hasMany('App\Models\Stores\Room', 'store_id');
    }

    public function managers()
    {
        return $this->belongsToMany('App\Models\Users\SkewUser', 'skew_user_store_managers', 'store_id');
    }
    public function members()
    {
        return $this->belongsToMany('App\Models\Users\SkewUser', 'skew_user_member_stores', 'store_id');
    }
    public function drop_in_members()
    {
        return $this->belongsToMany('App\Models\Users\SkewUser', 'skew_user_drop_in_stores', 'store_id');
    }

    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getApprovedOnAttribute()
    {
        return "";
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }

    public function getBufferTimeBeforeAttribute()
    {
        return $this->store_detail->buffer_time_before;
    }

    public function getBufferTimeAfterAttribute()
    {
        return $this->store_detail->buffer_time_after;
    }

    public function getStoreContactNumberAttribute()
    {
        return $this->store_detail->primary_contact_number;
    }

    public function getManagerNamesAttribute()
    {
        $store_names = $this->managers()->pluck('first_name');
        $names="";
        foreach($store_names as $store_name)
        {
            $names = $names.$store_name.", ";
        }
        return rtrim($names, ', ');
    }

    public function getIsOwnerAttribute()
    {
        if(Auth::guard('skew_user')->check())
        {
            if(Auth::guard('skew_user')->id() == $this->skew_user_id) return true;
        }
        return false;
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($store) { // before delete() method call this
            $store->store_detail->delete();
            foreach ($store->rooms as $room) $room->delete();
            DB::table('skew_user_store_managers')->where('store_id', $store->id)->delete();
            DB::table('skew_user_member_stores')->where('store_id', $store->id)->delete();
            DB::table('skew_user_drop_in_stores')->where('store_id', $store->id)->delete();
        });
    }


}
