<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['created_on', 'updated_on'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeVerified($query)
    {
        return $query->where('is_verified',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function room()
    {
        return $this->belongsTo('App\Models\Stores\Room', 'room_id');
    }
    public function service()
    {
        return $this->belongsTo('App\Models\Settings\Service', 'service_id');
    }
    public function approved_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }
}
