<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreDetail extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates =['deleted_at'];

    public function store()
    {
        return $this->belongsTo('App\Models\Stores\Store', 'store_id');
    }
}
