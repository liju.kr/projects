<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];


    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function stores()
    {
        return $this->hasMany('App\Models\Stores\Store', 'location_id');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function admins()
    {
        return $this->belongsToMany('App\Models\User', 'user_locations', 'user_id');
    }
}
