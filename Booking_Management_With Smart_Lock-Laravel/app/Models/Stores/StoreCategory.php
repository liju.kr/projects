<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreCategory extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];


    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function stores()
    {
        return $this->hasMany('App\Models\Stores\Store', 'store_category_id');
    }
}
