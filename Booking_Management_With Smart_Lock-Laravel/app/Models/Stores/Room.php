<?php

namespace App\Models\Stores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Room extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['created_on', 'updated_on', 'entry_unique_id', 'lock_device_id', 'lock_secret_key'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeVerified($query)
    {
        return $query->where('is_verified',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function store()
    {
        return $this->belongsTo('App\Models\Stores\Store', 'store_id');
    }
    public function entries()
    {
        return $this->hasMany('App\Models\Stores\Entry', 'room_id');
    }
    public function bookings()
    {
        return $this->hasMany('App\Models\Bookings\Booking', 'room_id');
    }
    public function amenities()
    {
        return $this->belongsToMany('App\Models\Settings\Amenity', 'room_amenities', 'room_id')->withPivot('price_per_hour', 'member_price_status');
    }
    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }

    public function getEntryUniqueIdAttribute()
    {
        return $this->entries()->first()->entry_unique_id;
    }

    public function getLockDeviceIdAttribute()
    {
        return $this->entries()->first()->lock_device_id;
    }

    public function getLockSecretKeyAttribute()
    {
        return $this->entries()->first()->lock_secret_key;
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($room) { // before delete() method call this
        foreach($room->bookings as $booking) $booking->delete();
        foreach($room->entries as $entry) $entry->delete();
        DB::table('room_amenities')->where('room_id', $room->id)->delete();
        });
    }
}
