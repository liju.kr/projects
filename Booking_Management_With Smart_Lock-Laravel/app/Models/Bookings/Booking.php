<?php

namespace App\Models\Bookings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['created_on', 'updated_on', 'booking_from_time', 'booking_to_time', 'booking_from_date', 'booking_to_date', 'cancelled_time', 'cancelled_date', 'booking_status', 'first_entry_time', 'first_entry_date', 'booked_member_name', 'booked_room_name', 'booked_store_name'];

    public function scopeValid($query)
    {
        return $query->where('cancelled_at', NULL);
    }
    public function scopeOngoing($query)
    {
        return $query->valid()->where('booking_from', '>=', date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())))->where('booking_to', '<=', date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())));
    }
    public function scopeUpcoming($query)
    {
        return $query->valid()->where('booking_from', '>', date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())))->where('booking_to', '>', date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())));
    }
    public function scopeCompleted($query)
    {
        return $query->valid()->where('booking_to', '<', date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())));
    }
    public function scopeOrderByBookingFrom($query)
    {
        return $query->orderBy('booking_from','ASC');
    }

    public function scopeExpiredNotVisited($query)
    {
        return $query->valid()->completed()->where('booking_from', '>', '2021-10-07 16:00:00')->has('booking_histories', '=', 0);
    }

    public function scopeExpiredVisited($query)
    {
        return $query->valid()->completed()->where('booking_from', '>', '2021-10-07 16:00:00')->has('booking_histories', '>', 0);
    }

    public function scopeVisited($query)
    {
        return $query->valid()->has('booking_histories', '>', 0);
    }

    public function scopeOngoingVisited($query)
    {
        return $query->valid()->ongoing()->where('booking_from', '>', '2021-10-07 16:00:00')->has('booking_histories', '>', 0);
    }

    public function scopeOngoingNotVisited($query)
    {
        return $query->valid()->ongoing()->where('booking_from', '>', '2021-10-07 16:00:00')->has('booking_histories', '=', 0);
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Stores\Room', 'room_id');
    }
    public function approved_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function booked_by()
    {
        return $this->belongsTo('App\Models\Users\SkewUser', 'skew_user_id');
    }

    public function booking_histories()
    {
        return $this->hasMany('App\Models\Bookings\BookingHistory', 'booking_id');
    }

    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }

    public function getBookingFromTimeAttribute()
    {
        if($this->booking_from)
        {
            return date('H:i', strtotime($this->booking_from));
        }
        else
        {
            return null;
        }

    }

    public function getBookingFromDateAttribute()
    {
        if($this->booking_from)
        {
            return date('Y-m-d', strtotime($this->booking_from));
        }
        else
        {
            return null;
        }

    }

    public function getBookingToTimeAttribute()
    {
        if($this->booking_to)
        {
            return date('H:i', strtotime($this->booking_to));
        }
        else
        {
            return null;
        }

    }



    public function getBookingToDateAttribute()
    {
        if($this->booking_to)
        {
            return date('Y-m-d', strtotime($this->booking_to));
        }
        else
        {
            return null;
        }

    }

    public function getCancelledDateAttribute()
    {
        if($this->cancelled_at)
        {
            return date('Y-m-d', strtotime($this->cancelled_at));
        }
        else
        {
            return null;
        }

    }
    public function getCancelledTimeAttribute()
    {
        if($this->cancelled_at)
        {
            return date('H:i', strtotime($this->cancelled_at));
        }
        else
        {
            return null;
        }

    }


    public function getBookingStatusAttribute()
    {
        if($this->cancelled_at) return "cancelled";
        $current_date = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        if(strtotime($this->booking_from) <= strtotime($current_date) && strtotime($this->booking_to) >= strtotime($current_date))
        {
            if($this->booking_histories()->count()) return "ongoing-visited"; else return "ongoing-not_visited";
        }
        if(strtotime($this->booking_from) >= strtotime($current_date)) return "upcoming";
        if(strtotime($this->booking_to) <= strtotime($current_date))
        {
            if($this->booking_histories()->count()) return "expired-visited"; else return "expired-not_visited";
        }

    }

    public function getFirstEntryDateAttribute()
    {
            if($this->booking_histories()->count())
            {
                $history = $this->booking_histories()->orderBy('id', 'ASC')->first();
                return date('Y-m-d', strtotime($history->entered_time));
            }
            else
            {
                return null;
            }

    }

    public function getFirstEntryTimeAttribute()
    {
        if($this->booking_histories()->count())
        {
            $history = $this->booking_histories()->orderBy('id', 'ASC')->first();
            return date('H:i', strtotime($history->entered_time));
        }
        else
        {
            return null;
        }

    }

    public function getBookedMemberNameAttribute()
    {
      return $this->booked_by->first_name;
    }

    public function getBookedRoomNameAttribute()
    {
        return $this->room->name;
    }

    public function getBookedStoreNameAttribute()
    {
       return $this->room->store->name;
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($booking) { // before delete() method call this
          foreach ($booking->booking_histories as $booking_history) $booking_history->delete();
        });
    }

}
