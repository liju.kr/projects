<?php

namespace App\Models\Bookings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingHistory extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['created_on', 'updated_on', 'entered_time', 'entered_date'];

    public function scopeOngoing($query)
    {
        return $query;
    }
    public function scopeUpcoming($query)
    {
        return $query;
    }
    public function scopeCompleted($query)
    {
        return $query;
    }


    public function booking()
    {
        return $this->belongsTo('App\Models\Bookings\Booking', 'booking_id');
    }

    public function room()
    {
        return $this->booking->room;
    }

    public function store()
    {
        return $this->booking->room->store;
    }

    public function entered_by()
    {
        return $this->belongsTo('App\Models\Users\SkewUser', 'skew_user_id');
    }


    public function getCreatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->created_at));
    }
    public function getUpdatedOnAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->updated_at));
    }

    public function getEnteredTimeAttribute()
    {
        if($this->entered_at)
        {
            return date('H:i', strtotime($this->entered_at));
        }
        else
        {
            return null;
        }

    }

    public function getEnteredDateAttribute()
    {
        if($this->entered_at)
        {
            return date('Y-m-d', strtotime($this->entered_at));
        }
        else
        {
            return null;
        }

    }
}
