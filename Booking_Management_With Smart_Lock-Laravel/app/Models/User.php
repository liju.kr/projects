<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }

    public function store_categories()
    {
        return $this->hasMany('App\Models\Stores\StoreCategory', 'user_id');
    }

    public function approved_stores()
    {
        return $this->hasMany('App\Models\Stores\Store', 'user_id');
    }

    public function cities()
    {
        return $this->belongsToMany('App\Models\Settings\City', 'admin_cities', 'user_id');
    }

    // set a member as manager by admin
    public function assigned_managers()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_store_managers', 'assigned_by_admin_id');
    }

    // set a member as store_member by admin
    public function assigned_members()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_member_stores', 'assigned_by_admin_id');
    }

    // set a member as store_drop_in_member by admin
    public function assigned_drop_in_members()
    {
        return $this->belongsToMany('App\Models\Stores\Store', 'skew_user_drop_in_stores', 'assigned_by_admin_id');
    }

    public function web_app_setting()
    {
        return $this->hasOne('App\Models\Settings\WebAppSetting', 'user_id');
    }

    public function mob_app_setting()
    {
        return $this->hasOne('App\Models\Settings\MobAppSetting', 'user_id');
    }

}
