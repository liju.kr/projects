<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Amenity extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function rooms()
    {
        return $this->belongsToMany('App\Models\Stores\Room', 'room_amenities', 'amenity_id')->withPivot('price_per_hour', 'member_price_status');
    }
}
