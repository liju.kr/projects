<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function web_app_settings()
    {
        return $this->hasMany('App\Models\Settings\WebAppSetting', 'currency_id');
    }
    public function mob_app_settings()
    {
        return $this->hasMany('App\Models\Settings\MobAppSetting', 'currency_id');
    }

}
