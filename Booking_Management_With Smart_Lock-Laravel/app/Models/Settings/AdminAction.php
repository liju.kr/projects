<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminAction extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['manager_name', 'manager_role', 'manager_email', 'action_date', 'action_time', 'store_name', 'store_contact_number'];

    public function action_by()
    {
        return $this->belongsTo('App\Models\Users\SkewUser', 'skew_user_id');
    }
    public function store()
    {
        return $this->belongsTo('App\Models\Stores\Store', 'store_id');
    }

    public function getActionDateAttribute()
    {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function getActionTimeAttribute()
    {
        return date('H:i', strtotime($this->created_at));
    }

    public function getStoreNameAttribute()
    {
        return $this->store->name;
    }

    public function getStoreContactNumberAttribute()
    {
        return $this->store->store_detail->primary_contact_number;
    }

    public function getManagerNameAttribute()
    {
        return $this->action_by->first_name;
    }
    public function getManagerEmailAttribute()
    {
        return $this->action_by->email;
    }
    public function getManagerRoleAttribute()
    {
        return $this->action_by->role;
    }
}
