<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use HasFactory,SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function cities()
    {
        return $this->hasMany('App\Models\Settings\City', 'state_id');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Settings\Country', 'country_id');
    }
}
