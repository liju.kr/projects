<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function states()
    {
        return $this->hasMany('App\Models\Settings\State', 'country_id');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function web_app_settings()
    {
        return $this->hasMany('App\Models\Settings\WebAppSetting', 'country_id');
    }
    public function mob_app_settings()
    {
        return $this->hasMany('App\Models\Settings\MobAppSetting', 'country_id');
    }
}
