<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locale extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByKey($query)
    {
        return $query->orderBy('key','ASC');
    }
}
