<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function scopeOrderByName($query)
    {
        return $query->orderBy('name','ASC');
    }
    public function add_by()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function state()
    {
        return $this->belongsTo('App\Models\Settings\State', 'state_id');
    }

    public function admins()
    {
        return $this->belongsToMany('App\Models\User', 'admin_cities', 'user_id');
    }
}
