<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebAppSetting extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
    public function admin()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function skew_user()
    {
        return $this->belongsTo('App\Models\Users\SkewUsers', 'skew_user_id');
    }
    public function language()
    {
        return $this->belongsTo('App\Models\Settings\Language', 'language_id');
    }
    public function currency()
    {
        return $this->belongsTo('App\Models\Settings\Currency', 'currency_id');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Settings\Country', 'country_id');
    }
}
