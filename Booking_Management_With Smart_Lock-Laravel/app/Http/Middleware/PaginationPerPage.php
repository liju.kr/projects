<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PaginationPerPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $limit = $request->input('limit');
        if (empty($limit) || ($limit > 100)) {
            $request['limit'] = 100;
        }
    }
}
