<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RedirectIfNotSkewUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard="skew_user")
    {
        if(!auth()->guard($guard)->check()) {
            return redirect(route('skew_user.login'));
        }
        return $next($request);
    }
}
