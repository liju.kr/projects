<?php

namespace App\Http\Controllers\ExternalApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Settings\AdminAction;
use App\Models\Stores\Room;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeleteDataController extends Controller
{
    use HelperTrait;
    public function externalApiDeleteData(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $model = $request->model;
        $reference_id = $request->reference_id;
        $manager_auth_id = $request->manager_auth_id;
        if($manager_auth_id) $skew_user_admin = SkewUser::where('reference_id', $manager_auth_id)->storeManager()->first();
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if($skew_client_check)
        {
            $skew_manager = $skew_client_check->skew_user;
            if($model=="App\Models\Shop")
            {
                $store = $skew_manager->assigned_managing_stores()
                    ->where('stores.reference_id', $reference_id)
                    ->orderByName()->first();
                if(!$store || $store->skew_user_id != $skew_manager->id)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }
                $deleted_id = $store->id;
                $store->delete();

                if($manager_auth_id && $skew_user_admin)
                {
                    $admin_action = new AdminAction();
                    $admin_action->skew_user_id = $skew_user_admin->id;
                    $admin_action->source_id =$deleted_id;
                    $admin_action->model_class ="App\Models\Stores\Store";
                    $admin_action->action_type ="destroy";
                    $admin_action->action =$this->getMessageText('store_model')." ".$this->getMessageText('delete_success_message');
                    $admin_action->store_id = $deleted_id;
                    $admin_action->save();
                }

                return response()->json([
                    'success' => true,
                ]);
            }
            elseif($model=="App\Models\Reservation")
            {
                $booking = Booking::where('reference_id', $reference_id)->first();
                if(!$booking)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }
                $store_id = $booking->room->store_id;
                $deleted_id = $booking->id;
                $booking->delete();

                if($manager_auth_id && $skew_user_admin)
                {
                    $admin_action = new AdminAction();
                    $admin_action->skew_user_id = $skew_user_admin->id;
                    $admin_action->source_id =$deleted_id;
                    $admin_action->model_class ="App\Models\Bookings\Booking";
                    $admin_action->action_type ="destroy";
                    $admin_action->action =$this->getMessageText('booking_model')." ".$this->getMessageText('delete_success_message');
                    $admin_action->store_id =$store_id;
                    $admin_action->save();
                }

                return response()->json([
                        'success' => true,
                    ]);
            }
            elseif ($model== "App\Models\Booth")
            {
                $room = Room::where('reference_id', $reference_id)->first();
                if(!$room)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }
                $deleted_id = $room->id;
                $store_id = $room->store_id;
                $room->delete();
                if($manager_auth_id && $skew_user_admin)
                {
                    $admin_action = new AdminAction();
                    $admin_action->skew_user_id = $skew_user_admin->id;
                    $admin_action->source_id =$deleted_id;
                    $admin_action->model_class ="App\Models\Stores\Room";
                    $admin_action->action_type ="destroy";
                    $admin_action->action =$this->getMessageText('room_model')." ".$this->getMessageText('delete_success_message');
                    $admin_action->store_id =$store_id;
                    $admin_action->save();
                }
                return response()->json([
                    'success' => true,
                ]);

            }
            elseif ($model== "App\Models\Member")
            {
                $skew_user= SkewUser::where('reference_id', $reference_id)->storeMember()->first();
                if(!$skew_user)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }
                $deleted_id = $skew_user->id;
                $store_id= $skew_user->assigned_member_stores()->first()->id;
                $skew_user->delete();
                if($manager_auth_id && $skew_user_admin)
                {
                    $admin_action = new AdminAction();
                    $admin_action->skew_user_id = $skew_user_admin->id;
                    $admin_action->source_id =$deleted_id;
                    $admin_action->model_class ="App\Models\Users\SkewUser";
                    $admin_action->action_type ="destroy";
                    $admin_action->action =$this->getMessageText('member_model')." ".$this->getMessageText('delete_success_message');
                    $admin_action->store_id =$store_id;
                    $admin_action->save();
                }
                return response()->json([
                    'success' => true,
                ]);
            }
            elseif ($model== "App\Models\Manager")
            {
                $skew_user= SkewUser::where('reference_id', $reference_id)->storeManager()->first();
                if(!$skew_user)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }
                $deleted_id = $skew_user->id;
                $stores = $skew_user->assigned_member_stores;
                $skew_user->delete();

                if($manager_auth_id && $skew_user_admin)
                {
                    foreach ($stores as $store)
                    {
                        $admin_action = new AdminAction();
                        $admin_action->skew_user_id = $skew_user_admin->id;
                        $admin_action->source_id =$deleted_id;
                        $admin_action->model_class ="App\Models\Users\SkewUser";
                        $admin_action->action_type ="destroy";
                        $admin_action->action =$this->getMessageText('manager_model')." ".$this->getMessageText('delete_success_message');
                        $admin_action->store_id =$store->id;
                        $admin_action->save();
                    }

                }

                return response()->json([
                    'success' => true,
                ]);
            }
            else
            {
                return response()->json([
                    'success' => true,
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }
    }


    public function externalApiCancelData(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $model = $request->model;
        $reference_id = $request->reference_id;
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if($skew_client_check)
        {
            $skew_manager = $skew_client_check->skew_user;
            if($model=="App\Models\Reservation")
            {
                $booking = Booking::where('reference_id', $reference_id)->first();

               if(!$booking)
                {
                    return response()->json([
                        'success' => false,
                    ]);
                }

               $cancel_date = date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now()));

               $booking->cancelled_at = $cancel_date;
               $booking->save();

                   if($booking)
                   {
                       return response()->json([
                           'success' => true,
                       ]);
                   }
                   else
                   {
                       return response()->json([
                           'success' => false,
                       ]);
                   }

            }
            else
            {
                return response()->json([
                    'success' => true,
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }
    }
}
