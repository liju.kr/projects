<?php

namespace App\Http\Controllers\ExternalApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Settings\AdminAction;
use App\Models\Stores\Room;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UpdateDataController extends Controller
{
    use HelperTrait;
    public function externalApiUpdateData(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $model = $request->model;
        $reference_id = $request->reference_id;
        $store_reference_id = $request->store_reference_id;
        $manager_auth_id = $request->manager_auth_id;
        if($manager_auth_id) $skew_user_admin = SkewUser::where('reference_id', $manager_auth_id)->storeManager()->first();
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if($skew_client_check)
        {
             $skew_manager = $skew_client_check->skew_user;
                    if($model=="App\Models\Shop")
                    {


                        $store = $skew_manager->assigned_managing_stores()
                                              ->where('stores.reference_id', $reference_id)
                                              ->orderByName()->first();

                        if(!$store || $store->skew_user_id != $skew_manager->id)
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                        if(isset($request->data['name']) && $request->data['name']) $store->name = $request->data['name'];
                        if(isset($request->data['name']) && $request->data['name']) $store->slug = $this->createSlug($request->data['name'])."-".$reference_id;
                        if(isset($request->data['address']) && $request->data['address']) $store->address_line_1 = $request->data['address'];
                        if(isset($request->data['status']) && $request->data['status'])  $store->status = $request->data['status'];
                        $store->save();

                        if($store)
                        {
                            $store_detail = $store->store_detail;
                            $store_detail->store_id = $store->id;
                            if(isset($request->data['tel']) && $request->data['tel']) $store_detail->primary_contact_number = $request->data['tel'];
                            $store_detail->save();

                            if($store_detail)
                            {
                                if($manager_auth_id && $skew_user_admin)
                                {
                                    $admin_action = new AdminAction();
                                    $admin_action->skew_user_id = $skew_user_admin->id;
                                    $admin_action->source_id =$store->id;
                                    $admin_action->model_class ="App\Models\Stores\Store";
                                    $admin_action->action_type ="update";
                                    $admin_action->action =$this->getMessageText('store_model')." ".$this->getMessageText('edit_success_message');
                                    $admin_action->store_id =$store->id;
                                    $admin_action->save();
                                }


                                return response()->json([
                                    'success' => true,
                                ]);
                            }
                            else
                            {
                                return response()->json([
                                    'success' => false,
                                ]);
                            }

                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                    }
                    elseif ($model== "App\Models\Member")
                    {
                        $store = $skew_manager->assigned_managing_stores()
                            ->where('stores.reference_id', $store_reference_id)
                            ->orderByName()->first();
                        $skew_user = SkewUser::where('reference_id', $reference_id)->storeMember()->first();
                          $user_store_check = DB::table('skew_user_member_stores')->where('skew_user_id', $skew_user->id)->where('store_id', $store->id)->count();
                        if(!$store || !$skew_user || !$user_store_check)
                        {

                            return response()->json([
                                'success' => false,
                            ]);
                        }

                        if(isset($request->data['name']) && $request->data['name'])$skew_user->first_name = $request->data['name'];
                        if(isset($request->data['email']) && $request->data['email'])$skew_user->email = $request->data['email'];
                        if(isset($request->data['kana']) && $request->data['kana']) $skew_user->first_name_furigana = $request->data['kana'];
                        if(isset($request->data['status']) && $request->data['status'])$skew_user->status = $request->data['status'];
                        $skew_user->save();

                        if($skew_user)
                        {
                            if($manager_auth_id && $skew_user_admin)
                            {
                                $admin_action = new AdminAction();
                                $admin_action->skew_user_id = $skew_user_admin->id;
                                $admin_action->source_id =$skew_user->id;
                                $admin_action->model_class ="App\Models\Users\SkewUser";
                                $admin_action->action_type ="update";
                                $admin_action->action =$this->getMessageText('member_model')." ".$this->getMessageText('edit_success_message');
                                $admin_action->store_id =$store->id;
                                $admin_action->save();
                            }
                            return response()->json([
                                'success' => true,
                            ]);
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                    }
                    elseif ($model== "App\Models\Booth")
                    {
                        $store = $skew_manager->assigned_managing_stores()
                            ->where('stores.reference_id', $request->data['shop_id'])
                            ->orderByName()->first();
                        $room = $store->rooms()
                               ->where('rooms.reference_id', $reference_id)
                              ->orderByName()->first();

                        if (!$store || !$room || $store->skew_user_id != $skew_manager->id) {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                        if(isset($request->data['name']) && $request->data['name'])$room->name = $request->data['name'];
                        if(isset($request->data['status']) && $request->data['status'])$room->status = $request->data['status'];
                        $room->save();

                        if($room)
                        {
                            if($manager_auth_id && $skew_user_admin)
                            {
                                $admin_action = new AdminAction();
                                $admin_action->skew_user_id = $skew_user_admin->id;
                                $admin_action->source_id =$room->id;
                                $admin_action->model_class ="App\Models\Stores\Room";
                                $admin_action->action_type ="update";
                                $admin_action->action =$this->getMessageText('room_model')." ".$this->getMessageText('edit_success_message');
                                $admin_action->store_id =$store->id;
                                $admin_action->save();
                            }

                            return response()->json([
                                'success' => true,
                            ]);
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                    }
                    elseif ($model== "App\Models\Reservation")
                    {
                        $booking = Booking::where('reference_id', $reference_id)->first();

                        if (!$booking) {
                            return response()->json([
                                'success' => false,
                            ]);
                        }


                        if(isset($request->data['reserve_date']) && $request->data['reserve_date'])
                        {

                            $slot = $booking->room->default_slot_range_in_hour * 60*60;
                            $booking_from = date('Y-m-d H:i:s', strtotime($request->data['reserve_date'])); // Back to string
                            $booking_to = date('Y-m-d H:i:s', strtotime($request->data['reserve_date']) + $slot); // Add slot range hour

                            $booking->booking_from = $booking_from;
                            $booking->booking_to = $booking_to;
                        }

                        if((isset($request->data['reserve_day']) && $request->data['reserve_day']) && (isset($request->data['reserve_time']) && $request->data['reserve_time']))
                        {

                           $day = $request->data['reserve_day'];
                           $reserve_time_array = explode(":", $request->data['reserve_time']);
                           $booth_id = $reserve_time_array[0];
                           $time = $reserve_time_array[1].":00:00";
                           $new_date = $day." ".$time;

                           if($booth_id)
                           {
                               $booked_room = Room::where('reference_id', $booth_id)->first();
                               if(!$booked_room)
                               {
                                   return response()->json([
                                       'success' => false,
                                   ]);

                               }
                               $room_id = $booked_room->id;
                               $booking->room_id = $room_id;
                               $slot = $booked_room->default_slot_range_in_hour * 60*60;
                           }
                           else
                           {
                               $slot = $booking->room->default_slot_range_in_hour * 60*60;

                           }

                            $booking_from = date('Y-m-d H:i:s', strtotime($new_date)); // Back to string
                            $booking_to = date('Y-m-d H:i:s', strtotime($new_date) + $slot); // Add slot range hour

                            $booking->booking_from = $booking_from;
                            $booking->booking_to = $booking_to;
                        }

                        if(isset($request->data['booth_id']) && $request->data['booth_id'])
                        {
                            $booked_room = Room::where('reference_id', $request->data['booth_id'])->first();
                            if(!$booked_room)
                            {
                                return response()->json([
                                    'success' => false,
                                ]);

                            }
                            $room_id = $booked_room->id;
                            $booking->room_id = $room_id;
                            $booked_store_id = $booked_room->store_id;
                        }

                        if(isset($request->data['manager_id']) && $request->data['manager_id'])
                        {
                            $booked_manager = SkewUser::where('reference_id', $request->data['manager_id'])->storeManager()->first();
                            if(!$booked_manager)
                            {
                                return response()->json([
                                    'success' => false,
                                ]);

                            }
                            $booking->skew_manager_id =$booked_manager->id;
                        }


                        if(isset($request->data['member_id']) && $request->data['member_id'])
                        {
                            $booked_member = SkewUser::where('reference_id', $request->data['member_id'])->storeMember()->first();
                            if(!$booked_member)
                            {
                                return response()->json([
                                    'success' => false,
                                ]);

                            }
                            $skew_user_id = $booked_member->id;
                            $booking->skew_user_id =$skew_user_id;
                        }
                        else
                        {
                            $skew_user_id = $booked_manager->id;
                            $booking->skew_user_id =$skew_user_id;
                        }
                        if(isset($request->data['note']) && $request->data['note']) $booking->remarks = $request->data['note'];
                        $booking->save();

                        if($booking)
                        {
                            if($manager_auth_id && $skew_user_admin && isset($request->data['manager_id']) && $request->data['manager_id'])
                            {
                                $admin_action = new AdminAction();
                                $admin_action->skew_user_id = $skew_user_admin->id;
                                $admin_action->source_id =$booking->id;
                                $admin_action->model_class ="App\Models\Bookings\Booking";
                                $admin_action->action_type ="update";
                                $admin_action->action =$this->getMessageText('booking_model')." ".$this->getMessageText('edit_success_message');
                                $admin_action->store_id = $booked_store_id;
                                $admin_action->save();
                            }
                            return response()->json([
                                'success' => true,
                            ]);
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                    }
                    elseif ($model== "App\Models\Manager")
                    {

                        $skew_user = SkewUser::where('reference_id', $reference_id)->storeManager()->first();
                        if(!$skew_user)
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                        $stores = $skew_manager->own_stores()->get();
                        if(isset($request->data['account']) && $request->data['account'])$email = $request->data['account']."@skewplus.com";
                        if(isset($request->data['password']) && $request->data['password'])$password = bcrypt($request->data['password']);


                        if(isset($request->data['name']) && $request->data['name']) $skew_user->first_name = $request->data['name'];
                        if(isset($request->data['account']) && $request->data['account']) $skew_user->email = $email;
                        if(isset($request->data['password']) && $request->data['password']) $skew_user->password = $password;
                        if(isset($request->data['status']) && $request->data['status']) $skew_user->status = $request->data['status'];
                        $skew_user->is_store_manager = 1;
                        $skew_user->is_any_store_member = 1;
                        $skew_user->is_verified = 1;
                        $skew_user->save();

                        if($skew_user)
                        {
                            DB::table('skew_user_store_managers')->where('skew_user_id', $skew_user->id)->delete();
                            DB::table('skew_user_member_stores')->where('skew_user_id', $skew_user->id)->delete();
                            foreach ($stores as $store)
                            {
                                if($request->data['authorization'])
                                {
                                    DB::table('skew_user_store_managers')->insert([
                                        'store_id' => $store->id,
                                        'skew_user_id' => $skew_user->id,
                                        'assigned_by_manager_id' => $skew_manager->id,
                                        'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                    ]);

                                    DB::table('skew_user_member_stores')->insert([
                                        'store_id' => $store->id,
                                        'skew_user_id' => $skew_user->id,
                                        'assigned_by_manager_id' => $skew_manager->id,
                                        'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                    ]);
                                }
                                else
                                {
                                    if(isset($request->data['shop_id']) && $store->reference_id == $request->data['shop_id'])
                                    {
                                        DB::table('skew_user_store_managers')->insert([
                                            'store_id' => $store->id,
                                            'skew_user_id' => $skew_user->id,
                                            'assigned_by_manager_id' => $skew_manager->id,
                                            'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                            'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        ]);
                                        DB::table('skew_user_member_stores')->insert([
                                            'store_id' => $store->id,
                                            'skew_user_id' => $skew_user->id,
                                            'assigned_by_manager_id' => $skew_manager->id,
                                            'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                            'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        ]);
                                    }
                                }

                            }

                            if($manager_auth_id && $skew_user_admin)
                            {

                                foreach ($stores as $store)
                                {
                                    if($request->data['authorization'])
                                    {
                                        $admin_action = new AdminAction();
                                        $admin_action->skew_user_id = $skew_user_admin->id;
                                        $admin_action->source_id =$skew_user->id;
                                        $admin_action->model_class ="App\Models\Users\SkewUser";
                                        $admin_action->action_type ="update";
                                        $admin_action->action =$this->getMessageText('manager_model')." ".$this->getMessageText('edit_success_message');
                                        $admin_action->store_id =$store->id;
                                        $admin_action->save();
                                    }
                                    else
                                    {
                                        if(isset($request->data['shop_id']) && $store->reference_id == $request->data['shop_id'])
                                        {
                                            $admin_action = new AdminAction();
                                            $admin_action->skew_user_id = $skew_user_admin->id;
                                            $admin_action->source_id =$skew_user->id;
                                            $admin_action->model_class ="App\Models\Users\SkewUser";
                                            $admin_action->action_type ="update";
                                            $admin_action->action =$this->getMessageText('manager_model')." ".$this->getMessageText('edit_success_message');
                                            $admin_action->store_id =$store->id;
                                            $admin_action->save();
                                        }
                                    }

                                }

                            }
                            return response()->json([
                                'success' => true,
                            ]);
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                    }
                    else
                    {
                        return response()->json([
                            'success' => true,
                        ]);
                    }
        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }
    }
}
