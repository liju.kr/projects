<?php

namespace App\Http\Controllers\ExternalApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use function GuzzleHttp\Psr7\str;

class QrCodeController extends Controller
{
    use HelperTrait;
    public function getQrCode(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $member_reference_id = $request->member_auth_id;
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if($skew_client_check)
        {

            $skew_manager = $skew_client_check->skew_user;
            $skew_user = SkewUser::where('reference_id', $member_reference_id)->storeMember()->first();
            $extra_buffer = 30 * 60; // 10 Min buffer time in Seconds
            $current_time_with_extra_buffer = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()) - $extra_buffer);
            $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
            $booking=null;
            if($skew_user)
            {
                $bookings = Booking::where('skew_user_id', $skew_user->id)
                                    ->valid()
                                    ->where('booking_to', '>=', date('Y-m-d H:i:s', strtotime($current_time_with_extra_buffer)))
                                    ->orderByBookingFrom()
                                    ->limit(2)
                                    ->get();



                if($bookings->count()==2)
                {

                    if($bookings[0]->booking_to == $bookings[1]->booking_from)
                    {

                        $booking = Booking::where('skew_user_id', $skew_user->id)
                            ->valid()
                            ->where('booking_to', '>=', $current_time)
                            ->orderByBookingFrom()
                            ->first();

                    }
                    else
                    {
                        $single_booking = Booking::where('skew_user_id', $skew_user->id)
                            ->valid()
                            ->where('booking_to', '>=', date('Y-m-d H:i:s', strtotime($current_time_with_extra_buffer)))
                            ->orderByBookingFrom()
                            ->first();
                        if($single_booking) {
                            $buffer = $single_booking->room->store->store_detail->buffer_time_after * 60;
                            $current_with_buffer_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()) - $buffer);
                            $booking = Booking::where('skew_user_id', $skew_user->id)
                                ->valid()
                                ->where('booking_to', '>=', $current_with_buffer_time)
                                ->orderByBookingFrom()
                                ->first();
                        }

                    }
                }
                else
                {

                    $single_booking = Booking::where('skew_user_id', $skew_user->id)
                        ->valid()
                        ->where('booking_to', '>=', date('Y-m-d H:i:s', strtotime($current_time_with_extra_buffer)))
                        ->orderByBookingFrom()
                        ->first();
                    if($single_booking)
                    {
                        $buffer = $single_booking->room->store->store_detail->buffer_time_after * 60;
                        $current_with_buffer_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()) - $buffer);
                        $booking = Booking::where('skew_user_id', $skew_user->id)
                            ->valid()
                            ->where('booking_to', '>=', $current_with_buffer_time)
                            ->orderByBookingFrom()
                            ->first();
                    }

                }

                if($booking)
                {
                    $qr_code = [
                        'qr_code' =>$booking->qr_code,
                        'reserved_day'=>date('Y-m-d H:i', strtotime($booking->booking_from))
                    ];

                }


                if(!$booking)
                {
                    return response()->json([
                        'success' => true,
                        'qr_code' =>null
                    ]);
                }

                    return response()->json([
                        'success' => true,
                        'qr_code' => $qr_code,
                    ]);
            }
            else
            {
                return response()->json([
                    'success' => false,
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }
    }
}
