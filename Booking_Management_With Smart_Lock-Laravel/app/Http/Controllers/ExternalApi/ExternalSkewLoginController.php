<?php

namespace App\Http\Controllers\ExternalApi;

use App\Http\Controllers\Controller;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ExternalSkewLoginController extends Controller
{
    use HelperTrait;
    public function loginToSkewPanel(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $manager_reference_id = $request->manager_auth_id;
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if($skew_client_check)
        {
            $manager = SkewUser::where('reference_id', $manager_reference_id)->storeManager()->first();

            if(!$manager)
            {
                return response()->json([
                    'success' => false,
                ]);
            }
            $skew_user_id = $manager->id;
            $manager_token = $manager->skew_user_token;

            $random1 = Str::random(12);
            $login_session_token = $skew_user_id.$random1;
            if($manager_token)
            {

                $manager_token->login_session_token = $login_session_token;
                $manager_token->save();
            }
            else
            {

                $manager_token = new SkewUserToken();
                $manager_token->skew_user_id = $skew_user_id;
                $manager_token->login_session_token = $login_session_token;
                $manager_token->save();
            }

            return response()->json([
                'success' => true,
                'skew_panel_url' => env('SKEW_MANAGER_PANEL_URL')."/login?token=".$login_session_token,
            ]);

        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }
    }
}
