<?php

namespace App\Http\Controllers\ExternalApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Settings\AdminAction;
use App\Models\Stores\Entry;
use App\Models\Stores\Room;
use App\Models\Stores\Store;
use App\Models\Stores\StoreDetail;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserProfile;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StoreDataController extends Controller
{
    use HelperTrait;
    public function externalApiStoreData(Request $request)
    {
        $skew_client_id = $request->skew_client_id;
        $skew_client_secret = $request->skew_client_secret;
        $model = $request->model;
        $reference_id = $request->reference_id;
        $store_reference_id = $request->store_reference_id;
        $manager_auth_id = $request->manager_auth_id;
        if($manager_auth_id) $skew_user_admin = SkewUser::where('reference_id', $manager_auth_id)->storeManager()->first();
        $skew_client_check = SkewUserToken::where('skew_client_id', $skew_client_id)->where('skew_client_secret', $skew_client_secret)->first();
        if ($skew_client_check) {
               $skew_manager = $skew_client_check->skew_user;
                    if ($model == "App\Models\Shop") {
                        $store_last_id = Store::max('id');
                        $random1 = Str::random(10);
                        $random2 = Str::random(12);
                        $skew_client_id = "Skew_" . $random1 . ($store_last_id + 1);
                        $skew_client_secret = "Skew_Secret" . ($store_last_id + 1) . $random2;

                        $store = new Store();
                        $store->name = $request->data['name'];
                        $store->slug = $this->createSlug($request->data['name']) . "-" . $reference_id;
                        $store->reference_id = $reference_id;
                        $store->skew_user_id = $skew_manager->id;
                        $store->is_verified = 1;
                        if(isset($request->data['address']) && $request->data['address']) $store->address_line_1 = $request->data['address'];
                        if(isset($request->data['status']) && $request->data['status']) $store->status = $request->data['status'];
                        $store->skew_client_id = $skew_client_id;
                        $store->skew_client_secret = $skew_client_secret;
                        $store->save();
                        if ($store) {
                            $store_detail = new StoreDetail();
                            $store_detail->store_id = $store->id;
                            $store_detail->primary_contact_number = $request->data['tel'];
                            $store_detail->save();

                            DB::table('skew_user_store_managers')->insert([
                                'store_id' => $store->id,
                                'skew_user_id' => $skew_manager->id,
                                'assigned_by_manager_id' => $skew_manager->id,
                                'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                            ]);

                            if ($store_detail) {
                                if($manager_auth_id && $skew_user_admin)
                                {
                                    $admin_action = new AdminAction();
                                    $admin_action->skew_user_id = $skew_user_admin->id;
                                    $admin_action->source_id =$store->id;
                                    $admin_action->model_class ="App\Models\Stores\Store";
                                    $admin_action->action_type ="store";
                                    $admin_action->action =$this->getMessageText('store_model')." ".$this->getMessageText('add_success_message');
                                    $admin_action->store_id =$store->id;
                                    $admin_action->save();
                                }
                                return response()->json([
                                    'success' => true,
                                ]);
                            } else {
                                return response()->json([
                                    'success' => false,
                                ]);
                            }

                        } else {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                    }
                    elseif ($model == "App\Models\Member")
                    {

                        $store = $skew_manager->assigned_managing_stores()
                            ->where('stores.reference_id', $store_reference_id)
                            ->orderByName()->first();
                        if (!$store || $store->skew_user_id != $skew_manager->id) {
                            return response()->json([
                                'success' => false,
                            ]);
                        }


                        $skew_user = new SkewUser();
                        if(isset($request->data['name']) && $request->data['name']) $skew_user->first_name = $request->data['name'];
                        $skew_user->reference_id = $reference_id;
                        if(isset($request->data['email']) && $request->data['email']) $skew_user->email = $request->data['email'];
                        if(isset($request->data['kana']) && $request->data['kana']) $skew_user->first_name_furigana = $request->data['kana'];
                        if(isset($request->data['status']) && $request->data['status']) $skew_user->status = $request->data['status'];
                        $skew_user->is_any_store_member = 1;
                        $skew_user->is_verified = 1;

                        $skew_user->save();

                        if ($skew_user) {
                            $skew_member_profile = new SkewUserProfile();
                            $skew_member_profile->skew_user_id = $skew_user->id;
                            $skew_member_profile->save();

                            DB::table('skew_user_member_stores')->insert([
                                'store_id' => $store->id,
                                'skew_user_id' => $skew_user->id,
                                'assigned_by_manager_id' => $skew_manager->id,
                                'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                            ]);

                            if ($skew_member_profile) {

                                if($manager_auth_id && $skew_user_admin)
                                {
                                    $admin_action = new AdminAction();
                                    $admin_action->skew_user_id = $skew_user_admin->id;
                                    $admin_action->source_id =$skew_user->id;
                                    $admin_action->model_class ="App\Models\Users\SkewUser";
                                    $admin_action->action_type ="store";
                                    $admin_action->action =$this->getMessageText('member_model')." ".$this->getMessageText('add_success_message');
                                    $admin_action->store_id =$store->id;
                                    $admin_action->save();
                                }
                                return response()->json([
                                    'success' => true,
                                ]);
                            } else {
                                return response()->json([
                                    'success' => false,
                                ]);
                            }
                        } else {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                    }
                    elseif ($model == "App\Models\Booth")
                    {
                        $store = $skew_manager->assigned_managing_stores()
                            ->where('stores.reference_id', $request->data['shop_id'])
                            ->orderByName()->first();

                        if (!$store || $store->skew_user_id != $skew_manager->id) {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                        $room = new Room();
                        if(isset($request->data['name']) && $request->data['name'])$room->name = $request->data['name'];
                        $room->reference_id = $reference_id;
                        if(isset($request->data['status']) && $request->data['status'])$room->status = $request->data['status'];
                        $room->skew_user_id = $skew_manager->id;
                        $room->store_id = $store->id;
                        $room->save();

                        if ($room) {
                            $entry_last_id = Entry::max('id');
                            $random1 = Str::random(4);
                            $entry_unique_id = "Entry_" . ($entry_last_id + 1) . $random1;

                            $entry = new Entry();
                            $entry->room_id = $room->id;
                            $entry->skew_user_id = $skew_manager->id;
                            $entry->entry_unique_id = $entry_unique_id;
                            $entry->is_verified = 1;
                            $entry->save();

                            if ($entry) {
                                if($manager_auth_id && $skew_user_admin)
                                {
                                    $admin_action = new AdminAction();
                                    $admin_action->skew_user_id = $skew_user_admin->id;
                                    $admin_action->source_id =$room->id;
                                    $admin_action->model_class ="App\Models\Stores\Room";
                                    $admin_action->action_type ="store";
                                    $admin_action->action =$this->getMessageText('room_model')." ".$this->getMessageText('add_success_message');
                                    $admin_action->store_id =$store->id;
                                    $admin_action->save();
                                }


                                return response()->json([
                                    'success' => true,
                                ]);
                            } else {
                                return response()->json([
                                    'success' => false,
                                ]);
                            }

                        } else {
                            return response()->json([
                                'success' => false,
                            ]);
                        }


                    }
                    elseif ($model== "App\Models\Reservation")
                    {

                        if(isset($request->data['manager_id']) && $request->data['manager_id'])
                        {
                            $booked_manager = SkewUser::where('reference_id', $request->data['manager_id'])->storeManager()->first();
                            if(!$booked_manager)
                            {
                                return response()->json([
                                    'success' => false,
                                ]);

                            }
                            $manager_id = $booked_manager->id;
                        }
                        else
                        {
                            $manager_id = null;
                        }


                        if(isset($request->data['member_id']) && $request->data['member_id'])
                        {

                            $booked_member = SkewUser::where('reference_id', $request->data['member_id'])->storeMember()->first();
                            if(!$booked_member)
                            {
                                return response()->json([
                                'success' => false,
                            ]);

                            }
                            $skew_user_id = $booked_member->id;

                        }
                        else
                        {
//                            return response()->json([
//                                'success' => false,
//                            ]);
                            $skew_user_id = $booked_manager->id;
                        }

                        if(isset($request->data['booth_id']) && $request->data['booth_id'])
                        {
                            $booked_room = Room::where('reference_id', $request->data['booth_id'])->verified()->first();
                            if(!$booked_room)
                            {
                                return response()->json([
                                    'success' => false,
                                ]);

                            }
                            $room_id = $booked_room->id;
                            $booked_store_id = $booked_room->store_id;
                            $slot = $booked_room->default_slot_range_in_hour * 60*60;

                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                        if(isset($request->data['reserve_date']) && $request->data['reserve_date'])
                        {

                            $booking_from = date('Y-m-d H:i:s', strtotime($request->data['reserve_date'])); // Back to string
                            $booking_to = date('Y-m-d H:i:s', strtotime($request->data['reserve_date']) + $slot); // Add slot range hour
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }


                        $booking_last_id = Booking::max('id');
                        $random1 = Str::random(12);
                        $qr_code = "Booking_" . ($booking_last_id + 1) . $random1;


                        $booking = new Booking();
                        $booking->reference_id = $reference_id;
                        $booking->skew_user_id = $skew_user_id;
                        $booking->room_id = $room_id;
                        $booking->skew_manager_id =$manager_id;
                        if(isset($request->data['note']) && $request->data['note']) $booking->remarks = $request->data['note'];
                        $booking->booking_from = $booking_from;
                        $booking->booking_to = $booking_to;
                        $booking->qr_code = $qr_code;
                        $booking->save();

                        if($booking)
                        {
                            if($manager_auth_id && $skew_user_admin && isset($request->data['manager_id']) && $request->data['manager_id'])
                            {
                                $admin_action = new AdminAction();
                                $admin_action->skew_user_id = $skew_user_admin->id;
                                $admin_action->source_id =$booking->id;
                                $admin_action->model_class ="App\Models\Bookings\Booking";
                                $admin_action->action_type ="store";
                                $admin_action->action =$this->getMessageText('booking_model')." ".$this->getMessageText('add_success_message');
                                $admin_action->store_id =$booked_store_id;
                                $admin_action->save();
                            }
                            return response()->json([
                                'success' => true,
                            ]);
                        }
                        else
                        {
                            return response()->json([
                                'success' => false,
                            ]);
                        }

                    }
                    elseif ($model == "App\Models\Manager")
                    {
                        $stores = $skew_manager->own_stores()->get();
                        $email = $request->data['account']."@skewplus.com";
                        $password = bcrypt($request->data['password']);

                        $skew_user = new SkewUser();
                        $skew_user->first_name = $request->data['name'];
                        $skew_user->reference_id = $reference_id;
                        $skew_user->email = $email;
                        $skew_user->password = $password;
                        $skew_user->status = $request->data['status'];
                        $skew_user->is_store_manager = 1;
                        $skew_user->is_any_store_member = 1;
                        $skew_user->is_verified = 1;

                        $skew_user->save();

                        if ($skew_user) {
                            $skew_member_profile = new SkewUserProfile();
                            $skew_member_profile->skew_user_id = $skew_user->id;
                            $skew_member_profile->save();

                            foreach ($stores as $store)
                            {
                                if($request->data['authorization'])
                                {
                                    DB::table('skew_user_store_managers')->insert([
                                        'store_id' => $store->id,
                                        'skew_user_id' => $skew_user->id,
                                        'assigned_by_manager_id' => $skew_manager->id,
                                        'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                    ]);
                                    DB::table('skew_user_member_stores')->insert([
                                        'store_id' => $store->id,
                                        'skew_user_id' => $skew_user->id,
                                        'assigned_by_manager_id' => $skew_manager->id,
                                        'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                    ]);
                                }
                                else
                                {
                                    if(isset($request->data['shop_id']) && $store->reference_id == $request->data['shop_id'])
                                    {
                                        DB::table('skew_user_store_managers')->insert([
                                            'store_id' => $store->id,
                                            'skew_user_id' => $skew_user->id,
                                            'assigned_by_manager_id' => $skew_manager->id,
                                            'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                            'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        ]);
                                        DB::table('skew_user_member_stores')->insert([
                                            'store_id' => $store->id,
                                            'skew_user_id' => $skew_user->id,
                                            'assigned_by_manager_id' => $skew_manager->id,
                                            'created_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                            'updated_at' => date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now())),
                                        ]);
                                    }
                                }

                            }

                            if($manager_auth_id && $skew_user_admin)
                            {
                                foreach ($stores as $store)
                                {
                                    if($request->data['authorization'])
                                    {
                                        $admin_action = new AdminAction();
                                        $admin_action->skew_user_id = $skew_user_admin->id;
                                        $admin_action->source_id =$skew_user->id;
                                        $admin_action->model_class ="App\Models\Users\SkewUser";
                                        $admin_action->action_type ="store";
                                        $admin_action->action =$this->getMessageText('manager_model')." ".$this->getMessageText('add_success_message');
                                        $admin_action->store_id =$store->id;
                                        $admin_action->save();
                                    }
                                    else
                                    {
                                        if(isset($request->data['shop_id']) && $store->reference_id == $request->data['shop_id'])
                                        {
                                            $admin_action = new AdminAction();
                                            $admin_action->skew_user_id = $skew_user_admin->id;
                                            $admin_action->source_id =$skew_user->id;
                                            $admin_action->model_class ="App\Models\Users\SkewUser";
                                            $admin_action->action_type ="store";
                                            $admin_action->action =$this->getMessageText('manager_model')." ".$this->getMessageText('add_success_message');
                                            $admin_action->store_id =$store->id;
                                            $admin_action->save();
                                        }
                                    }

                                }

                            }

                            if ($skew_member_profile) {
                                return response()->json([
                                    'success' => true,
                                ]);
                            } else {
                                return response()->json([
                                    'success' => false,
                                ]);
                            }
                        } else {
                            return response()->json([
                                'success' => false,
                            ]);
                        }
                    }
                    else
                    {
                        return response()->json([
                            'success' => true,
                        ]);
                    }
        }
        else
        {
            return response()->json([
                'success' => false,
            ]);
        }

    }


}
