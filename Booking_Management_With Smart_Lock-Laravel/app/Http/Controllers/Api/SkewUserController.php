<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserProfile;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SkewUserController extends Controller
{
    use HelperTrait;
    public function SkewUserRegister(Request $request)
    {

        $validator =  Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'first_name_furigana' => 'required|max:255',
            'email' => 'required|email|unique:skew_users',
            'mobile_number' => 'required|unique:skew_users',
            'skew_client_id' => 'required',
            'skew_client_secret' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if(!($request->skew_client_id == 1 && $request->skew_client_secret == "secret")) //
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'api_credentials_not_match',
                'message' => $this->getMessageText('unknown_client')
            ]);
        }

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {
            $skew_user =  new SkewUser();
            $skew_user->first_name = $request->first_name;
            $skew_user->first_name_furigana = $request->first_name_furigana;
            $skew_user->email = $request->email;
            $skew_user->mobile_number = $request->mobile_number;
            $skew_user->save();
            if($skew_user)
            {
                $skew_user_profile = new SkewUserProfile();
                $skew_user_profile->skew_user_id = $skew_user->id;
                $skew_user_profile->save();
                if($skew_user_profile)
                {
                    return response()->json([
                         'status' => 200,
                        'status_message' => 'success',
                        'message' => $this->getMessageText('add_success_message'),
                    ]);
                }
                else
                {
                    return response()->json([
                         'status' => 200,
                        'status_message' => 'failed',
                        'message' => $this->getMessageText('failed_message'),
                    ]);
                }

            }
            else
            {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
    }
}
