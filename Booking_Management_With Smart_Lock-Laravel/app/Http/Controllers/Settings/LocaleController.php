<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings\Locale;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Validator;

class LocaleController extends Controller
{
    use HelperTrait;
   public function index()
   {

       $locales = Locale::orderByKey()->paginate(25);
       return view('pages.admin.locales.index', compact('locales'));
   }

    public function store(Request $request)
    {
         $this->validate($request, [
            'key' => 'required|alpha_dash|max:150|unique:locales',
            'en' => 'required',
            'ja' => 'required',
            'type' => 'required',
            'panel' => 'required',
            'status' => 'required',
            ], [], $attributes = $this->localize_validation_form_elements());

        $locale = new Locale();
        $locale->key = $request->key;
        $locale->en = $request->en;
        $locale->ja = $request->ja;
        $locale->type = $request->type;
        $locale->panel = $request->panel;
        $locale->status = $request->status;
        $locale->save();



        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('add_success_message'));
    }

    public function edit($id)
    {
        $locale = Locale::findOrFail($id);
        $locales = Locale::orderByKey()->paginate(25);
        return view('pages.admin.locales.index', compact('locale', 'locales'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'key' => 'required|alpha_dash|max:150|unique:locales,key,'.$id,
            'en' => 'required',
            'ja' => 'required',
            'type' => 'required',
            'panel' => 'required',
            'status' => 'required',
            ], [], $attributes = $this->localize_validation_form_elements());

        $locale = Locale::findOrFail($id);
        $locale->key = $request->key;
        $locale->en = $request->en;
        $locale->ja = $request->ja;
        $locale->type = $request->type;
        $locale->panel = $request->panel;
        $locale->status = $request->status;
        $locale->save();


        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('edit_success_message'));
    }

    public function destroy(Request $request, $id)
    {
        $locale = Locale::findOrFail($id);
        $locale->delete();

        return redirect('locales')
            ->with('flash_message_success', $this->getMessageText('delete_success_message'));
    }
}
