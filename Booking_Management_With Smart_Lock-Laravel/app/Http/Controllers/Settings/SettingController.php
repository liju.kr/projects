<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings\Country;
use App\Models\Settings\State;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function getCountries(Request $request)
    {

        $countries = Country::orderByName()->select('id','name','nice_name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'countries' => $countries
        ]);
    }
    public function getStates(Request $request)
    {

        $country = Country::findOrFail($request->country);
        $states = $country->states()->orderByName()->select('id','name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'states' => $states
        ]);
    }
    public function getCities(Request $request)
    {

        $state= State::findOrFail($request->state);
        $cities = $state->cities()->orderByName()->select('id','name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'cities' => $cities
        ]);
    }
}
