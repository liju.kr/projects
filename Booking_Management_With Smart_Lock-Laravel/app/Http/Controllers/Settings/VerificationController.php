<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Stores\Entry;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    use HelperTrait;
    public function index()
    {

        $entries = Entry::latest()->paginate(25);
        return view('pages.admin.entries.index', compact('entries'));
    }


    public function edit($id)
    {
        $entry = Entry::findOrFail($id);
        $entries = Entry::latest()->paginate(25);;
        return view('pages.admin.entries.index', compact('entry', 'entries'));
    }

    public function update(Request $request, $id)
    {


        $entry = Entry::findOrFail($id);
        $room = $entry->room;
        $store = $room->store;


        $entry->lock_device_id = $request->lock_device_id;
        $entry->lock_secret_key = $request->lock_secret_key;
        $entry->save();


        $room->is_verified = $request->is_verified;
        $room->save();

        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('edit_success_message'));
    }


}
