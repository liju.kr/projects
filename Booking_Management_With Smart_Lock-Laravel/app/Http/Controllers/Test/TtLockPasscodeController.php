<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\Test\TtLockPasscode;
use App\Models\Test\TtLockRoom;
use App\Models\Test\TtLockUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;

class TtLockPasscodeController extends Controller
{
    use HelperTrait;

    public function index($tt_lock_user_id, $tt_lock_room_id)
    {
        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_room = TtLockRoom::findOrFail($tt_lock_room_id);
        $tt_lock_passcodes = $tt_lock_room->tt_lock_passcodes()->paginate(25);
        return view('pages.admin.tt_lock_users.passcodes', compact('tt_lock_user', 'tt_lock_room', 'tt_lock_passcodes'));
    }


    public function store(Request $request, $tt_lock_user_id, $tt_lock_room_id)
    {
        $this->validate($request, [
            'passcode_name' => 'required',
            'passcode' => 'required|numeric|digits_between:4,9',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ], [], $attributes = $this->localize_validation_form_elements());


        $client_id ="7e052b5d96c6460fb009681a8425097f";
        $client_secret ="f5700a8918b85024658bca93e4af59a9";

        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_room = TtLockRoom::findOrFail($tt_lock_room_id);

        $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds

        $start_date_utc = date('Y-m-d H:i:s', strtotime($request->start_date) - $utc_japan_ahead);
        $end_date_utc = date('Y-m-d H:i:s', strtotime($request->end_date) - $utc_japan_ahead);

        $start_date_utc_ms = strtotime($start_date_utc) * 1000;
        $end_date_utc_ms = strtotime($end_date_utc) * 1000;

        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $current_time_in_jst_ms = strtotime($current_time) * 1000;


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.ttlock.com/v3/keyboardPwd/add',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&accessToken='.$tt_lock_user->tt_access_token.'&lockId='.$tt_lock_room->lock_id.'&keyboardPwd='.$request->passcode.'&keyboardPwdName='.$request->passcode_name.'&startDate='.$start_date_utc_ms.'&endDate='.$end_date_utc_ms.'&addType=2&date='.$current_time_in_jst_ms.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $tt_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);
        if($tt_status==200 && isset($res['keyboardPwdId']))
        {
            $tt_lock_passcode = new TtLockPasscode();
            $tt_lock_passcode->passcode_name = $request->passcode_name;
            $tt_lock_passcode->passcode = $request->passcode;
            $tt_lock_passcode->tt_lock_room_id = $tt_lock_room_id;
            $tt_lock_passcode->start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
            $tt_lock_passcode->end_date = date('Y-m-d H:i:s', strtotime($request->end_date));
            $tt_lock_passcode->passcode_id = $res['keyboardPwdId'];
            $tt_lock_passcode->save();
        }
        elseif($tt_status==200 && isset($res['errcode']))
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
        }

        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('add_success_message'));
    }

    public function edit($tt_lock_user_id, $tt_lock_room_id, $id)
    {
        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_room = TtLockRoom::findOrFail($tt_lock_room_id);
        $tt_lock_passcodes = $tt_lock_room->tt_lock_passcodes()->paginate(25);
        $tt_lock_passcode = TtLockPasscode::findOrFail($id);
        return view('pages.admin.tt_lock_users.passcodes', compact('tt_lock_user', 'tt_lock_room', 'tt_lock_passcodes', 'tt_lock_passcode'));
    }


    public function update(Request $request, $tt_lock_user_id, $tt_lock_room_id, $id)
    {
        $client_id ="7e052b5d96c6460fb009681a8425097f";
        $client_secret ="f5700a8918b85024658bca93e4af59a9";

        $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds

        $start_date_utc = date('Y-m-d H:i:s', strtotime($request->start_date) - $utc_japan_ahead);
        $end_date_utc = date('Y-m-d H:i:s', strtotime($request->end_date) - $utc_japan_ahead);

        $start_date_utc_ms = strtotime($start_date_utc) * 1000;
        $end_date_utc_ms = strtotime($end_date_utc) * 1000;

        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $current_time_in_jst_ms = strtotime($current_time) * 1000;

        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_room = TtLockRoom::findOrFail($tt_lock_room_id);
        $tt_lock_passcodes = $tt_lock_room->tt_lock_passcodes()->paginate(25);
        $tt_lock_passcode = TtLockPasscode::findOrFail($id);

        $this->validate($request, [
            'passcode_name' => 'required',
            'passcode' => 'required|numeric|digits_between:4,9',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ], [], $attributes = $this->localize_validation_form_elements());


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.ttlock.com/v3/keyboardPwd/change',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&accessToken='.$tt_lock_user->tt_access_token.'&lockId='.$tt_lock_room->lock_id.'&keyboardPwdId='.$tt_lock_passcode->passcode_id.'&keyboardPwdName='.$request->passcode_name.'&newKeyboardPwd='.$request->passcode.'&startDate='.$start_date_utc_ms.'&endDate='.$end_date_utc_ms.'&changeType=2&date='.$current_time_in_jst_ms.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $tt_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);
        if($tt_status==200 && isset($res['errcode']) && $res['errcode']==0)
        {
            $tt_lock_passcode->passcode_name = $request->passcode_name;
            $tt_lock_passcode->passcode = $request->passcode;
            $tt_lock_passcode->tt_lock_room_id = $tt_lock_room_id;
            $tt_lock_passcode->start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
            $tt_lock_passcode->end_date = date('Y-m-d H:i:s', strtotime($request->end_date));
            $tt_lock_passcode->save();
        }
        elseif($tt_status==200 && isset($res['errcode']))
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
        }

        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('edit_success_message'));


        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('edit_success_message'));
    }

}
