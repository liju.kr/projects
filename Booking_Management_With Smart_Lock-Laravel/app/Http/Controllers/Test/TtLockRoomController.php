<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\Test\TtLockRoom;
use App\Models\Test\TtLockUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;

class TtLockRoomController extends Controller
{
    use HelperTrait;

    public function index($tt_lock_user_id)
    {
        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_rooms = $tt_lock_user->tt_lock_rooms()->paginate(25);
        return view('pages.admin.tt_lock_users.rooms', compact('tt_lock_user', 'tt_lock_rooms'));
    }

    public function store(Request $request, $tt_lock_user_id)
    {
        $this->validate($request, [
            'room_name' => 'required',
            'lock_id' => 'required|unique:tt_lock_rooms',
        ], [], $attributes = $this->localize_validation_form_elements());

        $tt_lock_room = new TtLockRoom();
        $tt_lock_room->room_name = $request->room_name;
        $tt_lock_room->tt_lock_user_id = $tt_lock_user_id;
        $tt_lock_room->lock_id = $request->lock_id;
        $tt_lock_room->save();
        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('add_success_message'));
    }

    public function edit($tt_lock_user_id, $id)
    {
        $tt_lock_user = TtLockUser::findOrFail($tt_lock_user_id);
        $tt_lock_room = TtLockRoom::findOrFail($id);
        $tt_lock_rooms = $tt_lock_user->tt_lock_rooms()->paginate(25);

        $client_id ="7e052b5d96c6460fb009681a8425097f";
        $client_secret ="f5700a8918b85024658bca93e4af59a9";
        $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds

        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $current_time_in_jst_ms = strtotime($current_time) * 1000;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.ttlock.com/v3/lock/queryOpenState',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&accessToken='.$tt_lock_user->tt_access_token.'&lockId='.$tt_lock_room->lock_id.'&date='.$current_time_in_jst_ms.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $tt_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);
        if($tt_status==200 && isset($res['state']))
        {
          $state = $res['state'];
        }
        elseif($tt_status==200 && isset($res['errcode']))
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
        }


        return view('pages.admin.tt_lock_users.rooms', compact('tt_lock_user', 'tt_lock_rooms', 'tt_lock_room', 'state'));
    }

    public function update(Request $request, $tt_lock_user_id, $id)
    {
        $this->validate($request, [
            'room_name' => 'required',
            'lock_id' => 'required|unique:tt_lock_rooms,lock_id,'.$id,
        ], [], $attributes = $this->localize_validation_form_elements());

        $tt_lock_room =  TtLockRoom::findOrFail($id);
        $tt_lock_room->room_name = $request->room_name;
        $tt_lock_room->lock_id = $request->lock_id;
        $tt_lock_room->save();

        return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('add_success_message'));
    }

    public function lockUnlock(Request $request, $tt_lock_user_id, $id)
    {
        $tt_lock_room =  TtLockRoom::findOrFail($id);
        $tt_lock_user =  TtLockUser::findOrFail($tt_lock_user_id);

        $client_id ="7e052b5d96c6460fb009681a8425097f";
        $client_secret ="f5700a8918b85024658bca93e4af59a9";
        $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds

        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $current_time_in_jst_ms = strtotime($current_time) * 1000;

        if($request->state==0)
        {
            $url="https://api.ttlock.com/v3/lock/unlock";
            $msg='unlock_success';
        }
        if($request->state==1)
        {
            $url="https://api.ttlock.com/v3/lock/lock";
            $msg='lock_success';
        }


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&accessToken='.$tt_lock_user->tt_access_token.'&lockId='.$tt_lock_room->lock_id.'&date='.$current_time_in_jst_ms.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $tt_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);
        if($tt_status==200 && isset($res['errcode']) && $res['errcode']==0 )
        {
           return redirect('tt_lock_users/'.$tt_lock_user->id.'/tt_lock_rooms/'.$tt_lock_room->id.'/edit') ->with('flash_message_success', $this->getMessageText($msg));;
        }
        elseif($tt_status==200 && isset($res['errcode']))
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
        }

        return redirect()
            ->back()
            ->with('flash_message_error', $this->getMessageText('failed_message'));
    }

}
