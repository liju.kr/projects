<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\Test\TtLockUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Session;

class TtLockUserController extends Controller
{
    use HelperTrait;
   public function index()
   {
       $tt_lock_users = TtLockUser::latest()->paginate(25);
       return view('pages.admin.tt_lock_users.index', compact('tt_lock_users'));
   }

    public function store(Request $request)
    {
        $this->validate($request, [
            'store_name' => 'required',
            'tt_user_name' => 'required|alpha_dash|max:150|unique:tt_lock_users',
            'tt_password' => 'required|confirmed|min:6',
        ], [], $attributes = $this->localize_validation_form_elements());

    $client_id ="7e052b5d96c6460fb009681a8425097f";
    $client_secret ="f5700a8918b85024658bca93e4af59a9";

    $password= md5($request->tt_password);

         $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds
         $current_jst_in_utc = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()) - $utc_japan_ahead);
         $current_jst_utc_ms = strtotime($current_jst_in_utc) * 1000;
         $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
         $current_time_in_jst_ms = strtotime(date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()))) * 1000;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.ttlock.com/v3/user/register',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&clientSecret='.$client_secret.'&username='.$request->tt_user_name.'&password='.$password.'&date='.$current_jst_utc_ms.'',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: JSESSIONID='.Session::getId()
            ),
        ));
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $tt_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);
        if($tt_status==200 && isset($res['username']))
        {
            $tt_lock_user = new TtLockUser();
            $tt_lock_user->tt_user_name = $res['username'];
            $tt_lock_user->tt_password = $request->tt_password;
            $tt_lock_user->store_name = $request->store_name;
            $tt_lock_user->save();
        }
        elseif($tt_status==200 && isset($res['errcode']))
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
        }

 return redirect()
            ->back()
            ->with('flash_message_success', $this->getMessageText('add_success_message'));
    }

    public function viewLocks($id)
    {
        $tt_lock_user = TtLockUser::findOrFail($id);

        $client_id ="7e052b5d96c6460fb009681a8425097f";
        $client_secret ="f5700a8918b85024658bca93e4af59a9";

        $utc_japan_ahead = 9 * 60 * 60; // 9 Hr buffer time in Seconds
        $current_jst_in_utc = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()) - $utc_japan_ahead);
        $current_jst_utc_ms = strtotime($current_jst_in_utc) * 1000;
        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $current_time_in_jst_ms = strtotime($current_time) * 1000;
        $page_no = 1;
        $page_size = 20;

        if($tt_lock_user)
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.ttlock.com/v3/lock/list',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => 'clientId='.$client_id.'&accessToken='.$tt_lock_user->tt_access_token.'&pageNo='.$page_no.'&pageSize='.$page_size.'&date='.$current_time_in_jst_ms.'',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Cookie: JSESSIONID=20D3F91E9E598842447DB7D1EB00B838'
                ),
            ));

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $tt_status = $info['http_code'];
            $res = json_decode($response, true);
            curl_close($curl);
            if($tt_status==200 && (isset($res['total']) && $res['total']>=1))
            {
                return view('pages.admin.tt_lock_users.view_lock', compact('res', 'tt_lock_user'));
            }
            elseif($tt_status==200 && isset($res['errcode']))
            {
                return redirect()
                    ->back()
                    ->with('flash_message_error', $this->getMessageText('Error Code: '.$res['errcode'].' Error Msg: '.$res['errmsg'].' ('.$res['description'].')'));
            }
            else
                {
                    return redirect()
                        ->back()
                        ->with('flash_message_error', $this->getMessageText('failed_message'));
                }
        }
        else
        {
            return redirect()
                ->back()
                ->with('flash_message_error', $this->getMessageText('failed_message'));
        }

    }
}
