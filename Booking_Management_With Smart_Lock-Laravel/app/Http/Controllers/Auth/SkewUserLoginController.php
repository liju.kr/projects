<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class SkewUserLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/skew_user/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('skew_user');
    }
    public function showLoginForm()
    {
        return view('auth.skew_user-login');
    }
}
