<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Stores\Room;
use App\Models\Stores\Store;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    use HelperTrait;
    public function storeMemberChart()
    {
        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id  = $skew_manager->assigned_managing_stores()->pluck('stores.id');

        $stores = Store::whereIn('id', $stores_id);
        $stores->select('id', 'name', 'created_at', 'updated_at');
        $stores->withCount(['members' => function ($query)
        {
            $query->active();
        }]);
        $stores_data = $stores->orderByName()->get();

        $total_members = SkewUser::storeMember()->active()
                                     ->whereHas('assigned_member_stores', function ($query) use ($stores_id)
                                        {
                                            $query->whereIn('stores.id', $stores_id);
                                        })
                                        ->count();
        $colours = ['#F83756', '#F9A82F', '#F85240', '#F99162', '#FCD03F', '#df314d', '#e0972a', '#df4939', '#e08258', '#e2bb38', '#c62c44', '#c78625', '#c64133', '#c7744e', '#c9a632', '#f84b66', '#f9b043', '#f86353', '#f99c71', '#fcd452', '#f95e77', '#fab958', '#f97466', '#faa781', '#fcd965'];
        foreach ($stores_data as $key=>$data)
        {
            if($key>24)
            {
                $stores_data[$key]['colour_code'] = "#F83756";
            }
            else
            {
                $stores_data[$key]['colour_code'] = $colours[$key];
            }

        }

        return response()->json(['success' => true, 'status' => 200, 'stores' => $stores_data, 'total_members' => $total_members]);
    }

    public function todayUsage(Request $request)
    {
       // $skew_manager = Auth::guard('skew_user')->user();
        //$stores_id  = $skew_manager->assigned_managing_stores()->pluck('stores.id');

        $date_from = date('Y-m-d 00:00:00', strtotime(\Carbon\Carbon::now()));
        $date_to = date('Y-m-d 23:59:59', strtotime(\Carbon\Carbon::now()));
        if(isset($request->store_id) && $request->store_id) $store_id = $request->store_id;
        $rooms_id = Room::where('store_id', $store_id)->pluck('id');

        $bookings = Booking::whereIn('room_id', $rooms_id)->valid();
        $bookings->select('id', 'created_at', 'updated_at', 'booking_from', 'booking_to', 'skew_user_id', 'room_id');
        $bookings->with(['booked_by' => function ($query)
        {
            $query->select('id', 'first_name');
        }]);
        $bookings->where('booking_from', '>=',$date_from);
        $bookings->where('booking_from', '<=',$date_to);
        $bookings_data = $bookings->orderBy('booking_from', 'ASC')->get();

       $total_bookings = $bookings->count();
       $total_visits = $bookings->whereHas('booking_histories')->count();

        return response()->json(['success' => true, 'status' => 200, 'bookings' => $bookings_data, 'total_bookings'=>$total_bookings, 'total_visits'=>$total_visits, 'current_date'=>date('Y-m-d', strtotime(\Carbon\Carbon::now()))]);
    }
    public function usageChart(Request $request)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $request->store_id)
            ->orderByName()->first();
        if(!$store)
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }
        $rooms_id = Room::where('store_id', $store->id)->pluck('id');

        $start_date = $request->start_date;
        $end_date = date("Y-m-t", strtotime($start_date));
        $usage_data = [];
        $i=0;
        while (strtotime($start_date) <= strtotime($end_date)) {
            $timestamp = strtotime($start_date);
            $date = date('Y-m-d', $timestamp);
            $day = date('d', $timestamp);
            $month = date('F', $timestamp);

            $date_from = date('Y-m-d 00:00:00', strtotime($date));
            $date_to = date('Y-m-d 23:59:59', strtotime($date));

            $bookings = Booking::valid();
            $bookings->where('booking_from', '>=',$date_from);
            $bookings->where('booking_from', '<=',$date_to);
            $bookings->whereIn('room_id', $rooms_id);
            $total_bookings = $bookings->count();
            $total_visits = $bookings->whereHas('booking_histories')->count();
            $usage_data[$i]['day']=(int)$day;
            $usage_data[$i]['total_bookings']=$total_bookings;
            $usage_data[$i]['total_visits']=$total_visits;
            $usage_data[$i]['date']=$date;
            $usage_data[$i]['month']=$month;
            $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
            $i++;
        }
        return response()->json(['success' => true, 'status' => 200, 'usage_data' => $usage_data]);
    }

    public  function getMonthYear()
    {
        $months = [
            'Jan'=>'一月',
            'Feb'=>'二月',
            'Mar'=>'三月',
            'Apr'=>'四月',
            'May'=>'五月',
            'Jun'=>'六月',
            'Jul'=>'七月',
            'Aug'=>'八月',
            'Sep'=>'九月',
            'Oct'=>'十月',
            'Nov'=>'十一月',
            'Dec'=>'十二月',
        ];
        $start_date = "2019-05-01";
        $end_date = date('Y-m-d', strtotime(\Carbon\Carbon::now()));;
        $i=0;
        $data=[];
        while (strtotime($start_date) <= strtotime($end_date)) {
            $timestamp = strtotime($start_date);
            $date = date('Y-m-d', $timestamp);

            $month = date('F', $timestamp);
            $year = date('Y', $timestamp);
            $month_short = date('M', $timestamp);
            $month_number = date('m', $timestamp);

            $data[$i]['date']=$date;
            $data[$i]['month']=$month;
            $data[$i]['year']=$year;
            $data[$i]['month_number']=$month_number;
            $data[$i]['month_short']=$month_short;
            $data[$i]['month_japanese']=$months[$month_short];
            $start_date = date ("Y-m-d", strtotime("+1 months", strtotime($start_date)));
            $i++;
        }


        return response()->json(['success' => true, 'status' => 200, 'data' => array_reverse($data)]);
    }

}
