<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Settings\ServiceCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function getServiceCategories(Request $request)
    {

        $service_categories = ServiceCategory::orderByName()->select('id','name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'service_categories' => $service_categories
        ]);
    }
    public function getServices(Request $request)
    {

        $service_category = ServiceCategory::findOrFail($request->service_category);
        $services = $service_category->services()->orderByName()->select('id','name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'services' => $services
        ]);
    }
}
