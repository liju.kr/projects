<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Settings\Amenity;
use Illuminate\Http\Request;

class AmenityController extends Controller
{
    public function getAmenities(Request $request)
    {

        $amenities = Amenity::orderByName()->select('id','name')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'amenities' => $amenities
        ]);
    }
}
