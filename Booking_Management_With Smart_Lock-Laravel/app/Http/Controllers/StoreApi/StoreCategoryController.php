<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Stores\StoreCategory;
use Illuminate\Http\Request;

class StoreCategoryController extends Controller
{
    public function getStoreCategories(Request $request)
    {

        $store_categories = StoreCategory::orderByName()->select('id','name','slug')->active()->get();

        return response()->json([
            'success' => true,
             'status' => 200,
            'store_categories' => $store_categories
        ]);
    }
}
