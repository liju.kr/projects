<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Stores\Room;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    use HelperTrait;
    public function UsageHistory(Request $request)
    {

        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id = $skew_manager->assigned_managing_stores()->whereIn('stores.id', json_decode($request->stores_id))->pluck('stores.id');
        $rooms_id = Room::whereIn('store_id', $stores_id)->pluck('id');
        $bookings = Booking::whereIn('room_id', $rooms_id);

        if (isset($request->keyword) && $request->keyword)
        {
            $keyword = $request->keyword;
            $bookings->where(function ($query) use ($keyword)
            {
                $query->orWhere('booking_from', 'like', '%' . $keyword . '%');
                $query->orWhereHas('booking_histories', function ($query) use ($keyword)
                {
                    $query->where('booking_histories.entered_at', 'like', '%' . $keyword . '%');
                });
                $query->orWhereHas('booked_by', function ($query) use ($keyword)
                {
                    $query->where('skew_users.first_name', 'like', '%' . $keyword . '%');
                });
                $query->orWhereHas('room', function ($query) use ($keyword)
                {
                    $query->where('rooms.name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('store', function ($query) use ($keyword)
                    {
                        $query->where('stores.name', 'like', '%' . $keyword . '%');
                    });
                });
            });

        }
        $bookings_data = $bookings->orderBy('booking_from', 'DESC')->paginate($perPageLimit);
        $expired_not_visited = $bookings->expiredNotVisited()->count();

        return response()->json(['success' => true, 'status' => 200, 'booking_data' => $bookings_data, 'expired_not_visited' => $expired_not_visited]);

    }

}
