<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Settings\AdminAction;
use App\Models\Settings\Country;
use App\Models\Stores\Store;
use App\Models\Stores\StoreDetail;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class StoreController extends Controller
{
    use HelperTrait;
    public function index(Request $request)
    {
      if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id = $skew_manager->assigned_managing_stores()
            ->pluck('stores.id');

        $stores = Store::whereIn('id', $stores_id);
        $stores->select('id', 'name', 'address_line_1', 'status', 'created_at', 'updated_at');
        $stores->withCount(['rooms' => function ($query)
        {
            $query->verified();
        }]);
        $stores->with('store_detail');

        if (isset($request->keyword) && $request->keyword)
        {
            $keyword = $request->keyword;
            if(gettype($keyword)=="integer")
            {
                $stores->having('rooms_count', '=', $keyword);
            }
            else
            {
                $stores->where(function ($query) use ($keyword)
                {
                    $query->orWhere('name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('store_detail', function ($query) use ($keyword)
                    {
                        $query->where('store_details.primary_contact_number', 'like', '%' . $keyword . '%');
                        $query->orWhere('store_details.buffer_time_before', 'like', '%' . $keyword . '%');
                        $query->orWhere('store_details.buffer_time_after', 'like', '%' . $keyword . '%');
                    });
                    $query->orWhere('address_line_1', 'like', '%' . $keyword . '%');
                });
            }

        }

        $stores_data = $stores->orderByName()->paginate($perPageLimit);
        return response()->json(['success' => true, 'status' => 200, 'stores' => $stores_data]);
    }


    public function getStoreLists()
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $stores =  $skew_manager->assigned_managing_stores()
            ->select('stores.id', 'stores.name', 'stores.created_at', 'stores.updated_at')
            ->orderByName()->get();
        return response()->json([
            'success' => true,
             'status' => 200,
            'stores' => $stores
        ]);
    }

   public function store(Request $request)
   {

       $validator =  Validator::make($request->all(), [
           'name' => 'required|max:255|unique:stores',
           'name_furigana' => 'required|max:255',
           'store_category' => 'required',
           'country' => 'required',
           'state' => 'required',
           'city' => 'required',
           'address_line_1' => 'required|max:255',
           'address_line_2' => 'required|max:255',
           'zip_code' => 'required|numeric',
           'primary_contact_number' => 'required',
           'primary_email' => 'required|email',
           'secondary_email' => 'email',
           'description' => 'required',
           'latitude' => 'required',
           'longitude' => 'required',
           'status' => 'required',
       ], [], $attributes = $this->localize_validation_form_elements());

       if($validator->fails())
       {
           return response()->json([
                'status' => 200,
               'status_message' => 'validation_error',
               'errors' => $validator->errors(),
               'message' =>$this->getMessageText('validation_failed')
           ]);

       }
       else
       {
           $skew_manager = Auth::guard('skew_user')->user();
           $store_last_id = Store::max('id');
           $random1 = Str::random(10);
           $random2 = Str::random(12);
           $skew_client_id = "Skew_".$random1.($store_last_id + 1);
           $skew_client_secret = "Skew_Secret".($store_last_id + 1).$random2;

           $store =  new Store();
           $store->name = $request->name;
           $store->name_furigana = $request->name_furigana;
           $store->slug = $this->createSlug($request->name);
           $store->skew_user_id = $skew_manager->id;
           $store->store_category_id = $request->store_category;
           $store->country_id = $request->country;
           $store->state_id = $request->state;
           $store->city_id = $request->city;
           $store->address_line_1 = $request->address_line_1;
           $store->address_line_2 = $request->address_line_2;
           $store->zip_code = $request->zip_code;
           $store->latitude = $request->latitude;
           $store->longitude = $request->longitude;
           $store->status = $request->status;
           $store->skew_client_id = $skew_client_id;
           $store->skew_client_secret = $skew_client_secret;
           $store->save();
           if($store)
           {
               $store_detail = new StoreDetail();
               $store_detail->store_id = $store->id;
               $store_detail->primary_contact_number = $request->primary_contact_number;
               $store_detail->secondary_contact_number = $request->secondary_contact_number;
               $store_detail->primary_email = $request->primary_email;
               $store_detail->secondary_email = $request->secondary_email;
               $store_detail->description = $request->description;
               $store_detail->website_url = $request->website_url;
               $store_detail->save();

               DB::table('skew_user_store_managers')->insert([
                   'store_id' => $store->id,
                   'skew_user_id' =>  $skew_manager->id,
                   'assigned_by_manager_id' =>  $skew_manager->id,
                   'created_at' =>  date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())),
                   'updated_at' =>  date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now())),
               ]);

               if($store_detail)
               {
                   return response()->json([
                        'status' => 200,
                       'status_message' => 'success',
                       'message' => $this->getMessageText('add_success_message'),
                   ]);
               }
               else
               {
                   return response()->json([
                        'status' => 200,
                       'status_message' => 'failed',
                       'message' => $this->getMessageText('failed_message'),
                   ]);
               }

           }
           else
           {
               return response()->json([
                    'status' => 200,
                   'status_message' => 'failed',
                   'message' => $this->getMessageText('failed_message'),
               ]);
           }
       }
   }
    public function edit($id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->with('city', 'state', 'country', 'store_category', 'store_detail', 'approved_by', 'store_owner')
            ->where('stores.id', $id)
            ->orderByName()->first();
        return response()->json([
            'success' => true,
             'status' => 200,
            'store' => $store
        ]);
    }
    public function update(Request $request, $id)
    {

        if($request->country)
        {
            $country = Country::select('iso')->where('id', $request->country)->first();
            $country_iso = $country->iso;
        }
        else
        {
            $country_iso = env('COUNTRY_ISO');
        }
        $validator =  Validator::make($request->all(), [
            'name'=>'required|max:255|unique:stores,name,'.$id,
            'name_furigana' => 'required|max:255',
            'store_category' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address_line_1' => 'required|max:255',
            'address_line_2' => 'required|max:255',
            'zip_code' => 'required|numeric',
            'primary_contact_number' => [
                'required',
                Rule::phone()->detect()->country($country_iso),
            ],
            'secondary_contact_number' => [
                Rule::phone()->detect()->country($country_iso),
            ],
            'primary_email' => 'required|email',
            'secondary_email' => 'email',
            'description' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'status' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' =>$this->getMessageText('validation_failed')
            ]);

        }
        else {
            $skew_manager = Auth::guard('skew_user')->user();
            $store = $skew_manager->assigned_managing_stores()
                ->where('stores.id', $id)
                ->orderByName()->first();
            if($store)
            {
            $store->name = $request->name;
            $store->name_furigana = $request->name_furigana;
            $store->slug = $this->createSlug($request->name);
            $store->skew_user_id = $skew_manager->id;
            $store->store_category_id = $request->store_category;
            $store->country_id = $request->country;
            $store->state_id = $request->state;
            $store->city_id = $request->city;
            $store->address_line_1 = $request->address_line_1;
            $store->address_line_2 = $request->address_line_2;
            $store->zip_code = $request->zip_code;
            $store->latitude = $request->latitude;
            $store->longitude = $request->longitude;
            $store->status = $request->status;
            $store->save();
            if ($store) {
                $store_detail = $store->store_detail;
                $store_detail->primary_contact_number = $request->primary_contact_number;
                $store_detail->secondary_contact_number = $request->secondary_contact_number;
                $store_detail->primary_email = $request->primary_email;
                $store_detail->secondary_email = $request->secondary_email;
                $store_detail->description = $request->description;
                $store_detail->website_url = $request->website_url;
                $store_detail->save();


                if ($store_detail) {
                    return response()->json([
                         'status' => 200,
                        'status_message' => 'success',
                        'message' => $this->getMessageText('edit_success_message'),
                    ]);
                } else {
                    return response()->json([
                         'status' => 200,
                        'status_message' => 'failed',
                        'message' => $this->getMessageText('failed_message'),
                    ]);
                }

            } else {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
        else
        {
            return response()->json([
                'status' => 200,
                'success' => false,
                'status_message' => 'failed',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

        }
    }

    public function updateBufferTime(Request $request, $id)
    {

        $validator =  Validator::make($request->all(), [
            'buffer_time_before' => 'required|numeric|min:0|max:30',
            'buffer_time_after' => 'required|numeric|min:0|max:30',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' =>$this->getMessageText('validation_failed')
            ]);

        }
        else {
            $skew_manager = Auth::guard('skew_user')->user();
            $store = $skew_manager->assigned_managing_stores()
                ->where('stores.id', $id)
                ->orderByName()->first();
            if($store)
            {
                   $store_detail = $store->store_detail;
                    $store_detail->buffer_time_before = $request->buffer_time_before;
                    $store_detail->buffer_time_after = $request->buffer_time_after;
                    $store_detail->save();

                    if ($store_detail) {

                        $admin_action = new AdminAction();
                        $admin_action->skew_user_id = $skew_manager->id;
                        $admin_action->source_id =$store->id;
                        $admin_action->model_class ="App\Models\Stores\Store";
                        $admin_action->action_type ="update";
                        $admin_action->action =$this->getMessageText('store_model')." ".$this->getMessageText('edit_success_message');
                        $admin_action->store_id =$store->id;
                        $admin_action->save();

                        return response()->json([
                            'status' => 200,
                            'success' => true,
                            'status_message' => 'success',
                            'message' => $this->getMessageText('edit_success_message'),
                        ]);
                    } else {
                        return response()->json([
                            'status' => 200,
                            'success' => false,
                            'status_message' => 'failed',
                            'message' => $this->getMessageText('failed_message'),
                        ]);
                    }
            }
            else
            {
                return response()->json([
                    'status' => 200,
                    'success' => false,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('permission_denied'),
                ]);
            }
        }
    }
    public function show($id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->with('city', 'state', 'country', 'store_category', 'store_detail', 'approved_by', 'store_owner')
            ->where('stores.id', $id)
            ->orderByName()->first();
        return response()->json([
            'success' => true,
             'status' => 200,
            'store' => $store
        ]);
    }
}
