<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Stores\Entry;
use App\Models\Stores\Store;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class EntryController extends Controller
{
    use HelperTrait;
    public function index(Request $request, $store_id, $room_id)
    {
        if(isset($request->limit) && $request->limit) $perPage = $request->limit; else $perPage=null;
        $perPageLimit = $this->perPage($perPage);
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        $room = $store->rooms()->where('rooms.id', $store_id)->first();

        $entries = $room->entries()->paginate($perPageLimit);

        return response()->json([
            'success' => true,
             'status' => 200,
            'entries' => $entries
        ]);
    }

    public function store(Request $request, $store_id, $room_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        $room = $store->rooms()->active()->where('rooms.id', $room_id)->first();

        if(!$store || !$room)
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'failed',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }


        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'name' => Rule::unique('entries')->where(function ($query) use ($room_id) {
                return $query->where('room_id', $room_id);
            }),
            'service' => 'required',
            'lock_device_id' => 'required|unique:entries',
            'description' => 'required',
            'status' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {

            $entry_last_id = Entry::max('id');
            $random1 = Str::random(4);
            $entry_unique_id = "Entry_".($entry_last_id + 1).$random1;

            $entry =  new Entry();
            $entry->name = $request->name;
            $entry->service_id = $request->service;
            $entry->lock_device_id = $request->lock_device_id;
            $entry->description = $request->description;
            $entry->entry_unique_id = $entry_unique_id;
            $entry->room_id = $room->id;
            $entry->skew_user_id = $skew_manager->id;
            $entry->save();
            if($entry)
            {

                return response()->json([
                     'status' => 200,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('add_success_message'),
                ]);
            }
            else
            {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
    }

    public function edit(Request $request, $store_id, $room_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();
        $room =  $store->rooms()
            ->where('rooms.id', $room_id)
            ->orderByName()->active()->first();


        $amenities = Amenity::active()->get()->map(function($amenity) use ($room) {
            $amenity->price_per_hour = data_get($room->amenities->firstWhere('id', $amenity->id), 'pivot.price_per_hour') ?? null;
            $amenity->member_price_status = data_get($room->amenities->firstWhere('id', $amenity->id), 'pivot.member_price_status') ?? null;
            return $amenity;
        });

        $selected_amenities_id = $room->amenities()->pluck('amenities.id')->toArray();

        return response()->json([
            'success' => true,
             'status' => 200,
            'room' => $room,
            'amenities' => $amenities,
            'selected_amenities_id' => $selected_amenities_id,

        ]);
    }

    public function update(Request $request, $store_id, $room_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'failed',
                'message' => $this->getMessageText('failed_message'),
            ]);
        }


        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'name' => Rule::unique('rooms')->ignore($room_id)->where(function ($query) use ($store_id, $room_id) {
                return $query->where('store_id', $store_id);
            }),
            'min_capacity' => 'required|numeric|min:0',
            'max_capacity' => 'required|numeric|min:0',
            'area_in_square_feet' => 'required|numeric|min:0',
            'base_price_per_hour' => 'required|numeric|min:0',
            'description' => 'required',
            'status' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('failed_message')
            ]);

        }
        else
        {


            $room =  Room::findOrFail($room_id);
            $room->name = $request->name;
            $room->min_capacity = $request->min_capacity;
            $room->max_capacity = $request->max_capacity;
            $room->area_in_square_feet = $request->area_in_square_feet;
            $room->base_price_per_hour = $request->base_price_per_hour;
            $room->description = $request->description;
            $room->status = $request->status;
            $room->store_id = $store->id;
            $room->skew_user_id = $skew_manager->id;
            $room->save();
            if($room)
            {
                if(count($request->amenities))
                {
                    $sync_data = [];
                    for($i = 0; $i < count($request->amenities); $i++)
                    {
                        if(isset($request->amenities[$i]['id'])) $sync_data[$request->amenities[$i]['id']] = ['price_per_hour' => $request->amenities[$i]['price_per_hour'], 'member_price_status' => $request->amenities[$i]['member_price_status']];

                    }
                    $room->amenities()->sync($sync_data);
                }

                return response()->json([
                     'status' => 200,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('edit_success_message'),
                ]);
            }
            else
            {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
    }

    public function show(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();
        $room =  $store->rooms()
            ->where('rooms.id', $room_id)
            ->with('entries', 'amenities')
            ->orderByName()->active()->first();

        return response()->json([
            'success' => true,
             'status' => 200,
            'room' => $room,
        ]);
    }
}
