<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Bookings\BookingHistory;
use App\Models\Stores\Entry;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SkewScannerLoginController extends Controller
{
    use HelperTrait;
    public function login(Request $request)
    {

        $validator =  Validator::make($request->all(), [
            'user_name' => 'required',
            'password' => 'required',
            'entry_unique_id' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());



        if($validator->fails())
        {
            return response()->json([
                'status' => 400,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        $email =$request->user_name."@skewplus.com";
        $skew_manager = SkewUser::where('email',$email)->first();
        if(!$skew_manager){
            return response()->json([
                'success' => false,
                'status' => 400,
                'message' => $this->getMessageText('user_not_exist')
            ]);
        }

        $entry = Entry::where('entry_unique_id', '=', $request->entry_unique_id)->active()->verified()->first();

        if(!$entry)
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('entry_unique_id_not_match')
            ]);
        }

        $remember_me = false;
        if($request->remember_me) $remember_me = true;
        if(Auth::guard('skew_user_auth')->attempt(['email'=>$email,'password'=>$request->password], $remember_me))
        {
                  return response()->json([
                    'success' => true,
                    'status' => 200,
                    'user_id'=> auth()->guard('skew_user_auth')->user()->id,
                    'first_name'=> auth()->guard('skew_user_auth')->user()->first_name,
                    'access_token' => auth()->guard('skew_user_auth')->user()->createToken('Bearer')->accessToken,
                      'entry_unique_id' => $entry->entry_unique_id,
                      'room' => $entry->room->name,
                ]);


        }
        else
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('credentials_not_match')
            ]);
        }
    }

    public function scanQrCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'entry_unique_id' => 'required',
            'qr_code' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if ($validator->fails()) {
            return response()->json([
                'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed'),
                'sub_title' => $this->getMessageText('retry_scan')
            ]);
        }

        $booking = Booking::where('qr_code', '=', $request->qr_code)->valid()->first();
       $entry = Entry::where('entry_unique_id', '=', $request->entry_unique_id)->active()->verified()->first();
        if(!$booking)
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('qr_code_not_match'),
            ]);
        }

        if(!$entry)
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('failed_message'),
            ]);
        }

        $scanned_room = $entry->room;
        $booked_room = $booking->room;
        $booked_store =$booked_room->store;
        $booked_store_detail = $booked_store->store_detail;

        if($scanned_room->id != $booked_room->id)
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('wrong_room_scanned')." ".$booked_room->name,
            ]);
        }

        $buffer_time_before = $booked_store_detail->buffer_time_before * 60; // 10 Min buffer time in Seconds
        $buffer_time_after = $booked_store_detail->buffer_time_after * 60; // 10 Min buffer time in Seconds
        $current_time = date('Y-m-d H:i:s', strtotime(\Carbon\Carbon::now()));
        $booking_from = date('Y-m-d H:i:s', strtotime($booking->booking_from) - $buffer_time_before); // Add slot range hour
        $booking_to = date('Y-m-d H:i:s', strtotime($booking->booking_to) + $buffer_time_after); // Add slot range hour

        if(strtotime($booking_from) > strtotime($current_time))
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('early_scan')." ".$booking->booking_from,
            ]);
        }

        if(strtotime($booking_to) < strtotime($current_time))
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('late_scan')." ".$booking->booking_to,
            ]);
        }

      $history="Member QR";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('SESAME_PYTHON_API_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'
            {
    "uuid": "'.$entry->lock_device_id.'",
     "secret_key": "'.$entry->lock_secret_key.'",
     "api_key": "'.env('SESAME_LOCK_API_KEY').'",
    "cmd": 83,
     "history": "'.$history.'"
}',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $sesame_status = $info['http_code'];
        $res = json_decode($response, true);
        curl_close($curl);

      if($sesame_status==200 && $res['message']=="Success")
        {
            $booking_history = new BookingHistory();
            $booking_history->booking_id = $booking->id;
            $booking_history->skew_user_id = $booking->skew_user_id;
            $booking_history->entered_at = date('Y-m-d H:i:s', strtotime( \Carbon\Carbon::now()));
            $booking_history->save();

            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => $this->getMessageText('scan_success'),
                'sub_message' => $this->getMessageText('door_opened'),
            ]);

        }
        else
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('retry_scan'),
                'sub_message' => $this->getMessageText('failed_message'),
            ]);
        }


    }
}
