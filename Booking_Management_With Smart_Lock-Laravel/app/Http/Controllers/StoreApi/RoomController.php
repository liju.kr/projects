<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\BookingHistory;
use App\Models\Settings\AdminAction;
use App\Models\Settings\Amenity;
use App\Models\Stores\Room;
use App\Models\Stores\Store;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class RoomController extends Controller
{
    use HelperTrait;
    public function index(Request $request, $store_id)
    {
        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id = $skew_manager->assigned_managing_stores()
            ->pluck('stores.id');

        $rooms = Room::whereIn('store_id', $stores_id)->where('store_id', $store_id);
        $rooms->select('id', 'name', 'status', 'created_at', 'updated_at', 'is_verified');
        if (isset($request->keyword) && $request->keyword)
        {
            $keyword = $request->keyword;

            $rooms->where(function ($query) use ($keyword)
                {
                    $query->orWhere('name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('entries', function ($query) use ($keyword)
                    {
                        $query->where('entries.entry_unique_id', 'like', '%' . $keyword . '%');
                        $query->orWhere('entries.lock_device_id', 'like', '%' . $keyword . '%');
                        $query->orWhere('entries.lock_secret_key', 'like', '%' . $keyword . '%');
                    });

                });
        }

        $rooms_data = $rooms->orderByName()->paginate($perPageLimit);
        return response()->json(['success' => true, 'status' => 200, 'rooms' => $rooms_data, 'secret_key_scan_url' => env('SESAME_SECRET_URL')]);
    }

    public function getRoomLists($store_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()->where('stores.id', $store_id)->first();
        if(!$store)
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'rooms' => []
            ]);
        }

        $rooms = $store->rooms()->select('rooms.id', 'rooms.name', 'rooms.created_at', 'rooms.updated_at')->orderByName()->get();

        foreach ($rooms as $key=>$room)
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://app.candyhouse.co/api/sesame2/'.$room->lock_device_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: '.env('SESAME_LOCK_API_KEY'),
                    'Accept: application/json'
                ),
            ));
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $sesame_status = $info['http_code'];
            curl_close($curl);
            $res = json_decode($response, true);
            if($sesame_status ==200)
            {
                $rooms[$key]['batteryPercentage'] = $res['batteryPercentage'];
                $rooms[$key]['CHSesame2Status'] = $res['CHSesame2Status'];
            }
            else
            {
                $rooms[$key]['batteryPercentage'] = null;
                $rooms[$key]['CHSesame2Status'] = null;
            }
        }

        return response()->json([
            'success' => true,
            'status' => 200,
            'rooms' => $rooms
        ]);

    }


    public function store(Request $request, $store_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
                               ->where('stores.id', $store_id)
                               ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'failed',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }


        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'name' => Rule::unique('rooms')->where(function ($query) use ($store_id) {
                return $query->where('store_id', $store_id);
            }),
            'min_capacity' => 'required|numeric|min:0',
            'max_capacity' => 'required|numeric|min:0',
            'area_in_square_feet' => 'required|numeric|min:0',
            'base_price_per_hour' => 'required|numeric|min:0',
            'description' => 'required',
            'status' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {


            $room =  new Room();
            $room->name = $request->name;
            $room->min_capacity = $request->min_capacity;
            $room->max_capacity = $request->max_capacity;
            $room->area_in_square_feet = $request->area_in_square_feet;
            $room->base_price_per_hour = $request->base_price_per_hour;
            $room->description = $request->description;
            $room->status = $request->status;
            $room->store_id = $store->id;
            $room->skew_user_id = $skew_manager->id;
            $room->save();
            if($room)
            {
                if(count($request->amenities))
                {
                    $sync_data = [];
                    for($i = 0; $i < count($request->amenities); $i++)
                    {
                        if(isset($request->amenities[$i]['id'])) $sync_data[$request->amenities[$i]['id']] = ['price_per_hour' => $request->amenities[$i]['price_per_hour'], 'member_price_status' => $request->amenities[$i]['member_price_status']];

                    }
                    $room->amenities()->attach($sync_data);
                }

                return response()->json([
                         'status' => 200,
                        'status_message' => 'success',
                        'message' => $this->getMessageText('add_success_message'),
                    ]);
            }
            else
            {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
    }

    public function edit(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();
        $room =  $store->rooms()
            ->where('rooms.id', $room_id)
            ->orderByName()->active()->first();


        $amenities = Amenity::active()->get()->map(function($amenity) use ($room) {
            $amenity->price_per_hour = data_get($room->amenities->firstWhere('id', $amenity->id), 'pivot.price_per_hour') ?? null;
            $amenity->member_price_status = data_get($room->amenities->firstWhere('id', $amenity->id), 'pivot.member_price_status') ?? null;
            return $amenity;
        });

        $selected_amenities_id = $room->amenities()->pluck('amenities.id')->toArray();

        return response()->json([
            'success' => true,
             'status' => 200,
            'room' => $room,
            'amenities' => $amenities,
            'selected_amenities_id' => $selected_amenities_id,

        ]);
    }

    public function update(Request $request, $store_id, $room_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'failed',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }


        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'name' => Rule::unique('rooms')->ignore($room_id)->where(function ($query) use ($store_id, $room_id) {
                return $query->where('store_id', $store_id);
            }),
            'min_capacity' => 'required|numeric|min:0',
            'max_capacity' => 'required|numeric|min:0',
            'area_in_square_feet' => 'required|numeric|min:0',
            'base_price_per_hour' => 'required|numeric|min:0',
            'description' => 'required',
            'status' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {


            $room =  Room::findOrFail($room_id);
            $room->name = $request->name;
            $room->min_capacity = $request->min_capacity;
            $room->max_capacity = $request->max_capacity;
            $room->area_in_square_feet = $request->area_in_square_feet;
            $room->base_price_per_hour = $request->base_price_per_hour;
            $room->description = $request->description;
            $room->status = $request->status;
            $room->store_id = $store->id;
            $room->skew_user_id = $skew_manager->id;
            $room->save();
            if($room)
            {
                if(count($request->amenities))
                {
                    $sync_data = [];
                    for($i = 0; $i < count($request->amenities); $i++)
                    {
                        if(isset($request->amenities[$i]['id'])) $sync_data[$request->amenities[$i]['id']] = ['price_per_hour' => $request->amenities[$i]['price_per_hour'], 'member_price_status' => $request->amenities[$i]['member_price_status']];

                    }
                    $room->amenities()->sync($sync_data);
                }

                return response()->json([
                     'status' => 200,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('edit_success_message'),
                ]);
            }
            else
            {
                return response()->json([
                     'status' => 200,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
        }
    }

    public function updateLock(Request $request, $store_id, $room_id)
    {

        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'failed',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }


        $validator =  Validator::make($request->all(), [
            'lock_device_id' => 'required',
            'lock_secret_key' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {

            $room =  $store->rooms()->where('rooms.id', $room_id)->first();
            if($room)
            {
               $entry = $room->entries()->where('entries.room_id', $room->id)->first();

               if(!$entry)
               {
                   return response()->json([
                       'status' => 200,
                       'success'=>false,
                       'status_message' => 'failed',
                       'message' => $this->getMessageText('failed_message'),
                   ]);
               }

                $history="Manager Update Key";
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => env('SESAME_PYTHON_API_URL'),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>'
            {
    "uuid": "'.$request->lock_device_id.'",
     "secret_key": "'.$request->lock_secret_key.'",
     "api_key": "'.env('SESAME_LOCK_API_KEY').'",
    "cmd": 83,
     "history": "'.$history.'"
}',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));
                $response = curl_exec($curl);
                $info = curl_getinfo($curl);
                $sesame_status = $info['http_code'];
                $res = json_decode($response, true);
                curl_close($curl);

                if($sesame_status==200 && $res['message']=="Success")
                {
                    $entry->lock_device_id = $request->lock_device_id;
                    $entry->lock_secret_key = $request->lock_secret_key;
                    $entry->is_verified = 1;
                    $entry->save();

                    $room->is_verified = 1;
                    $room->save();

                    $admin_action = new AdminAction();
                    $admin_action->skew_user_id = $skew_manager->id;
                    $admin_action->source_id =$room->id;
                    $admin_action->model_class ="App\Models\Stores\Room";
                    $admin_action->action_type ="update";
                    $admin_action->action =$this->getMessageText('room_model')." ".$this->getMessageText('edit_success_message');
                    $admin_action->store_id =$store->id;
                    $admin_action->save();


                }
                else
                {
                    return response()->json([
                        'success' => false,
                        'status' => 200,
                        'status_message' => 'lock_validation_failed',
                        'message' => $this->getMessageText('lock_validation_failed')
                    ]);
                }


                return response()->json([
                    'status' => 200,
                    'success'=>true,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('lock_validation_success'),
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 200,
                    'success'=>false,
                    'status_message' => 'permission_denied',
                    'message' => $this->getMessageText('permission_denied'),
                ]);
            }
        }
    }

    public function unLockRoom(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

        $room =  $store->rooms()->where('rooms.id', $room_id)->first();
        if($room)
        {
            $entry = $room->entries()->where('entries.room_id', $room->id)->first();

            if(!$entry)
            {
                return response()->json([
                    'status' => 200,
                    'success'=>false,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }

            $history="Manager UnLock";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('SESAME_PYTHON_API_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'
            {
    "uuid": "'.$entry->lock_device_id.'",
     "secret_key": "'.$entry->lock_secret_key.'",
     "api_key": "'.env('SESAME_LOCK_API_KEY').'",
    "cmd": 83,
     "history": "'.$history.'"
}',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $sesame_status = $info['http_code'];
            $res = json_decode($response, true);
            curl_close($curl);

            if($sesame_status==200 && $res['message']=="Success")
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="unlock";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('unlock_success');
                $admin_action->store_id =$store->id;
                $admin_action->save();

                return response()->json([
                    'status' => 200,
                    'success'=>true,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('unlock_success'),
                ]);

            }
            else
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="unlock";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('unlock_failed');
                $admin_action->store_id =$store->id;
                $admin_action->save();
                return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => $this->getMessageText('lock_validation_failed')
                ]);
            }

        }
        else
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

    }

    public function lockRoom(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

        $room =  $store->rooms()->where('rooms.id', $room_id)->first();
        if($room)
        {
            $entry = $room->entries()->where('entries.room_id', $room->id)->first();

            if(!$entry)
            {
                return response()->json([
                    'status' => 200,
                    'success'=>false,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }

            $history="Manager Lock";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('SESAME_PYTHON_API_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'
            {
    "uuid": "'.$entry->lock_device_id.'",
     "secret_key": "'.$entry->lock_secret_key.'",
     "api_key": "'.env('SESAME_LOCK_API_KEY').'",
    "cmd": 82,
     "history": "'.$history.'"
}',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $sesame_status = $info['http_code'];
            $res = json_decode($response, true);
            curl_close($curl);

            if($sesame_status==200 && $res['message']=="Success")
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="lock";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('lock_success');
                $admin_action->store_id =$store->id;
                $admin_action->save();

                return response()->json([
                    'status' => 200,
                    'success'=>true,
                    'status_message' => 'success',
                    'message' => $this->getMessageText('lock_success'),
                ]);

            }
            else
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="lock";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('lock_failed');
                $admin_action->store_id =$store->id;
                $admin_action->save();
                return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => $this->getMessageText('lock_validation_failed')
                ]);
            }

        }
        else
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

    }

    public function getBatteryStatus(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();

        if(!$store)
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

        $room =  $store->rooms()->where('rooms.id', $room_id)->first();
        if($room)
        {
            $entry = $room->entries()->where('entries.room_id', $room->id)->first();

            if(!$entry)
            {
                return response()->json([
                    'status' => 200,
                    'success'=>false,
                    'status_message' => 'failed',
                    'message' => $this->getMessageText('failed_message'),
                ]);
            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://app.candyhouse.co/api/sesame2/'.$entry->lock_device_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: '.env('SESAME_LOCK_API_KEY'),
                    'Accept: application/json'
                ),
            ));
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $sesame_status = $info['http_code'];
            curl_close($curl);
            $res = json_decode($response, true);
            if($sesame_status ==200)
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="battery_check";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('battery_check_success');
                $admin_action->store_id =$store->id;
                $admin_action->save();
               return response()->json([
                    'status' => 200,
                    'success'=>true,
                    'status_message' => 'success',
                    'batteryPercentage' =>  $res['batteryPercentage'],
                    'CHSesame2Status' =>  $res['CHSesame2Status'],
                    'message' =>$this->getMessageText('battery_check_success')
                ]);

            }
            else
            {
                $admin_action = new AdminAction();
                $admin_action->skew_user_id = $skew_manager->id;
                $admin_action->source_id =$room->id;
                $admin_action->model_class ="App\Models\Stores\Room";
                $admin_action->action_type ="battery_check";
                $admin_action->action =$room->name." (".$store->name.") ".$this->getMessageText('battery_check_failed');
                $admin_action->store_id =$store->id;
                $admin_action->save();
                return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => $this->getMessageText('battery_check_failed')
                ]);
            }

        }
        else
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'permission_denied',
                'message' => $this->getMessageText('permission_denied'),
            ]);
        }

    }

    public function show(Request $request, $store_id, $room_id)
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        $store =  $skew_manager->assigned_managing_stores()
            ->where('stores.id', $store_id)
            ->orderByName()->first();
        $room =  $store->rooms()
            ->where('rooms.id', $room_id)
            ->with('entries', 'amenities')
            ->orderByName()->active()->first();

        return response()->json([
            'success' => true,
             'status' => 200,
            'room' => $room,
        ]);
    }
}
