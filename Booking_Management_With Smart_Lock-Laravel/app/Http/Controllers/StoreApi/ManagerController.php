<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;

class ManagerController extends Controller
{
    use HelperTrait;
    public function index(Request $request)
    {

        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        if($skew_manager->role !="store_owner")
        {
            return response()->json(['success' => true, 'status' => 200, 'managers' => []]);
        }
        $stores_id = $skew_manager->assigned_managing_stores()->whereIn('stores.id', json_decode($request->stores_id))->pluck('stores.id');

        $managers = SkewUser::storeManager();
        $managers->whereHas('assigned_managing_stores', function ($query) use ($stores_id) {
            $query->whereIn('stores.id', $stores_id);
        });
        $managers->select('id', 'first_name', 'email', 'status', 'created_at', 'updated_at', 'is_store_manager');


        if (isset($request->keyword) && $request->keyword) {
            $keyword = $request->keyword;

            $managers->where(function ($query) use ($keyword) {
                    $query->orWhere('first_name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('assigned_managing_stores', function ($query) use ($keyword) {
                        $query->where('stores.name', 'like', '%' . $keyword . '%');
                    });
                    $query->orWhere('email', 'like', '%' . $keyword . '%');
                });
        }
        $managers_data = $managers->orderByName()->paginate($perPageLimit);
        return response()->json(['success' => true, 'status' => 200, 'managers' => $managers_data]);
    }
    public function getManagerProfile(Request $request)
    {
       $skew_manager = Auth::guard('skew_user')->user();
        return response()->json(['success' => true, 'status' => 200, 'manager_profile' => $skew_manager]);
    }
    public function changeManagerProfilePic(Request $request)
    {
        $skew_manager = Auth::guard('skew_user')->user();

        $validator =  Validator::make($request->all(), [
            'profile_picture' => 'required|mimes:jpeg,jpg,png,gif|max:1024',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                'status' => 200,
                'success'=>false,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
        else
        {
            $skew_user_profile = $skew_manager->skew_user_profile;
            $file_name = null;
            if ($request->hasfile('profile_picture'))
            {
                $file = $request->file('profile_picture');
                $file_name = "profile_pic-".time().'.'. $file->extension();
                $img = Image::make($file);
                if(Storage::disk('public')->put('uploads/manager/profile/' . $file_name, $img->stream()->__toString() , ['visibility' => 'public']))
                {
                    if (Storage::disk('public')->exists('uploads/manager/profile/' . $skew_user_profile->image)) {
                        Storage::disk('public')->delete('uploads/manager/profile/' . $skew_user_profile->image);
                    }
                    $skew_user_profile->image = $file_name;
                }

            }
        }
        $skew_user_profile->save();
        $skew_manager = Auth::guard('skew_user')->user();
        return response()->json(['success' => true, 'status' => 200, 'manager_profile' => $skew_manager]);
    }

    public function getManagerDetail(Request $request)
    {
        $skew_manager = SkewUser::where('id', $request->manager_id)->first();
        $auth_manager = Auth::guard('skew_user')->user();
        if($auth_manager->role !="store_owner")
        {
            return response()->json(['success' => true, 'status' => 200, 'manager_detail' => []]);
        }
        $stores_id = $skew_manager->assigned_managing_stores()->pluck('stores.id');
        $check = DB::table('skew_user_store_managers')->where('skew_user_id', $skew_manager->id)->whereIn('store_id', $stores_id)->count();
        if($check) $manager_detail =$skew_manager; else $manager_detail = [];
        return response()->json(['success' => true, 'status' => 200, 'manager_detail' => $manager_detail]);
    }

    public function deleteManagerProfilePic(Request $request)
    {
        $skew_manager = Auth::guard('skew_user')->user();
        $skew_user_profile = $skew_manager->skew_user_profile;
        if (Storage::disk('public')->exists('uploads/manager/profile/' . $skew_user_profile->image)) {
            Storage::disk('public')->delete('uploads/manager/profile/' . $skew_user_profile->image);
            $file_name = null;
            $skew_user_profile->image = $file_name;
            $skew_user_profile->save();
        }

        $skew_manager = Auth::guard('skew_user')->user();
        return response()->json(['success' => true, 'status' => 200, 'manager_profile' => $skew_manager]);

    }
}
