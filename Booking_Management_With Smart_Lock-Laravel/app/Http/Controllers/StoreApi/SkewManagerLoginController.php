<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserToken;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SkewManagerLoginController extends Controller
{
    use HelperTrait;
    public function login(Request $request)
    {

        $validator =  Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());



        if($validator->fails())
        {
            return response()->json([
                 'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);

        }
       $skew_manager = SkewUser::where('email',$request->email)->first();
        if(!$skew_manager){
            return response()->json([
                'success' => false,
                 'status' => 200,
                'message' => $this->getMessageText('user_not_exist')
            ]);
        }
        $remember_me = false;
        if($request->remember_me) $remember_me = true;
        if(Auth::guard('skew_user_auth')->attempt(['email'=>$request->email,'password'=>$request->password], $remember_me)){
            return response()->json([
                'success' => true,
                 'status' => 200,
                'user_id'=> auth()->guard('skew_user_auth')->user()->id,
                'first_name'=> auth()->guard('skew_user_auth')->user()->first_name,
                'profile_image'=> auth()->guard('skew_user_auth')->user()->profile_image,
                'default_profile_image'=> asset('assets/images/profile/profile_pic.jpg'),
                'role'=> auth()->guard('skew_user_auth')->user()->role,
                'access_token' => auth()->guard('skew_user_auth')->user()->createToken('Bearer')->accessToken
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                 'status' => 200,
                'message' => $this->getMessageText('credentials_not_match')
            ]);
        }
    }

    public function loginWithToken(Request $request)
    {

        $validator =  Validator::make($request->all(), [
            'token' => 'required',
        ], [], $attributes = $this->localize_validation_form_elements());

        if($validator->fails())
        {
            return response()->json([
                'status' => 200,
                'status_message' => 'validation_error',
                'errors' => $validator->errors(),
                'message' => $this->getMessageText('validation_failed')
            ]);
        }
        $skew_user_token = SkewUserToken::where('login_session_token', $request->token)->first();
        if(!$skew_user_token){
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('permission_denied')
            ]);
        }

        $skew_manager = SkewUser::where('id',$skew_user_token->skew_user_id)->first();
        if(!$skew_manager){
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('user_not_exist')
            ]);
        }
        $remember_me = false;
        if(Auth::guard('skew_user_auth')->loginUsingId($skew_manager->id)){

            $skew_user_token->login_session_token = null;
            $skew_user_token->save();

            return response()->json([
                'success' => true,
                'status' => 200,
                'user_id'=> auth()->guard('skew_user_auth')->user()->id,
                'first_name'=> auth()->guard('skew_user_auth')->user()->first_name,
                'profile_image'=> auth()->guard('skew_user_auth')->user()->profile_image,
                'default_profile_image'=> asset('assets/images/profile/profile_pic.jpg'),
                'role'=> auth()->guard('skew_user_auth')->user()->role,
                'access_token' => auth()->guard('skew_user_auth')->user()->createToken('Bearer')->accessToken
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                'status' => 200,
                'message' => $this->getMessageText('credentials_not_match')
            ]);
        }
    }





}
