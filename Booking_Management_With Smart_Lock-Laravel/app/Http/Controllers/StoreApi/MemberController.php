<?php
namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Bookings\BookingHistory;
use App\Models\Stores\Room;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    use HelperTrait;
    public function index(Request $request)
    {

        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id = $skew_manager->assigned_managing_stores()
            ->pluck('stores.id');

        $members = SkewUser::storeMember();
        $members->whereHas('assigned_member_stores', function ($query) use ($stores_id)
        {
            $query->whereIn('stores.id', $stores_id);
        });
        $members->select('id', 'first_name', 'email', 'mobile_number', 'status', 'created_at', 'updated_at', 'is_any_store_member');
        $members->withCount(['bookings as expired_not_visited_bookings' => function ($query)
        {
            $query->expiredNotVisited();
        }]);

        if (isset($request->keyword) && $request->keyword)
        {
            $keyword = $request->keyword;
            if(gettype($keyword)=="integer")
            {
                $members->having('expired_not_visited_bookings', '=', $keyword);
            }
            else
            {
                $members->where(function ($query) use ($keyword)
                {
                    $query->orWhere('first_name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('assigned_member_stores', function ($query) use ($keyword)
                    {
                        $query->where('stores.name', 'like', '%' . $keyword . '%');
                    });
                    $query->orWhere('email', 'like', '%' . $keyword . '%');
                    $query->orWhere('mobile_number', 'like', '%' . $keyword . '%');
                });
            }

        }

        if (isset($request->store_id) && $request->store_id)
        {
            $store_id = $request->store_id;
            $members->whereHas('assigned_member_stores', function ($query) use ($store_id)
            {
                $query->where('stores.id', $store_id);
            });
        }

        $members_data = $members->orderByName()->paginate($perPageLimit);
        return response()->json(['success' => true, 'status' => 200, 'members' => $members_data]);
    }
    public function memberUsageHistory(Request $request)
    {

        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();
        $stores_id = $skew_manager->assigned_managing_stores()->pluck('stores.id');
        $rooms_id = Room::whereIn('store_id', $stores_id)->pluck('id');
        $bookings = Booking::whereIn('room_id', $rooms_id)->where('skew_user_id', $request->member_id);

        if (isset($request->keyword) && $request->keyword)
        {
            $keyword = $request->keyword;
            $bookings->where(function ($query) use ($keyword)
            {
                $query->orWhere('booking_from', 'like', '%' . $keyword . '%');
                $query->orWhereHas('booking_histories', function ($query) use ($keyword)
                {
                    $query->where('booking_histories.entered_at', 'like', '%' . $keyword . '%');
                });
            });
    }
        $bookings_data = $bookings->paginate($perPageLimit);
        $expired_not_visited = $bookings->expiredNotVisited()->count();



        return response()->json(['success' => true, 'status' => 200, 'booking_data' => $bookings_data, 'expired_not_visited' => $expired_not_visited]);

    }

}

