<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Settings\AdminAction;
use App\Models\Users\SkewUser;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminActionController extends Controller
{
    use HelperTrait;
    public function index(Request $request)
    {
        if (isset($request->limit) && $request->limit) $perPage = $request->limit;
        else $perPage = null;
        $perPageLimit = $this->perPage($perPage);

        $skew_manager = Auth::guard('skew_user')->user();

        if($skew_manager->role != "store_owner")
        {
            return response()->json(['success' => true, 'status' => 200, 'admin_actions' => []]);
        }

        $store_ids = $skew_manager->assigned_managing_stores()->whereIn('stores.id', json_decode($request->stores_id))->pluck('stores.id');
        $admin_actions = AdminAction::latest();

        if(isset($request->date) && $request->date)
        {
            $date_from = date('Y-m-d 00:00:00', strtotime($request->date));
            $date_to = date('Y-m-d 23:59:59', strtotime($request->date));
            $admin_actions->where('created_at', '>=', $date_from);
            $admin_actions->where('created_at', '<=', $date_to);
        }



        if (isset($request->keyword) && $request->keyword) {
            $keyword = $request->keyword;
            $admin_actions->where(function ($query) use ($keyword) {
                $query->orWhere('action', 'like', '%' . $keyword . '%');
                $query->orWhere('created_at', 'like', '%' . $keyword . '%');
                $query->orWhereHas('store', function ($query) use ($keyword) {
                    $query->where('stores.name', 'like', '%' . $keyword . '%');
                    $query->orWhereHas('store_detail', function ($query) use ($keyword) {
                        $query->where('store_details.primary_contact_number', 'like', '%' . $keyword . '%');
                    });
                });
                $query->orWhereHas('action_by', function ($query) use ($keyword) {
                    $query->where('skew_users.first_name', 'like', '%' . $keyword . '%');
                    $query->orWhere('skew_users.email', 'like', '%' . $keyword . '%');
                });
            });
        }
        if(!empty($store_ids))
        {
            $admin_actions->whereIn('store_id', $store_ids)->select('id', 'source_id', 'created_at', 'action_type', 'action', 'store_id', 'skew_user_id');
            $admin_actions_data = $admin_actions->paginate($perPageLimit);
        }
       else
        {
            $admin_actions_data = [];
        }
        return response()->json(['success' => true, 'status' => 200, 'admin_actions' => $admin_actions_data]);
    }
}
