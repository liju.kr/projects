<?php

namespace App\Http\Controllers\StoreApi;

use App\Http\Controllers\Controller;
use App\Models\Settings\Locale;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    use HelperTrait;
    public function getStatusCodes()
    {
        $skew_manager =  Auth::guard('skew_user')->user();
        return response()->json([
            'success' => true,
            'status' => 200,
            'status_codes' => $this->localize_status_codes()
        ]);
    }
}
