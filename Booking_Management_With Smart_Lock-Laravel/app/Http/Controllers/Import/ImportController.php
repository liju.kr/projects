<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Models\Bookings\Booking;
use App\Models\Import\FungolMember;
use App\Models\Import\FungolReservation;
use App\Models\Users\SkewUser;
use App\Models\Users\SkewUserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ImportController extends Controller
{
   public function importManager()
   {
       return "Manager";
   }


    public function importMember()
    {

//        $fungol_members = FungolMember::get();
//
//
//        foreach ($fungol_members as $fungol_member)
//        {
//            if(!$fungol_member->deleted_at)
//            {
//                $skew_user = new SkewUser();
//                $skew_user->reference_id = $fungol_member->id;
//                $skew_user->first_name = $fungol_member->name;
//                $skew_user->first_name_furigana = $fungol_member->kana;
//                $skew_user->email = $fungol_member->email;
//                $skew_user->is_any_store_member = 1;
//                $skew_user->status = 1;
//                $skew_user->is_verified = 1;
//                $skew_user->created_at = date('Y-m-d H:i:s', strtotime($fungol_member->created_at));
//                $skew_user->updated_at = date('Y-m-d H:i:s', strtotime($fungol_member->updated_at));
//                $skew_user->save();
//
//
//                $profile = new SkewUserProfile();
//                $profile->skew_user_id = $skew_user->id;
//                $profile->created_at = date('Y-m-d H:i:s', strtotime($fungol_member->created_at));
//                $profile->updated_at = date('Y-m-d H:i:s', strtotime($fungol_member->updated_at));
//                $profile->save();
//
//
//                DB::table('skew_user_member_stores')->insert([
//                    'store_id' => 1,
//                    'skew_user_id' => $skew_user->id,
//                    'assigned_by_manager_id' => 1,
//                    'created_at' => date('Y-m-d H:i:s', strtotime( $fungol_member->created_at)),
//                    'updated_at' => date('Y-m-d H:i:s', strtotime( $fungol_member->updated_at)),
//                ]);
//            }
//
//        }

        return "success";
    }

    public function importReservations()
    {


//        $fungol_reservations = FungolReservation::where('id', '>', 6051)->get();
//
//
//        foreach ($fungol_reservations as $fungol_reservation)
//        {
//            if(!$fungol_reservation->deleted_at)
//            {
//
//                $booking_last_id = Booking::max('id');
//                if(!$booking_last_id) $booking_last_id=0;
//                $random1 = Str::random(12);
//                $qr_code = "Booking_" . ($booking_last_id + 1) . $random1;
//
//                $slot = 1*60*60;
//                $booking_from = date('Y-m-d H:i:s', strtotime($fungol_reservation->reserve_date)); // Back to string
//                $booking_to = date('Y-m-d H:i:s', strtotime($fungol_reservation->reserve_date) + $slot); // Add slot range hour
//
//                $member=SkewUser::where('reference_id', $fungol_reservation->member_id)->where('is_any_store_member', 1)->first();
//
//              if($member)
//              {
//                  $member_id=$member->id;
//
//                  $booking = new Booking();
//
//                  $booking->reference_id = $fungol_reservation->id;
//                  $booking->qr_code = $qr_code;
//                  $booking->remarks = $fungol_reservation->note;
//                  $booking->room_id = 1;
//                  $booking->skew_user_id = $member_id;
//                  if($fungol_reservation->manager_id)$booking->skew_manager_id = 1;
//                  $booking->booking_from = $booking_from;
//                  $booking->booking_to = $booking_to;
//                  if($fungol_reservation->cancel_date)$booking->cancelled_at = date('Y-m-d H:i:s', strtotime($fungol_reservation->cancel_date));
//                  $booking->created_at = date('Y-m-d H:i:s', strtotime($fungol_reservation->created_at));
//                  $booking->updated_at = date('Y-m-d H:i:s', strtotime($fungol_reservation->updated_at));
//                  $booking->save();
//
//              }
//
//
//
//
//            }
//
//        }

        return "success";
    }
}
