<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SkewUserHomeController extends Controller
{
    public function index()
    {
        return view('skew_user.home');
    }
}
