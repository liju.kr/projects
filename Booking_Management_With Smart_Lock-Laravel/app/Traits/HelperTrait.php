<?php
namespace App\Traits;

use App\Models\Settings\Locale;

trait HelperTrait
{
    public function createSlug($str){

        $delimiter = '-';
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

    public static function localize_data()
    {
        $lang       = app()->getLocale();
        $langs      = Locale::select('key','en','ja')->get()->toArray();
        $resp       = [];
        foreach($langs as $key => $lng ){
            $resp[$lng['key']] = $lang == 'en' ? $lng['en'] : $lng['ja'];
        }
        return $resp;
    }
    public static function localize_messages()
    {
        $lang       = app()->getLocale();
        $langs      = Locale::select('key','en','ja')->where('type', 'message')->get()->toArray();
        $resp       = [];
        foreach($langs as $key => $lng ){
            $resp[$lng['key']] = $lang == 'en' ? $lng['en'] : $lng['ja'];
        }
        return $resp;
    }

    public static function localize_status_codes()
    {
        $lang       = app()->getLocale();
        $langs      = Locale::select('key','en','ja')->where('type', 'status')->get()->toArray();
        $resp       = [];
        foreach($langs as $key => $lng ){
            $resp[$lng['key']] = $lang == 'en' ? $lng['en'] : $lng['ja'];
        }
        return $resp;
    }

    public  function getMessageText($message_key)
    {
        $lang       = app()->getLocale();
        $message_array = Locale::select('key','en','ja')->where('key',$message_key)->first();
        if($message_array)
        {
            $message =  $lang == 'en' ? $message_array['en'] : $message_array['ja'];
        }
        else
        {
            $message = $message_key;
        }
        return $message;

    }

    public static function localize_validation_form_elements()
    {
        $lang       = app()->getLocale();
        $langs      = Locale::select('key','en','ja')->where('type', 'form_element')->get()->toArray();
        $resp = [];
        foreach($langs as $key => $lng ){
            $resp[$lng['key']] = $lang == 'en' ? $lng['en'] : $lng['ja'];
        }
        return $resp;
    }

    public function perPage($perPage)
    {
        if (empty($perPage) || ($perPage > 100) || ($perPage == null))
        {
            $perPageLimit = 100;
        }
        else
        {
            $perPageLimit = $perPage;
        }
        return $perPageLimit;
    }

}
