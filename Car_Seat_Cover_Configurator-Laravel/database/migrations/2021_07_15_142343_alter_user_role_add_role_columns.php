<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserRoleAddRoleColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_roles', function (Blueprint $table) {
            $table->integer('can_view_report')->default(0)->after('view_configuration');
            $table->integer('can_view_report_stock')->default(0)->after('can_view_report');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_roles', function (Blueprint $table) {
           $table->dropColumn('can_view_report');
           $table->dropColumn('can_view_report_stock');
        });
    }
}
