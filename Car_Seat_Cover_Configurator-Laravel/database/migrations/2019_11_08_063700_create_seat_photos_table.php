<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('seat_id')->unsigned()->nullable();
            $table->foreign('seat_id')->references('id')
                ->on('seats')->onDelete('cascade');
            $table->text('image');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat_photos');
    }
}
