<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmbroideryConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('embroidery_configurations', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');


            $table->text('design_selected_image')->nullable();
            $table->longText('selections')->nullable();
            $table->longText('image_array')->nullable();
            $table->longText('materials')->nullable();
            $table->longText('vehicle_details')->nullable();

            $table->double('vat')->default(0);
            $table->double('quantity')->default(1);
            $table->double('price')->default(0);
            $table->double('total_price')->default(0);

            $table->integer('status')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('embroidery_configurations');
    }
}
