<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('sort')->after('name')->default(1);
            $table->string('tags')->after('sort')->nullable();
            $table->string('image')->after('tags')->nullable();
            $table->integer('status')->after('tags')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('sort');
            $table->dropColumn('tags');
            $table->dropColumn('image');
            $table->dropColumn('status');
        });
    }
}
