<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurations', function (Blueprint $table) {
           $table->double('base_price')->default(0);
           $table->double('design_addon')->default(0);
           $table->double('pattern_addon')->default(0);
           $table->integer('focus_price')->default(0);
           $table->double('total_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurations', function (Blueprint $table) {
            $table->dropColumn('base_price');
            $table->dropColumn('design_addon');
            $table->dropColumn('pattern_addon');
            $table->dropColumn('focus_price');
            $table->dropColumn('total_price');
        });
    }
}
