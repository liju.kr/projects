<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModelSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_model_seats', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('car_model_id')->unsigned()->nullable();
            $table->foreign('car_model_id')->references('id')
                ->on('car_models')->onDelete('cascade');

            $table->integer('seat_id')->unsigned()->nullable();
            $table->foreign('seat_id')->references('id')
                ->on('seats')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_model_seats');
    }
}
