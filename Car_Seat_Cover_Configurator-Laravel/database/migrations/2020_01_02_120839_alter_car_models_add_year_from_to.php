<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCarModelsAddYearFromTo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_models', function (Blueprint $table) {
            //
            $table->integer('year_from')->nullable();
            $table->integer('year_to')->nullable();
            $table->integer('old_seat_layout_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_models', function (Blueprint $table) {
            //
            $table->dropColumn(['year_from']);
            $table->dropColumn(['year_to']);
            $table->dropColumn(['old_seat_layout_id']);
        });
    }
}
