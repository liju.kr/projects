<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->integer('user_id')->nullable();
        });
        Schema::table('manufacturers', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });
        Schema::table('car_models', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });
        Schema::table('layouts', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });
        Schema::table('layout_images', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('manufacturers', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('car_models', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('layouts', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('layout_images', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
