<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceRangeSeats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('price_range_seats', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('price_range_id');
            $table->foreign('price_range_id')->references('id')->on('price_ranges')->onDelete('cascade');

            $table->unsignedInteger('seat_id');
            $table->foreign('seat_id')->references('id')->on('seats')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('price_range_seats');
    }
}
