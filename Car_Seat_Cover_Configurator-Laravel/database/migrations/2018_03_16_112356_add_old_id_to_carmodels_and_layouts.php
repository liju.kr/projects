<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldIdToCarmodelsAndLayouts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layouts', function (Blueprint $table) {
            $table->integer('old_id')->nullable();
        });
        Schema::table('car_models', function (Blueprint $table) {
            $table->integer('old_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layouts', function (Blueprint $table) {
            $table->dropColumn(['old_id'])->nullable();
        });
        Schema::table('car_models', function (Blueprint $table) {
            $table->dropColumn(['old_id']);
        });
    }
}
