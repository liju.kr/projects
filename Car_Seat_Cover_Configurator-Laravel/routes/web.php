<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('privacy_policy', function () {
    return view('pages.privacy.privacy_policy');
});

Route::get('/view-my-configuration/{id}', 'Main\ViewConfigurationController@view');
Route::get('/view-my-em-configuration/{id}', 'Main\ViewConfigurationController@viewEmbroideryOrder');

Auth::routes();
Route::get('/unverified', 'HomeController@unverified')->name('unverified');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/import/brands', 'ImportController@import_brands');
    Route::get('/import/cars', 'ImportController@import_cars');
    Route::get('/view/cars', 'ImportController@view_cars');
    Route::get('/rename/duplicates', 'ImportController@rename_duplicates');
    Route::get('/remove/duplicates', 'ImportController@remove_duplicates');
    Route::get('/import/years_layouts', 'ImportController@import_years_layouts');
    Route::get('/import/trigger_brands', 'ImportController@trigger_brands');
    Route::get('/import/trigger_cars', 'ImportController@trigger_cars');
    Route::get('/import/trigger_years_layouts', 'ImportController@trigger_years_layouts');
    Route::get('/import/trigger_layout_images', 'ImportController@trigger_layout_images');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/passport', 'Api\ApiController@index')->name('api');
    Route::get('/orders', 'Main\OrderController@orders')->name('orders');
    Route::get('/orders/{id}', 'Main\OrderController@order');
    Route::get('/configurations', 'Main\ConfigurationController@configurations')->name('configurations');

    //uae
    Route::get('/view_all_layouts', 'Main\WellfitLayoutController@index');
    Route::get('/view_all_car_models', 'Main\WellfitLayoutController@carModels');

    Route::resource('users', 'Main\UserController');

    Route::match(['put', 'patch'], '/quick_car_model_edit','Main\WellfitLayoutController@updateCarModel');
    Route::match(['put', 'patch'], '/change_password/{id}','Main\UserController@change_password');


    Route::post('/logout_all', 'Main\UserController@logoutAll');


    Route::resource('manufacturers', 'Data\ManufacturerController');
    Route::resource('manufacturers.cars', 'Data\CarController');
    Route::resource('cars', 'Data\CarController');
    Route::resource('cars.models', 'Data\CarModelController');
    Route::resource('models', 'Data\CarModelController');
    Route::resource('models.seats', 'Data\SeatController');
    Route::resource('seats', 'Data\SeatController');
    Route::resource('models.layouts', 'Data\LayoutController');
    Route::resource('layouts', 'Data\LayoutController');
    Route::resource('layout_image', 'Data\LayoutImageController');
    Route::resource('layout_design', 'Data\LayoutDesignController');
    Route::resource('seats.configuration', 'Main\ConfigurationController');
    Route::resource('seats.base', 'Configuration\BaseController');
    Route::resource('seats.original_image', 'Configuration\OriginalImageController');
    Route::resource('seats.layer', 'Configuration\LayerController');
    Route::resource('layer', 'Configuration\LayerController');
    Route::resource('layer.variant', 'Configuration\VariantController');
    Route::resource('variant', 'Configuration\VariantController');
    Route::resource('materials', 'Data\MaterialController');
    Route::resource('colour_categories', 'Data\ColourCategoryController');
    Route::resource('companies', 'Data\CompanyController');
    Route::resource('colour_categories.colours', 'Data\ColourController');
    Route::resource('colours', 'Data\ColourController');
    Route::resource('gallery_categories', 'Data\GalleryCategoryController');
    Route::resource('gallery_categories.galleries', 'Data\GalleryController');
    Route::resource('galleries', 'Data\GalleryController');
    Route::resource('patterns', 'Data\PatternController');


    Route::post('/car_model_layouts', 'Data\GalleryController@carModelLayouts');
    Route::post('/car_brand_models', 'Data\GalleryController@carBrandModels');

    Route::get('/embroidery_orders', 'Main\OrderController@embroideryOrders')->name('embroidery_orders');
    Route::get('/embroidery_orders/{id}', 'Main\OrderController@embroideryOrder');



    Route::resource('seats.seat_photos', 'Data\SeatPhotoController');

    Route::resource('price_ranges', 'Data\PriceRangeController');

    Route::post('/models/{id}/seats/existing', 'Data\SeatController@storeExisting');

    Route::delete('/delete_seats/{id}/', 'Data\SeatController@deleteSeat');

    Route::match(['put', 'patch'], '/orders/update/{id}','Main\OrderController@update');
    Route::match(['put', 'patch'], '/embroidery_orders/update/{id}','Main\OrderController@updateEmbroidery');




    Route::get('/import_car_model_year', 'ImportController@import_car_model_year');
    Route::get('/import_seat_layout', 'ImportController@import_seat_layout');
    Route::get('/check_image_path', 'ImportController@check_image_path');
    Route::get('/check_image_path_order', 'ImportController@check_image_path_order');
    Route::get('/layout_price', 'ImportController@layout_price');




  // New Import UAE
Route::get('/trigger_brands', 'NewImportController@trigger_brands');
Route::get('/trigger_cars', 'NewImportController@trigger_cars');
Route::get('/trigger_car_models', 'NewImportController@trigger_car_models');
Route::get('/trigger_layouts', 'NewImportController@trigger_layouts');
Route::get('/trigger_layout_images', 'NewImportController@trigger_layout_images');
Route::get('/testing_query', 'NewImportController@testing_query');

});


