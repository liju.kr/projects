<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', 'Api\AuthController@postLogin');
Route::post('/signup', 'Api\AuthController@signUp');


//Wellfit Seat Covers
Route::post('/user/login', 'Api\LayoutsApiController@postLogin');

//Wellfit Report App
Route::post('/wellfit_report_login', 'Api\WellfitReportAuthController@postLogin');

//Wellfit Seat Covers
Route::post('/em_login', 'Api\EmbroideryApiAuthController@postLogin');
Route::post('/em_signup', 'Api\EmbroideryApiAuthController@signUp');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request)    {
        return $request->user();
    });
    Route::get('/manufacturers', 'Data\ManufacturerController@getList');
    Route::post('/cars', 'Data\ManufacturerController@getCars');
    Route::post('/models', 'Data\CarController@getModels');
    Route::post('/layouts', 'Data\CarModelController@getLayouts');
    Route::post('/materials_list', 'Data\CarModelController@getMaterials');
    Route::post('/seats', 'Data\CarModelController@getSeatModels');
    Route::post('/configuration', 'Main\ConfigurationController@getLayers');
    Route::post('/configuration/save', 'Main\ConfigurationController@store');
    Route::get('/saved', 'Main\ConfigurationController@getSaved');
    Route::get('/orders', 'Main\OrderController@getOrders');
    Route::post('/confirm', 'Main\ConfigurationController@confirmOrder');


    //uae
    Route::get('/search_layouts', 'Data\CarModelController@searchLayouts');
    Route::get('/get_colours', 'Data\ColourController@getColours');
    Route::get('/get_materials', 'Data\MaterialController@getMaterials');
    Route::get('/get_gallery_categories', 'Data\GalleryController@getGalleryCategories');
    Route::get('/get_galleries', 'Data\GalleryController@getGalleries');
    Route::post('/stock_page_detail_filters', 'Data\StockController@getStockPageDetailFilters');
    Route::post('/user_roles', 'Data\StockController@getUserRoles');
    Route::post('/contact_send_mail', 'Main\ContactController@sendMail');
    Route::get('/get_contact', 'Main\ContactController@getContact');


//Wellfit Seat Covers
    Route::post('/brand/getBrands', 'Api\LayoutsApiController@getBrands');

    Route::post('/carmodel/getCarmodels', 'Api\LayoutsApiController@getCarmodels');

    Route::post('year/getyears', 'Api\LayoutsApiController@getYears');

    Route::post('seatlayout/getSeatLayouts', 'Api\LayoutsApiController@getSeatLayouts');
    Route::post('seatlayout/getSeatlayout', 'Api\LayoutsApiController@getSeatLayout');
    Route::post('seatlayout/getSeatLayoutYear', 'Api\LayoutsApiController@getSeatLayoutYear');

    Route::post('colour/getColours', 'Api\LayoutsApiController@getColours');

    Route::post('material/getMaterials', 'Api\LayoutsApiController@getMaterials');

    Route::post('prices/getPrices', 'Api\LayoutsApiController@getPrices');

    Route::post('gallery/getGalleryItems', 'Api\LayoutsApiController@getGalleryItems');
    Route::post('gallery/getGalleryItem', 'Api\LayoutsApiController@getGalleryItem');


    //Yaco Embroidery

    Route::get('/em_login_status', 'Api\EmbroideryApiAuthController@loginStatus');
    Route::get('/em_get_profile', 'Api\EmbroideryApiAuthController@getProfile');

    Route::get('/em_home_data', 'Api\EmbroideryApiController@getHomeData');
    Route::get('/em_price_ranges', 'Api\EmbroideryApiController@getPriceRanges');
    Route::get('/em_designs', 'Api\EmbroideryApiController@getDesigns');
    Route::get('/em_single_design', 'Api\EmbroideryApiController@getSingleDesign');
    Route::get('/em_car_brands', 'Api\EmbroideryApiController@getCarBrands');
    Route::get('/em_car_models', 'Api\EmbroideryApiController@getCarModels');
    Route::get('/em_car_years', 'Api\EmbroideryApiController@getCarYears');
    Route::get('/em_car_layouts', 'Api\EmbroideryApiController@getCarLayouts');
    Route::get('/em_saved', 'Api\EmbroideryApiController@getSaved');
    Route::get('/em_ordered', 'Api\EmbroideryApiController@getOrdered');
    Route::get('/get_profile', 'Api\EmbroideryApiAuthController@getProfile');

    Route::post('/em_save_configuration', 'Api\EmbroideryApiController@saveConfigurations');
    Route::post('/em_save_configuration_edit', 'Api\EmbroideryApiController@editConfiguration');
    Route::post('/em_delete_saved', 'Api\EmbroideryApiController@deleteSaved');
    Route::post('/em_order_configurations', 'Api\EmbroideryApiController@orderConfigurations');
    Route::post('/em_delete_ordered', 'Api\EmbroideryApiController@deleteOrdered');
    Route::post('/em_update_profile', 'Api\EmbroideryApiAuthController@updateProfile');
    Route::post('/em_change_password', 'Api\EmbroideryApiAuthController@changePassword');


    //Wellfit Report App
    Route::get('/wellfit_report_user_roles', 'Api\WellfitReportController@getUserRole');

});


