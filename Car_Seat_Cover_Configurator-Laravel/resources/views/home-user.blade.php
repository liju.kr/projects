@extends('layouts.app-user')

@section('content')
<div class="container">
    <div class="row">
       
        @yield('selection-content')
    </div>
</div>

@endsection
