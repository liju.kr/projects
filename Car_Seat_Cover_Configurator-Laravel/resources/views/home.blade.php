@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong class=" text-capitalize">
                    {{ \Auth::user()->role_name }}
                    </strong>
                        <div class="pull-right text-capitalize">
                            <strong>{{ \Auth::user()->name }}</strong>
                        </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                            @if(auth()->user()->hasRole('wellfit_data_manager') || auth()->user()->hasRole('yaco_data_manager') )
                            <div class="col-sm-3 home-card">
                                <a href="{{url('manufacturers')}}">
                                    <div class="thumbnail">
                                        <div class="caption text-center">
                                            <h4 id="thumbnail-label">Manufactures (Car Brands)</h4>
                                            <p><span class="badge badge-warning">{{ $response['manufacturer_count'] }} Brands</span></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           @endif
                                @if(auth()->user()->hasRole('wellfit_data_manager'))
                                    <div class="col-sm-3 home-card">
                                        <a href="{{url('view_all_layouts')}}">
                                            <div class="thumbnail">
                                                <div class="caption text-center">
                                                    <h4 id="thumbnail-label">View All Layouts</h4>
                                                    <p><span class="badge badge-warning">{{ $response['layouts_count'] }} Layouts</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 home-card">
                                        <a href="{{url('colour_categories')}}">
                                            <div class="thumbnail">
                                                <div class="caption text-center">
                                                    <h4 id="thumbnail-label">Colours</h4>
                                                    <p><span class="badge badge-warning">{{ $response['colour_count'] }} Colours</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 home-card">
                                        <a href="{{url('companies')}}">
                                            <div class="thumbnail">
                                                <div class="caption text-center">
                                                    <h4 id="thumbnail-label">Companies</h4>
                                                    <p><span class="badge badge-warning">{{ $response['company_count'] }} Companies</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 home-card">
                                        <a href="{{url('view_all_car_models')}}">
                                            <div class="thumbnail">
                                                <div class="caption text-center">
                                                    <h4 id="thumbnail-label">Car Models (Years)</h4>
                                                    <p><span class="badge badge-warning">Quick Edit</span></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @if(auth()->user()->hasRole('wellfit_admin') || auth()->user()->hasRole('yaco_admin'))
                                <div class="col-sm-3 home-card">
                                    <a href="{{url('users')}}">
                                        <div class="thumbnail">
                                            <div class="caption text-center">
                                                <h4 id="thumbnail-label">Users</h4>
                                                <p><span class="badge badge-warning">{{ $response['users_count'] }} Users</span></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if(auth()->user()->hasRole('yaco_data_manager'))
                            <div class="col-sm-3 home-card">
                                <a href="{{url('patterns')}}">
                                    <div class="thumbnail">
                                        <div class="caption text-center">
                                            <h4 id="thumbnail-label">Embroidery Patterns</h4>
                                            <p><span class="badge badge-warning">{{ $response['pattern_count'] }} Patterns</span></p>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-sm-3 home-card">
                                <a href="{{url('models/'.env('COMMON_CAR_MODEL_ID').'/seats')}}">
                                    <div class="thumbnail">
                                        <div class="caption text-center">
                                            <h4 id="thumbnail-label">Common Designs</h4>
                                            <p><span class="badge badge-warning">{{ $response['common_design_count'] }} Designs</span></p>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-sm-3 home-card">
                                <a href="{{url('gallery_categories')}}">
                                    <div class="thumbnail">
                                        <div class="caption text-center">
                                            <h4 id="thumbnail-label">Gallery</h4>
                                            <p><span class="badge badge-warning">{{ $response['gallery_count'] }} Photos</span></p>
                                        </div>
                                    </div>
                                </a>
                            </div>

                        <div class="col-sm-3 home-card">
                            <a href="{{url('materials')}}">
                                <div class="thumbnail">
                                    <div class="caption text-center">
                                        <h4 id="thumbnail-label">Materials</h4>
                                        <p><span class="badge badge-warning">{{ $response['material_count'] }} Materials</span></p>
                                    </div>
                                </div>
                            </a>
                        </div>


                            <div class="col-sm-3 home-card">
                                <a href="{{url('orders')}}">
                                    <div class="thumbnail">
                                        <div class="caption text-center">
                                            <h4 id="thumbnail-label">Order Tracking Management</h4>
                                            <p><span class="badge badge-warning">{{ $response['order_count'] }} Orders</span></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endif
                            @if(auth()->user()->hasRole('dev_admin'))
                                <div class="col-sm-3 home-card">
                                    <a href="{{url('passport')}}">
                                        <div class="thumbnail">
                                            <div class="caption text-center">
                                                <h4 id="thumbnail-label">API management</h4>
                                                <p><span class="badge badge-warning">Developer Use</span></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                    </div>

                </div>
            </div>
        </div>
        @yield('selection-content')
    </div>
</div>

@endsection
