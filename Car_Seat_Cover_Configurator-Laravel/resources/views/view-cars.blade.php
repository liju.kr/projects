@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Old brands
                    </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <a href="{{url('/rename/duplicates')}}">
                                <div class="btn btn-success">Rename duplicate names</div>
                                </a>
                                <a href="{{url('/remove/duplicates')}}">
                                    <div class="btn btn-success">Remove duplicates and sync layouts</div>
                                </a>
                            </div>
                        </div>
                        @foreach(\App\Models\Manufacturer::orderby('name')->get() as $brand)
                        <div class="panel-body">
                            <div class="col-md-2">
                                <img src="{{url('storage/'.$brand->image)}}" width="30">
                                {{$brand->name}}
                            </div>
                            <div class="col-md-10">
                                @foreach($brand->cars as $car)
                                    <h3>{{$car->name}}</h3><br>
                                    @foreach($car->models as $model)
                                        <div class="btn btn-success" >{{$model->name}}</div>
                                    @endforeach
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                        @endforeach

                    <a href="{{url('/import/trigger_brands')}}">
                        <button class="pull-right">IMPORT</button>
                    </a>
                </div>
            </div>
            @yield('selection-content')
        </div>
    </div>

@endsection
