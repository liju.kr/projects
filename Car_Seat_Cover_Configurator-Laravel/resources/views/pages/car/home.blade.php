@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a> >
                    <a href="{{route('manufacturers.index')}}">
                        Manufacturers
                    </a>>
                    <a href="{{route('manufacturers.cars.index',$manufacturer->getId())}}">
                        {{$manufacturer->getName()}}
                    </a>
                    <div class="dash-heading">MODELS</div>
                    <div class="pull-right">
                        @if(auth()->user()->hasRole('wellfit_data_manager'))
                        @if(env('EMBROIDERY_BRAND_ID')==$manufacturer->getId() || env('CUST_MAN_ID')==$manufacturer->getId())
                            @else
                            <a href="{{route('manufacturers.cars.create',$manufacturer->getId())}}">
                                +Add New
                            </a>
                            @endif
                        @endif

                    </div>
                </div>

                <div class="panel-body">
                    @yield('sub-content')
                </div>
            </div>
        </div>
    </div>
</div>                    
@endsection