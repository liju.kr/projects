@extends('pages.car.home')

@section('sub-content')
<div class="row container-fluid">
  @foreach($manufacturer->cars as $car)
        <div class="col-sm-2 home-card">
            <div class="thumbnail">
                <a href="{{route('cars.models.index',$car->getId())}}">
                    <div class="caption text-center">
                        <h5>{{$car->getName()}}</h5>
                    </div>
                </a>
                @if(auth()->user()->hasRole('wellfit_data_manager'))
                <p class="text-center z-index">
                    <a href="{{route('cars.edit',$car->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
                </p>
                    @endif
            </div>
        </div>
  @endforeach
</div>
@endsection