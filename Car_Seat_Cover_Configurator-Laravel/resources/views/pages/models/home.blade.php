@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a> >
                    <a href="{{route('manufacturers.index')}}">
                        Manufacturers
                    </a>>
                    <a href="{{route('manufacturers.cars.index',$car->manufacturer->getId())}}">
                        {{$car->manufacturer->getName()}}
                    </a>>
                    <a href="{{route('cars.models.index',$car->getId())}}">
                        {{$car->getName()}}
                    </a>
                    <div class="dash-heading">YEAR</div>
                    <div class="pull-right">
                        @if(auth()->user()->hasRole('wellfit_data_manager'))
                        @if(env('EMBROIDERY_CAR_ID')==$car->getId() || env('COMMON_CAR_ID')==$car->getId())
                            @else
                          <a href="{{route('cars.models.create',$car->getId())}}">
                        +Add New
                        </a>
                        @endif
                            @endif
                    </div>
                </div>

                <div class="panel-body">
                    @yield('sub-content')
                </div>
            </div>
        </div>
    </div>
</div>                    
@endsection