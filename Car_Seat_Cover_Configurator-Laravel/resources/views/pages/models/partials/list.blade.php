@extends('pages.models.home')

@section('sub-content')
<div class="row conf-container">
  @foreach($car->models as $car_model)

        <div class="col-sm-2 home-card">
            <div class="thumbnail">
                <a href="{{route('models.layouts.index',$car_model->getId())}}">
                    <div class="caption text-center">
                        <h5>{{$car_model->getName()}}</h5>
                    </div>
                </a>

                    <p class="text-center z-index">
                        @if(auth()->user()->hasRole('yaco_data_manager'))
                            <a href="{{route('models.seats.index',$car_model->getId())}}"><button class="btn btn-sm btn-success">Seat Designs</button></a>
                        @endif
                            @if(auth()->user()->hasRole('wellfit_data_manager'))
                        <a href="{{route('models.edit',$car_model->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
                            @endif
                    </p>


            </div>
        </div>
  @endforeach
</div>
@endsection