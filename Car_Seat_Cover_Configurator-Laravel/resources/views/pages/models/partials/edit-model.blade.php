@extends('pages.models.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-8">
		{!! Form::open(array('route' => ['models.update',$car_model->getId()], 'method' => 'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
		<div class="form-group">
			<label for="year_from" class="col-sm-2 control-label">Year From</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="year_from" placeholder="Year From" value="{{$car_model->year_from}}">
			</div>
		</div>
		<div class="form-group">
			<label for="year_to" class="col-sm-2 control-label">Year To</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="year_to" placeholder="Year To" value="{{$car_model->year_to}}">
			</div>
		</div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Save</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>

		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Created On</td>
					<td>{{ $car_model->createdOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated On</td>
					<td>{{ $car_model->updatedOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated By</td>
					<td class="text-capitalize">{{ $car_model->updatedBy() }}</td>
				</tr>
				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('wellfit_data_manager'))
							@if($car_model->seats->count() || env('EMBROIDERY_CAR_MODEL_ID')==$car_model->getId() || $car_model->layouts->count())
								{{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
								{{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
								{{ Form::close() }}
							@else
								{{ Form::open(array('style' => 'display:inline-block;', 'url' => 'models/'.$car_model->getId())) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
								{{ Form::close() }}
							@endif
						@endif
					</td>
				</tr>
			</table>
		</div>
	</div>
@endsection