@extends('pages.models.home')

@section('sub-content')
	<div class="row">

    <div class="col-md-6">
		{!! Form::open(array('route' => ['cars.models.store',$car->getId()],'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}

		<div class="form-group">
			<label for="year_from" class="col-sm-2 control-label">Year From</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="year_from" placeholder="Year From">
			</div>
		</div>
		<div class="form-group">
			<label for="year_to" class="col-sm-2 control-label">Year To</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="year_to" placeholder="Year To">
			</div>
		</div>
		<input type="hidden" class="form-control" name="old_id" value="1">
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Add</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
	</div>
@endsection