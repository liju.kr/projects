@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                <a href="{{url('home')}}">
                  Home
                </a> >
                    <a href="{{route('manufacturers.index')}}">
                        Manufacturers
                    </a> >
                <a href="{{route('manufacturers.cars.index',$seat->car_model->car->manufacturer->getId())}}">
                  {{$seat->car_model->car->manufacturer->getName()}} 
                </a>  > 
                <a href="{{route('cars.models.index',$seat->car_model->car->getId())}}">
                  {{$seat->car_model->car->getName()}} 
                </a>  > 
                <a href="{{route('models.seats.index',$seat->car_model->getId())}}">
                  {{$seat->car_model->getName()}}
                </a> >
                  {{$seat->getName()}}
                    <div class="dash-heading"> Configuration </div>
                  </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs">
{{--                        <li class="@if($tab=='seat_photos')active @endif">--}}
{{--                            <a href="{{route('seats.seat_photos.index',$seat->getId())}}">--}}
{{--                                Configuration Photos--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                      <li class="@if($tab=='original_image')active @endif">--}}
{{--                        <a href="{{route('seats.original_image.index',$seat->getId())}}">--}}
{{--                          Original Image--}}
{{--                        </a>--}}
{{--                      </li>--}}

                      <li class="@if($tab=='base')active @endif">
                        <a href="{{route('seats.base.index',$seat->getId())}}">
                          Base
                        </a>
                      </li>
                      @foreach($seat->layers as $layer)
                      <li class="@if($tab==$layer->getId())active @endif">
                        <a href="{{ route('seats.layer.show',[$seat->getId(),$layer->getId()]) }}">
                          {{$layer->getName()}}
                        </a>
                      </li>
                      @endforeach
                      <li class="@if($tab=='add-layer')active @endif">
                        <a href="{{route('seats.layer.create',$seat->getId())}}">
                          + Add Layer
                        </a>
                       </li>
                    </ul>

                    <div class="tab-content">
                    @yield('tab-content')
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
