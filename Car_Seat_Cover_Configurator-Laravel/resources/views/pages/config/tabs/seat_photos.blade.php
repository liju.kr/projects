@extends('pages.config.home')

@section('tab-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Seat Photos</div>

                <div class="panel-body">
                    <div class="col-md-6">
                        @if(isset($seat_photo))
                            {!! Form::open(['route' => ['seats.seat_photos.update',$seat->getId(), $seat_photo->id], 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
                        @else
                            {!! Form::open(['route' => ['seats.seat_photos.store',$seat->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}

                        @endif

                        <div class="form-group">
                            <label for="Variant Name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name"  @if(isset($seat_photo)) value="{{ $seat_photo->name }}" @endif placeholder="Photo Name">
                            </div>
                        </div>

                        <div class="form-group">
                            @if(isset($seat_photo))
                            <img src="{{Storage::url($seat_photo->getImageURL())}}">
                            @endif
                            <label for="Base Image" class=" control-label"> Add / Change Seat Photo
                            </label>
                            <div class="">
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary pull-right">Upload</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="row conf-container">
        @foreach($seat->seat_photos as $seat_photo)
            <div class="col-md-2 conf-block">
                <a href="">
                    <img src="{{Storage::url($seat_photo->getImageURL())}}">
                    <h2>{{$seat_photo->name}}</h2>

                </a>
                <div class="action-container">
                    <a href="{{url('seats/'.$seat->id.'/seat_photos/'.$seat_photo->id.'/edit')}}">
                        <button class="btn btn-primary">edit</button>
                    </a>
                    {{ Form::open(array('url' => 'seats/'.$seat->id.'/seat_photos/'.$seat_photo->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
                    {{ Form::close() }}
                </div>
            </div>
        @endforeach
    </div>
@endsection