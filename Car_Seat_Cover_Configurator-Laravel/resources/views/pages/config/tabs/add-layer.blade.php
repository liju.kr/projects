@extends('pages.config.home')

@section('tab-content')
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add Layer</div>
                    <div class="panel-body ">
                        <div class="col-md-6">
                        {!! Form::open(array('route' => ['seats.layer.store',$seat->getId()],'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
                          <div class="form-group">
                            <label for="Layer Name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="name" placeholder="Seat Name">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-primary pull-right">Add</button>
                            </div>
                          </div>
                        {!! Form::close() !!} 
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
@endsection