@extends('pages.config.home')

@section('tab-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{$layer->getName()}} Variants
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Edit Variant</div>
                                <div class="panel-body">
                                    {!! Form::open(['route' => ['variant.update',$variant->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}

                                    <div class="form-group">
                                        <label for="Variant Name" class="col-sm-2 control-label">Pattern</label>
                                        <div class="col-sm-10">
                                            <select type="text" class="form-control" name="pattern_id">
                                                @foreach(\App\Models\Pattern::get() as $pattern)
                                                    @php
                                                    $assigned_pattern = \App\Models\PatternVariant::where('layer_id', $variant->layer_id)->where('variant_id', $variant->id)->first();
                                                    @endphp
                                                    <option value="{{ $pattern->id }}" @if($assigned_pattern->pattern_id == $pattern->id) selected @endif>{{ $pattern->name }} || {{ $pattern->code }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="Variant Name" class="col-sm-2 control-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Variant Name" value="{{$variant->getName()}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Colour code" class="col-sm-2 control-label">Colour code</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="colour"
                                                   placeholder="Colour code" value="{{$variant->getColour()}}">
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label for="Variant Image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10">
                                            <img src="{{Storage::url($variant->getImageURL())}}">
                                            <input type="file" name="image" class="form-control" id="Variant Image"
                                                   placeholder="Variant Image">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary pull-right">Save</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}


                                </div>

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection