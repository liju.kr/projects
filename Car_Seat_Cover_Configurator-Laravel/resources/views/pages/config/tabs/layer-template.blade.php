@extends('pages.config.home')

@section('tab-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{$layer->getName()}} Variants
                    <div class="conf-action-container pull-right">
                        <a href="{{route('layer.edit',$layer->getId())}}">
                            <button class="btn btn-primary">edit</button>
                        </a>
                        {{ Form::open(array('url' => 'layer/'.$layer->getId())) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
                        {{ Form::close() }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add Variant</div>
                            <div class="panel-body">
                                {!! Form::open(['route' => ['layer.variant.store',$layer->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}

                                <div class="form-group">
                                    <label for="Variant Name" class="col-sm-2 control-label">Pattern</label>
                                    <div class="col-sm-10">
                                        <select type="text" class="form-control" name="pattern_id" >
                                            @foreach(\App\Models\Pattern::get() as $pattern)
                                            <option value="{{ $pattern->id }}" @if(env('DEFAULT_PATTERN_ID') == $pattern->id) selected @endif>{{ $pattern->name }} || {{ $pattern->code }} </option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Variant Name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" placeholder="Variant Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Colour code" class="col-sm-2 control-label">Colour code</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="colour" placeholder="Colour code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Variant Image" class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image" class="form-control" id="Variant Image"
                                               placeholder="Variant Image">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary pull-right">Add</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                        <div class="row conf-container">
                            @foreach($layer->variants as $variant)
                                <div class="col-sm-2 home-card">
                                    <div class="thumbnail delete-form">

                                            <div class="caption text-center">
                                                <div class="position-relative">
                                                    <img src="{{Storage::url($variant->getImageURL())}}" class="img-responsive">
                                                </div>
                                                <p>{{$variant->getName()}}</p>
                                                <p><span class="badge badge-warning">{{$variant->pattern()->pattern->name}}</span> <span class="badge badge-warning">{{$variant->pattern()->pattern->code}}</span></p>
                                            </div>
                                        @if(auth()->user()->hasRole('yaco_data_manager'))
                                            <div class="text-center">
                                                <a href="{{route('variant.edit',$variant->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
                                                {{ Form::open(array('url' => 'variant/'.$variant->getId())) }}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ?');")) }}
                                                {{ Form::close() }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    </div>
@endsection