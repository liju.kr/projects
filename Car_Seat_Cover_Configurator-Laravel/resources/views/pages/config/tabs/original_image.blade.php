@extends('pages.config.home')

@section('tab-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Original Image</div>

                <div class="panel-body">

                    <div class="col-md-2">
                        @if($seat->getOriginalImageURL())
                            <img style="width: 100%" src="{{Storage::url($seat->getOriginalImageURL())}}">
                        @endif
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(['route' => ['seats.original_image.store',$seat->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label for="Base Image" class=" control-label">Add/Change Original Image
                            </label>
                            <div class="">
                                <input type="file" name="image" class="form-control" id="Base Image"
                                       placeholder="Base Image">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-primary pull-right">Upload</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection