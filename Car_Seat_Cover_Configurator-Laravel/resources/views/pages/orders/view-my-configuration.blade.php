@extends('home-user')
@section('selection-content')
        <div class="col-md-12 list-cntr">
            <h2 style="padding-top:25px;">{{$order->name}}'s Order</h2>
            <div class="row">
             
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 style="padding-bottom:25px;">Selected Image</h3>
                            <div class="sub-img">
                                <img src="{{$order->configuration->selected_image}}" style="width:70%" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3 style="padding-bottom:25px;">Configuration Image</h3>
                            <div class="sub-img">
                                    @include('pages.configurations.partials.config_image', [$image_array=$order->configuration->image])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top:25px;">
                <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item"><span>Customer Name : </span>{{$order->name}}</li>
                            <li class="list-group-item"><span>Customer Email : </span>{{$order->email}}</li>
                            <li class="list-group-item"><span>Customer Phone : </span>{{$order->phone}}</li>
                            <li class="list-group-item"><span>Configuration ID : </span>{{$order->configuration->id}}</li>
                            <li class="list-group-item"><span>Order ID : </span>{{$order->id}}</li>
                        </ul>
                    @if($order->delivery_date)
                        <ul class="list-group">
                            <li class="list-group-item"><span>Delivery Date : </span>{{ date('d-m-Y', strtotime($order->delivery_date)) }}</li>
                        </ul>
                    @endif
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group">
                            @foreach(json_decode($order->configuration->selections) as $key=>$selection)
                            <li class="list-group-item"><span>{{$selection->label}} : </span>{{$selection->value}}</li>
                            @endforeach
                        </ul>
                    </div>
            </div>

        </div>
@endsection