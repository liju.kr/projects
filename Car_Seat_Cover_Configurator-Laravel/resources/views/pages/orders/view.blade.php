@extends('home-user')
@section('selection-content')
        <div class="col-md-12 list-cntr">
            <h2>Order</h2>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item"><span>Customer name : </span>{{$order->name}}</li>
                        <li class="list-group-item"><span>Customer email : </span>{{$order->email}}</li>
                        <li class="list-group-item"><span>Customer phone : </span>{{$order->phone}}</li>
                        <li class="list-group-item"><span>Configuration ID : </span>{{$order->configuration->id}}</li>
                        <li class="list-group-item"><span>Order ID : </span>{{$order->id}}</li>
                        <li class="list-group-item"><span>Ordered On : </span>{{ date('d-m-Y', strtotime($order->created_at)) }}</li>

                    </ul>
                    @if($order->delivery_date)
                    <ul class="list-group">
                           <li class="list-group-item"><span>Delivery Date : </span>{{ date('d-m-Y', strtotime($order->delivery_date)) }}</li>
                            <li class="list-group-item" ><span>Delivery Date Updated On : </span>{{ date('d-m-Y', strtotime($order->updated_at)) }}</li>
                    </ul>
                    @endif
                    <ul class="list-group">
                        @foreach(json_decode($order->configuration->selections) as $key=>$selection)
                        <li class="list-group-item"><span>{{$selection->label}} : </span>{{$selection->value}}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Selected Image</h3>
                            <div class="sub-img">
                                <img src="{{$order->configuration->selected_image}}" style="width:70%" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3>Configuration Image</h3>
                            <div class="sub-img">
                                    @include('pages.configurations.partials.config_image', [$image_array=$order->configuration->image])
                            </div>
                        </div>
                    </div>

                    <div class="row">
                            <h4 style="text-align:center"> <a href="{{ url('/') }}/view-my-configuration/{{ $order->id }}" target="_blank" >View Large Image</a> </h4>
                    </div>
                    <div class="row">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-md-12">
                            {!! Form::open(array('url' => ['orders/update/'.$order->id],'method'=>'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
                            <div class="form-group">
                                <label for="Layer Name" class="col-sm-4 control-label">Update Delivery Date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" name="delivery_date" @if($order->delivery_date) value="{{$order->delivery_date }}" @endif>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection