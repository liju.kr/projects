@extends('home-user')
@section('selection-content')
        <div class="col-md-12 list-cntr">
            <h2>Order</h2>
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group">

                        <li class="list-group-item"><span>Order Status : </span>  @if($order->status == 2)
                                <button type="button" class="btn btn-success">Delivered</button>
                            @elseif($order->status == 1)
                                <button type="button" class="btn btn-warning">Production</button>
                            @else
                                <button type="button" class="btn btn-danger">Pending</button>
                            @endif</li>
                            <li class="list-group-item"><span>Customer Name : </span>{{$order->user->name}}</li>
                        <li class="list-group-item"><span>Customer Email : </span>{{$order->user->email}}</li>
                        <li class="list-group-item"><span>Customer Phone : </span>{{$order->user->contact}}</li>
                        <li class="list-group-item"><span>Customer Shop : </span>{{$order->user->shop_name}}</li>
                        <li class="list-group-item"><span>Order ID : </span>{{$order->id}}</li>
                        <li class="list-group-item"><span>Ordered On : </span>{{ date('d-m-Y', strtotime($order->created_at)) }}</li>

                    </ul>
                    @if($order->delivery_date)
                    <ul class="list-group">
                           <li class="list-group-item"><span>Delivery Date : </span>{{ date('d-m-Y', strtotime($order->delivery_date)) }}</li>
                            <li class="list-group-item" ><span>Delivery Date Updated On : </span>{{ date('d-m-Y', strtotime($order->updated_at)) }}</li>
                    </ul>
                    @endif
                    <ul class="list-group">
                        @php
                            $selections = json_decode($order->configuration)->selections;
                            $vehicle_details = json_decode($order->configuration)->vehicle_details;
                            $materials = json_decode($order->configuration)->materials;
                            $price = json_decode($order->configuration)->price;
                            $vat = json_decode($order->configuration)->vat;
                            $total_price = json_decode($order->configuration)->total_price;
                            $quantity = json_decode($order->configuration)->quantity;
                        @endphp
                        <li class="list-group-item"><span>Car Brand : </span>{{$vehicle_details[0]->car_brand}}</li>
                        <li class="list-group-item"><span>Car  : </span>{{$vehicle_details[0]->car}}</li>
                        <li class="list-group-item"><span>Car Model : </span>{{$vehicle_details[0]->car_model}}</li>
                        <li class="list-group-item"><span>YC Layout : </span>{{$vehicle_details[0]->car_layout}}</li>
                        <li class="list-group-item"><span>Material : </span>{{$materials[0]->material_name}}</li>
                        @foreach($selections as $key=>$selection)
                            <li class="list-group-item"><span>{{$selection->label}} : </span>{{$selection->value}}</li>
                        @endforeach
                        <li class="list-group-item"><span>Price : </span>{{$price}} AED</li>
                        <li class="list-group-item"><span>Quantity : </span>{{$quantity}}</li>
                        <li class="list-group-item"><span>Vat : </span>{{$vat}} %</li>
                        <li class="list-group-item"><span>Estimate Price : </span>{{$total_price}} AED</li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Selected Image</h3>
                            <div class="sub-img">

                                <img src=" {{url('storage/'.json_decode($order->configuration)->design_selected_image)}}" style="width:70%" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3>Configuration Image</h3>
                            <div class="sub-img">
                                @php
                                    $image_array = json_decode($order->configuration)->image_array;
                                @endphp
                                @include('pages.orders.partials.config_image', [$image_array])

                            </div>
                        </div>
                    </div>

                    <div class="row">
                            <h4 style="text-align:center"> <a href="{{ url('/') }}/view-my-em-configuration/{{ $order->id }}" target="_blank" >View Large Image</a> </h4>
                    </div>
                    <div class="row">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-md-12">
                            {!! Form::open(array('url' => ['embroidery_orders/update/'.$order->id],'method'=>'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
                            <div class="form-group">
                                <label for="Layer Name" class="col-sm-4 control-label">Update Delivery Date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" name="delivery_date" @if($order->delivery_date) value="{{$order->delivery_date }}" @endif>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-sm-4 control-label">Order Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="status" required>
                                        <option value="0" @if($order->status == 0) selected @endif>Pending</option>
                                        <option value="1" @if($order->status == 1) selected @endif>Production</option>
                                        <option value="2" @if($order->status == 2) selected @endif>Delivered</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection