@extends('home-user')
@section('selection-content')
        <div class="col-md-12 list-cntr">
            <h2>My Orders</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Order ID</th>
                    <th>Configuration ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Ordered On</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key=>$order)
                <tr>
                    <td>{{(($orders->currentpage()-1)*$orders->perpage())+$key+1}}</td>
                    <td>{{$order->id}}</td>
                    <td>{{$order->configuration_id}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->phone}}</td>
                    <td>{{ date('d-m-Y', strtotime($order->created_at)) }}</td>
                    <td><a href="{{url('orders/'.$order->id)}}">VIEW ORDER</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
@endsection