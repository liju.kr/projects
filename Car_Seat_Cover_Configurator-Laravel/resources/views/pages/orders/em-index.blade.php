@extends('home-user')
@section('selection-content')
        <div class="col-md-12 list-cntr">
            <h2>Order Tracking Manager</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>SL NO</th>
                    <th>Order ID</th>
                    <th>Ordered On</th>
                    <th>Delivery Date</th>
                    <th>Ordered By</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key => $order)
                <tr>
                    <td>{{(($orders->currentpage()-1)*$orders->perpage())+$key+1}}</td>
                    <td>{{$order->id}}</td>
                    <td>{{ date('d-m-Y h:i A', strtotime($order->created_at))}}</td>
                    <td>{{ date('d-m-Y', strtotime($order->delivery_date))}}</td>
                    <td>{{$order->user->name}} <br> {{$order->user->shop_name}}</td>
                    <td>
                        @if($order->status == 2)
                            <button type="button" class="btn btn-success">Delivered</button>
                            @elseif($order->status == 1)
                            <button type="button" class="btn btn-warning">Production</button>
                            @else
                            <button type="button" class="btn btn-danger">Pending</button>
                            @endif
                    </td>
                    <td><a href="{{url('embroidery_orders/'.$order->id)}}">VIEW ORDER</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
@endsection