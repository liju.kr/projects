@extends('pages.gallery_category.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($gallery_categories as $gallery_category)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<a href="{{route('gallery_categories.galleries.index',$gallery_category->id)}}">
						<div class="caption text-center">
							<h5>{{ $gallery_category->name }}</h5>
						</div>
						</a>
						@if(auth()->user()->hasRole('yaco_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('gallery_categories.edit',$gallery_category->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection