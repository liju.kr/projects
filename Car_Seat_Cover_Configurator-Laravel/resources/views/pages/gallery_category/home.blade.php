@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a>>
                    <a href="{{url('gallery_categories')}}">Gallery Categories </a>

                    <div class="dash-heading">Gallery Categories</div>
                @if(auth()->user()->hasRole('yaco_data_manager'))
                <div class="pull-right">
                   <a href="{{route('gallery_categories.create')}}">+Add New</a>
                </div>
                 @endif
</div>
<div class="panel-body">
@yield('sub-content')
</div>
</div>
</div>
</div>
</div>                    
@endsection