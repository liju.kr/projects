@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a>>
                    <a href="{{url('colour_categories')}}">Colour Categories </a>

                    <div class="dash-heading">Colour Categories</div>
                @if(auth()->user()->hasRole('wellfit_data_manager'))
                <div class="pull-right">
                   <a href="{{route('colour_categories.create')}}">+Add New</a>
                </div>
                 @endif
</div>
<div class="panel-body">
@yield('sub-content')
</div>
</div>
</div>
</div>
</div>                    
@endsection