@extends('pages.colour_category.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-8">
		{!! Form::open(['route' => array('colour_categories.update', $colour_category->id), 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
		{{--{!! Form::open(array('route' => array('colours.update', $colour->id),'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}--}}
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Colour Category Name" value="{{$colour_category->name}}">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Update</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
		<div class="col-md-4 text-right">

		</div>
		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Created On</td>
					<td>{{ $colour_category->createdOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated On</td>
					<td>{{ $colour_category->updatedOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated By</td>
					<td class="text-capitalize">{{ $colour_category->updatedBy() }}</td>
				</tr>
				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('wellfit_data_manager'))


							@if(auth()->user()->hasRole('wellfit_data_manager'))
								@if($colour_category->colours->count())
									{{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
									{{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
									{{ Form::close() }}
								@else
									{{ Form::open(array('url' => 'colour_categories/'.$colour_category->id, 'style' => 'display:inline-block;')) }}
									{{ Form::hidden('_method', 'DELETE') }}
									{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
									{{ Form::close() }}
								@endif
							@endif
						@endif
					</td>
				</tr>
			</table>
		</div>


	</div>
@endsection