@extends('pages.colour_category.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($colour_categories as $colour_category)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<a href="{{route('colour_categories.colours.index',$colour_category->id)}}">
						<div class="caption text-center">
							<h5>{{ $colour_category->name }}</h5>
						</div>
						</a>
						@if(auth()->user()->hasRole('wellfit_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('colour_categories.edit',$colour_category->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection