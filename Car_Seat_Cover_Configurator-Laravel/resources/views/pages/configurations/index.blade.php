@extends('home-user')
@section('selection-content')
    <div class="row">
        <div class="col-md-12">
            <h2>My Orders</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Configuration id</th>
                    <th>Slected Image</th>
                    <th>Configuration Image</th>
                    <th>Selections</th>
                </tr>
                </thead>
                <tbody>
                @foreach($configurations as $configuration)
                    <tr>
                        <td >{{$configuration->id}}</td>
                        <td>
                            
                                <img class="img-responsive" src="{{ $configuration->selected_image }}" width="150">
                        </td>
                        
                        <td >
                            
                        @include('pages.configurations.partials.config_image', [$image_array=$configuration->image])

                        </td>
                        
                        <td>
                            @foreach(json_decode($configuration->selections) as $selection)
                                {{$selection->label}} : {{$selection->value}}<br/>
                            @endforeach
                            @php
                           // print_r(json_decode($configuration->image) )
                            

                            @endphp
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $configurations->links() }}
        </div>
    </div>
    

@endsection