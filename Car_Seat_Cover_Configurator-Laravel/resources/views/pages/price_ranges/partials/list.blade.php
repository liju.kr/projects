@extends('pages.price_ranges.home')

@section('sub-content')
<div class="row conf-container">
  @foreach($price_ranges as $price_range)
      <div class="col-md-2 conf-block">

            <h2>{{$price_range->name}}</h2>
          <h6>{{$price_range->price}} AED</h6>

        <div class="action-container">

            <a href="{{route('price_ranges.edit', $price_range->id) }}"><button class="btn btn-primary">Edit</button></a>
            @if($price_range->seats()->count())
                {{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
                {{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
                {{ Form::close() }}
            @else

            {{ Form::open(array('style' => 'display:inline-block;', 'url' => 'price_ranges/'.$price_range->id )) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
            {{ Form::close() }}
                @endif
        </div>

      </div>
  @endforeach
</div>
@endsection