@extends('pages.price_ranges.home')

@section('sub-content')
	<div class="row">

    <div class="col-md-6">
		{!! Form::open(array('route' => 'price_ranges.store','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Price Range Name">
		    </div>
		  </div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Price</label>
			<div class="col-sm-10">
				<input type="number" min="0" class="form-control" name="price" placeholder="Price Range Price">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary pull-right">Add</button>
			</div>
		</div>

		{!! Form::close() !!}
	</div>
	</div>
@endsection