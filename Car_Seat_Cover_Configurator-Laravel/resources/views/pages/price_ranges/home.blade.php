@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a> >
                    <a href="{{route('price_ranges.index')}}">
                        Price Ranges
                    </a>


                    <div class="pull-right">

                            <a href="{{route('price_ranges.create')}}">
                                +Add New
                            </a>

                    </div>
                </div>

                <div class="panel-body">
                    @yield('sub-content')
                </div>
            </div>
        </div>
    </div>
</div>                    
@endsection