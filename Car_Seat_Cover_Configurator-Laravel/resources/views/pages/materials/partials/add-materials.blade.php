@extends('pages.materials.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-6">
		{!! Form::open(array('route' => 'materials.store','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
		  <div class="form-group">
		    <label for="Car Name" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Material Name">
		    </div>
		  </div>
		<div class="form-group">
			<label for="Car Name" class="col-sm-2 control-label">Min Price</label>
			<div class="col-sm-10">
				<input type="number" class="form-control" name="min_price" min="0">
			</div>
		</div>
		<div class="form-group">
			<label for="Manufacturer Logo" class="col-sm-2 control-label">Image</label>
			<div class="col-sm-10">
				<input type="file" name="image" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary pull-right">Add</button>
			</div>
		</div>

		{!! Form::close() !!}
	</div>
	</div>
@endsection