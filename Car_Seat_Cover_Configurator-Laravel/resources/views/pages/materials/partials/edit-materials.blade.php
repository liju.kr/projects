@extends('pages.materials.home')

@section('sub-content')
	<div class="row">
		<div class="col-md-8">
			{!! Form::open(array('route' => ['materials.update',$material->id],'method' => 'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="name" placeholder="Material Name" value="{{ $material->name }}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Min Price</label>
				<div class="col-sm-10">
					<input type="number" class="form-control" name="min_price" min="0" value="{{ $material->min_price }}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Image</label>
				<div class="col-sm-10">
					<img src="{{ $material->material_image }}">
					<input type="file" name="image" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary pull-right">Edit</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('yaco_data_manager'))
							@if(0)
								{{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
								{{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
								{{ Form::close() }}
							@else
								{{ Form::open(array('style' => 'display:inline-block;', 'url' => 'materials/'.$material->id )) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
								{{ Form::close() }}
							@endif
						@endif
					</td>
				</tr>
			</table>
		</div>

	</div>
@endsection