@extends('pages.materials.home')

@section('sub-content')

    <div class="row conf-container">
        @foreach($materials as $material)
            <div class="col-sm-2 home-card">
                <div class="thumbnail">
                    <div class="caption text-center">
                        <div class="position-relative  brand-card">
                            <img src="{{$material->material_image}}" class="img-responsive">
                        </div>
                        <p><span class="badge badge-warning">{{$material->name}}</span><span class="badge badge-success">{{$material->min_price}} AED</span></p>
                    </div>
                    @if(auth()->user()->hasRole('yaco_data_manager'))
                        <p class="text-center z-index">
                            <a href="{{route('materials.edit',$material->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
                        </p>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection