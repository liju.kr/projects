@extends('pages.company.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-6">
		{!! Form::open(['route' => 'companies.store', 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
		  <div class="form-group">
		    <label  class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Company Name">
		    </div>
		  </div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">Database Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="database_name" placeholder="Database Name">
			</div>
		</div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">Previous Database Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="previous_database_name" placeholder="Previous Database Name">
			</div>
		</div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">One Zero</label>
			<div class="col-sm-10">
				<input type="number" min="0" max="1" class="form-control" name="one_zero">
			</div>
		</div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Add</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
	</div>
@endsection