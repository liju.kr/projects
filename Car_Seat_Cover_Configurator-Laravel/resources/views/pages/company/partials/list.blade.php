@extends('pages.company.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($companies as $company)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<a href="">
						<div class="caption text-center">
							<h5>{{ $company->name }}</h5>
							<p><span class="badge badge-success">{{$company->one_zero}}</span><br><span class="badge badge-warning">{{$company->database_name}}</span></p>
						</div>
						</a>
						@if(auth()->user()->hasRole('wellfit_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('companies.edit',$company->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection