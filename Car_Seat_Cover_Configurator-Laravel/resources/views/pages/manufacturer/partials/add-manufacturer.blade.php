@extends('pages.manufacturer.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-6">
		{!! Form::open(['route' => 'manufacturers.store', 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
		  <div class="form-group">
		    <label for="Manufacturer Name" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Manufacturer Name">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="Manufacturer Logo" class="col-sm-2 control-label">Logo</label>
		    <div class="col-sm-10">
		      <input type="file" name="image" class="form-control" id="Manufacturer Logo" placeholder="Manufacturer Logo">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Add</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
	</div>
@endsection