@extends('pages.manufacturer.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($manufacturers as $manufacturer)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<a href="{{route('manufacturers.cars.index',$manufacturer->getId())}}">
						<div class="caption text-center">
							<div class="position-relative  brand-card">
								<img src="{{url('storage/'.$manufacturer->getImageURL())}}" class="img-responsive">
							</div>
							<p><span class="badge badge-warning">{{$manufacturer->getName()}}</span></p>
						</div>
						</a>
						@if(auth()->user()->hasRole('wellfit_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('manufacturers.edit',$manufacturer->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection