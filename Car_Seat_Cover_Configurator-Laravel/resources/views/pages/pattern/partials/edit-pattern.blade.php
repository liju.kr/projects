@extends('pages.pattern.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-8">
		{!! Form::open(['route' => array('patterns.update', $pattern->id), 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
		{{--{!! Form::open(array('route' => array('patterns.update', $pattern->id),'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}--}}
		  <div class="form-group">
		    <label for="Manufacturer Name" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Manufacturer Name" value="{{$pattern->getName()}}">
		    </div>
		  </div>
		<div class="form-group">
			<label for="Manufacturer Name" class="col-sm-2 control-label">Code</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="code"  value="{{$pattern->code}}">
			</div>
		</div>
		<div class="form-group">
			<label  class="col-sm-2 control-label">Extra Price</label>
			<div class="col-sm-10">
				<input type="number" min="0" class="form-control" name="extra_price"  value="{{$pattern->extra_price}}">
			</div>
		</div>
		  <div class="form-group">
		    <label  class="col-sm-2 control-label">Photo</label>
		    <div class="col-sm-10">
				<img src="{{Storage::url($pattern->getImageURL())}}">
		      <input type="file" name="image" class="form-control">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Update</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
		<div class="col-md-4 text-right">

		</div>
		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Created On</td>
					<td>{{ $pattern->createdOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated On</td>
					<td>{{ $pattern->updatedOn() }}</td>
				</tr>

				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('yaco_data_manager'))
							@if($pattern->getId() == env('DEFAULT_PATTERN_ID') || $pattern->variants()->count() || $pattern->layers()->count())
								{{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
								{{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
								{{ Form::close() }}
							@else
								{{ Form::open(array('url' => 'patterns/'.$pattern->getId(), 'style' => 'display:inline-block;')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
								{{ Form::close() }}
							@endif
						@endif
					</td>
				</tr>
			</table>
		</div>


	</div>
@endsection