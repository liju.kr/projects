@extends('pages.pattern.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($patterns as $pattern)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<div class="caption text-center">
							<div class="position-relative  brand-card">
								<img src="{{url('storage/'.$pattern->getImageURL())}}" class="img-responsive">
							</div>
							<p><span class="badge badge-warning">{{$pattern->getName()}}</span></p>
							<p><span class="badge badge-warning">{{$pattern->code}}</span> <span class="badge badge-success">{{$pattern->extra_price}} AED</span></p>
						</div>
						@if(auth()->user()->hasRole('yaco_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('patterns.edit',$pattern->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection