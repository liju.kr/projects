@extends('pages.pattern.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-6">
		{!! Form::open(['route' => 'patterns.store', 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
		  <div class="form-group">
		    <label for="Manufacturer Name" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Name">
		    </div>
		  </div>
		<div class="form-group">
			<label  class="col-sm-2 control-label">Code</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="code" placeholder="Code">
			</div>

			<div class="form-group">
				<label  class="col-sm-2 control-label">Extra Price</label>
				<div class="col-sm-10">
					<input type="number" min="0" class="form-control" name="extra_price">
				</div>
		</div>
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Photo</label>
		    <div class="col-sm-10">
		      <input type="file" name="image" class="form-control">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Add</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
	</div>
@endsection