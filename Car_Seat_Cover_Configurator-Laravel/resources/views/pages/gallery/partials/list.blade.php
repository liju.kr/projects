@extends('pages.gallery.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($galleries as $gallery)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<div class="caption text-center">
							<div class="position-relative  brand-card">
								<img src="{{$gallery->gallery_image}}" class="img-responsive">
							</div>
							<p><span class="badge badge-warning">{{$gallery->name}}</span></p>
						</div>
						@if(auth()->user()->hasRole('yaco_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('galleries.edit',$gallery->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
		  <div class="col-sm-12">
			  {{ $galleries->links() }}
		  </div>
	</div>
@endsection