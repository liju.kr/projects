@extends('pages.gallery.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-6">
		{!! Form::open(['route' => ['gallery_categories.galleries.store',$gallery_category->id], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Gallery Name">
		    </div>
		  </div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">Car Brand</label>
			<div class="col-sm-10">
				<select name="car_brand" onchange="geCarBrand()" class="form-control">
					<option value="">-Select Any-</option>
					@foreach($manufactures as $manufacturer)
						<option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group"  id="dynamic_section_car"></div>

		<div class="form-group" id="dynamic_section_layout"></div>

		  <div class="form-group">
		    <label for="Manufacturer Logo" class="col-sm-2 control-label">Image</label>
		    <div class="col-sm-10">
		      <input type="file" name="image" class="form-control">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Add</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
	</div>
	<script>
		function geCarBrand()
		{
			var car_brand_id = $("select[name='car_brand']").val();
			if (car_brand_id)
			{
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					url: '/car_brand_models',
					data: { car_brand_id: car_brand_id },
					success: function (data) {
						document.getElementById("dynamic_section_car").innerHTML=data.options;
					}
				});

			}

		}

		function getCar()
		{
			var car_id = $("select[name='car']").val();
			if (car_id)
			{
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					url: '/car_model_layouts',
					data: { car_id: car_id },
					success: function (data) {
						document.getElementById("dynamic_section_layout").innerHTML=data.options;
					}
				});

			}

		}
	</script>
@endsection

