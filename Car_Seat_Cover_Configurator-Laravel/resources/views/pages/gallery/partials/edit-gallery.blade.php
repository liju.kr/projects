@extends('pages.gallery.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-8">
		{!! Form::open(['route' => array('galleries.update', $gallery->id), 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
		{{--{!! Form::open(array('route' => array('galleries.update', $gallery->id),'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}--}}
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Gallery Name" value="{{$gallery->name}}">
		    </div>
		  </div>
		<div class="form-group">
			<label  class="col-sm-2 control-label">Car Brand</label>
			<div class="col-sm-10">
				<select name="car_brand" onchange="geCarBrand()" class="form-control">
					<option value="" @if($car_brand_id==null) selected @endif>-Select Any-</option>
					@foreach($manufactures as $manufacturer)
						<option value="{{ $manufacturer->id }}" @if($car_brand_id==$manufacturer->id) selected @endif>{{ $manufacturer->name }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group"  id="dynamic_section_car">
			@if(isset($cars))
			<label  class="col-sm-2 control-label">Car</label>
			<div class="col-sm-10">
				<select name="car" onchange="getCar()" class="form-control">
					<option value="" @if($car_id==null) selected @endif>-Select Any-</option>
					@foreach($cars as $car)
						<option value="{{ $car->id }}" @if($car_id==$car->id) selected @endif>{{ $car->name }}</option>
					@endforeach
				</select>
			</div>
			@endif
		</div>

		<div class="form-group" id="dynamic_section_layout">
			@if(isset($car_models))
			<label  class="col-sm-2 control-label">Layouts</label>
			<div class="col-sm-10">
				@foreach($car_models as $car_model)
					@if($car_model->layouts()->count())
						<strong> {{ $car_model->name }}</strong>&nbsp;&nbsp;
						@foreach($car_model->layouts as $layout)
							<input type="checkbox" name="layouts[]" value="{{ $layout->id }}" {{in_array($layout->id,$assignedLayouts)?'checked':''}}>{{ $layout->name }} &nbsp;
						@endforeach
						<hr>
					@endif
				@endforeach
			</div>
			@endif
		</div>

		  <div class="form-group">
		    <label class="col-sm-2 control-label">Image</label>
		    <div class="col-sm-10">
				<img src="{{ $gallery->gallery_image }}">
		      <input type="file" name="image" class="form-control">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Update</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
		<div class="col-md-4 text-right">

		</div>
		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('yaco_data_manager'))
							@if($gallery->layouts()->count())
								{{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
								{{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
								{{ Form::close() }}
								@else
								{{ Form::open(array('url' => 'galleries/'.$gallery->id, 'style' => 'display:inline-block;')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
								{{ Form::close() }}
							@endif
						@endif
					</td>
				</tr>
			</table>
		</div>


	</div>

	<script>
		function geCarBrand()
		{
			var car_brand_id = $("select[name='car_brand']").val();
			if (car_brand_id)
			{
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					url: '/car_brand_models',
					data: { car_brand_id: car_brand_id },
					success: function (data) {
						document.getElementById("dynamic_section_car").innerHTML=data.options;
						document.getElementById("dynamic_section_layout").innerHTML="";
					}
				});

			}

		}

		function getCar()
		{
			var car_id = $("select[name='car']").val();
			if (car_id)
			{
				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: 'POST',
					url: '/car_model_layouts',
					data: { car_id: car_id },
					success: function (data) {
						document.getElementById("dynamic_section_layout").innerHTML=data.options;
					}
				});

			}

		}
	</script>
@endsection