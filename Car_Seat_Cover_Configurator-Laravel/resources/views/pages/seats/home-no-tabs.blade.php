@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a> >
                    <a href="{{route('manufacturers.index')}}">
                        Manufacturers
                    </a>>
                    <a href="{{route('manufacturers.cars.index',$car_model->car->manufacturer->getId())}}">
                        {{$car_model->car->manufacturer->getName()}}
                    </a>>
                    <a href="{{route('cars.models.index',$car_model->car->getId())}}">
                        {{$car_model->car->getName()}}
                    </a>>
                    @if(auth()->user()->hasRole('yaco_data_manager'))
                    <a href="{{route('models.seats.index',$car_model->getId())}}">
                        {{$car_model->getName()}} Seats
                    </a>
                    @endif
                    @if(auth()->user()->hasRole('wellfit_data_manager'))
                        <a href="{{route('models.layouts.index',$car_model->getId())}}">
                            {{$car_model->getName()}} Layouts
                        </a>
                    @endif
                    <div class="dash-heading">@if(auth()->user()->hasRole('yaco_data_manager'))SEAT DESIGNS @endif  @if(auth()->user()->hasRole('wellfit_data_manager'))LAYOUTS @endif</div>
                    
                </div>

                <div class="panel-body">
                        <div class="tab-content">
                        @yield('sub-content')
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>                    
@endsection