@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a> >
                        <a href="{{route('manufacturers.index')}}">
                            Manufacturers
                        </a>
                        @if($layout->car_model->car->manufacturer)
                        >
                        @endif
                        <a href="{{route('manufacturers.cars.index',$layout->car_model->car->manufacturer->getId())}}">
                            {{$layout->car_model->car->manufacturer->getName()}}
                        </a>
                        @if($layout->car_model->car)
                        >
                        <a href="{{route('cars.models.index',$layout->car_model->car->getId())}}">
                            {{$layout->car_model->car->getName()}}
                        </a>
                        @endif
                        @if($layout->car_model)
                        >
                            @if(auth()->user()->hasRole('yaco_data_manager'))
                                <a href="{{route('models.seats.index',$layout->car_model->getId())}}">
                                    {{$layout->car_model->getName()}} Seats
                                </a>
                            @endif
                            @if(auth()->user()->hasRole('wellfit_data_manager'))
                                <a href="{{route('models.layouts.index',$layout->car_model->getId())}}">
                                    {{$layout->car_model->getName()}} Layouts
                                </a>
                            @endif
                        @endif
                        <div class="dash-heading">{{ $layout->name }} </div>

                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3 style="padding-bottom:25px;">{{$layout->name}}</h3>
                                        <ul class="list-group">
{{--                                            @foreach($layout->materials as $material_price)--}}
{{--                                            <li class="list-group-item">{{$material_price->name}} :<span> {{$material_price->pivot->price}} AED </span></li>--}}
{{--                                            @endforeach--}}
                                            <li class="list-group-item">Brand  : <span>{{$layout->car_model->car->manufacturer->getName()}} </span></li>
                                            <li class="list-group-item">Car  : <span>{{$layout->car_model->car->getName()}} </span></li>
                                            <li class="list-group-item">Car Model Year : <span>{{$layout->car_model->getName()}} </span></li>
                                            <li class="list-group-item">Created On : <span>{{ $layout->createdOn() }} </span></li>
                                            <li class="list-group-item">Last Updated On : <span>{{ $layout->updatedOn() }} </span></li>
                                            <li class="list-group-item">Last Updated By : <span class="text-capitalize">{{ $layout->updatedBy() }} </span></li>
                                            @if(auth()->user()->hasRole('wellfit_data_manager'))
                                            <li class="list-group-item">
                                                <span>
                                                    <a href="{{route('layouts.edit',$layout->getId())}}"><button class="btn btn-primary pull-right">Edit</button></a>

                                                    @if(env('EMBROIDERY_LAYOUT_ID')==$layout->getId())
                                                        {{ Form::open(array('url' => '')) }}
                                                        {{ Form::button('Lock', array('class' => 'btn btn-warning','onclick'=>"return alert('Has Many Relations !!');")) }}
                                                        {{ Form::close() }}
                                                    @else
                                                        {{ Form::open(array('url' => 'layouts/'.$layout->getId())) }}
                                                        {{ Form::hidden('_method', 'DELETE') }}
                                                        {{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
                                                        {{ Form::close() }}
                                                    @endif
                                                </span>
                                            </li>
                                                @endif

                                        </ul>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <h4>
                                                Layout Images
                                            </h4>
                                            @foreach($layout->images as $layout_image)
                                                <div class="col-sm-4 home-card">
                                                    <div class="thumbnail">
                                                        <a href="{{ $layout_image->layout_image }}" target="_blank">
                                                            <div class="caption text-center">
                                                                <div class="position-relative">
                                                                    <img src="{{ $layout_image->layout_image }}" class="img-responsive">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="row">
                                            <h4>
                                                Layout Designs
                                            </h4>
                                            @foreach($layout->layout_designs as $layout_design)
                                                <div class="col-sm-4 home-card">
                                                    <div class="thumbnail">
                                                        <a href="{{ $layout_design->layout_image }}" target="_blank">
                                                            <div class="caption text-center">
                                                                <div class="position-relative">
                                                                    <img src="{{ $layout_design->layout_image }}" class="img-responsive">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @if($layout_design->name) <p class="text-center"><span class="badge badge-warning">{{ $layout_design->name }}</span></p> @endif
                                                        @if($layout_design->description) <p class="text-center"><span class="badge badge-primary">{{ $layout_design->description }}</span></p> @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection