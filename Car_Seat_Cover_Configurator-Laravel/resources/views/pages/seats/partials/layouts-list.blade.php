@extends('pages.seats.home')

@section('sub-content')
<div class="row conf-container container-fluid">
  @foreach($car_model->layouts as $layout)
    <div class="col-sm-2 home-card">
      <div class="thumbnail">
        <a href="{{route('layouts.show',$layout->getId())}}">
          <div class="caption text-center">
            <div class="position-relative">
              <img src="{{ $layout->layout_image }}" class="img-responsive">
            </div>
            {{--      @foreach($layout->materials as $material_price)--}}
            {{--      <h5>{{$material_price->name}} : {{$material_price->pivot->price}} AED</h5>--}}
            {{--      @endforeach--}}
            <p><span class="badge badge-warning">{{$layout->getName()}}</span></p>
            @if(auth()->user()->hasRole('wellfit_data_manager'))
            <p class="text-center z-index"><a href="{{route('layouts.edit',$layout->getId())}}"><button class="btn btn-primary btn-sm">Edit</button></a></p>
              @endif
          </div>
        </a>
      </div>
    </div>
  @endforeach
</div>
@endsection
