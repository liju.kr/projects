@extends('pages.seats.home')

@section('sub-content')
    <div class="row">

        <div class="col-md-12">
            {!! Form::open(['route' => ['models.layouts.store',$car_model->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="Layout Name" class="col-sm-2 control-label">Layout Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="Layout Name">
                </div>
            </div>
            <div class="form-group">
                <label for="more_information" class="col-sm-2 control-label">More Information</label>
                <div class="col-sm-10">
                    <input type="text"  class="form-control" name="more_information" placeholder="More Information">
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <input type="text"  class="form-control" name="description" placeholder="Description">
                </div>
            </div>
{{--            <h4>Price Based On Materials (AED)</h4>--}}
{{--            @foreach($materials as $material)--}}
{{--                <div class="form-group">--}}
{{--                    <label for="price{{ $material->id }}" class="col-sm-4 control-label">{{ $material->name }}</label>--}}
{{--                    <input type="hidden" name="material_id[]">--}}
{{--                    <div class="col-sm-8">--}}
{{--                        <input type="number" id="price{{ $material->id }}" min="0" value="0" class="form-control" name="price[]" placeholder="Price AED">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}

            <div class="form-group">
                <label for="Layout Image" class="col-sm-2 control-label">Layout Images</label>
                <div class="col-sm-10">
                    <table class="table table-bordered" id="dynamicAddRemove">
                        <tr>
                            <td><img src="{{ asset('/images/no_image.png') }}" id="preview0" style="height: 50px;"> </td>
                            <td><input type="number" name="sort[]" min="0" value="0" required class="form-control" /></td>
                            <td><input type="file" name="image[]" id="imgInp0"  class="form-control" /></td>
                            <td><button type="button" name="add" id="add-btn" class="btn btn-success">Add More</button></td>
                        </tr>
                    </table>

                </div>
            </div>

            <div class="form-group">
                <label for="Layout Image" class="col-sm-2 control-label">Layout Designs</label>
                <div class="col-sm-10">
                    <table class="table table-bordered" id="dynamicAddRemove1">
                        <tr>
                            <td><img src="{{ asset('/images/no_image.png') }}" id="preview01" style="height: 50px;"> </td>
                            <td><input type="number" name="design_sort[]" min="0" value="0" required class="form-control" /></td>
                            <td><input type="text" name="design_name[]" placeholder="Name"  class="form-control" /></td>
                            <td><input type="text" name="design_description[]" placeholder="Description"  class="form-control" /></td>
                            <td><input type="file" name="design_image[]" id="imgInp01"  class="form-control" /></td>
                            <td><button type="button" name="add" id="add-btn1" class="btn btn-success">Add More</button></td>
                        </tr>
                    </table>

                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary pull-left">Add</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>



    <script type="text/javascript">
        var i = 0;
        $("#add-btn").click(function(){
            ++i;
            $("#dynamicAddRemove").append('<tr><td><img src="{{ asset('/images/no_image.png') }}" id="preview'+i+'" style="height: 50px;"></td><td><input type="number" name="sort[]" min="0" value="0" class="form-control" /></td><td><input type="file" name="image[]" id="imgInp'+i+'" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
            @for($i=1; $i<=50; $i++)
            $("#imgInp{{$i}}").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#preview{{$i}}").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
            @endfor
        });
        $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
        });

        $("#imgInp0").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview0').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });



        var j = 0;
        $("#add-btn1").click(function(){
            ++j;
            $("#dynamicAddRemove1").append('<tr><td><img src="{{ asset('/images/no_image.png') }}" id="preview1'+j+'" style="height: 50px;"></td><td><input type="number" name="design_sort[]" min="0" value="0" class="form-control" /></td><td><input type="text" name="design_name[]" placeholder="Name" class="form-control" /></td><td><input type="text" name="design_description[]" placeholder="Description" class="form-control" /></td><td><input type="file" name="design_image[]" id="imgInp1'+j+'" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr1">Remove</button></td></tr>');
            @for($j=1; $j<=50; $j++)
            $("#imgInp1{{$j}}").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#preview1{{$j}}").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
            @endfor
        });
        $(document).on('click', '.remove-tr1', function(){
            $(this).parents('tr').remove();
        });

        $("#imgInp01").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview01').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });


    </script>
@endsection