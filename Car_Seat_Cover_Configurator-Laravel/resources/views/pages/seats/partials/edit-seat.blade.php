@extends('pages.seats.home-no-tabs')
@section('sub-content')
    <div class="row">

        <div class="col-md-6">
            {!! Form::open(['route' => ['seats.update',$seat->getId()],'method'=>'put', 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="Seat Name" class="col-sm-2 control-label">Seat Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="Seat Name"
                           value="{{$seat->getName()}}">
                </div>
            </div>

            <div class="form-group">
                <label for="Seat Name" class="col-sm-2 control-label">Extra Price</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="extra_price" value="{{$seat->extra_price}}" min="0" >
                </div>
            </div>

            <div class="form-group">

                <label for="Seat Image" class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <img src="{{Storage::url($seat->getImageURL())}}">
                    <input type="file" name="image" class="form-control" id="Seat Image" placeholder="Seat Image">
                </div>
            </div>
            @if(env('EMBROIDERY_CAR_MODEL_ID')==$car_model->id)
            <div class="form-group row">
                <label for="" class="col-sm-2 control-label">Price Range</label>
                <div class="col-sm-10">
                    @foreach ($price_ranges as $price_range )
                        <input type="checkbox" name="price_range[]"  id="price_range{{ $price_range->id }}" value="{{ $price_range->id }}" {{in_array($price_range->id,$assignedRanges)?'checked':''}}>
                        <label for="price_range{{ $price_range->id }}">{{ $price_range->name }}</label> &nbsp; &nbsp;
                    @endforeach
                </div>
            </div>
          @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
            <div class="col-md-6 text-right delete-form">

            </div>
    </div>
@endsection