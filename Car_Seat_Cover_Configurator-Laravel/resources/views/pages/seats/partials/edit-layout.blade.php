@extends('pages.seats.home-no-tabs')

@section('sub-content')
    <div class="row">

            <div class="col-md-12">
                <div class="col-md-3">
                    <h2>{{$car_model->car->manufacturer->getName()}}</h2>
                </div>
                <div class="col-md-3">
                    <h2>{{$car_model->car->getName()}}</h2>
                </div>
                <div class="col-md-3">
                    <h2>{{$car_model->getName()}}</h2>
                </div>
                <div class="col-md-3">
                    <h2>{{$layout->getName()}}</h2>
                </div>
            </div>
        <div class="col-md-12">
            {!! Form::open(['route' => ['layouts.update',$layout->getId()],'method'=>'put', 'enctype' => 'multipart/form-data','class' => 'form-horizontal','id' => 'form']) !!}
            <div class="form-group">
                <label for="Layout Name" class="col-sm-2 control-label">Layout Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="Layout Name"
                           value="{{$layout->getName()}}">
                </div>
            </div>
            <div class="form-group">
                <label for="more_information" class="col-sm-2 control-label">More Information</label>
                <div class="col-sm-10">
                    <input type="text"  class="form-control" name="more_information" placeholder="More Information"value="{{$layout->more_information}}">
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <input type="text"  class="form-control" name="description" placeholder="Description"value="{{$layout->description}}">
                </div>
            </div>


            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Change Model Year</label>
                <div class="col-sm-5">
                    <input type="number" min="0" max="{{ date('Y')+2 }}" class="form-control" name="year_from"  value="{{$layout->car_model->year_from}}">
                </div>
                <div class="col-sm-5">
                    <input type="number" min="0" max="{{ date('Y')+2 }}" class="form-control" name="year_to"  value="{{$layout->car_model->year_to}}">
                </div>
            </div>

{{--            <h4>Price Based On Materials (AED)</h4>--}}
{{--            @foreach($materials as $index=>$material)--}}
{{--                <div class="form-group">--}}
{{--                    <label for="price{{ $material->id }}" class="col-sm-4 control-label">{{ $material->name }}</label>--}}
{{--                    <input type="hidden" name="material_id[]">--}}
{{--                    <div class="col-sm-8">--}}
{{--                        <input type="number" id="price{{ $material->id }}" min="0"  @if($layout->materials()->count()) value="{{ $layout->materials[$index]->pivot->price }}" @else value="0"  @endif class="form-control" name="price[]" placeholder="Price AED">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--            <div class="form-group">--}}
{{--                <label for="Layout Image" class="col-sm-2 control-label">Image</label>--}}
{{--                <div class="col-sm-10">--}}
{{--                    <img src="{{Storage::url($layout->getImageURL())}}">--}}
{{--                    <input type="file" name="image" class="form-control" id="Layout Image" placeholder="Layout Image">--}}
{{--                </div>--}}

                <div class="form-group">
                    <label for="Layout Image" class="col-sm-2 control-label">Layout Images</label>
                    <div class="col-sm-10">
                        <table class="table table-bordered" id="dynamicAddRemove">
                            @foreach($layout->images as $key=>$layout_image)
                            <tr>
                                <td><img src="{{ $layout_image->layout_image }}" id="preview_old{{ $layout_image->id }}" style="height: 50px;"> </td>
                                <td><input type="hidden" name="layout_image_id[{{$key}}]" value="{{ $layout_image->id }}"  class="form-control" />
                                    <input type="number" name="sort_order[{{$key}}]" min="0" value="{{ $layout_image->sort }}" required class="form-control" />
                                </td>
                                <td><input type="file" name="image_preview[{{$key}}]" id="imgInp_old{{ $layout_image->id }}"  class="form-control" /></td>
                                <td>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td><img src="{{ asset('/images/no_image.png') }}" id="preview0" style="height: 50px;"> </td>
                                <td><input type="number" name="sort[]" min="0" value="0" required class="form-control" /></td>
                                <td><input type="file" name="image[]" id="imgInp0"  class="form-control" /></td>
                                <td><button type="button" name="add" id="add-btn" class="btn btn-success">Add More</button></td>
                            </tr>
                        </table>

                    </div>
                </div>


            <div class="form-group">
                <label for="Layout Image" class="col-sm-2 control-label">Layout Deigns</label>
                <div class="col-sm-10">
                    <table class="table table-bordered" id="dynamicAddRemove1">
                        @foreach($layout->layout_designs as $key1=>$layout_design)
                            <tr>
                                <td><img src="{{ $layout_design->layout_image }}" id="preview_old1{{ $layout_design->id }}" style="height: 50px;"> </td>
                                <td>
                                    <input type="hidden" name="design_layout_image_id[{{$key1}}]" value="{{ $layout_design->id }}"  class="form-control" />
                                    <input type="number" name="design_sort_order[{{$key1}}]" min="0" value="{{ $layout_design->sort }}" required class="form-control" />
                                </td>
                                <td>
                                    <input type="text" name="design_name[{{$key1}}]" placeholder="Name" value="{{ $layout_design->name }}" class="form-control" />
                                </td>
                                <td>
                                    <input type="text" name="design_description[{{$key1}}]" placeholder="Description" value="{{ $layout_design->description }}" class="form-control" />
                                </td>
                                <td><input type="file" name="design_image_preview[{{$key1}}]" id="imgInp_old1{{ $layout_design->id }}"  class="form-control" /></td>
                                <td>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><img src="{{ asset('/images/no_image.png') }}" id="preview01" style="height: 50px;"> </td>
                            <td><input type="number" name="design_sort[]" min="0" value="0" required class="form-control" /></td>
                            <td><input type="text" name="design1_name[]" min="0" placeholder="Name"  class="form-control" /></td>
                            <td><input type="text" name="design1_description[]"  placeholder="Description" class="form-control" /></td>
                            <td><input type="file" name="design_image[]" id="imgInp01"  class="form-control" /></td>
                            <td><button type="button" name="add" id="add-btn1" class="btn btn-success">Add More</button></td>
                        </tr>
                    </table>

                </div>
            </div>

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
{{--            <div class="col-md-6">--}}
{{--                {!! Form::open(['route' => ['layout_image.store'],'method'=>'post','enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}--}}
{{--                <label for="Layout Image" class="col-sm-2 control-label">Images</label>--}}
{{--                <div class="form-group">--}}



{{--                    <div class="col-sm-12">--}}
{{--                        <input type="hidden" name="layout_id" value="{{$layout->getId()}}">--}}
{{--                        <input type="file" name="image" class="form-control" id="Layout Image" placeholder="Layout Image">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <div class="col-sm-offset-2 col-sm-10">--}}
{{--                        <button type="submit" class="btn btn-primary pull-right">Upload</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                {!! Form::close() !!}--}}
{{--            </div>--}}
            <div class="col-sm-12">
                <h4>Layout Images</h4>
                @foreach($layout->images as $image)
                    <div class="col-sm-2 home-card">
                        <div class="thumbnail  delete-form">
                            <a href="{{ $image->layout_image }}" target="_blank">
                                <div class="caption text-center">
                                    <div class="position-relative">
                                        <img src="{{ $image->layout_image }}" class="img-responsive">
                                    </div>
                                </div>
                            </a>
                            <div class="text-center">
                                {{ Form::open(array('url' => 'layout_image/'.$image->id)) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Remove', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ?');")) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        <div class="col-sm-12">
            <h4>Layout Designs</h4>
            @foreach($layout->layout_designs as $layout_design)
                <div class="col-sm-2 home-card">
                    <div class="thumbnail  delete-form">
                        <a href="{{ $layout_design->layout_image }}" target="_blank">
                            <div class="caption text-center">
                                <div class="position-relative">
                                    <img src="{{ $layout_design->layout_image }}" class="img-responsive">
                                </div>
                            </div>
                        </a>
                       @if($layout_design->name) <p class="text-center"><span class="badge badge-warning">{{ $layout_design->name }}</span></p> @endif
                       @if($layout_design->description) <p class="text-center"><span class="badge badge-primary">{{ $layout_design->description }}</span></p> @endif
                        <div class="text-center z-index">
                            {{ Form::open(array('url' => 'layout_design/'.$layout_design->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Remove', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ?');")) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


    </div>

    <script type="text/javascript">
        var i = 0;
        $("#add-btn").click(function(){
            ++i;
            $("#dynamicAddRemove").append('<tr><td><img src="{{ asset('/images/no_image.png') }}" id="preview'+i+'" style="height: 50px;"></td><td><input type="number" name="sort[]" min="0" value="0" class="form-control" /></td><td><input type="file" name="image[]" id="imgInp'+i+'" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
            @for($i=1; $i<=50; $i++)

            $("#imgInp{{$i}}").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#preview{{$i}}").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
            @endfor
        });
        $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
        });

        $("#imgInp0").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview0').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        @foreach($layout->images as $key=>$layout_image)
        $("#imgInp_old{{ $layout_image->id }}").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_old{{ $layout_image->id }}').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        @endforeach



        var j = 0;
        $("#add-btn1").click(function(){
            ++j;
            $("#dynamicAddRemove1").append('<tr><td><img src="{{ asset('/images/no_image.png') }}" id="preview1'+j+'" style="height: 50px;"></td><td><input type="number" name="design_sort[]" min="0" value="0" class="form-control" /></td><td><input type="text" name="design1_name[]"  placeholder="Name" class="form-control" /></td><td><input type="text" name="design1_description[]"  placeholder="Description" class="form-control" /></td><td><input type="file" name="design_image[]" id="imgInp1'+j+'" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr1">Remove</button></td></tr>');
            @for($j=1; $j<=50; $j++)

            $("#imgInp1{{$j}}").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#preview1{{$j}}").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
            @endfor
        });
        $(document).on('click', '.remove-tr1', function(){
            $(this).parents('tr').remove();
        });

        $("#imgInp01").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview01').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        @foreach($layout->layout_designs as $key1=>$layout_design)
        $("#imgInp_old1{{ $layout_design->id }}").change(function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_old1{{ $layout_design->id }}').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });

        @endforeach

    </script>
@endsection