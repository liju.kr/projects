@extends('pages.seats.home')
@section('sub-content')
<div class="row conf-container">
  @foreach($car_model->seats as $seat)
    <div class="col-sm-3 home-card">
      <div class="thumbnail delete-form">
        <a href="{{route('seats.configuration.index',$seat->getId())}}">
          <div class="caption text-center">
            <div class="position-relative">
              <img src="{{Storage::url($seat->getImageURL())}}" class="img-responsive">
            </div>
            <p><span class="badge badge-warning">{{$seat->getName()}}</span> <span class="badge badge-success">{{ $seat->extra_price }} AED</span> </p>
{{--            @if($seat->price_range_from) <p> AED {{$seat->price_range_from}} @endif - @if($seat->price_range_to) AED {{$seat->price_range_to}} </p> @endif--}}
{{--            <p>{{ $seat->price_ranges->pluck('name')->implode(', ') }}</p>--}}
          </div>
        </a>
        @if(auth()->user()->hasRole('yaco_data_manager'))
          <div class="text-center z-index">
            <a href="{{route('seats.edit',$seat->getId())}}"><button class="btn btn-sm btn-primary">Edit</button></a>
              {{ Form::open(array('url' => 'delete_seats/'.$seat->getId())) }}
              <input type="hidden" value="{{ $car_model->id }}" name="car_model_id">
              {{ Form::hidden('_method', 'DELETE') }}
              {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ? ');")) }}
              {{ Form::close() }}

              @if($seat->id == env('COMMON_MODEL_ID') || $seat->car_models()->count() > 1)
                {{ Form::open(array('url' => '', 'style' => 'display:inline-block;')) }}
                {{ Form::button('Lock', array('class' => 'btn btn-warning btn-sm','onclick'=>"return alert('Common Model!!');")) }}
                {{ Form::close() }}
              @else
                {{ Form::open(array('style' => 'display:inline-block;', 'url' => 'seats/'.$seat->getId())) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Hard Delete', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ?');")) }}
                {{ Form::close() }}
              @endif
          </div>
          @endif
        <ul class="list-inline">
          <li>

          </li>
          <li>

          </li>
        </ul>
      </div>
    </div>
  @endforeach
</div>
@endsection
