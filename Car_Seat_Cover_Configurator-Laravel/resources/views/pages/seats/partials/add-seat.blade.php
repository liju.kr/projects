@extends('pages.seats.home')

@section('sub-content')
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['route' => ['models.seats.store',$car_model->getId()], 'enctype' => 'multipart/form-data','class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="Seat Name" class="col-sm-2 control-label">Seat Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="Seat Name">
                </div>
            </div>

            <div class="form-group">
                <label for="Seat Name" class="col-sm-2 control-label">Extra Price</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="extra_price" min="0" >
                </div>
            </div>

            <div class="form-group">
                <label for="Seat Image" class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="Seat Image" placeholder="Seat Image">
                </div>
            </div>

          @if(env('EMBROIDERY_CAR_MODEL_ID')==$car_model->id)

            <div class="form-group row">
                <label for="" class="col-sm-2 control-label">Price Range</label>
                <div class="col-sm-10">
                  @foreach ($price_ranges as $price_range )
                   <input type="checkbox" name="price_range[]"  id="price_range{{ $price_range->id }}" value="{{ $price_range->id }}">
                  <label for="price_range{{ $price_range->id }}">{{ $price_range->name }}</label> &nbsp; &nbsp;
                @endforeach
                </div>
            </div>
           @endif


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary pull-right">Add</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
            @if(env('EMBROIDERY_CAR_MODEL_ID') ==$car_model->id || env('COMMON_CAR_MODEL_ID') ==$car_model->id)

                @else
             <div class="col-md-6">
                <h3>Choose From Existing</h3>
                {{ Form::open(array('url' => 'models/'.$car_model->getId().'/seats/existing')) }}
                <div class="form-group">
                    <label for="Seat Name" class="col-sm-2 control-label">Seat Name</label>
                    <div class="col-sm-10">
                       <select class="form-control" name="seat_id">
                           <option>-- Select Any --</option>

                           @foreach($seats as $seat)

                           <option value="{{ $seat->id }}">{{ $seat->id }} || {{ $seat->name }} ||@if(!empty($seat->car_model)) {{ $seat->car_model->name }}  @if(!empty($seat->car_model->car)) || {{ $seat->car_model->car->name }}  @if(!empty($seat->car_model->car->manufacturer)) || {{ $seat->car_model->car->manufacturer->name  }} @endif @endif @endif</option>
                               @endforeach




                       </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary pull-right">Add</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
                @endif
    </div>
@endsection