@extends('layouts.app')
@section('content')
  <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a> >
                        <a href="{{url('passport')}}">
                            Passport
                        </a>
                        <div class="dash-heading">Passport</div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <passport-clients></passport-clients>
                            <passport-authorized-clients></passport-authorized-clients>
                            <passport-personal-access-tokens></passport-personal-access-tokens>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection