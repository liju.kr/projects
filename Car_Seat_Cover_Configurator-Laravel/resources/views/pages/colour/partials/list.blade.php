@extends('pages.colour.home')

@section('sub-content')
	<div class="row conf-container">
	  @foreach($colours as $colour)
			<div class="col-sm-2 home-card">
					<div class="thumbnail">
						<div class="caption text-center">
							<div class="position-relative  brand-card">
								<img src="{{$colour->colour_image}}" class="img-responsive">
							</div>
							<p><span class="badge badge-warning">{{$colour->name}}</span></p>
							<p><span class="badge badge-warning">{{$colour->code}}</span></p>
						</div>
						@if(auth()->user()->hasRole('wellfit_data_manager'))
						<p class="text-center z-index">
							<a href="{{route('colours.edit',$colour->id)}}"><button class="btn btn-sm btn-primary">Edit</button></a>
						</p>
							@endif
					</div>
			</div>
	  @endforeach
	</div>
@endsection