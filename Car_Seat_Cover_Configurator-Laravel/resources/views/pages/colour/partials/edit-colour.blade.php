@extends('pages.colour.home')

@section('sub-content')
	<div class="row">
    <div class="col-md-8">
		{!! Form::open(['route' => array('colours.update', $colour->id), 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
		{{--{!! Form::open(array('route' => array('colours.update', $colour->id),'class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}--}}
		  <div class="form-group">
		    <label for="Manufacturer Name" class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="name" placeholder="Colour Name" value="{{$colour->name}}">
		    </div>
		  </div>
		<div class="form-group">
			<label for="Manufacturer Name" class="col-sm-2 control-label">Code</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="code" placeholder="Colour Code" value="{{$colour->code}}">
			</div>
		</div>
		  <div class="form-group">
		    <label for="Manufacturer Logo" class="col-sm-2 control-label">Image</label>
		    <div class="col-sm-10">
				<img src="{{ $colour->colour_image }}">
		      <input type="file" name="image" class="form-control">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary pull-right">Update</button>
		    </div>
		  </div>
		{!! Form::close() !!}
	</div>
		<div class="col-md-4 text-right">

		</div>
		<div class="col-md-4">
			<table class="table table-striped">
				<tr>
					<td> Created On</td>
					<td>{{ $colour->createdOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated On</td>
					<td>{{ $colour->updatedOn() }}</td>
				</tr>
				<tr>
					<td> Last Updated By</td>
					<td class="text-capitalize">{{ $colour->updatedBy() }}</td>
				</tr>
				<tr>
					<td> Action</td>
					<td class="delete-form">
						@if(auth()->user()->hasRole('wellfit_data_manager'))
								{{ Form::open(array('url' => 'colours/'.$colour->id, 'style' => 'display:inline-block;')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
								{{ Form::close() }}

						@endif
					</td>
				</tr>
			</table>
		</div>


	</div>
@endsection