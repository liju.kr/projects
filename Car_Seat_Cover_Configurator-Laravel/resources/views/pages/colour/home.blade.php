@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url('home')}}">Home </a>>
                    <a href="{{url('colour_categories')}}">Colours Categories</a>
                    > <a href="{{route('colour_categories.colours.index',$colour_category->id)}}">{{ $colour_category->name }}</a>
                    <div class="dash-heading">{{ $colour_category->name }}</div>
                @if(auth()->user()->hasRole('wellfit_data_manager'))
                <div class="pull-right">
                   <a href="{{route('colour_categories.colours.create', $colour_category->id)}}">+Add New</a>
                </div>
                 @endif
</div>
<div class="panel-body">
@yield('sub-content')
</div>
</div>
</div>
</div>
</div>                    
@endsection
