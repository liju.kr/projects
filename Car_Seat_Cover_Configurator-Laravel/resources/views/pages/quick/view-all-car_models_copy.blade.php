@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a>>
                        <a href="{{url('view_all_car_models')}}">Car Models (Years) </a>
                        <div class="dash-heading">Car Models (Years)</div>
                        <div class="pull-right">
                            <form action="" method="get" style="margin: 0px;">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <select class="form-control"  name="year_from">
                                            <option value="all" @if($year_from=="all") selected @endif>Year From (All)</option>
                                            @for($i=date('Y'); $i >=1987; $i--)
                                            <option value="{{ $i }}" @if($year_from==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <select class="form-control"  name="year_to" required>
                                            <option value="" @if($year_to=="") selected @endif>Year To (Select Any)</option>
                                            @for($j=date('Y'); $j >=1987; $j--)
                                                <option value="{{ $j }}" @if($year_to==$j) selected @endif>{{ $j }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="hidden" name="search_status" value="1">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                <div class="col-lg-2">
                                    <a href="{{ url('/view_all_car_models') }}" class="btn btn-info">Reset</a>
                                </div>
                        </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    {!! Form::open(array('url' => 'quick_car_model_edit','method'=>'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Layout</th>
                                            <th>Year From</th>
                                            <th>Year To</th>
                                            <th>Car Model</th>
                                            <th>Car</th>
                                            <th>Brand</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($car_models as $key=>$car_model)
                                        <tr>
                                            <td>
                                                {{ $key +1 }}
                                                <input type="checkbox" value="{{ $car_model->id }}" name="car_model_ids[]" {{ ( is_array(old('car_model_ids')) && in_array($car_model->id, old('car_model_ids')) ) ? 'checked ' : '' }}>
                                            </td>
                                            <td>
                                                @foreach($car_model->layouts as $layout)
                                                    <a href="{{ route('layouts.show', $layout->id) }}" target="_blank">
                                                <img src="{{ $layout->layout_image }}" style="height: 50px;">
                                                    </a>
                                                @endforeach
                                            </td>
                                            <td>{{ $car_model->year_from }}</td>
                                            <td>{{ $car_model->year_to }}</td>
                                            <td>{{ $car_model->name }}</td>
                                            <td>@if($car_model->car){{ $car_model->car->name }}@endif</td>
                                            <td>@if($car_model->car && $car_model->car->manufacturer){{ $car_model->car->manufacturer->name }}@endif</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input type="number" min="1987" max="{{ date('Y')+2 }}" name="new_year_from"   value="{{ old('new_year_from') }}" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" min="1987" max="{{ date('Y')+2 }}" name="new_year_to"  value="{{ old('new_year_to') }}" class="form-control">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </td>
                                            <td>
                                                <a href="{{url()->full()}}" class="btn btn-info">Reset</a>
                                            </td>

                                        </tr>
                                        <tr>
                                            <th>#</th>
                                            <th>Layout</th>
                                            <th>Year From</th>
                                            <th>Year To</th>
                                            <th>Car Model</th>
                                            <th>Car</th>
                                            <th>Brand</th>

                                        </tr>
                                        </tbody>
                                    </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12">--}}
{{--                                @if(isset($search_status) && $search_status ==1)--}}
{{--                                    {{$car_models->appends(['year_from' => $year_from, 'year_to' => $year_to, 'search_status' => $search_status])->links()}}--}}
{{--                                @else--}}
{{--                                    {{$car_models->links()}}--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
