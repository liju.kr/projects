@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a>>
                        <a href="{{url('view_all_layouts')}}">Layouts </a>
                        <div class="dash-heading">Layouts</div>
                        <div class="pull-right">
                            <form action="" method="get" style="margin: 0px;">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" value="{{ $wf_code }}" name="wf_code">
                                    </div>
                                    <div class="col-lg-3">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                <div class="col-lg-3">
                                    <a href="{{ url('/view_all_layouts') }}" class="btn btn-info">Reset</a>
                                </div>
                        </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach($layouts as $key=>$layout)
                                <div class="col-sm-1 home-card">
                                    <a href="{{route('layouts.show',$layout->getId())}}" target="_blank">
                                        <div class="thumbnail">
{{--                                            <span class="badge badge-success"> {{(($layouts->currentpage()-1)*$layouts->perpage())+$key+1}} </span>--}}
                                            <span class="badge badge-success"> {{ $layout->code_no }} </span>
                                            <div class="caption text-center">
                                                <div class="position-relative layout-card-img">
                                                    <img src="{{$layout->layout_image}}" class="img-responsive">
                                                </div>
                                                <span class="badge badge-warning">{{$layout->getName()}}</span>
                                            </div>
                                        <p class="text-center"><a href="{{route('layouts.edit',$layout->getId())}}" target="_blank"><button class="btn btn-sm btn-primary">Edit</button></a></p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                @if(isset($wf_code) && $wf_code !=null)
                                    {{$layouts->appends(['wf_code' => $wf_code])->links()}}
                                @else
                                    {{$layouts->links()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
