@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a>>
                        <a href="{{url('view_all_car_models')}}">Car Models (Years) </a>
                        <div class="dash-heading">Car Models (Years) <span class="text-capitalize"> {{ $year_from }} - {{ $year_to }}</span></div>
                        <div class="pull-right">
                            <form action="" method="get" style="margin: 0px;">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <select class="form-control"  name="year_from">
                                            <option value="all" @if($year_from=="all") selected @endif>Year From (All)</option>
                                            @for($i=date('Y')+2; $i >=1900; $i--)
                                            <option value="{{ $i }}" @if($year_from==$i) selected @endif>{{ $i }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <select class="form-control"  name="year_to" required>
                                            <option value="" @if($year_to=="") selected @endif>Year To (Select Any)</option>
                                            @for($j=date('Y')+2; $j >=1900; $j--)
                                                <option value="{{ $j }}" @if($year_to==$j) selected @endif>{{ $j }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="hidden" name="search_status" value="1">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                <div class="col-lg-2">
                                    <a href="{{ url('/view_all_car_models') }}" class="btn btn-info">Reset</a>
                                </div>
                        </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body">

                        {!! Form::open(array('url' => 'quick_car_model_edit','method'=>'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
                            <div class="row">
                                @foreach($layouts as $key=>$layout)
                                <div class="col-sm-2 home-card">
                                    <a href="{{route('layouts.show',$layout->getId())}}" target="_blank">
                                        <div class="thumbnail">
                                            <p class="text-center text-uppercase">
                                            <span class="badge badge-warning">{{$layout->getName()}}</span>
                                                @if($layout->car_model)<span class="badge badge-success">{{$layout->car_model->getName()}}</span>@endif
                                            </p>
                                            <div class="caption text-center">
                                                <div class="position-relative layout-card-img">
                                                    <img src="{{$layout->layout_image}}" class="img-responsive">
                                                </div>
                                                <p class="text-center text-uppercase">
                                                @if($layout->car_model->car && $layout->car_model->car->manufacturer)<span class="badge badge-primary">{{ $layout->car_model->car->manufacturer->name }}</span>@endif
                                               <br>
                                                @if($layout->car_model->car) <span class="badge badge-info"> {{ $layout->car_model->car->name }}</span>@endif
                                                </p>
                                                <input type="checkbox" class="form-control" value="{{ $layout->id }}" name="layout_ids[]" {{ ( is_array(old('layout_ids')) && in_array($layout->id, old('layout_ids')) ) ? 'checked ' : '' }}>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                    @endforeach
                            </div>
                        <div class="row">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Year From</th>
                                    <th>Year To</th>
                                    <th>Update</th>
                                    <th>Reset</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="chk-box">
                                        <input type="checkbox" class="form-control" onclick="toggle(this);" />
                                    </td>
                                    <td>
                                        <input type="number" min="1900" max="{{ date('Y')+2 }}" name="new_year_from"   value="{{ old('new_year_from') }}" class="form-control">
                                    </td>
                                    <td>
                                        <input type="number" min="1900" max="{{ date('Y')+2 }}" name="new_year_to"  value="{{ old('new_year_to') }}" class="form-control">
                                    </td>

                                    <td>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </td>
                                    <td>
                                        <a href="{{url()->full()}}" class="btn btn-info">Reset</a>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        {!! Form::close() !!}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12">--}}
{{--                                @if(isset($search_status) && $search_status ==1)--}}
{{--                                    {{$car_models->appends(['year_from' => $year_from, 'year_to' => $year_to, 'search_status' => $search_status])->links()}}--}}
{{--                                @else--}}
{{--                                    {{$car_models->links()}}--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>

    <script>
        function toggle(source) {
            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] != source)
                    checkboxes[i].checked = source.checked;
            }
        }
    </script>

@endsection
