@extends('pages.users.home')

@section('sub-content')
    <div class="row">
    <div class="col-md-6">
            {!! Form::open(array('route' => 'users.store','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}
            <div class="form-group">
                <label class="col-sm-4 control-label">User Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="password" value="{{ old('password') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Contact Number</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="contact" value="{{ old('contact') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-4 control-label">App Login Status</label>
                <div class="col-sm-8">
                    <select class="form-control" name="status">
                        <option value="1">Active</option>
                        <option value="0">In Active</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-4 control-label">Verified</label>
                <div class="col-sm-8">
                    <select class="form-control" name="verified">
                        <option value="1">Verified</option>
                        <option value="0">Not Verified</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Company</label>
                <div class="col-sm-8">
                    <select class="form-control" name="company" required>
                        @if(auth()->user()->hasRole('dev_admin'))
                            <option value="ALL" @if(old('company')=="ALL") selected @endif>ALL</option>
                        @endif
                        @if(auth()->user()->hasRole('wellfit_admin'))
                            <option value="WELLFIT" @if(old('company')=="WELLFIT") selected @endif>Wellfit</option>
                        @endif
                        @if(auth()->user()->hasRole('yaco_admin'))
                            <option value="YACO" @if(old('company')=="YACO") selected @endif>Yaco</option>
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Role</label>
                <div class="col-sm-8">
                    <select class="form-control" name="role" required>
                        @if(auth()->user()->hasRole('dev_admin'))
                            <option value="super_admin" @if(old('role')=="super_admin") selected @endif>Super Admin</option>
                        @endif
                        @if(auth()->user()->hasRole('super_admin'))
                            <option value="wellfit_admin" @if(old('role')=="super_admin") selected @endif>Wellfit Admin</option>
                            <option value="yaco_admin" @if(old('role')=="super_admin") selected @endif>Yaco Admin</option>
                        @endif
                        @if(auth()->user()->hasRole('wellfit_admin'))
                        <option value="wellfit_data_manager" @if(old('role')=="wellfit_data_manager") selected @endif>Wellfit Data Manager</option>
                        <option value="wellfit_distributor" @if(old('role')=="wellfit_distributor") selected @endif>Wellfit Distributor</option>
                        <option value="wellfit_user" @if(old('role')=="wellfit_user") selected @endif>Wellfit User</option>
                                <option value="wellfit_report_user" @if(old('role')=="wellfit_report_user") selected @endif>Wellfit Report User</option>
                                <option value="wellfit_report_manager" @if(old('role')=="wellfit_report_manager") selected @endif>Wellfit Report Manager</option>
                       @endif
                      @if(auth()->user()->hasRole('yaco_admin'))
                                <option value="yaco_data_manager" @if(old('role')=="yaco_data_manager") selected @endif>Yaco Data Manager</option>
                                <option value="yaco_distributor" @if(old('role')=="yaco_distributor") selected @endif>Yaco Distributor</option>
                                <option value="yaco_user" @if(old('role')=="yaco_user") selected @endif>Yaco User</option>
                      @endif
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">Layout App Permissions</label>
                <div class="col-sm-8 chk-box">
                    <input type="checkbox" name="view_price" value="1" id="view_price" @if(old('view_price')==1) checked @endif> <label for="view_price">Price</label>&nbsp;
                    <input type="checkbox" name="view_stock" value="1" id="view_stock" @if(old('view_stock')==1) checked @endif> <label for="view_stock">Stock</label>&nbsp;
                    <input type="checkbox" name="view_reserved_stock" value="1" id="view_reserved_stock" @if(old('view_reserved_stock')==1) checked @endif> <label for="view_reserved_stock">Reserved Stock</label>&nbsp;
                    <input type="checkbox" name="view_configuration" value="1" id="view_configuration" @if(old('view_configuration')==1) checked @endif> <label for="view_configuration">Configuration</label>&nbsp;
                </div>
            </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Report App Permissions</label>
            <div class="col-sm-8 chk-box">
                <input type="checkbox" name="can_view_report" value="1" id="can_view_report" @if(old('can_view_report')==1) checked @endif> <label for="can_view_report">Customer Reports</label>&nbsp;
                <input type="checkbox" name="can_view_report_stock" value="1" id="can_view_report_stock" @if(old('can_view_report_stock')==1) checked @endif> <label for="can_view_report_stock">Stock</label>&nbsp;
             </div>
        </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Companies in Layout App</label>
                <div class="col-sm-8 chk-box">
                    <input type="checkbox" name="layout_companies[]" value="{{ env('COMPANY_1') }}" id="layout_companies1" {{ ( is_array(old('layout_companies')) && in_array(env('COMPANY_1'), old('layout_companies')) ) ? 'checked ' : '' }}> <label for="layout_companies1">{{ env('COMPANY_1') }}</label>&nbsp;
                    <input type="checkbox" name="layout_companies[]" value="{{ env('COMPANY_2') }}" id="layout_companies2" {{ ( is_array(old('layout_companies')) && in_array(env('COMPANY_2'), old('layout_companies')) ) ? 'checked ' : '' }}> <label for="layout_companies2">{{ env('COMPANY_2') }}</label>&nbsp;
                </div>
            </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Companies in Reports App</label>
            <div class="col-sm-8 chk-box">
                {{--                    <input type="checkbox" name="companies[]" value="{{ env('COMPANY_1') }}" id="companies1" {{ ( is_array(old('companies')) && in_array(env('COMPANY_1'), old('companies')) ) ? 'checked ' : '' }}> <label for="companies1">{{ env('COMPANY_1') }}</label>&nbsp;--}}
                {{--                    <input type="checkbox" name="companies[]" value="{{ env('COMPANY_2') }}" id="companies2" {{ ( is_array(old('companies')) && in_array(env('COMPANY_2'), old('companies')) ) ? 'checked ' : '' }}> <label for="companies2">{{ env('COMPANY_2') }}</label>&nbsp;--}}
                @foreach($companies as $key=>$company)
                    <input type="checkbox" id="company{{ $company->id  }}" value="{{ $company->id  }}" name="companies[{{ $key }}][id]" @if(isset($company)) {{ ( is_array(old('companies')) && in_array($company->id, old('companies')) ) ? 'checked ' : '' }} @endif> <label for="company{{ $company->id  }}">{{ $company->name  }}</label>&nbsp;
                    <input type="text" placeholder="Area Code" class="form-control" name="companies[{{ $key }}][areas]"> <br>
                @endforeach
            </div>
        </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary pull-right">Add</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection