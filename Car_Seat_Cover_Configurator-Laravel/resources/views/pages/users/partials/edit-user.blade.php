@extends('pages.users.home')

@section('sub-content')
    <div class="row">
    <div class="col-md-6">
        {!! Form::open(['route' => array('users.update', $user->id), 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
        <div class="form-group">
                <label class="col-sm-4 control-label">User Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Contact Number</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="contact" value="{{ $user->contact }}">
                </div>
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-4 control-label">App Login Status</label>
                <div class="col-sm-8">
                    <select class="form-control" name="status">
                        <option value="1" @if($user->status==1) selected @endif>Active</option>
                        <option value="0" @if($user->status==0) selected @endif>In Active</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-4 control-label">Verified</label>
                <div class="col-sm-8">
                    <select class="form-control" name="verified">
                        <option value="1" @if($user->verified==1) selected @endif>Verified</option>
                        <option value="0" @if($user->verified==0) selected @endif>Not Verified</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Company</label>
                <div class="col-sm-8">
                    <select class="form-control" name="company" required>
                        @if(auth()->user()->hasRole('dev_admin'))
                            <option value="ALL" @if($user->company=="ALL") selected @endif>ALL</option>
                        @endif
                        @if(auth()->user()->hasRole('wellfit_admin'))
                            <option value="WELLFIT" @if($user->company=="WELLFIT") selected @endif>Wellfit</option>
                        @endif
                        @if(auth()->user()->hasRole('yaco_admin'))
                            <option value="YACO" @if($user->company=="YACO") selected @endif>Yaco</option>
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Role</label>
                <div class="col-sm-8">
                    <select class="form-control" name="role" required>
                        @if(auth()->user()->hasRole('dev_admin') || $user->role=="super_admin")
                            <option value="super_admin" @if($user->role=="super_admin") selected @endif>Super Admin</option>
                        @endif
                        @if(auth()->user()->hasRole('super_admin') || $user->role=="wellfit_admin" )
                            <option value="wellfit_admin" @if($user->role=="wellfit_admin") selected @endif>Wellfit Admin</option>
                            @endif
                            @if(auth()->user()->hasRole('super_admin') || $user->role=="yaco_admin" )
                            <option value="yaco_admin" @if($user->role=="yaco_admin") selected @endif>Yaco Admin</option>
                        @endif
                        @if(auth()->user()->hasRole('wellfit_admin'))
                        <option value="wellfit_data_manager" @if($user->role=="wellfit_data_manager") selected @endif>Wellfit Data Manager</option>
                        <option value="wellfit_distributor" @if($user->role=="wellfit_distributor") selected @endif>Wellfit Distributor</option>
                        <option value="wellfit_user" @if($user->role=="wellfit_user") selected @endif>Wellfit User</option>
                        <option value="wellfit_report_user" @if($user->role=="wellfit_report_user") selected @endif>Wellfit Report User</option>
                        <option value="wellfit_report_manager" @if($user->role=="wellfit_report_manager") selected @endif>Wellfit Report Manager</option>
                       @endif
                      @if(auth()->user()->hasRole('yaco_admin'))
                                <option value="yaco_data_manager" @if($user->role=="yaco_data_manager") selected @endif>Yaco Data Manager</option>
                                <option value="yaco_distributor" @if($user->role=="yaco_distributor") selected @endif>Yaco Distributor</option>
                                <option value="yaco_user" @if($user->role=="yaco_user") selected @endif>Yaco User</option>
                      @endif
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">Layout App Permissions</label>
                <div class="col-sm-8 chk-box">
                    <input type="checkbox" name="view_price" value="1" id="view_price" @if($user->user_role->view_price==1) checked @endif> <label for="view_price">Price</label>&nbsp;
                    <input type="checkbox" name="view_stock" value="1" id="view_stock" @if($user->user_role->view_stock==1) checked @endif> <label for="view_stock">Stock</label>&nbsp;
                    <input type="checkbox" name="view_reserved_stock" value="1" id="view_reserved_stock" @if($user->user_role->view_reserved_stock==1) checked @endif> <label for="view_reserved_stock">Reserved Stock</label>&nbsp;
                    <input type="checkbox" name="view_configuration" value="1" id="view_configuration" @if($user->user_role->view_configuration==1) checked @endif> <label for="view_configuration">Configuration</label>&nbsp;
                </div>
            </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Report App Permissions</label>
            <div class="col-sm-8 chk-box">
                <input type="checkbox" name="can_view_report" value="1" id="can_view_report" @if($user->user_role->can_view_report==1) checked @endif> <label for="can_view_report">Customer Report</label>&nbsp;
                <input type="checkbox" name="can_view_report_stock" value="1" id="can_view_report_stock" @if($user->user_role->can_view_report_stock==1) checked @endif> <label for="can_view_report_stock">Stock</label>&nbsp;
                  </div>
        </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Companies in Layout App</label>
                <div class="col-sm-8 chk-box">
               <input type="checkbox" name="layout_companies[]" value="{{ env('COMPANY_1') }}" id="layout_companies1" {{ ( is_array(explode(",", $user->user_role->companies)) && in_array(env('COMPANY_1'), explode(",", $user->user_role->companies)) ) ? 'checked ' : '' }}> <label for="layout_companies1">{{ env('COMPANY_1') }}</label>&nbsp;
               <input type="checkbox" name="layout_companies[]" value="{{ env('COMPANY_2') }}" id="layout_companies2" {{ ( is_array(explode(",", $user->user_role->companies)) && in_array(env('COMPANY_2'), explode(",", $user->user_role->companies)) ) ? 'checked ' : '' }}> <label for="layout_companies2">{{ env('COMPANY_2') }}</label>&nbsp;

                </div>
            </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Companies in Reports App</label>
            <div class="col-sm-8 chk-box">
                 @if($assignedCompaniesAreas)
                    @foreach($assignedCompaniesAreas as $key=>$assignedCompaniesArea)
                        <input type="checkbox" id="company{{ $assignedCompaniesArea->id  }}" value="{{ $assignedCompaniesArea->id  }}" name="companies[{{ $key }}][id]" @if(isset($assignedCompaniesArea)) {{in_array($assignedCompaniesArea->id,$assignedCompanies)?'checked':''}} @endif>
                        <label for="company{{ $assignedCompaniesArea->id  }}">{{ $assignedCompaniesArea->name  }}</label>&nbsp;
                        <input type="text" placeholder="Area Code" class="form-control" name="companies[{{ $key }}][areas]" @if($assignedCompaniesArea->areas) value="{{ $assignedCompaniesArea->areas }}" @endif> <br>
                    @endforeach
                @endif

            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Default Company</label>
            <div class="col-sm-8">
                <select class="form-control" name="default_company" required>
                    @if($user->companies)
                        <option value="0" @if(!$user->user_role->default_company) selected @endif>Select</option>
                    @foreach($user->companies as $company)
                        <option value="{{ $company->id }}" @if($user->user_role->default_company && $user->user_role->default_company == $company->id) selected @endif>{{ $company->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>


            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            {!! Form::open(['url' => 'change_password/'. $user->id, 'enctype' => 'multipart/form-data','class' => 'form-horizontal','method' => 'PUT']) !!}
            <div class="form-group">
                <label class="col-sm-4 control-label">Set New Password</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="password" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary pull-right">Reset Password</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection