@extends('pages.users.home')

@section('sub-content')
    <div class="row">
        <div class="col-md-12">
                <div class="panel-body">
                    <div class="col-md-12">
                       <form  action="{{ url('/logout_all') }}" method="POST">
                           {{ csrf_field() }}
                           <h2>User Management
                               @if(auth()->user()->hasRole('wellfit_admin') || auth()->user()->hasRole('yaco_admin'))
                               <input type="submit" name="logout" value="Logout All" class="btn-danger btn" style="float:right">
                                   @endif
                           </h2>

                            </form>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>App Login</th>
                                <th>Verification</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr>
                                    <td>{{(($users->currentpage()-1)*$users->perpage())+$key+1}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}} <br> {{$user->contact}}</td>
                                    <td class="text-capitalize">{{$user->role_name}}</td>
                                    <td>
                                        @if($user->status == 1)
                                            <button type="button" class="btn btn-success btn-sm">Active</button>
                                        @else
                                            <button type="button" class="btn btn-danger btn-sm">In Active</button>
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->verified == 1)
                                            <button type="button" class="btn btn-success btn-sm">Verified</button>
                                        @else
                                            <button type="button" class="btn btn-danger btn-sm">Not Verified</button>
                                        @endif
                                    </td>
                                    <td><a href="{{route('users.show',$user->id)}}" class="btn-primary btn btn-sm">View</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
        </div>
    </div>
@endsection