@extends('pages.users.home')

@section('sub-content')
    <div class="row">
        <div class="col-md-12">
                <div class="panel-body">
                    <div class="col-md-6">
                        <h3 style="padding-bottom:25px;">{{$user->name}}</h3>
                       <table class="table table-striped">
                           <tr>
                               <td> User Name</td>
                               <td>{{$user->name}}</td>
                           </tr>
                           <tr>
                               <td> Email</td>
                               <td>{{$user->email}}</td>
                           </tr>
                           <tr>
                               <td> Contact</td>
                               <td>{{$user->contact}}</td>
                           </tr>
                           <tr>
                               <td> Role</td>
                               <td class="text-capitalize">{{$user->role_name}}</td>
                           </tr>
                           <tr>
                               <td> Company</td>
                               <td class="text-capitalize">{{$user->company}}</td>
                           </tr>
                           <tr>
                               <td>Layout App Permissions</td>
                               <td>
                                   @if($user->user_role->view_stock == 1) <span  class="badge-success badge">View Stock</span> &nbsp @endif
                                   @if($user->user_role->view_reserved_stock == 1) <span  class="badge-warning badge">View  Reserved Stock</span> &nbsp @endif
                                   @if($user->user_role->view_price == 1) <span  class="badge-primary badge">View Price</span> &nbsp @endif
                                   @if($user->user_role->view_configuration == 1) <span  class="badge-info badge">View Configuration</span> &nbsp @endif
                               </td>
                           </tr>

                           <tr>
                               <td>Report App Permissions</td>
                               <td>
                                   @if($user->user_role->can_view_report_stock == 1) <span  class="badge-success badge">View Stock</span> &nbsp @endif
                                   @if($user->user_role->can_view_report == 1) <span  class="badge-warning badge">View Report</span> &nbsp @endif
                               </td>
                           </tr>
                           <tr>
                               <td> Companies</td>
                               <td>

                                   @foreach($user->companies as $company)
                                       <span  class="badge-error badge">{{ $company->name }}</span>
                                       &nbsp
                                       @endforeach

                               </td>
                           </tr>
                           <tr>
                               <td> Default Company</td>
                               <td class="text-capitalize">@if($user->user_role->default_company) {{ App\Models\Company::findOrFail($user->user_role->default_company)->name }} @endif</td>
                           </tr>
                           <tr>
                               <td> App Login</td>
                               <td>
                                   @if($user->status == 1)
                                       <button type="button" class="btn btn-success btn-sm">Active</button>
                                   @else
                                       <button type="button" class="btn btn-danger btn-sm">In Active</button>
                                   @endif
                               </td>
                           </tr>
                           <tr>
                               <td> Verified</td>
                               <td>
                                   @if($user->verified == 1)
                                       <button type="button" class="btn btn-success btn-sm">Verified</button>
                                   @else
                                       <button type="button" class="btn btn-danger btn-sm">Not Verified</button>
                                   @endif
                               </td>
                           </tr>

                       </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tr>
                                <td> Created On</td>
                                <td>{{ $user->createdOn() }}</td>
                            </tr>
                            <tr>
                                <td> Last Updated On</td>
                                <td>{{ $user->updatedOn() }}</td>
                            </tr>
                            <tr>
                                <td> Last Updated By</td>
                                <td class="text-capitalize">{{ $user->updatedBy() }}</td>
                            </tr>
                            <tr>
                                <td> Action</td>
                                <td class="delete-form">
                                    <a href="{{route('users.edit',$user->id)}}" class="btn-primary btn btn-sm">Edit</a>
                                    {{ Form::open(array('url' => 'users/'.$user->id, 'style' => 'display:inline-block;')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-sm','onclick'=>"return confirm('Are you sure ?');")) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
    </div>
@endsection