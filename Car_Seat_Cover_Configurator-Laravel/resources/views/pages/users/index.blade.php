@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a> >
                        <a href="{{url('users')}}">
                            Users
                        </a>
                        <div class="dash-heading">Users</div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <h2>User Management</h2>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>SL NO</th>
                                    <th>Name / Shop</th>
                                    <th>Email / Contact</th>
                                    <th>Role</th>
                                    <th>App Login Status</th>
                                    <th>Verification</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{(($users->currentpage()-1)*$users->perpage())+$key+1}}</td>
                                        <td>{{$user->name}} <br> {{$user->shop_name}}</td>
                                        <td>{{$user->email}} <br> {{$user->contact}}</td>
                                        <td>{{$user->role}}</td>
                                        <td>
                                            @if($user->status == 1)
                                                <button type="button" class="btn btn-success">Active</button>
                                            @else
                                                <button type="button" class="btn btn-danger">In Active</button>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->verified == 1)
                                                <button type="button" class="btn btn-success">Verified</button>
                                            @else
                                                <button type="button" class="btn btn-danger">Not Verified</button>
                                            @endif
                                        </td>
                                        <td><a href="{{url('users/'.$user->id)}}">Edit Status</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection