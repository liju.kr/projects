@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a> >
                        <a href="{{url('users')}}">
                            Users
                        </a>
                        <div class="dash-heading">Users</div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        @if (session('status'))
                                            <div class="alert alert-success">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="col-md-12">
                                            {!! Form::open(array('url' => ['users/update/'.$user->id],'method'=>'PUT','class' => 'form-horizontal','novalidate'=> 'novalidate','files' => true)) !!}

                                            <div class="form-group">
                                                <label for="status" class="col-sm-4 control-label">App Login Status</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="status" required>
                                                        <option value="0" @if($user->status == 0) selected @endif>In Active</option>
                                                        <option value="1" @if($user->status == 1) selected @endif>Active</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="status" class="col-sm-4 control-label">Verified</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="verified" required>
                                                        <option value="0" @if($user->verified == 0) selected @endif>Not Verified</option>
                                                        <option value="1" @if($user->verified == 1) selected @endif>Verified</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
