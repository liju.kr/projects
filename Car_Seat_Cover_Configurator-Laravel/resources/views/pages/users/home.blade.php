@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{url('home')}}">Home </a> >
                        <a href="{{route('users.index')}}">
                            Users
                        </a>
                        <div class="dash-heading">Users</div>
                        <div class="pull-right">
                            @if(auth()->user()->hasRole('wellfit_admin') || auth()->user()->hasRole('yaco_admin'))
                                    <a href="{{route('users.create')}}">
                                        +Add New
                                    </a>
                            @endif

                        </div>
                    </div>

                    <div class="panel-body">
                        @yield('sub-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection