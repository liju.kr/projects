<label  class="col-sm-2 control-label">Layouts</label>
<div class="col-sm-10">
    @foreach($car_models as $car_model)
        @if($car_model->layouts()->count())
           <strong> {{ $car_model->name }}</strong>&nbsp;&nbsp;
                @foreach($car_model->layouts as $layout)
                <input type="checkbox" name="layouts[]" value="{{ $layout->id }}">{{ $layout->name }} &nbsp;
                @endforeach
        <hr>
        @endif
    @endforeach
</div>