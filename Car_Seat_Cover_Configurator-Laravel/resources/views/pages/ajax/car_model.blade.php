<label  class="col-sm-2 control-label">Car</label>
<div class="col-sm-10">
    <select name="car" onchange="getCar()" class="form-control">
        <option value="">-Select Any-</option>
        @foreach($cars as $car)
            <option value="{{ $car->id }}">{{ $car->name }}</option>
        @endforeach
    </select>
</div>
