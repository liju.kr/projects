@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <table class="table table-striped">
            <thead>
            <tr>
                <th> ID</th>
                <th>Car Model ID</th>
                <th>Seat ID</th>
                <th>Car </th>
                <th>Brand</th>
                <th>Car Model</th>
            </tr>
            </thead>
            <tbody>
            @foreach($car_model_seats as $car_model_seat)
                <tr>
@php
$car_model = \App\Models\CarModel::findOrFail($car_model_seat->car_model_id);
@endphp                    <td>{{$car_model_seat->id}}</td>
                    <td>{{$car_model_seat->car_model_id}}</td>
                    <td>{{$car_model_seat->seat_id}}</td>
                    <td>@if($car_model && $car_model->car && $car_model->car->manufacturer){{ $car_model->car->manufacturer->name }}@endif</td>
                    <td>@if($car_model && $car_model->car){{ $car_model->car->name }}@endif</td>
                    <td>@if($car_model){{ $car_model->name }}@endif</td>
                  </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
