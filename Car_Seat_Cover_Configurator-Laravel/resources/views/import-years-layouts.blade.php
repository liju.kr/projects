@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Old Layouts
                </div>
                <div class="panel-body">
                    @foreach(\App\Models\Imported\OldLayout::all() as $layout)
                        <div class="col-md-4">
                            @if($layout->images->count()>0)
                            <img src="{{url('storage/'.$layout->images->first()->image)}}" width="30">
                            @endif
                            <br/>
                            {{$layout->name}}
                        </div>
                    @endforeach
                </div>
                <a href="{{url('/import/trigger_years_layouts')}}">
                    <button class="pull-right">IMPORT</button>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current Years
                </div>
                <div class="panel-body">
                    @foreach(\App\Models\CarModel::all() as $car_model)
                        <div class="col-md-4">
                            {{$car_model->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current Layouts
                </div>
                <div class="panel-body">
                    @foreach(\App\Models\Layout::all() as $layout)
                        <div class="col-md-4">
                            {{$layout->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @yield('selection-content')
    </div>
</div>

@endsection
