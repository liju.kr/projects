Hello <strong>{{ $name }}</strong>,
<p>{{ $body }}, <a href="{{ $url }}">Click Here</a> to view the details. </p>
<p>Regards, <br> <strong> Team Yaco</strong></p>