<br/><br/>
Hi,
<br/><br/>
A new order has been placed<br/> <br/>
<a>Order ID : {{$order->id}}</a> <br/> <br/>
<a>Customer Name : {{$order->name}}</a> <br/> <br/>
<a>Customer email : {{$order->email}}</a> <br/> <br/>
<a>Customer Contact Number : {{$order->phone}}</a> <br/> <br/>
<a>Remarks : {{$order->remark}}</a> <br/> <br/>
<table>
    <thead>
    <tr>
        <th>Configuration ID</th>
        {{-- <th>Image</th> --}}
        <th>Selections</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$configuration->id}}</td>
            {{-- <td  style="position:relative !important;">
                    
                            @php
                            $i=1;
                            
                         @endphp
                         
                            @foreach(json_decode($configuration->image) as $image) 
                        <img class="layer" style="position:absolute !important;width:250px !important; left:20px !important;@php if($i==1){@endphp z-index: 8 !important; @php }else{ @endphp margin-left:-250px; @php  } @endphp" src="{{ $image }}">
                         @php
                            $i=$i+1;
                         @endphp
                        @endforeach
                    
                  
            </td> --}}
            <td>
                @foreach(json_decode($configuration->selections) as $selection)
                    {{$selection->label}} : {{$selection->value}}<br/>
                @endforeach
            </td>
        </tr>
    </tbody>
</table>
<div>
<h4> <a href="{{ url('/') }}/view-my-configuration/{{ $order->id }}" target="_blank">Click Here To View Configured Image</a> </h4>
    <h2>Slected Image</h2>
    
    <img src="{{ $configuration->selected_image }}" width="150">
    
   

</div>