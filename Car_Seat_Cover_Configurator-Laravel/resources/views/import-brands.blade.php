@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Old brands
                </div>
                <div class="panel-body">
                    @foreach(\App\Models\Imported\OldBrand::all() as $brand)
                        <div class="col-md-4">
                            <img src="{{url('storage/'.$brand->image)}}" width="30">
                            {{$brand->name}}
                        </div>
                    @endforeach
                </div>
                <a href="{{url('/import/trigger_brands')}}">
                    <button class="pull-right">IMPORT</button>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current brands
                </div>
                <div class="panel-body">
                    @foreach(\App\Models\Manufacturer::all() as $brand)
                        <div class="col-md-4">
                            <img src="{{url('storage/'.$brand->getImageURL())}}" width="30">
                            {{$brand->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @yield('selection-content')
    </div>
</div>

@endsection
