<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configuration extends Model
{
    use SoftDeletes;
    protected $appends =['min_delivery_day'];
    public function seat()
    {
        return $this->belongsTo('App\Models\Seat');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
    public function layout_image()
    {
        return $this->hasOne('App\Models\LayoutImage','id','image_id');
    }
    public function scopeCreatedDescending($query)
    {
            return $query->orderBy('created_at','DESC');
    }

    public function getMinDeliveryDayAttribute()
    {
       return env('MIN_DELIVERY_DAY');
    }
}
