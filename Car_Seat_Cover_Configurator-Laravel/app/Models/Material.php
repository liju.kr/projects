<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    //
    use SoftDeletes;

    protected $appends = ['material_image'];


    public function getMaterialImageAttribute()
    {
        if(env('APP_ENV')=='development')
        {
            return env('APP_URL').'/storage/'.$this->image;
        }
        else
        {
            $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
            if($json === false) {
                return env('APP_URL').'/images/Image-Not-Available.png';
            } else {
                return env('APP_URL').'/storage/'.$this->image;
            }
        }
    }
}
