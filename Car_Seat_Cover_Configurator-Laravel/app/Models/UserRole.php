<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function default_com()
    {
        return $this->hasOne('App\Models\Company');
    }

}
