<?php

namespace App\Models;
use App;
use App\User;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{
    use SoftDeletes;
    protected $appends = ['layout_image_hide_status'];
    protected $visible = ['id','name','image', 'layout_image_hide_status'];
	public function cars()
    {
        return $this->hasMany('App\Models\Car')->orderBy('name');
    }

	public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function getImageURL()
    {
       return $this->image;
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }

    public function getLayoutImageHideStatusAttribute()
    {
        if($this->id == env('YACO_SEATX_BRAND_ID')) return 1; else return 0;

    }


}
