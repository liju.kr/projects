<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pattern extends Model
{
    use SoftDeletes;
    protected $appends=['pattern_image', 'code_name'];

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getImageURL()
    {
        return $this->image;
    }

    public function variants()
    {
        return $this->belongsToMany(Variant::class, 'pattern_variants');
    }

    public function layers()
    {
        return $this->belongsToMany(Layer::class, 'pattern_variants')->withPivot('layer_id', 'pattern_id', 'variant_id');
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }

    public function getPatternImageAttribute()
    {
        return env('APP_URL').'/storage/'.$this->image;

        $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
        if($json === false) {
            return env('APP_URL').'/images/Image-Not-Available.png';
        } else {
            return env('APP_URL').'/storage/'.$this->image;
        }

    }

    public function getCodeNameAttribute()
    {
        return $this->name."  (".$this->code.")";
    }
}
