<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_companies');
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }
}
