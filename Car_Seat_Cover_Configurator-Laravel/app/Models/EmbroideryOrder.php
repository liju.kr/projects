<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmbroideryOrder extends Model
{
    //
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeCreatedDescending($query)
    {
        return $query->orderBy('created_at','DESC');
    }
}
