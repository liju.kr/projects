<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Colour extends Model
{
    use SoftDeletes;
    protected $appends = ['colour_image'];
    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }


    public function colour_category()
    {
        return $this->belongsTo('App\Models\ColourCategory');
    }

    public function getColourImageAttribute()
    {
        if(env('APP_ENV')=='development')
        {
            return env('APP_URL').'/storage/'.$this->image;
        }
        else
        {
            $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
            if($json === false) {
                return env('APP_URL').'/images/Image-Not-Available.png';
            } else {
                return env('APP_URL').'/storage/'.$this->image;
            }
        }
    }
}
