<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
   protected $appends = ['vat'];
	public function configuration()
    {
        return $this->belongsTo('App\Models\Configuration');
    }
    public function scopeCreatedDescending($query)
    {
            return $query->orderBy('created_at','DESC');
    }
	public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function getEmail()
    {
    	return $this->name;
    }
    public function getPhone()
    {
    	return $this->name;
    }
    public function getRemark()
    {
    	return $this->name;
    }

    public function getVatAttribute()
    {
        return 5;
    }
}
