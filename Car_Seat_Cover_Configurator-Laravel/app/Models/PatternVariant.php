<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatternVariant extends Model
{
    public function layer()
    {
        return $this->belongsTo('App\Models\Layer');
    }

    public function variant()
    {
        return $this->belongsTo('App\Models\Variant');
    }

    public function pattern()
    {
        return $this->belongsTo('App\Models\Pattern');
    }
}
