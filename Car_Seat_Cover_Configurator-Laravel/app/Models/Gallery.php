<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    protected  $appends = ['gallery_image', 'layout_image'];
    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getImageURL()
    {
        return $this->image;
    }

    public function gallery_category()
    {
        return $this->belongsTo('App\Models\GalleryCategory');
    }

    public function layouts()
    {
        return $this->belongsToMany(Layout::class, 'gallery_layouts');
    }

    public function getGalleryImageAttribute()
    {
        if(env('APP_ENV')=='development')
        {
            return env('APP_URL').'/storage/'.$this->image;
        }
        else
        {
            $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
            if($json === false) {
                return env('APP_URL').'/images/Image-Not-Available.png';
            } else {
                return env('APP_URL').'/storage/'.$this->image;
            }
        }
    }

    public function getLayoutImageAttribute()
    {
        if(env('APP_ENV')=='development')
        {
            return env('APP_URL').'/storage/'.$this->image;
        }
        else
        {
            $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
            if($json === false) {
                return env('APP_URL').'/images/Image-Not-Available.png';
            } else {
                return env('APP_URL').'/storage/'.$this->image;
            }
        }
    }
}
