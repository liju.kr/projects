<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;
	protected $fillable = ['name','manufacturer_id'];
	
	public function manufacturer()
    {
        return $this->belongsTo('App\Models\Manufacturer');
    }

	public function models()
    {
        return $this->hasMany('App\Models\CarModel')->orderBy('name','desc');
    }

    public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }
}
