<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;
use Illuminate\Support\Facades\Auth;
use Storage;
class Layout extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','car_model_id','price'];
    protected $appends = ['layout_image', 'image', 'public_url'];
    protected $guarded=[];
    

	public function car_model()
    {
        return $this->belongsTo('App\Models\CarModel');
    }

    public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function getImageURL()
    {
        if($this->images()->count()) return $this->images()->first()->image; else return "not_found.png";
    }
    public function images()
    {
        return $this->hasMany('App\Models\LayoutImage','layout_id')->orderBy('sort','ASC')->orderBy('id','ASC');
    }
    public function layout_designs()
    {
        return $this->hasMany('App\Models\LayoutDesign','layout_id')->orderBy('sort','ASC')->orderBy('id','ASC');
    }
    public function materials()
    {
        //  return $this->belongsToMany(Variant::class, 'price_range_variants');
        return $this->belongsToMany('App\Models\Material', 'layout_materials')->withPivot('layout_id','price');
    }

    public function galleries()
    {
        return $this->belongsToMany(Gallery::class, 'gallery_layouts');
    }

    public function getImageAttribute()
    {
        if($this->images()->count()) return $this->images()->first()->image; else return "not_found.png";

    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }

    public function getPublicUrlAttribute()
    {
     return "";
    }

    public function getNameAttribute($value)
    {
        if(Auth::check())
        {
            if(Auth::user()->company == "YACO")
            {
                return str_replace("WF", "YC", $value);
            }
            else
            {
                return $value;
            }
        }
        else
        {
            return $value;
        }
    }

    public function getLayoutImageAttribute()
    {

        if(env('APP_ENV')=='development')
        {
            return env('APP_URL').'/storage/'.$this->image;
        }
        else
        {
            $json = @file_get_contents(env('APP_URL').'/storage/'.$this->image, true);
            if($json === false) {
                return env('APP_URL').'/images/Image-Not-Available.png';
            } else {
                return env('APP_URL').'/storage/'.$this->image;
            }
        }


    }

}
