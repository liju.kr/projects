<?php

namespace App\Models;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;
class Variant extends Model
{
    use SoftDeletes;
	protected $fillable = ['name','layer_id'];
    protected $appends=['code_name'];
    public function layer()
    {
        return $this->belongsTo('App\Models\Layer');
    }
    public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function getColour()
    {
    	return $this->colour;
    }
    public function getImageURL()
    {
       return $this->image;
    }
    public function patterns()
    {
        return $this->belongsToMany(Pattern::class, 'pattern_variants')->distinct('pattern_id');
    }

    public function pattern()
    {
        return $assigned_pattern = PatternVariant::where('layer_id', $this->layer_id)->where('variant_id', $this->id)->first();

    }

    public function getCodeNameAttribute()
    {
        return $this->name;
    }
}
