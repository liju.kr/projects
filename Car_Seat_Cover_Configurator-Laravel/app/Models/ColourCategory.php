<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ColourCategory extends Model
{
    use SoftDeletes;
    public function colours()
    {
        return $this->hasMany('App\Models\Colour')->orderBy('name');
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }
}
