<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','car_id','old_id','old_seat_layout_id','year_from','year_to'];
	
	public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }

//	public function seats()
//    {
//        return $this->belongsToMany('App\Models\Seat');
//    }


    public function seats()
    {
        return $this->belongsToMany(Seat::class, 'car_model_seats');
    }

    public function layouts()
    {
        return $this->hasMany('App\Models\Layout');
    }

    public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }
}
