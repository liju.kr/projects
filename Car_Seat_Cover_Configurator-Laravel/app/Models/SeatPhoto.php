<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeatPhoto extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['name', 'image'];


    public function seat()
    {
        return $this->belongsTo('App\Models\Seat');
    }

    public function getImageURL()
    {
        return $this->image;
    }
}
