<?php

namespace App\Models;
use Illuminate\Support\Facades\File;
use Storage;
use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seat extends Model
{
    use SoftDeletes;
	protected $fillable = ['name','car_model_id','price_range_from','price_range_to'];

	public function car_model()
    {
        return $this->belongsTo('App\Models\CarModel');
    }

    public function car_models()
    {
       // return $this->belongsToMany('App\Models\CarModel');
        return $this->belongsToMany(CarModel::class, 'car_model_seats');
    }

	public function layers()
    {
        return $this->hasMany('App\Models\Layer');
    }

    public function seat_photos()
    {
        return $this->hasMany('App\Models\SeatPhoto');
    }

    public function price_ranges()
    {
        //  return $this->belongsToMany(Variant::class, 'price_range_variants');
        return $this->belongsToMany('App\Models\PriceRange', 'price_range_seats')->withPivot('seat_id');
    }
    public function getId()
	{
		return $this->id;
	}
    public function getName()
    {
    	return $this->name;
    }
    public function getImageURL()
    {
       return $this->image;
    }

    public function getBaseImageURL()
    {
       $file = 'storage/base/'.$this->getId().'.png';
        if (File::exists($file))
        {
            return 'base/'.$this->getId().'.png';
        }
        else return null;
    }
    public function getOriginalImageURL()
    {
       $file = 'storage/original_images/'.$this->getId().'.png';
        if (File::exists($file))
        {
            return 'original_images/'.$this->getId().'.png';
        }
        else return null;
    }
    
}
