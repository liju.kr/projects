<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceRange extends Model
{
    //
    use SoftDeletes;
    protected $visible = ['id','name','price'];

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }

    public function seats()
    {
        //  return $this->belongsToMany(Variant::class, 'price_range_variants');
        return $this->belongsToMany('App\Models\Seat', 'price_range_seats')->withPivot('price_range_id');
    }

}
