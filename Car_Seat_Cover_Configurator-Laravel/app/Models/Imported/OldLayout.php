<?php

namespace App\Models\Imported;

use Illuminate\Database\Eloquent\Model;

class OldLayout extends Model
{
    protected $connection = "mysql2";
    protected $table = "oc_seatlayout";
    public function images()
    {
        return $this->hasMany('App\Models\Imported\OldLayoutImage','seatlayout_id');
    }
}
