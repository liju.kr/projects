<?php

namespace App\Models\Imported;

use Illuminate\Database\Eloquent\Model;

class OldCar extends Model
{
    protected $connection = "mysql2";
    protected $table = "oc_carmodel";
}
