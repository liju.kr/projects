<?php

namespace App\Models\Imported;

use Illuminate\Database\Eloquent\Model;

class OldLayoutImage extends Model
{
    protected $connection = "mysql2";
    protected $table = "oc_seatlayout_image";
}
