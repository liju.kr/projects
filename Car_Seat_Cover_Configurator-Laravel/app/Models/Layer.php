<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Layer extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','seat_id'];
    public function seat()
    {
        return $this->belongsTo('App\Models\Seat');
    }
    public function variants()
    {
        return $this->hasMany('App\Models\Variant');
    }

    public function patterns()
    {
       return $this->belongsToMany(Pattern::class, 'pattern_variants')->withPivot('layer_id')->distinct('pattern_id')->orderBy('pattern_id', 'ASC');
    }

    public function getId()
	{
		return $this->id;
	}
	
    public function getName()
    {
    	return $this->name;
    }
}
