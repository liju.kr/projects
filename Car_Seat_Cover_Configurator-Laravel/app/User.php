<?php

namespace App;
use App\Models\Company;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'role_name',
    ];
    public function configurations()
    {
        return $this->hasMany('App\Models\Configuration');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }

    public function embroidery_configurations()
    {
        return $this->hasMany('App\Models\EmbroideryConfiguration');
    }

    public function embroidery_orders()
    {
        return $this->hasMany('App\Models\EmbroideryOrder');
    }

    public function user_role()
    {
        return $this->hasOne('App\Models\UserRole');
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'user_companies')->withPivot('areas');;
    }




    public function scopeCreatedDescending($query)
    {
        return $query->orderBy('created_at','DESC');
    }

    public function hasRole($role){

        if($role=='wellfit_user'&&($this->role == 'wellfit_user'||$this->role == 'wellfit_data_manager'||$this->role == 'wellfit_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='yaco_user'&&($this->role == 'yaco_user'||$this->role == 'yaco_data_manager'||$this->role == 'yaco_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='yaco_distributor'&&($this->role == 'yaco_distributor'||$this->role == 'yaco_data_manager'||$this->role == 'yaco_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='wellfit_distributor'&&($this->role == 'wellfit_distributor'||$this->role == 'wellfit_data_manager'||$this->role == 'wellfit_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='wellfit_data_manager'&&($this->role == 'wellfit_data_manager'||$this->role == 'wellfit_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='yaco_data_manager'&&($this->role == 'yaco_data_manager'||$this->role == 'yaco_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='wellfit_data_manager'&&($this->role == 'wellfit_data_manager'||$this->role == 'wellfit_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='yaco_admin'&&($this->role == 'yaco_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='wellfit_admin'&&($this->role == 'wellfit_admin'||$this->role == 'super_admin'||$this->role == 'dev_admin')) return true;

        if($role=='super_admin'&&($this->role == 'super_admin'||$this->role == 'dev_admin')) return true;


        if($role==$this->role) return true;
        return false;
    }
    public function isDevAdmin()    {
        return $this->role === 'dev_admin';
    }
    public function isSuperAdmin()    {
        return $this->role === 'super_admin';
    }
    public function isWellfitAdmin()    {
        return $this->role === 'wellfit_admin';
    }
    public function isYacoAdmin()    {
        return $this->role === 'yaco_admin';
    }
    public function isYacoDistributor()    {
        return $this->role === 'yaco_distributor';
    }
    public function isWellfitDistributor()    {
        return $this->role === 'wellfit_distributor';
    }
    public function isWellfitDataManager()    {
        return $this->role === 'wellfit_data_manager';
    }
    public function isYacoDataManager()    {
        return $this->role === 'yaco_data_manager';
    }
    public function isYacoUser()    {
        return $this->role === 'yaco_user';
    }
    public function isWellfitUser()    {
        return $this->role === 'wellfit_user';
    }
    public function isWellfitReportUser()    {
        return $this->role === 'wellfit_report_user';
    }
    public function isWellfitReportManager()    {
        return $this->role === 'wellfit_report_manager';
    }

    public function isAnyAdmin(){

        if($this->isYacoDataManager() || $this->isWellfitDataManager() || $this->isDevAdmin()  || $this->isYacoAdmin() || $this->isWellfitAdmin()  || $this->isSuperAdmin() )
             return true;
        else
            return false;
    }

    public function isAnyUser(){
        if($this->isYacoDistributor() || $this->isWellfitDistributor() || $this->isYacoUser()  || $this->isWellfitUser() )
            return true;
        else
            return false;
    }

    public function isAnyReportUser(){
        if($this->isWellfitReportUser()|| $this->isWellfitReportManager() )
            return true;
        else
            return false;
    }



    public function getRoleNameAttribute()
    {
      return str_replace('_', ' ', $this->role);
    }

    public function createdOn()
    {
        return date('j M Y, h:i A', strtotime($this->created_at));
    }
    public function updatedOn()
    {
        return date('j M Y, h:i A', strtotime($this->updated_at));
    }
    public function updatedBy()
    {
        if($this->user_id) return User::findOrFail($this->user_id)->name." | ".User::findOrFail($this->user_id)->role_name;
        return "";
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }

}
