<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReceived extends Mailable
{
    use Queueable, SerializesModels;
    public $order,$configuration,$image;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$configuration,$image)
    {
        $this->order = $order;
        $this->configuration = $configuration;
        $this->image = $image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order-confirmed')->with([
            'order' => $this->order,
            'configuration' => $this->configuration,
            'image' => $this->image
            
        ]);
    }
}
