<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarModel;
use App\Models\Imported\OldBrand;
use App\Models\Imported\OldCar;
use App\Models\Imported\OldLayout;
use App\Models\Imported\OldLayoutImage;
use App\Models\Layout;
use App\Models\LayoutImage;
use App\Models\Manufacturer;
use App\Models\Old2SeatLayout;
use App\Models\Old2SeatLayoutImage;
use App\Models\Old2SeatPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller
{

    public function import_brands()
    {
        return view('import-brands');
    }
    public function import_cars()
    {
        return view('import-cars');
    }
    public function view_cars()
    {
        return view('view-cars');
    }
    public function rename_duplicates()
    {
        foreach(CarModel::all() as $model)
        {
          if(substr($model->name, 0, 4)==substr($model->name, 5, 4))
          {
              $model->update(['name'=>substr($model->name, 0, 4)]);
          }
        }
        return redirect()->back();
    }
    public function remove_duplicates()
    {
        foreach (Car::all() as $car)
        {
            foreach ($car->models as $model) {
                foreach ($model->car->models as $lmodel) {
                    if ($lmodel->id != $model->id && $lmodel->name == $model->name) {
                        foreach ($lmodel->seats as $seat) {
                            $seat->update(['car_model_id' => $model->id]);
                        }
                        foreach ($lmodel->layouts as $layout) {
                            $layout->update(['car_model_id' => $model->id]);
                        }
                        $cm = $lmodel->name;
                        $c = $lmodel->car->name;
                        $lmodel->delete();
                        return "Merged layout ".$cm." of ".$c." to " . $model->name . " of " .$model->car->name;
                    }
                }
            }
        }
    }
    public function import_years_layouts()
    {
        return view('import-years-layouts');
    }
    public function trigger_brands()
    {
        foreach(OldBrand::all() as $old_brand){
             $item = new Manufacturer();
             $item->name = $old_brand->name;
             $item->image = $old_brand->image;
             $item->old_id = $old_brand->id;
             $item->save();
             $old_brand->delete();
        }
        return "Done";
    }

    public function trigger_cars()
    {
        foreach(OldCar::all() as $old_car){
            $manufacturer =  Manufacturer::where('old_id',$old_car->brand_id)->first();
            $item = new Car();
            $item->name = $old_car->name;
            $item->old_id = $old_car->id;
            $item->manufacturer_id = $manufacturer->id;
            $item->save();
            $old_car->delete();
        }
        return "Done";
    }
    public function trigger_years_layouts()
    {
        foreach(OldLayout::all() as $old_layout){
            $car = Car::where('old_id',$old_layout->car_id)->first();
            $item = new CarModel();
            $item->name = $old_layout->year_from."-".$old_layout->year_to;
            $item->car_id = $car->id;
            $item->old_id = $old_layout->id;
            $item->save();
            $item2 = new Layout();
            $item2->name = $old_layout->name;
            $item2->car_model_id = $item->id;
            if($old_layout->images->count()>0)
            {
                $item2->image = $old_layout->images->first()->image;
            }
            else
            {
                $item2->image = "none";
            }
            $item2->old_id = $old_layout->id;
            $item2->description = $old_layout->description;
            $item2->save();
            $old_layout->timestamps = false;
            $old_layout->more_information = "imported";
            $old_layout->save();
        }
        return "Done";
    }
    public function trigger_layout_images()
    {
        foreach(OldLayoutImage::all() as $image){
            $layout = Layout::where('old_id',$image->seatlayout_id)->first();
            $img = LayoutImage::create([
                'layout_id'=>$layout->id,
                'image'=>$image->image
            ]);
        }
        return "Done";
    }


    public function import_car_model_year()
    {
        $query = Old2SeatLayout::select('year_from', 'year_to', 'carmodel_id')->where('seatlayout_id' , '>', 6442)
            ->distinct()
            ->get();

        foreach ($query as $q)
        {

            if($q->carmodel_id >=937 && $q->carmodel_id <=948) $car_model_id = $q->carmodel_id +3;
            if($q->carmodel_id >=950 && $q->carmodel_id <=954) $car_model_id = $q->carmodel_id +2;
            if($q->carmodel_id <=936) $car_model_id = $q->carmodel_id;

            $car_model = new CarModel();
            $car_model->name = $q->year_from." - ".$q->year_to;
            $car_model->year_from = $q->year_from;
            $car_model->year_to = $q->year_to;
            $car_model->car_id = $car_model_id;
            $car_model->old_seat_layout_id = null;
            $car_model->old_id =1;
            $car_model->save();
        }


    }

    public function import_seat_layout()
    {
//       $old_layouts = Old2SeatLayout::where('seatlayout_id' , '>', 6442)->get();
//        $ids=[];
//        foreach ($old_layouts as $old_layout)
//        {
//
//            if($old_layout->carmodel_id >=937 && $old_layout->carmodel_id <=948) $car_model_id = $old_layout->carmodel_id +3;
//            if($old_layout->carmodel_id >=950 && $old_layout->carmodel_id <=954) $car_model_id = $old_layout->carmodel_id +2;
//            if($old_layout->carmodel_id <=936) $car_model_id = $old_layout->carmodel_id;
//
//              $car_model = CarModel::where('year_from',$old_layout->year_from)
//                ->where('year_to', $old_layout->year_to)
//                ->where('car_id',$car_model_id)
//                ->first();
//
//
//               $old_seat_layout_image = Old2SeatLayoutImage::where('seatlayout_id', $old_layout->seatlayout_id)
//                               ->first();
//
//               if(empty($old_seat_layout_image))
//               {
//                   $img =  "image";
//               }
//               else{
//                   $img =  $old_seat_layout_image->image;
//               }
//
//            $layout = new Layout();
//            $layout->name = $old_layout->name;
//            $layout->image =$img;
//            $layout->description = $old_layout->description;
//            $layout->more_information = $old_layout->more_information;
//            $layout->car_model_id = $car_model->id;
//            $layout->old_id =$old_layout->seatlayout_id;
//            $layout->save();
//
//       }



    }


    public function check_image_path()
    {

        $layouts = Layout::orderby('created_at','ASC')->get();

//        foreach ($layouts as $lay)
//        {
//
//            $lay_edit = Layout::findOrFail($lay->id);
//            $lay_edit->image =  "layouts/".$lay->image;
//            $lay_edit->save();
//
//        }


        return view('pages.check_image_path',compact('layouts'));

    }


//    public function check_image_path_order()
//    {
//
//        $layout_images = LayoutImage::orderby('created_at','ASC')->get();
//
//        foreach ($layout_images as $layout_image)
//        {
//            $lay = LayoutImage::findOrFail($layout_image->id);
//            $lay->image = str_replace('layouts/2020 NEW PHOTOS/', 'layouts/', $lay->image);
//            $lay->save();
//        }
//
//
//        //return view('pages.check_image_path',compact('layouts'));
//
//    }

//    public function check_image_path_order()
//    {
//
//        $layouts = Layout::orderby('created_at','ASC')->get();
//
//        foreach ($layouts as $layout)
//        {
//
//          $old =  Old2SeatLayoutImage::where('seatlayout_id', $layout->old_id)->orderBy('sort_order', 'ASC')->orderBy('seatlayout_image_id', 'DESC')->first();
//           if($old && $layout->old_id)
//           {
//               $lay = Layout::findOrFail($layout->id);
//               $lay->image = $old->image;
//               $lay->save();
//           }
//
//        }
//
//
//        //return view('pages.check_image_path',compact('layouts'));
//
//    }

    public function check_image_path_order()
    {

        $layouts = Layout::orderby('created_at','ASC')->get();

        foreach ($layouts as $lay)
        {
            if($lay->old_id)
                $old_seat_layout_images = Old2SeatLayoutImage::where('seatlayout_id', $lay->old_id)->orderBy('sort_order', 'ASC')->orderBy('seatlayout_image_id', 'DESC')->get();
            foreach ($old_seat_layout_images as $key=>$old_seat_layout_image)
            {
                if($old_seat_layout_images->count()>2 && $key !=0)
                {
                    $lay_image = new LayoutImage();
                    $lay_image->layout_id = $lay->id;
                    $lay_image->image = $old_seat_layout_image->image;
                    $lay_image->save();
                }

            }
        }


        //return view('pages.check_image_path',compact('layouts'));

    }

    public function layout_price()
    {

        $old_layouts = Old2SeatPrice::orderby('price_id','ASC')->get();
       $i=0;
        $layouts = array();
        foreach ($old_layouts as $lay)
        {

            $ex = explode(" ", $lay->code);
            $name = $ex[0];
            $number = sprintf("%04d", $ex[1]);
            $wf_code = trim($name."-".$number);

            $layout = Layout::where('name', $wf_code)->first();



            if(empty($layout))
            {

                $layouts[$i]=array('name' => $wf_code, 'id' => $lay->price_id);;
                $i++;

//                if($lay->sl=="" || !is_numeric($lay->sl) || empty($lay->sl) || $lay->sl==null)
//                {
//                    $sl_price = 0;
//                }
//                 else
//                 {
//                     $sl_price = $lay->sl;
//                 }
//
//                if($lay->delux=="" || !is_numeric($lay->delux) || empty($lay->delux) || $lay->delux==null)
//                {
//                    $delux_price = 0;
//                }
//                else
//                {
//                    $delux_price = $lay->delux;
//                }
//
//
//                DB::table('layout_materials')->insert([
//                    ['material_id' => 1, 'layout_id' => $layout->id, 'price' => $sl_price],
//                    ['material_id' => 2, 'layout_id' => $layout->id, 'price' => $delux_price],
//                ]);
            }



        }

return $layouts;
       // return view('pages.layout_price',compact('layouts'));

    }

}
