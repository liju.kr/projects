<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    public function postLogin(Request $request)
    {
    	if (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
    	    $user = Auth::user();
    	    if($user->status)
            {
                if($user->isAnyUser())
                {
                    // Authentication passed...
                    return response()->json([
                        'status' => 'success',
                        'id' => $user->id,
                        'username' => $user->getName(),
                        'role' => $user->role,
                        'company' => $user->company,
                        'view_configuration' => $user->user_role->view_configuration,
                        'token' => $user->createToken('Access Token')->accessToken,
                        'url'=>url('/').'/'
                    ]);
                }
                else
                {
                    return response()->json([
                        'status' => 'no_permission',
                        'message' => 'No Permission!...',
                        'message_title' => 'Login Failed',
                    ]);
                }
            }
    	    else
            {
                return response()->json([
                    'status' => 'inactive',
                    'message' => 'Inactive User!...',
                    'message_title' => 'Login Failed',
                    ]);
            }

        }
        else
        {
            return response()->json([
                'status' => 'no_match',
                'message' => 'These Credentials Do Not Match Our Records',
                'message_title' => 'Login Failed',
            ]);
        }

    }

public function signUp(Request $request)
{
$validator =  $request->validate([
  'name' => 'required',
  'email' => 'required|email|unique:users',
  'password' => 'required'

      ]);


          $user =   User::create([
              'name' => $request->name,
              'email' => $request->email,
              'status' => 1,
              'verified' => 0,
              'role' => 'distributor',
              'password' => Hash::make($request->password),
          ]);

          return response()->json([
              'status' => 'success',
              'message' => 'Successfully Registered',
              'message_title' => 'Success',
          ]);

}

}
