<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WellfitReportController extends Controller
{
    public function getUserRole(Request $request)
    {

        $user = User::findOrFail(auth()->id());
        $companies = $user->companies;
        $default_company_id = $user->user_role->default_company;

        return $response = [
            'user' => $user,
            'companies' => $companies,
            'default_company_id' => $default_company_id,
        ];

    }

}
