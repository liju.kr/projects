<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WellfitReportAuthController extends Controller
{
    public function postLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->status) {
                if ($user->isAnyReportUser()) {
                    // Authentication passed...
                    return response()->json([
                        'status' => 'success',
                        'id' => $user->id,
                        'username' => $user->getName(),
                        'role' => $user->role,
                        'company' => $user->company,
                        'token' => $user->createToken('Access Token')->accessToken,
                        'url' => url('/') . '/'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'no_permission',
                        'message' => 'No Permission!...',
                        'message_title' => 'Login Failed',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'inactive',
                    'message' => 'Inactive User!...',
                    'message_title' => 'Login Failed',
                ]);
            }

        } else {
            return response()->json([
                'status' => 'no_match',
                'message' => 'These Credentials Do Not Match Our Records',
                'message_title' => 'Login Failed',
            ]);
        }


    }
}