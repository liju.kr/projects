<?php

namespace App\Http\Controllers\Api;


use App\Models\Car;
use App\Models\CarModel;
use App\Models\EmbroideryConfiguration;
use App\Models\EmbroideryOrder;
use App\Models\Layer;
use App\Models\Layout;
use App\Models\Manufacturer;
use App\Models\PriceRange;
use App\Models\Seat;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class EmbroideryApiController extends Controller
{
    //


    public function getHomeData(Request $request)
    {


        $total_cart_count = EmbroideryConfiguration::where('user_id', '=', $request->user_id)
            ->orderBy('id', 'DESC')->count();

        $total_order_count = EmbroideryOrder::where('user_id', '=', $request->user_id)
            ->orderBy('id', 'DESC')->count();


            return response()->json([
                'status'=>"success",
                'total_cart_count' =>  $total_cart_count,
                'total_order_count' =>  $total_order_count,

            ]);

    }
    public function getPriceRanges(Request $request)
    {
        $price_ranges = PriceRange::orderBy('name')->paginate(6);

        if($price_ranges->count())
        {
            return response()->json([
                'status'=>"success",
                'price_ranges' =>  $price_ranges,
            ]);
        }
        else{
        return response()->json([
            'status'=>"error",
            'message'=>"No Price Ranges Brands Found",
        ]);
    }
    }

    public function getDesigns(Request $request)
    {

        $price_range = PriceRange::findOrFail($request->price_range_id);

        $designs = $price_range->seats()->get();

        foreach ($designs as $design)
        {
            $design['base'] = $design->getBaseImageURL();
        }



        if($designs->count())
        {
            return response()->json([
                'status'=>"success",
                'designs' =>  $designs,
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Designs Brands Found",
            ]);
        }
    }


    public function getSingleDesign(Request $request)
    {

        //price_range = 4 (p1) ........   1, 2, 7 (e1, e2, l2)
        //variants    = 1 ........   4
        //design      = 6
        //layer       = 1
        //return $request;

        $design = Seat::where('id', $request->design_id)->first();
        $base=$design->getBaseImageURL();

        $design['base'] = $base;

        $price_range = PriceRange::where('id', $request->price_range_id)->first();


        $layers = Layer::with('variants')
            ->whereHas('variants')
            ->where('seat_id', $request->design_id)->get();



        if($layers->count())
        {
            return response()->json([
                'status' => "success",
                'design' =>  $design,
                'layers' => $layers,
                'price_range' =>  $price_range,
            ]);

        }
        else{
            return response()->json([
                'status' => "success",
                'layers' => "No Design or Layers Found",

            ]);

        }


    }


    public function getCarBrands()
    {

        $car_brands = Manufacturer::where('id','!=',env('CUST_MAN_ID'))
            ->where('id','!=',env('EMBROIDERY_BRAND_ID'))
            ->orderBy('name')
            ->get();

        $material_array = array([
            "id" => 1,
            "name" => "PVC Leather",
        ],
            [
                "id" => 2,
                "name" => "Cloth",
            ]);



          if($car_brands->count())
          {
              return response()->json([
                  'status'=>"success",
                  'car_brands' =>  $car_brands,
                  'material_array' =>  $material_array,
                  'item_vat' =>  env('VAT'),
              ]);
          }
          else{
              return response()->json([
                  'status'=>"error",
                  'message'=>"No Car Brands Found",
              ]);
          }

    }

    public function getCarModels(Request $request)
    {


        $car_models = Car::where('manufacturer_id', $request->car_brand_id)
                                    ->orderBy('name')
                                    ->get();

        if($car_models->count())
        {
            return response()->json([
                'status'=>"success",
                'car_models' =>  $car_models
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Car Model Found",
            ]);
        }

    }

    public function getCarYears(Request $request)
    {


        $car_years = CarModel::where('car_id', $request->car_model_id)
            ->orderBy('id')
            ->get();

        if($car_years->count())
        {
            return response()->json([
                'status'=>"success",
                'car_years' =>  $car_years
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Car Model Year Found",
            ]);
        }

    }

    public function getCarLayouts(Request $request)
    {

        $car_model_ids = CarModel::where('year_from','<=', $request->car_year)
            ->where('year_to','>=', $request->car_year)
            ->where('car_id', $request->car_model_id)
            ->pluck('id');

        $car_layouts = Layout::whereIn('car_model_id', $car_model_ids)
            ->get();

        if($car_layouts->count())
        {
            return response()->json([
                'status'=>"success",
                'car_layouts' =>  $car_layouts
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Seat Layout Found",
            ]);
        }

    }


    public function getSaved(Request $request)
    {


        $saved_configurations = EmbroideryConfiguration::where('user_id', '=', $request->user_id)
            ->orderBy('id', 'DESC')->get();

        if($saved_configurations->count())
        {
            $total_price = $saved_configurations->sum('total_price');
            $total_count = $saved_configurations->count();
            return response()->json([
                'status'=>"success",
                'saved_configurations' =>  $saved_configurations,
                'total_price' =>  $total_price,
                'total_count' =>  $total_count
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Cart Items Found",
            ]);
        }

    }


    public function getOrdered(Request $request)
    {


        $ordered_configurations = EmbroideryOrder::where('user_id', '=', $request->user_id)
            ->orderBy('id', 'DESC')->paginate(8);

        $total_count = EmbroideryOrder::where('user_id', '=', $request->user_id)->count();

        if($ordered_configurations->count())
        {

            return response()->json([
                'status'=>"success",
                'ordered_configurations' =>  $ordered_configurations,
                'total_count' =>  $total_count
            ]);
        }
        else{
            return response()->json([
                'status'=>"error",
                'message'=>"No Ordered Items Found",
            ]);
        }

    }


    public function deleteSaved(Request $request)
    {

        $saved = EmbroideryConfiguration::findOrFail($request->id);
        $saved->delete();

        if( $saved->delete())
        {
            return response()->json([
                'status' => 'success',
                'message' => 'Deleted Successfully',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Please Try Again Later',
            ]);
        }
    }


    public function deleteOrdered(Request $request)
    {

        $ordered = EmbroideryOrder::findOrFail($request->id);

        if( $ordered->delete())
        {
            return response()->json([
                'status' => 'success',
                'message' => 'Deleted Successfully',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Please Try Again Later',
            ]);
        }

    }


    public function saveConfigurations(Request $request)
    {

        $save_configuration = new EmbroideryConfiguration();

        $save_configuration->image_array = json_encode($request->cart_image_array);
        $save_configuration->selections = json_encode($request->cart_data);
        $save_configuration->materials = json_encode($request->cart_layer_materials);
        $save_configuration->vehicle_details = json_encode($request->cart_vehicle_details);
        $save_configuration->user_id = $request->user_id;
        $save_configuration->design_selected_image = $request->cart_design_selected_image;
        $save_configuration->price = $request->item_price;
        $save_configuration->quantity = $request->item_quantity;
        $save_configuration->total_price = $request->item_total_price;
        $save_configuration->vat = $request->item_vat;
        $save_configuration->status = 0;


        if( $save_configuration->save())
        {
            return response()->json([
                'status' => 'success',
                'save_configuration' => $save_configuration,
                'message' => 'Cart Added Successfully',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Please Try Again Later',
            ]);
        }

    }

    public function editConfiguration(Request $request)
    {

        $save_configuration = EmbroideryConfiguration::findOrFail($request->configuration_id);

        $save_configuration->materials = json_encode($request->cart_layer_materials);
        $save_configuration->vehicle_details = json_encode($request->cart_vehicle_details);
        $save_configuration->price = $request->item_price;
        $save_configuration->quantity = $request->item_quantity;
        $save_configuration->total_price = $request->item_total_price;
        $save_configuration->vat = $request->item_vat;


        if( $save_configuration->save())
        {
            return response()->json([
                'status' => 'success',
                'save_configuration' => $save_configuration,
                'message' => 'Cart Updated Successfully',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Please Try Again Later',
            ]);
        }

    }


    public function orderConfigurations(Request $request)
    {



        foreach ($request->ordered_cart_items as $cart_item)
        {
            $order_configuration = new EmbroideryOrder();
            $order_configuration->configuration = json_encode($cart_item);
            $order_configuration->user_id = $request->user_id;
            $order_configuration->remarks = $request->user_remarks;
            $order_configuration->quantity = $cart_item['quantity'];
            $order_configuration->total_price =  $cart_item['total_price'];
            $order_configuration->delivery_date = substr($request->delivery_date,0,10);
            $order_configuration->status = 0;
            $order_configuration->save();

            $saved_configuration = EmbroideryConfiguration::findOrFail($cart_item['id']);
            $saved_configuration->delete();
        }

        $admins = User::where('role', 'data-manager')->where('verified', 1)->get();
        $url = env('APP_URL', '');


        foreach ($admins as $admin)
        {
            $to_name = $admin->name;
            $to_email = $admin->email;
            $from_name = env('MAIL_FROM_ADDRESS', '');
            $from_email = env('MAIL_FROM_NAME', '');

            $data = array('name'=>$admin->name, 'url' => $url, 'body' => "New Orders Submitted, Pending Approval");
            Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email, $from_name, $from_email) {
                $message->to($to_email, $to_name)
                    ->subject("New Order Yaco Embroidery");
                $message->from($from_email, $from_name);
            });
        }


        return response()->json([
                'status' => 'success',
                'message' => 'Order Submitted Successfully',
            ]);

    }


}
