<?php

namespace App\Http\Controllers\Api;

use App\Models\Car;
use App\Models\CarModel;
use App\Models\Gallery;
use App\Models\Layout;
use App\Models\Manufacturer;
use App\Models\Material;
use App\Models\Variant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LayoutsApiController extends Controller
{
    //

    public function postLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->username, 'password' => $request->password, 'status' =>1])) {


            // Authentication passed...
            return response()->json([
                'status' => 'success',
                'id' => Auth::user()->id,
                'email' => Auth::user()->email,
                'device_type' =>$request->device_type,
                'device_id' => $request->device_id,
                'username' => Auth::user()->getName(),
                'token' => Auth::user()->createToken('Access Token')->accessToken,
                'url'=>url('/').'/'
            ]);
        }
        else
        {
            return response()->json(['status' => 'error']);
        }

    }

    public function getBrands(Request $request)
    {
        $brands = Manufacturer::where('id','!=',env('CUST_MAN_ID'))
            ->where('id','!=',env('EMBROIDERY_BRAND_ID'))
            ->orderBy('name')
            ->get();

        if($brands->count())
        {
            foreach($brands as $key => $brand){
                $brands[$key]['image'] = env('APP_URL')."/storage/".$brand->image;
            }
            return response()->json([
                'status' => "success",
                'brands' => $brands,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }


    public function getCarmodels(Request $request)
    {
        $car_models = Car::where('manufacturer_id',$request->brand_id)
            ->orderBy('name')
            ->get();

        if($car_models->count())
        {
            return response()->json([
                'status' => "success",
                'car_models' => $car_models,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }


    public function getYears(Request $request)
    {
        $car_years = CarModel::where('car_id',$request->carmodel_id)
            ->orderBy('id', 'DESC')
            ->get();

        if($car_years->count())
        {
            return response()->json([
                'status' => "success",
                'years' => $car_years,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }

    public function getSeatLayouts(Request $request)
    {

        $query = CarModel::query();

        if($request->year_from && $request->year_to)
        {
           // $query = $query->where('year_from','=', $request->year_from);
           // $query = $query->where('year_to','=', $request->year_to);
            $query = $query->whereBetween('year_from', [$request->year_from, $request->year_to]);
            $query = $query->whereBetween('year_to', [$request->year_from, $request->year_to]);

        }
        $query = $query->where('car_id', $request->carmodel_id);

        $model_ids=  $query->pluck('id');


        $layouts = Layout::whereIn('car_model_id', $model_ids)
            ->with('images')
            ->with('car_model')
            ->get();

        if($layouts->count())
        {
            foreach($layouts as $key => $layout){
                $layouts[$key]['image'] = env('APP_URL')."/storage/".$layout->image;
            }
            return response()->json([
                'status' => "success",
                'layouts' => $layouts,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }


    public function getSeatLayout(Request $request)
    {

   $layout = Layout::where('id', $request->seatlayout_id)
            ->with('images')
            ->with('car_model')
            ->get();

        if($layout->count())
        {
            foreach($layout as $key => $lay){
                $layout[$key]['image'] = env('APP_URL')."/storage/".$lay->image;

                foreach($lay->images as $key2 => $img){
                    $lay->images[$key2]['image'] = env('APP_URL')."/storage/".$img->image;
                }

            }
            return response()->json([
                'status' => "success",
                'layout' => $layout,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }


    public function getSeatLayoutYear(Request $request)
    {

        $query = CarModel::query();

        $query = $query->where('car_id', $request->carmodel_id)
            ->with('layouts');

        $years=  $query->get();



        if($years->count())
        {
            foreach ($years as $year)
            {
                foreach($year->layouts as $key => $layout){
                    $year->layouts[$key]['image'] = env('APP_URL')."/storage/".$layout->image;
                }
            }


            return response()->json([
                'status' => "success",
                'years' => $years,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }

    public function getColours(Request $request)
    {
        $colors = Variant::orderBy('name')
           // ->with('layer.seat')
          //  ->whereHas('layer.seat')
            ->get();

        if($colors->count())
        {

            foreach ($colors as $key=>$color)
            {

                $colors[$key]['image'] = env('APP_URL')."/storage/".$color->image;
               // $color->layer['seat']->image = env('APP_URL')."/storage/".$color->layer['seat']->image;

            }

            return response()->json([
                'status' => "success",
                'colors' => $colors,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }

    public function getMaterials(Request $request)
    {
        $materials = Material::with('sub_materials')
            ->orderBy('name')
            ->get();

        if($materials->count())
        {
            return response()->json([
                'status' => "success",
                'materials' => $materials,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }


    public function getPrices(Request $request)
    {
        $layouts = Layout::with('materials')->whereHas('materials')->get();

        if($layouts->count())
        {
            foreach($layouts as $key => $layout){
                $layouts[$key]['image'] = env('APP_URL')."/storage/".$layout->image;
            }
            return response()->json([
                'status' => "success",
                'prices' => $layouts,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }

    public function getGalleryItems(Request $request)
    {
        $gallery_items = Gallery::get();

        if ($gallery_items->count()) {

            foreach($gallery_items as $key => $gallery_item){
                $gallery_items[$key]['image'] = env('APP_URL')."/storage/".$gallery_item->image;
            }
            return response()->json([
                'status' => "success",
                'gallery_items' => $gallery_items,
            ]);
        } else {
            return response()->json([
                'status' => "error"
            ]);
        }
    }
        public function getGalleryItem(Request $request)
       {
        $gallery_item = Gallery::findOrFail($request->gallery_id);

        if($gallery_item->count())
        {

                $gallery_item->image = env('APP_URL')."/storage/".$gallery_item->image;

            return response()->json([
                'status' => "success",
                'gallery_item' => $gallery_item,
            ]);
        }
        else
        {
            return response()->json([
                'status' => "error"
            ]);
        }

    }



}
