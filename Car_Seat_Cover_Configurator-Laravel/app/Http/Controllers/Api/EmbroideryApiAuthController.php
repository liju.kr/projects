<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
class EmbroideryApiAuthController extends Controller
{
    //
    public function postLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'yaco_distributor']))
        {

            if(Auth::user()->status)
            {
                // Authentication passed...
                return response()->json([
                    'status' => 'success',
                    'message' => 'Login Success',
                    'id' => Auth::user()->id,
                    //'status_id' => Auth::user()->status,
                    'status_id' =>1,
                    'token' => Auth::user()->createToken('Access Token')->accessToken,
                    'url'=>url('/').'/'
                ]);

            }
            else {

                return response()->json([
                    'status' => 'not_verified',
                    'message' => 'Your Account Not Verified / Activated']);

            }


        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Login Email ID Or Password Not Match']);
        }

    }

    public function signUp(Request $request)
    {
//return $request->contact;
        $admins = User::where('role', 'data-manager')->where('verified', 1)->get();

        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'shop' => 'required',
            'contact' => 'required|numeric',
            'password' => 'required|min:6'

        ]);


        if($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user =  User::create([
                'name' => $request->name,
                'shop_name' => $request->shop,
                'email' => $request->email,
                'contact' => $request->contact,
                'status' => 0,
                'verified' => 0,
                'role' => 'distributor',
                'password' => Hash::make($request->password),
            ]);

            if($user) {


                $url = env('APP_URL', '');


                foreach ($admins as $admin)
                {
                    $to_name = $admin->name;
                    $to_email = $admin->email;
                    $from_name = env('MAIL_FROM_ADDRESS', '');
                    $from_email = env('MAIL_FROM_NAME', '');

                    $data = array('name'=>$admin->name, 'url' => $url, 'body' => "New User Registered, Pending Approval");
                    Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email, $from_name, $from_email) {
                        $message->to($to_email, $to_name)
                            ->subject("New User Yaco Embroidery");
                        $message->from($from_email, $from_name);
                    });
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully Registered',
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);

            }
        }

        //return $request->customer_id;
    }


    public function loginStatus(Request $request)
    {

        $user= User::findOrFail($request->id);

        if ($user->count())
        {
          //  if($user->status)
           if($user->status)
            {
                // Authentication passed...
                return response()->json([
                    'status' => 'success',
                    'message' => 'Verified / Active User',
                    'status_id' => true,
                ]);

            }
            else {

                return response()->json([
                    'status' => 'success',
                    'message' => 'Account Not Verified / Activated',
                    'status_id' => false,

                ]);

            }


        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Oops!.. Something Went To Wrong',
            ]);
        }


    }


    public function getProfile(Request $request)
    {


        $user= User::findOrFail($request->user_id);

        if ($user->count())
        {
            return response()->json([
                'status' => 'success',
                'profile'=> $user,
            ]);

        }
        else
        {
            return response()->json([
                'status' => 'success',
                'message'=> 'Please Try Again Later',
            ]);

        }


    }


    public function updateProfile(Request $request)
    {



        $validator =   Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required|email|unique:users,email,'.$request->user_id,
            'shop_name' => 'required',
            'contact' => 'required|numeric',
            'password' => 'required|min:6'

        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user =  User::findOrFail($request->user_id);

            if (Hash::check($request->password, $user->password))
            {
                $user->name = $request->name;
                $user->email = $request->email;
                $user->shop_name = $request->shop_name;
                $user->contact = $request->contact;
                $user->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Updated Successfully',
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'password_not_match',
                    'message' => 'Your Entered Password Is Wrong',
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }


        }

    }

    public function changePassword(Request $request)
    {
        $validator =   Validator::make($request->all(), [

            'password' => 'required|min:6',
            'old_password' => 'required|min:6'

        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors(),
            ]);

        }
        else
        {
            $user =  User::findOrFail($request->user_id);

            if (Hash::check($request->old_password, $user->password))
            {
                $user->password = Hash::make($request->password);
                $user->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Password Updated Successfully',
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'password_not_match',
                    'message' => 'Your Old Password Is Wrong',
                    //'token' => Auth::user()->createToken('Access Token')->accessToken,

                ]);
            }


        }

    }

}
