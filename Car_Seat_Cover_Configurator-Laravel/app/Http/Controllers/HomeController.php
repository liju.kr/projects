<?php

namespace App\Http\Controllers;

use App\Models\CarModel;
use App\Models\Colour;
use App\Models\Company;
use App\Models\Gallery;
use App\Models\Layout;
use App\Models\LayoutImage;
use App\Models\Manufacturer;
use App\Models\Material;
use App\Models\Order;
use App\Models\Pattern;
use App\User;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users_count = User::query();
        if(auth()->user()->company !="ALL") $users_count = $users_count->where('company', auth()->user()->company);
        $users_count = $users_count->count();


        $layouts_count = Layout::count();
        $manufacturer_count = Manufacturer::count();
        $colour_count = Colour::count();
        $pattern_count = Pattern::count();
        $gallery_count = Gallery::count();
        $material_count = Material::count();
        $company_count = Company::count();
        $order_count = Order::count();

        $common_model = CarModel::findOrFail(env('COMMON_CAR_MODEL_ID'));
         $common_embroidery_model = CarModel::findOrFail(env('EMBROIDERY_CAR_MODEL_ID'));

       $common_design_count = $common_model->seats()->count();
       $embroidery_design_count = $common_embroidery_model->seats()->count();

        $response= [
            'users_count' => $users_count,
            'layouts_count' => $layouts_count,
            'manufacturer_count' => $manufacturer_count,
            'colour_count' => $colour_count,
            'pattern_count' => $pattern_count,
            'common_design_count' => $common_design_count,
            'embroidery_design_count' => $embroidery_design_count,
            'gallery_count' => $gallery_count,
            'material_count' => $material_count,
            'company_count' => $company_count,
            'order_count' => $order_count,
        ];
        return view('home', compact('response'));
    }
    public function unverified()
    {
        return view('pages.errors.unverified');
    }
}
