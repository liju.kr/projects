<?php

namespace App\Http\Controllers\Data;

use App\Models\Material;
use App\Models\SubMaterial;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SubMaterialController extends Controller
{
    //

    public function index($material_id)
    {
        $material = Material::findOrFail($material_id);
        $page="sub_materials";
        return view('pages.materials.partials.list-sub',compact('material', 'page'));
    }
    public function create($material_id)
    {
        $page="sub_materials";
        $material = Material::findOrFail($material_id);
        return view('pages.materials.partials.add-sub-materials',compact('material', 'page'));
    }
    public function edit($material_id, $id)
    {

        $page="sub_materials";
        $material = Material::findOrFail($material_id);
        $sub_material = SubMaterial::findOrFail($id);
        return view('pages.materials.partials.edit-sub-materials',compact('material', 'sub_material', 'page'));
    }

    public function store(Request $request, $material_id)
    {
        Validator::make($request->all(), [
            'name' => 'required'
        ])->validate();

        $sub_material = new SubMaterial();
        $sub_material->name = $request->name;
        $sub_material->material_id = $material_id;
        $sub_material->save();
        return redirect('/materials/'.$material_id.'/sub_materials')->with('status', 'Sub Material Added!');
    }
    public function update(Request $request, $material_id, $id)
    {
        Validator::make(Input::all(), [
            'name' => 'required'
        ])->validate();

        $sub_material = SubMaterial::findOrFail($id);
        $sub_material->name =  Input::get('name');
        $sub_material->save();
        return back()->with('status', 'Sub Material Updated!');
    }
    public function destroy($material_id, $id)
    {
        // delete
        $sub_material = SubMaterial::findOrFail($id);
        $sub_material->delete();

        // redirect
        return back()->with('status', 'Material deleted!');
    }
}
