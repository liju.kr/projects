<?php

namespace App\Http\Controllers\Data;

use App\Models\Seat;
use App\Models\SeatPhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
class SeatPhotoController extends Controller
{
    //

    public function index($seat_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $seat = Seat::findOrFail($seat_id);
        $tab = "seat_photos";
        return view('pages.config.tabs.seat_photos',compact('seat','tab'));
    }

    public function edit($seat_id, $id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $seat_photo =  SeatPhoto::findOrFail($id);
         $seat = Seat::findOrFail($seat_id);
        $tab="seat_photos";
        return view('pages.config.tabs.seat_photos',compact('seat','tab','seat_photo'));
    }
    public function store(Request $request, $seat_id)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ])->validate();

        $path = $request->file('image')->store('public/seat_photos');

        $seat_photo = new SeatPhoto();
        $seat_photo->name = $request->name;
        $seat_photo->seat_id = $seat_id;
        $seat_photo->image = str_replace("public/","",$path);
        $seat_photo->save();

        return back()->with('status', 'Seat Photo Added!');
    }
    public function update($seat_id, $id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/seat_photos');
        $seat_photo = SeatPhoto::find($id);
        $seat_photo->name = Input::get('name');
        if(Input::file('image'))  $seat_photo->image = str_replace("public/","",$path);
        $seat_photo->save();

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Seat Photo Modified!');
    }
    public function destroy($seat_id, $id)
    {
        // delete
        $seat_photo = SeatPhoto::find($id);
        $seat_photo->delete();

        // redirect
        return back()->with('status', 'Seat Photo Deleted!');
    }

}
