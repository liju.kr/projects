<?php

namespace App\Http\Controllers\Data;

use App\Models\Layout;
use App\Models\Seat;
use App\Models\SeatPhoto;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Input;

class CarModelController extends Controller
{
    public function index($car_id)
    {
    	$car = Car::findOrFail($car_id);
    	return view('pages.models.partials.list',compact('car'));
    }
    public function create($car_id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $car = Car::findOrFail($car_id);
        return view('pages.models.partials.add-model',compact('car'));
    }
    public function edit($car_model_id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $car_model =  CarModel::findOrFail($car_model_id);
        $car = Car::findOrFail($car_model->car_id);
        return view('pages.models.partials.edit-model',compact('car','car_model'));
    }
    public function store(Request $request, $car_id)
    {
        Validator::make($request->all(), [
            'year_from' => 'required|numeric|digits:4',
            'year_to' => 'required|numeric|digits:4|min:'.Input::get('year_from'),
        ])->validate();

        $car = Car::findOrFail($car_id);
        $car_model = new CarModel();
        $car_model->name = $request->year_from." - ".$request->year_to;
        $car_model->year_from = $request->year_from;
        $car_model->year_to = $request->year_to;
        $car_model->car_id = $car->id;
        $car_model->user_id = auth()->id();
        $car_model->save();
        return redirect('/cars/'.$car_id.'/models')->with('status', 'Model Added!');
    }
    public function update($car_model_id)
    {
        Validator::make(Input::all(), [
            'year_from' => 'required|numeric|digits:4',
            'year_to' => 'required|numeric|digits:4|min:'.Input::get('year_from'),
        ])->validate();

        $car_model = CarModel::findOrFail($car_model_id);
        $car_model->name = Input::get('year_from')." - ".Input::get('year_to');
        $car_model->year_from =  Input::get('year_from');
        $car_model->year_to =  Input::get('year_to');
        $car_model->user_id = auth()->id();
        $car_model->save();
        return back()->with('status', 'Car Model Updated!');
    }
    public function destroy($id)
    {
        // delete
        $car_model = CarModel::find($id);
        $car_id = $car_model->car_id;
        $car_model->delete();
        // redirect
        return redirect('/cars/'.$car_id.'/models')->with('status', 'Model deleted!');
    }
    public function getLayouts(Request $request)
    {

          $query = CarModel::query();

          if($request->year)
          {
             $query = $query->where('year_from','<=', $request->year);
             $query = $query->where('year_to','>=', $request->year);
          }
        $query = $query->where('car_id', $request->car_id);
        $query = $query->orderBy('year_from', 'desc');

        $model_ids=  $query->pluck('id');

        $user = Auth::user();
        if($user->company=="YACO") //  if($user->role == "sub-distributor")
        {
            $layouts = Layout::whereIn('car_model_id', $model_ids)
                ->with('images')
                ->with('galleries')
                ->with('layout_designs')
                ->with('car_model.car.manufacturer')
                ->join('car_models', 'layouts.car_model_id', '=', 'car_models.id')
                ->orderBy('car_models.year_from', 'desc')
                ->get(['layouts.*']);
        }
        else
        {
            $layouts = Layout::whereIn('car_model_id', $model_ids)
                ->with('images')
                ->with('layout_designs')
                ->with('car_model.car.manufacturer')
                ->join('car_models', 'layouts.car_model_id', '=', 'car_models.id')
                ->orderBy('car_models.year_from', 'desc')
                ->get(['layouts.*']);
        }



        return $layouts;
    }
    public function searchLayouts(Request $request)
    {

        if(isset($request->per_page))
        {
            $layouts = Layout::where('name', 'like', '%' . $request->wf_code . '%')
                ->orderBy('id', 'desc')
                ->where('id','!=',env('EMBROIDERY_LAYOUT_ID'))
                ->paginate(36);
        }
        else
        {
            $user = Auth::user();
            if($user->company=="YACO") //  if($user->role == "sub-distributor")
            {
                $layouts = Layout::where('name', 'like', '%' . $request->wf_code . '%')
                    ->with('images')
                    ->with('galleries')
                    ->with('layout_designs')
                    ->where('id', '!=', env('EMBROIDERY_LAYOUT_ID'))
                    ->with('car_model.car.manufacturer')
                    ->orderBy('id', 'desc')
                    ->paginate(6);
            }
            else
            {
                $layouts = Layout::where('name', 'like', '%' . $request->wf_code . '%')
                    ->with('images')
                    ->with('layout_designs')
                    ->where('id', '!=', env('EMBROIDERY_LAYOUT_ID'))
                    ->with('car_model.car.manufacturer')
                    ->orderBy('id', 'desc')
                    ->paginate(6);
            }
        }


        return $layouts;
    }
    public function getSeatModels(Request $request)
    {

        $model = CarModel::findOrFail($request)->first();
        $common_seat_ids = Seat::where('car_model_id', env('COMMON_CAR_MODEL_ID'))->pluck('id');

        $model_seats = $model->seats()->with('seat_photos')->get();
        //$model_seats = $model->seats()->with('seat_photos')->get();

        $custom_seats = Seat::with('seat_photos')->where('id',env('COMMON_MODEL_ID'))->get();
//        $common_car_model = CarModel::findOrFail(env('COMMON_CAR_MODEL_ID'));
//        $common_seats = $common_car_model->seats()->with('seat_photos')->get();

        $seats = $model_seats;
        $common_model_seats = $model->seats()->whereIn('seat_id',$common_seat_ids)->with('seat_photos')->get();
        $common_model_seats = $common_model_seats ->merge($custom_seats);
        if(!$model_seats->count()) $seats = $seats->merge($custom_seats);
        $custom = true;

        //        if($model_seats->count()>0)
//        {
//            $seats = $model_seats;
//            $custom = false;
////            if($model->id != env('EMBROIDERY_CAR_MODEL_ID'))
////            {
////                $seats = $seats->merge($common_seats);
////            }
//
//        }
//        else
//            {
//          //  $seats= $custom_seats;
//            $seats= $custom_seats;
//            $custom = true;
//          }


        $common_style  = array("name"=>"0000", "image"=>env('APP_URL').'/images/Image-Not-Available.png');
        $json = ['success'=>true,'seats' => $seats, 'common_model_seats'=>$common_model_seats,  'custom'=>$custom, 'common_style'=>$common_style ];
        return response()->json($json, '200');
    }

    public function getMaterials(Request $request)
    {

        $layout = Layout::findOrFail($request)->first();
      $materials = $layout->materials()->with('sub_materials')->where('price', '!=', 0)->get();

        $materials_array = array([
          "created_at"=>null,
        "deleted_at" => null,
        "id" => 1,
        "name" => env('MATERIAL_NAME'),
        "pivot" => array("layout_id" => 0, "material_id" => 0, "price" => 50),
       "sub_materials" => array([
               "created_at" => null,
               "deleted_at" => null,
               "id" => 1,
               "material_id" => 1,
               "name" => env('SUB_MATERIAL_NAME_1'),
               "updated_at" => null
           ],
               [
                   "created_at" => "",
                   "deleted_at" => "",
                   "id" => 2,
                   "material_id" => 1,
                   "name" => env('SUB_MATERIAL_NAME_2'),
                   "updated_at" => null
               ]),
           "updated_at" => null

      ]);

        if($materials->count())
        {
            return $materials;
        }
        else
        {
            return $materials_array;
        }

    }


}
