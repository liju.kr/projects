<?php

namespace App\Http\Controllers\Data;

use App\Models\PriceRange;
use App\Models\Seat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class PriceRangeController extends Controller
{
    //
    public function index()
    {
        $price_ranges = PriceRange::get();
        return view('pages.price_ranges.partials.list',compact('price_ranges'));
    }
    public function create()
    {

        return view('pages.price_ranges.partials.add-price_ranges');
    }
    public function edit($id)
    {

        return view('pages.price_ranges.partials.edit-price_ranges',compact('price_range'));
    }
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'numeric|required'
        ])->validate();


        $price_range = new PriceRange();
        $price_range->name = $request->name;
        $price_range->price = $request->price;
        $price_range->save();



        return redirect('/price_ranges/')->with('status', 'Price Range Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'price' => 'numeric|required'
        ])->validate();

        $price_range = PriceRange::findOrFail($id);
        $price_range->name =  Input::get('name');
        $price_range->price =  Input::get('price');
        $price_range->save();
        return back()->with('status', 'Price Range Updated!');
    }
    public function destroy($id)
    {
        // delete
        $price_range = PriceRange::findOrFail($id);
        $price_range->delete();

        // redirect
        return back()->with('status', 'Price Range deleted!');
    }
}
