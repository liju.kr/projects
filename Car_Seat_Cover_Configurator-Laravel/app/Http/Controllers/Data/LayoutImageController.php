<?php

namespace App\Http\Controllers\Data;

use App\Models\LayoutImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LayoutImageController extends Controller
{
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,bmp,png',
        ])->validate();

        $path = $request->file('image')->store('public/layouts');

        $layout = new LayoutImage();
        $layout->layout_id = $request->layout_id;
        $layout->image = str_replace("public/","",$path);
        $layout->save();

        return redirect()->back()->with('status', 'Layout Added!');
    }
    public function destroy($id)
    {
        // delete
        $layout = LayoutImage::find($id);
        $layout->delete();

        // redirect
        return back()->with('status', 'Image Deleted!');
    }
}
