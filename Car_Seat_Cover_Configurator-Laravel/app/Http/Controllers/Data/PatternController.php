<?php

namespace App\Http\Controllers\Data;

use App\Models\Pattern;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class PatternController extends Controller
{
    public function index()
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $patterns = Pattern::orderby('name')->get();
        return view('pages.pattern.partials.list',compact('patterns'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        return view('pages.pattern.partials.add-pattern');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $pattern = Pattern::find($id);
        return view('pages.pattern.partials.edit-pattern',compact('pattern'));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'extra_price' => 'required|numeric|min:0',
            'image' => 'required|mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $path = $request->file('image')->store('public/patterns');
        $pattern = new Pattern();
        $pattern->name = $request->name;
        $pattern->extra_price = $request->extra_price;
        $pattern->code = $request->code;
        $pattern->image = str_replace("public/","",$path);
        $pattern->save();

        return back()->with('status', 'Pattern Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'extra_price' => 'required|numeric|min:0',
            'image' => 'mimes:jpeg,bmp,png|max:10240',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/patterns');
        $pattern = Pattern::find($id);
        $pattern->name = Input::get('name');
        $pattern->extra_price = Input::get('extra_price');
        $pattern->code = Input::get('code');
        if(Input::file('image'))  $pattern->image = str_replace("public/","",$path);
        $pattern->save();

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Pattern Modified!');
    }
    public function destroy($id)
    {
        // delete
        $pattern = Pattern::find($id);
        $pattern->delete();

        // redirect
        return redirect('/patterns/')->with('status', 'Pattern Deleted!');
    }
}
