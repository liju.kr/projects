<?php

namespace App\Http\Controllers\Data;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Manufacturer;
use App\Models\Car;
use Validator;

class CarController extends Controller
{
    public function index($manufacturer_id)
    {
		$manufacturer = Manufacturer::findOrFail($manufacturer_id);
    	return view('pages.car.partials.list',compact('manufacturer'));
    }
    public function create($manufacturer_id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $manufacturer = Manufacturer::findOrFail($manufacturer_id);
        return view('pages.car.partials.add-car',compact('manufacturer'));
    }
    public function edit($car_id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $car = Car::findOrFail($car_id);
        $manufacturer = Manufacturer::findOrFail($car->manufacturer_id);
        return view('pages.car.partials.edit-car',compact('manufacturer','car'));
    }
    public function store(Request $request, $manufacturer_id)
    {
        Validator::make($request->all(), [
            'name' => 'required'
        ])->validate();

    	$manufacturer = Manufacturer::findOrFail($manufacturer_id);
        $car = new  Car();
        $car->name = $request->name;
        $car->manufacturer_id = $manufacturer_id;
        $car->user_id = auth()->id();
        $car->save();
        return redirect('/manufacturers/'.$manufacturer_id.'/cars')->with('status', 'Car Added!');
    }
    public function update($id)
    {
         Validator::make(Input::all(), [
		    'name' => 'required',
		])->validate();
        $car = Car::find($id);
        $car->name = Input::get('name');
        $car->user_id = auth()->id();
        $car->save();

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Car Modified!');
    }
    public function destroy($id)
    {
        // delete
        $car = Car::find($id);
        $manufacturer_id = $car->manufacturer_id;
        $car->delete();

        // redirect
        return redirect('/manufacturers/'.$manufacturer_id.'/cars')->with('status', 'Car deleted!');

    }
    public function getModels(Request $request)
    {
        $car = Car::findOrFail($request)->first();
        return $car->models;
    }

}
