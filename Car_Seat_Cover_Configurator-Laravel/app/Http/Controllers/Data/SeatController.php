<?php

namespace App\Http\Controllers\Data;

use App\Models\Car;
use App\Models\CarModelSeat;
use App\Models\Manufacturer;
use App\Models\PriceRange;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CarModel;
use App\Models\Seat;
use Illuminate\Support\Facades\Input;
use Validator;

class SeatController extends Controller
{
    public function index($model_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $car_model = CarModel::findOrFail($model_id);
        $tab = "models";
    	return view('pages.seats.partials.seats-list',compact('car_model','tab'));
    }

    public function create($model_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $car_model = CarModel::findOrFail($model_id);
        $seats = Seat::where('car_model_id', '!=', 0)->where('car_model_id', env('COMMON_CAR_MODEL_ID'))->orderby('name', 'ASC')->get();
        $tab = "add-model";
        $price_ranges = PriceRange::orderby('id', 'DESC')->get();
        return view('pages.seats.partials.add-seat',compact('car_model','tab', 'seats', 'price_ranges'));
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
       $seat = Seat::findOrFail($id);
       $car_model = CarModel::findOrFail($seat->car_model_id);

        $price_ranges = PriceRange::orderby('id', 'DESC')->get();
        $assignedRanges  = $seat->price_ranges->pluck('id')->toArray();

        return view('pages.seats.partials.edit-seat',compact('car_model','seat', 'price_ranges', 'assignedRanges'));
    }
    public function store(Request $request, $car_model_id)
    {

        if(env('EMBROIDERY_CAR_MODEL_ID')==$car_model_id)
        {
            Validator::make($request->all(), [
                'name' => 'required',
                'image' => 'required|mimes:jpeg,bmp,png',
                'extra_price' => 'required',
                'price_range' => 'required|min:1',
            ])->validate();
        }
        else
        {
            Validator::make($request->all(), [
                'name' => 'required',
                'extra_price' => 'required',
                'image' => 'required|mimes:jpeg,bmp,png',
            ])->validate();
        }
		$path = $request->file('image')->store('public/seats');

        $seat = new Seat();
        $seat->name = $request->name;
        $seat->extra_price = $request->extra_price;
        $seat->car_model_id = $car_model_id;
        $seat->image = str_replace("public/","",$path);
        $seat->save();


        if(env('EMBROIDERY_CAR_MODEL_ID')==$car_model_id)
        {
            $seat->price_ranges()->attach($request->price_range);

        }

        $car_model_seat = new CarModelSeat();
        $car_model_seat->seat_id = $seat->id;
        $car_model_seat->car_model_id = $car_model_id;

        $car_model_seat->save();




        return back()->with('status', 'Seat Added!');
    }


    public function storeExisting(Request $request, $car_model_id)
    {
        Validator::make($request->all(), [
            'seat_id' => 'required|unique:car_model_seats,seat_id,null,null,car_model_id,'.$car_model_id,

        ])->validate();


        $car_model_seat = new CarModelSeat();
        $car_model_seat->seat_id = $request->seat_id;
        $car_model_seat->car_model_id = $car_model_id;

        $car_model_seat->save();

        return back()->with('status', 'Seat Added!');
    }



    public function update(Request $request, $id)
    {
        $seat = Seat::find($id);
        if(env('EMBROIDERY_CAR_MODEL_ID')==$seat->car_model_id) {

            Validator::make(Input::all(), [
                'name' => 'required',
                'price_range' => 'required|min:1',
                'extra_price' => 'required',
                'image' => 'mimes:jpeg,bmp,png',
            ])->validate();
        }
        else
            {
                Validator::make(Input::all(), [
                    'name' => 'required',
                    'extra_price' => 'required',
                    'image' => 'mimes:jpeg,bmp,png',
                ])->validate();
        }

        if(Input::file('image')) $path = Input::file('image')->store('public/seats');

        $seat->name = Input::get('name');
        $seat->extra_price = Input::get('extra_price');

        if(Input::file('image'))  $seat->image = str_replace("public/","",$path);
        $seat->save();

        if(env('EMBROIDERY_CAR_MODEL_ID')==$seat->car_model_id) {
            $seat->price_ranges()->sync($request->price_range);
        }


        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Seat Modified!');
    }
    public function destroy($id)
    {
        // delete
        $seat = Seat::find($id);
        $car_model_seats = CarModelSeat::where('seat_id', '=', $id)->get();
        foreach ($car_model_seats as $car_model_seat)
        {
            $car_model_seat->delete();
        }
     $seat->delete();
   // redirect
        return back()->with('status', 'Seat deleted!');
    }

    public function deleteSeat(Request $request, $id)
    {
        $car_model_seat = CarModelSeat::where('seat_id', '=', $id)->where('car_model_id', '=', $request->car_model_id)->first();
        $car_model_seat->delete();

        // redirect
        return back()->with('status', 'Assigned Seat deleted!');
    }
}
