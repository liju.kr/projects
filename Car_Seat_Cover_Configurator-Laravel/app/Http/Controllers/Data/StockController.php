<?php

namespace App\Http\Controllers\Data;

use App\Models\CarModel;
use App\Models\ColourCategory;
use App\Models\Layout;
use App\Models\Material;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockController extends Controller
{
    public function getStockPageDetailFilters(Request $request)
    {

        if(gettype($request->layout_id)=="integer")
        {
            $layout = Layout::findOrFail($request->layout_id);
            $car_model = $layout->car_model;
            $car = $car_model->car;
            $manufacturer = $car->manufacturer;
        }
        else
        {
            $layout = [];
            $car_model = [];
            $car = [];
            $manufacturer =[];
        }

        $user = User::findOrFail($request->user_id);
        $companies = $user->user_role->companies;
        $assigned_companies = $user->companies;
        $default_company_id = $user->user_role->default_company;
        $yaco_materials = Material::select('id', 'name')->get();
        $wellfit_materials = ColourCategory::select('id', 'name')->get();



        $companies_array = explode(",", $companies);


        $colours_array = array([
            "id"=>1,
            "value" => "ALL",
            "name" => "ALL",
        ],
            [
                "id"=>2,
                "value" => "BEIGE",
                "name" => "BEIGE",
            ],
            [
                "id"=>3,
                "value" => "GREY",
                "name" => "GREY",
            ],
            [
                "id"=>4,
                "value" => "BLACK",
                "name" => "BLACK",
            ],
            [
                "id"=>5,
                "value" => "ORIENT",
                "name" => "ORIENT",
            ],
            [
                "id"=>6,
                "value" => "MAROON",
                "name" => "MAROON",
            ],
            [
                "id"=>7,
                "value" => "BROWN BEIGE",
                "name" => "BROWN BEIGE",
            ],
            [
                "id"=>8,
                "value" => "CHOCOLATE",
                "name" => "CHOCOLATE",
            ],
            [
                "id"=>9,
                "value" => "SAMAN TAN",
                "name" => "SAMAN TAN",
            ],
        );

        $wellfit_colours = array([
            "id"=>1,
            "value" => "ALL",
            "name" => "ALL",
        ],
            [
                "id"=>2,
                "value" => "BEIGE",
                "name" => "BEIGE",
            ],
            [
                "id"=>3,
                "value" => "GREY",
                "name" => "GREY",
            ],
            [
                "id"=>4,
                "value" => "BLACK",
                "name" => "BLACK",
            ],
            [
                "id"=>5,
                "value" => "ORIENT",
                "name" => "ORIENT",
            ],
            [
                "id"=>6,
                "value" => "MAROON",
                "name" => "MAROON",
            ],
            [
                "id"=>7,
                "value" => "BROWN BEIGE",
                "name" => "BROWN BEIGE",
            ],
            [
                "id"=>8,
                "value" => "CHOCOLATE",
                "name" => "CHOCOLATE",
            ],
            [
                "id"=>9,
                "value" => "SAMAN TAN",
                "name" => "SAMAN TAN",
            ],
        );

        $yaco_colours = array([
            "id"=>1,
            "value" => "ALL",
            "name" => "ALL",
        ],
            [
                "id"=>2,
                "value" => "BEIGE",
                "name" => "BEIGE",
            ],
            [
                "id"=>3,
                "value" => "GREY",
                "name" => "GREY",
            ],
            [
                "id"=>4,
                "value" => "BLACK",
                "name" => "BLACK",
            ],
            [
                "id"=>5,
                "value" => "ORIENT",
                "name" => "ORIENT",
            ],
            [
                "id"=>6,
                "value" => "MAROON",
                "name" => "MAROON",
            ],
            [
                "id"=>7,
                "value" => "BROWN BEIGE",
                "name" => "BROWN BEIGE",
            ],
            [
                "id"=>8,
                "value" => "CHOCOLATE",
                "name" => "CHOCOLATE",
            ],
            [
                "id"=>9,
                "value" => "SAMAN TAN",
                "name" => "SAMAN TAN",
            ],
        );

        $materials_array = array([
            "id"=>1,
            "value" => "ALL",
            "name" => "ALL",
        ],
            [
                "id"=>2,
                "value" => "SL",
                "name" => "SL",
            ],
            [
                "id"=>3,
                "value" => "DELUX",
                "name" => "DELUXE",
            ],
            [
                "id"=>4,
                "value" => "SUPER",
                "name" => "SUPER",
            ],
            [
                "id"=>5,
                "value" => "CSL",
                "name" => "CSL",
            ],
        );

        //also change in below function getUserRoles
        $database  = array("yaco"=>"Focus5034", "wellfit"=>"Focus5019", "wellfit_international_dubai"=>"Focus5054", "demnat"=>"Focus5099");


        return $response = [
            'layout' => $layout,
            'database' => $database,
            'car_model' => $car_model,
            'car' => $car,
            'manufacturer' => $manufacturer,
            'companies_array' => $companies_array,
            'colours_array' => $colours_array,
            'materials_array' => $materials_array,
            'assigned_companies' => $assigned_companies,
            'default_company_id' => $default_company_id,
            'yaco_materials' => $yaco_materials,
            'wellfit_materials' => $wellfit_materials,
            'wellfit_colours' => $wellfit_colours,
            'yaco_colours' => $yaco_colours,
        ];

    }

    public function getUserRoles(Request $request)
    {

        $user = User::findOrFail($request->user_id);
        $companies = $user->user_role->companies;
        $assigned_companies = $user->companies;
        $default_company_id = $user->user_role->default_company;
        $yaco_materials = Material::select('id', 'name')->get();
        $wellfit_materials = ColourCategory::select('id', 'name')->get();

        $companies_array = explode(",", $companies);

         $roles = [
            'view_stock' => $user->user_role->view_stock,
            'view_reserved_stock' => $user->user_role->view_reserved_stock,
            'view_price' => $user->user_role->view_price,
            'view_configuration' => $user->user_role->view_configuration,
        ];
        $database  = array("yaco"=>"Focus5034", "wellfit"=>"Focus5019", "wellfit_international_dubai"=>"Focus5054", "demnat"=>"Focus5099");
        return $response = [
            'roles' => $roles,
            'companies_array' => $companies_array,
            'company' => $user->company,
            'database' => $database,
            'assigned_companies' => $assigned_companies,
            'default_company_id' => $default_company_id,
            'yaco_materials' => $yaco_materials,
            'wellfit_materials' => $wellfit_materials,
        ];

    }



}
