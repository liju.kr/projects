<?php

namespace App\Http\Controllers\Data;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::orderby('name')->get();
        return view('pages.company.partials.list',compact('companies'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        return view('pages.company.partials.add-company');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $company = Company::find($id);
        return view('pages.company.partials.edit-company',compact('company'));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'database_name' => 'required',
            'one_zero' => 'required|numeric|max:1|min:0',
        ])->validate();

        $company = new Company();
        $company->name = $request->name;
        $company->one_zero = $request->one_zero;
        $company->previous_database_name = $request->previous_database_name;
        $company->database_name = $request->database_name;
        $company->user_id = auth()->id();
        $company->save();

        return back()->with('status', 'Company Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'database_name' => 'required',
            'one_zero' => 'required|numeric|max:1|min:0',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/colours');
        $company = Company::find($id);
        $company->name = Input::get('name');
        $company->one_zero = Input::get('one_zero');
        $company->previous_database_name = Input::get('previous_database_name');
        $company->database_name = Input::get('database_name');
        $company->user_id = auth()->id();
        $company->save();
        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Company Modified!');
    }
    public function destroy($id)
    {
        // delete
        $company = Company::find($id);

        foreach ($company->colours as $colour)
        {
            $colour->delete();
        }
        $company->delete();

        // redirect
        return redirect('companies/')->with('status', 'Company Deleted!');
    }
}
