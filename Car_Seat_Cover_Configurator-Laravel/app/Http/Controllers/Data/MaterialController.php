<?php

namespace App\Http\Controllers\Data;

use App\Models\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class MaterialController extends Controller
{
    //
    public function index()
    {
        $materials = Material::get();
        return view('pages.materials.partials.list',compact('materials'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        return view('pages.materials.partials.add-materials');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $material = Material::findOrFail($id);
        return view('pages.materials.partials.edit-materials',compact('material'));
    }
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'min_price' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $material = new Material();
        $material->name = $request->name;
        $material->min_price = $request->min_price;
        $path = $request->file('image')->store('public/materials');
        $material->image = str_replace("public/","",$path);
        $material->save();
        return redirect('/materials/')->with('status', 'Material Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'min_price' => 'required',
            'image' => 'mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $material = Material::findOrFail($id);
        $material->name =  Input::get('name');
        $material->min_price =  Input::get('min_price');
        if(Input::file('image')) $path = Input::file('image')->store('public/materials');
        if(Input::file('image'))  $material->image = str_replace("public/","",$path);
        $material->save();
        return back()->with('status', 'Material Updated!');
    }
    public function destroy($id)
    {
        // delete
        $material = Material::findOrFail($id);
        $material->delete();
        // redirect
        return redirect('materials/')->with('status', 'Material Deleted!');
    }

    public function getMaterials(Request $request)
    {

        return   $materials = Material::orderBy('name', 'asc')
            ->paginate(36);

    }
}
