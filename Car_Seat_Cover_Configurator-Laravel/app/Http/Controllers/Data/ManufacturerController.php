<?php

namespace App\Http\Controllers\Data;

use App\Models\Layout;
use App\Models\Variant;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use Illuminate\Support\Facades\Input;
use App\Models\Manufacturer;

class ManufacturerController extends Controller
{
    public function index()
    {
        $manufacturers = Manufacturer::orderby('name')->get();

    	return view('pages.manufacturer.partials.list',compact('manufacturers'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
    	return view('pages.manufacturer.partials.add-manufacturer');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
       $manufacturer = Manufacturer::find($id);
       return view('pages.manufacturer.partials.edit-manufacturer',compact('manufacturer'));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
		    'name' => 'required',
		    'image' => 'required|mimes:jpeg,bmp,png|max:10240',
		])->validate();

        $path = $request->file('image')->store('public/manufacturers');
        $manufacturer = new Manufacturer();
        $manufacturer->name = $request->name;
        $manufacturer->user_id = auth()->id();
        $manufacturer->image = str_replace("public/","",$path);
        $manufacturer->save();
        
        return back()->with('status', 'Manufacturer Added!');
    }
    public function update($id)
    {
         Validator::make(Input::all(), [
		    'name' => 'required',
		    'image' => 'mimes:jpeg,bmp,png|max:10240',
		])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/manufacturers');
        $manufacturer = Manufacturer::find($id);
        $manufacturer->name = Input::get('name');
        $manufacturer->user_id = auth()->id();
        if(Input::file('image'))  $manufacturer->image = str_replace("public/","",$path);
        $manufacturer->save();

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Manufacturer Modified!');
    }
    public function destroy($id)
    {
        // delete
        $manufacturer = Manufacturer::find($id);
        $manufacturer->delete();

        // redirect
        return redirect('/manufacturers/')->with('status', 'Manufacturer Deleted!');
    }
    public function getList(Request $request)
    {

        $user_id = $request->user_id;
        $user = User::findOrFail($user_id);
        if($user->company=="YACO") //  if($user->role == "sub-distributor")
        {
            if($user->role == "yaco_user")
            {
                $brands = Manufacturer::where('id','!=',env('CUST_MAN_ID'))
                    ->where('id','!=',env('EMBROIDERY_BRAND_ID'))
                    ->where('id','!=',env('UNIVERSAL_BRAND_ID'))
                    ->orderBy('name')->get();
            }
            else
            {
                $brands = Manufacturer::where('id','!=',env('CUST_MAN_ID'))
                    ->where('id','!=',env('EMBROIDERY_BRAND_ID'))
                    ->orderBy('name')->get();
            }

            $embroidery_status =   env('EMBROIDERY_STATUS');
            $layout = Layout::findOrFail(env('EMBROIDERY_LAYOUT_ID'));
            $brand = Manufacturer::findOrFail(env('EMBROIDERY_BRAND_ID'));
            $gallery_status =1;
        }
        else
        {
            $brands = Manufacturer::where('id','!=',env('CUST_MAN_ID'))->where('id','!=',env('EMBROIDERY_BRAND_ID'))->orderBy('name')->get();
            $embroidery_status = 0;
            $layout = [];
            $brand = [];
            $gallery_status =0;
        }
        $gallery_banner = env('APP_URL').'/images/gallery_banner.jpg';
        return response()->json([
            'manufacturers' => $brands,
            'embroidery_status' => $embroidery_status,
            'embroidery_model_id' => env('EMBROIDERY_CAR_MODEL_ID'),
            'embroidery_car_id' => env('EMBROIDERY_CAR_ID'),
            'embroidery_layout' => $layout,
            'embroidery_brand' => $brand,
            'gallery_status' => $gallery_status,
            'gallery_new_data' => true,
            'gallery_banner' => $gallery_banner,
        ]);

    }
    public function getCars(Request $request)
    {
        $manufacturer = Manufacturer::findOrFail($request)->first();
        return $manufacturer->cars;
    }

}
