<?php

namespace App\Http\Controllers\Data;

use App\Models\LayoutDesign;
use App\Models\LayoutImage;
use App\Models\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CarModel;
use App\Models\Layout; 
use Validator;
use Illuminate\Support\Facades\Input;

class LayoutController extends Controller
{
    public function index($model_id)
    {
        $car_model = CarModel::findOrFail($model_id);
        $tab = "layouts";
    	return view('pages.seats.partials.layouts-list',compact('car_model','tab'));
    }

    public function create($model_id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $car_model = CarModel::findOrFail($model_id);
        $materials = Material::orderby('id', 'ASC')->get();
        $tab = "add-layout";
        return view('pages.seats.partials.add-layout',compact('car_model','tab', 'materials'));
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
       $layout = Layout::findOrFail($id);
       $car_model = CarModel::findOrFail($layout->car_model_id);
        $materials = Material::orderby('id', 'ASC')->get();
       $assignedMaterials  = $layout->materials;
       //return $materials[0]->layouts[0]->pivot;
      //  return $assignedMaterials[0]->pivot->price;
       return view('pages.seats.partials.edit-layout',compact('car_model','layout','assignedMaterials', 'materials'));
    }

    public function show($id)
    {
        $layout = Layout::findOrFail($id);
        return view('pages.seats.partials.view-layout',compact('layout'));
    }
    public function store(Request $request, $car_model_id)
    {

        Validator::make($request->all(), [
            'name' => 'required',
            'sort.*' => 'min:0',
            'image.*' => 'mimes:jpeg,bmp,png|max:10240',
            'design_sort.*' => 'min:0',
            'design_image.*' => 'mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $materials = Material::orderby('id', 'ASC')->get();

        $layout = new Layout();
        $layout->name = $request->name;
        if(is_numeric(trim(substr($request->name, -4))))
        {
            $layout->code_no=trim(substr($request->name, -4));
        }
        else
        {
            $layout->code_no=0;
        }
        $layout->description = $request->description;
        $layout->more_information = $request->more_information;
        $layout->car_model_id = $car_model_id;
        $layout->old_id = 0;
       // $layout->image = str_replace("public/","",$path);
        $layout->image = null;
        $layout->user_id = auth()->id();
        $layout->save();

        $images = $request->file('image');
        $sort = $request->input('sort');
        if(!empty($images))
        {
            foreach ($images as $key=>$image)
            {
                if($image)
                {
                    $path = $image->store('public/catalog/new_layouts');
                    $layout_image= new LayoutImage();
                    $layout_image->layout_id = $layout->id;
                    $layout_image->image = str_replace("public/","",$path);
                    $layout_image->sort = $sort[$key];
                    $layout_image->user_id = auth()->id();
                    $layout_image->save();
                }
            }
        }


        $design_images = $request->file('design_image');
        $design_sort = $request->input('design_sort');
        $design_name = $request->input('design_name');
        $design_description = $request->input('design_description');
        if(!empty($design_images))
        {
            foreach ($design_images as $key1=>$design_image)
            {
                if($image)
                {
                    $path1 = $design_image->store('public/catalog/new_layout_designs');
                    $layout_design= new LayoutDesign();
                    $layout_design->layout_id = $layout->id;
                    $layout_design->image = str_replace("public/","",$path1);
                    $layout_design->sort = $design_sort[$key1];
                    $layout_design->name = $design_name[$key1];
                    $layout_design->description = $design_description[$key1];
                    $layout_design->user_id = auth()->id();
                    $layout_design->save();
                }
            }
        }

        foreach ($materials as $index=>$material)
        {
                $layout->materials()->attach($material->id, ['price' => $request->price[$index]]);
        }

        return redirect('/models/'.$car_model_id.'/layouts')->with('status', 'Layout Added!');
    }
    public function update(Request $request, $id)
    {
        $max_year = date('Y')+2;

         Validator::make(Input::all(), [
		    'name' => 'required',
             'sort.*' => 'min:0',
             'sort_order.*' => 'min:0',
             'design_sort.*' => 'min:0',
             'design_sort_order.*' => 'min:0',
             'image.*' => 'mimes:jpeg,bmp,png|max:10240',
             'image_preview.*' => 'mimes:jpeg,bmp,png|max:10240',
             'design_image.*' => 'mimes:jpeg,bmp,png|max:10240',
             'design_image_preview.*' => 'mimes:jpeg,bmp,png|max:10240',
             'year_from' => 'required|min:0|numeric|digits:4|max:'.$max_year,
             'year_to' => 'required|numeric|digits:4|max:'.$max_year.'|min:'.$request->year_from,

		])->validate();

       // return $request;

        $materials = Material::orderby('id', 'ASC')->get();
        $layout = Layout::find($id);
        $layout->name = Input::get('name');
        if(is_numeric(trim(substr(Input::get('name'), -4))))
        {
            $layout->code_no=trim(substr(Input::get('name'), -4));
        }
        else
        {
            $layout->code_no=0;
        }
        $layout->description = Input::get('description');
        $layout->more_information = Input::get('more_information');
        $layout->user_id = auth()->id();
        $layout->save();



        $image_previews = $request->file('image_preview');
        $sort_order = $request->input('sort_order');
        $layout_image_ids = $request->input('layout_image_id');

        $design_image_previews = $request->file('design_image_preview');
        $design_sort_order = $request->input('design_sort_order');
        $design_name = $request->input('design_name');
        $design_description = $request->input('design_description');
        $design_layout_image_ids = $request->input('design_layout_image_id');


        if(!empty($layout_image_ids))
        {
            foreach ($layout_image_ids as $key1=>$layout_image_id)
            {
                $layout_image= LayoutImage::findOrFail($layout_image_id);
                $layout_image->sort = $sort_order[$key1];
                $layout_image->user_id = auth()->id();
                $layout_image->save();
            }

            if(!empty($image_previews))
            {
                foreach ($image_previews as $key2=>$image_preview)
                {
                    $layout_image= LayoutImage::findOrFail($layout_image_ids[$key2]);
                    $path1 = $image_preview->store('public/catalog/new_layouts');
                   if(file_exists('storage/'.$layout_image->image)) unlink('storage/'.$layout_image->image);
                    $layout_image->image = str_replace("public/","",$path1);
                    $layout_image->user_id = auth()->id();
                    $layout_image->save();
                }
            }
        }

        if(!empty($design_layout_image_ids))
        {
            foreach ($design_layout_image_ids as $key11=>$design_layout_image_id)
            {
                $layout_design= LayoutDesign::findOrFail($design_layout_image_id);
                $layout_design->sort = $design_sort_order[$key11];
                $layout_design->name = $design_name[$key11];
                $layout_design->description = $design_description[$key11];
                $layout_design->user_id = auth()->id();
                $layout_design->save();
            }

            if(!empty($design_image_previews))
            {
                foreach ($design_image_previews as $key22=>$design_image_preview)
                {
                    $layout_design= LayoutDesign::findOrFail($design_layout_image_ids[$key22]);
                    $path11 = $design_image_preview->store('public/catalog/new_layout_designs');
                    if(file_exists('storage/'.$layout_design->image)) unlink('storage/'.$layout_design->image);
                    $layout_design->image = str_replace("public/","",$path11);
                    $layout_design->user_id = auth()->id();
                    $layout_design->save();
                }
            }
        }

        $images = $request->file('image');
        $sort = $request->input('sort');
        if(!empty($images))
        {
            foreach ($images as $key=>$image)
            {
                if($image)
                {
                    $path = $image->store('public/catalog/new_layouts');
                    $layout_image= new LayoutImage();
                    $layout_image->layout_id = $layout->id;
                    $layout_image->image = str_replace("public/","",$path);
                    $layout_image->sort = $sort[$key];
                    $layout_image->user_id = auth()->id();
                    $layout_image->save();
                }
            }
        }

        $design_images = $request->file('design_image');
        $design_sort = $request->input('design_sort');
        $design1_name = $request->input('design1_name');
        $design1_description = $request->input('design1_description');
        if(!empty($design_images))
        {
            foreach ($design_images as $key3=>$design_image)
            {
                if($design_image)
                {
                    $path3 = $design_image->store('public/catalog/new_layout_designs');
                    $layout_design= new LayoutDesign();
                    $layout_design->layout_id = $layout->id;
                    $layout_design->image = str_replace("public/","",$path3);
                    $layout_design->sort = $design_sort[$key3];
                    $layout_design->name = $design1_name[$key3];
                    $layout_design->description = $design1_description[$key3];
                    $layout_design->user_id = auth()->id();
                    $layout_design->save();
                }
            }
        }


        $materialsData = array();
        foreach ($materials as $index=>$material)
        {
            $materialsData[$material->id] = ['price' => $request->price[$index]];
        }
        $layout->materials()->sync($materialsData);


        $year_from = $request->year_from;
        $year_to = $request->year_to;

        if(($year_from != $layout->car_model->year_from) || ($year_to != $layout->car_model->year_to))
        {
            $car_id = $layout->car_model->car->id;
            $car_model = CarModel::where('car_id',$car_id)->where('year_from',$year_from)->where('year_to',$year_to)->first();
            if($car_model)
            {
                $layout = Layout::find($id);
                $layout->car_model_id = $car_model->id;
                $layout->user_id = auth()->id();
                $layout->save();
            }
            else
            {
                $new_car_model = new CarModel();
                $new_car_model->name = $year_from." - ".$year_to;
                $new_car_model->year_from = $year_from;
                $new_car_model->year_to = $year_to;
                $new_car_model->car_id = $car_id;
                $new_car_model->user_id = auth()->id();
                $new_car_model->save();

                $layout = Layout::find($id);
                $layout->car_model_id = $new_car_model->id;
                $layout->save();
            }

        }

        return back()->with('status', 'Layout Modified!');
    }
    public function destroy($id)
    {

        $layout = Layout::find($id);
        $car_model_id = $layout->car_model_id;
        $layout_images = $layout->images;
        $layout_designs = $layout->layout_designs;
        $layout_galleries = $layout->galleries;
        foreach ($layout_images as  $layout_image)
        {
            $layout_image->delete();
        }
        foreach ($layout_designs as  $layout_design)
        {
            $layout_design->delete();
        }
        foreach ($layout_galleries as  $gallery)
        {
            $gallery->delete();
        }
        $layout->delete();
        // redirect
        return redirect('/models/'.$car_model_id.'/layouts')->with('status', 'Layout deleted!');
    }

}
