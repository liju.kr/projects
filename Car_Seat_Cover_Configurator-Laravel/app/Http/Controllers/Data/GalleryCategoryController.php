<?php

namespace App\Http\Controllers\Data;

use App\Models\GalleryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class GalleryCategoryController extends Controller
{
    public function index()
    {
        $gallery_categories = GalleryCategory::orderby('name')->get();
        return view('pages.gallery_category.partials.list',compact('gallery_categories'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        return view('pages.gallery_category.partials.add-gallery_category');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $gallery_category = GalleryCategory::find($id);
        return view('pages.gallery_category.partials.edit-gallery_category',compact('gallery_category'));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
        ])->validate();

        $gallery_category = new GalleryCategory();
        $gallery_category->name = $request->name;
        $gallery_category->save();

        return back()->with('status', 'Gallery Category Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/colours');
        $gallery_category = GalleryCategory::find($id);
        $gallery_category->name = Input::get('name');
        $gallery_category->save();
        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Gallery Category Modified!');
    }
    public function destroy($id)
    {
        // delete
        $gallery_category = GalleryCategory::find($id);

        foreach ($gallery_category->photos as $photo)
        {
            $photo->delete();
        }
        $gallery_category->delete();

        // redirect
        return redirect('gallery_categories/')->with('status', 'Gallery Category Deleted!');
    }
}
