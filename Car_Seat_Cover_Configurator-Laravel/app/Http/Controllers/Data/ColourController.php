<?php

namespace App\Http\Controllers\Data;

use App\Models\Colour;
use App\Models\ColourCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use Illuminate\Support\Facades\Input;

class ColourController extends Controller
{
    public function index($colour_category_id)
    {
        $colours = Colour::orderby('name')->where('colour_category_id', $colour_category_id)->get();
        $colour_category = ColourCategory::findOrFail($colour_category_id);
        return view('pages.colour.partials.list',compact('colours','colour_category'));
    }
    public function create($colour_category_id)
    {
        $colour_category = ColourCategory::findOrFail($colour_category_id);
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        return view('pages.colour.partials.add-colour', compact('colour_category'));
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
         $colour = Colour::find($id);
        $colour_category = $colour->colour_category;
        return view('pages.colour.partials.edit-colour',compact('colour', 'colour_category'));
    }

    public function store(Request $request, $colour_category_id)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $path = $request->file('image')->store('public/colours');
        $colour = new Colour();
        $colour->name = $request->name;
        $colour->code = $request->code;
        $colour->colour_category_id = $colour_category_id;
        $colour->user_id = auth()->id();
        $colour->image = str_replace("public/","",$path);
        $colour->save();

        return back()->with('status', 'Colour Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png|max:10240',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/colours');
        $colour = Colour::find($id);
        $colour->name = Input::get('name');
        $colour->code = Input::get('code');
        $colour->user_id = auth()->id();
        if(Input::file('image'))  $colour->image = str_replace("public/","",$path);
        $colour->save();

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Colour Modified!');
    }
    public function destroy($id)
    {
        // delete
        $colour = Colour::find($id);
        $colour_category_id = $colour->colour_category_id;
        $colour->delete();

        // redirect
        return redirect('colour_categories/'.$colour_category_id.'/colours/')->with('status', 'Colour Deleted!');
    }
    public function getColours(Request $request)
    {

        return   $colour_categories = ColourCategory::orderBy('name', 'asc')
            ->with('colours')
            ->paginate(36);

    }
}
