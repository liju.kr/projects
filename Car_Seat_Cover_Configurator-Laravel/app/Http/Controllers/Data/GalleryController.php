<?php

namespace App\Http\Controllers\Data;

use App\Models\Car;
use App\Models\CarModel;
use App\Models\Gallery;
use App\Models\GalleryCategory;
use App\Models\Layout;
use App\Models\Manufacturer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use Illuminate\Support\Facades\Input;

class GalleryController extends Controller
{
    public function index($gallery_category_id)
    {

        $galleries = Gallery::orderby('name')->where('gallery_category_id', $gallery_category_id)->paginate(60);
        $gallery_category = GalleryCategory::findOrFail($gallery_category_id);
        return view('pages.gallery.partials.list',compact('galleries','gallery_category'));
    }
    public function create($gallery_category_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $gallery_category = GalleryCategory::findOrFail($gallery_category_id);
        $manufactures = Manufacturer::orderby('name')->get();
        return view('pages.gallery.partials.add-gallery', compact('gallery_category', 'manufactures'));
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $gallery = Gallery::find($id);
        $gallery_category = $gallery->gallery_category;
        $manufactures = Manufacturer::orderby('name')->get();
        $assignedLayouts  = $gallery->layouts->pluck('id')->toArray();
        if(!empty($assignedLayouts))
        {
            $car_model_ids = Layout::whereIn('id', $assignedLayouts)->pluck('car_model_id');
            $assigned_car_models = CarModel::whereIn('id', $car_model_ids)->get();
            $car_brand_id = $assigned_car_models[0]->car->manufacturer->id;
            $car = $assigned_car_models[0]->car;
            $car_id = $car->id;
            $car_models = CarModel::where('car_id', $car_id)->get();
            $cars = Car::where('manufacturer_id', $car_brand_id)->get();
        }
        else
        {
            $car_models = null;
            $car_brand_id = null;
            $car_id = null;
            $cars = null;
        }
        return view('pages.gallery.partials.edit-gallery',compact('gallery', 'gallery_category', 'manufactures', 'car_models', 'assignedLayouts', 'car_brand_id', 'car_id', 'cars'));
    }

    public function store(Request $request, $gallery_category_id)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png|max:10240',
        ])->validate();

        $path = $request->file('image')->store('public/galleries');
        $gallery = new Gallery();
        $gallery->name = $request->name;
        $gallery->gallery_category_id = $gallery_category_id;
        $gallery->image = str_replace("public/","",$path);
        $gallery->save();

        if(!empty($request->layouts)) $gallery->layouts()->attach($request->layouts);

        return back()->with('status', 'Gallery Added!');
    }
    public function update(Request $request, $id)
    {

        Validator::make(Input::all(), [
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png|max:10240',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/galleries');
        $gallery = Gallery::find($id);
        $gallery->name = Input::get('name');
        if(Input::file('image'))  $gallery->image = str_replace("public/","",$path);
        $gallery->save();


        $gallery->layouts()->sync($request->layouts);

        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Gallery Modified!');
    }
    public function destroy($id)
    {
        // delete
        $gallery = Gallery::find($id);
        $gallery_category_id = $gallery->gallery_category_id;
        $gallery->delete();

        // redirect
        return redirect('gallery_categories/'.$gallery_category_id.'/galleries/')->with('status', 'Gallery Deleted!');
    }
    public function getGalleryCategories(Request $request)
    {
        return   $gallery_categories = GalleryCategory::orderBy('name', 'asc')
            ->paginate(36);

    }

    public function getGalleries(Request $request)
    {


        return  $galleries = Gallery::where('name', 'like', '%' . $request->name . '%')
                ->where('gallery_category_id', $request->category_id)
                ->with('gallery_category', 'layouts.car_model.car.manufacturer')
                ->orderBy('id', 'desc')
                ->paginate(20);
    }


    public  function carBrandModels(Request $request)
    {

        $car_brand_id = $request->car_brand_id;

        if($car_brand_id)
        {
            $cars = Car::where('manufacturer_id',$car_brand_id )->get();
            $data = view('pages.ajax.car_model',compact('cars'))->render();
            return response()->json(['options'=>$data]);
        }
        else
        {
            return response()->json(['options'=>""]);
        }

    }
    public  function carModelLayouts(Request $request)
    {

        $car_id = $request->car_id;

        if($car_id)
        {
            $car_models = CarModel::where('car_id', $car_id)->with('layouts')->get();
            $data = view('pages.ajax.layout',compact('car_models'))->render();
            return response()->json(['options'=>$data]);
        }
        else
        {
            return response()->json(['options'=>""]);
        }

    }
}
