<?php

namespace App\Http\Controllers\Data;

use App\Models\LayoutDesign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LayoutDesignController extends Controller
{
    public function destroy($id)
    {
        // delete
        $layout = LayoutDesign::find($id);
        $layout->delete();

        // redirect
        return back()->with('status', 'Design Deleted!');
    }
}
