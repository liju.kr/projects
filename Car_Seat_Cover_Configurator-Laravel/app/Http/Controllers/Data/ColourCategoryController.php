<?php

namespace App\Http\Controllers\Data;

use App\Models\ColourCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class ColourCategoryController extends Controller
{
    public function index()
    {
        $colour_categories = ColourCategory::orderby('name')->get();
        return view('pages.colour_category.partials.list',compact('colour_categories'));
    }
    public function create()
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        return view('pages.colour_category.partials.add-colour_category');
    }
    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_data_manager')) return view('pages.errors.no_permission');
        $colour_category = ColourCategory::find($id);
        return view('pages.colour_category.partials.edit-colour_category',compact('colour_category'));
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
        ])->validate();

        $colour_category = new ColourCategory();
        $colour_category->name = $request->name;
        $colour_category->user_id = auth()->id();
        $colour_category->save();

        return back()->with('status', 'Colour Category Added!');
    }
    public function update($id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
        ])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/colours');
        $colour_category = ColourCategory::find($id);
        $colour_category->name = Input::get('name');
        $colour_category->user_id = auth()->id();
        $colour_category->save();
        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Colour Category Modified!');
    }
    public function destroy($id)
    {
        // delete
        $colour_category = ColourCategory::find($id);

        foreach ($colour_category->colours as $colour)
        {
            $colour->delete();
        }
        $colour_category->delete();

        // redirect
        return redirect('colour_categories/')->with('status', 'ColourCategory Deleted!');
    }

}
