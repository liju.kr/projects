<?php

namespace App\Http\Controllers\Main;

use App\Models\EmbroideryOrder;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Auth;
use Validator;
class OrderController extends Controller
{
    public function getOrders(Request $request)
    {
        $user = User::find($request->user_id);
        return $user->orders()->with('configuration')->CreatedDescending()->paginate(12);
    }
    public function order($id)
    {
        $order = Order::find($id);
        return view('pages.orders.view',compact('order'));
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $validator = Validator::make($request->all(), [
            'delivery_date'=>'date|required|after:'.$order->created_at,

        ])->validate();

        $order->delivery_date = $request->delivery_date;

        $order->save();
        return back()->with('status', 'Delivery Date Updated!');
        //return view('pages.orders.view',compact('order'));
    }
    public function orders(Request $request)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
         $orders =  Order::CreatedDescending()->with('configuration')->paginate(12);
        return view('pages.orders.index',compact('orders'));
    }

    public function embroideryOrders(Request $request)
    {
        if(Auth::user()->role=="admin" || Auth::user()->role=="data-manager") $orders =  EmbroideryOrder::CreatedDescending()->paginate(12);
        else $orders =  Auth::user()->orders()->CreatedDescending()->paginate(12);
        return view('pages.orders.em-index',compact('orders'));
    }

    public function embroideryOrder($id)
    {
        $order = EmbroideryOrder::find($id);
        //return $order->configuration
        return view('pages.orders.em-view',compact('order'));
    }

    public function updateEmbroidery(Request $request, $id)
    {
        $order = EmbroideryOrder::find($id);

        $validator = Validator::make($request->all(), [
            'delivery_date'=>'date|required|after:'.$order->created_at,
            'status'=>'required',

        ])->validate();

        $order->delivery_date = $request->delivery_date;
        $order->status = $request->status;

        $order->save();
        return back()->with('status', 'Order Status Updated!');
        //return view('pages.orders.view',compact('order'));
    }




}
