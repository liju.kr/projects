<?php

namespace App\Http\Controllers\Main;

use App\Models\Company;
use App\Models\UserRole;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{

    public function index(Request $request)
    {
        if(!auth()->user()->hasRole('wellfit_admin') && !auth()->user()->hasRole('yaco_admin')) return view('pages.errors.no_permission');
        $users = User::query();
        if(auth()->user()->company !="ALL") $users = $users->where('company', auth()->user()->company);
        $users = $users->paginate(100);


        return view('pages.users.partials.list',compact('users'));
    }

    public function create()
    {
        if(!auth()->user()->hasRole('wellfit_admin') && !auth()->user()->hasRole('yaco_admin')) return view('pages.errors.no_permission');

        $companies = Company::latest()->get();
        return view('pages.users.partials.add-user', compact('companies'));
    }

    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'companies' => 'required',
        ])->validate();

        if(!empty($request->layout_companies))
        {
            $companies="";
            foreach ($request->layout_companies as $company)
            {
                $companies = $companies.$company.",";
            }
            $companies = rtrim($companies, ",");
        }
        else
        {
            $companies=null;
        }

        $user = new User();
        $user->name =  $request->name;
        $user->email =  $request->email;
        $user->contact =  $request->contact;
        $user->status =  $request->status;
        $user->verified =  $request->verified;
        $user->company =  $request->company;
        $user->role =  $request->role;
        $user->password =  Hash::make($request->password);
        $user->user_id =  auth()->id();
        $user->save();

        $user_role = new UserRole();
        $user_role->companies = $companies;
        $user_role->user_id = $user->id;
        if($request->view_stock)$user_role->view_stock = 1; else $user_role->view_stock = 0;
        if($request->view_reserved_stock)$user_role->view_reserved_stock = 1; else $user_role->view_reserved_stock = 0;
        if($request->view_price)$user_role->view_price = 1; else $user_role->view_price = 0;
        if($request->view_configuration)$user_role->view_configuration = 1; else $user_role->view_configuration = 0;
        if($request->can_view_report)$user_role->can_view_report = 1; else $user_role->can_view_report = 0;
        if($request->can_view_report_stock)$user_role->can_view_report_stock = 1; else $user_role->can_view_report_stock = 0;
        $user_role->save();

        $sync_data = [];

        for($i = 0; $i < count($request->companies); $i++)
        {
            if(isset($request->companies[$i]['id'])) $sync_data[$request->companies[$i]['id']] = ['areas' => $request->companies[$i]['areas']];

        }
        $user->companies()->attach($sync_data);
        return redirect('users')->with('status', 'User Added');
    }

    public function edit($id)
    {
        if(!auth()->user()->hasRole('wellfit_admin') && !auth()->user()->hasRole('yaco_admin')) return view('pages.errors.no_permission');
        if(auth()->user()->company !="ALL")
        {
            $user =  User::where('company', auth()->user()->company)->where('id',$id)->first();
            if(!$user) return view('pages.errors.no_permission');
        }
        else
        {
            $user = User::findOrFail($id);
        }
        $assignedCompanies  = $user->companies->pluck('id')->toArray();
        $assignedCompaniesAreas = Company::get()->map(function($company) use ($user) {
            $company->areas = data_get($user->companies->firstWhere('id', $company->id), 'pivot.areas') ?? null;
            return $company;
        });

        $companies = Company::latest()->get();
        return view('pages.users.partials.edit-user',compact('user', 'assignedCompanies', 'companies', 'assignedCompaniesAreas'));
    }

    public function show($id)
    {
        if(!auth()->user()->hasRole('wellfit_admin') && !auth()->user()->hasRole('yaco_admin')) return view('pages.errors.no_permission');
        if(auth()->user()->company !="ALL")
        {
            $user =  User::where('company', auth()->user()->company)->where('id',$id)->first();
            if(!$user) return view('pages.errors.no_permission');
        }
        else
        {
            $user = User::findOrFail($id);
        }


        return view('pages.users.partials.view',compact('user'));

    }

    public function update(Request $request, $id)
    {
        $user= User::find($id);

        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'companies' => 'required',
        ])->validate();

        if(!empty($request->layout_companies))
        {
            $companies="";
            foreach ($request->layout_companies as $company)
            {
                $companies = $companies.$company.",";
            }
            $companies = rtrim($companies, ",");
        }
        else
        {
            $companies=null;
        }


        $user =  User::findOrFail($id);
        $user->name =  $request->name;
        $user->email =  $request->email;
        $user->contact =  $request->contact;
        $user->status =  $request->status;
        $user->verified =  $request->verified;
        $user->company =  $request->company;
        $user->role =  $request->role;
        $user->user_id =  auth()->id();
        $user->save();

        $user_role = $user->user_role;
        $user_role->companies = $companies;
        if($request->view_stock)$user_role->view_stock = 1; else $user_role->view_stock = 0;
        if($request->view_reserved_stock)$user_role->view_reserved_stock = 1; else $user_role->view_reserved_stock = 0;
        if($request->view_price)$user_role->view_price = 1; else $user_role->view_price = 0;
        if($request->view_configuration)$user_role->view_configuration = 1; else $user_role->view_configuration = 0;
        if($request->can_view_report)$user_role->can_view_report = 1; else $user_role->can_view_report = 0;
        if($request->can_view_report_stock)$user_role->can_view_report_stock = 1; else $user_role->can_view_report_stock = 0;
        if($request->default_company)$user_role->default_company = $request->default_company; else $user_role->default_company = null;
        $user_role->save();
        $sync_data = [];

        for($i = 0; $i < count($request->companies); $i++)
        {
            if(isset($request->companies[$i]['id'])) $sync_data[$request->companies[$i]['id']] = ['areas' => $request->companies[$i]['areas']];

        }
        $user->companies()->sync($sync_data);

        return redirect('users')->with('status', 'User Updated');
        //return view('pages.orders.view',compact('order'));
    }




    public function change_password(Request $request, $id)
    {
        Validator::make($request->all(), [
            'password' => 'required|min:6',
        ])->validate();
        $user = User::findOrFail($id);
        $user->password =  Hash::make($request->password);
        $user->save();
        return redirect('users')->with('status', 'User Password Updated');
    }

    public function destroy($id)
    {
        $user= User::find($id);
        $user_role = $user->user_role;
        $user_role->delete();
        $user->delete();
        // redirect
        return redirect('users')->with('status', 'User Deleted!');
    }
    public function logoutAll()
    {
        $users = User::where('id', '!=', Auth::id())->get();
            foreach($users as $user)
            {
                if ($user->AauthAcessToken()) {
                    $user->AauthAcessToken()->delete();
                }
            }
       return redirect('/users')->with('status', 'Logout All Users');
    }
}
