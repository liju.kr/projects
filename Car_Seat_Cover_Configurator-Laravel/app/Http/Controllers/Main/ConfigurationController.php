<?php

namespace App\Http\Controllers\Main;

use App\Mail\OrderReceived;
use App\Models\Layout;
use App\Models\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Seat;
use App\Models\Configuration;
use App\Models\Order;
use App\Models\Layer;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class ConfigurationController extends Controller
{
    public function configurations(){
        if(Auth::user()->role=="admin")  $configurations =  Configuration::CreatedDescending()->with('orders')->paginate(12);
        else $configurations =  Auth::user()->configurations()->CreatedDescending()->with('orders')->paginate(12);
//        return $configurations;
        return view('pages.configurations.index',compact('configurations'));
    }
    public function index($seat_id)
    {

        $seat = Seat::findOrFail($seat_id);
    	$tab = "base";
    	return view('pages.config.tabs.base',compact('seat','tab'));
    }
    public function getLayers(Request $request)
    {

        $seat_check = Seat::findOrFail($request->seat_id);
        $materials = Material::get();
        $base_check=$seat_check->getBaseImageURL();
        if($base_check)
        {

            $seat = Seat::findOrFail($request->seat_id);
            $layout =  Layout::find($request->layout);
            $car_model = $layout->car_model->getName();
            $car = $layout->car_model->car->getName();
            $manufacturer = $layout->car_model->car->manufacturer->getName();
            $layers = $seat->layers()->with('patterns')->get();
            foreach ($layers as $key1=>$layer)
            {
                foreach ($layer->patterns as $key2=>$pattern)
                {
                    $pattern['variants'] = $pattern->variants()->wherePivot('layer_id', '=', $layer->id)->get();
                }
            }
            $base=$seat->getBaseImageURL();
        }
        else
        {
            $seat= Seat::where('id',env('COMMON_MODEL_ID'))->first();
            $layers=$seat->layers()->with('patterns')->get();
            foreach ($layers as $key1=>$layer)
            {
                foreach ($layer->patterns as $key2=>$pattern)
                {
                    $pattern['variants'] = $pattern->variants()->wherePivot('layer_id', '=', $layer->id)->get();
                }
            }
            $layout =  Layout::find($request->layout);
            $car_model = $layout->car_model->getName();
            $car = $layout->car_model->car->getName()." [Custom] ";
            $manufacturer = $layout->car_model->car->manufacturer->getName();
            $base=$seat->getBaseImageURL();
        }



//        if($request->custom)
//        {
//            $seat= Seat::where('id',env('COMMON_MODEL_ID'))->first();
//            $layers=$seat->layers()->with('patterns')->get();
//            foreach ($layers as $key1=>$layer)
//            {
//                foreach ($layer->patterns as $key2=>$pattern)
//                {
//                    $pattern['variants'] = $pattern->variants()->wherePivot('layer_id', '=', $layer->id)->get();
//                }
//            }
//            $layout =  Layout::find($request->layout);
//            $car_model = $layout->car_model->getName();
//            $car = $layout->car_model->car->getName()." [Custom] ";
//            $manufacturer = $layout->car_model->car->manufacturer->getName();
//            $base=$seat->getBaseImageURL();
//        }
//        else
//        {
//            $car =  $seat->car_model->car->getName();
//            $car_model = $seat->car_model->getName();
//            $manufacturer = $seat->car_model->car->manufacturer->getName();
//            $layers = $seat->layers()->with('patterns')->get();
//            foreach ($layers as $key1=>$layer)
//            {
//                foreach ($layer->patterns as $key2=>$pattern)
//                {
//                    $pattern['variants'] = $pattern->variants()->wherePivot('layer_id', '=', $layer->id)->get();
//                }
//            }
//            $base=$seat->getBaseImageURL();
//        }


        return response()->json([
            'layers' => $layers,
            'car_model' =>  $car_model,
            'car' => $car,
            'manufacturer' => $manufacturer,
            'base' => $base,
            'materials' => $materials,
            'vat_percentage' => env('VAT'),
        ]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'=>'required',
		    'selections' => 'required',
		    'image' => 'required',
		])->validate();
        $de = $request->selections;
        $img = $request->image;
        $configuration = new Configuration();
        $configuration->user_id = $request->user_id;
        $configuration->seat_id = $request->seat_id;
        $configuration->base_price = $request->base_price;
        $configuration->design_addon = $request->design_addon;
        $configuration->pattern_addon = $request->pattern_addon;
        $configuration->focus_price = $request->focus_price;
        $configuration->total_price = $request->total_price;
        $configuration->vat = env('VAT');
        $configuration->image_id = $request->layout_image['id'];
        $configuration->selections = json_encode($de);
        $configuration->image = json_encode($img);
        $configuration->selected_image = $request->selected_image;
        $configuration->save();
        return $configuration;
    }
    public function getSaved(Request $request)
    {
        return Configuration::where('user_id',$request->user_id)->with('layout_image')->CreatedDescending()->paginate(12);
    }
    public function confirmOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'=>'required',
		    'configuration_id' => 'required',
		    'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'delivery_date' => 'required',
            'logo' => 'required'
		])->validate();
        $configuration = Configuration::findOrFail($request->configuration_id);
        $order =  new Order();
        $order->user_id = $request->user_id;
        $order->configuration_id = $request->configuration_id;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->total = $request->total;
        $order->net_total = $request->net_total;
        $order->quantity = $request->quantity;
        $order->phone = $request->phone;
        $order->remark = $request->remark;
        $order->logo = $request->logo;
        $order->delivery_date = substr($request->delivery_date,0,10);
        $order->save();
        $image = $configuration->seat->id;
       // Mail::to([$order->email,env('ADMIN_EMAIL')])->send(new OrderReceived($order,$configuration,$image));
        return response()->json([
            'order' => $order,
            'configuration' => $configuration
        ]);
    }
}
