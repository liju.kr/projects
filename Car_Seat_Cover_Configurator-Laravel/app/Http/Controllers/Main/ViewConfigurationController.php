<?php

namespace App\Http\Controllers\Main;

use App\Models\EmbroideryOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class ViewConfigurationController extends Controller
{
    //

    public function view($id)
    {
        $order = Order::find($id);
        return view('pages.orders.view-my-configuration',compact('order'));
    }

    public function viewEmbroideryOrder($id)
    {
        $order = EmbroideryOrder::find($id);
        return view('pages.orders.view-my-em-configuration',compact('order'));
    }

}
