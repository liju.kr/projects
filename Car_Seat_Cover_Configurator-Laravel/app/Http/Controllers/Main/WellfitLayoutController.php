<?php

namespace App\Http\Controllers\Main;

use App\Models\CarModel;
use App\Models\Layout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class WellfitLayoutController extends Controller
{
    public function index(Request $request)
    {

        $layouts = Layout::query();
        $wf_code=null;
        if($request->wf_code)
        {
            $wf_code = $request->wf_code;
            $layouts = $layouts->where('name', 'like', '%' . $wf_code . '%');
        }
        $layouts = $layouts->orderBy('code_no', 'ASC')->with('car_model.car.manufacturer')
            ->with('images')
            ->where('id', '!=', env('EMBROIDERY_LAYOUT_ID'))
            ->paginate(108);
        return view('pages.quick.view-all-layouts',compact('layouts', 'wf_code'));
    }



    public function carModels(Request $request)
    {

        $layouts = Layout::query();
        $year_from=null;
        $year_to=null;
        $search_status=null;
        if($request->search_status=="1")
        {
            $year_from=$request->year_from;
            $year_to=$request->year_to;
            $search_status=$request->search_status;

            $layouts = $layouts->join('car_models', 'layouts.car_model_id', '=', 'car_models.id');

            if($year_from !='all')
            {
                $layouts = $layouts->where('car_models.year_from', $year_from);

            }

            $layouts = $layouts->where('car_models.year_to', $year_to);
            $layouts = $layouts->orderBy('car_models.year_from', 'DESC')->with('car_model.car.manufacturer')->with('images')->get(['layouts.*']);
        }
        else
        {
            $layouts = [];
        }

        return view('pages.quick.view-all-car_models',compact('layouts', 'year_from', 'year_to', 'search_status'));
    }


    public function  updateCarModel(Request $request)
    {
        $max_year = date('Y')+2;
        Validator::make($request->all(), [
            'new_year_from' => 'nullable|min:0|numeric|digits:4|max:'.$max_year,
            'new_year_to' => 'required|numeric|digits:4|max:'.$max_year.'|min:'.$request->year_from,
            'layout_ids' => 'required',
        ])->validate();

        $layout_ids = $request->layout_ids;

        if(!empty($layout_ids)) {
            foreach ($layout_ids as $layout_id) {
                $layout = Layout::findOrFail($layout_id);
                if($request->new_year_from) $year_from = $request->new_year_from;  else $year_from = $layout->car_model->year_from;
                $year_to = $request->new_year_to;

                if (($year_from != $layout->car_model->year_from) || ($year_to != $layout->car_model->year_to)) {
                    $car_id = $layout->car_model->car->id;
                    $car_model = CarModel::where('car_id', $car_id)->where('year_from', $year_from)->where('year_to', $year_to)->first();
                    if ($car_model) {
                        $layout->car_model_id = $car_model->id;
                        $layout->user_id = auth()->id();
                        $layout->save();
                    } else {
                        $new_car_model = new CarModel();
                        $new_car_model->name = $year_from . " - " . $year_to;
                        $new_car_model->year_from = $year_from;
                        $new_car_model->year_to = $year_to;
                        $new_car_model->car_id = $car_id;
                        $new_car_model->user_id = auth()->id();
                        $new_car_model->save();

                        $layout->car_model_id = $new_car_model->id;
                        $layout->user_id = auth()->id();
                        $layout->save();
                    }

                }
            }
        }
        return back()->with('status', 'Model Year Modified!');
       //return $request;
    }

}
