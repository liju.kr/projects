<?php

namespace App\Http\Controllers\Main;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
   public  function getContact(Request $request)
   {

       $user = User::findOrFail($request->user_id);

       if($user && $user->company=="WELLFIT")
       {
           return $response = [
               'whatsapp_no' => "+971 55 676 3949",
               'email' => "wellfit@eim.ae",
               'show_form' => 1,
               'show_address' => 0,
               'address_line_1' => "Wellfit Address Line 1",
               'address_line_2' => "Wellfit Address Line 2",
               'address_line_3' => "Wellfit Address Line 3",
               'address_line_4' => "Wellfit Address Line 4",
               'address_line_5' => "Wellfit Address Line 5",
           ];
       }
       elseif($user && $user->company=="YACO")
       {
           return $response = [
               'whatsapp_no' => "+971 52 708 8102",
               'email' => "r.sreedher86@gmail.com",
               'show_form' => 1,
               'show_address' => 0,
               'address_line_1' => "Yaco Address Line 1",
               'address_line_2' => "Yaco Address Line 2",
               'address_line_3' => "Yaco Address Line 3",
               'address_line_4' => "Yaco Address Line 4",
               'address_line_5' => "Yaco Address Line 5",
           ];
       }
       else
       {
           return $response = [
               'whatsapp_no' => "",
               'email' => "",
               'show_form' => 0,
               'show_address' => 0,
               'address_line_1' => "",
               'address_line_2' => "",
               'address_line_3' => "",
               'address_line_4' => "",
               'address_line_5' => "",
           ];
       }



   }

    public function sendMail(Request $request)
    {

            if(1)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'We Will Get Back You Soon',
                    'message_title' => 'Sending Failed',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Try Again Later',
                    'message_title' => 'Sending Failed',
                ]);
            }



    }
}
