<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarModel;
use App\Models\Imported\OldBrand;
use App\Models\Layer;
use App\Models\Layout;
use App\Models\LayoutImage;
use App\Models\Manufacturer;
use App\Models\Old2SeatLayout;
use App\Models\Old2SeatLayoutImage;
use App\Models\PatternVariant;
use App\Models\Seat;
use App\Models\UserRole;
use App\Models\Variant;
use App\Old2Brand;
use App\Old2Car;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewImportController extends Controller
{
    public function trigger_brands()
    {
        return $imported_brands = Old2Brand::get();

//        foreach($imported_brands as $imported_brand){
//            $brand = new Manufacturer();
//            $brand->id = $imported_brand->brand_id;
//            $brand->name = $imported_brand->name;
//            $brand->image = $imported_brand->image;
//            $brand->old_id = $imported_brand->brand_id;
//            $brand->sort = $imported_brand->sort_order;
//            $brand->save();
//        }
//        return "Done";
    }


    public function trigger_cars()
    {
        return   $imported_cars = Old2Car::get();

//        foreach($imported_cars as $imported_car){
//            $car = new Car();
//            $car->id = $imported_car->carmodel_id;
//            $car->name = $imported_car->name;
//            $car->tags = $imported_car->tags;
//            $car->status = $imported_car->status;
//            $car->image = $imported_car->image;
//            $car->old_id = $imported_car->carmodel_id;
//            $car->sort = $imported_car->sort_order;
//            $car->manufacturer_id = $imported_car->brand_id;
//            $car->save();
//        }
//       return "Done";

    }


    public function trigger_car_models()
    {
       $imported_layouts = Old2SeatLayout::get();

        foreach($imported_layouts as $imported_layout){

            $model = CarModel::where('year_from',$imported_layout->year_from)
                ->where('year_to', $imported_layout->year_to)
                ->where('car_id',$imported_layout->carmodel_id)->count();
            if(!$model)
            {
                $car_model = new CarModel();
                $car_model->name = $imported_layout->year_from." - ".$imported_layout->year_to;
                $car_model->year_from = $imported_layout->year_from;
                $car_model->year_to = $imported_layout->year_to;
                $car_model->car_id = $imported_layout->carmodel_id;
                $car_model->old_seat_layout_id = $imported_layout->seatlayout_id;
                $car_model->old_id = 0;
                $car_model->save();
            }

        }
     return "Done";

    }


    public function trigger_layouts()
    {
        return  $imported_layouts = Old2SeatLayout::get();
//        foreach($imported_layouts as $imported_layout){
//
//        $car_model = CarModel::where('year_from',$imported_layout->year_from)
//                ->where('year_to', $imported_layout->year_to)
//                ->where('car_id',$imported_layout->carmodel_id)
//                     ->first();
//
//            $layout = new Layout();
//            $layout->id = $imported_layout->seatlayout_id;
//            $layout->name = $imported_layout->name;
//            $layout->image =null;
//            $layout->description = $imported_layout->description;
//            $layout->more_information = $imported_layout->more_information;
//            $layout->car_model_id = $car_model->id;
//            $layout->old_id =$imported_layout->seatlayout_id;
//            $layout->save();
//        }
//     return "Done";

    }


    public function trigger_layout_images()
    {
       return   $imported_layout_images = Old2SeatLayoutImage::get();
//         foreach($imported_layout_images as $imported_layout_image){
//            $layout_image = new LayoutImage();
//            $layout_image->id = $imported_layout_image->seatlayout_image_id;
//            $layout_image->layout_id = $imported_layout_image->seatlayout_id;
//            $layout_image->image =$imported_layout_image->image;
//            $layout_image->sort = $imported_layout_image->sort_order;
//             $layout_image->save();
//        }
//     return "Done";

    }

    public function testing_query()
    {
//       $layers = Layer::where('id',99)->with("patterns")->get();
//       foreach ($layers as $key1=>$layer)
//       {
//           foreach ($layer->patterns as $key2=>$pattern)
//           {
//               $pattern['variants'] = $pattern->variants()->wherePivot('layer_id', '=', $layer->id)->get();
//           }
//       }
//       return $layers;
     //  return Layer::where('id',99)->with("patterns.variants")->get();

//        $variants = Variant::get();
//        foreach ($variants as $variant)
//        {
//            $pattern_variant = new PatternVariant();
//            $pattern_variant->pattern_id = 1;
//            $pattern_variant->variant_id = $variant->id;
//            $pattern_variant->layer_id = $variant->layer_id;
//            $pattern_variant->save();
//        }
//        //return view('test',compact('car_model_seats'));
    }


}
