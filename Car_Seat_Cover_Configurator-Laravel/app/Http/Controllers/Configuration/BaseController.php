<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Seat;
use Validator;

class BaseController extends Controller
{
    public function index($seat_id)
    {
    	$seat = Seat::findOrFail($seat_id);
    	$tab = "base";
    	return view('pages.config.tabs.base',compact('seat','tab'));
    }
    public function store(Request $request, $seat_id)
    {
        Validator::make($request->all(), [
            'image' => 'required',
        ])->validate();

		$seat = Seat::findOrFail($seat_id);
		$path = $request->file('image')->storeAs('public/base',$seat_id.'.png');
        
        return back()->with('status', 'Base Uploaded!');
    }
}
