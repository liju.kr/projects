<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Seat;
use App\Models\Layer;
use Validator;

class LayerController extends Controller
{
    
    public function create($seat_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
    	$seat = Seat::findOrFail($seat_id);
    	$tab = "add-layer";
    	return view('pages.config.tabs.add-layer',compact('seat','tab'));
    }
    public function edit($layer_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $layer =  Layer::findOrFail($layer_id);
        $tab = $layer->getId();
        $seat = Seat::findOrFail($layer->seat_id);
        return view('pages.config.tabs.edit-layer',compact('seat','tab','layer'));
    }
    public function store(Request $request, $seat_id)
    {
        Validator::make($request->all(), [
            'name' => 'required',
        ])->validate();

		$seat = Seat::findOrFail($seat_id);
        $seat->layers()->create($request->all());
        return back()->with('status', 'Layer Added!');
    }
    public function update($layer_id)
    {
        Validator::make(Input::all(), [
            'name' => 'required',
        ])->validate();

		$layer = Layer::findOrFail($layer_id);
        $layer->name = Input::get('name');
        $layer->save();
        return back()->with('status', 'Layer Modified!');
    }
    public function show($seat_id,$layer_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $seat =  Seat::findOrFail($seat_id);
    	$layer =  Layer::findOrFail($layer_id);
    	$tab = $layer->getId();
    	return view('pages.config.tabs.layer-template',compact('seat','layer','tab'));
    }
    public function destroy($id)
    {
        // delete
        
        $layer = Layer::find($id);
        $layer_id = $layer->seat_id;
        $layer->delete();

        // redirect
        $seat = Seat::findOrFail($layer_id);
    	$tab = "base";
    	return view('pages.config.tabs.base',compact('seat','tab'))->with('status', 'Layer deleted!');
    }
    
}
