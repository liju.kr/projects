<?php

namespace App\Http\Controllers\Configuration;

use App\Models\PatternVariant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Variant;
use App\Models\Layer;
use App\Models\Seat;
use Validator;

class VariantController extends Controller
{
    public function edit($variant_id)
    {
        if(!auth()->user()->hasRole('yaco_data_manager')) return view('pages.errors.no_permission');
        $variant =  Variant::findOrFail($variant_id);
        $layer = Layer::findOrFail($variant->layer_id);
        $tab = $layer->getId();
        $seat = Seat::findOrFail($layer->seat_id);
        return view('pages.config.tabs.edit-variant',compact('seat','tab','layer','variant'));
    }
    public function store(Request $request, $layer_id)
    {
        Validator::make($request->all(), [
            'name' => 'required',
            'colour' => 'required',
            'image' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ])->validate();

		$path = $request->file('image')->store('public/variants');

        $variant = new Variant();
        $variant->name = $request->name;
        $variant->layer_id = $layer_id;
        $variant->colour = $request->colour;
        $variant->image = str_replace("public/","",$path);
        $variant->save();

        $pattern_varriant = PatternVariant::where('pattern_id',$request->pattern_id)->where('layer_id',$layer_id)->where('variant_id',$variant->id)->first();
        if(!$pattern_varriant)
        {
            $new_pattern_variant = new PatternVariant();
            $new_pattern_variant->pattern_id = $request->pattern_id;
            $new_pattern_variant->variant_id = $variant->id;
            $new_pattern_variant->layer_id = $layer_id;
            $new_pattern_variant->save();
        }

        return back()->with('status', 'Variant Added!');
    }
    public function update($id)
    {
         Validator::make(Input::all(), [
		    'name' => 'required',
            'colour' => 'required',
		    'image' => 'mimes:jpeg,bmp,png',
		])->validate();
        if(Input::file('image')) $path = Input::file('image')->store('public/variants');
        $variant = Variant::find($id);
        $variant->name = Input::get('name');
        $variant->colour = Input::get('colour');
        if(Input::file('image'))  $variant->image = str_replace("public/","",$path);
        $variant->save();

        $pattern_varriant = PatternVariant::where('layer_id',$variant->layer_id)->where('variant_id',$variant->id)->first();
        if($pattern_varriant)
        {
            $pattern_varriant->pattern_id = Input::get('pattern_id');
            $pattern_varriant->save();
        }


        // redirect
        // Session::flash('message', 'Successfully updated nerd!');
        return back()->with('status', 'Variant Modified!');
    }
    public function destroy($id)
    {
        // delete
        $variant = Variant::find($id);
        $pattern_variants = PatternVariant::where('layer_id',$variant->layer_id)->where('variant_id',$variant->id)->get();
        foreach ($pattern_variants as $pattern_variant)
        {
            $pattern_variant->delete();
        }
        $variant->delete();

        // redirect
        return back()->with('status', 'Variant deleted!');
    }
}
