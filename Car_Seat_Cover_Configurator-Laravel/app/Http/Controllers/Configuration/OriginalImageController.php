<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Seat;
use Validator;

class OriginalImageController extends Controller
{
    public function index($seat_id)
    {
    	$seat = Seat::findOrFail($seat_id);
    	$tab = "original_image";
    	return view('pages.config.tabs.original_image',compact('seat','tab'));
    }
    public function store(Request $request, $seat_id)
    {
        Validator::make($request->all(), [
            'image' => 'required',
        ])->validate();

		$seat = Seat::findOrFail($seat_id);
		$path = $request->file('image')->storeAs('public/original_images',$seat_id.'.png');
        
        return back()->with('status', 'Original Image Uploaded!');
    }
}
